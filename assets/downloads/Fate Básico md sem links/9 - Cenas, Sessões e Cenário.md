# 9 - Cenas, Sessões e Cenário

## Certo, e Agora?

Você e seu grupo já criaram os personagens, estabeleceram coisas sobre o mundo e conversaram sobre o jogo que estão prestes a iniciar. Agora você tem uma pilha de aspectos e PdNs transbordando potencial dramático e esperando ganhar vida.

O que fazer com tudo isso?

Agora é hora de entrar na parte mais divertida do jogo: criar e jogar cenários.

## O Que É Um Cenário?

Como mencionado em [*Estruturando o Jogo*](), um cenário é uma unidade de tempo de jogo geralmente com duração de três ou quatro sessões e é formado por uma certa quantidade de cenas. O fim de um cenário deve desencadear um marco significativo, permitindo que os seus PJs se aprimorem.

------

### Criando Um Cenário, Passo A Passo

 -  Crie os problemas
 -  Crie perguntas relacionadas à história
 -  Estabeleça a oposição
 -  Organize a primeira cena

------

Em um **cenário**, os PJs enfrentarão e tentarão resolver algum tipo de problema (ou problemas) grande, urgente e com final em aberto. O Narrador geralmente dá início ao cenário apresentando esse problema aos jogadores, e as cenas subsequentes deixam os PJs escolherem como lidar com ele, seja procurando informações, adquirindo recursos ou indo direto à fonte do problema.

Ao longo do caminho, haverão também PdNs que se oporão aos objetivos dos PJs, interferindo em suas tentativas de resolver o problema. Isso pode ser de forma mais direta, do tipo “dois caras armados” derrubando a porta para matar todos ou simplesmente alguém com interesses diferentes dos PJs querendo negociar para que lidem com a questão de outra forma.

Os melhores cenários não possuem um final "certo". Talvez os PJs não resolvam o problema ou o façam de forma que crie más repercussões. Talvez consigam fazer tudo com perfeição. Talvez eles contornem o problema ou mudem a situação para minimizar o seu impacto. Você não saberá até jogar.

Assim que o problema for resolvido (ou não puder ser mais resolvido), o cenário termina. A sessão seguinte dará início a um novo cenário, que pode estar diretamente relacionado ao cenário anterior ou apresentar um problema inédito.

## Crie Problemas

Criar um cenário começa com buscar um problema para os PJs resolverem. Um bom problema é **relevante para os PJs, não pode ser resolvido sem seu envolvimento pessoal e não pode ser ignorado sem que haja consequências terríveis**.

Isso pode parecer uma tarefa difícil. Felizmente, você possui uma excelente ferramenta de contar histórias para ajudá-lo a criar problemas adequados para as suas partidas: aspectos.

Os aspectos dos seus PJs possuem bastante conteúdo histórico – eles representam o que é importante sobre (e para) cada personagem; eles mostram características do mundo do jogo às quais os PJs estão conectados e descrevem os diferentes lados da personalidade de cada um.

Há também os aspectos ligados ao seu jogo – problemas iminentes, aspectos de local e quaisquer outros colocados sobre as características da campanha. Explorá-los pode ajudá-lo a reforçar a sensação de um mundo dinâmico e manter a premissa central do seu jogo em foco.

Por causa de todos esses aspectos, você já possui uma tonelada de histórias possíveis na palma de sua mão – agora só é preciso liberar seu potencial.

Podemos imaginar um problema relacionado a um aspecto como se fosse um ato de forçar de grande escala baseado em um evento. Organizar as coisas dá um pouco mais de trabalho, mas a estrutura é similar – possuir um aspecto sugere algo problemático para um ou todos PJs. Mas, diferentes de um ato de forçar, é algo que não podem resolver ou lidar naquele exato momento.

------

### Você Nem Sempre Precisa Destruir O Mundo

Como verá nos exemplos, nem todas as nossas urgências envolvem necessariamente o destino do mundo ou mesmo de grande parte dele. Problemas interpessoais podem possuir tanto impacto em um grupo de PJs quanto ter que impedir o vilão da semana – ganhar o respeito de alguém ou resolver uma questão pendente entre dois personagens pode ganhar foco em um cenário tanto quanto um grande plano maléfico de um vilão especial.

Se você deseja uma história clássica de ação, veja se pode criar dois problemas para o seu cenário – um que foque em algo externo (como o plano do vilão) e outro que lide com questões pessoais dos personagens. Este último servirá como um drama secundário em seu cenário e cria um tempo para que os personagens se desenvolvam enquanto lidam com outros problemas.

------

### Problemas e Aspectos de Personagem

Quando estiver tentando criar algum problema a partir de um aspecto de personagem, tente encaixá-lo de alguma forma no parágrafo abaixo:  

- Você possui o aspecto \_\_\_, o que implica em \_\_\_ (aqui você pode criar uma lista de coisas). Por isso, \_\_\_ pode se tornar um grande problema para você.

O segundo espaço em branco é o que torna isso mais difícil do que forçar um aspecto baseado num evento – você precisa pensar em todas as implicações do aspecto. Aqui vão algumas questões que podem ajudar.

-  Quem pode ter um problema com o personagem por causa desse aspecto?
-  O aspecto indica alguma ameaça àquele personagem?
-  O aspecto descreve alguma conexão ou relacionamento que poderiam causar problemas ao personagem?
-  O aspecto se refere a algo na história do personagem que poderia voltar para persegui-lo?
-  O aspecto descreve algo ou alguém importante para o personagem que você pode pôr em perigo?

Você pode seguir em frente desde que o que for colocado na terceira lacuna preencha os critérios do início desta seção.

> Fräkeline possui o aspecto ***Famosa Ladra Robótica*** que sugere que sua reputação seja conhecida por muitos. Por causa disso, um sósia começou a cometer crimes em seu nome, provavelmente fazendo com que cidadãos irritados passem a persegui-la em várias cidades, ou até mesmo que haja assassinos na sua cola.
> 
> Esopo tem o aspecto ***O Explorador Sabe Meus Segredos***, o que significa que ele se sente na obrigação de manter o Explorador por perto, em segurança no alcance de suas antenas. Por causa disso, Esopo precisa ajudá-lo a se livrar de alguns velhos companheiros de crime que estão atrás de seu dinheiro.
> 
> Bandu possui ***O Império Quer Minha Cabeça***, o que significa que soldados do Império estão constantemente atrás dele. Isso resulta numa série de tentativas de assassinato e captura começam, planejadas por facínoras em todos os cantos do universo.

### Problemas e os Aspectos de Jogo

Os problemas criados a partir das questões do jogo terão um alcance maior que os problemas baseados nos personagens, afetando todos os seus PJs e possivelmente um número considerável de PdNs também. Eles são menos pessoais, mas isso não os torna menos interessantes.

-  Como \_\_\_ é uma questão do cenário, isso implica em \_\_\_. Portanto, \_\_\_ provavelmente criaria um grande problema para os PJs. Pergunte-se:
-  Que desafos essa questão representa aos PJs?
-  Quem está guiando as coisas por trás dessa questão e que tipo de estrago ele pode ter que fazer para que seu plano seja concretizado?
-  Quem mais se importa em lidar com a questão e como sua “solução” pode ser ruim para os PJs?
-  Qual seria um bom passo para resolver a questão e o que dificulta que isso seja executado?

------

### Dê-Lhe Um Rosto

Embora nem todos os problemas do cenário precisem ser causados diretamente um PdN que serve a um "vilão supremo" que os PJs precisam derrotar, é mais fácil se for assim. Você no mínimo deve ser capaz de apontar um PdN que se beneficia consideravelmente daquele problema não ser resolvido da forma que os PJs gostariam.

------

> Como o ***Clã do Dedo*** é uma questão do cenário, isso implica no seu poder por toda a região do jogo. Portanto um golpe de estado planejado por membros do Clã na cidade em que os PJs estão provavelmente será um grande problema para eles.
> 
> Como ***A Bruxaria Retornará*** é uma questão do cenário, isso implica nos Bruxos de Moondor estarem constantemente tentando cumprir suas antigas profecias e adquirindo novos servos. Portanto uma série de rituais buscando despertar um poder antigo e perigoso que repousa na escuridão do espaço causará problemas para os PJs.
> 
> Como os ***Bruxos de Moondor*** possuem Problemas de Alinhamento, conﬂitos em diversos planetas estão a ponto de acontecer. Uma guerra espacial colocará a vida de inocentes em risco, criando um grande problema para os PJs.

### Problemas e Aspecto Pareados

É aqui que a coisa fica ainda mais interessante. Você também pode criar problemas a partir da *relação entre dois aspectos ao invés de um só*. Isso permite a deixar as coisas em um nível pessoal, mas alargar o âmbito do seu problema para impactar vários personagens ou unir a história de um PJ à história do jogo.

Há duas formas básicas de parear aspectos: conectar aspectos de dois personagens e conectar o aspecto de um personagem a uma questão.

#### Aspectos de Dois Personagens

-  Como \_\_\_ possui o aspecto \_\_\_ e \_\_\_ possui o aspecto \_\_\_, sugere que em \_\_\_. Portanto \_\_\_ provavelmente será um grande problema para eles.

Pergunte-se:

-  Os aspectos colocam os personagens em desacordo ou sugere alguma tensão entre eles?
-  Existe algum tipo específico de problema ou dificuldade ao qual os dois estejam propensos por causa de tais aspectos?
-  Será que um dos personagens tem uma relação ou conexão que poderia tornar-se um problema para outros?
-  Será que os aspectos apontam elementos da história dos personagens que possam se cruzar com fatos recentes?
-  Existe alguma forma da vantagem de um PJ se tornar o infortúnio de outro, por causa dos aspectos?

> Porque Bandu possui ***O Império Quer Minha Cabeça*** e Esopo é um *Mercenário Lagosta*, isso sugere que Esopo pode ser contratado para eliminar seu próprio colega de equipe.
> 
> Como Fräkeline possui ***Se É Valioso, Poderia Ser Meu*** e Esopo possui ***Não Entendo Absolutamente Nada***, isso sugere que eles são provavelmente os piores parceiros possíveis para um roubo enquanto disfarçados. Portanto, um contrato para entrar no Baile Real de Lachesis 4 sem serem notados e chegar até as joias reais provavelmente será um grande problema.
> 
> Como Bandu é um ***Mentalista Andarilho*** e Fräkeline possui ***Falo Comigo Mesma No Passado***, isso sugere que, num momento de confronto, Bandu pode acabar entendendo a verdade sobre as viagens no tempo de Fräk/Zoidai. E como essa informação não pode se tornar pública, faz sentido que Zoidai faça o possível para eliminá-lo.

#### Aspecto de Personagem e Questão

-  Como você possui o aspecto \_\_\_ e \_\_\_ é uma questão do cenário, isso sugere que \_\_\_. Portanto \_\_\_ pode ser um grande problema para você. 

Pergunte-se:

-  Será que uma questão sugere uma ameaça no relacionamento entre os PJs?
-  Será o próximo passo para lidar com uma questão é algo que afeta um personagem em particular por causa de seus aspectos?
-  Será que alguém ligado à questão tem um motivo em especial para ter o PJ como alvo por causa do aspecto?

------

### Quantos Problemas São Necessários?

Para um jogo simples, um ou dois problemas são suficientes. Você verá abaixo que mesmo um único problema pode criar material suficiente para duas ou três sessões. Não pense que você precisa conectar cada PJ a cada cenário - ao invés disso, alterne o foco entre os personagens para que todos tenham seu momento, e a partir disso jogue-os dentro da trama principal que.

------

> Como Fräkeline possui ***Falo Comigo Mesma No Passado*** e o ***Império Mareesiano*** é um problema, isso sugere que o Império pode ter alguma inﬂuência sobre Fräkeline, chantageando-a. Portanto o Império a contrata para um trabalho extremamente perigoso e moralmente duvidoso ameaçando revelar seu segredo e torná-la uma inimiga pública pela galáxia, o que pode ser um grande problema para ela.
> 
> Como Bandu possui ***Funciono Sob Pressão***, e o ***Império Mareesiano*** é um problema, isso sugere que Bandu possui algo que a Imperatriz deseja. Ele pode ser forçado a aceitar utilizar seus poderes mentais contra inimigos do Império. Isso pode acabar voltando a raiva de terceiros para si e trazer problemas relacionados.
> 
> Como Esopo possui ***Não Entendo Absolutamente Nada*** e ***A Bruxaria Retornará!***, Esopo pode se envolver com os Bruxos de Moondor, eventualmente ajudando-os na drenagem de seu mar de origem, o que provavelmente será uma grande dor de cabeça para ele e seus compatriotas.

## Perguntas Narrativas

Agora que há um problema realmente significativo, podemos encorpar a situação um pouco e descobrir qual é a verdadeira questão que o cenário quer resolver – em outras palavras, quais são as questões pertinentes no cerne do problema?

Eis o que faremos: criar uma série de perguntas que o seu cenário responderá. Chamamos isso de **perguntas narrativas**, porque a história emerge naturalmente no processo de resposta.

Quanto mais perguntas você tiver, mais longo será o cenário. Uma a três perguntas provavelmente serão resolvidas em uma sessão. Quatro a oito levará umas duas ou três sessões. Mais que oito ou nove e você talvez possua perguntas até mesmo para o *próximo* cenário, mas isso não é de forma alguma um problema.

Recomendamos fazer tais perguntas com respostas limitadas a si/não, com um formato “Será que (personagem) pode/vai conseguir (objetivo)?”. Você não precisa seguir exatamente esse formato e pode expandir o formato da pergunta de várias formas, o que mostraremos em breve.

Cada problema criado possuirá uma pergunta bem óbvia: “Os PJs podem resolver o problema?”. Você precisa saber disso em algum momento, mas não precisa pular diretamente para essa parte – afinal, se trata do fim do cenário. Coloque outras questões antes dessa que agreguem complexidade ao cenário para então chegar a pergunta final. Tente descobrir o que torna o problema difícil de resolver.

Para criar essas perguntas, você provavelmente terá que reﬂetir sobre o problema que criou, ao menos um pouco, e descobrir detalhes como “quem, o que, como, onde, quando, por que”. Esse é o objetivo do processo.

> **Uma Conspiração Galáctica: Problema e Perguntas**
> 
> Fräkeline possui ***Se É Valioso, Poderia Ser Meu*** e Bandu tem ***O Império Quer Minha Cabeça***, o que sugere que o Império pode usar Fräk como isca para chegar até Bandu. Uma espiã disfarçada conta a Bandu sobre uma possível relação entre sua origem e uma relíquia do Museu que o Império deseja. Ao mesmo tempo, um contrato com um bom pagamento chega até Fräk, solicitando o roubo de um tesouro no Museu.
> 
> Duas perguntas óbvias sobre a história vêm à mente: *Será que Fräkeline consegue o tesouro? Será que Bandu é tentado pela informação?* Mas Amanda quer deixar essas respostas para o final, então pensa em outras questões.
> 
> Primeiramente, ela não sabe se eles concordam plenamente com a situação, então ela começa com: *Fräkeline vai aceitar o trabalho? Será que Bandu correrá atrás dessa pista?*
> 
> Em seguida ela precisa imaginar porque eles não vão direto ao problema. Ela decide que Fräk possui um rival anônimo na busca pelo tesouro do Museu (vamos chamá-lo de “O Cetro de Coronilla”) e um empregador misterioso que prometeu outra boa quantia atraente de créditos se o serviço for completado.
> 
> Bandu, por sua vez, precisa descobrir como chegará à informação que deseja sem se tornar alvo do Império, e provavelmente tentará descobrir precisamente quem está atrás dessa relíquia.
> Isso, então, gera mais três perguntas: *Será que Fräkeline pode encontrar o seu concorrente antes que ele a encontre? Poderá Bandu encontrar um aliado no Museu para defendê-lo? Poderá Bandu descobrir quem está armando para ele, sem sofrer maiores consequências?*
> 
> Então, como ela quer alguma tensão entre os dois, ela acrescenta mais uma pergunta referente à relação entre os personagens: Será que Fräkeline dará as costas a Bandu por causa de seus próprios interesses?

Note que cada uma dessas questões tem o potencial para moldar significativamente o enredo do cenário. Logo de cara, se Bandu decidir ignorar o aviso, você terá uma situação bastante diferente da que aconteceria se ele investigasse. Se a busca de Bandu for uma armadilha do Império, sua captura pode se tornar uma questão principal. Se Fräk decidir ajudar Bandu ao invés de perseguir o Cetro, então eles terão um outro problema para lidar, diferente do que foi pensado para Fräkeline.

Note também que algumas das perguntas possuem algo a mais que modifica a condição básica de “Pode X conseguir Y?”. O motivo pelo qual você pode preferir abordar o assunto dessa forma é o mesmo pelo qual você pode querer evitar rolar os dados às vezes – um resultado de apenas sucesso ou falha nem sempre é interessante, especialmente no caso das falhas.

Veja uma das questões para Fräkeline: “Será que Fräk pode encontrar o seu concorrente *antes que ele a encontre?*” Sem a parte realçada, a questão pode ficar bem entediante – se ela falhar na descoberta, essa pergunta poderá se tornar praticamente inútil na trama, e parte do jogo perde o foco. Isso não é bom.

Da maneira que expressamos, no entanto, está claro que teremos algo acontecendo se ela falhar – talvez ela não saiba quem é o seu rival, mas seu rival sabe quem *ela* é. Aconteça o que acontecer com o Cetro, o rival pode aparecer para caçá-la em um cenário futuro. Podemos também guardar a identidade do rival como algo que possa ser revelado a Fräkeline eventualmente, mas ainda podemos manter alguns conﬂitos e disputas no caminho enquanto um avalia as habilidades do outro.

Há também algum espaço para estender o material de um cenário para outro futuro. Talvez a identidade do oponente de Fräk não seja revelada nessa sessão – tudo bem, pois isso é um detalhe que sempre poderá aparecer mais à frente.

------

Se você acabar com muitas perguntas histórias (mais de oito, por exemplo), tenha em mente que você não precisa necessariamente responder a todas em um único cenário – você pode trazer à tona as questões que ficarem sem resposta como abertura para os cenários seguintes. Na verdade, é exatamente assim que você cria bons arcos ([p. XX]()) – você possui diversas perguntas e leva dois ou três cenários para responder a todas elas.

------

## Estabelecendo a Oposição

Você pode já ter pensado em alguns PdNs ou um grupo deles que são responsáveis pelo que está havendo, mas se não tiver, é preciso começar a montar o elenco de personagens-chave para responder suas perguntas narrativas. Também é preciso acertar suas motivações e objetivos, por que se opõem aos objetivos dos PJs e o que procuram.

No mínimo, você deve responder a algumas perguntas para cada PdN importante em seu cenário:

-  O que esse PdN *precisa*? Como os PJs o ajudam a conseguir isso ou atrapalham seus objetivos?
-  Por que o PdN não pode conseguir o que precisa de forma não problemática? (Em outras palavras, porque isso precisa ser um problema?)
-  Por que ele não pode ser ignorado?

Sempre que puder, tente unificar os PdNs, pois assim não terá muitos personagens para acompanhar. Se um de seus PdNs serve a apenas um propósito em seu cenário, considere se livrar dele e unir o seu papel ao de outro PdN. Isso não apenas reduz sua carga de trabalho, mas também permite que desenvolva um pouco mais a personalidade de cada PdN, tornando-o mais vívido em suas motivações.

Para cada PdN que possuir, veja se precisa transformá-lo em um PdN principal ou de suporte. Estruture-o de acordo com as dicas dadas em [*Estruturando o Jogo*]().

> **Uma Conspiração Galáctica: Oposição**
> 
> Amanda observa as perguntas narrativas e pensa nos PdNs que precisa para respondê-las. Ela cria um lista de suspeitos:
> 
> -  O empregador misterioso de Fräkeline (não aparece)
> -  O curador-chefe do Museu (suporte)
> -  O rival de Fräk pelo Cetro (suporte)
> -  Uma advogada que não faz parte da conspiração (suporte)
> -  Um advogado corrupto associado ao Império (suporte)
> -  O chefe da guarda, secretamente filiado ao Império, que elaborou a conspiração para capturar Bandu (principal)
> 
> São seis PdNs, quatro de suporte, um principal e um que não estará presente no cenário – Amanda ainda não quer criar os detalhes de quem contratou Fräk. Ela também não quer ter que controlar cinco PdNs, então estuda as possibilidades de fusão.
> 
> Uma possibilidade que lhe vem à mente é transformar o concorrente de Fräkeline e a advogada neutra no mesmo personagem, que ela chama de Anya. Anya pode não estar envolvida *nessa* conspiração, mas ela certamente tem motivos mais profundos. O que está acontecendo então? Amanda decide que a motivação de Anya é benéfica; ela deseja pegar o Cetro de Coronilla para mantê-lo longe das mãos corruptas do Império Mareesiano. Ela não sabe nada sobre Fräk e irá confundi-la com uma agente imperial até que as coisas sejam esclarecidas.
> 
> Então ela decide que o chefe da guarda do Museu e o arquiteto da conspiração são a mesma pessoa – ele não confia em ninguém que não seja do Império, então faz o possível para conseguir capturar seus inimigos. Amanda gosta dessa ideia porque seu poder político faz dele um oponente formidável e lhe dá um lacaio, um advogado corrupto. Mas qual é sua motivação para arranjar problemas com Bandu?
> 
> Ela decide, ainda, que seu motivo não é pessoal, mas ele está começando algumas atividades que terão efeito na galáxia, e sabe que entregar Bandu ao Império lhe trará bastante poder e prestígio, acelerando seus planos.
> 
> Quanto ao advogado corrupto, a primeira coisa que vem à mente é um bajulador patético, chorão, que está na palma da mão de seu chefe. Mas ela quer adicionar mais alguns detalhes para dar profundidade, então decide que o chefe da guarda tem algum material para chantageá-lo, o que ajuda a assegurar sua lealdade. Ela ainda não sabe qual é a informação, mas espera que PJs curiosos a ajudarão a criar os detalhes mais tarde.
> 
> Ela nomeia o chefe de Stamatios Wolfram e o advogado corrupto de Festus. Ela agora tem os PdNs que precisava e criará suas fichas.

------

### Vantagens Podem Salvar Seu Trabalho

Ao elaborar os PdNs para o cenário, você não precisa ter nada permanentemente definido quando chegar à mesa – se houver algo que ainda não sabe, sempre será possível estabelecer esses fatos criando Aspectos para os PdNs com ideias vindas das vantagens criadas pelos jogadores. Veja mais conselhos sobre como improvisar esse tipo de coisa durante o jogo na [p. XX]().

------

## Organizando a Primeira Cena

Comece a cena sendo o menos sutil possível – pegue uma das perguntas narrativas, faça algo que coloque a questão em jogo e acerte seus jogadores com ela o mais forte possível. Você não precisa responder nada logo de cara (embora não haja nada de errado em fazer isso), mas você deveria mostrar aos jogadores que a pergunta precisa de resposta.

Dessa forma, você estará definindo o ritmo do jogo, garantindo que os jogadores não andarão em círculos. Lembre-se, eles deveriam ser pessoas proativas e competentes – forneça tudo que for necessário para que ajam dessa forma desde o primeiro instante.

Se você está em uma campanha em andamento, talvez precise das primeiras cenas para resolver as pontas soltas deixadas na sessão anterior. Tudo bem gastar algum tempo nisso, pois ajuda a manter o senso de continuidade que existe de uma sessão para outra. No entanto, logo que houver uma pausa no andamento, atinja-os com a cena inicial de forma rápida e impactante.

> **Uma Conspiração Galáctica: Cena de Abertura**
> 
> Amanda pensa sobre as questões de seu cenário e começa a elaborar a cena inicial. Algumas coisas óbvias vêm à sua mente:
> 
> -  Fräkeline recebe o contrato e os detalhes sobre o trabalho de um empregador misterioso, solicitando mais um acompanhante, e deve decidir se quer ou não assinar.
> 
> -  Bandu deve decidir se confia na informação e partir para saber mais sobre seu passado.
> 
> Amanda decide iniciar com a primeira cena, pois pensa que se Fräk rejeitar o contrato e descobrir que Bandu vai já ia para lá, isso pode criar uma cena divertida na qual ela tenta pedir uma segunda chance ao empregador misterioso. Mesmo que Fräk se mantenha firme em sua decisão, isso determinará se terão ou não que lidar algum problema no caminho para lá, enquanto os lacaios do empregador misterioso os perturbam no caminho.
> 
> Isso não significa que ela vai deixar a cena de Bandu de lado – só vai guardá-la para depois da primeira cena.

------

### Truque do Narrador Ninja Para o Início da Sessão

Conversar com os jogadores para contribuírem com algo para a primeira cena é uma ótima forma de fazê-los investir no que pode vir a acontecer com seus personagens. Se houver pontos ﬂexíveis sobre a cena de abertura, peça aos jogadores para preencherem as lacunas que ainda existirem. Jogadores inteligentes podem tentar usar isso como uma oportunidade para forçar aspectos e conseguir pontos de destino extra – é o estilo de jogo que chamamos de 'incrível'!

> Vamos dar uma olhada nas cenas acima. A trama não especifica onde estão os PJs quando são confrontados com suas escolhas. Então Amanda pode começar a sessão perguntando a Michel “Onde Bandu está exatamente quando a moça o aborda?”
> 
> Agora, mesmo que Michel responda apenas “em uma feira de rua” ou “um bar”, você já solicitou sua participação, que ajudou a definir a cena. Mas Michel é um cara incrível, então ele diz “Ah, provavelmente em um banho público, relaxando depois de um dia de investigação”.
> 
> “Perfeito!” diz Amanda, e pega um ponto de destino “Ela lhe conta a respeito da relíquia, e diz que ouviu boatos que guardas imperiais sabem que você está ali, e como o ***Império quer sua cabeça*** é melhor você fugir.”
> 
> Michel sorri e pega o ponto de destino “Sim, faz sentido".

Claro você também pode considerar seus ganchos de cenário como “pré-forçados”, e distribuir alguns pontos de destino no começo da sessão para indicar aos PJs algum problema com o qual terão de lidar imediatamente. Isso ajuda jogadores com recarga menor e pode alavancar o gasto de pontos de destino logo de cara. Apenas tenha a certeza de que eu grupo concorda em ser posto nessa situação – alguns jogadores não se sentem confortáveis com a perda de controle.

> Amanda quer começar dando alguns pontos de destino aos jogadores.
> 
> “Bandu, já é ruim quando o Império quer te pegar, mas quando você é acuado pela guarda local, sob a pressão, você avista uma nave de caravana para o Museu a alguns metros de você. Decidido a escapar, você corre para dentro da fila e desaparece dentro da nave (2 pontos de destino por ***O Império Quer Minha Cabeça*** e ***Funciono Sob Pressão***).”
> 
> “Esopo, sei que você pensa que ***Bater Sempre Funciona***, então como você vai explicar o que aconteceu quando você tentou consertar o sistema de navegação? (1 ponto de destino por Bater).”
> 
> “Fräkeline, quem fez esse contrato lhe conhece bem. Ele incluiu um chip de créditos rastreável no pacote. O problema é que você pode ser encontrada e presa se não assinar o contrato – e ninguém vai acreditar se você disser que o ganhou (2 pontos de destino por ***Famosa Ladra Robótica*** e ***Se É Valioso, Poderia Ser Meu***).

------

## O Que É Uma Cena?

Uma **cena** é uma unidade de tempo de jogo que dura de alguns minutos a meia hora ou mais, durante a qual os jogadores tentam alcançar um objetivo ou realizar algo significativo no cenário. O conjunto de cenas jogadas formam uma sessão de jogo e, por extensão, também vão formando partes dos cenários, arcos e campanhas.

Assim, você pode olhar para as cenas como a unidade básica de tempo do jogo, além de provavelmente já ter uma boa ideia de como elas são. Não é muito diferente de uma cena em um filme, série de TV ou livro – os personagens principais estão fazendo coisas o tempo todo, normalmente todos no mesmo espaço. Uma vez que a ação siga um novo objetivo, mude de local relacionado àquele objetivo ou salta no tempo, você está na próxima cena.

Como Narrador, um dos trabalhos mais importantes é administrar o início e o fim das cenas. A melhor forma de controlar o ritmo do que acontece em suas sessões é mantendo um controle rígido sobre quando as cenas começam e terminam – deixe as coisas ﬂuírem enquanto os jogadores estiverem investindo na cena e envolvidos nela, mas assim que as coisas começarem a perder o ritmo, siga para a próxima cena. Nesse caso, você pode se inspirar no que os bons editores de filme fazem – você “corta” uma cena e começa uma nova, garantindo assim que a história continue suavemente.

### Iniciando Uma Cena

Ao iniciar uma cena, deixe duas coisas claras:

-  Qual o propósito da cena?
-  Que coisas interessantes estão prestes a ocorrer?

Responder a primeira pergunta é bastante importante, pois quanto mais específico for o propósito de sua cena, mais fácil será para saber quando ela tem que finalizar. Uma boa cena gira em torno de resolver um conﬂito específico ou atingir um alvo específico – uma vez que os PJs forem bem-sucedidos ou falharem no que quer que estejam tentando, a cena acaba. Se a sua cena não possuir um propósito claro, você corre o risco de torná-la mais longa que o necessário e acabar com o ritmo da sessão.

Na maioria das vezes, os próprios jogadores lhe dirão qual o propósito da cena, porque eles sempre estarão dizendo a você o que desejam fazer a seguir. Então se eles disserem “Bem, vamos até o esconderijo do ladrão para ver se podemos botar uma pressão nele”, você saberá o propósito da cena – ela acaba quando os PJs conseguirem pressionar o ladrão ou acabará se os personagens se meterem em uma situação que impossibilite-os se cumprir esse objetivo.

Às vezes, porém, eles serão bastante vagos sobre isso. Se você não possuir um certo instinto para notar suas intenções, faça perguntas até que eles digam a você diretamente o que desejam. Então se um jogador disse “Certo, vou até a taverna para encontrar o meu contato”, isso pode soar um pouco vago – você sabe que haverá um encontro, mas não sabe do que se trata. Você pode perguntar “Qual o seu interesse em encontrá-lo? Você já negociou algum preço para a informação que deseja?” ou outra questão que ajude o jogador a destrinchar sua vontade.

Outras vezes você terá que criar um propósito para a cena, como o início de um novo cenário ou a cena logo depois de um momento de suspense. Independentemente de como faça isso, tente voltar as perguntas sobre a história que criou mais cedo e introduzir uma situação que contribua diretamente para responder alguma delas. Dessa forma, sempre que for começar uma cena, estará movendo a história adiante.

> Amanda terminou a sessão anterior com um suspense: o empregador misterioso de Fräkeline é um agente dos Bruxos de Mondoor, e o Cetro de Coronilla é uma joia necessária para o ritual que trará a bruxaria de volta. Além disso, Bandu é capturado pela guarda do Museu, que o acusa de ter roubado o Cetro, curiosamente a mesma relíquia que Fräk havia sido enviada para roubar.
> 
> Agora Amanda pensa sobre como vai iniciar a próxima sessão. Toda a situação da última sessão assustou os jogadores, então ela definitivamente vai focar nisso. Anya retornará, confusa sobre o papel de Fräk no roubo. A cena será um uma revelação, mostrando que ambas estão do mesmo lado.

Há outra questão importantíssima – uma cena deve começar *logo antes* de algo interessante acontecer. Filmes e séries são especialmente bons nisso – normalmente, você não assiste determinada cena por mais de três segundos antes que algo interessante aconteça para mudar a situação e abalar as coisas.

“Cortar” direto para um momento antes do início da ação ajuda a manter o ritmo e chamar a atenção dos jogadores. Não é preciso detalhar cada momento da caminhada dos PJs da estalagem até o esconderijo do ladrão – isso é perder tempo de jogo onde nada interessante acontece. Ao invés disso, você pode começar a cena com os PJs na porta do esconderijo, frente a uma série de intricadas fechaduras na porta do ladrão.

Se você sentir dificuldades nesta questão, pense em algo que possa complicar o propósito deles ou criar um problema. Você também pode usar o truque ninja que falamos logo antes e fazer perguntas aos jogadores para ajudá-lo a elaborar o que acontecerá de interessante a seguir.

Se você possuir um propósito claro em cada cena e começar quando algo significativo estiver para acontecer, é pouco provável que algo saia errado.

> Amanda inicia a cena com Fräkeline e Esopo voltando para seu alojamento tarde da noite, conversando distraidamente sobre os eventos recentes. Léo sugere que eles não devem ir para lá – não após o roubo. Ele imagina que todos, desde os Bruxos de Moondor ao Império Mareesiano estão procurando por Fräkeline, então eles estão em algum tipo de esconderijo seguro.
> 
> Então são surpreendidos por três estranhos armados que os emboscam assim que chegam próximo à porta.
> 
> “Uou!” Maira pergunta “Como eles sabem que estamos aqui?”
> 
> “Difícil dizer”, Amanda fala, fornecendo um ponto de destino para Maira e Léo. “***Este é um Centro de Comércio, Antro de Vilania***”.
> 
> “Justo”, Léo diz e ambos aceitam forçar o aspecto.
> 
> “Fräk, antes que consiga entrar para o local um dos indivíduos segura uma espada contra sua garganta. O capuz cai aos poucos e revela Anya! Ela parece brava quando diz “Onde está o Cetro, escória imperial?”.

### Finalizando Cenas

Você pode finalizar as cenas da mesma forma que as iniciou, mas em um processo contrário: no momento em que notar que a cena alcançou o seu propósito, passe adiante e chame o fim da cena imediatamente após as ações interessantes terminarem.

Esta é uma abordagem bastante eficiente porque ajuda a manter o interesse na *próxima* cena. Mais uma vez, você vê isso o tempo todo em bons filmes – uma cena termina com parte da ação resolvida, mas também com algum assunto inacabado; é bem aí que cortam para a próxima cena.

Muitas de suas cenas acabarão da mesma forma. Os PJs podem vencer um conﬂito ou alcançar um objetivo, mas sempre haverá algo a ser feito a seguir – conversar sobre a resolução, descobrir o que vão fazer em seguida, etc.

Ao invés de prolongar a cena, no entanto, faça uma sugestão para eles seguirem para uma nova, o que pode ajudar a responder uma das questões não resolvidas na cena atual. Tente fazer com que revelem o que desejam fazer na próxima cena e então volte às duas questões para iniciar uma cena que mostramos acima – qual o propósito da próxima cena e qual a ação iminente interessante que pode acontecer? Parta diretamente para isso.

A única restrição com relação a isso é se os jogadores estiverem muito, mas muito empolgados mesmo com as interações na cena atual. Às vezes as pessoas se empolgam em algo e não conseguem parar e você não deve se preocupar se as coisas tomarem esse rumo. Se o interesse pela cena começar a cair, aproveite a chance para intervir e perguntar sobre a cena seguinte.

### Os Pilares (Competência, Proatividade e Drama)

Sempre que estiver tentando criar ideias para o que deve acontecer na próxima cena, pense mas ideias básicas do *Fate* que apresentamos no capítulo [*O Básico*]() – competência, proatividade e drama.

Em outras palavras, pergunte-se se sua cena está fazendo ao menos uma das seguintes coisas:

-  Dando aos PJs a chance de mostrarem no que eles são realmente bons, seja contra pessoas despreparadas ou contra uma boa quantidade de adversários dignos.
-  Dando aos PJs a chance de fazer algo que possa ser descrito em uma frase simples. “Tentando descobrir informações” é muito vago; “Invadir o gabinete do prefeito” é mais específico. Não precisa ser uma ação física – “convencer o informante a falar” também é uma ação válida.
-  Introduzindo uma escolha difícil ou complicação para os PJs. Sua melhor ferramenta para fazer isso é forçar os aspectos dos PJs, mas se a situação é problemática por si só, talvez não seja necessário.

> O primeiro impulso de Fräkeline é entender o que Anya está falando – mas Amanda sabe que os impulsos de Esopo são... um pouco mais violentos.
> 
> “Eu atiro com minha lança cascavéis!”, Léo grita.
> 
> “Mas... nós nem começamos a conversar”, diz Maira. “Mesmo assim! Para que conversar quando ***Bater Sempre Funciona***?"
> 
> Léo abre a mão e Amanda cede um ponto de destino por forçar o aspecto.

### Use os Aspectos

Outro bom jeito de elaborar cenas interessantes de ação é usar os aspectos dos jogadores para criar uma complicação ou evento baseados neles. Isso é especialmente adequado para aqueles aspectos dos PJs que você não incluiu no problema do cenário, porque isso muda um pouco o foco para eles, mesmo que a história principal não foque nos PJs tanto assim.

> A cena se inicia com um grande julgamento. Bandu está preso, parado em pé diante dos oficiais do Museu do Consenso. Enquanto eles o cobrem de perguntas, eventualmente um dos estudiosos presentes no salão lança um insulto ou uma palavra de desânimo (o que rende um ponto de destino por seu ***Me Ofendo Por Pouco***). A coisa toda parece uma reunião de parlamentares. Fräk e Esopo estão escondidos na ventilação, assistindo tudo a uma distância segura.
> 
> Amanda se vira para os dois e pergunta, “Vocês vão deixar seu companheiro nessa situação?”
> 
> “Não importa. ***Eu Não Entendo Absolutamente Nada***!” grita Léo, narrando como Esopo quebra o teto e cai no centro do salão, na mira dos guardas.
> 
> “Não posso ficar parada depois dessa... porque eu sei que o ***Esopo Precisa Ser Protegido***!”, anuncia Maira, fazendo Fräkeline saltar do telhado no meio da confusão.
> 
> Amanda ri, dizendo “Melhor cena!”, e lança dois pontos de destino, um para Maira e outro para Léo.

## O Cenário em Jogo

Agora você já deve estar pronto para começar: você tem um problema que não pode ser ignorado, uma boa variedade de questões sobre a história que ajudarão a resolver os problemas de uma forma ou de outra, um grupo de PdNs e suas motivações e a primeira cena que fará as coisas pegarem fogo.

A partir de agora deve ser mais fácil, certo? Você apresenta as questões, os jogadores as responderão gradualmente e tudo correrá tranquilamente para uma conclusão.

Bem... acredite, nunca acontece assim.

A coisa mais importante que você deve se lembrar quando começar um cenário é: *tudo sempre acontece diferente do esperado*. Os PJs odiarão um PdN que deveria ser um aliado, terão sucessos que revelarão os segredos mais cedo que o esperado, sofrerão perdas inesperadas e mudarão o curso de suas ações ou inúmeras outras coisas que fazem com que a história não siga o curso planejado.

Observe que não recomendamos predeterminar quais locais e cenas estarão envolvidos em um cenário – isso porque achamos que muito material pode ser perdido, já que muita coisa depende da dinâmica do grupo e de suas escolhas.

No entanto, nem tudo está perdido – as coisas que você preparou o ajudarão quando os personagens fizerem coisas inesperadas. Suas perguntas narrativas são vagas o suficiente para que possam ser respondidas de diversas maneiras, e você pode rapidamente cortar algo que não é tão relevante e substituir por algo sem ter que perder todo o seu trabalho.

> Amanda esperava que a cena com Esopo, Fräk e Anya resultasse num conﬂito rápido, graças a Esopo, seguido de uma conversa onde todos descobrem que estão do mesmo lado.
> 
> Certo? Não.
> 
> O primeiro golpe de Esopo derruba Anya, acabando também seu primeiro contato com a Irmandade do Equilíbrio, uma organização que se opõe ao Império e aos Bruxos. Mudança de planos. Amanda pensa em como continuar a partir dali:
> 
> - Os acompanhantes de Anya gritam “Vingança!” e lutam até a morte.
> 
> - Um dos três assume o papel de Anya e tenta conversar.
> 
> - Os guerreiros fogem (concedendo o conﬂito) e relatam a morte a seus superiores.
> 
> Ela escolhe a terceira opção. Depois de um golpe como esse, ninguém tentaria encarar Esopo. Além disso, Amanda imagina que Maira e Léo irão revistar o corpo, uma boa oportunidade para obterem informações sobre a Irmandade. Também é a chance de trazer Bandu à ação – talvez a Irmandade do Equilíbrio tenha informações que ele queira.

Além disso, conhecer as motivações e objetivos de seus PdNs permite ajustar seus comportamentos mais facilmente do que colocá-los em cena esperando a chegada dos PJs. Quando os PJs tomarem uma decisão complicada, torne os PdNs tão dinâmicos quanto eles, fazendo-os tomar decisões súbitas e surpreendentes para atingir seus objetivos.

> Amanda ainda está um pouco perdida com essa morte inesperada. Ela planejava fazer de Anya uma personagem importante para o arco da história – talvez não uma PdN poderosa, mas uma bastante importante. Amanda pode ao menos tornar sua morte significativa.
> 
> Ela decide que embora a morte de um membro da Irmandade pudesse passar despercebida para a maioria dos habitantes de Ondrin, Meegooo, o Falasiano, certamente ouviria a respeito. Ele já conhecia Esopo de confrontos anteriores com o Clã do Dedo, e se você não pode vencê-los... recrute-os.

## Resolvendo o Cenário

Um cenário termina quando você consegue cenas suficientes para resolver a maioria das questões sobre a história que você criou quando preparou o cenário. Às vezes será possível fazer isso com uma simples sessão se possuir muito tempo ou poucas questões. Se possuir muitas perguntas para o cenário, talvez precise de várias sessões para resolver tudo.

Não sinta a necessidade de responder todas as questões se as coisas se concluíram satisfatoriamente – você pode usar questões não resolvidas para cenários futuros ou deixá-los de lado se não chamaram a atenção dos jogadores.

O fim de um cenário desencadeia um **marco maior**. Quando isso acontece, você também deve verificar se o mundo do jogo precisa avançar de alguma forma.
