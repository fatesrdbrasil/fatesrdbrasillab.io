I"�
<h1 id="players--gamemasters">Players &amp; Gamemasters</h1>

<h2 id="in-any-game-of-fate-youre-either-a-player-or-a-gamemaster">In any game of Fate, you’re either a player or a gamemaster.</h2>

<p><strong>If you’re a player</strong>, your primary job is to take responsibility for portraying one of the protagonists of the game, which we call a player character (or “<abbr title="Player Character">PC</abbr>” for short). You make decisions for your character and describe to everyone else what your character says and does. You’ll also take care of the mechanical side of your character—rolling dice when it’s appropriate, choosing what abilities to use in a certain situation, and keeping track of fate points.</p>

<p><strong>If you’re a gamemaster</strong>, your primary job is to take responsibility for the world the PCs inhabit. You make decisions and roll dice for every character in the game world who isn’t portrayed by a player—we call those non-player characters (or “NPCs”). You describe the environments and places the PCs go to during the game, and you create the scenarios and situations they interact with. You also act as a final arbiter of the rules, determining the outcome of the PCs’ decisions and how that impacts the story as it unfolds.</p>

<p><strong>Both players and gamemasters also have a secondary job: make everyone around you look awesome.</strong> Fate is best as a collaborative endeavor, with everyone sharing ideas and looking for opportunities to make the events as entertaining as possible.</p>

<h3 id="the-example-game">THE EXAMPLE GAME</h3>

<p>All of the rules examples in this site refer to the same example game and
setting. The name is <em>Hearts of Steel</em>, a tongue-in-cheek fantasy romp about a
group of troubleshooters for hire. They traipse about the countryside and get
into trouble at the behest of the various petty kings and fief lords who hire
them.</p>

<p>The participants are Lenny, Lily, Ryan, and Amanda. Amanda is the <abbr title="Game Master">GM</abbr>. Lenny
plays a thuggish swordsman named Landon. Lily plays the nimble, dashing, and
dangerous Cynere, who also happens to love swords. Ryan plays Zird the Arcane,
a wizard who, by contrast, has absolutely no love for swords.</p>

<p>Check out <a href="../../fate-core/game-creation">Game Creation</a> to see how
this game came about. There are character sheets for the example PCs for you
to check out. (Currently no example character sheets. -Site Editor)</p>

<ul>
  <li><a href="/fate-srd/fate-core/what-you-need-play">« What You Need To Play</a></li>
  <li><a href="/fate-srd/fate-core/character-sheet">The Character Sheet »</a></li>
</ul>

:ET