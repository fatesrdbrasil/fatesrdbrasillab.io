---
title: Conflito Social
layout: default
---

## Conflito Social

As perícias sociais do Fate Sistema Básico (Comunicação, Empatia, Enganar e Provocar) já permitem uma variedade de formas de abordar conflitos sociais, mas o seu grupo também pode implementar um sistema de **motivações** e **instintos** para criar mais oportunidades de interações sociais entre PJs e PdNs.

### Motivações e Instintos

A lógica social de qualquer PdN — mesmo os sem importância — possui dois componentes: uma motivação e um instinto derivado dessa motivação. Por exemplo, um professor pode querer proteger seus estudantes ( **_Motivação: Proteger Seus Estudantes do Perigo_** ) ao manter os personagens dos jogadores impossibilitados de interrogar um estudante em particular ( **_Instinto: Negar Acesso aos PJs_** ). Qualquer tentativa exigirá que os PJs alterem a motivação do PdN ou convencê-lo de que algum outro instinto serve melhor para aquela motivação.

### Descobrindo e Modificando Motivações

Claro, motivações às vezes são difíceis de detectar. Enquanto alguns PdN anunciarão claramente porque estão se opondo aos PJs — “Estou aqui para vingar o meu pai!” — a maioria das pessoas não costuma declarar suas questões pessoais para o mundo. A fim de descobrir as motivações de um PdN, o PJ precisa criar vantagem usando a perícia social apropriada. Os personagens podem conseguir algumas pistas em diálogo (Empatia), desafiá-lo através de declarações provocativas (Provocar) ou mesmo fingir ser alguém que o PdN possa confiar (Enganar) na intenção de descobrir a motivação do PdN.

Uma vez que os PJs descubram a motivação do PdN, podem tentar alterar essa motivação usando uma variedade de perícias sociais, substituindo a motivação por algum interesse urgente baseado na nova informação (Comunicação) ou na falsa tentativa de convencer o alvo de que sua motivação é baseada em suposições erradas (Enganar). Por exemplo, um guarda leal (___Motivação: Seguindo as Ordens do Chefe, Instinto: Matar os PJs___) poderia ficar assustado por sofrer uma ameaça impressionante (Provocar), feita na intenção de mudar sua motivação para Salvar Minha Própria Vida.

### Modificando Instintos


Ao invés de tentar influenciar as motivações de um PdN, os PJs também podem criar situações que exijam a atenção imediata ou sugiram que um instinto diferente possa servir melhor à motivação original. Por exemplo, os PJs podem iniciar um incêndio (Provocar) na entrada do hotel para distrair o porteiro ( ___Motivação: Manter o Hotel Funcionando Tranquilamente___) ou tentar convencer um traficante a vender drogas mais barato (Comunicação) na promessa que as remessas futuras recompensarão o prejuízo (___Motivações: Ganhar Rios de Dinheiro___). Essa tentativa de mudar os instintos podem mudar um PdN de ___Instinto: Manter os PJs Distantes___ para ___Instinto: Apagar o Fogo!___ ou ___Instinto: Vender as Drogas a Preço de Mercado___ para ___Instinto: Vender as Drogas Baratíssimo___. No final das contas, essas interações sociais dependem da capacidade dos PJs de oferecer o novo instinto como algo melhor para a motivação original ao invés de mudar a motivação original completamente.

---

Talvez tenha notado que a maioria das motivações são aspectos de situação que podem ser invocados ou forçados por pontos de destino. Lembre-se da Regra de Bronze!

---

### Desafios, Disputas e Conflitos
 
A maioria das tentativas de modificar uma motivação ou instinto de um PdN requerirá uma rolagem de superar contra uma oposição ativa; o PdN rolará uma perícia social apropriada para perceber o engodo ou resistir à conversa traiçoeira. Em alguns casos fará mais sentido usar a mecânica do desafio — tentar convencer um juiz a liberar alguém da prisão antes dele passar para o próximo caso — ou a mecânica de conflito — o sindicato e representantes de empresas conduzem uma negociação que deixa ambos os lados esgotados. Os jogadores também devem ter em mente que seus oponentes possuem pontos sociais fortes e fracos: é fácil convencer um guarda leal que seja estúpido, com Empatia Regular (+1) e Vontade Ótimo (+4) que o seu chefe quer que ele deixe você passar para um encontro secreto (Enganar para modificar o instinto) do que para convencer que ele deve abandonar o seu chefe (Comunicação para alterar a motivação).

Michael Romero e Marika Davis são detetives do departamento de homicídios de Los Angeles e estão investigando o assassinato de um socialite rico, Richard Bentley. Após analisar o caso, descobrem que a esposa de Bentley, Sandra Orastin, é a assassina. Quando aparecem para prendê-la, um de seus seguranças deseja impedir que cheguem perto o bastante para fechar o caso ( **_Instinto: Impedir Que os Tiras Prendam Minha Chefe_** ). 

Sabendo que um conflito físico poderia levar à fuga de Sandra, Marika tenta conversar para passar pelo guarda, iniciando com uma rolagem de Provocar para descobrir a motivação do guarda (criar vantagem com Provocar). Ela possui Provocar Bom (+3) e o resultado dos dados é +3, totalizando um Fantástico (+6). O guarda, um PdN sem importância, possui Enganar Regular (+1) e consegue +2 nos dados, revelando sua motivação: ele não quer ser demitido por deixar sua chefe ser presa. O Narrador adiciona o aspecto **_Motivação: Manter Meu Emprego_** com duas invocações grátis, já que Amarika foi bem-sucedida com estilo. Amarika pergunta a ele porque quer proteger uma assassina e o guarda explica que ele precisa fazer o seu trabalho, mesmo não gostando.

Michael, aproveitando o aspecto que Amarika descobriu, decide que vai tentar alterar o instinto do guarda. Ele sugere que se o guarda quer manter o emprego, ele deve ajudar a polícia a prender Orastin. Afinal, ele vai perder o emprego de qualquer forma se Michael e Amarika tiverem que prendê-lo também.

Michael possui um nível melhor em Comunicação (+3) do que em Provocar (+1), então ele decide focar em convencer o guarda, em vez de assustá-lo (ação de superar usando Comunicação). A rolagem de Michael é medíocre (+0), mas ele usa a invocação grátis em **_Motivação: Manter o Meu Emprego_** para melhorar a sua jogada, totalizando Excepcional (+5). O guarda, com Vontade Razoável (+2), consegue uma rolagem terrível (-2), e Michael o convence que trabalhar com a polícia é a melhor forma de preencher sua motivação. Michael ganha um impulso adicional, **_Ajuda dos Guardas_** por seu sucesso com estilo.

------

- [« Circunstâncias Especiais](../circunstancias-especiais/)
- [Ferramentas Personalizadas »](../ferramentas-personalizadas/)
