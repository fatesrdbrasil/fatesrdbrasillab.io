---
title: Mudanças Estruturais
layout: default
---

## Mudanças Estruturais

Até agora, todos esses sistemas assumem algo que se assemelha à lista de perícia nos pontos mais importantes – há uma lista organizada de uma certa forma e um grupo progressivo de bônus. Mas esses detalhes não precisam ser universais – você pode fazer algumas loucuras ao sair do padrão.


### Alternativas à Pirâmide

A alteração mais óbvia é substituir a pirâmide por um sistema de colunas — como usado nos avanços —, um sistema de pontos ou algo como pacotes de perícias ligados ao histórico do personagem. Isso é algo que você pode fazer livremente, mas antes de fazê-lo tenha certeza de que entende o motivo do uso da pirâmide. ela é rápida, exige que os personagens sejam capazes (o topo), mas bem estruturados (a base) e garante que nem todo personagem seja capaz de fazer tudo (as perícias que você não pode comprar). Não é incoerente alterar isso; um jogo de superespiões, por exemplo, pode ter personagens com bases muito amplas de perícias, mas certifique-se de saber qual problema está tentando resolver ao realizar a mudança.

### Passos Maiores

Nada exige que as perícias progridam vagarosamente. Suponhamos que cada “nível” de uma perícia fosse, na verdade, dois níveis na escala – agora você poderia ter uma pirâmide de 1 perícia Excepcional (+5), 2 em Bom (+3) e 3 ou 4 em
Regular (+1).

Por que fazer isso? Suponhamos que você possua uma lista pequena de perícias, como no caso das profissões ou abordagens. Você terá uma pirâmide de potência heroica — mais ainda do que o padrão — para apenas algumas perícias. Isso também separa claramente as “categorias” dos níveis de perícias, algo que pode ser tematicamente apropriado em certos gêneros.

### Apenas Aspectos

Se quiser tomar uma medida bastante extrema, poderia abrir mão inteiramente das perícias e usar aspectos para tudo. Isso exige uma pequena alteração na forma como os aspectos funcionam. Agora, além do que já fazem, eles concedem um bônus de +1 a todas as situações nas quais se apliquem. Assim, se meus aspectos forem **_Acrobata, Mulherengo, Forte_** e **_Espadachim_** , posso contar com um +2 — uma perícia Razoável na prática - na maioria das lutas com espadas envolvendo
força, o que se eleva para Bom (+3) se eu fizer de modo acrobático.

Se tomar esse rumo será preciso um raciocínio extra na a seleção dos aspectos, de forma que todos compreendam o que representa cada um deles e como se aplicam. O instinto de muitos jogadores será usar aspectos amplos sobre armas,
aspectos simples, como forte ou esperto, porque se aplicam facilmente na maioria das situações. No entanto, a verdadeira vantagem será dos jogadores que tomam mais tempo para criar aspectos mais profundos. **_Cavaleiro_** é bastante abran-
gente, mas **_Cavaleiro das Estrelas_** pode ser mais interessante e **_Cavaleiro Renegado das Estrelas_** pode ser mais profundo e intenso. Como costuma ser o caso, os personagens mais interessantes serão também os mais poderosos.


------

- [« Outras Soluções](../outras-solucoes/)
- [Outras Funções de Perícias »](../outras-funcoes-de-pericias/)
