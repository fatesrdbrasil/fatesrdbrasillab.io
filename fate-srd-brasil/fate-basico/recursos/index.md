---
title: Recursos
layout: default
---

### Recursos

Recursos descreve o nível geral de bens materiais do seu personagem no mundo do jogo, assim como a habilidade de utilizá-los de alguma forma. Isso não precisa significar necessariamente dinheiro na mão, considerando as várias formas de representar riqueza em um dado cenário – em um jogo medieval, pode significar terras, vassalos ou ouro; em um cenário moderno, pode representar seu crédito.

Essa perícia está na lista básica para criar uma forma básica e simples de lidar com riquezas de forma abstrata sem entrar em detalhes contábeis. Algumas pessoas podem achar estranho haver uma perícia estática para algo que costumamos ver como uma fonte limitada de recursos. Se isso lhe incomodar, veja o [quadro abaixo](#limitando-os-recursos) para conhecer formas de limitar esses recursos.

- `O`{: .fate_font} **Superar:** Você pode usar Recursos para se livrar de uma situação onde o dinheiro possa ajudar, como pagar subornos ou adquirir itens raros e caros. Desafios ou disputas podem envolver leilões ou licitações.
- `C`{: .fate_font} **Criar Vantagem:** Você pode usar Recursos para facilitar as coisas e tornar as pessoas mais amigáveis seja através do suborno (***Uma Mão Lava a Outra...***) ou simplesmente pagando drinques para as pessoas (***No Vinho Repousa a Verdade***). Você também pode usar Recursos para declarar que você possui algo útil em mãos ou que pode consegui-la facilmente, o que pode lhe dar um aspecto que represente tal objeto.
- `A`{: .fate_font} **Atacar:** Esta perícia não é utilizada em ataques.
- `D`{: .fate_font} **Defender:** Esta perícia não é utilizada como defesa.

#### Façanhas Para Recursos

-  **O Dinheiro Fala Mais Alto:** Você pode usar Recursos no lugar de Comunicação em qualquer situação na qual dinheiro possa ajudar.
-  **Investidor Experiente:** Você recebe uma invocação gratuita adicional quando criar vantagens com Recursos, contanto representem o retorno de um investimento feito em outra sessão (ou seja, não é possível declarar que já tinha feito, mas se você fizer durante o jogo, você recebe retornos maiores).
-  **Fundos de Investimento:** Duas vezes por sessão, você pode receber um impulso que represente algum tipo de sorte inesperada ou entrada de dinheiro.

------

### Limitando Os Recursos

Caso alguém estiver usando a perícia Recursos exageradamente ou se você simplesmente quer representar o retorno cada vez menor causado pelo gasto constante de suas reservas, considere usar um destes sistemas:

-  Quando um personagem obtiver sucesso em uma rolagem de Recursos, mas não com estilo, dê a ele um aspecto de situação que reﬂita sua perda temporária de recursos, como ***Carteira Vazia*** ou ***Precisando de Dinheiro***. Se isso acontecer novamente simplesmente renomeie o aspecto para algo pior – ***Precisando de Dinheiro*** poderia se tornar ***Falido***, e ***Falido*** poderia se tornar ***Devendo aos Credores***. O aspecto não é uma consequência, mas pode ser um alvo de ações de forçar contra jogadores que não param de gastar. O aspecto pode ser removido se o personagem parar de gastar ou no final da sessão.
-  Sempre que o personagem for bem-sucedido em uma rolagem de Recursos, diminua o nível da perícia em um até o final da sessão. Se ele for bem-sucedido em uma rolagem com um nível Medíocre (+0), não poderá realizar mais rolagens de Recursos naquela sessão.

Se quiser ir além, é possível transformar finanças numa categoria de conﬂito e dar aos personagens caixas de estresse de riqueza, com caixas extras para quem possui Recursos em níveis altos, mas não recomendamos fazer isso a não ser que riqueza material seja realmente algo muito importante no seu jogo.

------

- [« Provocar](../provocar/)
- [Roubo »](../roubo/)
