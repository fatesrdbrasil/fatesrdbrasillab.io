---
title: Fate Condensado - Regras opcionais
layout: default
---
# Regras opcionais

Essas são algumas regras opcionais ou alternativas que você pode decidir usar em seus jogos.

## Condições

As ***condições*** são um substituto para as consequências e as substituem por completo. Condições servem a dois propósitos: elas aliviam a pressão sobre os jogadores e Narrador para definir rapidamente o aspecto da consequência recebida, e te dão a oportunidade para moldar a natureza do seu jogo ao pré-definir as formas como os ferimentos duradouros recaem sobre os personagens.

A versão do ***Fate Condensado*** para as condições pega cada nível de consequência e a separa em duas condições com a metade do valor.

| Físico                                 | Mentais                                   |
| --------------------------             | ----------------------------              |
| `1`{: .fate_font} Arranhado (Leve)     | `1`{: .fate_font} Assustado (Leve)        |
| `2`{: .fate_font} Machucado (Moderado) | `2`{: .fate_font} Abalado (Moderado)      |
| `3`{: .fate_font} Ferido (Severo)      | `3`{: .fate_font}  Desmoralizado (Severo) |

Estes correspondem a estados físicos e mentais, mas só porque você sofreu um ataque físico não significa que você não possa marcar também uma condição mental, e vice-versa, desde que faça sentido. Os ataques são traumáticos\!

As condições são recuperadas da mesma forma que as consequências, baseadas em sua gravidade. Caso você ganhe uma consequência suave adicional, em seu lugar você recebe duas caixas a mais, sendo em _Arranhado_ ou _Assustado_, conforme apropriado.

### Separando ainda mais as Condições

Caso você prefira manter as condições físicas e mentais separadas, dobre o número de caixas de cada uma. Dito isso, existe um limite: se duas caixas no total foram marcadas em qualquer uma das condições em uma linha, nenhuma outra caixa poderá ser marcada nessa linha. Portanto, se você tiver uma caixa (de duas) marcada em _Arranhado_ e nenhuma em _Assustado_, e depois marcar a segunda caixa de _Arranhado_ ou a primeira de _Assustado_, você não poderá marcar mais nenhuma caixa naquela linha.

Se você ganhar um espaço de consequência suave (de um valor alto de Vigor, Vontade ou por uma Façanha), em vez disso, adicione mais duas caixas em Arranhado ou Assustado, conforme o apropriado. Essas caixas adicionadas aumentam o limite de corte para aquela linha, um por um.

### Outras Versões de Condições

Vários jogos publicados baseados em Fate usam condições em vez de consequências. Sinta-se livre para adotar outra implementação ao invés dessa se for melhor para você. Cada uma atinge praticamente o mesmo propósito para o jogo: reduzir a pressão de descobrir os aspectos de consequência de improviso e orientar a natureza do jogo ao limitar os tipos de danos duradouros que os personagens podem sofrer.

## Mudando a lista de perícias

Conforme mencionado [anteriormente](../comecando/#lista-de-perícias), a ***lista de perícias*** é a primeira coisa a se considerar um ajuste ao criar seu próprio jogo utilizando Fate. Nossa configuração padrão apresenta uma lista de 19 perícias organizadas em uma pirâmide de 10 espaços. Essa lista também é estruturada em torno da tradicional ideia de capacidades em vários campos de ação, em essência abordando a questão “o que você pode fazer?” Outras listas de perícias não são necessariamente do mesmo tamanho, nem organizadas da mesma maneira, tampouco abordam a mesma questão. Dito isto, aqui estão algumas listas curtas de perícias para serem consideradas, emprestadas e modificadas:

  - **Ações:** Resistir, Lutar, Saber, Mover, Observar, Pilotar,     Esgueirar, Falar, Consertar.
  - **Abordagens:** Cuidadoso, Esperto, Estiloso, Poderoso, Ágil,    Sorrateiro.
  - **Aptidões:** Atletismo, Combate, Liderança, Conhecimento,    Subterfúgio.
  - **Atributos:** Força, Destreza, Constituição, Inteligência e    Carisma.
  - **Relacionamentos:** Liderança, Parceria, Suporte, Solo.
  - **Papéis:** Piloto, Lutador, Hacker, Construtor, Atravessador,    Ladrão, Mentor.
  - **Temas:** Ar, Fogo, Metal, Mente, Pedra, Vazio, Água, Vento,    Madeira.
  - **Valores:** Dever, Glória, Justiça, Amor, Poder, Segurança,    Verdade, Vingança.

Se você quiser uma lista mais longa, tente começar pela lista padrão, adicionando, combinando e removendo perícias dela à medida que achar necessário, até encontrar o que você está procurando. Você pode, em vez disso, misturar duas ou mais listas descritas acima de algum modo.

**Avanço:** quanto menor é o número de perícias em sua lista em relação à lista padrão, menor é a frequência com que você irá recompensar pontos de perícias devido aos avanços. Permita talvez, apenas durante a evolução dos personagens (mas detalhes [aqui](../avanco/)), ou restringi-las de outro modo.

### Alternativas para a pirâmide:

  - **Diamante:** um meio largo (cerca de um terço das perícias) que     afunila no topo e na base. Por exemplo, 1 em +0, 2 em +1, 3 em +2, 2     em +3 e 1 em +4.
  - **Coluna:** um número aproximado de perícias em cada nível. Se sua     lista for curta o suficiente, isso pode ser uma linha, com uma     perícia por nível.
  - **Livre + Limite:** Dê aos jogadores pontos de perícia o suficiente     para fazerem uma pirâmide (ou outro formato), mas não os obrigue.    Eles podem comprar o que quiserem, desde que fiquem abaixo do    limite.

**Abrangência:** certifique-se de considerar quantas perícias você espera que sejam usadas do total. A lista padrão tem uma taxa de uso de 53% (10 de 19). Quanto maior for essa porcentagem, maior a chance de haver mais jogadores com competências repetidas. Preserve a proteção do nicho de personagens.

**Combinação:** Você pode querer ter duas listas com os jogadores somando uma perícia de cada para realizar sua rolagem. Tenha sempre em mente os limites mínimos e máximos possíveis da combinação dentro da faixa de 0 ao limite estabelecido. Você pode também ter valores de +0 a +2 em cada lista, ou -1 a +1 em uma e +1 a +3 na outra, etc.

## Criação de Personagem Durante o Jogo

Se um jogador se sente confortável em tomar decisões criativas e rápidas de improviso, ele pode gostar de criar personagens *à medida que joga* ao invés de antes do jogo. Isso imita a maneira que os personagens se revelam e se desenvolvem em outras mídias. Não é para todos, mas para grupos que se identificam com esse método, pode ser realmente agradável para todos.

Com esse método, os personagens começam apenas com um nome, um conceito e sua maior perícia, e só\! Conforme o jogo progride e eles são desafiados a usar alguma perícia sem nível, eles podem escolher um espaço vazio e revelar uma nova perícia de imediato. Da mesma forma, os aspectos e façanhas podem ser preenchidos quando as circunstâncias as pedirem, bem no momento em que um ponto de destino é gasto ou um bônus é reivindicado.

## Contagem Regressiva

Uma contagem regressiva adiciona urgência a um adversário ou situação: lide com isso agora ou coisas ficarão piores. Quer você esteja falando de uma bomba-relógio, um ritual quase concluído, um ônibus se equilibrando na beira de uma ponte suspensa ou um soldado com um rádio prestes a chamar reforços, as contagens regressivas forçam os PJs a agir rapidamente ou enfrentar um resultado pior.

Contagens regressivas possuem três componentes: uma trilha de contagem regressiva, um ou mais gatilhos e um resultado.

A ***trilha da contagem*** se parece muito com uma trilha de estresse: é uma linha de caixas que você marca da esquerda para a direita. Toda vez que você marcar uma caixa, a contagem regressiva fica mais próxima do fim. Quanto mais curta for a trilha, mais rápido a catástrofe se aproxima. 

O ***gatilho*** é um evento que marca uma caixa na trilha de contagem regressiva. Ele pode ser tão simples quanto a passagem de minuto, hora, dia ou rodada ou tão específico como “o vilão recebe uma consequência ou é tirado de ação”.

Quando você marca a última caixa, a contagem regressiva termina e o ***resultado*** acontece, seja ele qual for. Os narradores, se desejarem, podem revelar a existência de uma trilha de contagem regressiva para os jogadores sem dizer a eles o que ela representa a princípio, como um tipo de presságio para aumentar a sensação de tensão na história.

Uma contagem regressiva pode ter mais de um gatilho se você quiser; talvez a contagem regressiva avance em um ritmo previsível até que aconteça algo que a acelere. Você também poderia dar um gatilho diferente para cada caixa na trilha de contagem regressiva se quiser que uma série de eventos específicos desencadeiem o resultado.

## Consequências Extremas

As consequências extremas introduzem uma quarta severidade opcional de consequências em seu jogo: alguma coisa que muda permanentemente e irrevogavelmente um personagem.

Tomar uma consequência extrema reduz o estresse recebido em 8. Quando tomado, você deve **substituir** um dos aspectos existentes do seu personagem (que não seja o conceito, o qual está fora de questão) por um aspecto que represente a mudança profunda no personagem resultante do ferimento que ele tomou.

Por padrão, não há opção de recuperação de uma consequência extrema. Ela se tornou parte do personagem agora. No seu próximo progresso, você poderá renomeá-la para que reflita como você se adaptou a ela, mas você não poderá voltar ao aspecto original.

Entre progressos, um personagem só pode usar essa opção uma vez.

## Disputas mais rápidas

Alguns grupos podem sentir que as disputas envolvem muitas tentativas de criar vantagens por rodada. Para esses grupos, tente o seguinte método: em cada rodada de uma disputa, cada participante pode escolher apenas uma das três opções:

  - Rolar superar para seu lado (mais detalhes [aqui](../agindo-rolando-dados/#superar))
  - Rolar para criar vantagem, mas sem bônus de trabalho em equipe (mais detealhes [aqui](../desafios-disputas-conflitos/#trabalho-em-equipe))
  - Fornecer seu bônus de trabalho em equipe para a rolagem de superar    do seu lado ou à tentativa de outro jogador em criar vantagens. Não    role os dados.

## Defesa Total

Às vezes, um jogador (ou o narrador) pode declarar que seu personagem somente se defenderá até o próximo turno ao invés de realizar uma ação nesse turno. Isso se chama defesa total.

Ao declarar defesa total, você deve deixar claro o foco de seus esforços. Por padrão, você está se defendendo (de ataques e esforços para criar vantagens sobre você), mas você pode desejar especificar alguém que está protegendo, ou uma defesa contra um grupo específico de agressores, ou ainda contra um esforço ou resultado particular que você queira se opor.

**Enquanto estiver em defesa total, você ganha +2 para todas as defesas relacionadas ao foco declarado.**

Se nada ocorrer e você não rolou para se defender, no próximo turno você ganha um impulso (mais detalhes [aqui](../aspectos-pontos-destino/#impulsos)) já que ganhou a oportunidade de se preparar para sua próxima ação. Isso compensa “perder sua vez” porque você focou seus esforços em defender-se contra algo que não aconteceu.

## Obstáculos

A característica que define os inimigos é que eles podem ser atacados e retirados de jogo. Em contraste, a característica que define os **obstáculos** é que eles não podem. Os obstáculos tornam as cenas comprovadamente mais difíceis para os PJs, mas os personagens não podem simplesmente lutar contra eles. Os obstáculos devem ser contornados, resistidos ou tornados irrelevantes.

Embora a maioria dos obstáculos sejam características do ambiente, alguns podem ser personagens que não podem ser tirados de jogo usando métodos convencionais. O dragão pode ser um chefe, mas pode também ser um obstáculo perigoso. A estátua animada que impede você de chegar ao mago maligno pode ser uma ameaça, mas também pode ser um bloqueio ou uma distração. Tudo dependerá da função do adversário na cena e como os PJs deverão lidar com isso.

Os obstáculos não aparecem em todas as cenas. Eles servem para acentuar os inimigos na cena, para torná-los mais ameaçadores ou memoráveis, mas o uso excessivo de obstáculos pode ser frustrante para os PJs, particularmente aqueles focados em combate. Você pode usá-los para dar aos PJs menos combativos algo para fazer durante uma luta.

Existem três tipos de obstáculos: perigos, bloqueios e distrações.

### Perigos

Se um obstáculo pode atacar os PJs, ele é um ***perigo***. Jatos de fogo, pedras rolantes ou um atirador de elite longe o suficiente para ser diretamente resolvido: todos são perigos. Cada perigo tem um nome, um nível de perícia e uma Potência de Arma (mais detalhes [aqui](#potência-de-armas-e-armaduras)) de 1 a 4.

O nome do perigo é tanto sua perícia quanto um aspecto, isto é, o nome define o que o perigo pode fazer e seu valor de perícia define o quão bom ele é fazendo isso, mas o nome pode também ser invocado ou forçado como qualquer outro aspecto.

De modo geral, o nível da perícia de um perigo deve ser pelo menos tão alta quanto a maior perícia dos PJs, se não um pouco maior. Um perigo com um nível de perícia *e* Potência de Arma muito altos, provavelmente tirará de cena um ou dois PJs. Você também pode criar um perigo com um nível menor de perícia, mas uma alta Potência de Arma, criando algo que não acerta frequentemente, mas quando acerta, acerta com força. Invertendo isso, se cria um perigo que acerta frequentemente, mas não causa muito dano.

Um perigo age na iniciativa, exatamente como os PJs e seus inimigos fazem. Se suas regras exigem que todos rolem iniciativa, os perigos irão rolar com seu valor de perícia. Em seu turno, a cada rodada, um perigo age como implícito em seu nome e rola com seu nível de perícia. Se ele atacar e acertar com um empate ou melhor, adicione sua Potência de Arma às tensões. Perigos podem atacar ou criar vantagens; eles não podem ser atacados e não superam obstáculos.

Se um jogador quiser superar ou criar vantagem contra um perigo, ele enfrentará uma oposição passiva igual ao seu nível de perigo.

### Bloqueios

Enquanto os perigos existem para ferir os PJs, os ***bloqueios*** os impedem de fazer coisas que eles queiram fazer. Os bloqueios podem causar estresse, embora nem sempre o façam. As principais diferenças entre bloqueios e perigos é que os bloqueios não realizam ações e são mais difíceis de serem removidos. Os bloqueios oferecem oposição passiva em certas circunstâncias e podem ameaçar ou causar dano se não lhes for dada atenção.

Assim como os perigos, bloqueios tem um nome e um nível de perícia, e seu nome é tanto uma perícia quanto um aspecto. Diferente dos perigos, o nível de perícia do bloqueio não deveria ser muito maior que um nível a mais que a perícia mais alta dos PJs, caso contrário, as coisas podem se tornar frustrantes rapidamente. Um bloqueio pode ter uma Potência de Arma tão alto quanto 4, mas não é necessário que tenha uma.

Os bloqueios somente entram em jogo em circunstâncias específicas. Um _tanque de ácido_ só importa quando alguém tenta atravessá-lo ou é arremessado dentro dele. Uma _cerca de arame_ só afeta quem tenta passar por ela. A _estátua animada_ só impede a entrada em uma sala específica.

Os bloqueios não atacam e não possuem um turno nas rodadas de ação. Em vez disso, sempre que um bloqueio interferir na ação de alguém, ele deverá rolar contra o valor do bloqueio como dificuldade base. Se o bloqueio não pode causar danos, ele simplesmente impede que o PJ realize a ação desejada. Se ele pode causar danos e o PJ falha na rolagem de superar, ele toma um golpe igual à quantidade de tensões do teste falho.

Os personagens podem tentar forçar alguém contra um bloqueio como um ataque. Se você fizer isso, você rolará para atacar normalmente, mas adicionará uma Potência de Arma igual à metade da Potência de Arma do bloqueio (arredondado para baixo, mínimo de 1).

Finalmente, alguns bloqueios podem ser usados como cobertura ou armadura. Isso é situacional e para alguns bloqueios isso simplesmente não faz sentido. Você provavelmente não pode se esconder atrás de um _tanque de ácido_, mas uma _cerca de arame_ é uma proteção efetiva contra um taco de beisebol, provavelmente evitando o ataque por completo.

Quando alguém está usando um bloqueio como cobertura, decida se isso mitiga ou nega o ataque. Se nega, o ataque simplesmente não é possível. Se mitiga, o defensor adiciona uma Potência de Armadura igual à metade da perícia do bloqueio (arredondado pra baixo, mínimo 1).

Use os bloqueios com moderação. Os bloqueios podem dificultar a realização de certas ações pelos PJs, então poderá ser frustrante se os usar demais, mas também podem levar os jogadores a pensarem criativamente. Eles podem ver uma oportunidade de usar os bloqueios a seu favor. Se eles descobrirem como, permita que o façam\!

Às vezes, os jogadores querem apenas remover os bloqueios de uma vez. Para fazer isso, faça uma rolagem de superar contra uma dificuldade fixa igual ao nível do bloqueio +2.

### Distrações

Se os perigos atacam diretamente os PJs e os bloqueios impedem que eles realizem certas ações, as ***distrações*** forçam os PJs a decidirem quais são suas prioridades. Dos obstáculos, as distrações costumam ser as menos definidas mecanicamente. Elas também não tornam necessariamente a cena mecanicamente mais difícil, em vez disso, elas apresentam aos PJs decisões difíceis. Aqui estão as parte da distração:

  - O **nome** de uma distração é uma representação breve e incisiva do que     ela é. Pode ser um aspecto, se você quiser ou precise que seja.
  - A **escolha** de uma distração é uma pergunta simples que codifica a     decisão que oferece aos PJ.
  - A **repercussão** da distração é o que acontece aos PJs se eles não     lidam com ela. Algumas distrações podem ter múltiplas repercussões,     incluindo aquelas que surgem ao lidar com a distração *com sucesso*.
  - A **oposição** da distração é uma oposição passiva contra as rolagens     dos PJs que visam lidar com ela. Nem toda distração precisa oferecer     uma oposição.

Se você teme que os PJs lidem facilmente com uma luta que você tem reservada, adicionar uma ou duas distrações pode forçá-los a decidir se é mais importante derrotar os bandidos ou lidar com as distrações.

Lidar com uma distração deve sempre ter um benefício claro ou, caso seja ignorada, não lidar com uma distração deveria sempre ter uma consequência clara.

### Exemplos de Obstáculos

- #### Perigos
  - _Torre de metralhadora_ Ótimo (+4), Arma:3
  - _Sniper Distante_ Excepcional (+5), Arma:4
- #### Bloqueios
  - _Cerca de Arame_ Razoável (+2), dificuldade Ótima (+4) para ser    removida.
  - _Tanque de Ácido_ Bom (+3), Arma:4, dificuldade Excepcional (+5) para    ser removida.
- #### Distrações
  - _O Ônibus Cheio de Civis_ 
    - **Escolha:** o ônibus cairá da ponte?
    - **Oposição:** Bom (+3)
    - **Repercussão (deixá-los):** Todos os civis do ônibus morrem.
    - **Repercussão (salvá-los):** O vilão consegue fugir\!
  - _A Gema Brilhante_
    - **Escolha:** você consegue tirar a gema do pedestal?
    - **Repercussão (deixar a gema):** Você não obtém a gema (de valor inestimável).
    - **Repercussão (pegar a gema):** Você ativa as armadilhas dentro do templo.

## Escala

**Escala** é um subsistema opcional que você pode usar para representar seres sobrenaturais que atuam em um nível além das capacidades gerais da maioria dos personagens em seu jogo. Normalmente você não precisa se preocupar com o impacto da escala em seu jogo. No entanto, pode haver momentos onde será desejável apresentar aos personagens uma ameaça maior que eles normalmente enfrentam, ou uma oportunidade para os personagens lutarem fora de sua categoria de peso normal.

Por exemplo: se você desejar mudar a lista para algo mais adequado ao seu cenário, apresentaremos aqui cinco níveis potenciais de escala: Mundano, Sobrenatural, Extraplanar, Lendário e Divino.

  - **Mundano** representa personagens sem acesso a poderes sobrenaturais ou    tecnologias que os impulsionariam além das capacidades humanas.
  - **Sobrenatural** representa personagens que têm acesso a poderes    sobrenaturais ou tecnologias que vão além da capacidade humana, mas    que permanecem humanos na essência.
  - **Extraplanar** representa personagens incomuns ou únicos cujos poderes    os distinguem das preocupações normais da humanidade.
  - **Lendário** representa espíritos poderosos, entidades e seres    alienígenas para os quais a humanidade é mais uma curiosidade que    uma ameaça.
  - **Divino** representa as forças mais poderosas do universo: arcanjos,    deuses, fadas rainhas, planetas vivos e assim por diante.

Ao aplicar escala para dois indivíduos ou forças opostas, compare os níveis de cada lado e determine quem é maior e por quantos níveis. Eles obtêm *um* dos seguintes benefícios em qualquer ação rolada contra os de escala menor:

  - \+1 por nível de diferença em sua ação *antes* da rolagem;
  - \+2 por nível de diferença para o resultado *após* a rolagem, *somente     se* ela for bem sucedida;
  - 1 invocação gratuita adicional por nível de diferença para os    resultados de uma ação de criar vantagem bem-sucedida.

A aplicação frequente e rígida das regras de escala podem colocar os PJs em uma clara desvantagem. Compense proporcionando generosas oportunidades a esses jogadores de subverterem as desvantagens da escala de maneiras inteligentes. Opções viáveis incluem pesquisar pelos pontos fracos do alvo, mudar para um local onde a escala não se aplique ou alterar os objetivos para que seu oponente não possa aproveitar da sua vantagem de escala.

### Aspectos e Escala

Aspectos de situação ativos às vezes representam um efeito sobrenatural. Nesses casos, o narrador pode determinar que invocar aquele aspecto concede o benefício adicional da sua escala.

Além disso, um aspecto criado sobrenaturalmente pode conceder escala para algumas ações quando invocado. Ele também pode fornecer escala mesmo sem uma invocação, como no caso de um véu mágico ou um traje de camuflagem de alta tecnologia; você não precisa invocar *Velado* para ganhar escala sobrenatural ao se esgueirar.

#### A Escala se aplica ao criar uma vantagem sobrenatural?

Se você está criando uma vantagem e *não há oposição*, ao invés de rolar dados, você simplesmente ganha o aspecto com uma invocação gratuita. Esse aspecto garante a escala conforme descrito anteriormente.

Se você estiver criando vantagem *em alguém para prejudicá-lo*, como lançar *Emaranhado por vinhas animadas* em seu oponente, você pode ganhar escala em seu esforço para criar vantagem.

Se você está criando uma vantagem por meios sobrenaturais e o *grupo adversário pode impedir diretamente seu esforço por meio de interferência física ou sobrenatural*, sua escala pode ser aplicada contra a rolagem de defesa deles.

*Caso contrário*, role para criar vantagem sem escala (provavelmente contra uma dificuldade definida), mas o uso posterior daquele aspecto concede escala quando apropriado.

## Tensões de Tempo

Ao determinar quanto tempo leva para os personagens fazerem algo, você pode querer usar uma abordagem mais sistemática para decidir os impactos de sucesso, de fracasso e das opções “a um custo”. Quão mais lento ou mais rápido? Use estas orientações e deixe que as tensões decidam.

Primeiro, decida quanto tempo a tarefa leva para ser feita com um simples sucesso. Use uma quantidade aproximada mais uma unidade de tempo: “alguns dias”, “meio minuto”, “várias semanas” e assim por diante. Quantidades aproximadas podem ser: metade, cerca de um, alguns ou vários de uma determinada unidade de tempo.

Em seguida, observe por quantas tensões a rolagem excede ou erra o valor alvo. Cada tensão vale um passo na quantidade de onde quer que seja o seu ponto de partida.

Portanto, se o seu ponto de partida é “algumas horas”, então um ponto de tensão mais rápido pula a quantidade para “cerca de uma hora”, duas tensões para “meia hora”. Ir mais rápido do que “metade” reduz a unidade para um nível menor (de horas para minutos, etc) e a quantidade para “vários”, então três pontos de tensão mais rápido faria com que o tempo fosse “vários minutos”. 

No caso de mais lento, é o mesmo processo na direção oposta: um ponto de tensão mais lento são “várias horas”, dois são “meio dia” e três são “cerca de um dia”.

## Maneiras de quebrar as regras para o Grande Mal

Entre combinar perícias e criar vantagens para trabalho em equipe (mais detalhes [aqui](../desafios-disputas-conflitos/#trabalho-em-equipe)), um grupo de PJs pode realmente dominar por completo um único oponente. Isso funciona se você deseja respeitar a vantagem dos números, mas não é tão bom se você deseja apresentar um “Grande Mal” que equivale ao grupo todo.

Mas lembre-se: para monstros e outras grandes ameaças é aceitável quebrar as regras (mais detalhes [aqui](../sendo-narrador/#monstros-grandes-males-e-outras-ameaças)) então faça isso procurando maneiras de neutralizar a vantagem numérica do grupo enquanto dá a eles alguma chance. Aqui estão algumas sugestões de como você poderia fazer isso. Você pode usar uma ou mais dessas em combinação para chefes finais especialmente difíceis ou assustadores.

### Imunidade por Desafio ou Disputa

Ambos os métodos tem como objetivo prolongar o confronto final conduzindo o grupo a uma atividade “contra o relógio” antes que eles possam efetivamente encarar o Grande Mal diretamente.

Com a ***imunidade por desafios***, o seu Grande Mal não pode ser afetado diretamente (fisicamente, mentalmente ou ambos) até que o grupo vença um desafio (por exemplo, desativando a fonte de seu poder, descobrindo qual é sua fraqueza, etc). O Grande Mal, enquanto isso, pode agir livremente e poderá atacá-los durante seus esforços, se opor às ações de superar ou criar vantagens com rolagens de defesa, remover as invocações gratuitas dos PJs com seu próprio superar ou preparar-se para o eventual progresso dos jogadores criando vantagens próprias.

Com a ***imunidade por disputas***, o grupo deverá vencer uma disputa para poder atacar diretamente o Grande Mal; e o Grande Mal pode atacá-los enquanto eles tentam. Se o Grande Mal vence a disputa, ele consegue realizar seus planos e sair ileso.

### Armadura descartável de lacaios

Cercar-se de lacaios é um modo de tentar equilibrar o lado do Grande Mal contra o dos PJs, mas só funcionará até o momento em que os jogadores decidam ir atrás do Grande Mal diretamente e ignorem aqueles lacaios irritantes por um tempo.

Mas com a ***armadura descartável de lacaios*** em jogo, um Grande Mal sempre poderá ter sucesso a um custo em suas rolagens de defesa contra ataques, forçando um lacaio a ficar no caminho desse ataque. Esse lacaio não rola para se defender uma vez que simplesmente recebe o golpe que teria acertado o Grande Mal de qualquer modo. Isso força os PJs a desgastarem o exército do Grande Mal antes do confronto final.

Lembre-se: lacaios não precisam ser lacaios literais. Por exemplo, você pode escrever um ou mais “geradores de escudos”, cada um com uma trilha de estresse e talvez uma perícia para criar vantagens defensivas para blindar o Grande Mal\!

### Revelar a Forma Verdadeira

Ok, o grupo jogou tudo que eles tinham no Grande Mal, e - *incrível\!* - eles simplesmente o derrotaram\! Há apenas um problema: isso o libera de sua jaula de carne para revelar sua forma verdadeira\!

Com a ***revelação da sua forma verdadeira***, o seu Grande Mal não é apenas um personagem, são ao menos dois personagens que deverão ser derrotados sequencialmente, cada um revelando novas capacidades, façanhas, níveis maiores de perícias, novas caixas de estresse e consequências e até mesmo novos “quebra-regras”.

Se você quiser suavizar isso, mantenha as consequências que o Grande Mal já tinha sofrido entre as formas, dispensando as suaves e diminuindo as médias e graves em um passo cada.

### Aumentando a Escala

Você poderia ***aumentar a escala*** para permitir que seu Grande Mal opere em uma escala maior que a dos PJs usando a regra de escala vista [anteriormente](#escala). Você pode fazer isso mesmo que a escala não seja normalmente usada em sua campanha, essas regras só precisam ser aplicadas quando o Grande Mal entra em campo\!

### Bônus Solo

Os jogadores podem desfrutar de um bônus de trabalho em equipe, claro, mas por quê não dar ao seu Grande Mal um ***bônus solo*** complementar quando é o único a encarar os heróis?

Você poderia implementar esse bônus solo de algumas maneiras. Você pode usar mais de uma delas, mas tenha cuidado ao combiná-las, pois elas escalam rapidamente.

  - O Grande Mal recebe um **bônus para rolagens de perícias** igual ao     máximo do potencial de bônus de trabalho em equipe (mais detalhes [aqui](../desafios-disputas-conflitos/#trabalho-em-equipe) do     número de PJs agindo contra o Grande Mal menos um (então +2 contra     um grupo de três, etc). Assim como acontece com os PJs, este bônus     não pode ser maior do que o dobro da perícia do Grande Mal (ou     talvez você quebre essa regra *também*).
  - O Grande Mal poderá **reduzir o estresse** recebido de ataques bem     sucedidos pelo número de PJs oponentes dividido por dois,     arredondado para cima. Caso você ache que isso tornará a luta muito     demorada, então os golpes reduzidos desta forma não podem ser     reduzidos para menos que 1.
  - O Grande Mal tem **invocações amplificadas**: ao *pagar* pela invocação de     um aspecto, seu bônus é igual ao número de PJs que ele enfrenta.     Isso não acontece com invocações gratuitas, mas fará com que cada    ponto de destino gasto seja aterrorizante.
  - O Grande Mal pode **suprimir invocações**: quando enfrentando 2 ou mais     inimigos, as invocações da oposição fornecem apenas um bônus +1, ou    somente permitirão re-rolagens quando utilizadas diretamente contra    o Grande Mau. Opcionalmente, o Grande Mal também poderá remover a    capacidade dos PJs de acumular invocações gratuitas.

### A ameaça é um mapa (ou uma colméia de personagens)

Em Fate qualquer coisa pode ser um personagem, então por quê não um mapa? Quando ***a ameaça é um mapa***, seu Grande Mal tem zonas (mais detalhes [aqui](../desafios-disputas-conflitos/)) que deverão ser navegadas para alcançar a vitória.

Conforme você detalha o mapa do Grande Mal, cada zona poderá ter suas próprias perícias, aspectos e caixas de estresse. Algumas zonas poderão conter desafios simples que precisarão ser superados para se aproximarem da criatura. Cada zona poderá realizar uma ação como um personagem independente contra os PJs que estejam ocupando aquela zona ou, no caso da zona representar um membro do corpo ou similar, ser capaz de atacar zonas adjacentes também. Se uma zona é removida de ação pelo ataque de um dos PJs, ela poderá ser ignorada e não poderá realizar ações por conta própria, mas em geral o Grande Mal não é derrotado até que os heróis alcancem seu coração e o matem de verdade.

Este método funciona particularmente bem se o seu Grande Mal é um monstro verdadeiramente gigantesco, mas não precisa se limitar a essa situação. Você pode usar a ideia de tratar a ameaça como uma coleção de personagens interconectados, sem exigir que os PJs realmente entrem ou naveguem pelo chefão como um mapa literal. Usado dessa forma, você tem um híbrido entre um mapa e uma armadura de lacaios descartável (mais detalhes [aqui](#armadura-descartável-de-lacaios)): uma ***colmeia de personagens***, de certo modo. Algumas partes do Grande Mal devem ser derrotadas antes que os jogadores possam atingir onde ele é realmente vulnerável, e em troca essas partes podem realizar suas próprias ações.

Quer você se engaje totalmente na ideia do mapa ou simplesmente construa o Grande Mal como uma colméia, com certeza vai acabar com uma luta mais dinâmica, onde o Grande Mal age com mais frequência, e os jogadores devem descobrir um plano de ataque que elimine a ameaça, peça por peça, antes que eles possam finalmente abatê-la.

## Maneiras de Lidar com Múltiplos Alvos

Inevitavelmente alguém em sua mesa desejará afetar vários alvos de uma vez. Se for permitido, aqui estão alguns métodos que você pode usar.

Se você deseja ser seletivo com seus alvos, você pode ***dividir seu esforço***. Role sua perícia e se o resultado final for positivo você pode dividir esse total como quiser entre os alvos, dos quais podem cada um se defender contra o esforço que você designou a eles. Você deve atribuir pelo menos um ponto de esforço para um alvo ou você não o atingiu de forma alguma.

> Sofia enfrenta um trio de capangas e quer atacar todos os três em uma sequência de estocadas com seu florete. Graças a uma invocação e uma boa rolagem, o esforço do Lutar chegou a Épico (+7). Ela atribuiu um Bom (+3) para o primeiro que parece ser o veterano e Razoável (+2) para cada um dos outros dois, em um total de sete. Então cada um rola para se defender.

Em algumas circunstâncias especiais, como em uma explosão ou similar, você pode querer realizar um ***ataque de zona*** contra todo mundo em uma zona, seja amigo ou inimigo. Aqui, você não divide seu esforço, cada alvo precisará se defender contra sua rolagem total. As circunstâncias e o método devem ser adequados para se fazer isso, geralmente o narrador exigirá que você invoque um aspecto ou use uma façanha para obter permissão.

Se você deseja criar uma vantagem afetando uma zona inteira ou grupo, ***mire na cena*** ao invés disso: coloque um aspecto único na zona ou na própria cena ao invés de colocar aspectos separados em cada um dos alvos. Isso tem a vantagem adicional de reduzir a contabilidade de aspectos no geral. Se alguém insistir em criar um aspecto separado em cada alvo, ele deve se restringir ao método de divisão de esforço.

Em qualquer um dos métodos, todos os alvos devem ocupar a mesma zona. O narrador pode permitir exceções ocasionais baseado no método e/ou às circunstâncias.

Somente um tipo de ação deve ser usado, como atacar vários alvos de uma vez, resolver dois problemas de uma vez com superar ou influenciar a mente de alguns PdNs importantes para criar uma vantagem. O narrador pode permitir os dois tipos de ações diferentes em circunstâncias especiais, mas essas ações devem fazer sentido para a perícia usada por
ambas.

## Potência de Armas e Armaduras

Quer explorar um pouco mais da vibe de equipamentos de combate que outros jogos tem? Considere usar Potências de Armas e Armaduras. Resumindo, ser atingido por uma arma irá feri-lo mais e ter uma armadura prevenirá que isso aconteça (você poderia simular isso com façanhas, mas usar as caixas de façanha pode não parecer adequado para você).

Uma Potência de ***Arma*** é adicionada à tensão de um ataque bem sucedido. Se você tem Arma:2, significa que qualquer golpe inflige 2 tensões a mais que o normal. Isso conta para empates: você inflige estresse em um empate *em vez* de receber um impulso.

Uma Potência de ***Armadura*** reduz a tensão de um golpe bem sucedido. Assim, Armadura:2 faz com que qualquer golpe cause 2 tensões a menos que o usual. Se você acertar, mas a armadura do alvo reduz a tensão do ataque para 0 ou menos, você obtém um impulso para usar contra seu alvo, porém não causa dano algum.

Escolha cuidadosamente o nível desses valores. Fique de olho em como eles poderiam causar uma consequência (ou pior) em um empate. Recomendamos uma variação de 0 a 4 no máximo.

------

- [« Sendo o Narrador](../sendo-narrador/)
- [ Que Versão é Essa »](../versao/)
