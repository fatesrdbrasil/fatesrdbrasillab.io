---
title: 7 - Desafios, Disputas e Conflitos
layout: default
---

# 7 - Desafios, Disputas e Conflitos

## Foco na Ação

Na maioria das vezes uma rolagem simples de perícia será o suficiente para decidir como determinada situação será resolvida. Não há nível de detalhe ou tempo pré-determinados para as ações representadas por uma perícia. Logo, você pode usar uma única rolagem de Atletismo para saber se consegue escalar um rochedo que pode levar dias para ser desbravado ou usar a mesma rolagem simples para saber se consegue desviar de uma árvore caindo rapidamente em seu caminho, prestes a esmagá-lo.

Às vezes, porém, você estará em uma situação onde faz algo realmente dramático e interessante, como nas cenas marcantes de um livro ou filme. Quando isso acontece, é uma boa ideia focar na ação e lidar com ela usando várias rolagens de perícia, pois a variedade de resultados tornará as coisas realmente dinâmicas e surpreendentes. A maioria das cenas de luta se enquadra nisso, mas você pode focar em qualquer coisa que considerar suficientemente importante – perseguição de carros, processos judiciais, jogos de pôquer com apostas altas e por aí vai.

Há três formas de focar na ação em *Fate*:

-  Desafios, quando um ou mais personagens tentam realizar algo dinâmico ou complicado.
-  Disputas, quando dois ou mais personagens estão competindo pelo mesmo objetivo.
-  Conﬂitos, quando dois ou mais personagens estão tentando ferir uns aos outros.

- [« Vontade](../vontade/)
- [Desafios »](../desafios/)

