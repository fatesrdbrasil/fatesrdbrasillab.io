---
title: Contatos
layout: default
---

### Contatos

Contatos é a perícia de conhecer e criar conexões com pessoas. Ela pressupõe que você seja proficiente no uso de quaisquer meios de formar redes de contatos possíveis no cenário.

- `O`{: .fate_font} **Superar:** Use Contatos para superar qualquer obstáculo que impeça de encontrar alguém. Seja pela velha busca nas ruas, questionando sua rede de contatos ou procurando arquivos em bancos de dados, você é capaz de encontrar pessoas ou obter acesso a elas.
- `C`{: .fate_font} **Criar Vantagem:** Contatos permite saber quem é a pessoa certa para o que quer que você precise ou decidir que você já conhece tal pessoa. É provável que você use esta perícia para criar detalhes da história, representados através de aspectos ("Ei, galera, meu contato me informou que Joe Steel é ***O Melhor Mecânico de Toda Região***, vamos falar com ele.").

Você também pode criar vantagem para saber qual a opinião nas ruas sobre um indivíduo, objeto ou local específicos, baseado no que os seus contatos lhe dizem. Esses aspectos lidam mais com a reputação do que com os fatos, como ***Conhecido Por Sua Vilania*** ou ***Vigarista Notório***. Não há certeza se a pessoa faz jus à reputação que tem, embora isso não torne o aspecto inválido – as pessoas frequentemente ganham reputações enganosas que complicam suas vidas. Esta perícia também pode ser usada para criar aspectos que representem informações plantadas ou coletadas por você através de sua rede de contatos.

- `A`{: .fate_font} **Atacar:** Esta perícia não é usada para atacar; é difícil machucar alguém apenas por conhecer pessoas.
- `D`{: .fate_font} **Defender:** Contatos pode ser usada para se defender de pessoas que tentam criar vantagens sociais contra você, contanto que sua rede de contatos possa interferir na situação. Você também pode utilizá-la para evitar que alguém use Enganar ou Contatos para “sumir do mapa” ou para interferir no uso de Investigação em tentativas de rastrear seu personagem.

#### Façanhas Para Contatos

-  **Ouvido Atento:** Sempre que alguém iniciar um conﬂito contra você em uma área onde haja uma rede de contatos sua, você usa a perícia Contatos no lugar de Percepção para determinar a iniciativa, pois foi avisado a tempo.
-  **Língua Afiada:** +2 quando usar ação criar vantagem para espalhar rumores sobre alguém**.**
-  **Peso da Reputação:** Você pode usar Contatos ao invés de Provocar para criar vantagens baseadas no medo gerado pela reputação sinistra associada a você e àqueles que o acompanham. Deve existir um aspecto apropriado que esteja ligado a essa façanha.


- [« Conhecimentos](../conhecimentos/)
- [Empatia »](../empatia/)
