---
title: Tudo Pronto!
layout: default
---

## Tudo Pronto!

Ao final desse processo você deve ter um personagem com:

-  Um nome;
-  Cinco aspectos, além de um histórico;
-  Uma perícia Ótima, duas Boas, três Razoáveis e quatro Regulares;
-  Três a cinco façanhas;
-  2 a 4 caixas para cada tipo de estresse (Físico ou Mental);
-  Recarga de 1 a 3 pontos.

Agora você está pronto para jogar!

Narradores, vejam [*Cenas, Sessões e Cenário*](../cenas-sessoes-e-cenario) para conselhos sobre como usar os aspectos dos personagens e dos cenários e transformá-los em aventuras.

Jogadores, deem uma olhada no próximo capítulo para saber como usar melhor os aspectos ou pulem direto para *Ações e Resoluções* para saber mais sobre como usar suas perícias para realizar tarefas.

- [« Estresse e Consequências](../estresse-e-consequencias/)
- [Criação rápida de personagem  »](../criacao-rapida-de-personagem/)
