---
title: Criando Um Extra
layout: default
---

## Criando Um Extra

A criação de um extra começa com uma conversa. Isso deve acontecer durante a criação do jogo ou criação do personagem. O grupo precisa decidir o seguinte:

- Quais elementos de sua ambientação se enquadram melhor como um extra?
- O que você quer que o extra faça?
- Quais características do personagem você precisa para expressar melhor o extra?
- Quais são os custos ou as permissões para possuir um extra? Uma vez que tenha respondido a tais perguntas, procure por exemplos nesse livro que lhe ajudem a traçar os detalhes e criar algo dentro do que deseja. Pronto!

### Definindo os Detalhes

Vocês possivelmente já têm ideias formadas para os extras depois da elaboração do jogo; praticamente todos os jogos de fantasia possuem algum tipo de poder mágico, assim como um jogo com heróis tem superpoderes. Se as coisas girarem em torno de algum lugar de importância – como uma nave intergaláctica, uma base central ou uma taverna favorita – considere criar esse ambiente como um extra.

Extras naturalmente chamam bastante atenção quando entram em cena – os jogadores têm uma atração por opções legais e interessantes, então é normal que essas coisas recebam muita atenção. Quando estiver conversando sobre as possibilidades de extras, tenha certeza que está preparado para que esses elementos recebam muito do foco do jogo.

> O grupo conversa sobre extras em *Caroneiros de Asteroide*. 
>
> Os poderes de Bandu são uma primeira opção óbvia, assim como a lançadora de cascavéis de Esopo. Léo e Michel não têm interesse em listas compridas de poderes e armas. Além disso, como é um jogo onde existem poderes, eles concordam que haverá itens poderosos também.
>
> Pensando nas questões e locais, eles decidem que não tornarão nenhum deles em extras – eles viajarão com frequência, e os personagens não possuem uma participação ativa em alguma organização para criar algum extra relacionado a isso.

### O Que Os Extras Fazem

Em termos gerais, procure saber o que você quer que o extra faça, comparado ao que as perícias, façanhas e aspectos já podem fazer. Também, pense em como são "diante das câmeras". O que as pessoas veem quando você o usa? Qual é sua aparência e quais são as sensações passadas?

Considere estes pontos:

- O extra tem inﬂuência na historia? Como?
- O extra permite que você faça algo que nenhuma outra perícia permite?
- O extra torna as suas perícias mais úteis e poderosas?
- Como descreveria o uso desse extra?

Este é um passo importante porque pode revelar que o extra na verdade não contribui tanto quanto imaginado, o que lhe ajuda a adicionar mais detalhes ou desconsiderá-lo.

> Para os poderes de Bandu, o grupo decide que querem manter as coisas bem discretas e vagas – é apenas mais uma forma de resolver problemas, como a propensão de brigar de Esopo ou a habilidade de Fräk com chicotes (que Maira prefere manter apenas na técnica) – um psiônico treinado deve ser tão temido quanto um atirador profissional, não mais.
>
> Eles concordam que isso pode inﬂuenciar a história de várias maneiras; imaginam lugares repletos de efeitos desconhecidos e enredos perfeitos para Bandu, assim como o desejo do Império de tomar posse desses conhecimentos.
>
> Eles decidem então que os poderes de Bandu permitirão que alguém interaja com o sobrenatural de uma forma que ninguém mais consegue fazer, e isso pode afetar e prejudicar as pessoas, mas novamente enfatizam que esses poderes não serão mais poderosos que as outras perícias. Efeitos básicos irão usar as quatro ações normais, e poderes mentais usarão desafios, disputas ou conﬂitos conforme apropriado.
>
> O grupo descarta a presença de poderes titânicos, capazes de alterar o universo ou a realidade, incendiando cidades e planetas, assim por diante. Se essas coisas existirem, serão o foco de algum cenário e fruto dos esforços de muitas pessoas sacrificando coisas muito importantes. O grupo não vê esses poderes mentais inﬂuenciando outras perícias diretamente, o que ajuda a manter sua natureza exclusiva. Michel imagina limitações e requisitos imprevisíveis – algumas coisas ele pode conseguir facilmente, outras não, e tudo depende do drama do momento para determinar quando e como. O grupo gosta da ideia e todos concordam.

### Atribuindo Elementos ao Personagem

Depois de ter a ideia geral, descubra que características do personagem você precisa para fazer o extra.

- Se o extra inﬂuencia a história, então deve usar aspectos.
- Se o extra cria um novo contexto para a ação, então ele deve usar perícias.
- Se o extra permite realizar algo extraordinário com uma perícia, então deve usar façanhas.
- Se o extra pode sofrer dano ou ser consumido de alguma forma, então deve poder receber estresse e consequências.

Um extra pode usar um aspecto como uma permissão – exigindo um determinado aspecto de personagem para acessar outras habilidades do aspecto. Seu personagem pode precisar ter nascido com algum traço ou obter certo status para fazer uso do aspecto. Ou o extra pode dar um novo aspecto ao qual o personagem tem acesso, se é o próprio extra que importa
na história.

Extras podem usar perícias de vários modos. O extra pode ser uma nova perícia que não pertença a lista padrão; pode modificar uma perícia, acrescentando funções às suas quatro ações; pode custar um espaço dedicado a uma perícia durante a criação de personagem ou durante sua evolução. É possível que um extra inclua uma ou mais perícias existentes que o jogador pode acessar enquanto o controla.

Escrever um extra como uma façanha funciona da mesma forma que criar uma façanha. Um extra pode ter algumas façanhas ligadas a ele – pode até mesmo incluir as perícias que tais façanhas modificam. Extras que incluem façanhas costumam custar pontos de Recarga, como façanhas.

Um extra que descreve alguma habilidade inerente ao personagem pode conceder-lhe mais uma linha de estresse – além de físico e mental. Um extra que represente uma entidade separada do personagem, como um local ou veículo, pode possuir suas próprias linhas de estresse físico. Você também pode escolher uma perícia que inﬂuencie essa nova linha de estresse – assim como Vigor provê caixas extras de estresse físico e consequências.

Sabendo melhor o que o extra faz, será mais fácil escolher os elementos do personagem que reforçam a ideia e determinar como serão usados.

> Para os poderes de Bandu, o grupo decide que usará aspectos e perícias - há uma inﬂuência direta na história, e seus poderes criam um novo jeito de lidar com problemas. Ele não quer que isso afete outras perícias e sim que seja um fator isolado, então acham melhor que não seja uma façanha. Eles também não imaginam algum tipo de “mana” ou outros recursos associados, então não vão usar estresse e consequências.

### Permissões e Custos

Uma **permissão** é uma justificativa narrativa que permite a você criar um extra. Na maioria das vezes, você estabelece uma permissão ao criar um extra que esteja relacionado aos aspectos de seu personagem, descrevendo o que o torna qualificado a receber o extra. Você também pode simplesmente concordar que faz sentido que aquela pessoa tenha aquele extra e pronto.

Um **custo** é como você paga pelo extra com os recursos disponíveis na sua ficha de personagem, podendo ser um ponto de perícia, um ponto de recarga, um espaço de façanha ou espaço de aspecto.

Felizmente, como os extras usam elementos do personagem que já são familiares a você, as coisas ficam mais simples – você vai pagar o que normalmente pagaria durante a criação de personagem. Se o extra é uma nova perícia, então coloque-o na pirâmide de perícias normalmente. Se for um aspecto, escolha um de seus cinco aspectos para sê-lo. Se for uma façanha, pague um ponto de recarga (ou mais) e coloque-o na sua ficha.

Narradores que não quiserem que os jogadores escolham entre possuir um extra e possuir as características normais de um personagem inicial têm a liberdade de aumentar a quantidade de cada uma das características disponíveis para que possam acomodar melhor os extras – apenas esteja certo de que cada PJ receba a mesma quantidade de espaços adicionais.

> Amanda sugere que Bandu possua um aspecto com algum efeito no jogo como permissão para o extra. Ele sugere ser colocado na mira de um grupo, o Império. Para poupar esforços, ela decide que a perícia a ser usada será Conhecimentos, logo não precisa de uma perícia a mais na pirâmide. Ela sugere que outros personagens que possuam tal perícia e algum aspecto relacionado podem manifestar poderes. Michel gostou, porque é simples e equilibrado, e concorda.

### Escrevendo

Uma vez que tenha todos esses elementos estabelecidos, você pode escrever seu extra. Parabéns!

------

### Extra: Poderes Mentais

+ **Permissões:** Um aspecto relacionado que seja reﬂetido no jogo.
+ **Custo:** Um nível de perícia, especificamente para a perícia Conhecimentos (normalmente, você também gastaria pontos de recarga por estar adicionando novas ações a uma perícia, mas Amanda e seu grupo consentem que isso não será necessário). 

Pessoas com poderes mentais são capazes de usar seu conhecimento para criar efeitos sobrenaturais, adicionando as seguintes ações à perícia Conhecimentos:

+ `O`{: .fate_font} **Superar:** Use Conhecimentos para superar desafios mentais e disputas psicológicas, ou para questionar alguém.
+ `C`{: .fate_font} **Criar Vantagem:** Use Conhecimentos para impor condições mentais ao seu alvo, como Lentidão ou Atordoado. O personagem pode usar Vontade para se defender.
+ `A`{: .fate_font} **Ataque:** Use Conhecimentos para ferir diretamente alguém causando dano por agressão mental. O alvo pode se defender usando Vontade, ou Conhecimentos se possuir treinamento.
+ `D`{: .fate_font} **Defender:** Use Conhecimentos para se defender de ataques mentais e dano psicológico.

------

## Extra e Evolução


Os extras evoluem assim como qualquer característica do personagem, de acordo com os marcos em *Criando Campanhas.* Para isso, podemos seguir algumas orientações:

- Um extra em forma de aspecto pode ser alterado em um marco menor ou em um marco maior se estiver ligado a seu conceito.
- Um extra em forma de perícia pode ser alterado a qualquer marco significativo ou maior, contanto que isso seja possível de acordo com a pirâmide, e você também pode adicionar novos extras nesses marcos. Também é possível trocar o nível do seu extra com outra perícia em um marco menor, como poderia fazer com duas perícias normais.
- Um extra em forma de façanha pode avançar em um marco maior ao ganhar um ponto de recarga. Isso pode significar a adição de novos efeitos a um extra existente ou a compra de um novo extra em forma de façanha. Você também pode alterar este tipo de extra em um marco menor como qualquer outra façanha.

Claro que muitos extras usam mais de um elemento. Recomendamos que você permita que os jogadores desenvolvam partes diferentes do extra em marcos separados, para minimizar a confusão durante o jogo.

- [« Extras](../extras/)
- [Mais Exemplos De Extras »](../mais-exemplos-de-extras/)

