---
title: Fate Condensado - Sendo o Narrador
layout: default
---
# Sendo o Narrador

Como narrador, você é o diretor das sessões de jogo. Note que você não é o chefe. ***Fate Condensado*** é colaborativo e os jogadores têm opinião com o que acontece com seus personagens. Seu trabalho é manter o jogo em movimento agindo desse modo:

  - **Dirija as cenas:** Uma sessão é composta por cenas. Decida onde a     cena começa, quem está nela e o que está acontecendo. Decida quando     todas as coisas interessantes já aconteceram e quando a cena chega     ao fim. Ignore as coisas desnecessárias; da mesma forma que você não     rola os dados se o resultado de uma ação não for interessante, não     crie uma cena caso nada excitante, dramático, útil ou divertido     aconteça nela.
  - **Julgue as regras:** Quando surgir alguma dúvida sobre como aplicar     as regras, você pode discutir com os jogadores e tentar chegar a um     consenso aceitável, mas você tem a palavra final como narrador.
  - **Defina a Dificuldade:** Decida quando as rolagens serão    necessárias e defina suas dificuldades.
  - **Determine os custos da falha:** Quando um personagem falha em sua    rolagem, decida qual será o custo do sucesso a um custo. Você    certamente pode aceitar sugestões dos jogadores - eles podem até    dizer o que gostariam que acontecesse ao seus personagens, mas é    você que tem a palavra final.
  - **Jogue com os PdNs:** Cada jogador controla o seu próprio    personagem, mas você controla todo o resto do jogo, desde cultistas    a monstros e até o próprio Grande Mal.
  - **Dê aos PJs oportunidades de ação:** Se os jogadores não sabem o    que fazer a seguir, é seu trabalho dar-lhes um empurrãozinho. Nunca    deixe as coisas ficarem muito paradas por indecisão ou falta de    informação, faça algo para agitar as coisas. Em caso de dúvida,    pense nas táticas e objetivos do Grande Mal para criar um leve    incômodo para os heróis.
  - **Faça com que todos tenham seu momento de brilho:** Seu objetivo    não é derrotar os jogadores, mas desafiá-los. Certifique-se de que    cada PJ tenha a chance de ser a estrela de vez em quando. Force    Aspectos e crie desafios sob medida para as diferentes perícias e    fraquezas dos personagens.
  - **Complique a vida dos PJ:** além de jogar monstros nos personagens,     você será a fonte primária de forçar aspectos. Os jogadores podem    forçar aspectos de si mesmos ou de outros personagens, é claro, mas    você deve garantir que todos tenham a oportunidade de experienciar    as repercussões negativas de seus aspectos.
  - **Construir sobre as escolhas dos jogadores:** observe as ações que    os PJ tomaram durante a partida e pense como o mundo muda e responde    a elas. Faça que sintam o mundo como algo vivo apresentando essas    consequências - boas ou ruins - de suas ações no jogo.

## Definindo Dificuldade e Oposição

Às vezes, a ação de um PJ enfrentará ***oposição*** por meio de uma rolagem de defesa de outro personagem na cena. Nesse caso, o personagem oponente rola os dados e adiciona seus níveis de perícia relevantes, assim como um PJ. Se o personagem oponente tiver aspectos relevantes, eles podem ser invocados; o narrador pode invocar os aspectos dos PdN usando os pontos de destino de sua reserva (mais detalhes [aqui](#seus-pontos-de-destino)).

Mas, se não houver oposição, você deve decidir a ***dificuldade*** da ação:

  - **Dificuldades baixas**, abaixo do valor da perícia relevante do PJ, são    melhores quando você quer dar a eles uma chance de se exibirem.
  - **Dificuldades moderadas**, próximas do valor da perícia relevante do    PJ, são melhores quando você quer gerar tensão, mas não    sobrecarregá-los.
  - **Dificuldades altas**, muito maiores do que o nível da perícia    relevante do PJ, são melhores quando você quer enfatizar quão    ameaçador ou incomum são as circunstâncias e fazê-los utilizar todo    o seu potencial, ou colocá-los em uma posição onde sofrerão as    consequências de uma falha.

Da mesma forma, use a escala de adjetivos (mais detalhes [aqui](../comecando/#a-escala-de-adjetivos)) de valores para ajudá-lo a escolher uma dificuldade apropriada. É algo excepcionalmente difícil? Então escolha Excepcional (+5)\! Aqui estão algumas regras básicas para você começar:

Se a tarefa não for muito difícil, torne-a Medíocre (+0), ou apenas diga ao jogador que ele teve sucesso sem uma rolagem, desde que não haja muita pressão de tempo ou o personagem tem algum aspecto que sugere que ele seria bom nisso.

Se você puder pensar em pelo menos um motivo do porquê a tarefa é difícil, escolha Razoável (+2); para cada fator extra trabalhando contra eles, adicione outro +2 à dificuldade.

Ao pensar sobre esses fatores, consulte quais aspectos estão em jogo. Quando algo é importante o suficiente para se tornar um aspecto, ele deveria ganhar alguma atenção aqui. Uma vez que aspectos são verdades (mais detalhes [aqui](../aspectos-pontos-destino/#aspectos-são-sempre-verdades)), eles poderão influenciar o quanto algo deve ser fácil ou difícil de ser realizado. Isso não significa que os aspectos são os únicos fatores a serem considerados\! Escuridão é escuridão independentemente de você decidir ou não torná-la um aspecto de cena. 

Se a tarefa é incrivelmente difícil, vá o mais alto quanto você achar que faz sentido. O PJ precisará gastar alguns pontos de destino e obter muita ajuda para ser bem sucedido, mas tudo bem.

Para uma visão mais ampla do que você pode fazer para criar oposições e adversários variados e interessantes para seus jogadores, verifique o Kit de Adversários de Fate, disponível para venda em PDF ou as regras essenciais de forma gratuita no sistema de referência de regras (em inglês) em <https://fate-srd.com/>

## Personagens do Narrador (PdN)

Os PdN incluem espectadores, elenco de apoio, aliados, inimigos, monstros e praticamente qualquer coisa que possa complicar ou se opor aos esforços dos PJ. Você provavelmente criará outros personagens para que os PJ interajam com eles.

### PdN Principais

Se alguém é particularmente importante para a história, você pode criá-lo como se fosse um PJ. Isso é apropriado para alguém com quem os PJ irão lidar muito, como um aliado, um rival, o representante de um grupo poderoso ou um Grande Mal.

Um PdN importante não segue necessariamente os mesmos limites que um PJ iniciante. Se o PdN se tornar uma ameaça recorrente como um chefe, dê a ele uma perícia com nível máximo (veja Definindo Dificuldade e Oposição, [aqui](#definindo-dificuldade-e-oposição)), mais façanhas e tudo que seja necessário para torná-lo perigoso.

### PdN Menores

Os PdN que não são personagens significativos ou recorrentes não precisam ser tão bem definidos como PdN principais. Para um PdN menor, defina apenas o que for absolutamente necessário. A maioria dos PdN menores terão um único aspecto, que é exatamente o que eles são: _cão de guarda, burocrata obstrutivo, cultista enfurecido_, etc. Se  necessário, dê-lhes um ou dois aspectos que refletem algo interessante sobre eles ou uma fraqueza. Eles também podem ter uma façanha.

Dê a eles uma ou duas perícias que descrevam no que eles são bons. Você pode usar perícias da lista de perícias ou inventar algo mais específico, como Razoável (+2) em Arranjar Brigas de Bar ou Ótimo (+4) em Morder Pessoas.

Dê a eles de zero a três caixas de estresse, quanto mais eles tem, mais ameaçador eles serão. Geralmente eles não têm caixas de consequências, se eles forem atingidos com mais tensões do que podem absorver com seu estresse, eles simplesmente são tirados de ação. Os PdN secundários não são feitos para durar.

### Monstros, Grandes Males e Outras Ameaças

Assim como PdN menores, os monstros e outras ameaças (como uma tempestade, um fogo que se alastra ou um esquadrão de lacaios blindados) são escritos como personagens, mas geralmente são mais simples que um PJ. Você só precisa definir o que é absolutamente necessário. Diferente de PdN menores, essas ameaças podem ser definidas de qualquer forma. Quebre as regras (mais detalhes [aqui](../regras-opcionais/#maneiras-de-quebrar-as-regras-para-o-grande-mal)), dê a eles qualquer combinação de aspectos, perícias, façanhas, estresses e consequências necessárias para torná-los perigosos e pense sobre que tipo de dificuldades eles apresentarão aos PJs quando determinar suas pontuações.

## Seus pontos de destino

No início de cada cena, comece com uma reserva de pontos de destino igual ao número de PJs. Se a cena inclui um PdN principal ou um monstro que concedeu (mais detalhes [aqui](../desafios-disputas-conflitos/#concedendo)) um conflito anterior ou recebeu invocações hostis (mais detalhes [aqui](../aspectos-pontos-destino/#invocações-hostis)) em uma cena anterior, esses pontos de destino são adicionados à sua reserva. Se um aspecto foi forçado contra você na cena anterior, não dando a oportunidade de gastar esse ponto de destino ganho, você pode adicioná-lo à sua reserva também.

> Carlos, Raquel, Carolina e Edson se dirigem para o confronto final contra Alice VillaBoas que anteriormente havia escapado dos heróis após conceder em um conflito depois de ter sofrido uma consequência moderada. Isso significa que o narrador tem quatro pontos de destino pelos quatro jogadores e dois a mais que Alice está trazendo.

Como narrador, você pode gastar os pontos de destino dessa reserva para invocar aspectos, recusar forçar aspectos que os jogadores ofereçam aos PdNs e utilizar qualquer façanha que exija um ponto, tudo exatamente como os jogadores fazem.

**Entretanto, você _não precisa_ gastar ponto de destinos para forçar aspectos.** Você tem um estoque infinito de pontos de destino para esse propósito. 

## Ferramentas de Segurança

Narradores (e na verdade, os jogadores também) têm a responsabilidade de garantir que todos na mesa se sintam seguros no jogo e no espaço que estão jogando. Uma maneira do narrador apoiar isto é oferecendo uma estrutura em que qualquer um da mesa possa expressar preocupação ou objeção. Quando isso acontecer, deve ter prioridade e deve ser resolvido. Aqui estão algumas ferramentas que podem ajudar a tornar esse processo mais acessível para os jogadores na mesa e mais fácil de ser aplicado quando necessário.

  - **Carta X:** a carta X é uma ferramenta opcional (criada por John     Stavropoulos) que permite que qualquer pessoa em seu jogo (incluindo     você) mude qualquer conteúdo que faça alguém se sentir     desconfortável durante o jogo. Você pode aprender mais sobre a     carta X [aqui][xcard]
  - **Caixas de Ferramentas de Mudança de Roteiro para RPG:** para algo com     um pouco mais de nuances e granularidade, busque por Script Change     de Brie Beau Sheldon, que oferece opções para pausar, retroceder,     avançar e muito mais, lembrando a familiar metáfora de um tocador de    mídia. Veja mais sobre o Script Change em *<http://tinyurl.com/nphed7m>*.

Ferramentas como essas também podem ser usadas como a regra do “Fala Sério” (mais detalhes [aqui](../agindo-rolando-dados/#invocando-aspectos)) para calibração. Elas oferecem uma maneira para os jogadores debaterem confortavelmente sobre o que eles procuram no jogo. Dê a essas ferramentas o respeito e o suporte que elas merecem\!



------

- [« Avanço](../avanco/)
- [ Regras Opcionais »](../regras-opcionais/)


[xcard]: {% post_url 2021-05-24-CartaX %}
