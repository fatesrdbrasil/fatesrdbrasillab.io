---
title: Cibernética
layout: default
---


## Cibernética

A forma mais fácil de lidar com implantes cibernéticos em Fate é com as ferramentas que já existem. Quer ser um ciborgue? Coloque um aspecto ou dois representando o fato de você possuir uma ou duas melhorias. Seus braços mecânicos lhe dão força sobre-humana? Coloque sua perícia Vigor ou Lutar como a mais alta. Seus olhos possuem mapeamento térmico? Uma façanha pode ajudar com isso.
 
Esta seção é para quem estiver em busca de um sistema para implantes cibernéticos que se mantenha por si só ao invés de modificar sistemas já existentes ou quem quiser uma justificativa para as coisas que já colocaria, é para isso que essa sessão foi criada.

### Próteses vs Melhorias


Implantes cibernéticos trazem um custo para a pessoa. Muitos indivíduos possuem próteses cibernéticas ocasionais, um membro simples para imitar sua versão humana, anexado para compensar uma perda ou ferimento. Todas essas novas peças custarão algum dinheiro e tempo em uma clínica de reabilitação, deixando-o novo em folha — praticamente. Se tudo o que quer for uma ou duas próteses, não é preciso pagar a recarga nem possuir qualquer aspecto ligado às próteses, a não ser que queira isso.

Melhorias são totalmente diferentes. Enquanto uma prótese é desenvolvida para fazer parte do corpo, imitando funções naturais da melhor forma possível, uma melhoria é instalada para _aperfeiçoar_ o corpo humano, tornar uma pessoa mais forte, rápida, durável, com armas naturais ou capaz de coisas que os humanos não conseguem por si só. Enquanto há pessoas que obtêm próteses para repor uma parte perdida e voltar à sua forma completa, muitas outras removem partes do próprio corpo para para instalar melhorias. 

Essas melhorias precisam de uma fonte de energia, algo além da bioenergia que o corpo humano produz. Para esse fim, as pessoas que recebem sua primeira melhoria ganham um coração cibernético. Não é literalmente um coração. Ele não substitui seu coração humano a menos que seja necessário, mas funciona como um bom reserva se possuir um verdadeiro que funcione. Um coração cibernético é, antes de mais nada, uma fonte de energia. É um capacitor que se recarrega com as próprias fontes de bioenergia do seu corpo, que pode ser plugado a uma tomada como complemento, e permite que você alimente uma ou mais melhorias.

Isso significa duas coisas em jogo. Primeiro, é obrigatório dedicar ao menos um aspecto ao fato de ser um ciborgue. Você voluntariamente transcendeu suas limitações humanas — uma característica que define qualquer pessoa que tenha escolhido esse caminho. O segundo custo mecânico é reduzir a recarga em 1, em troca da seguinte melhoria.

__Coração Cibernético:__ Você pode instalar e usar outras melhorias cibernéticas. O coração também atua como um suporte para seu coração natural e filtra toxinas e impurezas de seu sangue. Se você for submetido a veneno ou toxina, gaste um ponto de destino para ignorá-los. Se você for morto por algo que poderia ter sido evitado pelo coração cibernético, gaste um ponto de destino conceder a luta ao invés de ser tirado de ação.

### Tipos de Melhorias

Há dois tipos de melhorias: menores e maiores. Obtê-las custa dinheiro, mas também tempo, além de poder custar recarga também.

**Melhorias menores** são pequenas mudanças, coisas que não exigem muito do corpo ou que não exigem grandes alterações no seu sistema biológico. Um novo olho, uma mão, implante de pele ou o aperfeiçoamento de uma melhoria existente — estas coisas são melhorias menores. Se for comprar e instalar uma melhoria menor durante o jogo, você precisará de uma rolagem de Recursos com uma dificuldade Regular (+1) para cada melhoria adicional que estiver instalando. Também precisará se recuperar da cirurgia. Você recebe uma consequência moderada que se recupera normalmente. Independente se você escolher as melhorias menores durante o jogo ou durante a criação do personagem, para cada três delas que pegar, pagará 1 ponto de recarga. Ao receber sua primeira melhoria menor, pague o ponto de recarga. Ao receber sua quarta melhoria menor, pague novamente e assim por diante. Instalar uma prótese exige o mesmo custo em Recursos e consequências, mas não exige
o custo de recarga.

**Melhorias maiores** requerem cirurgias grandes e substituição de uma parte grande do corpo. Membros são sempre melhorias maiores, assim como órgãos e qualquer coisa que se conecte ao seu cérebro. Ao fazer uma melhoria maior durante o jogo, faça uma rolagem de Recursos contra dificuldade Bom (+3) para _cada_ melhoria maior que fizer. Além disso, fazer uma cirurgia desse porte impõe uma consequência severa, que se recupera normalmente. Seja durante a criação do personagem ou durante o jogo, _cada_ melhoria maior custa um ponto de recarga.

### Exemplos de Melhorias

**Olho Cibernético (menor):** Você recebe um bônus de +1 em rolagens da perícia Notar relacionadas à visão. Além disso, escolha um dos seguintes aspectos. Escolher um aspecto adicional é considerado como outra melhoria menor.

**_Filtro de  Imagem ;  Visão Em Pouca Luz  ; Visão de  Sonar ; Interface de  Mira ; Imagem Térmica ; Interface Visual de  Rede ; Ampliação de Imagem._**

**Pernas Cibernéticas (maior):** Suas pernas foram substituídas por pernas cibernéticas muito mais poderosas. Você pode se mover duas zonas como uma ação livre e recebe +2 nas rolagens de Atletismo para correr ou pular. Além disso, escolha umadas extensões a seguir. Extensões adicionais são consideradas melhorias menores.
+ **Compartimento Secreto:** Você possui um compartimento onde pode esconder algo como uma arma de mão ou um tablete de cocaína.
+ **Aderência Magnética:** Se estiver de pé sobre uma superfície metálica, você não pode ser derrubado. Você também pode caminhar em superfícies metálicas íngremes ou mesmo verticais, embora desajeitadamente.
+ **Chute Pneumático:** Se chutar alguém, seu golpe é considerado uma Arma: 2.

**Interface Neural (maior):** Você pode acessar a Rede de qualquer lugar, com um pensamento. Você pode invadir sistemas de baixa segurança automaticamente — eles simplesmente fazem o que você quiser. Mesmo sistemas de alta segurança são fáceis — você recebe um bônus de +2 nas rolagens de Computadores para invadir.

**Unhas Afiadas (menor):** Você tem três centímetros de lâminas afiadas que saem da ponta de seus dedos; podem ser recolhidas quando quiser. São consideradas Arma: 1.

**Blindagem Subdérmica (maior):** Você pode conter a maioria dos ataques físicos usando a perícia Vigor — socos, lâminas, cassetetes e armas de pequeno porte têm dificuldade em ultrapassar as placas de metal sob de sua pele. Além disso, uma vez por cena você pode ignorar uma consequência física menor ou moderada vinda de um ataque desse tipo. 

**Camuflagem Térmica e Óptica (menor):** Você pode gastar um ponto de destino para se tornar invisível aos espectros visuais e térmicos, contanto que não se mova.
 
### A Desvantagem das Melhorias

Embora não mencionado acima, cada melhoria tem suas desvantagens. Uma interface neural pode ser invadida, dando acesso ao seu cérebro. Camuflagem térmica pode entrar em curto circuito debaixo d’água, causando um choque elétrico violento. É por isso que você possui um aspecto ligado ao fato de ser um ciborgue.

Narradores, sintam-se livres para fazer cumprir a desvantagem de uma melhoria — qualquer que seja a decisão do grupo. Isso é uma ação de forçar o aspecto do ciborgue, o que significa que o jogador colocado em tal situação pode recusar a ação de forçar ou receber um ponto de destino pelo problema no qual você o colocou.

------

- [« Kung Fu](../kung-fu/)
- [Dispositivos e Ferramentas »](../dispositivos-e-ferramentas/)
