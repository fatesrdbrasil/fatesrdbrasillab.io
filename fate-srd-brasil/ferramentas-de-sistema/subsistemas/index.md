---
title: Subsistemas
layout: default
---


# Capítulo 9 - Subsistemas

- [Kung Fu](../kung-fu/)
- [Cibernética](../cibernetica/)
- [Dispositivos e Ferramentas](../dispositivos-e-ferramentas/)
- [Monstros](../monstros/)
- [Equipe](../equipes/)
- [Isto Significa Guerra: Combate Em Massa](../isto-significa-guerra-combate-em-massa/)
- [Duelos Aventurescos](../duelos-aventurescos/)
- [Veículos](../veiculos/)
- [Supers](../supers/)
- [O Paradoxo do Horror](../paradoxo-do-horror/)

------

- [« Fragmentos de Poder](../fragmentos-de-poder/)
- [Kung Fu »](../kung-fu/)
