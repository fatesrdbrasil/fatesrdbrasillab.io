---
title: Aspectos De Equipamento
layout: default
---

## Aspectos De Equipamento


Equipamentos ficam em segundo plano no Fate. Suas perícias ganham foco no que diz respeito a mostrar o que você pode fazer; o equipamento está lá para permitir que você as use. Você possui um alto nível de Atirar? Então tem uma arma! Priorizou Condução? Então possui um carro! Há coisas que podem ser assumidas e que não possuem efeitos mecânicos. O capítulo de Extras no Fate Sistema Básico explica como adicionar mecânica de jogo a equipamentos, coisas como potência de armas ou seus próprios aspectos ou perícias. No entanto, há um meio termo que não requer redução de recarga e permite que todos os equipamentos importantes tenham importância significativa.

Se quiser seguir por esse caminho, mantendo equipamentos simples e em segundo plano, mas aumentando seu peso em jogo, pode usar aspectos de equipamento. Neste método, a maior parte dos equipamentos se comporta como apresentado no padrão Fate – ele permite o uso de perícias e justifica as ações. No entanto, se o equipamento tem o potencial para ser importante em
algum ponto da história, ele se torna um aspecto.

Um aspecto de equipamento pode ser tão genérico ou descritivo quanto desejar. Se para você armas são importantes, mas intercambiáveis, talvez tenha apenas um ___Revólver___ ou ___Rifle de Precisão___. Se desejar ser um pouco mais específico, talvez algo como ___Colt .45 Em Perfeitas Condições___ ou um ___XM Silencioso___. Quer ser mais profundo? Crie algo como ___Revólver de Serviço de Meu Pai___ ou ___Meu XM21 Bem Utilizado e Modificado___. A questão é a seguinte, se for importante, se torna um aspecto. Coisas como sua jaqueta ou  sapatos, óculos de sol, chaves do carro — talvez até carro — não precisam de aspectos, a menos que isso se torne importante na história.

Um aspecto de equipamento funciona como qualquer outro aspecto no jogo: você pode invocá-lo e outros podem forçá-lo — ou invocá-lo — contra você. É possível invocar um aspecto de equipamento a qualquer momento em que isso possa ser útil: invoque seu ___Jóias Importadas___ quando quiser chamar atenção e impressionar alguém ou invoque ___Passe de Imprensa___ para ter acesso à cena do crime.
 
Para Narradores, a regra final no que diz respeito a aspectos de equipamento é: podem ser removidos. Se o PJ está começando a depender demais de um aspecto de equipamento ou se achar que está na hora de agitar um pouco mais as coisas, encontre uma razão para fazer esse aspecto desaparecer. Fazer isso é um ato de forçar, então o jogador pode recusar — e tudo bem se isso acontecer —, afinal você não quer tirar do jogo a parte favorita dos jogadores. Além disso, a retirada do aspecto não significa necessariamente que o jogador perde aquilo permanentemente. Um ___Passe de Imprensa___ pode desaparecer quando o personagem é suspenso e um ___Revólver___ pode não ser útil quando a munição acabar. Em ambos os casos, há uma forma narrativa para ganhar o aspecto de volta e às vezes isso pode impulsionar uma nova aventura!

---

#### MUITOS ASPECTOS?

Se cada PJ possui cinco aspectos e talvez quatro ou cinco aspectos de equipamento, além disso há os aspectos de cena e consequências, entre outros.. Será que não são aspectos demais? Pode ser que sim! Eis um segredo: aspectos de equipamento são basicamente aspectos de situação portáteis. Dessa forma, podem fazer uma grande parte do trabalho pesado que os aspectos de situação realizam, se sentir que há muita coisa acontecendo ao mesmo tempo numa cena. Não é uma boa ideia eliminar os aspectos de situação completamente, mas se cada PJ possuir um certo número de aspectos de equipamento, comece com menos aspectos de situação. 

Também é uma boa ideia limitar o número de aspectos de equipamento que cada PJ possui. Nem todo equipamento precisa de
aspectos, então limitar os PJs a apenas três, dois ou mesmo um é perfeitamente razoável.

Por último, em um jogo focado em equipamento ao invés do relacionamento entre os PJs ou aventuras anteriores, aspectos de equipamento podem substituir os [aspectos das “Três Fases”](../../fate-basico/tres-fases).


------

- [« Aspectos de Gênero](../aspectos-de-genero/)
- [Condições »](../condicoes/)
