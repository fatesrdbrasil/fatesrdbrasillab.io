---
title: Façanhas e Recarga
layout: default
---

## Estresse e Consequências

**Determine quanto sofrimento seu personagem aguenta.**

Quando os personagens se encontram em perigo – algo bastante comum quando você é altamente competente, ativo e enfrenta os seus dramas – há duas formas de se manterem firmes: **Estresse e Consequências**.

O capítulo [*Desafios, Disputas e Conﬂitos*](../desafios-disputas-e-conflitos/) explica o que significam e como usá-los. Em resumo, estresse representa o quão bem você está, enquanto as consequências são os efeitos prolongados e até mesmo traumáticos por levar algum tipo de dano.

Cada PJ tem duas **barras de estresse** diferentes. A **barra de estresse físico** lida com o dano físico enquanto a **barra de estresse mental** lida com todo o dano mental e psicológico. Quanto mais caixas o personagem possuir, mais resistente ele será. Por padrão, cada personagem possui duas caixas em cada uma das barras.

Cada personagem também possui três campos para **consequências**. Uma é *suave*, outra *moderada* e a última *severa*. Diferentes do estresse, elas não são classificadas como físicas ou mentais e elas se aplicam a qualquer coisa. Como mencionado acima, consequências são danos e traumas que você não pode simplesmente apagar ou esquecer.

Certas perícias e algumas façanhas podem melhorar suas chances. Veja [*Perícias e Façanhas*](../pericias-e-facanhas/) para saber mais. Como referência rápida, estas são as perícias em *Caroneiros de Asteroide* que alteram o estresse e as consequências:

**Vigor** ajuda com o estresse físico, **Vontade** ajuda com o estresse mental. Cada uma dessas perícias concede uma caixa a mais de estresse do mesmo tipo (físico ou mental) se ela possui um nível Regular (+1) ou Razoável (+2) e duas caixas se possuir um nível Bom (+3) ou mais. Com Excepcional (+5) ou mais o personagem também ganha mais um campo de consequência suave. Essa consequência, porém, se restringe a danos físicos (Vigor) ou mentais (Vontade), dependendo da perícia.

**Nota:** Se estiver jogando em um cenário com um conjunto de perícias diferentes, as perícias que afetam as caixas de estresse podem ser diferentes. Faça anotações desses benefícios de perícias quando criar o seu personagem.

------

É possível criar novas barras de estresse se no seu jogo os personagens puderem sofrer tipos de dano específicos, como [estresse de riqueza](../mais-exemplos-de-extras/#riqueza) em um jogo com foco político. Alterar o número de caixas de estresse prolongará os conﬂitos, o que pode ser mais apropriado para jogos de ação, pulp, entre outros, onde se espera que os personagens sejam resistentes.

------

> Esopo tem Vigor Bom (+3), o que lhe concede mais duas caixas na barra de estresse. Sua Vontade, no entanto, é apenas Regular (+1), mas é o suficiente para que adquira uma caixa a mais de estresse mental.
>
> Fräkeline tem Vigor Razoável (+2), assim ela ganha uma terceira caixa de estresse físico. Mas seu estresse mental continua o mesmo, graças a sua Vontade Medíocre (+0).
>
> Bandu, O Oculto, mais um pensador do que um guerreiro, possui Vigor Medíocre (+0), ou seja, ele só possui a quantidade padrão de duas caixas de estresse físico. Seu Razoável (+2) em Vontade é bom o bastante para lhe conceder uma caixa extra de estresse mental.
>
> Visto que nenhum dos personagens possuí Vigor ou Vontade Excepcional (+5) cada um possui apenas o número padrão de consequências: uma suave, uma moderada e uma severa.

------

- [« Façanhas e Recarga](../facanhas-e-recarga/)
- [Tudo pronto! »](../tudo-pronto/)
