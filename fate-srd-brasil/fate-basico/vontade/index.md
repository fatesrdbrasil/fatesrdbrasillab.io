---
title: Vigor
layout: default
---


### Vontade

Esta perícia representa o nível geral da fortitude mental de seu personagem, da mesma forma que Vigor representa a fortitude física.

- `O`{: .fate_font} **Superar:** Você pode usar Vontade para encarar obstáculos que exijam esforço mental. Enigmas e quebra-cabeças estão dentro desta categoria, assim como qualquer tarefa mental que exija concentração, como decifrar um código. Use Vontade quando vencer um desafio mental for apenas uma questão de tempo e Conhecimentos se for preciso mais do que esforço mental para superar o desafio. Muitos dos obstáculos que você enfrentará usando Vontade podem fazer parte de desafios maiores, para reﬂetir o esforço envolvido.
Disputas de Vontade podem reﬂetir jogos mais desafiadores, como xadrez ou uma bateria de provas difíceis. Em cenários onde a magia ou o psiquismo são habilidades comuns, disputas de Vontade ocorrerão bastante.
- `C`{: .fate_font} **Criar Vantagem:** Você pode usar Vontade para colocar aspectos em você mesmo, representando um estado de concentração e foco profundos.
- `A`{: .fate_font} **Atacar:** Vontade não é usada para atacar. Dito isso, em cenários onde há habilidades psíquicas, entrar em conﬂitos desse gênero pode ser algo que você faça usando essa perícia. Esse tipo de coisa pode ser adicionado à Vontade através de uma façanha ou extra.
- `D`{: .fate_font} **Defender:** Vontade é a perícia principal para se defender contra ataques mentais de Provocar, representando seu controle sobre suas emoções.

**Especial:** A perícia Vontade concede caixas adicionais de estresse e consequência mentais. Regular (+1) ou Razoável (+2) libera a terceira caixa de estresse. Bom (+3) e Ótimo (+4) liberam a quarta caixa de estresse. Excepcional (+5) ou mais concede, além das caixas, um espaço adicional para consequência suave. Todos esses espaços só poderão ser usados para danos mentais.

#### Façanhas Para Vontade

-  **Determinado:** Use Vontade no lugar de Vigor em qualquer rolagem de superar para feitos físicos.
-  **Duro na Queda:** Você pode escolher ignorar uma consequência suave ou moderada pela duração da cena. Ela não poderá ser forçada ou invocada por seus inimigos. Ao fim da cena ela voltará pior, no entanto; se era uma consequência suave, se torna moderada e se era moderada, se tornará severa.
-  **Indomável:** +2 para se defender contra ataques de Provocar ligados especificamente à intimidação ou medo.

- [« Vigor](../vigor/)
- [Ações e Resoluções »](../acoes-e-resolucoes/)
