I"�<h1 id="façanhas">Façanhas</h1>

<p><strong>Façanhas</strong> são truques, manobras ou técnicas que seu personagem possui e que mudam o funcionamento de uma <a href="../realizando-acoes-resolucoes-abordagens-e-acoes/#escolha-sua-abordagem">abordagem</a> para aquele personagem. Geralmente isso significa receber um bônus em certas situações, mas às vezes elas te concedem outras habilidades e características. Uma façanha também pode refletir um equipamento especial, de alta qualidade ou exótico que o seu personagem tem acesso e que fornece, com certa frequência, uma vantagem sobre outros personagens.</p>

<p>Não há uma lista definitiva de façanhas onde você possa escolher as suas; assim como os aspectos, cada um cria as suas próprias façanhas. Há dois guias básicos para lhe ajudar na criação das suas façanhas para que você tenha algo para se inspirar.</p>

<p>O primeiro tipo de façanha fornece um bônus de +2 quando você usar certa abordagem em certas situações. Use este formato:</p>

<p>Como eu <strong>[descreva algo em que você é excepcional, sabe fazer bem ou é incrível de alguma forma]</strong>, consigo +2 quando eu <strong>[escolha uma: Cuidadoso, Esperto, Estiloso, Poderoso, Ágil, Sorrateiro]</strong> <strong>[escolha um: ataco, defendo, crio vantagem ou supero]</strong> quando <strong>[descreva uma circunstância]</strong>.</p>

<p>Por exemplo:</p>

<blockquote>
  <ul>
    <li>Como <strong>possuo uma voz encantadora</strong>, consigo +2 quando <strong>Furtivamente obtenho vantagem enquanto converso com alguém</strong>.</li>
    <li>Como <strong>amo desafios</strong>, consigo +2 quando <strong>uso minha Esperteza para superar um obstáculo quando estou diante de um quebra-cabeça, enigma ou algo parecido</strong>.</li>
    <li>Como <strong>sou um dos maiores duelistas do mundo</strong>, consigo +2 quando <strong>ataco Estilosamente quando me encontro em um duelo de espadas</strong>.</li>
    <li>Como <strong>tenho um escudo grande</strong>, consigo +2 quando <strong>me defendo Poderosamente usando o escudo em combate corpo a corpo</strong>.</li>
  </ul>
</blockquote>

<p>Às vezes, se uma circunstância é restritiva, é possível aplicar a façanha para ambos criar vantagem <em>e</em> uma ação de superar.</p>

<p>O segundo tipo de façanhas permite tornar algo real, fazer algo incrível ou então ignorar as regras de algum forma. Use este formato:</p>

<p>Como eu <strong>[descreva algo em que você é excepcional, sabe fazer bem ou de alguma forma é incrível]</strong>, uma vez por sessão eu posso <strong>[descreva algo legal que você pode fazer]</strong>.</p>

<p>Por exemplo:</p>

<blockquote>
  <ul>
    <li>Como <strong>possuo vários contatos</strong>, uma vez por sessão eu posso <strong>conseguir a ajuda de um aliado na hora certa</strong>.</li>
    <li>Como sou <strong>instintivo em combate</strong>, uma vez por sessão eu posso <strong>escolher agir primeiro em um conflito</strong>.</li>
    <li>Como <strong>corro como um leopardo</strong>, uma vez por sessão eu posso <strong>surgir onde quiser, desde que eu possa correr até lá, não importando de onde eu parti</strong>.</li>
  </ul>
</blockquote>

<p>Esses formatos existem para dar uma ideia sobre como as façanhas podem ser construídas, mas não se sinta obrigado a segui-las à risca se tiver uma ideia melhor. Se quiser saber mais sobre a criação de façanhas então veja <a href="../../fate-basico/pericias-e-facanhas/"><em>Perícias e Façanhas</em></a> no <em>Fate Básico</em>.</p>

<ul>
  <li><a href="../aspectos-e-pontos-de-destino/">« Aspectos e Pontos de Destino</a></li>
  <li><a href="../aprendendo-com-suas-acoes-evolucao-do-personagem/">Aprendendo Com Suas Ações: Evolução do Personagem »</a></li>
</ul>
:ET