---
title: Evolução e Alteração
layout: default
---

## Evolução e Alteração

Seus personagens não permanecem imutáveis durante a campanha inteira. Conforme suas histórias avançam, eles também terão a chance de crescer e mudar em resposta aos eventos no jogo. Os conﬂitos que enfrentam e complicações que superam alterarão o modo como são vistos e os levarão a novos desafios.

Além de seus personagens, o mundo do jogo também muda. Os personagens resolverão ameaças, mudarão a aparência de um lugar ou causarão algum impacto durante o jogo que afetará alguma das questões do cenário. Falaremos sobre a evolução do mundo mais à frente.

A evolução de personagens em *Fate* pode ocorrer de duas maneiras: você pode ou mudar uma informação na ficha para algo equivalente, ou acrescentar elementos novos à ficha. Cada oportunidade para fazer essas alterações é chamada de **marco**.

## O Que É Um Marco?

Um marco é um momento durante o jogo onde você tem a chance de mudar ou melhorar o seu personagem. Chamamos isso de marco porque ele acontece em pontos significativos do jogo – o fim de uma sessão, o fim de um cenário ou o fim de um arco.

Normalmente, esses pontos significativos vêm logo depois de algum evento importante na história que justifique a mudança em seu personagem. Você pode revelar um detalhe significativo na trama ou terminar a sessão com um suspense. Pode ser a derrota de um vilão importante ou a resolução de alguma questão do enredo no final de um cenário. Talvez os personagens terminem uma história grande que abala o mundo da campanha ao final de um arco.

Obviamente, as coisas nem sempre caminham perfeitamente, por isso o Narrador precisa de algum critério para decidir quando cada tipo de marco ocorre. Se parecer satisfatório ter um marco no meio de uma sessão, vá em frente, mas procure seguir as sugestões apresentadas aqui para não oferecer oportunidades de avanço com tanta frequência.

Há três tipos de marco, de acordo com nível de importância: menores, significativos e maiores.

### Marcos Menores

Marcos menores normalmente ocorrem ao final de uma sessão de jogo ou quando uma parte da história é resolvida. A ideia deste tipo de marco é causar uma mudança no personagem, não necessariamente tornando-o mais poderoso; é um ajuste devido aos acontecimentos na história. Às vezes não fará muito sentido tentar tirar algum proveito de um marco menor, mas você sempre tem essa oportunidade caso precise.

Durante um marco menor, você pode fazer uma das seguintes alterações (apenas uma):

-  Trocar os valores de duas perícias um pelo outro ou substituir uma perícia Regular (+1) por uma que não possua em sua ficha.
-  Alterar uma façanha para outra qualquer.
-  Comprar uma nova façanha, contanto que possua recarga para isso (lembre-se que a recarga não pode ser inferior a 1).
-  Renomear um aspecto de personagem que não o conceito.

Além disso, você também pode renomear qualquer consequência moderada que possuir, iniciando assim o processo de recuperação, presumindo que você ainda não o tenha feito.

Esta é uma boa forma de fazer pequenos ajustes, se parecer que algo não ficou tão bom em seu personagem – você não usa tão frequentemente uma façanha como pensava, talvez você resolva a ***Rixa Violenta Com Edmund*** e esse aspecto não é mais apropriado, ou qualquer mudança que mantenha a consistência do personagem perante os acontecimentos do jogo.

Na verdade, você deve ser capaz de justificar quase sempre a mudança feita no contexto da história do jogo. Talvez não seja interessante alterar ***Cabeça Quente*** para ***Pacifista***, por exemplo, a não ser que algo inspirador tenha ocorrido na história para uma alteração tão significativa – você conhece um homem santo ou teve uma experiência traumática que o fez desistir da espada ou qualquer coisa assim. Narradores, vocês são os árbitros finais em julgar tais situações, mas não seja um carrasco que sacrifica a diversão de um jogador em benefício da história.

> Fräkeline atinge um marco menor. Maira olha sua ficha de personagem, procurando algo para mudar. Uma coisa que nota é que nas últimas sessões, Esopo a tem colocado em perigo, mas sempre se saído (quase) ileso.
> 
> Ela olha para Léo e diz, “Quer saber? Acho que Fräk não pensa mais que ***Esopo Precisa Ser Protegido***. Vou mudá-lo para ***Não Posso Confiar em Esopo***.”
> 
> Michel entra na conversa e diz, “Mas e os planos dos Bruxos que você viu?”
> 
> Maira pensa um pouco e responde, “Bom, se eu encontrar provas que ele precisa mesmo de proteção, eu mudo de volta.”
> 
> Amanda aprova a mudança e Maira reescreve um dos aspectos de Fräkeline.
> 
> Enquanto isso, Esopo também consegue um marco menor.Léo olha sua ficha e nota que ele gastou mais tempo mentindo para as pessoas do que tentando fazer amigos. Ele pergunta a Amanda se pode alterar os níveis de suas perícias Enganar e Comunicação, ficando com Enganar Bom (+3) e Comunicação Razoável (+2). Ela concorda e ele faz a alteração em sua ficha.

### Marco Significativo

Marcos significantes geralmente ocorrem no final de um cenário ou na conclusão de algum evento importante (ou, em caso de dúvida, ao final de duas ou três sessões). São diferentes dos marcos menores (basicamente uma alteração) pois representam algum tipo de aprendizado – lidar com problemas e desafios torna o seu personagem mais capaz no que faz.

Em adição aos benefícios de um marco menor, você também tem direito a ambos os seguintes:

-  Um ponto de perícia adicional, para comprar uma nova perícia em nível Regular (+1) ou aumentar o nível de uma perícia existente.
-  Se você possuir qualquer consequência grave, pode renomear a mesma para iniciar o processo de recuperação, se ainda não o fez.

Quando você gasta seu ponto de perícia, você está dando um passo a mais na escala. Você pode usá-lo para comprar uma nova perícia em nível Regular (+1) ou pode aumentar o nível de uma perícia existente em sua ficha de personagem – digamos de Bom (+3) para Ótimo (+4).

#### A Coluna de Perícias

Durante a criação do personagem, as perícias são organizadas em uma pirâmide. Não é preciso se prender a esse formato durante a evolução do personagem.

No entanto, ainda há a limitação das **colunas de perícias**. Ou seja, **você não pode ter mais perícias num nível acima do que o nível abaixo dela**.

Então, se você tem três colunas em nível Bom (+3), você também tem três perícias Regulares (+1) e ao menos três Razoáveis (+2) para suportar suas três perícias Boas (+3).

A pirâmide já segue esse padrão, mas quando você adiciona novas perícias, você precisa ter certeza que não estará ultrapassando esse limite. É fácil esquecer que, ao aumentar o nível de uma perícia em um ponto, você pode não ter mais espaço suficiente na pirâmide para dar suporte ao novo nível dessa perícia.

Então digamos que você possui uma Boa (+3), duas Razoáveis (+2) e três Regulares (+1). Sua distribuição de perícias ficaria assim:

| ***Nível***       | ***Perícia*** | ***Perícia*** | ***Perícia*** |
|------------------:|---------------|---------------|---------------|
| **Bom (+3)**      | Vontade       |               |               |
| **Razoável (+2)** | Provocar      | Comunicação   |               |
| **Regular (+1)**  | Enganar       | Contatos      |  Recursos     |

Ao alcançar um marco, você melhora uma perícia Razoável (+2) para Boa (+3). Isso vai lhe deixar com duas Boas (+3), uma Razoável (+2) e três Regulares (+1):

| ***Nível***       | ***Perícia*** | ***Perícia*** | ***Perícia*** |
|------------------:|---------------|---------------|---------------|
| **Bom (+3)**      | Vontade       | Comunicação   |               |
| **Razoável (+2)** | Provocar      |               |               |
| **Regular (+1)**  | Enganar       | Contatos      |  Recursos     |

Vê como não funciona? Agora falta uma segunda perícia em nível Razoável para se enquadrar nas regras.

Quando isso acontecer, você tem duas opções. Você pode comprar uma nova perícia no nível mais baixo possível – nesse caso, Regular (+1) – e então melhorar o seu nível em marcos futuros até que ela alcance o nível desejado. A outra alternativa é guardar o ponto de perícia e esperar até acumular o suficiente para comprar a perícia em qualquer nível que deseje.

Então, no caso acima, você pode comprar uma perícia Regular (+1), promover uma de suas perícias Regulares para Razoável (+2) e em seguida melhorar a perícia original para Bom (+3). Isso pode levar até três marcos significantes para ser realizado. Ou você pode esperar, poupar os pontos até possuir três pontos de perícia, comprar uma nova perícia em nível Razoável (+2) e melhorar a perícia que desejar para Bom (+3). Só depende de você decidir se quer comprar perícias novas ou não nesse meio tempo.

> Bandu consegue um marco significativo após o fim de um cenário. Ele ganha um ponto de perícia.
> 
> Michel olha para sua ficha de personagem e decide que quer aumentar o seu nível em Percepção de Razoável (+2) para Bom (+3). Ele sabe que isso vai contra à regra da pirâmide de perícias, então em vez disso decide comprar Recursos em nível Regular (+1) – os PJs passaram por algumas aventuras lucrativas e ele acredita que é uma boa oportunidade para começar a acumular riquezas.
> 
> Se ele esperar mais dois marcos, poderá por mais uma perícia em nível Razoável (+2) e então colocar o nível desejado em Percepção, que é Bom (+3).
> 
> Ele também tem a oportunidade de usar um dos benefícios de um marco menor. Ele acredita que esteve em muitas lutas durante o jogo e pensa que ***Me Ofendo por Pouco*** não é tão apropriado, considerando o número de vezes que ele foi ofendido. Ele altera para ***Me Desafie e Haverá Consequências***, para reﬂetir a mudança de atitude de seu personagem perante a opinião alheia.

Você pode ter notado que quanto mais sobe na escala, mais difícil fica de melhorar suas perícias. Isso é proposital — ninguém conseguirá ser incrível em tudo o tempo todo. Não teria graça.

------

Narradores, restringir demais o uso das perícias pode ser uma dor de cabeça às vezes. Se você e seus jogadores realmente querem melhorar o nível de certa perícia sem se preocuparem em quebrar regras, sugira ao jogador melhorar a perícia agora e gastar os próximos marcos para “corrigir” a pirâmide de perícias, no lugar de fazê-lo esperar.

------

### Marco Maior

Um marco maior deve apenas ocorrer quando algo acontece que abala *de forma significativa* a estrutura do mundo – o fim de um arco (ou algo em torno de três cenários), a morte de um PdN principal ou qualquer outra alteração em grande escala que possa mexer com todo o universo do jogo.

A ideia destes marcos é tornar o personagem mais poderoso. Os desafios anteriores não são mais suficientes para ameaçar os personagens e as ameaças futuras precisarão ser mais organizadas, determinadas e difíceis para enfrentá-los.

Alcançar um marco maior confere os benefícios de um marco significativo e um marco menor e todas as opções a seguir:

-  Se você possuir uma consequência extrema, renomeie-a para reﬂetir o fato de você já ter passado por seu efeito mais debilitante. Isso permite a você receber outra consequência extrema no futuro, se for preciso.
-  Receba um ponto adicional de recarga, o que permite a você comprar uma nova façanha imediatamente ou mantê-lo para que receba mais pontos de destino no início de uma sessão.
-  Adicione uma perícia nova acima do limite da pirâmide, se possuir pontos para isso, aumentando assim o limite.
-  Renomeie o conceito de personagem se desejar.

Alcançar um marco maior é algo grandioso. Personagens com mais façanhas possuirão diversas possibilidades de receberem bônus, tornando suas perícias mais efetivas que o normal. Personagens com alto nível em recarga possuirão uma boa fonte de pontos de destino para trabalhar seus aspectos durante as sessões, o que significa não precisa forçar demais os próprios spectos.

Narradores, quando um PJ ultrapassa o limite atual de sua pirâmide, é preciso alterar a forma como você cria os PdNs de oposição, pois agora pois de inimigos que possam opor os PJs em termos de competência para prover um desafio digno. Isso não acontece de uma vez só, o que lhe dá a oportunidade de introduzir inimigos mais poderosos de forma gradual. Mas se jogar por bastante tempo, eventualmente terá PJs com perícias em níveis Épicos e Lendários – isso já lhe dá uma noção do tipo de vilões que precisarão aparecer.

Acima de tudo, um marco maior deve sinalizar que muitas coisas no mundo de jogo foram alteradas. Algumas mudanças serão reﬂetidas no avanço do mundo, mas dado o número de vezes que os PJs tiveram a chance de alterar seus aspectos em resposta à história, é bem provável que você esteja agora lidando com um grupo que possui prioridades e preocupações bem diferentes do início do jogo.

> Fräkeline chega ao fim de um arco e alcança um marco maior. Em jogo, os PJs derrubaram Zoidai Vasa, uma PdN importante do mundo de jogo.
> 
> Maira verifica sua ficha de personagem. Ela recebeu uma consequência extrema no arco passado e deixou um de seus aspectos ser substituído por ***Memórias Corrompidas***. Agora ela tem a oportunidade de renomear esse aspecto novamente e o altera para ***Memórias Superficiais*** – essa experiência deixou cicatrizes, mas está melhor que antes, agora que ela possui um objetivo.
> 
> Ela também recebe um ponto adicional de recarga. Conversando com Amanda, ela pensa que talvez pudesse transformar a experiência de perder e recuperar memórias em uma façanha. Amanda não vê razão alguma para não concordar e Maira decide comprar uma nova façanha, criando “Cérebro Reserva: +2 quando usar Conhecimentos para guardar ou lembrar informações fornecidas previamente.”
> 
> Maira escreve a nova façanha na ficha de personagem e reescreve, também, o aspecto.
> 
> Bandu, O Oculto também alcança um marco maior. Michel olha para sua ficha de personagem e percebe que pode avançar ainda mais sua maior perícia Conhecimentos, que é Excepcional (+5). Ele faz isso e Amanda faz uma nota que ela precisa criar inimigos à altura de Bandu que agora é mais poderoso, para dar-lhe um desafio no mesmo nível.
> 
> Finalmente, Esopo também atinge um marco maior.
> 
> Recentemente Esopo se envolveu numa reunião dos Ordenadores espaciais e descobriu que todos os mercenários que ele conhece estão trabalhando com o Clã do Dedo, que recentemente apoiaram Zoidai em seus esforços para conquistar Ondrin. Em resposta a isso, Léo decide alterar seu conceito para ***Soldado Ordenador Lagosta***, indicando seu desejo de se distanciar de sua vida ilegal. Amanda diz que seus conhecidos não aceitarão isso de bom grado. Então temos Fräk com memórias reservas, Bandu atingindo um nível de poder ainda não visto e Esopo questionando seu próprio estilo de vida. Amanda faz diversas anotações sobre as implicações disso nos próximos cenários.

------

### De Volta À Criação De Personagem

Um marco maior pode ser visto como o final de uma temporada numa série de TV. Quando a próxima sessão iniciar, muita coisa pode ser diferente em seu jogo – você pode criar novos problemas, muitos personagens terão aspectos modificados, haverá novos traços e características na campanha, etc.

Quando isso acontece, pode ser a hora de sentar, como você fez na criação dos personagens, e revisar as fichas dos PJs, alterando ou ajustando qualquer coisa que possa precisar de revisão - novas configurações de perícias, um novo pacote de façanhas, alterações nos aspectos, etc. Você também pode examinar as questões e problemas em jogo para ter certeza de que ainda são apropriados, revisar aspectos de local, ou o que for preciso para manter o ﬂuxo do jogo.

Contanto que você mantenha os PJs no mesmo nível de recarga e perícias que estavam, reorganizar as fichas pode ser exatamente o que você precisa para ter certeza que todos sabem o que está acontecendo no jogo. E lembrem-se, Narradores: quanto mais chances os jogadores tiverem de investirem ativamente no mundo de jogo, maior será o retorno.

------

- [« Construindo Campanhas](../construindo-campanhas/)
- [Evolução do Mundo »](../evolucao-do-mundo/)

