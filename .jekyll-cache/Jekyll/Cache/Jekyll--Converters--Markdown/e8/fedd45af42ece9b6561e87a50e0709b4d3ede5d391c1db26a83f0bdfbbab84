I"�<h1 id="provoke">Provoke</h1>

<p>Provoke is the skill about getting someone’s dander up and eliciting negative
emotional response from them—fear, anger, shame, etc. It’s the “being a jerk”
skill.</p>

<p>To use Provoke, you need some kind of justification. That could come entirely
from situation, or because you have an aspect that’s appropriate, or because
you’ve created an advantage with another skill (like Rapport or Deceive), or
because you’ve assessed your target’s aspects (see Empathy).</p>

<p>This skill requires that your target can feel emotions—robots and zombies
typically can’t be provoked.</p>

<p><span class="fate_font">O</span><strong>Overcome</strong>: You can Provoke someone into doing what
you want in a fit of emotional pique. You might intimidate them for
information, piss them off so badly that they act out, or scare them into
running away. This will often happen when you’re going up against nameless
NPCs or it isn’t worthwhile to play out the particulars. Against PCs or
important NPCs, you’ll need to win a contest. They oppose with Will.</p>

<p><span class="fate_font">C</span><strong>Create an Advantage</strong>: You can create advantages
representing momentary emotional states, like <span class="aspect">Enraged</span>,
<span class="aspect">Shocked</span>, or <span class="aspect">Hesitant</span>. Your target
opposes with <a href="../../fate-core/will">Will</a>.</p>

<p><span class="fate_font">A</span><strong>Attack</strong>: You can make mental attacks with Provoke,
to do emotional harm to an opponent. Your relationship with the target and the
circumstances you’re in figure a great deal into whether or not you can use
this action.</p>

<p><span class="fate_font">D</span><strong>Defend</strong>: Being good at provoking others doesn’t
make you better at avoiding it yourself. You need Will for that.</p>

<h2 id="provoke-stunts">Provoke Stunts</h2>

<ul>
  <li><strong>Armor of Fear</strong>. You can use Provoke to defend against Fight attacks, but only until the first time you’re dealt stress in a conflict. You can make your opponents hesitate to attack, but when someone shows them that you’re only human your advantage disappears.</li>
  <li><strong>Provoke Violence</strong>. When you create an advantage on an opponent using Provoke, you can use your free invocation to become the target of that character’s next relevant action, drawing their attention away from another target.</li>
  <li>
    <p><strong>Okay, Fine!</strong> You can use Provoke in place of Empathy to learn a target’s aspects, by bullying them until they reveal one to you. The target defends against this with Will. (If the <abbr title="Game Master">GM</abbr> thinks the aspect is particularly vulnerable to your hostile approach, you get a +2 bonus.)</p>
  </li>
  <li><a href="/fate-srd/fate-core/physique">« Physique</a></li>
  <li><a href="/fate-srd/fate-core/rapport">Rapport »</a></li>
</ul>

:ET