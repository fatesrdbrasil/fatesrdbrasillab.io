I"'<h2 id="kung-fu">Kung Fu</h2>

<hr />

<p>O termo “kung fu” é bem abrangente nessa sessão, mas esta é apenas uma das várias artes marciais do mundo. As regras aqui podem se aplicar ao savate ou ninjutsu facilmente, ou a qualquer arte marcial louca que seja praticada em Marte.</p>

<hr />

<p>O <em>Kung Fu</em>, como vemos na vida real, é coberto pela perícia Lutar. Falaremos aqui sobre o <em>Kung Fu</em> cinematográfico, com grandes pulos, acrobacias e o estilo melodramático dos filmes de artes marciais de Hong Kong.</p>

<p>Esse tipo de <em>kung fu</em> é tão dramático que não se encaixa em jogos mais realistas, e com certeza altera o tom até mesmo uma ação aventuresca do tipo <em>pulp</em>. Pode ser um pouco demais. Se pretende incluir este estilo de arte marcial louco e exagerado em seu jogo, deixe claro para seus jogadores que isso é algo que todos os personagens podem fazer – tanto PCs <em>quanto</em> PdNs.</p>

<p>Em um filme de <em>kung fu</em>, os praticantes da arte estão um nível acima dos praticantes normais. Se o seu jogo se concentrará fortemente em mestres de <em>kung fu</em>, então faz sentido que a maioria — se não todos — dos personagens dos jogadores possuam essa habilidade. Se apenas um personagem pratica <em>kung fu</em>, certifique-se de que os outros personagens possuam sua própria forma de se destacar. Além disso, você pode querer suavizar algumas loucuras descritas nesta seção. Você não quer apenas um personagem dominando a ação de uma cena ao sair saltando por todos os lugares. Se todos têm a habilidade, então o risco de alguém monopolizar a atenção é menor. O <em>kung fu</em> cinematográfico é muito poderoso. Se você o introduz, é provável que domine todo o seu cenário.</p>

<p>Os mestres do <em>kung fu</em> podem correr pelas paredes, saltar grandes distâncias, parar projéteis em pleno ar ou apará-los com uma arma, lutar em condições precárias de apoio e até mesmo andar sobre a água. Praticamente qualquer pessoa que tenha treinamento nas artes marciais pode fazer isso. Se um personagem possui treinamento em kung fu , então essas ações são tratadas como uma ação normal de correr ou saltar. Um jogador pode descrever seu personagem realizando essas ações sem nenhuma dificuldade especial. O Narrador pode exigir rolagens, mas esse tipo de ação não é algo excepcionalmente difícil ou fora do comum. Basta adicionar esses detalhes para que seu jogo tenha a mesma sensação de um filme de <em>kung fu</em>.</p>

<p>Há duas maneiras de abordar o <em>kung fu</em> em Fate. Um método é usar perícias existentes para cobrir as habilidades e luta com a arte marcial. A segunda é criar uma perícia <em>Kung Fu</em> dedicada a isso. A decisão de qual método adotar depende da natureza do seu jogo. Se há apenas um mestre <em>kung fu</em> em jogo, então a perícia <em>Kung Fu</em> pode ser apropriada. Se todos possuem treinamento na arte, então talvez faça mais sentido que todos usem perícias já existentes para realizar as façanhas do <em>kung fu</em>. Se for por esse caminho, um praticante do kung fu deve possuir um aspecto relacionado ao seu treinamento, para poder disponibilizar as habilidades da arte ligadas às perícias existentes — algo como <strong><em>Treinado no Monastério Wudang</em></strong> ou <strong><em>Mestre no Estilo Louva-a-Deus</em></strong>.</p>

<h3 id="usando-perícias-existentes">Usando Perícias Existentes</h3>

<p>Se preferir usar o método das perícias existentes, então Lutar obviamente ficará com a maior parte do trabalho. Qualquer ataque, desarmado ou com armas, depende da perícia Lutar, além de também poder ser usada em defesas. Como um mestre de <em>kung fu</em> também pode aparar flechas e outras armas de projétil, esse feito também seria realizado com a perícia Lutar. Se estiver jogando em um cenário moderno ou se houver armas de fogo no universo de jogo, será necessário tomar uma decisão sobre o uso do <em>kung fu</em> contra elas. Não parece fora do gênero sugerir que um praticante de <em>kung fu</em> possa desviar ou mesmo aparar balas, especialmente se ele usa uma espada ou outra arma de metal.</p>

<p>Andar em paredes e saltar estão incluídas em Atletismo. Qualquer tipo de façanha física além de lutar será coberta por Atletismo ou possivelmente Vigor. Os praticantes de <em>kung fu</em> tradicionalmente possuem treinamento para resistir a dano ou sobreviver a quedas, que são ações da perícia Vigor. A perícia Conhecimentos pode ser usada para identificar escolas rivais ou técnicas dos oponentes. Uma rolagem de Conhecimentos pode criar uma vantagem, se você deduzir corretamente quem é o professor ou escola de luta de seu oponente.</p>

<h3 id="a-perícia-kung-fu">A Perícia <em>Kung Fu</em></h3>

<p>Se adicionar uma perícia <em>Kung Fu</em> ao jogo, qualquer habilidade que não se enquadrarem no que é considerado normal para Lutar ou Atletismo, como saltar e andar pelas paredes, exige uma rolagem de <em>Kung Fu</em>. Isso pode ser bastante útil se você desejar destacar as ações de <em>Kung Fu</em>. Os praticantes de <em>kung fu</em> normalmente possuem movimentos especiais ou segredos conhecidos apenas por eles e seus mestres. Em um jogo com muitos artistas marciais, esses movimentos especiais são uma boa forma de diferenciar os personagens e criar a sensação de que há vários estilos diferentes. Movimentos especiais podem ser modelados usando façanhas e aspectos.</p>

<p>Apesar da maior parte dos movimentos no <em>kung fu</em> provavelmente possuir nomes, um mestre de <em>kung fu</em> pode possuir um ou dois movimentos característicos que representam seu estilo pessoal. Esses podem ser representado por aspectos, como o <strong><em>Toque da Morte</em></strong> ou <strong><em>Estilo da Lâmina Escondida</em></strong>. Quando invocado, o jogador pode descrever em detalhes a natureza incrível ou mística daquela ação. Esses aspectos devem fazer com que o personagem pareça incrível e você pode usá-los para mostrar que PdNs conhecem o seu personagem e seu estilo, como “Não se aproxime, ela é mestre do Estilo da Lâmina Escondida”. Eles podem ser forçados se um oponente conhecer um movimento que sirva de contra-ataque, assim como o jogador pode usar seu próprio conhecimento de <em>kung fu</em> para analisar o estilo de um oponente e deduzir como contra-atacar.</p>

<p>Para uma versão mais mecanicamente completa, use o sistema de façanhas para criar movimentos. Você pode criar toda uma escola de <em>kung fu</em> usando façanhas, com façanhas mais fáceis como pré-requisitos para acessar as mais avançadas. Use a descrição de suas façanhas para criar um estilo de <em>kung fu</em> marcante, com os nomes e efeitos dos movimentos retratando o tema do seu estilo de luta.</p>

<h4 id="punho-bêbado">Punho Bêbado</h4>

<p>Abaixo segue um exemplo de uma árvore de façanhas de <em>kung fu</em> para o estilo do Punho Bêbado.</p>

<p><strong>O Vacilo do Bêbado:</strong> Você balança e vacila nos passos, evitando os golpes do adversário aparentemente por acaso. Quando bem-sucedido numa rolagem de Atletismo para se defender usando esta técnica, você recebe +1 no seu próximo ataque contra o oponente que tentou acertá-lo. Se for bem-sucedido com estilo, recebe +2.</p>

<p><strong>O Empurrão do Bêbado:</strong> Seu empurrão rude e bruto contém mais poder do que parece possível. Você recebe +2 ao usar Vigor para criar vantagem contra um oponente ao tentar tirar-lhe o equilíbrio.</p>

<p><strong>Beber do Jarro:</strong> Você faz uma pausa momentânea para tomar um gole de vinho de seu jarro, fortalecendo-se para a batalha. Quando você toma um gole durante uma luta, limpe sua menor caixa de estresse. Isso exige gastar uma ação inteira bebendo.</p>

<p><strong>A Queda do Bêbado (Exige o Vacilo do Bêbado):</strong> Quando um inimigo o ataca você perde o equilíbrio e cai, rapidamente voltando a ficar de pé, mas seu inimigo agora se encontra perigosamente exposto. Role Atletismo para esquivar. Em caso de sucesso, coloque um impulso em seu adversário como Guarda Baixa ou então Desequilibrado que pode ser usado por qualquer um contra ele. Em um sucesso com estilo, coloque um segundo impulso em seu oponente.</p>

<p><strong>A Dança do Bêbado (Exige o Empurrão do Bêbado):</strong> Seus golpes são brutos e óbvios, mas, ao esquivar-se, seu adversário parece ser atingido por um cotovelo ou joelho acidentalmente. Faça uma rolagem de Lutar normal. Se atingir o seu oponente, você causa estresse normalmente. Se você errar ou empatar, seu oponente recebe um de estresse físico de qualquer forma.</p>

<p><strong>Derramar Vinho (Exige Beber do Jarro):</strong> Você pega um copo e despeja uma dose do jarro. Essa tarefa difícil e elaborada causa uma pausa na batalha. Ninguém pode atacá-lo enquanto estiver derramando e você remove sua caixa de estresse mais baixa. Essa técnica exige uma ação completa e não pode ser realizada mais de uma vez em seguida.</p>

<p><strong>O Bêbado Tropeça (Exige A Queda do Bêbado):</strong> Você cambaleia e tropeça sem controle, mas seu inimigo sempre o erra e acaba acertando algum obstáculo próximo, causando dano a si mesmo. Quando você se desvia de um golpe usando Atletismo, seu oponente recebe um de estresse ou dois se você for bem-sucedido com estilo.</p>

<p><strong>A Firmeza do Bêbado (Exige a Dança do Bêbado):</strong> Você cambaleia e parece prestes a cair, então se aproxima e agarra o braço de seu adversário para firmar-se. Esse ato aparentemente desproposital bloqueia o <em>qi</em> e paralisa o seu oponente. Você pode colocar um aspecto de situação como <strong><em>Qi Bloqueado</em></strong> em seu oponente com uma invocação grátis. Seu adversário não pode usar nenhuma manobra de <em>kung fu</em> enquanto não remover o aspecto.</p>

<hr />

<ul>
  <li><a href="../subsistemas/">« Subsistemas</a></li>
  <li><a href="../cibernetica/">Cibernética »</a></li>
</ul>
:ET