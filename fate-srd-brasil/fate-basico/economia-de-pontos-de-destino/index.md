---
title: A Economia de Pontos de Destino
layout: default
---

## A Economia de Pontos de Destino

Na maioria das vezes, usar aspectos envolve o gasto de pontos de destino. Você indica a sua quantidade de pontos de destino usando marcadores como fichas de pôquer, contas de vidro ou outras coisas.

No geral deve haver um equilíbrio entre gastar e ganhar pontos de destino durante cada sessão. Os jogadores gastam à medida que eventos incríveis e cruciais aparecem e recebem de volta quando vivem momentos dramáticos e complicados. Logo, se os pontos de destino estiverem ﬂuíndo como deveriam, haverá esses ciclos de vitórias e empecilhos que deixam a história mais interessante e divertida.

Vejamos como isso funciona.

### Recarga

Cada jogador ganha um número de pontos de destino no início de cada sessão. O total desses pontos ganhos é chamado de **recarga**. O valor padrão de recarga, para personagens iniciantes, é três pontos de destino, mas você pode optar por gastar até dois desses pontos para conseguir façanhas extras.

Você ganha um ponto a mais na sua recarga quando alcança um marco maior (o que vamos discutir em [*Construindo Campanhas*](../construindo-campanhas)), que pode ser usado para conseguir mais uma façanha ou permitir que ele aumente o seu valor de recarga, ganhando um ponto de destino a mais no início de cada sessão. Sua recarga nunca pode ser menor que um.

É possível terminar a sessão de jogo com mais pontos de destino que o seu nível atual de recarga. Se isso acontecer, você não perde os pontos adicionais quando iniciar a próxima sessão, mas você não vai ganhar o seu nível de recarga. No início de um novo cenário, a quantidade de pontos volta ao valor da recarga, sem exceções.

### Gastando Pontos de Destino

Você gasta pontos de destino das seguintes formas:

-  **Invocando um Aspecto:** Invocar um aspecto custa um ponto de destino, a não ser que a invocação seja grátis.
-  **Ativando uma Façanha:** Algumas façanhas são bastante poderosas, portanto custam um ponto de destino para serem ativadas.
-  **Recusando uma ação de Forçar:** Uma vez que forçar um aspecto tenha sido proposto, é possível pagar um ponto de destino para evitar a complicação.
-  **Declarando um Detalhe da História:** Adicionar algo à narrativa baseado em um de seus aspectos custa um ponto de destino.

------

### Façanhas e Recarga

-  3 Façanhas = Recarga 3
-  4 Façanhas = Recarga 2
-  5 Façanhas = Recarga 1

------

### Ganhando Pontos de Destino

Você ganha pontos de destino das seguintes maneiras:

-  **Forçando um Aspecto:** Você ganha um ponto de destino por aceitar uma complicação associada a um aspecto forçado. Como já falamos acima, isso pode ser algo retroativo se as circunstâncias permitirem.
-  **Quando Seus Aspectos São Invocados Contra Você:** Se alguém gastar um ponto de destino para invocar um aspecto ligado a seu personagem, você ganha aquele ponto de destino ao final da cena. Isso inclui vantagens criadas sobre seu personagem, assim como consequências.
- **Concedendo um Conﬂito:** Você recebe um ponto de destino ao conceder a vitória em um conﬂito, assim como um ponto para cada consequência que adquirir nesse mesmo conﬂito (isso não é a mesma coisa que ser derrotado em um conﬂito, mas falaremos sobre isso mais à frente).

### O Narrador e os Pontos de Destino

Narradores também possuem pontos de destino, mas as regras são um pouco diferente dos jogadores.

Quando o Narrador concede pontos de destino por aspectos forçados ou rendições, esses pontos saem de uma fonte ilimitada, separada para isso – não é preciso se preocupar com pontos de destino acabando, além de que o Narrador não gasta pontos para forçar aspectos.

Os PdNs já não possuem a mesma sorte. Eles possuem uma quantidade limitada de pontos de destino para usar em seu benefício. **Sempre que uma cena se inicia, você ganha um ponto de destino para cada PJ em cena**. Esses pontos podem ser usados para beneficiar qualquer PdN na cena, mas é possível conseguir mais pontos aceitando aspectos forçados, como os PJs fazem.

**O Narrador sempre volta ao valor padrão de um ponto por PJ ao início de cada cena.**

Há duas exceções:

-  Se o Narrador aceitou um aspecto forçado que encerrou a cena anterior ou começa a próxima. Se isso acontecer, o Narrador recebe um ponto de destino extra na próxima cena.
-  Um dos PdNs concedeu um conﬂito com os PJs na cena anterior. Se isso acontecer, o Narrador recebe os pontos que normalmente seriam cedidos por conceder em acréscimo aos pontos padrão de início de cena.

Se a próxima cena não possui uma interação significativa com algum PdN, esses pontos extras podem ser guardados até a próxima cena que inclua uma interação maior.

> Amanda está narrando um conﬂito crucial no qual os PJs enfrentam um inimigo importante que vinha tentando subjugá-los há vários cenários. Esses são os personagens na cena:
>
> -  Zoidai Vasa, Rainha do Crime da Nebulosa Cadente, um PdN importante
> -  Thalar, o Sobrevivente, um de seus agentes chefes, um PdN de apoio
> -  Sauri, a Invisível, uma antiga inimiga dos PJs contratada a pedidode Zoidai Vasa, um PdN de apoio
> -  Dois sargentos, PdNs sem nome
> -  Esopo
> -  Fräkeline
> -  Bandu, O Oculto
>
> Seu total de pontos de destino para esta cena é 3 – um para cada PJ: Esopo, Fräk e Bandu. Se Bandu estivesse em outro lugar (digamos, fazendo pesquisas num templo antigo), Amanda teria direito a dois pontos de destino, um por Esopo e um por Fräkeline.
>
> Um tempo depois no conﬂito, Zoidai Vasa é obrigada a conceder o conﬂito para que ela possa fugir sem sofrer danos sérios. Ela recebeu duas consequências no conﬂito, isso significa que ela ganhou três pontos de destino por conceder. Esses três pontos de destino são mantidos até a próxima cena.

- [« Usando Aspectos na Interpretação](../usando-aspectos-na-interpretacao/)
- [Perícias e Façanhas »](../pericias-e-facanhas/)
