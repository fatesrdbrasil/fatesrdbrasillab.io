I"m<h2 id="o-cenário-em-jogo">O Cenário em Jogo</h2>

<p>Agora você já deve estar pronto para começar: você tem um problema que não pode ser ignorado, uma boa variedade de questões sobre a história que ajudarão a resolver os problemas de uma forma ou de outra, um grupo de PdNs e suas motivações e a primeira cena que fará as coisas pegarem fogo.</p>

<p>A partir de agora deve ser mais fácil, certo? Você apresenta as questões, os jogadores as responderão gradualmente e tudo correrá tranquilamente para uma conclusão.</p>

<p>Bem… acredite, nunca acontece assim.</p>

<p>A coisa mais importante que você deve se lembrar quando começar um cenário é: <em>tudo sempre acontece diferente do esperado</em>. Os PJs odiarão um PdN que deveria ser um aliado, terão sucessos que revelarão os segredos mais cedo que o esperado, sofrerão perdas inesperadas e mudarão o curso de suas ações ou inúmeras outras coisas que fazem com que a história não siga o curso planejado.</p>

<p>Observe que não recomendamos predeterminar quais locais e cenas estarão envolvidos em um cenário – isso porque achamos que muito material pode ser perdido, já que muita coisa depende da dinâmica do grupo e de suas escolhas.</p>

<p>No entanto, nem tudo está perdido – as coisas que você preparou o ajudarão quando os personagens fizerem coisas inesperadas. Suas perguntas narrativas são vagas o suficiente para que possam ser respondidas de diversas maneiras, e você pode rapidamente cortar algo que não é tão relevante e substituir por algo sem ter que perder todo o seu trabalho.</p>

<blockquote>
  <p>Amanda esperava que a cena com Esopo, Fräk e Anya resultasse num conﬂito rápido, graças a Esopo, seguido de uma conversa onde todos descobrem que estão do mesmo lado.</p>

  <p>Certo? Não.</p>

  <p>O primeiro golpe de Esopo derruba Anya, acabando também seu primeiro contato com a Irmandade do Equilíbrio, uma organização que se opõe ao Império e aos Bruxos. Mudança de planos. Amanda pensa em como continuar a partir dali:</p>

  <ul>
    <li>
      <p>Os acompanhantes de Anya gritam “Vingança!” e lutam até a morte.</p>
    </li>
    <li>
      <p>Um dos três assume o papel de Anya e tenta conversar.</p>
    </li>
    <li>
      <p>Os guerreiros fogem (concedendo o conﬂito) e relatam a morte a seus superiores.</p>
    </li>
  </ul>

  <p>Ela escolhe a terceira opção. Depois de um golpe como esse, ninguém tentaria encarar Esopo. Além disso, Amanda imagina que Maira e Léo irão revistar o corpo, uma boa oportunidade para obterem informações sobre a Irmandade. Também é a chance de trazer Bandu à ação – talvez a Irmandade do Equilíbrio tenha informações que ele queira.</p>
</blockquote>

<p>Além disso, conhecer as motivações e objetivos de seus PdNs permite ajustar seus comportamentos mais facilmente do que colocá-los em cena esperando a chegada dos PJs. Quando os PJs tomarem uma decisão complicada, torne os PdNs tão dinâmicos quanto eles, fazendo-os tomar decisões súbitas e surpreendentes para atingir seus objetivos.</p>

<blockquote>
  <p>Amanda ainda está um pouco perdida com essa morte inesperada. Ela planejava fazer de Anya uma personagem importante para o arco da história – talvez não uma PdN poderosa, mas uma bastante importante. Amanda pode ao menos tornar sua morte significativa.</p>

  <p>Ela decide que embora a morte de um membro da Irmandade pudesse passar despercebida para a maioria dos habitantes de Ondrin, Meegooo, o Falasiano, certamente ouviria a respeito. Ele já conhecia Esopo de confrontos anteriores com o Clã do Dedo, e se você não pode vencê-los… recrute-os.</p>
</blockquote>

<h2 id="resolvendo-o-cenário">Resolvendo o Cenário</h2>

<p>Um cenário termina quando você consegue cenas suficientes para resolver a maioria das questões sobre a história que você criou quando preparou o cenário. Às vezes será possível fazer isso com uma simples sessão se possuir muito tempo ou poucas questões. Se possuir muitas perguntas para o cenário, talvez precise de várias sessões para resolver tudo.</p>

<p>Não sinta a necessidade de responder todas as questões se as coisas se concluíram satisfatoriamente – você pode usar questões não resolvidas para cenários futuros ou deixá-los de lado se não chamaram a atenção dos jogadores.</p>

<p>O fim de um cenário desencadeia um <strong>marco maior</strong>. Quando isso acontece, você também deve verificar se o mundo do jogo precisa avançar de alguma forma.</p>

<ul>
  <li><a href="../o-que-e-uma-cena/">« O que é uma cena?</a></li>
  <li><a href="../criando-campanhas/">Criando Campanhas »</a></li>
</ul>

:ET