---
title: Usando Como Foram Escritas
layout: default
---

## Usando Como Foram Escritas

A coisa mais fácil na maioria dos casos é simplesmente ajustar a lista de perícias para refletir as necessidades de seu jogo. Há algumas maneiras de abordar isso.

### Adicionando e Removendo Perícias

Construir coisas não é algo importante no seu jogo? Descarte a perícia Ofícios. Quer que as pessoas possam manobrar em gravidade zero? Adicione uma perícia que o faça. Essa é a forma mais fácil de modificar a lista de perícias, pois basta checar perícia por perícia e decidir o que é necessário ou não e o que precisa ser adicionado.

Tenha algumas coisas em mente, porém. Primeiro, se muitas perícias forem cortadas, os jogadores podem ter acesso a maior parte das perícias da lista. Se houver muitas perícias repetidas entre os PJs, você pode perder oportunidades para dar destaque aos PJs. Por outro lado, se adicionar muitas perícias, pode acontecer de os PJs não possuírem perícias suficientes
dessa lista, ou seja, não possuírem uma perícia necessária para uma determinada situação. Uma solução fácil é aumentar ou diminuir o número de perícias que cada PJ recebe durante a criação de personagem.

Considere que o Fate Sistema Básico possui 18 perícias. Como regra geral, a pirâmide de perícias permite que um personagem se destaque em 3 delas e tenha nível em mais 7, deixando 8 disponíveis. Não é uma relação precisa, mas vale a pena ter em mente quando for alterar a pirâmide. Veja [_Mudanças Estruturais_](../mudancas-estruturais/) para mais detalhes.

Finalmente, lembre-se que alterações nas perícias trazem repercussões nas caixas de estresse e ordem dos turnos.

### Renomeando e Fazendo Alterações


Se as perícias padrão funcionam bem para você do ponto de vista mecânico, mas algumas delas não encaixam perfeitamente no tema do jogo, mude alguns detalhes. Conhecimento pode se tornar Acadêmico ou Erudição, Condução se torna Cavalgar ou Pilotar e assim por diante. Isso também pode alterar o que uma determinada perícia pode fazer. Por exemplo, ao trocar Conhecimentos por Conhecimento Arcano e determinar que é a perícia padrão para conjurar magias, então fará sentido usá-la tanto em ataques quanto em defesas.

### Expandindo e Comprimindo


Um truque útil para alterar a lista de perícias é analisá-las do ponto de vista dos tipos de atividades que podemos realizar com elas, além do seu nível de importância no jogo. Se algumas atividades parecerem menos importantes, então podemos comprimir as atividades em uma perícia funcional – em um jogo não violento, você pode comprimir Atirar e
Lutar em uma única perícia de combate. 

Por outro lado, se algumas atividades se mostram importantes, é possível dividi-las em várias partes. Se as artes marciais são importantes em seu jogo, talvez queira expandir Lutar em várias perícias, como Luta Armada, Estilos Pesados ou Estilos Suaves. A vantagem dessa abordagem é as linhas gerais das perícias ainda funcionam - você apenas ajustou o foco.


------

- [« Perícias](../pericias/)
- [Outras Soluções »](../outras-solucoes/)
