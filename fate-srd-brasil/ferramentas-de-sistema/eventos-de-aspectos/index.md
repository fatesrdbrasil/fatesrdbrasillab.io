---
title: Eventos de Aspectos
layout: default
---

## Eventos de Aspectos

Este é um truque que você pode usar para criar a estrutura de uma aventura — pode até mesmo combinar esta técnica com a anterior para injetar eventos baseados nas ideias e sugestões dos jogadores.

Um evento de aspecto possui dois componentes: a **lista de eventos** e o aspecto gradativo. A lista de eventos é uma série de coisas que acontecerão, direcionando ao aspecto gradativo. Pense no **aspecto gradativo** como o que acontecerá se os jogadores não intervierem. Um bom evento possui de três a seis aspectos além do aspecto gradativo.

Quando a aventura começar, marque o primeiro aspecto e traga-o para a história. Este é o incidente inicial, a questão na qual os PJs se envolverão. Trata-se de um aspecto como qualquer outro — você e os jogadores podem forçar e invocar normalmente. Ele permanece em jogo até deixar de ser relevante; nesse momento é só riscá-lo da lista.

Sempre que a história sugerir que as coisas devem seguir para uma nova etapa, ou assim que a ação diminuir, verifique qual é o próximo aspecto e traga-o para a história. Agora ele passa a ser um aspecto que pode ser forçado e invocado, além de ser um novo elemento da história. Continue a fazer isso por quanto tempo for preciso. Acelere o ritmo da troca de aspectos caso os jogadores estejam distraídos ou não se envolvam, ou diminua o ritmo se estiverem muito engajados e proativos.

Se o aspecto gradativo for marcado, isso indica que as coisas estão ficando realmente feias. Isso normalmente indica que os vilões estão a um passo da vitória e que os jogadores precisam virar o jogo.

Se nem todos os aspectos gradativos tiverem sido riscados quando os PJs terminarem de resolver a situação, não é um problema! Isso significa que os PJs estavam envolvidos no jogo, foram incríveis e se saíram bem. Se todos os aspectos entrarem em jogo e as coisas acabarem mal para os PJs, tudo bem também! Os acontecimentos da história atual podem refletir na próxima, agora com novos desafios.

Eis um exemplo:
+ ***Explosões e Fogo!***
+ ***Uma Onda de Assassinatos***
+ ***Pânico Generalizado***
+ ***Sob Ameaça Terrorista***
+ ***Três Horas Para Explodir***
+ ***Cratera Fumegante***


------

- [« Forçando Aspectos Para Estruturar Aventuras](../forcando-aspectos-para-estruturar-aventuras/)
- [Nível de Poder »](../nivel-de-poder/)
