I"gm<ul>
  <li><a href="http://www.genesisoflegend.com/PDF/A_Spark_In_Fate_Core-Setting_Creation_System.pdf">Link Original</a></li>
  <li><a href="https://docs.google.com/document/d/1H8VytN1MgcOE0D9wNimepS5z96d1rtbxjzEW4HbxpBA/edit?usp=drive_web">Link Original da tradução</a></li>
</ul>

<table>
  <tbody>
    <tr>
      <td>Jason Pitre</td>
      <td>Escritor</td>
    </tr>
    <tr>
      <td>John Adamus</td>
      <td>Editor</td>
    </tr>
    <tr>
      <td>Mark Richardson</td>
      <td>Layout</td>
    </tr>
    <tr>
      <td>Paulo Guerche</td>
      <td>Tradução</td>
    </tr>
    <tr>
      <td>Fate Masters</td>
      <td>Apoio</td>
    </tr>
    <tr>
      <td>Fábio Silva</td>
      <td>Diagramação</td>
    </tr>
    <tr>
      <td>Fábio Costa</td>
      <td>Versão para Web/Markdown</td>
    </tr>
  </tbody>
</table>

<h2 id="nota">Nota</h2>

<p>Esta tradução foi feita à partir do arquivo A Spark In Fate Core, disponível na seção de downloads para Fate Básico do site da Evil Hat. O arquivo é gratuito e absolutamente nada do que está presente neste documento é de minha autoria, sendo de minha responsabilidade apenas a tradução deste rico material de criação de cenários para o público lusófono.</p>

<p>Atenciosamente,</p>

<p>Paulo.</p>

<!-- excerpt -->

<h2 id="sparks-em-fate-básico">Sparks em Fate Básico</h2>

<h3 id="o-que-é-isso">O que é isso?</h3>

<p>Este documento foi criado como uma meta expandida para a campanha de financiamento coletivo do Spark RPG. Se você gostar da construção coletiva de mundos apresentada aqui, considere dar uma olhada no jogo original - no qual eu apresento uma abordagem alternativa para a criação coletiva de campanhas.</p>

<p>Trate isso como um complemento ao capítulo 2 do Fate Básico. Ele foi feito para te dar um processo mais compreensível para a criação de mundos e provavelmente tomará a maior parte de sua primeira sessão.</p>

<h3 id="de-onde-isso-veio">De onde isso veio?</h3>

<p>O primeiro esboço dessa criação colaborativa de cenários veio de Rob Donohue e Fred Hicks, da Evil Hat Productions. A criação de fatos baseada em fichas veio de Fred, enquanto as raízes do processo de criação de cidades de Dresden Files é creditado ao Rob.
7
 Ruan Macklin contribuiu redesenhando a criação de cidades de  Dresden Files. Ryan e o fantástico Leonard Balsera tiveram as ideias do capítulo 2 do Fate básico, que serviram de base para esse artigo.</p>

<blockquote>
  <p><em>“O primeiro jogo de Fate vinha com um mapa onde eu nomeava os terrítórios mas não definia nada mais sobre eles.”</em></p>

  <p><strong><em>Fred Hicks</em></strong></p>
</blockquote>

<p>Enquanto Ryan e Leonard desenvolviam o Fate Básico, eu desenvolvia o meu próprio jogo com foco na criação colaborativa de cenários. Eu sempre fui apaixonado pela criação de mundos e escrevi um jogo para explorar isso. Juntando o sistema de Spark RPG e de Fate, temos essa nova abordagem.</p>

<p>Agora é sua vez de criar em cima do que criamos. Mal podemos esperar para ver o que vai surgir disso.</p>

<blockquote>
  <p><em>“Para mim, criar mundos é oferecer pontos de tensão nos quais os personagens possam se encaixar. Se você não tiver vários desses, você não tem um mundo; você tem uma masmorra, tavez com uma roupagem diferente</em></p>

  <p><strong><em>Ryan Mackiln</em></strong></p>
</blockquote>

<h2 id="o-que-faz-um-bom-jogo-de-fate">O que faz um bom jogo de Fate?</h2>

<p>Você pode usar o Fate para contar histórias em vários gêneros e com uma variedade de premissas. Não existe um cenário padrão: você e seu grupo o criarão. Os melhores jogos de Fate, no entanto, compartilham algumas ideias que consideramos servir melhor aos propósitos do jogo.</p>

<p>Não importa se você esteja falando de fantasia, ficção científica, super heróis ou programas policiais, Fate funciona melhor quando você conta histórias sobre gente <em>proativa</em>, <em>competente</em> e <em>dramática.</em></p>

<h3 id="proatividade">Proatividade</h3>

<p>Personagens em jogo de Fate devem ser proativos. Eles dedicam diversas habilidades à solução de problemas e não tem vergonha de usá-las. Eles não esperam que a solução para uma crise caia do céu - eles põem as cartas na mesa e investem suas energias, correm riscos e superam obstáculos para alcançar seus objetivos.</p>

<p>Isso não quer dizer que esses personagens não planejem ou sejam descuidados. Só que mesmo os mais pacientes entre eles eventualmente tomarão atitudes tangíveis e notáveis.</p>

<p>Qualquer jogo de Fate que você jogar deveria gerar oportunidades claras para que os personagens sejam proativos ao resolver seus problemas e de preferência com várias opções de como fazê-lo. Um jogo sobre bibliotecários passando o tempo entre tomos empoeirados não é Fate. Um jogo sobre <em>bibliotecários usando conhecimento esquecido para salvar o mundo</em> é.</p>

<h3 id="competência">Competência</h3>

<p>Personagens em um jogo de Fate são bons em fazer coisas. Eles não são palermas que sempre ficam ridículos quando tentam fazer algo - eles são habilidosos, talentosos ou treinados e capazes de fazer a diferença em seus mundos. São as pessoas certas para o trabalho e se envolvem em uma crise por terem boas chances de resolvê-la.</p>

<p>Isso não quer dizer que sempre terão sucesso ou que suas ações não gerem consequências inesperadas, só que quando falharem não vai ser por algum erro bobo ou falta de preparo.</p>

<p>Qualquer jogo de Fate deveria tratar os personagens como seres competentes, dignos dos riscos e desafios que virão. Um jogo sobre lixeiros forçados a combater super vilões só para levar a pior não é Fate. Um jogo sobre <em>lixeiros que se tornam um esquadrão foda de combate a supervilões</em> é.</p>

<h3 id="drama">Drama</h3>

<p>Personagens em um jogo de Fate levam vidas dramáticas. Para eles, as apostas são sempre altas - tanto em termos do que eles têm que enfrentar no mundo real quanto no que se passa naquele palmo entre uma orelha e outra. Como nós, eles tem problemas interpessoais e passam maus bocados lutando contra eles. Mesmo que o que se passe com eles possa ser muito maior em escopo do que nossas vidas, no final das contas ainda seremos capazes de nos ligar e simpatizar eles.</p>

<p>Isso não quer dizer que os personagens passem suas vidas num mar de desespero e dor, ou que qualquer coisinha em suas vidas seja o fim do mundo - quer dizer que suas vidas exigem escolhas difíceis com consequências que os acompanharão por muito tempo. Ou seja: essencialmente, eles são humanos.</p>

<p>Qualquer jogo de Fate deve ter o potencial e as oportunidades para drama ao redor e entre os personagens, dando chances para que os jogadores se relacionem com seus personagens e os reconheçam como criaturas vivas. Um jogo sobre aventureiros socando monstros cada vez mais numerosos e maiores não é Fate. Um jogo sobre <em>aventureiros lutando para levar vidas normais apesar de estarem destinados a combater o mal maior</em> é.</p>

<blockquote>
  <h3 id="quando-for-criar-seu-jogo"><strong>Quando for criar seu jogo:</strong></h3>

  <ol>
    <li>Comece listando suas <strong>mídias</strong> favoritas.</li>
    <li>Explique as diferentes <strong>inspirações</strong> para essas mídias.</li>
    <li>Use as explicações para <strong>descrever o gênero</strong>.</li>
    <li>Decida o quão épica ou pessoal será a <strong>escala</strong> de sua história.</li>
    <li><strong>Estabeleça fatos</strong> sobre o cenário.</li>
    <li><strong>Crie um título</strong> para focar sua visão.</li>
    <li>Crie uma <strong>lista de Sparks</strong> (questões em potencial) para o cenário.</li>
    <li><strong>Escolha problemas</strong> , pegando três das Sparks de sua lista.</li>
    <li>Crie <strong>rostos</strong> para esses problemas.</li>
    <li>Crie um <strong>lugar</strong> para cada Spark ainda não utilizada.</li>
  </ol>
</blockquote>

<h2 id="passo-1---liste-suas-mídias-favoritas">Passo 1 - <strong>Liste suas mídias favoritas</strong></h2>

<p>Se reúna em torno de uma mesa com o grupo e pergunte a cada um qual é sua mídia favorita. Pode ser um livro, um filme, um jogo, quadrinho, poema ou uma música. Tudo bem se os outros não conhecerem essa mídia, então tente ser único.</p>

<p>Escreva-as na ficha de criação de mundo, na sessão Mídias.</p>

<blockquote>
  <p>Amanda, Lucas , Clara e Renan sentam para conversar sobre o cenário:</p>
</blockquote>

<blockquote>
  <ul>
    <li>Amanda diz que sua série favorita é <em>Acerto de Contas</em>.</li>
    <li>Lucas, por outro lado, estava lendo alguns clássicos e sugere <em>Conan, o Bárbaro</em>.</li>
    <li>Clara decide jogar a série <em>Babylon 5</em> na mistura.</li>
    <li>Renan cita <em>Guerra dos Tronos</em> como uma mídia em potencial.</li>
  </ul>
</blockquote>

<h2 id="passo-2---reúna-inspirações">Passo 2 - <strong>Reúna inspirações</strong></h2>

<p>Agora todos devem explicar seus aspectos favoritos das mídias escolhidas Faça uma lista dessas explicações na sessão Inspirações. Cada um pode adicionar mais inspirações à lista se quiser.</p>

<p>Esse passo é basicamente sobre ter ideias e discutí-las. É aqui que vocês poderão juntar uma lista de ingredientes que gostariam de incluir no cenário.</p>

<blockquote>
  <p>Agora o grupo começa a falar sobre as coisas que os interessam nas mídias que escolheram:</p>
</blockquote>

<blockquote>
  <ul>
    <li>Amanda explica que ela ama a ideia de <strong>roubos heróicos</strong> em <em>Acerto de Contas</em>.</li>
    <li>Lucas é um fã das <strong>aventuras urbanas</strong> em <em>Conan</em>.</li>
    <li>Clara conta aos outros que a <strong>diversidade cultural cosmopolita</strong> de <em>Babylon 5</em> é interessante.</li>
    <li>Renan curte a <strong>magia sutil e oculta</strong> de <em>Guerra dos Tronos</em>.</li>
    <li>Amanda menciona o quanto <strong>espadas</strong> são maneiras.</li>
    <li>Renan comenta que <strong>crime organizado</strong> seria um obstáculo divertido.</li>
  </ul>
</blockquote>

<h2 id="passo-3---descreva-o-gênero">Passo 3 - <strong>Descreva o gênero</strong></h2>

<p>Em grupo, considerem todas as Inspirações e decidam um gênero para o cenário. Esses são alguns gêneros comuns que vocês podem escolher, embora não seja uma lista definitiva:</p>

<blockquote>
  <ul>
    <li>História alternativa</li>
    <li>Fantasia</li>
    <li>Moderno</li>
    <li>Romance</li>
    <li>Mistério</li>
    <li>Terror</li>
    <li>Sci-fi</li>
    <li>Super heróis</li>
  </ul>
</blockquote>

<p>Uma vez definido o gênero, expliquem o que faz o seu cenário diferente dos demais cenários do gênero. Crie um adjetivo ou pronome que descreva essas diferenças. Os melhores descritores são emocionais, culturais ou filosóficos.</p>

<p>Esse passo gera uma visão comum e interpreta suas inspirações e estabelece Fatos. Escreva o gênero e o descritor na ficha.</p>

<blockquote>
  <p>O grupo se decide rapidamente  por <strong>Espada e Magia</strong>. Tudo bem, Lucas fez um comentário sobre Romance ser interessante mas amanda o lembrou que espadas são maneiras. Depois vem o descritor. Clara mencionou que Espada e Magia num cenário <strong>Cosmopolitano</strong> seria um contraste bacana com os outros livros e filmes do gênero.</p>
</blockquote>

<h2 id="passo-4----determine-a-escala">Passo 4 -  <strong>Determine a escala</strong></h2>

<p>Decidam o quão épica ou pessoal sua história será.</p>

<p>O cenário pode ser pequeno ou imenso, mas o lugar onde acontecem suas histórias determina a escala do seu jogo. Em um jogo de escala pequena, personagens lidam com problemas em uma cidade ou região sem viajar grandes distâncias. Um jogo de maior escala envolve lidar com problemas que afetam um mundo, uma civilização ou até mesmo uma galáxia se o gênero que vocês escolheram comportar esse tipo de coisa.</p>

<p>Algumas vezes, um jogo de pequena escala crescerá - como você já deve ter visto em livros e séries que duram muito tempo.</p>

<blockquote>
  <p>Amanda gosta da pegada de <em>“cara e garota com espada”</em> e pensa que será fantástico manter o jogo numa escala pequena, viajando de cidade em cidade e lidando com os problemas locais - como a guilda dos ladrões ou os planos malignos do regente.</p>
</blockquote>

<h2 id="passo-5---estabeleça-fatos">Passo 5 - <strong>Estabeleça Fatos</strong></h2>

<p>Nesse passo vocês estabelecerão Fatos sobre o cenário. Cada Fato expressa duas Inspirações diferentes na ficha e é estabelecido perguntando algumas questões.</p>

<p>Todos na mesa devem pegar duas fichas. O mestre começa fazendo uma pergunta sobre o cenário. Questões devem se basear nas Inspirações previamente criadas e idealmente devem se focar em como duas inspirações específicas se relacionam.</p>

<p>Qualquer um com uma ficha pode colocá-la no meio da mesa e dizer que tem a resposta certa para aquela pergunta criando um Fato sobre o cenário. Os melhores Fatos são evocativos, concisos e específicos. Fique à vontade para criar alguns nomes interessantes para as pessoas, eventos ou facções, mas você não é obrigado a explicá-los. Todos os outros podem dar sugestões e elaborar esse Fato, mas a pessoa que pagou a ficha tem a palavra final.</p>

<p>Contanto que todos estejam bem com o Fato, escreva-o na ficha na sessão de Fatos. A pessoa que acabou de criá-lo faz uma pergunta e deixa que outra pessoa a responda. Continue fazendo questões e estabelecendo Fatos até que todos tenham gasto ambas as fichas. Quando isso acontecer, alguns temas vão surgir. Esse passo ajuda a expressar as singularidades de seu cenário e garante que o mundo esteja cheio de conteúdo pelo qual o grupo se interesse.</p>

<p>Leve em conta os membros mais quietos de um grupo. Esse processo é muito atraente e empolgante, o que pode levar algumas pessoas a dominar a conversa.</p>

<blockquote>
  <ul>
    <li>Amanda pergunta o porquê de a nobreza ter tanto poder. Lucas paga uma ficha e diz que eles controlam a guilda dos ferreiros.</li>
    <li>Lucas pergunta quais grupos étnicos estão acampados ao redor da cidade. Clara os chama de os Benari e os Narnu.</li>
    <li>Clara pergunta quem está corrompendo a nobreza, o que Renan responde nomeando o Culto da Serenidade.</li>
    <li>Renan pergunta o que distingue os membros da guilda dos ladrões e Lucas paga sua segunda ficha para descrever o ritual de escarificação.</li>
    <li>Lucas pergunta quais são essas estranhas magias em posse das Lâminas de Baland. Amanda responde que eles claramente tem roubado a vitalidade de suas vítimas.</li>
    <li>Amanda pergunta o porquê de as pessoas evitarem as ruas à noite, ao que Clara responde descrevendo os roedores de tamanho incomum que devoram os distraídos.</li>
    <li>Como Renan tem a última ficha, Clara pergunta a ele o nome da cidade e Renan a chama de <em>Greywall</em>.</li>
  </ul>
</blockquote>

<h2 id="passo-6---crie-um-título">Passo 6 - <strong>Crie um título</strong></h2>

<p>Nesse passo vocês pisam no freio e olham pro cenário como um todo. Considere todos os fatos e veja quais são as ideias comuns que surgem. Este passo ajudará vocês a discutirem todos os Fatos e entrar num acordo sobre de que se trata esse cenário.</p>

<p>Tente expressar isso com um título curto e evocativo. Sugira títulos entre uma e três palavras para o jogo e escolha o melhor. Vocês devem ser capazes de expressar o conceito principal de seu cenário declarando seu título, seguido pela descrição do gênero que surgiu no Passo 3.</p>

<p><strong>Título: Descritor de Gênero</strong></p>

<blockquote>
  <p>O grupo decide que o jogo se chamará <strong>Espadas da Noite: Espada e Feitiçaria Urbana.</strong></p>
</blockquote>

<h2 id="passo-7---crie-sparks">Passo 7 - <strong>Crie Sparks</strong></h2>

<p>Agora você tem uma compreensão geral da história de seu jogo. Nesse passo vocês criarão Sparks, os incontáveis problemas em potencial que poderão explodir a qualquer momento. Esses problemas são crises sociais, políticas e econômicas abrangentes que podem mudar ou destruir uma sociedade como um todo. Talvez haja um grande conflito cultural, um disastre horrível ou uma mudança demográfica. Regimes corruptos, crime organizado, uma nova praga ou uma nova tecnologia - todos tem o potencial para botar os protagonistas em ação.</p>

<p>Criar essas Sparks é uma responsabilidade imensa. Nesse passo, cada jogador tem a chance de criar duas Sparks. A seguir, o mestre escolherá algumas dessas Sparks para criar Problemas que afetarão diretamente o cenário no início da história.</p>

<p>Rode a mesa dando a cada jogador a chance de criar suas próprias sparks. Quando um jogador propuser uma Spark, as outras pessoas da mesa darão sentido a ela - caso faça sentido para o cenário e o grupo esteja confortável com o assunto. Quando todos concordarem com uma proposta, coloque-a na sessão de Sparks.</p>

<p>Uma vez que cada jogador tiver contribuído, rode novamente a mesa para que todos escrevam sua segunda Spark. Você terá 4, 6 ou 8 Sparks, dependendo do número de jogadores.</p>

<p>Por estarem em três jogadores (Lucas, Clara e Renan), temos um total de 6 Sparks.</p>

<blockquote>
  <ul>
    <li>Lucas: A Tríade da Cicatriz domina o submundo do crime.</li>
    <li>Clara: Guerra entre os Benari e os Narnu.</li>
    <li>Renan: Profecia da Destruição Que Virá.</li>
    <li>Lucas: Nobres estão desaparecendo durante a noite.</li>
    <li>Clara: O mestre da Guilda dos Alquimistas descobre o Fogo de Dragão.</li>
    <li>Renan: O primeiro Benari se junta à Guilda dos Ferreiros.</li>
  </ul>
</blockquote>

<h2 id="passo-8---escolha-os-problemas">Passo 8 - <strong>Escolha os problemas</strong></h2>

<p>Nesse passo o mestre olha para a lista de Sparks e escolhe três delas para se focar no começo do jogo. Elas se tornam problemas que afetarão diretamente a sociedade e os personagens. Cada Problema se tornará um Aspecto de Jogo, representando a influência desses problemas.</p>

<p>Primeiro, escolha uma Spark como um <strong>Problema Legado</strong>. Esse já foi o problema dominante do cenário e ainda influencia os eventos atuais de certo modo. Talvez uma raça alienígena tenha se extinguido misteriosamente há 50 mil anos ou os humanos e elfos se aliaram em uma guerra contra o deus das sombras. Mesmo que a importância dos eventos esteja se esvaindo, deixaram uma marca inapagável no mundo.</p>

<p>Então, escolha uma Spark para ser o <strong>Problema Atual</strong>. Esses são os problemas que existem atualmente e ameaçam acabar com a ordem existente. Quase todos os personagens são moldados por esse problema, seja lutando para ajudar ou impendindo que aconteça. Cada protagonista precisa se ligar ao problema atual de algum modo e ele provavelmente dominará o primeiro arco de qualquer campanha. Talvez você esteja tentando construir uma aliança unificada entre incontáveis raças alienígenas ou um dragão tenha dominado a fortaleza da montanha.</p>

<p>Por fim, escolha uma das Sparks como um <strong>Problema Futuro</strong>. Esses são os problemas e ameaças no horizonte. Eles são as sementes dos conflitos futuros e das perguntas não respondidas que vocês enfrentarão. Talvez um inimigo antigo e poderoso retorne para conquistar sua civilização  ou uma nova religião se espalhe pelo império.</p>

<p>Uma vez escolhidos os três problemas, trabalhem juntos para criar um Aspecto de Jogo para cada um deles. Tente imaginar situações nas quais os problemas podem ser forçados contra os protagonistas ou invocados pelos inimigos. Escreva-os na ficha na sessão Problemas.</p>

<p>O Aspecto para o Problema Atual se fará sentir em todas as cenas do jogo e terá forte influência sobre a narrativa. No começo de cada cena, o mestre também escolhe um dos dois outros Aspectos de Jogo para influenciá-la. Isso quer dizer que cada cena apontará para o Problema Futuro ou para o passado com o Problema Legado. Sempre haverá um Aspecto que não se aplica em todas as cenas.</p>

<p>Esses problemas mudarão e se desenvolverão enquanto os jogadores interagem com o mundo. Cada vez que o grupo alcançar um Marco Maior, cada Problema mudará:</p>

<blockquote>
  <ul>
    <li>O Problema Legado desaparecerá na história e o Aspecto será removido.</li>
    <li>O Problema Atual se tornará o novo Problema Legado, já que sua importância diminui.</li>
    <li>O Problema Futuro se tornará o novo Problema Atual, chamando a atenção dos jogadores.</li>
    <li>O mestre escolherá uma nova Spark da lista para se tornar o próximo Problema Futuro.</li>
  </ul>
</blockquote>

<blockquote>
  <ul>
    <li>Amanda decide que o Problema Legado será a segunda Spark, o Problema Atual será a primeira Spark e o Problema Futuro será a terceira.</li>
    <li>Para o Problema Legado, Renan sugere que o aspecto seja <strong>A Guerra dos Ossos</strong> , o título para o conflito entre os Benari e os Narnu. Amanda concorda e anota.</li>
    <li>Para o Problema Atual, eles decidem que <strong>Ruas Feridas</strong> refletiria o domínio da Tríade da Cicatriz sobre o submundo do crime e sua influência negativa sobre a cidade.</li>
    <li>O problema futuro inspirou vários Aspectos em potencial. Lucas sugere que o grupo use <strong>Destruiçããããão!</strong> , enquanto Clara pensa em <strong>A Profecia Final</strong>. Amanda sugere que ao invés disso eles fiquem com <strong>A Profecia que Virá</strong> e os outros concordam.</li>
  </ul>
</blockquote>

<h2 id="passo-9---crie-rostos">Passo 9 - <strong>Crie Rostos</strong></h2>

<p>Aspectos são coisas inerentemente nebulosas, cheias de significados em potencial. Nesse passo, crie alguns rostos, personagens que expressem esses aspectos na história de uma maneira mais concreta.</p>

<p>Cada Problema é representado por dois Rostos diferentes; um definido por sua ajuda ao Aspecto e um que luta contra ele. Se o aspecto for “tem a razão quem tem o poder”, então seus dois Rostos podem ser um chefão do crime impiedoso e um juiz imparcial. Ambos os personagens expressam o Problema de maneiras bem diferentes.</p>

<p>Mesmo que esses Rostos sejam mecanicamente importantes, não se preocupe com seus dados. Eles não precisam ser uma só pessoa; um par de tenentes ou uma horda de servos serviriam ao mesmo propósito desde que ajudem ou atrapalhem em relação ao Problema.</p>

<p>Rode a mesa dando a cada jogador a chance de definir pelo menos um dos Rostos. Cada pessoa pode criar um nome evocativo para cada Rosto, seguido de uma pequena frase descritiva. Elas também podem escolher uma habilidade que se destaca nesse personagem baseadas no tipo de atividade que esse Rosto realiza. Já que é o mestre quem jogará com esses Rostos, ele tem o direito de vetá-los ou reinterpretá-los como for melhor para a história.</p>

<p>Quando você tiver seus seis Rostos, vá para o próximo passo.</p>

<p>Agora o grupo pode criar vários Rostos que representarão os problemas.</p>

<blockquote>
  <ul>
    <li>Lucas cria Hugo, o Generoso, um tenente da Tríade. Ele contribui para o aspecto de Ruas Feridas, comandando as operações locais com crueldade. Sua habilidade chave é Provocar.</li>
    <li>Clara cria O Primarca, líder do Culto da Serenidade cuja identidade é um mistério. Ele fala da Destruição que Virá e realizou a Profecia. Sua habilidade chave é Conhecimento.</li>
    <li>Renan cria um personagem simpático, Kale Westal, que agirá como um contrapeso para Hugo. Ela não é coagida pelas extorsões de Hugo e provavelmente sofrerá um “acidente”. Ela luta contra o aspecto Ruas Feridas e sua habilidade chave é Contatos.</li>
    <li>Lucas, Clara e Renan criam as outras três faces do mesmo jeito.</li>
  </ul>
</blockquote>

<h2 id="passo-10---crie-lugares">Passo 10 - <strong>Crie lugares</strong></h2>

<p>Tudo no jogo precisa acontecer em algum lugar e esse passo te ajuda a criar uma variedade de diferentes locais com maiores significados.</p>

<p>Cada lugar representa uma das Sparks geradas no passo 6 que ainda não são um problema. Eles representam os diferentes problemas em potencial que podem surgir mais tarde.</p>

<p>Rode a mesa, dando a cada jogador a chance de declarar um desses Lugares. Cada pessoa pode criar um nome evocativo para esses lugares, seguido de uma frase descritiva. Com o que esse lugar se parece e como ele soa? Algum cheiro ou gosto estranho? Quão movimentado ou quieto ele é?</p>

<p>Eles também podem criar um Aspecto Situacional para aquele local baseando-se nas características físicas do lugar (Becos escuros, Lago congelado) ou em detalhes contextuais (Cidadãos desconfiados, Terrores ocultos).</p>

<p>Mais uma vez, o mestre tem o poder de veto em qualquer lugar ou aspecto que sinta fora da linha.</p>

<p>Das seis Sparks originais, três foram usadas para criar Problemas e as três restantes definem os Lugares.</p>

<blockquote>
  <ul>
    <li>Baseando-se na quarta Spark, Lucas cria as Ruas de Ardósia. Os nobres usam esses caminhos secretos nos telhados da cidade para seus encontros noturnos secretos. As Ruas tem o aspecto Traiçoeiras, sendo que muitos nobres já caíram desses telhados à noite.</li>
    <li>Clara decide que o Quarteirão dos Alquimistas, cheio de poções misteriosas e reagentes perigosos seria ideal para a quinta Spark. O aspecto para essa vizinhança é Exótica, cheia de elixires estranhos e comerciantes estrangeiros.</li>
    <li>Renan cria o último lugar se baseando na sexta Spark. Os Cortiços Benari são o acampamento ao redor dos muros da cidade, onde os Benari mantém sua comunidade. São Pobres e Sujos devido tanto à Guerra dos Ossos quanto ao preconceito dos locais.</li>
  </ul>
</blockquote>

<h2 id="o-que-saiu-disso"><strong>O que saiu disso?</strong></h2>

<p>Graças a esse processo, o mestre terá vários recursos à disposição para desenrrolar o jogo com o grupo.</p>

<blockquote>
  <ul>
    <li>Um Problema Futuro com um Aspecto.</li>
    <li>Um Problema atual com um Aspecto.</li>
    <li>Um Problema Legado com um Aspecto.</li>
    <li>6 Rostos que representam os problemas maiores.</li>
    <li>4, 6 ou 8 Sparks - problemas em potencial que podem surgir em arcos posteriores da campanha.</li>
    <li>1, 3 ou 5 Lugares que expressarão essas Sparks durante o jogo.</li>
  </ul>
</blockquote>

<h3 id="criando-protagonistas"><strong>Criando protagonistas</strong></h3>

<p>Com tudo em seu lugar, vocês podem passar para a criação de personagens, onde cada jogador criará um Protagonista. Considere o modo como o Problema Legado afetou seus personagens durante suas juventudes e como eles tem lidado com o Problema Atual no dia a dia. Cada protagonista deve ter alguma conexão com os Rostos e Lugares  para ligá-los ao mundo em um nível pessoal. Se for difícil relacioná-los ao cenário, pode ser necessário repensar seus protagonistas ou revisar o jogo para que ambos se encaixem melhor.</p>

<p>Quando você estiver criando os personagens, descobrirá um pouco mais sobre o cenário enquanto as pessoas falam sobre quem seus personagens conhecem e o que fazem. Se algo interessante surgir, adicione às notas de criação de jogo antes de prosseguir com o jogo.</p>

<h3 id="habilidades-e-seu-cenário"><strong>Habilidades e seu cenário</strong></h3>

<p>Uma grande parte de seus cenários é o que as pessoas podem fazer nele. As várias habilidades em Habilidades e Façanhas cobrem muitas situações, mas você vai querer dar uma olhada nelas para garantir que todas se apliquem ao cenário ou que você adicione as habilidades necessárias.</p>

<p>Adicionar uma habilidade é tratado em mais detalhes no capítulo de Extras.</p>

:ET