---
title: Lutar
layout: default
---

### Lutar

A perícia Lutar cobre todas as formas de combate corpo a corpo (em outras palavras, entre indivíduos que estejam na mesma zona), tanto desarmado quanto armado. Para armas de longo alcance, veja Atirar.

- `O`{: .fate_font} **Superar:** Você pode Em geral você não usa Lutar f ora de um conﬂito, portanto ela não é muito usada para se livrar de obstáculos. Você pode usá-la para demonstrar seu poder marcial ou para participar em algum tipo de competição esportiva.
- `C`{: .fate_font} **Criar Vantagem:** Você provavelmente usará Lutar para criar a maior parte das vantagens em um conﬂito físico. Qualquer manobra especial pode ser representada em vantagens: um golpe atordoador, um "golpe sujo", desarmar, entre outros. Você pode até usar Lutar para analisar o estilo de outro lutador, detectando fraquezas que possam ser exploradas em sua técnica.
- `A`{: .fate_font} **Atacar:** Autoexplicativo. Você pode realizar ataques físicos com Lutar. Lembre-se que serve apenas para ataques corpo a corpo, então você deve estar na mesma zona que o seu oponente.
- `D`{: .fate_font} **Defender:** Use Lutar para se defender de ataques ou vantagens criadas contra você com a perícia Lutar, assim como de qualquer ação que possa ser interrompida através da violência. Não é possível usar esta perícia para se defender de ataques de Atirar, a menos que cenário seja fantasioso o bastante para permitir agarrar projéteis no ar ou usar espadas laser para deﬂetir tiros de energia.

#### Façanhas Para Lutar

-  **Mão Pesada:** Quando é bem-sucedido com estilo em um ataque usando Lutar e escolher reduzir o resultado para obter um impulso, você ganha um aspecto de situação com uma invocação grátis ao invés do impulso.
-  **Arma Reserva:** Sempre que alguém estiver prestes a adicionar o aspecto de situação ***Desarmado*** em você ou similar, gaste um ponto de destino para declarar que você possui uma segunda arma. Ao invés de ganhar o aspecto de situação, seu oponente ganha um impulso, representando o momento de distração durante a troca de arma.
-  **Golpe Matador:** Uma vez por cena, ao causar uma consequência em um oponente, você pode gastar um ponto de destino para aumentar a severidade dela (então suave se torna moderada, moderada se torna severa). Se seu oponente receberia uma consequência severa, ele recebe a consequência severa *mais* uma segunda consequência ou tem que sair da luta.

------

### A(s) Arte(s) da Luta

É esperado que a maioria das partidas jogadas em *Fate* tenha uma boa quantidade de ação e conﬂitos físicos. É um ponto muito importante, como na perícia Ofícios, já que as perícias de combate escolhidas dizem muito do estilo do jogo.

Nos exemplos temos Lutar e Atirar como perícias separadas, o que nos dá uma divisão básica sem sermos muito detalhistas. É fácil perceber que isso sugere que um combate usando armas e um combate desarmado são praticamente a mesma coisa – não há vantagem inerente de um para outro. É bastante comum criar uma divisão entre luta armada e desarmada — em categorias como Punhos e Armas, por exemplo.

Você poderia especializar isso ainda mais se quisesse que cada tipo de arma tenha sua própria perícia (espadas, lanças, machados, armas de plasma, carabinas, etc.), mas é bom reiterar mais uma vez que você não se empolgue muito nisso a não ser que seja realmente importante para seu cenário. O uso de especializações em armas também pode ser encontrado na parte [*Extras*](../extras/).

------

- [« Investigar](../investigar/)
- [Ofícios »](../oficios/)
