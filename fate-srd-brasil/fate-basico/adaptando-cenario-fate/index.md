---
title: Adaptando O Cenário Ao Fate
layout: default
---

## Adaptando O Cenário Ao Fate

**Decida como é o mundo ao redor dos protagonistas.**

Você provavelmente está familiarizado com a ideia de cenário, mas de forma resumida se trata de tudo com o que os personagens podem interagir como pessoas, organizações e instituições, tecnologia, fenômenos estranhos e mistérios (intrigas, lendas históricas ou cósmicas, etc.). São essas coisas com as quais os personagens querem ou são forçados a interagir. Podem também ser coisas que os ajudarão ou então que precisarão impedir.

Ao utilizar um cenário já existente como a de um filme, série ou outro jogo, muitos desses pontos já estarão resolvidos. Claro que você provavelmente desejará acrescentar seus próprios detalhes: novas organizações ou mistérios diferentes a serem solucionados.

Criar um cenário requer muito trabalho. Não é o objetivo deste capítulo ensinar como criar criá-los: assumimos que você sabe como fazer isso se é o que quer fazer (sem mencionar que vivemos no mundo da mídia; veja [**tvtropes.org**](http://tvtropes.org) se não acredita). Um conselho, não tente criar muita coisa de antemão. Como verá no decorrer deste capítulo, muitas ideias surgirão já durante o processo de criação do jogo e dos personagens, então os detalhes virão com o tempo.

> Amanda, Léo, Michel e Maira sentam para conversar sobre a ambientação. Todos querem uma partida de muita fantasia, com coisas espaciais, como um filme que Léo e Maira viram recentemente. Então ela escreve “uma garota com partes robóticas”. Léo prefere algo mais engraçado, então decide anotar “um homem-lagosta”. O mundo é “bem espacial, exagerado. Alienígenas, naves espaciais e trabalhos perigosos por todo o lugar. ”
>
> Michel sugere “um cara que não usa armas”, diferenciando-o dos outros personagens. Ele também quer jogar com alguém que seja mais estudioso (para contrastar). Todos concordam e seguem para o próximo passo.

- [« Estruturando Seu Jogo](../estruturando-seu-jogo/)
- [A Grandeza do Jogo »](../grandeza-do-jogo/)
