---
title: Durante a Criação do Jogo
layout: default
---

## Durante a Criação do Jogo

Como já foi dito em [*Estruturando o Jogo*](../estruturando-o-jogo/), criar ou escolher um cenário é um esforço colaborativo entre você e os jogadores. Nesse sentido, a melhor coisa que se pode fazer durante o processo de criação do jogo enquanto narrador, é estar aberto a novas ideias e ser generoso consigo mesmo, assim como todo mundo. Brinque um pouco e expanda as sugestões que outros oferecerem. Seus jogadores darão mais de si no jogo se sentirem que estão contribuindo na construção do mesmo.

É claro que, se todos aceitarem, nada te impede de chegar com uma ideia clara do que exatamente você quer jogar. “Certo, esse será um jogo sobre a Guerra Fria nos anos 60, só que com máquinas a vapor e robôs gigantes.” Apenas tenha certeza de que todos estão de acordo se você seguir por esse caminho. Se houver sequer um jogador que não goste da ideia e não estiver interessado em *tentar* jogá-la, isso pode ser um problema sério.

### Bem Distante vs Exatamente Assim

Falando em robôs gigantes a vapor na União Soviética de 1960, é uma boa ideia considerar o quão distante da realidade você quer chegar. Ter ideias de conceitos de personagem é bastante divertido, mas se for algo difícil demais para se pensar os jogadores podem ter dificuldades em encaixar as próprias ideias no seu conceito de jogo. Isso difere bastante de grupo para grupo (e de jogador para jogador), então não há uma fórmula mágica. Apenas tenha consciência de que qualquer distanciamento do familiar – seja isso o mundo real ou gêneros narrativos bem conhecidos – pode se tornar um obstáculo na criatividade dos jogadores. Responda todas as perguntas com antecedência e certifique-se de que todas as dúvidas possíveis foram resolvidas.

O oposto seria definir o jogo em nosso mundo real, talvez com algumas poucas ramificações que você pode explorar à medida que o jogo avança. A forma mais fácil de fazer isso é usando uma época e local com os quais esteja familiarizado e então criar as exceções do cenário. Por exemplo, “É como na Londres dos dias atuais, mas robôs são comuns” ou “É na Los Angeles do pós guerra, mas alguns veteranos possuem superpoderes"

### De Cima pra Baixo vs De Baixo pra Cima

Uma questão importante é quão amplo seu jogo será. Alguns gostam de começar com a ideia geral e especificar os detalhes, enquanto outros preferem partir do aqui e agora e desenvolver o principal conforme o jogo se desenvolve. Essas abordagens costumam ser chamadas de "de cima pra baixo" e "de baixo pra cima", respectivamente. Uma não é melhor que a outra. Ambas têm seus prós e contras.

Na abordagem de cima pra baixo, você determinará a maior parte do cenário com antecedência – coisas como quem são os vilões, os lugares importantes da cidade, a origem de organizações importantes e assim por diante. Isso tem a vantagem de formar uma visão clara de como tudo se encaixa. Por exemplo, você decide que o Reino de Talua está em constante conﬂito entre cinco casas poderosas que disputam o controle, assim você sabe que de uma forma ou de outra todos têm ligações com algumas dessas casas – e se alguém não possuir, deverá ser por uma razão muito boa.

A desvantagem, claro, é que a menos que você esteja usando uma ambientação de um filme, série, livro, jogo eletrônico ou qualquer outra, normalmente terá bastante trabalho pela frente. Também exige que os jogadores tenham algum entendimento da coisa, o que pode ser desencorajador em alguns casos. Mas se todos estiverem empolgados, será uma partida bastante agradável e recompensadora.

Se escolher ir de baixo pra cima, você focará no que é mais importante para os PJs no momento. Isso pode ser qualquer coisa desde alguns PdNs notáveis na cidade ao nome do sujeito que trabalha na joalheria. A partir disso o grupo cria os detalhes da história à medida que a partida avança. Não há necessidade de uma ideia sobre como as coisas se encaixam no mundo, porque todos inventarão conforme a história avança. O mundo vai brotando a partir do ponto de partida.

Uma desvantagem possível é que exige um pouco de improvisação e pensamento rápido. Isso vale para todos na mesa, Narradores e jogadores. Para você, o Narrador, pode não ser grande coisa – narrar uma partida quase sempre envolve um pouco de liberdade criativa – mas nem todos os jogadores estão preparados para esse tipo de responsabilidade. Ainda mais se seus jogadores gostam de entrar em seus personagens e ver o mundo do jogo através de seus olhos, eles podem se incomodar com as quebrar de ponto de vista, como quando criarem o nome de um machado encantado que acabaram de encontrar ou contarem a você o que aconteceu com o último chefe da CIA.

*Fate* pode lidar com ambas as formas, mas o sistema de apoio a contribuição dos jogadores na narrativa, na forma de aspectos e detalhes da história, realmente valoriza o método de baixo pra cima. Se você já gosta de jogar assim, ótimo! Se não, sem problemas - mas procure experimentar algum dia.

### Pequena Escala vs Grande Escala

Isso já foi discutido no capítulo [*Estruturando o Jogo*](../estruturando-o-jogo/), mas vale a pena dedicar mais algumas palavras sobre o assunto.

Como foi dito no capítulo mencionado, histórias em pequena escala se concentram em eventos que ocorrem ao redor dos PJs, e provavelmente ocorrerão em uma área geográfica limitada. Histórias de grande escala são o oposto: contos épicos que se espalham por nações, planetas ou galáxias com consequências que abalam mundos. Ambos estilos podem ser bastante divertidos – ganhar o título de Imperador da Galáxia pode ser tão recompensador quanto ganhar a mão da garota mais bonita do vilarejo.

No entanto, não seja tolo ao pensar que ambos os estilos só funcionam separadamente. Aqui vão algumas ideias de combinações:

-  **Começa Simples e Cresce:** Esse é o tipo de história clássica do “herói fracote” que não possui ambições de glória. Pense em Luke Skywalker em *Star Wars: Uma Nova Esperança*. Ele começa como um caipira, participando de corridas de T-16 e arranjando problemas na Estação Tosche. Então uma dupla de dróides entra em sua vida e injeta um pequeno mistério: Quem é esse Obi-Wan Kenobi? Antes que possa perceber, ele está convivendo com traficantes, resgatando uma princesa e iniciando um golpe para a rebelião. Isso é um exemplo clássico de uma história em pequena escala que cresce para uma grande escala.
-  **Altos e Baixos** Aqui você alterna entre pequena e grande escala, usando a última quase como um respiro. Normalmente, as histórias em grande escala vão lidar com questões de estado, a conquista de planetas, o banimento de seres inimagináveis e coisas assim, enquanto histórias de pequena escala terão uma natureza pessoal, com pouca ou nenhuma conexão com eventos que fazem a terra tremer. Por exemplo, você pode gastar uma sessão ou duas lutando contra o Grande Império para então mudar o foco para um personagem que busca reencontrar o pai ou que vai ao auxílio de um amigo necessitado. As sessões de pequena escala servem como um descanso entre as aventuras de ação épica e dão aos jogadores a chance de mergulhar em cantos inexplorados de seus personagens. Além disso, se você *desejar* conectar as histórias pequenas às de grande escala mais para frente, você pode — o resultado será ainda mais gratificante para os jogadores.

### Extras: São Necessários?

Sua campanha exige coisas como superpoderes, magia, parafernálias de alta tecnologia ou outras coisas que vão além do mundano? De qualquer forma, é bom decidir isso agora, antes do jogo começar. Veja o capítulo [*Extras*](../extras/) para mais informações e como usar esse tipo de coisa em seu jogo.

- [« Conduzindo o Jogo](../conduzindo-o-jogo/)
- [Durante o Jogo »](../durante-o-jogo/)

