---
title: Façanhas e Recarga
layout: default
---

## Façanhas e Recarga

**Escolha ou crie de três a cinco façanhas. Determine os pontos de destino iniciais.**

As façanhas alteram o funcionamento das perícias de seu personagem. O capítulo [*Perícias e Façanhas*](../pericias-e-facanhas/) explica melhor como escolher e criar façanhas.

Você tem três façanhas grátis e você pode pegar outras ao custo de baixar sua recarga em um para cada nova façanha (a ideia é: quanto mais truques legais você tiver, mais precisa aceitar forçamentos para ganhar pontos de destino). Pensar nas façanhas pode demorar um pouco, então você pode criar uma agora e as demais durante o jogo.

> Maira decide pegar a façanha Mestre em Combate como uma de suas façanhas gratuitas: ela recebe +2 nas rolagens de Lutar quando criar vantagem sobre um oponente, desde que o oponente tenha um estilo de luta ou fraqueza que ela possa explorar.
>
> Para suas façanhas restantes ela escolhe Especialista em Sistemas e Senso de Perigo. Você pode vê-los escritos na sua ficha de personagem.

### Ajustando a Recarga

Um personagem em *Fate* começa com recarga 3. Isso indica que ele começa cada sessão com aos menos 3 pontos de destino.

Se você pegou quatro façanhas, sua recarga será 2. Se você pegou cinco façanhas, sua recarga será 1.

**Nota:** Alguns jogos de *Fate* poderão alterar esse valor. Independentemente de como as façanhas funcionarão no seu jogo, não é permitido possuir uma recarga inferior a 1.

------

Você pode ajustar esses valores se desejar oferecer mais façanhas grátis se quiser que os PJs possam fazer muitas coisas legais e receber vantagens especiais. É também possível alterar o valor padrão da Recarga – valores altos indicam que os PJs não precisarão forçar aspectos para ganhar pontos de destino (pense em quadrinhos de super-heróis) enquanto valores baixos indicam que eles irão se apertar e entrar em problemas constantemente (pense em *Duro de Matar*). Também, quanto mais alto a recarga, mais fácil para os jogadores comprarem façanhas.

------

- [« Perícias](../pericias/)
- [Estresse e Consequências »](../estresse-e-consequencias/)
