I"t<h3 id="percepção">Percepção</h3>

<p>A perícia Percepção trata justamente disso - perceber as coisas. Ela anda lado a lado com Investigar, representando a percepção geral do personagem, sua habilidade em notar detalhes rapidamente e outros poderes de observação. Normalmente, ao usar Percepção, o resultado será mais rápido que Investigar, portanto os detalhes são mais superficiais, mas você também não precisa de tanto esforço para encontrá-los.</p>

<ul>
  <li><code class="language-plaintext fate_font highlighter-rouge">O</code> <strong>Superar:</strong> Percepção quase não é usada para superar obstáculos, mas é usada como uma reação: perceber algo em uma cena, ouvir algum som fraco, notar a arma escondida na cintura do adversário.
Perceba que isso não é uma desculpa para o Narrador pedir rolagens de Percepção a torto e a direito para ver quão observadores os jogadores são; isso é entediante. Ao invés disso, peça testes de Percepção quando ser bem sucedido resultaria em algo interessante e falhar, também.</li>
  <li><code class="language-plaintext fate_font highlighter-rouge">C</code> <strong>Criar Vantagem:</strong> Você usa Percepção para criar aspectos baseados na observação direta – procurar em uma sala por aspectos que se destacam, encontrar uma rota de fuga em um prédio abandonado, notar alguém em meio a uma multidão, etc. Ao observar pessoas, Percepção pode mostrar o que está acontecendo com elas <em>externamente</em>; para notar mudanças internas, veja Empatia. Você também pode usar Percepção para declarar que seu personagem encontra algo que poderá ser utilizado a seu favor em alguma situação, como uma <strong><em>Rota de Fuga Conveniente</em></strong> quando está tentando fugir de um prédio ou uma <strong><em>Fraqueza Sutil</em></strong> na linha de defesa inimiga. Por exemplo, se você estiver em uma briga de bar, poderá realizar uma rolagem de Percepção para dizer que há uma poça no chão, bem próxima aos pés de seu oponente e que poderia fazê-lo escorregar.</li>
  <li><code class="language-plaintext fate_font highlighter-rouge">A</code> <strong>Atacar:</strong>Esta perícia não é utilizada em ataques.</li>
  <li><code class="language-plaintext fate_font highlighter-rouge">D</code> <strong>Defender:</strong> Você pode usar Percepção para se defender do uso de Furtividade contra você ou para descobrir se está sendo observado.</li>
</ul>

<h4 id="façanhas-para-percepção">Façanhas Para Percepção</h4>

<ul>
  <li><strong>Perigo Iminente:</strong> Você possui uma capacidade sobrenatural para detectar o perigo. Sua perícia Percepção funciona normalmente para condições como camuﬂagem total, escuridão ou outros empecilhos sensoriais em situações onde alguém ou algo tem a intenção de prejudicar você.</li>
  <li><strong>Leitura Corporal:</strong> Você pode usar Percepção no lugar de Empatia para aprender sobre os aspectos de alguém através da observação.</li>
  <li>
    <p><strong>Reação Rápida:</strong> Você pode usar Percepção no lugar de Atirar para realizar tiros rápidos que não envolvam uma grande necessidade de precisão na pontaria. No entanto, por estar realizando uma ação instintiva, você não tem a capacidade de identificar seu alvo quando usar essa façanha. Então, por exemplo, você pode ser capaz de atirar em alguém que está se movendo por entre os arbustos com essa façanha, mas você não será capaz de dizer se é um amigo ou inimigo antes de puxar o gatilho. Escolha com cuidado!</p>
  </li>
  <li><a href="../oficios/">« Ofícios</a></li>
  <li><a href="../provocar/">Provocar »</a></li>
</ul>
:ET