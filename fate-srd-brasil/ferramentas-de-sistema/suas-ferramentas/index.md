---
title: Suas Ferramentas
layout: default
---


## Suas Ferramentas

A seguir você encontra uma boa quantidade de componentes incompltos para sistemas de magia. Alguns são maneiras de criar poderes, outros são efeitos em potencial e resoluções. Desmanche-os e use suas partes, junte-os ou baseie-se neles para criar seu próprio sistema.

### Limites

#### Canalização

Para magias baseadas em perícias, adicione uma perícia para “Canalizar”. Quando desejar realizar algo mágico, você usa a perícia canalizadora para invocar o poder e utilizá-lo (esperamos). Para fazer isso, use uma ação de criar vantagem. Nesse caso, a vantagem que está tentando criar é um aspecto de **_Poder Invocado_**. Quando chegar a hora de conjurar a magia — em sua próxima ação, presume-se —, faça uma rolagem da perícia com a dificuldade Medíocre (+0), mas use quaisquer bônus obtidos pela ação de criar vantagem — então geralmente você receberá +2, +4 ou +6 por invocações gratuitas acumuladas e possivelmente pagando mais pontos de destino. Até agora, tudo ótimo — gere mana, gere um efeito. Agora, se quiser brincar mais um pouco:

- Se quiser lançar o feitiço _em uma ação_ , então precisará usar um     aspecto — quer de graça, quer gastando pontos de destino – sem     ganhar o bônus de +2. Isso faz com que a invocação rápida seja     bastante instável. Pode haver uma façanha que permita realizar a     magia de forma rápida e gratuita.

- A dificuldade para canalizar é Medíocre (+0) e a dificuldade real     da conjuração dependerá da magia. Em todo caso, um resultado     inferior a Regular significa que o poder saiu do controle. O perso-     nagem recebe estresse mental igual à diferença entre a rolagem e 0. 
- Regra opcional (Esgotamento): O conjurador pode optar por receber uma consequência previamente como parte da rolagem. Neste     caso, a caixa de consequência é marcada e o conjurador recebe um    bônus igual ao estresse que aquela consequência normalmente    absorveria.
- Outra regra opcional: Se quiser que o risco esteja na canalização    e não na conjuração, faça o seguinte. Peça que jogador que está    canalizando declare um nível de sucesso, algo entre Razoável (+2)    e Épico (+7), e então que faça uma jogada de superar contra a    dificuldade declarada. Se a rolagem falhar, ele recebe estresse igual    à diferença entre a rolagem e a dificuldade declarada. Se a rolagem    for um sucesso, então ele usará o nível de sucesso para a rolagem    da invocação. A regra sobre precisar de um aspecto ou façanha para    realizar conjurações rápidas ainda é válida, então ele terá que gerar    um impulso, comprar uma façanha ou gastar um ponto de destino para tal.

#### Pontos de Mana

Para um sistema que exige pontos de destino para realizar algo mágico, altere as regras de recarga. Cada vez que a recarga for reduzida em um, conceda um ponto de mana (PM) ao personagem como compensação. PMs são recuperados da mesma forma que pontos de destino e podem ser usados para alimentar magias ou aprimorar perícias mágicas, mas não podem mais ser usados como pontos de destino.

Aspectos “mágicos” podem gerar PMs em vez de pontos de destino quando usados como limitações ritualísticas. O que são essas limitações exatamente depende da natureza da magia, mas podem incluir coisas como orações diárias ou renunciar o uso de armaduras.

Se o sistema de magia usa mais pontos de mana, é fácil alterar a conversão de pontos de recarga — um único ponto de recarga pode fornecer 2 ou 3 PMs.

#### Magia de Sangue

Cada ponto de estresse físico que um personagem recebe gera um ponto de mana. Cada consequência física recebida concede pontos de mana igual ao nível da consequência — então uma consequência suave gera dois PMs — contato que a consequência seja um ferimento que sangre. Os PMs permanecem até que sejam usados para magia ou até que o estresse ou consequência sejam recuperados.

#### Poder Emprestado

Os humanos possuem a habilidade de manipular a magia, mas não a habilidade de criá-la. O poder deve vir de outra fonte, como itens, lugares ou seres de poder, mas cada um desses tem seu preço e suas exigências. Seguindo esse modelo, o termo “magos” é usado geralmente para representar aqueles que usam o poder, mas pode se tratar de um sacerdote, guerreiro sagrado, místico, druida ou o que for apropriado ao cenário. 

Em todos os casos abaixo, conectar a fonte de poder a um aspecto a torna mais firme. Poderes sem aspecto estão sujeitos a imprevistos e desconexões à vontade do Narrador.

**Itens de Poder** podem conter uma quantidade pequena de mana, mas deve ser mantidos por perto e ser usados durante a conjuração. A vasta maioria desses itens são acessórios descartáveis que se tornam inúteis após fornecer a carga. Criá-los leva cerca de um dia em um ambiente apropriado — como um laboratório para um alquimista, uma floresta para um druida e assim por diante — um custo moderado e uma rolagem de dificuldade Ótima (+4). Um sucesso faz com que o item seja preenchido com 1 PM e um sucesso com estilo o preenche com 2 PM. A quantidade permitida desses itens por personagem é igual ao seu nível na perícia mágica. Sim, isso significa que roubar os focos mágicos de um rival e escondê-los é uma forma excelente de roubar poder de um adversário. Também é possível criar um item poderoso que se reabastece todo dia. Fazer isso requer um mês de esforço a um grande custo e uma dificuldade similar. Um mago pode possuir apenas um único item desse tipo, o que o torna ainda pior se for roubado.

**Locais de Poder** concedem mana àqueles em sintonia com eles, de acordo com as regras específicas do local. Na maioria das vezes, eles concedem um único PM por dia ao nascer do sol, que deve ser usado naquele mesmo dia ou será perdido no próximo nascer do sol. No entanto, certos lugares possuem benefícios especiais (como conceder PMs extras, permitir que o mago mantenha uma reserva de 3 PMs ou permitindo ao mago respirar debaixo d'água) ou limitações (PMs que podem ser usados apenas para magias de fogo, perder todos os PMs se matar uma gaivota, etc.).

Uma vez que o personagem esteja em sintonia com o local, o benefício permanece indefinidamente, embora alguns locais forneçam benefícios extras se o personagem estiver presente, na maioria das vezes com ganho acelerado de mana. No entanto, obter e manter a sintonia é um tanto complicado.

Locais de poder são fortemente procurados por magos e outros seres mágicos, então normalmente há um proprietário com interesses adquiridos no lugar, especialmente porque a maioria desses locais possui um limite na quantidade de pessoas que podem se sintonizar com eles. Porém, mesmo sem se preocupar com tais guardiões, nem sempre fica óbvio como é possível sintonizar com um local, então conhecimento e pesquisa podem ser necessários.

Você perderá a sua sintonia com um local de poder se outra pessoa sintonizar e lhe expulsar, seja por tomar o seu lugar no local — se o local estiver no limite —, seja por ativamente remover sua conexão. Os detalhes dependerão do local.

Dessa forma, lugares de poder geralmente são valiosos para magos, mas também criam negociação e politicagem mágica. Ninguém quer gastar todo o tempo protegendo seus locais de poder, mas todos querem tantas sintonias quanto possível, e esse equilíbrio é o ponto central de muitas cabalas mágicas.

**Seres de Poder** oferecem muitos dos benefícios e efeitos dos locais de poder, mas sem os intermediários. O mago faz um acordo com o ser, compromete-se a respeitar suas regras e recebe uma certa quantia de poder — e possivelmente outros benefícios — em troca desse poder estar sujeito ao discernimento do ser e permitindo ao ser uma conexão constante com o mago — uma conexão que pode muito bem ser usada em outras negociações. 

A natureza precisa desse ser de poder pode variar — deuses, espíritos, bestas em forma de totem, lordes feéricos, construtos universais axiomáticos ou qualquer outra coisa. O truque com tais seres é saber como estabelecer o contato. Para alguns, isso é fácil, mas para outros pode envolver desvendar mistérios profundos.

Não há nada que impeça um mago de realizar pactos com múltiplos seres, pelo menos até que esses pactos entrem em conflito uns com os outros. Nesse ponto, o jogador pode descobrir que quebrar esses pactos também possui um preço.

### Efeitos

Vários destes efeitos fazem referência ao uso de uma perícia mágica, mas não veja isso como obrigatório — não significa que deve existir uma perícia mágica. Na verdade significa que qualquer perícia que você determinou que controla a magia deve ser usada nesse ponto.

#### O Que Há No Chapéu?

Se é porque o chapéu é mágico ou porque o seu portador é talentoso, não importa — o ponto é que o dono do chapéu coloca sua mão dentro e puxa algo para fora. As regras para isso são simples:

**Efeito 1:** Sem custo algum, o personagem pode retirar itens inúteis, pelo valor visual. Se o personagem gastar uma ação para puxar objetos inúteis, ele recebe um bônus de +1 em qualquer rolagem relacionada à conjuração em seu próximo turno.

**Efeito 2:** Ao custo de 1 PM, o personagem pode produzir algo útil, mas simples, como uma arma ou a ferramenta correta para a tarefa em questão. Não há rolagem de perícia relacionada, é apenas um facilitador para futuras rolagens.

**Efeito 3:** Ao custo de 2 PMs, o personagem pode puxar algo grande, perigoso ou estranho, que permita a ele usar sua perícia mágica em lugar de outra perícia, contanto que ele possa descrever como o objeto permite realizar tal rolagem específica. Por exemplo, um martelo gigante pode ser usado para atacar, uma mola pode ser usada para pular, uma nuvem de fumaça pode facilitar a furtividade e assim por diante. Se já tiver usado um determinado truque antes, receba -2 em sua rolagem.

**Efeito 4:** Ao custo de 3 PMs ou mais o personagem pode tirar uma criatura ou autômato capaz de ações independentes. Essa criatura ou autômato é um extra baseado em perícia — o conjurador seleciona a forma da criatura e sua perícia primária; o Narrador preenche quaisquer perícias secundárias que forem necessárias. Com 3 PMs, essa criatura pode ter uma perícia máxima em Razoável (+2), que pode ser aumentada em uma base de gasto de um PM para um ponto. Depois de determinado esse nível, o conjurador fará uma rolagem da magia, com dificuldade igual ao nível da criatura. Se a conjuração falhar, a criatura ainda é invocada, mas possuirá um certo número de complicações inesperadas equivalentes à margem da falha. Uma ou duas complicações podem ser inconvenientes, mas três provavelmente criará uma ameaça fora de controle ou outro problema grande.

**Efeito 5:** Um personagem também pode gastar todos os PMs restantes (mínimo de 1) e fazer um “puxão às cegas”. Isso traz algo grande, dramático e que, de uma forma ou de outra, termina a cena atual, mas é o Narrador que determina os detalhes exatos. Quando isso acontece, o Narrador secretamente rola um único dado Fate. Um + indica que a resolução foi a favor do jogador — um tufão os carrega para um local seguro, seus inimigos se transformam em rãs ou algo do gênero. Um resultado - funcionará contra o jogador de alguma forma — ele acaba se prendendo ou tornando a situação ainda pior. Em caso de 0 , a situação muda dramaticamente, embora não necessariamente para melhor ou para pior — os inimigos são transformados em um tipo diferente de inimigo, a paisagem se transforma em doce e assim por diante.

##### Variações

Tradicionalmente isso seria um chapéu, mas nada impede que seja um manto, uma bolsa ou algo similar.

Também é possível usar este modelo para representar grandes construtos de energia brilhante, como aqueles que aparecem nos quadrinhos. Nesse caso você remove o efeito 5, o bônus do efeito 1 e faz a falha do efeito 4 exigir mais PMs (numa base de um para um).

Para tornar isso ainda mais cósmico, você poderia reduzir o custo dos efeitos 2 e 3 em um. Isso fica bem perto do uso da feitiçaria como uma perícia para todos os fins, mas para certos gêneros — como super-heróis — isso pode cair bem.

Com uma mudança cosmética, as regras aqui podem se tornar um excelente sistema para lidar com certos aparatos da ficção científica, especialmente as não muito bem definidas, ferramentas para todo tipo de coisa, mesmo aquelas mais exóticas. Nesse caso, a manifestação física é substituída por descrições técnicas incompreensíveis e a interação por tecnologia. O efeito 0 é uma sequência de efeitos eletrônicos acidentais, o efeito 1 é praticamente inalterado, mas o efeito 3 basicamente permite que a “magia” seja usada para um super-hack, fazer qualquer coisa que o equipamento local seja capaz. O efeito 4 se aplica apenas quando existe algum aparelho para se controlar — como um robô ou carrega- dor mercadorias — e o efeito 5 está fora de cogitação, a não ser que o Narrador goste de situações como como “o que acontece se eu apertar todos os botões?”.

#### As Seis Pragas

Ao contrário do que parece, as seis pragas são os nomes dos seis maiores demônios do inferno. Seus nomes não podem ser perfeitamente enunciados por língua ou caneta, mas cada um possui um símbolo que pode conceder uma fração de seu poder sombrio. O poder é fácil de ser usado — basta fazer uma inscrição permanente através de uma tatuagem, marca com fogo ou escarificação para conceder o poder —, mas o conhecimento desses símbolos é mantido em segredo. Há rumores que só os demônios sabem o segredo e tomam a forma de mortais para compartilhá-lo, na esperança de enlaçar almas. As pragas possuem vários nomes, a maioria impronunciável — usamos os nomes mais comuns aqui, embora isso não garanta que todos as chamarão dessa forma.

**A Flecha** permite ao usuário realizar teletransporte até uma distância máxima de 30 metros (3 ou 4 zonas). O corpo do personagem precisa ser capaz de percorrer tal distância, portanto o teletransporte pode ser para cima, ou por cima de um poço, mas não pode atravessar uma parede ou grade, parando antes de ir de encontro a um obstáculo. Esse processo exige apenas um passo, então o personagem pode alcançar uma longa distância realizando saltos sequenciais. Cada salto custa um PM.

**A Pele de Ferro** fornece proteção contra ameaças físicas. Quando um personagem recebe dano físico, ele pode gastar PM para ganhar armadura igual ao número de PM gastos. Além disso, se o personagem sabe com alguma antecedência que um golpe está a caminho, ele pode gastar 3 PM para se fortificar e ignorar o dano. Embora isto seja inútil em uma luta, pode ser bastante útil para quedas perigosas ou conter o golpe mortal do carrasco, mas fique ciente que a proteção dura por uma fração de segundo. Ele pode sobreviver ao impacto de um trem, mas isso não garante que sobreviverá a queda ou, pior, se for arrastado para debaixo do trem.

**O Esplendor** , por 1 PM, concede o poder de conversar com qualquer cadáver que ainda esteja em boa forma física para falar. Convencer o cadáver a fazer outra coisa que não seja gritar é um problema totalmente diferente.

**O Cavaleiro** permite a um personagem gastar 1 PM para “saltar” para a mente de qualquer mamífero ao alcance da visão. Enquanto estiver em sua mente, ele não possui acesso aos seus pensamentos ou controle sob suas ações, mas pode acessar seus sentidos e permanecer com ele por cerca de dois minutos. O jogador pode optar por gastar um PM adicional para realizar outro “salto” para outro alvo em sua linha de visão. Isso também estende a caminhada por mais 2 minutos. Períodos extras de 2 minutos custam 1 PM. Enquanto isso acontece, o corpo do personagem permanece impotente, olhando para o nada, tornando-se uma presa fácil.

**A Visão** concede consciência de fenômenos sobrenaturais, mas acontece de forma irregular. Quando houver magia em funcionamento, o personagem sente um formigamento e pode gastar 1 PM para receber os detalhes sobre o que está acontecendo. A visão também pode carregar detalhes proféticos ou sonhos, embora esses sejam mais perturbadores do que úteis.

**O Terror** cria uma aura de horror em volta de um personagem por um 1 PM. A aura tem a duração de uma cena e os ataques do personagem podem infligir estresse físico ou mental, conforme desejado. Animais com um mínimo de autopreservação não entrarão na mesma zona e farão o possível para fugir dela. Por um PM adicional, o personagem pode encarar um alvo racional nos olhos e usar intimidação para criar o aspecto ___Medo de [Personagem]___ nele. Se bem-sucedido, ele pode invocar gratuitamente o aspecto pelo restante da cena.

Se um personagem não possuir PM suficientes para usar o poder, ele ainda poderá fazê-lo, mas sua alma estará em risco. Escolha um aspecto e o sublinhe – o personagem recebe o PM que precisa, mas esse aspecto agora está contaminado. Na história, isso significa distorcê-lo de forma sombria quando surgir a chance, e mecanicamente ele não produz mais pontos de destino quando forçado; em vez disso, ganhe um PM. Tais aspectos contaminados criam compulsões sombrias ocasionais, então o poder que vem com essa mácula traz um grande risco.

##### Variações

Pode haver versões mais poderosas de cada uma das marcas, embora seja melhor não discutir os requisitos necessários para obtê-las.

+ **Flecha:** Por 2 PMs, o alcance pode ser aumentado até onde se possa ver claramente.
+ **Pele de Ferro:** Golpes desarmados agora atingem como uma arma de metal e o personagem pode adicionar +2 por PM gasto para qualquer tarefa de força bruta.
+ **Esplendor:** Por 3 PMs o personagem pode animar qualquer cadáver em boas condições físicas, para se mover por uma cena. Ele seguirá instruções básicas e, mesmo não sendo muito útil em uma luta, é algo bem assustador.
+ **Cavaleiro:** Ao custo de 3 PMs por salto, o corpo do personagem desaparece. Ele reaparece atrás do último alvo.
+ **Visão:** Por 1 PM o personagem pode descobrir algum segredo sobre uma pessoa ou objeto. Não há garantia de que seja aquilo que queria saber e é permitido apenas um uso por alvo.
+ **Terror:** Ao gasto de 1 PM adicional (2 PMs no total) o personagem pode aumentar a intensidade de sua aura de medo. Agora ele causa dano mental igual a qualquer dano físico infligido num ataque.

------

- [« Criando Seu Próprio Sistema](../criando-seu-proprio-sistema/)
- [Fragmentos de Poder »](../fragmentos-de-poder/)
