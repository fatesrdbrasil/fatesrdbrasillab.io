---
title: Senhores do Vazio
layout: default
---


## Senhores do Vazio


### Notas de Criação

Tal como acontece com os Invocadores das Tempestades, este sistema está intimamente ligado ao sistema das Cinco Tempestades e, da mesma forma, foi criado que possa ser usado por si só com facilidade. Embora, neste caso, possa parecer algo ainda mais diferente. Esta é a magia das trevas e de coisas terríveis que se encontram fora dos limites da realidade. Classicamente, é o espaço para horrores inimagináveis e cheios de tentáculos, mas a esperança aqui é torná-la mais palpável. Baseado em fontes como os livros da série _Twenty Palaces_ de Harry Connolly, a ideia é que o indivíduo não precise apoiar tanto no “desconhecido” para pensar em coisas perturbadoras.

Seria fácil dizer que se trata de magia para vilões, mas estaríamos fazendo um desserviço a você e seus jogadores — para não falar sobre o desserviço aos vilões. Esta magia vem a um custo horrível, quase desumano, mas quase desumano não é o mesmo que desumano. Os cultistas que invocam coisas das trevas não são simplesmente loucos que buscam destruir o mundo. Eles querem algo e estão dispostos a pagar o preço. 

O que querem e o preço a ser pago torna difícil distingui-los de alguns heróis.

### Descrição

Ninguém sabe ao certo o que é o Vazio. Algumas literaturas sugerem que é tudo o que não pertence ao universo, enquanto outros textos sugerem que é o fim do universo onde o tempo e espaço são indistinguíveis. Para alguns é simplesmente o inferno. O que quer que seja, é um lugar ruim. Escuridão em todos os sentidos da palavra. Às vezes alguém tropeça em suas bordas e se ela não o matar, o marcará terrivelmente.

Felizmente, isso é raro. O Vazio não interage com o mundo, a menos que alguém realmente vá a sua procura e, mesmo assim, é algo bem difícil de ser encontrado. Na verdade, é provavelmente impossível de ser encontrado, exceto por uma verdade perigosa: há coisas que querem sair de lá.

Não há uma descrição única do que são essas coisas e o que querem. Algumas são pouco mais do que animais, embora animais possuídos de poderes horríveis. Outros são claramente possuidores de algum nível de inteligência, de sub- a sobre-humanos.

Os mais espertos são uma ameaça óbvia – eles buscam meios para tornar o Vazio mais fácil de ser encontrado, oferecendo pactos e buscando espalhar conhecimento que deveria ser ocultado. Seus objetivos diferem. Alguns claramente buscam cruzar o limiar para o nosso mundo, outros parecem querer atrair outros para seu mundo de trevas. Outros são sim-
plesmente um mistério.

Ainda assim, a ameaça desses animais não deve ser subestimada — a ameaça que representam muitas vezes é ecológica. Uma única criatura pode não oferecer grande risco, mas dado o tempo para se reproduzir e se espalhar, eles podem representar uma ameaça de extinção.

Mas eles são tão úteis.

O poder do Vazio toma várias formas. Mais comumente é a forma de invocação e vínculo com alguma criatura útil. Enquanto as devidas precauções forem tomadas, essas criaturas podem ser mantidas de forma bastante segura, mas precauções muitas vezes têm limites. Isso é duplamente verdadeiro para criaturas muito poderosas. Elas não são muito mais difíceis de serem invocadas — embora a formação do vínculo seja outro assunto — e farão o possível para contornar as restrições impostas sobre si.

Às vezes, verdadeiro poder também pode ser adquirido de relações com o Vazio. Isso pode assumir a forma de conhecimento, como uma magia, truque ou algo mais direto, normalmente através de algum tipo de infecção. Esta última pode ser tão ruim quanto soa — não há nenhuma garantia de que o poder recebido hoje não é o mesmo que o fará explodir em uma horda de minhocas devoradoras de carne amanhã.

### **Versão Resumida**

Encontre as instruções para realizar uma invocação das trevas.

- Faça uma rolagem de Conhecimento contra a dificuldade de invocação. Após isso, você adiciona qualquer bônus necessário para tornar a    rolagem bem-sucedida. O Narrador recebe 1 Ponto de Condenação    para cada +1 concedido.
- Você usa a coisa horrível que invocar para seu próprio benefício e em    detrimento do mundo.
- O Narrador gasta os Pontos de Condenação para tornar sua ideia    ruim ainda pior.

### Mecânica

Há duas considerações mecânicas diferentes para lidar com os poderes do Vazio. A primeira é uma questão de como o contato é feito, como as coisas são invocadas e assim por diante. A segunda é como os efeitos dessas coisas são expressados.

#### Invocações

Parece banal, mas invocar algo do Vazio é aproximadamente comparável à montagem de um móvel grande. Você tem instruções extensas e se segui-las à risca e possuir todas as ferramentas corretas, devem produzir o resultado prometido. Infelizmente, mesmo na melhor das circunstâncias, esse tipo de coisa pode ser confuso. O autor raramente é um escritor qualificado e não podemos esquecer que é o tipo de pessoa que escreve livros sobre como invocar monstruosidades profanas.

Isto é, na verdade, o porquê de qualquer um que entenda do assunto ser cético a respeito de grimórios. Qualquer um que tente montar um livro a partir dessas coisas deve ter prioridades suspeitas, além de que não há real garantia que algo funcionará. Há diversos grimórios conhecidos em circulação em número suficiente para serem reconhecidos; a capacidade de identificar quais são falsos é uma perícia bastante útil para a sobrevivência. Mesmo assim deve-se continuar atento — não é raro um praticante incluir rituais ruins conhecidos em seus livros.

Os verdadeiros tesouros são os cadernos de anotações. A realidade é que praticantes desleixados são praticantes mortos, enquanto aqueles que sobrevivem por mais tempo costumam documentar tudo — sucessos, falhas e todo o resto. Infelizmente essas anotações tendem a ser rascunhos pessoais na melhor das hipóteses e códigos cifrados na pior delas, então nunca é fácil.

Tudo isso serve para dizer que, em muitos casos, a dificuldade em descobrir como invocar algo tem menos a ver com a dificuldade da tarefa em si, que normalmente não é difícil, mas tem muito a ver com obter instruções úteis e confiáveis. Essa limitação é a grande razão pela qual mesmo os praticantes bem-sucedidos normalmente possuem apenas alguns truques na manga. Cada nova invocação que aprendem exige um período similar a uma “roleta russa” com apostas cada vez mais arriscadas.

Portanto, o ato de invocar propriamente dito é realizado com uma rolagem de Conhecimento para ver quão bem você segue as instruções, assim como quão bem você toma precauções, aplica seu julgamento durante o processo e procede com cautela. Assumindo que o ritual usado seja correto — e não há garantia disso — há duas dificuldades em jogo: a dificuldade do ritual em si e a dificuldade de compreender o ritual através do texto. Para fins de melhor controle, essas são a __dificuldade de invocação__ e a __dificuldade de compreensão__.

Como regra geral, a dificuldade de invocação é normalmente baixa, mesmo para criaturas poderosas. Lembre-se, elas _querem vir_ aqui e o único desafio real é fazer isso com segurança. Compreender as instruções é muito mais difícil.

As dificuldades para invocar tendem a ser consistentes, mas as de compreensão dependem totalmente do material consultado. A menor dificuldade de compreensão do texto pode ser igual à dificuldade de invocação. Em todo caso, ambas as dificuldades são desconhecidas para o jogador.

Para invocar com eficácia, o personagem precisa realizar todos os passos necessários de acordo com o ritual e fazer uma única rolagem de Conhecimento contra ambas as dificuldades.

Após as rolagens:

- Se o jogador não atingir nenhuma das dificuldades, o feitiço não    funciona, ou funciona sem a segurança adequada e o que quer que tenha sido chamado agora está à solta. Essa decisão fica a cargo do Narrador, dependendo de quão divertido achar que será.
- Se o jogador alcançar a dificuldade de invocação, mas não a de     compreensão, o feitiço funciona! Exatamente como esperado! Bem,     mais ou menos como esperado.

Anote por quanto o jogador falhou em alcançar a dificuldade de compreensão. O valor é convertido em Pontos de Perdição. Perceba que isso se refere explicitamente a _bater_ a dificuldade — um empate ainda rende um Ponto. Pontos de Perdição são a moeda de troca para coisas que dão errado. Podem ser pequenas ou sutis, podem ser grandes e dolorosas, mas não são evidentes de imediato e podem ser reveladas conforme a vontade do Narrador.

---

Então, porque a invocação é tão “segura” para os PJs? É um dos pontos básicos do design de jogos. Não crie um sistema de magia onde os feitiços podem matá-lo, mas são úteis quando bem-sucedidos. Isso é totalmente razoável de uma perspectiva mais “realista”, mas é terrível para o jogo. A magia segue o princípio básico da utilidade — se for colocada em jogo, alguém a usará e então caberá a você fazê-la funcionar, de um jeito ou de outro. Neste caso específico, é mais interessante ter jogadores lidando com as consequências do sucesso. Isso pode ser pior do que morrer e ninguém precisará criar outro personagem; ao menos não imediatamente.

---

---

Por exemplo: David tenta um ritual com uma dificuldade de invocação Razoável (+2) e uma dificuldade de compreensão Fantástica (+6). Ele consegue um Ótimo (+4) então o feitiço funciona, mas o Narrador acumula 3 Pontos de Perdição já que David precisaria de mais 3 para superar a dificuldade.

---

Se for bem-sucedido com estilo em um ritual, você consegue sem acumular Perdição e a dificuldade de compreensão cai em um ponto nas próximas execuções do mesmo ritual. Isto não pode reduzir a dificuldade de compreensão abaixo da dificuldade de invocação e se aplica apenas a você. Qualquer outro ainda precisa usar a dificuldade padrão para a compreensão do texto original — ou suas anotações, se for apropriado.

##### Rituais Extensos

As regras até agora pressupõem a invocação de seres de tamanho pequeno a médio vindos do Vazio. Há criaturas maiores que também podem ser invocadas e controladas. As regras atuais e dificuldades não mudam, mas as exigências para o ritual são bem mais extravagantes. Topos de montanhas isoladas, círculos de correntes de ouro, mil origamis em forma de grou com asas banhadas no sangue menstrual de uma assassina e coisas desse tipo. Assim como em uma invocação normal, a rolagem não é a parte essencial do processo — e na verdade as regras são as mesmas — o que importa são as coisas que levam a esse momento.

Pode parecer contraintuitivo — se coisas maiores estão tentando atravessar para nosso mundo, porque os rituais requerem mais componentes? É apenas uma questão de praticidade — pense nos rituais como andaimes. Quanto maior for a coisa que quer trazer, mais forte deverá ser o andaime — e isso antes de começar a considerar a questão do vínculo.

Claro, muitas dessas coisas são grandes, espertas e poderosas suficiente para encontrar brechas. Algumas criaram ou levam à criação de artefatos — espelhos, estátuas, patas de macaco, caixas de quebra-cabeça e afins – que podem abrir caminhos em direção a eles. 

Felizmente, estes em geral são limitados de alguma forma; caso contrário a criatura já teria atravessado. Contudo, são iscas excelentes, por assim dizer.

#####  Vínculos

A criatura invocada não pode fazer muita coisa. Parte da invocação é a vinculação, ao menos deve ser, se tudo for feito corretamente. Basicamente, o Invocador deve liberá-lo — totalmente ou em parte — para usufruir de suas habilidades. Para criaturas estúpidas, isso significa apenas quebrar o círculo e deixá-las fazer seu trabalho. Para criaturas mais inteligentes, significa liberá-las no intuito de usar seus poderes sob condições estritas.

Tecnicamente, essas coisas são bem faustianas em suas negociações. Eles geralmente não podem negociar — a menos, é claro, que possam — mas tentarão explorar todas as brechas nos limites impostos a eles. No entanto, nada é menos divertido do que aparecer com condições exatas para essas coisas, portanto isso não deve ser exigido. Narradores devem falar com os jogadores sobre suas intenções, e em seguida, respeitá-las. Pontos de Perdição e consequências naturais devem prover complicações até mais do que suficientes.

##### Pontos De Perdição

Pontos de Perdição são uma moeda simples para contabilizar todas as coisas que os personagens não levaram em consideração. Pense neles como brechasinfernais, um balde de coisas só esperando o momento para quando algo der errado na invocação. Basicamente, eles dão carta branca ao Narrador para piorar a situação — não que ele não possa fazer isso de qualquer maneira, mas gastar Pontos de Perdição transfere a culpa. Algumas coisas que podem ser feitas com o gasto de um Ponto de Perdição são:

- Permitir à criatura usar um poder além do âmbito do vínculo,    mesmo que apenas um pouco de cada vez.
- Permite à criatura invocar outras criaturas.
- Permite à criatura estabelecer contato com alguém que possa estar    interessado em um acordo melhor
- A mera presença da criatura impõe um aspecto de cena ao ambiente, em um raio de crescimento constante.

###### _Rituais E Aspectos_

Aspectos parecem ser uma excelente maneira de garantir um ritual seguro e evitar toda essa questão de Pontos de Perdição do Narrador. Tecnicamente realmente são, mas há algumas coisas a considerar.

Ao fazer uma dessas invocações, você está fazendo algo terrível. Tudo que é trazido do outro lado é uma abominação e uma ameaça ao mundo. Quando invoca um aspecto para ajudá-lo a fazer isso, você poderia muito bem estar contaminando aquele aspecto.

Todos entendemos a questão de fazer coisas ruins por bons motivos e isso pode ser a motivação que leva alguém a invocar. Porém, ao tomar essa decisão você ultrapassa um limite e diz algo profundo sobre esse aspecto. Isso diz algo sobre o personagem e também sobre como o aspecto aparece em jogo. Uma vez que abre a porta para coisas abomináveis em nome do amor, você convida o Narrador a ver o quão longe você está disposto a ir.

Talvez isso seja incrível. Pode ser exatamente o que você espera ver em jogo — é um tema ótimo e poderoso. Apenas comentamos aqui para que você abra essa porta com os olhos bem abertos.

### Criaturas e Poderes

Eis a dura realidade — não há como catalogar plenamente todas as manifestações dessas coisas horríveis. Oferecemos aqui uma variedade de exemplos, mas a verdade é que, para isso, você deve procurar em _cada uma_ das estruturas do Fate que puder busca de ideias. Estas sugestões são isoladas, então se as regras para uma criatura em particular não combinarem
com algo no jogo, esse será o contexto no qual ela **fará sentido**. Esta é a sua chance de pirar.

O único limitador é que você deve monitorar algumas coisas, evitando uma morte em massa de seus PJs assim que as coisas perigosas forem soltas. Uma das formas seria limitar a variedade de coisas ruins a um número que os PJs consigam lidar. Outra seria certificar-se de que os PJs possuam as defesas corretas. São apenas alguns pontos a se levar em consideração.

#### Exemplos De Criaturas

##### Besouros Devoradores de Feridas

**Dificuldade de Invocação:** Regular (+1)

Estes besouros pouco mais longos que um polegar têm uma carapaça macia e branca, como se tivessem acabado de sair de algum ciclo de crescimento, mas que nunca endurece. Para usá-los, basta deixá-los andar um pouco sobre a pele — é um tanto asqueroso no começo, mas com o tempo você nem perceberá que estão lá. Literalmente. A não ser que esteja fazendo um grande esforço para tentar localizá-los, você simplesmente não estará ciente de sua presença. Se não souber onde esperar que apareçam, é como aquelas sensações estranhas que surgem e desaparecem rapidamente.

O besouro recebe esse nome por sua capacidade de comer feridas. Ele desfaz coisas em menor escala e mecanicamente possui um efeito bastante potente: uma vez por dia, o seu portador pode remover uma consequência física, apagando o ferimento. Após comer três dessas feridas, ele deixará um pequeno ovo em alguma parte de seu corpo — o que também será imperceptível — e, em uma semana, outro besouro nascerá e andará despercebido — podendo assim, comer novas feridas.

Se, por outro lado, um besouro passar uma semana sem feridas para se alimentar, ele encontra alimento em outro lugar e devora um dos aspectos do hospedeiro. Isso não causa qualquer mudança direta — nada é esquecido ou removido, só se tornam menos importantes. Uma vez que todos os aspectos de uma pessoa tenham sido devorados, ela basicamente se rende a um tédio apático, com momentos ocasionais de atividade intensa ou tendência suicida — que são controlados rapidamente — como se tentasse obter algo que perdeu mas não consegue encontrar.

Seu lar se torna bagunçado e mais repleto de tranqueiras, que se assemelham a ninhos com caminhos partindo dos arredores do brilho da televisão. Não por coincidência, esses ambientes cheios de detritos são favoráveis ao besouro, que usa essa desordem para viajar com mais segurança para longe de seu hospedeiro — já que são presas fáceis tanto para botas quanto predadores — na tentativa de encontrar um novo hospedeiro, de preferência aqueles que estejam dormindo e nunca notarão seu novo hóspede.
 
Os besouros em si não são uma grande ameaça — até mesmo uma infestação menor raramente se estende a mais que um prédio. No entanto, eles também são simbióticos com algumas infestações e invasores agressivos. Um humano transformado em uma máquina de matar é perigoso, mas um coberto de besouros devoradores de feridas é um problema muito maior.

##### Vermes Trovejantes

**Dificuldade de Invocação:** Razoável (+2)

Há um nome maior para esta coisa, mas é bem complicado. Se parece com um cruzamento entre um raio e uma centopeia acelerada, que nunca diminui o ritmo o suficiente para que se possa dar uma boa olhada. Ele não consegue se manter em repouso por muito tempo, embora possa ser mantido em um frasco de vidro preparado adequadamente. Lançado, é como uma luz estrondosa, tão perigoso quanto um relâmpago, e então desaparece.

Se, no entanto, for lançado através de algo pelo qual possa viajar, como um cabo de força, ele pode se manter indefinidamente. Dessa forma ele pode manipular dispositivos eletrônicos de forma rudimentar e pode dar um bote como uma cobra a partir de qualquer ponto de exposição elétrica como tomadas, pontos de luz e assim por diante. Esse ataque — Excepcional (+5) contra Percepção, Arma: 7 — é bastante potente, mas dispersa a criatura e muitos praticantes os usam como armas descartáveis.

Se algum conseguir se libertar, ele tende a ocupar um “ninho” de fios, como uma casa ou escritório — eles parecem ter dificuldades em atravessar grandes distâncias através da rede elétrica. O único sinal óbvio disso são estranhezas elétricas, pelo menos até a criatura se multiplicar. Um verme trovejante pequeno é do tamanho de uma centopeia verdadeira e surge de tomadas, procurando por qualquer aparelho eletrônico que conseguir encontrar — normalmente dispositivos pequenos —, permanecendo lá até que seja conectado a outra rede onde possa crescer e eventualmente produzir sua própria prole. Se não, crescerá até que o dispositivo não possa mais contê-lo, então matará a próxima pessoa que encontrar. O que vier primeiro.

Deixando de lado a ameaça à infraestrutura que essas coisas representam, eles possuem outro péssimo hábito — cadáveres frescos são uma rede interessante para eles. Coisas vivas não atraem muito o seu interesse, mas corpos recentemente falecidos que possuem um sistema nervoso sofisticado? É como uma festa. Uma festa trôpega, não morta, crepitante e com dedos que eletrocutam — ao menos até que se esvaia em chamas. Os vermes podem difíceis de detectar, mas o corte total da eletricidade os mata instantaneamente.

##### Olhos de Lázaro

***Dificuldade de Invocação:*** Razoável (+2)

Esta esfera branca, aproximadamente do tamanho de um ovo, é inofensiva e inerte a maior parte do tempo. Se, no entanto, for colocada na órbita do olho de uma pessoa recentemente falecida, estenderá tentáculos até o seu cérebro fazendo com que o corpo se regenere. Desde que a carne — e o mais importante, o cérebro — esteja em grande parte intacto, então ao longo das próximas 12 horas a pessoa voltará à vida. Se o cérebro não estiver intacto, então o corpo se reanimará e caminhará trôpego, até que morra de fome. Assustador, mas na maioria das vezes, inofensivo. Porém, se o cérebro estiver intacto, então a pessoa realmente volta à vida – personalidade, memória e tudo mais. A única diferença é que o Olho de Lázaro nunca consegue ser da mesma cor que o original, então os olhos ficam diferentes.

O problema é que o Olho de Lázaro se alimenta de massa encefálica e só há duas formas de obtê-la — do hospedeiro ou de outra pessoa. O método primário é estimular o hospedeiro a comer cérebros — ou então comer o cérebro do hospedeiro. Essa vontade pode começar por animais, mas eventualmente eles forçarão o hospedeiro a cometer canibalismo para conseguir cérebros humanos. Cérebros de corpos recém falecidos serão suficientes por um tempo, mas nunca saciará a fome como um que acabou de ser morto — ou melhor ainda, um ainda vivo.

No entanto, isso não muda a natureza da pessoa que o faz — ela faz essa coisa monstruosa plenamente consciente da monstruosidade, mas porque precisa. Ela não pode parar, só precisa tomar medidas para não atacar seus amigos ou entes queridos. além disso, o Olho de Lázaro sobrecarrega o sistema suprarrenal do hospedeiro, ampliando seus reflexos e geralmente ajudando-o a se tornar o predador que precisa ser — recebendo +1 em todas as rolagens de perícias físicas, -1 para todas as rolagens mentais que não sejam sensoriais. Ele também bota ovos em seu estômago que podem surgir de sua
boca sempre que o hospedeiro precisar, como para salvar um ente querido ou ajudar um ente querido a juntar-se a ele, para que possam estar seguros.

Se o Olho não for alimentado com regularidade — uma vez por mês inicialmente, mas com maior frequência ao tongo do tempo — então ele se volta à sua única fonte de alimento à mão, comendo o cérebro do hospedeiro, normalmente começando por suas memórias e funções mais complexas. Quando isso acontece, o resultado é uma máquina primitiva de matar — +3 em todas as perícias físicas e -3 em todas as perícias mentais que não sejam sensoriais — fazendo com que quebre crânios com suas
próprias mãos em busca de seu conteúdo. Se continuarem a passar fome, Olho e seu hospedeiro morrem.

##### Freet

**Dificuldade de Invocação:** Boa (+3)

Estas formas corrompidas de ifrit parecem sapos de fogo, mas suas chamas apodrecem e escurecem conforme queimam, o oposto da chama da purificação. Como seres de energia, são difíceis de ferir fisicamente, mas sofrem dano ou são dispersos com água limpa. Não possuem caixas de estresse ou consequências, mas ignoram qualquer dano exceto imersão ou ou jatos fortes de água, como uma mangueira de incêndio — é semelhante a apagar uma fogueira (embora seja uma que está tentando comer o seu rosto), mas fazer isso poluirá profundamente os arredores. Quando invocadas, essas criaturas normalmente são enviadas atrás de um alvo para matá-lo. _Freets_ possuem Lutar e Furtividade em nível Ótimo (+4). Se bem-sucedidos em um ataque contra um alvo, infligem uma **_Podridão Ardente_** — uma infecção terrível e dolorosa que consome o alvo aos poucos. Até que o aspecto seja removido, o personagem recebe 1 de estresse todos os dias e não pode recuperar estresse ou consequências de forma natural. A única maneira de remover esse aspecto é destruir a criatura e todas suas crias, se houver.

No que tange a prole, _Freets_ põem seus ovos no fogo, pequenas faíscas que dão um certo mau odor às chamas. Após três horas, o fogo se extingue, gerando um número de _Freets não vinculados_ de acordo com o tamanho do fogo. Uma lareira pode produzir um, enquanto um incêndio florestal pode produzir dezenas.

##### O Cavalheiro Elegante


***Dificuldade de Invocação:*** Ruim (-1)

Ele é conhecido por vários nomes e invocá-lo é simplesmente uma questão de dizer a palavra certa no contexto certo. Ele, no entanto, só aparecerá de acordo com sua própria agenda, e uma vez que o tenha invocado, ele pode aparecer para você a qualquer momento em que esteja só.

Ele é tema de muitas histórias que possuem alguns pontos em comum. Ele não está fisicamente presente, e muitas vezes pode aparecer apenas em reflexos, como uma silhueta sombria ou de outras formas impossíveis, apesar de às vezes aparecer normalmente. Ele é magro, embora sua aparência seja bastante debatida, e está sempre bem-vestido, embora de uma forma estranha. Ele aparece quando o Invocador está só, apesar disso ter mais a ver com quem pode vê-los do que com solidão literal. Ele já surgiu em festas e outros eventos grandes onde é possível se perder em uma multidão, embora ninguém
exceto seu parceiro possa vê-lo.

O Senhor Elegante está disposto a ajudar. Tenha em mente que ele não pode fazer nada fisicamente para ajudar, mas seu conhecimento é grande. Seu conhecimento de segredos, tanto arcanos como mundanos, não tem fim e ele se alegra em compartilhá-los. Por outro lado, ele também deixa claro que requererá alguns favores ocasionais em troca da informação.

Os favores são aparentemente benignos, embora os praticantes mais experientes tenham notado que costumam girar em torno de preservar e proteger informações e rituais sobre o Vazio que de alguma forma poderiam ser perdidos. É raro o Senhor Elegante levar parceiro diretamente em direção a esses rituais, possivelmente porque ele se beneficia mais ao man-
tê-lo no escuro. Um favor bastante comum é “fazer uma apresentação” e ensinar o nome do Senhor Elegante a alguém. É possível recusar fazer o favor e o Senhor educadamente irá se despedir e nunca mais retornar. A não ser, talvez, que você se encontre em uma situação muito ruim depois, mas perceberá que o preço subiu consideravelmente.

Para alguns, esta é a dimensão desse relacionamento levemente perturbador, um pouco de olho por olho e nada mais. Contudo, ele se torna mais complexo para aqueles que ele considera interessantes. Se você interessa ao Cavalheiro, ele estará mais do que disposto a ajudar e essa ajuda abrirá grandes portas, trazendo também grandes problemas. Esses problemas tendem a ficar piores e piores até o ponto em que seu único desejo é encontrar uma saída.

E isso é algo que o Sr. Elegante tem o prazer de fazer. Ninguém sabe o que acontece depois disso.

##### Cogumelo Apodrecido

**Dificuldade de Invocação:** Boa (+3)

Quando invocado, surge como um cogumelo horrendo e coberto de pústulas, com talvez 30 centímetros de altura. Repouse sua mão sobre ele e uma pústula semelhante aparecerá em sua mão — indolor, mas nojenta. Repouse essa mão sobre uma pessoa doente — ou sobre você mesmo — e a doença desaparecerá, à medida que pústula se fecha e um Cogumelo menor cresce nas costas de sua mão. Ele pode ser removido — dolorosamente — e plantado, onde eventualmente crescerá até o tamanho do primeiro. Desde que o Cogumelo cresça e permaneça saudável, a doença permanece em remissão, embora a saúde de todos os cogumelos subsequentes dependam da saúde do primeiro.

Mais uma coisa desagradável: as doenças seguem o seu curso enquanto estiverem dentro do Cogumelo — seu retorno abrupto incluirá todo o progresso da doença desde o momento da intervenção, muitas vezes com efeitos terríveis e dramáticos.

O cogumelo também pode curar ferimentos, até mesmo lesões traumáticas, mas isso é um pouco mais problemático. O personagem curado parece bem por fora, mas por dentro, a “cura” toma a forma de um fungo esponjoso amarelado que funciona como o substituto da carne. Isso não é diretamente prejudicial — a não ser que esteja repondo massa encefálica, caso no qual os resultados são imprevisíveis, mas raramente bons. No entanto, aqueles curados dessa forma geram esporos com frequência. Cogumelos crescem onde eles repousam. São cogumelos normais, embora venenosos, mas aceleram a degradação normal que um Cogumelo Apodrecido causa.

A mera presença de um Cogumelo Apodrecido é prejudicial — não para o invocador, mas para a área como um todo. Apenas um não é tão perigoso — gripes podem ser um pouco mais sérias na cidade, mas raramente algo perceptível. Cada cogumelo adicional piora a situação. Como se não bastasse, se alguém morrer enquanto recebe o benefício de um cogumelo apodrecido — ou seja, enquanto a doença estiver contida —, outro Cogumelo Apodrecido crescerá em seu túmulo.

### Variações e Opções

#### Mantendo O Controle

Então, se o Vazio é tão perigoso, porque que ainda não fizemos uma grande besteira? Cedo ou tarde algo virá, se multiplicará e dominará o planeta. É questão de matemática.

Há algumas respostas possíveis, qualquer uma ou todas podem ser verdade.

Primeiro, o próprio mundo pode se mover para rejeitar o Vazio. Brechas se consertam com o tempo, antigas invocações se tornam inúteis após o uso excessivo e há uma tendência constante para manter o Vazio distante, o que contrabalança seu esforço contínuo de tentar entrar. 

Segundo, o mundo não possui apenas humanos. Em um mundo mágico, isso pode significar feéricos, espíritos ou até mesmo deuses que tomem medidas para impedir as piores incursões, mas isso vale mesmo em um mundo razoavelmente moderno. Os grandes cérebros dos quais a humanidade tanto se orgulha também são uma razão para estarmos tão vulneráveis a tais coisas. Uma criatura que drene a essência de sua alma e o faça desejar de carne humana para preencher o Vazio pode devastar uma pequena cidade, mas para um coiote é apenas um lanche delicioso.  Animais são menos impressionados por tais ameaças e seria perturbador contar o número de vezes que o mundo foi salvo por ratos e aranhas.

Por último, pode haver pessoas que trabalham ativamente contra esse tipo de coisa. Limitar informações é difícil, mas não impossível, e isso tem todas as características de uma guerra secreta. A necessidade de controlar e destruir informações enquanto se permanece ciente delas e capaz de responder a altura é paradoxalmente difícil. É a daí que nascem boas histórias.

------

- [« Invocadores da Tempestade](../invocadores-da-tempestade/)
- [Criando Seu Próprio Sistema »](../criando-seu-proprio-sistema/)
