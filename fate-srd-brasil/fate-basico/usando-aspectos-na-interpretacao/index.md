---
title: Usando Aspectos na Interpretação
layout: default
---

## Usando Aspectos na Interpretação

Por último, aspectos possuem um uso passivo que pode ser utilizado na maior parte do jogo. Jogadores podem usar os aspectos como orientação para interpretar seu personagem. Isso é evidente, mas preferimos avisar – os aspectos na sua ficha de seu personagem são uma parte essencial daquele personagem *o tempo todo*, não apenas quando você invoca ou força.

Pense nos seus aspectos como um oráculo – como cartas de tarô ou a borra de chá no fundo da xícara. Eles dão uma visão geral de seu personagem e podem revelar implicações interessantes se você tentar imaginar além do que eles descrevem. Se houver dúvida no que seu personagem faria em determinada situação, dê uma olhada em seus aspectos. O que eles falam sobre a personalidade, objetivos e desejos do personagem? Há alguma pista neles que possa sugerir um caminho a ser seguido? Uma vez que encontrar a sugestão, siga-a.

Interpretar de acordo com seus aspectos tem outro benefício: você está sempre dando ideias de forçamento para o Narrador. Seus aspectos já estão sendo colocados em jogo, então tudo o que ele terá que fazer é sugerir uma complicação e pronto.

Narradores podem usar os aspectos dos seus PdNs da mesma maneira, mas possuem mais uma opção – usá-los para saber como o mundo reagirá aos personagens. Então alguém possui o aspecto ***O Homem Mais Forte do Mundo***? Essa pode ser uma reputação conhecida por outras pessoas que podem reagir de acordo. Elas poderiam, por exemplo, se aglomerar ao redor dele quando o virem passar.

Além disso, é algo diretamente ligado ao biotipo do personagem. Você pode imaginar que em um espaço apertado ele tomará bastante espaço ou pode ser excessivamente rude e agressivo caso seja intimidado.

Ninguém, porém, *ignorará* um personagem assim. Inserir esse tipo de aspecto acresce detalhes à narração podendo tornar o seu jogo mais vívido e consistente, mesmo quando não há recompensas em pontos de destino.

> Em uma sessão de *Caroneiros de Asteroide*, Esopo descobre que O Explorador foi capturado por soldados do Clã do Dedo. Amanda informa que, segundo os contatos de Esopo, o Clã é conhecido por torturar seus prisioneiros para extrair informações.
>
> Léo olha os aspectos na ficha de Esopo: ***Mercenário Lagosta***, ***O Explorador Sabe Meus Segredos***, ***Não Entendo Absolutamente Nada***, ***Tá Olhando Pra Mim?*** e ***Bater Sempre Funciona***. Lendo esses aspectos ele nota o quão Esopo é simples (de uma forma grosseira), agressivo, inclinado a resolver os problemas através da violência e leal aos seus companheiros próximos.
>
> Por causa de seus aspectos, Esopo se sente obrigado a resgatar seu companheiro de viagem, mas não só isso, pois também arranjará muitos problemas tentando chegar a seu objetivo, seja através de palavras ou socos.
>
> Amanda diz que a incursão violenta de Esopo nos bares da região é vista com maus olhos e ele arranja um bom punhado de inimigos. Ela segura um ponto de destino e sorri, indicando que está forçando um aspecto – suas maneiras vão acabar expulsando-o do planeta.
>
> Léo aceita a complicação. “Eles que se lasquem”, diz. “Vou encontrar o Explorador por conta própria”.

## Removendo ou Alterando um Aspecto

Aspectos de jogo e personagem tendem a mudar com o tempo. Veja a sessão Marcos em [*Construindo Campanhas*](../construindo-campanhas/) para saber mais.

Se você quer se livrar de um aspecto de situação, isso pode ocorrer de duas formas: realizando uma ação de superar especificamente para se livrar do aspecto indesejado ou realizar outro tipo de ação que, se bem-sucedida, fará com que tal aspecto não faça mais sentido (Por exemplo, se você está sendo ***Agarrado***, é possível tentar escapar. Se for bem-sucedido, não faz mais sentido estar agarrado, livrando-se assim do aspecto).

Se um personagem puder interferir em sua ação, ele faz uma rolagem de oposição contra você normalmente. Caso contrário, é função do Narrador oferecer oposição passiva ou simplesmente diga ao jogador que ele se livra do aspecto sem a necessidade de uma rolagem, caso não haja algo arriscado ou interessante no caminho.

Por último, se em dado momento não houver mais um sentido que um aspecto de situação permaneça em jogo, descarte-o.

## Criando e Descobrindo Novos Aspectos em Jogo

Além dos aspectos do seu personagem, aspectos de jogo e de situação que o Narrador apresenta, você possui a habilidade de criar, descobrir ou ganhar acesso a outros aspectos durante o jogo.

Na maioria das vezes você usará a ação de criar vantagem para criar um aspecto. Quando descrever a ação que realizou para criar vantagem, o contexto deve informar se será preciso um novo aspecto ou se ela utilizará um já existente. Se você trouxer uma nova circunstância para o jogo – como jogar areia nos olhos de alguém – isso indica que você está trazendo um novo aspecto de situação.

Com algumas perícias, fará mais sentido buscar criar vantagem em um aspecto que já esteja na ficha de algum personagem. Nesse caso, o PJ ou PdN afetado ofereceria oposição ativa na tentativa de impedi-lo de usar aquele aspecto.

Se sua intenção é obter uma invocação grátis e acha que faz sentido que determinado aspecto de situação entre em jogo, **não é necessário rolar os dados ou fazer nada para adicionar novos aspectos – apenas faça uma sugestão e, se o grupo achar interessante, é só anotá-lo.**

------

### Narradores

### Truque do Narrador Ninja

Se você não criou nenhum aspecto para a cena ou para um PdN, apenas pergunte aos jogadores que tipo de aspecto eles estão tentando criar com a manobra criar vantagem. Se empatarem ou forem bem-sucedidos, escreva algo que tenha ligação com com a ideia deles e diga que estavam certos. Se falharem, escreva mesmo assim ou escreva outro aspecto que não seja vantajoso para eles, para contrastar com as expectativas.

------

### Aspectos Secretos

Algumas perícias permitem que você descubra aspectos secretos usando a ação de criar vantagem, tanto em PdNs como no ambiente – nesse caso, o Narrador informa qual é o aspecto caso você consiga um empate ou seja bem-sucedido na rolagem dos dados. Você pode usar isso para “pescar” um aspecto caso não esteja seguro do que está procurando – se sair bem na rolagem já é suficiente para encontrar algo vantajoso.

De forma geral, assumimos que os aspectos em jogo são conhecidos *pelos jogadores*. As fichas dos PJs estão dispostas na mesa e, possivelmente, as dos PdNs e coadjuvantes importantes também. Isso não significa necessariamente que *os personagens* saibam da existência de tais aspectos, mas essa é uma das razões para a ação de criar vantagem existir – para justificar como um personagem aprende sobre os outros.

Lembre-se também que os aspectos só ajudam no desenvolvimento da história se você os usar – aspectos que nunca são usados nem deveriam existir. Assim, na maior parte das vezes os jogadores devem saber quais os aspectos estão disponíveis para uso e, se houver qualquer dúvida com relação a um curso de ação de um personagem, use os dados para ajudá-lo a decidir.

Por fim, sabemos que às vezes o Narrador preferirá manter os aspectos de um PdN em segredo ou não revelar na hora um determinado aspecto de situação, tentando criar uma tensão na história. Se os PJs são investigadores buscando pistas sobre uma série de assassinatos, você não vai querer que um dos PdNs tenha o aspecto ***Assassino Serial Sociopata*** escrito em um cartão sobre a mesa no início da aventura.

Nesses casos aconselhamos a não criar aspectos sobre fatos que você deseja manter em segredo. Ao invés disso, faça desse aspecto um detalhe que faça sentido dentro do contexto após o segredo ser revelado.

> Amanda está criando um PdN que consegue mudar de aparência para ser o vilão principal da história. Ele também está constantemente na cidade para onde os PJs irão, então ela não quer deixar as coisas muito óbvias.
>
> Ao invés de criar um aspecto chamado ***Alterador de Aparência***, ela decide criar alguns detalhes pessoais como: ***Comportamentos Variados***, ***Traços Confundíveis***, e ***Segredos Obscuros***. Se os PJs descobrirem alguns desses ou se os virem escritos em cartões em cima da mesa, eles podem começar a desconfiar, mas não estragarão o mistério do cenário na mesma hora.

- [« Invocando](../invocando/)
- [A Economia de Pontos de Destino »](../economia-de-pontos-de-destino/)
