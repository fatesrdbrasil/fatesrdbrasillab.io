---
title: Duelos Aventurescos
layout: default
---

## Duelos Aventurescos

 
Aquele tipo de ação heroica e cinematográfica típica de histórias de espadachins aventureiros está bem próximo ao coração do Fate — é a própria natureza do jogo. Porém, essas lutas temáticas, mano a mano, entre nosso herói e um vilão maligno quase sempre envolve várias investidas antes que um deles consiga acertar um golpe. Nesse meio tempo, pode ser que troquem alfinetadas inteligentes e provocações ofensivas, balancem em lustres, saltem de varandas, confundam o oponente com sua capa e mil coisas mais. Veja D’Artagnan e Jussac em _Os Três Mosqueteiros_, Errol Flynn e Basil Rathbone em _As Aventuras de Robin Hood_, o duelo poético entre Cyrano e Valvet em _Cyrano de Bergerac_ ou o duelo entre Luke Skywaker e Darth Vader em _O Império Contra-Ataca_.

A ação de criar vantagem em Fate torna mais fácil modelar esse tipo de conflito, mas a maioria dos jogadores ainda tenderão a focar nos meios mais fáceis de despachar a oposição, especialmente se não houver outros PJs por perto para dar-lhes a desculpa de criar aspectos de situação. É isso que esta modificação na regra faz — exige que os jogadores dependam de outras perícias, não apenas Lutar, durante um conflito, com resultados bem variados.

Estas regras de duelos introduzem algo chamado __vantagem__. Apenas o duelista com a vantagem pode realmente usar uma perícia com a ação de ataque para causar dano. O outro duelista pode realizar qualquer outra ação, menos atacar — a não ser que ele consiga a vantagem.

E como se consegue essa vantagem? Ao ser bem-sucedido com estilo em qualquer perícia _exceto_ a que causa dano físico em conflitos (Lutar ou Atirar, ou qualquer que seja o equivalente em seu jogo). Uma vez que um combatente seja bem-sucedido com estilo em uma dessas perícias, ele obtém a vantagem. Isso _substitui_ a resolução da ação de sucesso com estilo, tal como conseguir um impulso ou uma invocação grátis. Você pode receber a vantagem ou a resolução padrão, mas não os dois.

Use um marcador para representar a vantagem, algo que possa ser facilmente passado de lá pra cá, como uma moeda, uma pequena espada de plástico para coquetéis, um cartão com uma mão desenhada, uma luva de esgrima — o que seu grupo preferir.

No início de um conflito físico entre dois (e apenas dois) participantes, determine a ordem dos turnos normalmente. Se envolver a rolagem de uma perícia e alguém for bem-sucedido com estilo, ele começa o conflito com a vantagem — já começou partindo para cima do adversário.

Após isso, os combatentes podem fazer qualquer uma das seguintes coisas a cada turno:
 
- Atacar, se possuir a vantagem.
- Tentar conseguir obter a vantagem, se ele não a tiver.
- Fazer outra coisa — colocar aspectos de situação, tentar escapar do conflito, etc.

É altamente recomendável usar a regra opcional de jogo [Sem Estresse](../ferramentas-personalizadas/#estresse) juntamente com essas regras. Caso contrário, há um risco real do combate se arrastar demais, ao invés de derrubar e arrastar o inimigo para fora de combate.

---

Dekka, uma Agente da Lei do Império em Porthos V, está enfrentando seu arqui-inimigo Xoren, um ciborgue traiçoeiro e interessado em usurpar o Trono Celestial, no meio da cerimônia de coroação. Ambos são mestres na espada de fótons, como já haviam demonstrado em disputas passadas. Suas armas de alta tecnologia brilham e zunem com sons característicos protegidos por direitos autorais. Está valendo.

Dekka vence a iniciativa com um +5 contra o +3 de Xoren — um sucesso, mas não com estilo. Ela começa com algumas palavras, na esperança de descobrir algum aspecto usando Empatia. “Qual é o seu problema, Xoren? Há algum algoritmo da maldade em algum lugar de seu sistema central? Ou você realmente acredita que esse truque vai funcionar?” Ela consegue um +6 enquanto Xorem consegue +2. Sucesso com estilo!

“Maldade?”, ele responde. “Passe um dia como um ciborgue neste império miserável e entenderá a _verdadeira_ maldade!” ele diz.

Ela descobre o aspecto _Todos Devem Sofrer Por Minha Dor!_ e escolhe ficar com a vantagem em lugar de receber a invocação grátis extra.

Agora é a vez de Xoren. Sendo um ciborgue maligno, ele agarra um dos espectadores e arremessa contra Dekka, na esperança de criar vantagem com Vigor. Ele vence Dekka por uma margem de 4 tensões — o suficiente para conseguir a vantagem — mas ela usa sua invocação grátis de _Todos Devem Sofrer Por Minha Dor!_ , o que reduz para apenas 2 tensões de diferença. O jogador explica que Xoren, em sua fúria, acidentalmente deixou claro o que pretendia fazer. O Narrador aceita. Xoren
coloca o aspecto de situação _Cidadãos em Perigo_ , com uma invocação grátis.

Dekka ainda possui a vantagem e pretende usá-la. Empurrando o pobre espectador para o lado e saltado para frente, se aproximando de Xoren com sua lâmina de fótons, ela ataca com Lutar +4, superando sua defesa de Lutar +3. Ele aproveita o fato dela estar distraída pelos _Cidadãos em Perigo_ , usando sua invocação grátis para alcançar +5. Após mais alguns pontos de destino gastos, Dekka sai na frente com apenas uma tensão de diferença. Uma vez que eles estão usando a regra variante Sem Estresse, isso significa uma consequência suave para Xoren - _Confiança Abalada_.

---

------

- [« Isto Significa Guerra: Combate Em Massa](../isto-significa-guerra-combate-em-massa/)
- [Veículos »](../veiculos/)

