---
title: Fate Acelerado
layout: default
---
# Fate Acelerado

## Documento de Referência de Fate Acelerado

© 2018 Evil Hat Productions, Coletivo Solar e Fábio Silva

Versão 1.0

Este é o Documento de Referência do Sistema (*System Reference Document*) de Fate Acelerado para uso com licença Creative Commons. A seguinte atribuição deve ser fornecida em seu texto, onde você indica seus créditos (*copyright*), em tamanho equivalente ao seu texto de créditos (*copyright*):

Esta obra é baseada em Fate Sistema Básico, encontrado em www.solarentretenimento.com.br, traduzido, desenvolvido e editado por Alain Valchera, Fábio Silva, Fernando del Angeles, Gabriel Faedrich, Luís Henrique Fonseca e Matheus Funfas e licenciado para uso sob licença Creative Commons Atribuição 4.0 Internacional (<http://creativecommons.org/licenses/by/4.0/>). Versão original: Fate Accelerated Edition © Evil Hat Productions, LLC. Documento de Referência do Sistema produzido por Fábio Emílio Costa, Fábio Silva e Jaime Rangel de S. Junior.

- [Começando!](comecando/)
- [Contando Histórias Juntos](contando-historias-juntos/)
- [O que você quer ser?](o-que-voce-quer-ser/)
- [Realizando Ações: Resoluções, Abordagens e Ações](realizando-acoes-resolucoes-abordagens-e-acoes/)
- [Desafios, Disputas e Conflitos](desafios-disputas-e-conflitos/)
- [Ai! Dano, Estresse e Consequências](ai-dano-estresse-e-consequencias/)
- [Aspectos e Pontos de Destino](aspectos-e-pontos-de-destino/)
- [Façanhas](facanhas/)
- [Aprendendo Com Suas Ações: Evolução do Personagem](aprendendo-com-suas-acoes-evolucao-do-personagem/)
- [Narrando o Jogo](narrando-o-jogo/)
- [Exemplos de Personagens](exemplos-de-personagens/)
