---
title: Para Que Servem os Aspectos?
layout: default
---

## Para Que Servem os Aspectos?

Em *Fate*, os aspectos fazem duas coisas importantes: eles te **dizem coisas importantes sobre o jogo** e ajudam a **decidir quando usar a mecânica do jogo**.

### Importância

O conjuntos dos aspectos do jogo e dos personagens ilustram o foco do jogo. Pense neles como uma mensagem de você para você mesmo, apontando o caminho mais divertido.

Ao criar um cenário para *Fate*, Narradores devem usar esses aspectos e as conexões entre eles para gerar os problemas que os PJs terão que resolver. Para os jogadores, aspectos são o que diferencia um personagem do outro que possua perícias similares – vários personagens em *Fate* podem ter a perícia Lutar, mas apenas Esopo é um ***Mercenário Lagosta***. Quando o fato de ser um mercenário ou sua herança de lagosta entrarem em jogo, o jogo ganha um toque personalizado que não existiria se não fosse por esse personagem.

Os aspectos de jogo fazem algo similar em grande escala – eles nos dizem principalmente porque decidimos jogar tal estilo, o que foi definido e o que nos interessa. Podemos dizer “Ah, gostamos de ópera espacial”, mas até que especifiquemos os detalhes do universo onde as pessoas fazem ***Qualquer Coisa Para Sobreviver*** e onde o ***Império Está Em Toda Parte***, não há muito para nos apegarmos.

Aspectos de situação tornam momentos específicos interessantes para o jogo por adicionar cor à cenas que de outra forma poderiam não ter graça. Uma luta em uma taverna é genérica por natureza – pode ser qualquer taverna, em qualquer lugar. Porém, ao adicionar o aspecto ***Enorme Fosso no Chão*** à cena, as pessoas lembrarão dela como “aquele momento em que lutamos perto de um fosso e eu joguei o gigante dentro do buraco”. Detalhes especiais deixam a cena mais interessante e memorável.

### Decidindo Quando Usar O Sistema

Como os aspectos dizem o que é importante, eles também dizem quando é apropriado usar a mecânica do sistema para resolver uma situação, ao invés de apenas deixar que as pessoas decidam o que acontece ao só por descrever suas ações.

Para os Narradores, isso surge com mais frequência quando estiver tentando decidir se deve ou não pedir que um jogador role os dados. Se o jogador disser "eu me equilibro sobre a rachadura e tento alcançar a fita", e não houver nada de especial na rachadura ou no item, então não há necessidade de uma ação de superar para pegá-lo. Porém, se os aspectos de situação disserem que a rachadura está ***A Ponto de Desabar*** e a fita é ***Envolta de Maldições Antigas***, você subitamente tem um elemento de pressão e risco pelos quais vale a pena rolar os dados.

No caso dos Jogadores, isso acontecerá ao invocarem aspectos ou considerarem forçar. Seus aspectos são o que fazem seu personagem único, e você quer usar isso, certo? Então quando houver uma oportunidade de tornar seu personagem mais incrível ao invocar um aspecto, use-a! Quando surgir uma oportunidade de inﬂuenciar a história ao forçar um aspecto do seu personagem, aproveite-a! O jogo será mais profundo dessa forma.

- [« Tipos de Aspectos](../tipos-de-aspectos/)
- [Criando Bons Aspectos »](../criando-bons-aspectos/)
