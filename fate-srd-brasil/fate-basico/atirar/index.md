---
title: Atirar
layout: default
---


### Atirar

Atirar é a perícia usada para armas à distância em um conﬂito ou mesmo em situações em que seu alvo não possa resistir ativamente (seja um tiro ao alvo ou a parede de um celeiro).

Assim como Lutar, se seu cenário requerer que haja a distinção entre tipos de armas à distância, é possível desmembrar esta perícia em outras como Arcos, Armas de Fogo, Armas de Feixe, etc. Não vá muito longe com isso, a não ser que seja fundamental para seu jogo.

- `O`{: .fate_font} **Superar:** A menos que, por alguma razão, você precise demonstrar sua habilidade em Atirar em uma situação em que não esteja em um conﬂito, você provavelmente não usará essa perícia para se livrar de algum obstáculo. Obviamente, disputas envolvendo armas de fogo são bastante comuns em algumas aventuras de ficção, logo recomendamos que você fique de olho nas oportunidades de acrescentá-las no jogo se possuir um personagem que seja especialista nisso.
- `C`{: .fate_font} **Criar Vantagem:** Em um conﬂito físico, Atirar pode ser usado para executar uma variedade de movimentos, como truques de tiro, manter alguém sob fogo pesado e algo parecido. Em partidascinematográficas, você pode ser capaz de desarmar um oponente com um tiro – bem parecido com o que vemos nos filmes. Você também pode justificar criar aspectos baseados em seu conhecimento em armas (como colocar o aspecto ***Propenso a Travar*** na arma do adversário).
- `A`{: .fate_font} **Ataque:** Esta perícia permite realizar ataques físicos. Você pode realizar esses ataques de uma a duas zonas de distância, diferente de Lutar (algumas vezes a distância mudará de acordo com a arma).
- `D`{: .fate_font} **Defesa:** Atirar não pode ser usado para defesa direta - Atletismo serviria para isso. Você pode usá-la para dar cobertura - o que pode servir como defesa para seus aliados ou servir de oposição ao movimento de alguém - embora isso pudesse ser facilmente representado ao criar uma vantagem (***Fogo de Cobertura***, ou ***Chuva de Balas***, por exemplo).

------

Talvez ache impróprio que Atletismo possa ser usado como defesa contra armas de fogo ou outras armas de alta tecnologia que existam em seu cenário. Na verdade não há outra perícia apropriada para se defender contra isso. Se optar por manter as coisas assim, armas se tornarão bastante perigosas. Você também pode escolher outra perícia para esses casos

------

#### Façanhas para Atirar

-  **Tiro Específico:** Durante um ataque usando Atirar, gaste um ponto de destino e declare uma condição específica que você deseja inﬂigir no alvo, como ***Tiro na Mão***. Se bem-sucedido, você coloca isso como um aspecto de situação além do estresse causado pelo acerto.
-  **Rápido No Gatilho:** Você pode usar Atirar no lugar de Percepção para determinar sua ordem no turno de um conﬂito físico onde atirar rapidamente possa ser relevante.
-  **Precisão Misteriosa:** Uma vez por conﬂito, adicione uma invocação grátis de uma vantagem criada que represente o tempo gasto apontando ou alinhando o tiro (como ***Na Mira***).

- [« Atletismo](../atletismo/)
- [Comunicação »](../comunicacao/)
