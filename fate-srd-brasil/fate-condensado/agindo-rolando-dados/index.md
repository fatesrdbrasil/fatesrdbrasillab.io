---
title: Fate Condensado - Agindo e Rolando os Dados
layout: default
---
# Agindo e Rolando os Dados

Em uma sessão de ***Fate Condensado***, você controlará as ações do personagem que criou contribuindo para a história que vocês estão contando juntos. Em geral, o narrador descreve o mundo e as ações de personagens do narrador (PdN) enquanto que os outros jogadores narraram as ações de seus PJs individualmente.

Para agir, siga o princípio da ***ficção primeiro***: diga o que seu personagem está tentando fazer para _depois_ descobrir quais regras usará. Os aspectos do seu personagem informam o que ele pode tentar fazer e ajudam a definir o contexto para interpretar os resultados. A maioria das pessoas não podem nem tentar realizar uma cirurgia de emergência em um aliado eviscerado, mas com um aspecto estabelecendo um antecedente médico, você poderia tentar. Sem esse aspecto, você poderia no máximo conseguir alguns momentos para as últimas palavras. Em caso de dúvida, verifique com seu narrador e com a mesa.

Como saber se você foi bem sucedido? Frequentemente, seu personagem simplesmente terá sucesso, porque a ação não é difícil e ninguém está tentando impedi-lo. Mas em situações difíceis ou imprevisíveis, você precisará usar os dados para descobrir o que acontece.  

Quando um personagem quiser realizar uma ação, o grupo deverá refletir sobre estas questões:

  - O que está impedindo isso de acontecer?
  - O que poderia dar errado?
  - O que seria interessante acontecer caso desse errado?

Se ninguém tiver boas respostas para todas essas questões, a ação simplesmente acontece. Dirigir para o aeroporto não requer uma rolagem de dados. Por outro lado, dirigir apressado pela rodovia até um avião te esperando enquanto é perseguido por bestas ciberneticamente aprimoradas vindas de outro mundo é o momento perfeito para rolar os dados.

Sempre que você agir, siga estes passos:

1.  Ficção primeiro: descreva o que você está tentando fazer e, só     então, escolha a perícia e a ação adequadas;
2.  Role 4 dados;
3.  Some os símbolos dos dados: um `+`{: .fate_font} vale +1, `-`{: .fate_font} vale -1 e `0`{: .fate_font}     é 0. Isso resultará em um resultado entre -4 a 4;
4.  Some o resultado dos dados com o nível de sua perícia;
5.  Modifique os dados invocando aspectos (mais detalhes [aqui](#invocando-aspectos) e [aqui](../aspectos-pontos-destino/#invocações)) e usando     façanhas (mais detalhes [aqui](#usando-façanhas)).
6.  Declare seu resultado total, chamado de ***esforço***.

## Dificuldade e Oposição

Se a ação de um personagem enfrenta um obstáculo fixo ou tenta alterar o mundo ao invés de um personagem ou criatura, sua ação enfrenta um valor estático de **dificuldade**. Essas ações incluem arrombar fechaduras, bloquear portas e avaliar taticamente um acampamento inimigo. O narrador pode decidir que a presença de certos aspectos (do personagem, da cena, ou qualquer outra coisa) justifica mudanças na dificuldade.

Em outras ocasiões, um oponente fará **oposição** contra a ação do personagem usando a ação de defender (mais detalhes [aqui](#defender)). Nesses casos, o narrador também rolará os dados e seguirá as mesmas regras da seção anterior usando perícias, façanhas ou aspectos que o oponente tenha. Toda vez que você rolar para atacar um oponente ou para criar uma vantagem diretamente contra ele, o oponente rolará para defender-se. 

A oposição pode assumir formas variadas. Lutar com um cultista pela adaga ritualística tem um oponente claro. Ou você poderia ser contraposto pelo poder de um antigo ritual que deve ser superado para salvar o mundo. Arrombar o cofre do Primeiro Banco Metropolitano para acessar as caixas de depósito seguro é um desafio com risco de ser descoberto, mas cabe ao narrador decidir se você está rolando contra a *oposição* dos guardas em patrulha ou pela *dificuldade*  presentada pelo próprio cofre.

## Modificando os Dados

Você pode modificar seus dados invocando aspectos para ganhar +2 na sua rolagem ou para rolar novamente os dados. Algumas façanhas também garantem um bônus. Você também pode invocar aspectos para ajudar um aliado (mais detalhes [aqui](../desafios-disputas-conflitos/#trabalho-em-equipe)) ou para aumentar a dificuldade que um oponente enfrenta.

### Invocando aspectos

Quando você realiza uma ação, mas os dados acabam resultando em um valor baixo, você não precisa aceitar a falha (embora você possa. Isso também é divertido.). Os aspectos em jogo oferecem opções e oportunidades para você ser bem sucedido.

Quando um aspecto puder justificadamente ajudar seus esforços, descreva como ele ajuda e gaste um ponto de destino para ***invocá-lo*** (ou use uma invocação gratuita). O que é ou não é justificável está sujeito à ***regra do “Fala Sério”*** - qualquer um pode dizer *“Fala Sério\!”* para uma invocação de um aspecto. De modo simples, a regra “Fala Sério” é uma **ferramenta de calibração** que qualquer um da mesa pode usar para garantir que o jogo permaneça fiel à sua visão e conceito. Você também pode usar as ferramentas de segurança discutidas [aqui](../sendo-narrador/#ferramentas-de-segurança) de maneira semelhante.

Você tem duas opções quando sua invocação receber um “Fala Sério”. Primeiro, você pode retirar sua invocação e tentar outra coisa ou talvez até invocar um aspecto diferente. Segundo, você pode ter uma breve discussão sobre o porquê você acredita que aquele aspecto funcione. Se mesmo assim a pessoa não for convencida, retire a invocação e bola pra frente. Se ela acabar concordando com seu ponto de vista, vá em frente e invoque o aspecto normalmente. A regra do “Fala Sério” existe para ajudar na diversão de todos da mesa. Use quando algo não soe certo, não faça sentido ou não encaixe no tom da narrativa. Alguém invocando _Ótimo em Primeiras Impressões_ para arremessar um carro seria  provavelmente um “Fala sério”. Mas talvez o personagem tenha uma façanha sobrenatural que o torna incrivelmente forte, o suficiente para plausivelmente arremessar um carro, e esta seja sua manobra de abertura em uma luta contra um monstro terrível. Nesse caso, talvez _Ótimo em Primeiras Impressões_ seja plausível.

> Sinta-se à vontade para adaptar o nome dessa regra para uma gíria ou termo regional que você e seu grupo tenham mais familiaridade.

Cada vez que você invoca um aspecto, você poderá **ganhar um bônus de +2** na sua rolagem, ou rolar novamente todos os seus quatro dados ou ainda adicionar 2 na dificuldade da rolagem de alguém caso seja justificável. Você pode invocar múltiplos aspectos na mesma rolagem, mas não pode invocar o mesmo aspecto várias vezes nela. Há uma exceção: você pode gastar quantas *invocações gratuitas* quiser de um aspecto na mesma rolagem.

Frequentemente você invocará um de seus próprios aspectos. Você pode também invocar um aspecto de situação ou fazer uma invocação hostil de um aspecto de outro personagem (maiores detalhes [aqui](../aspectos-pontos-destino/#invocações-hostis)).

### Usando Façanhas

Façanhas podem dar um bônus para sua rolagem se considerar que você atende aos critérios descritos nela como circunstâncias, ações, ou perícia usada. Você poderá usar a ação de Criar Vantagem (mais detalhes [aqui](#criar-vantagem)) para introduzir aspectos que se alinhem a essas circunstâncias. Lembre-se das circunstâncias das quais suas façanhas podem ser usadas quando  escrever suas ações e prepare-se para o sucesso. 

Normalmente, façanhas dão um bônus de +2 em uma circunstância específica sem custo algum; você pode usá-las sempre que elas se aplicarem à situação. Algumas façanhas raras e excepcionalmente poderosas podem exigir que você pague um ponto de destino para usá-las.

## Resoluções

Sempre que rolar os dados, a diferença entre seu esforço e a dificuldade alvo ou oposição é medida em ***tensões***. Uma tensão tem o valor de 1. Existem quatro possíveis resultados:

  - Se o seu esforço é menor que a dificuldade alvo ou oposição, você **falha**;
  - Se o seu esforço é igual ao alvo, você **empata**; 
  - Se o seu esforço é uma ou duas tensões maiores que o alvo, você obtém **sucesso**;
  - Se o seu esforço é três ou mais tensões maiores que o alvo, você obtém **sucesso com estilo**.

Alguns resultados são obviamente melhores para você do que outros, mas todos devem avançar a história de maneiras interessantes. Você começou com ficção primeiro (mais sobre isso no [início dessa sessão](#agindo-e-rolando-os-dados)); certifique-se que termine com isso também para manter o foco da história e para garantir que o resultado seja interpretado de uma forma que se encaixe na ficção.

> Edson não é um exímio arrombador de cofres (embora ele tenha as ferramentas), e mesmo assim ele está dentro do quartel general de um culto sinistro no qual uma porta de metal está entre ele e o livro de rituais que ele desesperadamente precisa. Conseguirá ele arrombar o cofre?

### Falha

> _Se seu esforço for menor que a dificuldade alvo ou oposição, você falha._

Isso pode acontecer de algumas formas: falha simples, sucesso a um custo maior ou recebendo dano. 

#### Falha simples

A primeira e mais fácil de entender é a ***falha simples***. Você não alcança seu objetivo, não realiza nenhum progresso e deixa a desejar. Garanta que isso mantenha a história andando - simplesmente falhar no arrombamento do cofre é estagnado e entediante.

> Edson puxa a trava do cofre triunfantemente, mas o cofre se mantém fechado enquanto os alarmes disparam. A falha mudou a situação e impulsionou a história adiante - agora há guardas a caminho. Edson se depara com uma nova escolha: tentar outra forma de abrir o cofre, agora que a sutileza não é mais uma opção, ou desistir e fugir?

#### Sucesso a um custo maior

Em segundo lugar, existe o ***sucesso a um custo maior***. Você faz o que definiu que faria, mas existe um preço significativo a pagar: a situação piora ou fica mais complicada. Narrador, você pode tanto declarar que esse é o resultado quanto oferecê-lo no lugar da falha. Ambas opções são boas e úteis em diferentes situações.

> Edson falha na sua rolagem e o narrador diz: “Você ouve um clique do último tambor caindo em seu lugar. Ele ecoa com o clique do cão do revólver de um guarda que te diz para erguer as mãos para o alto”. O custo maior aqui é o confronto com um guarda que ele esperava evitar.

#### Receber um Ataque

Por último, você pode ***levar um ataque***, que deverá ser absorvido com estresse ou consequências, ou sofrer alguma desvantagem. Esse tipo de falha é mais comum ao se defender de ataques ou superar obstáculos perigosos. É diferente de uma falha simples porque somente o personagem, e não necessariamente o grupo todo, é afetado. Também é diferente de um sucesso a um custo maior, já que o sucesso não está necessariamente envolvido.

> Edson foi capaz de abrir a porta do cofre, mas enquanto ele gira a maçaneta, ele sente um golpe nas costas de sua mão. Ele não conseguiu desarmar a armadilha\! Ele escreve a consequência leve: _"envenenado"_.

Você pode misturar essas opções: uma falha nociva pode ser dura, mas apropriada para o momento. Sucesso ao custo de um ferimento é certamente uma opção.

### Empate

> _Se seu esforço é igual a dificuldade alvo ou oposição, você empata._

Assim como na falha, empates devem mover a história adiante e nunca travar a ação. Algo interessante deve acontecer. Semelhante à falha, isso pode ser resolvido de duas formas: sucesso a um custo menor ou sucesso parcial.

#### Sucesso a um custo menor

O primeiro é o ***sucesso a um custo menor*** - alguns pontos de estresse, detalhes da história que dificultam ou complicam, mas não impedimentos em si, ou um impulso (mais detalhes [aqui](../aspectos-pontos-destino/#impulsos)) dado ao inimigo são custos menores.

> Todas as primeiras tentativas de Edson falham. Quando finalmente consegue abrir a porta, o dia amanheceu e escapar com a ajuda da escuridão é impossível. Ele conseguiu o que queria, mas sua situação piorou agora.

#### Sucesso Parcial

A outra forma de lidar com o empate é com um ***sucesso parcial*** - você foi bem sucedido, mas conseguiu somente parte do que queria.

> Edson pôde abrir somente uma pequena fresta do cofre - se a porta abrir mais que alguns centímetros o alarme tocará e ele não sabe como desativá-lo. Ele acaba puxando algumas páginas do ritual através da estreita abertura, mas terá que adivinhar as etapas finais.

### Sucesso

> _Se seu esforço for uma ou duas tensões maior que a dificuldade alvo ou oposição, você obtém sucesso._

Você consegue o que queria sem nenhum custo adicional.

> Aberto\! Edson pega o ritual e foge antes que os guardas o percebam.

Aplicando o princípio da “ação primeiro” ao sucesso.

A ficção _define_ como o sucesso se parece. E se o Edson não tivesse as ferramentas ou experiência necessária para arrombar o cofre? Talvez aquele sucesso estaria mais para nosso exemplo de “custo menor” descrito acima. De forma  imilar, se o Edson estava no time porque ele _construiu_ o cofre, aquele sucesso poderia se parecer mais com nosso exemplo de “com estilo”.

### Sucesso com estilo.

> _Se seu esforço for três ou mais tensões maior que a dificuldade alvo ou oposição - você obtém um sucesso com estilo._

Você consegue o que queria e ganha um pouco mais além disso.

> Edson é mais que sortudo. O cofre abre quase que instantaneamente. Ele não só pega o ritual, como tem tempo o suficiente para vasculhar outros papéis no fundo do cofre. Entre vários registros fiscais e documentos financeiros, ele encontra um mapa da antiga mansão Akeley.

## Ações

Existem quatro ações que você pode usar rolamentos, cada uma com um propósito e efeito específico na história:

  - **Superar:** supere obstáculos com suas perícias;
  - **Criar Vantagem:** crie vantagem que mude a situação em seu     benefício;
  - **Atacar:** ataque para prejudicar o inimigo;
  - **Defender:** defenda-se para sobreviver a um ataque, impedir alguém     de criar uma vantagem, ou se opor ao esforço de superar um     obstáculo.

### Superar

> `O`{: .fate_font}  _Supere obstáculos usando suas perícias_

Todo personagem encontrará desafios imprevistos no decorrer da história. A ação de ***Superar*** mostrará como eles encaram e transpõem tais obstáculos.

Um personagem bom em Atletismo pode escalar muros e correr por ruas aglomeradas de pessoas. Um detetive com Investigação elevada pode conectar pistas que outros não perceberiam. Alguém hábil em Comunicação evitará com facilidade que uma briga comece em um bar hostil;

Os resultados ao Superar são:

  - **Se você falhar**, discuta com o Narrador (ou com o jogador defensor,     caso exista) se isso é uma falha ou um sucesso a um custo maior     (mais detalhes [aqui](#sucesso-a-um-custo-maior))
  - **Se você empatar**, é um sucesso a um custo menor (mais detalhes [aqui](#sucesso-a-um-custo-menor)) - você se     encontra em uma enrascada, a sua oposição ganha um Impulso (mais detalhes [aqui](../aspectos-pontos-destino/#impulsos)) ou você pode sofrer algum dano. Alternativamente você falha, mas     ganha um Impulso.
  - **Se você obtém sucesso**, você consegue o que deseja, e a história     segue em frente sem problemas.
  - **Se você obtém sucesso com estilo**, é um sucesso e você também ganha    um Impulso.

> Carlos se dirigiu até o centro de pesquisa na Antártica. As edificações foram destruídas e os ocupantes desapareceram. Ele deseja procurar por pistas nos destroços. O narrador diz a ele para rolar Investigar contra uma dificuldade Razoável (+2). Carlos obtém `00++`{: .fate_font} nos dados, e somando sua perícia Investigar de nível Regular (+1), resulta em um esforço Bom (+3). Um sucesso\! O narrador descreve a pista que ele encontrou: pegadas na neve, feitas por criaturas que andam sobre pernas finas e inumanas.

Ações de Superar são frequentemente usadas para determinar se um personagem pode acessar ou notar um fato ou pista em particular. Fique de olho nas opções de sucesso a um custo quando for o caso. Se perder um detalhe fizer sua história estagnar, não siga com o resultado de falha, mas sim o sucesso a um custo.

### Criar Vantagem

> `C`{: .fate_font}  _Crie um aspecto de situação ou ganhe um benefício a partir de um aspecto existente._

Você pode usar a ação ***Criar Vantagem*** para mudar o curso da história. Ao usar suas perícias para introduzir novos aspectos ou adicionar invocações em aspectos existentes, você pode virar a maré a seu favor e de seus companheiros de equipe. Você pode mudar as circunstâncias (barrando uma porta ou criando um plano), descobrir  informações adicionais (aprendendo o ponto fraco de um monstro através de pesquisa), ou se aproveitar de algo conhecido (como o gosto de um CEO por uísque).

Um aspecto criado (ou descoberto) através de criar vantagem funciona como qualquer outro: ele define as circunstâncias da narrativa e pode permitir, prevenir ou impedir ações, por exemplo: você não pode ler um feitiço se a sala estiver em _Escuridão Total_. Isso também pode ser invocado (mais detalhes [aqui](../aspectos-pontos-destino/#invocações)) ou forçado (mais detalhes [aqui](../aspectos-pontos-destino/#forçando). Além disso, criar vantagem garante uma ou mais ***invocações gratuitas*** do aspecto criado. Uma invocação gratuita, como o nome indica, permite que você invoque um aspecto sem gastar um ponto de destino. Você pode inclusive permitir que seus aliados usem as invocações gratuitas que você criou. 

Ao rolar para criar vantagem, especifique se você está criando um novo aspecto ou tirando vantagem de um existente. No primeiro caso, você vincula o aspecto a um aliado, oponente ou ao ambiente? Se você o vincular a um oponente, ele pode usar a ação Defender para se opor a você. Caso contrário, você frequentemente enfrentará uma dificuldade estática, mas o narrador pode decidir se algo ou alguém se opõe aos seus esforços com uma rolagem de defesa.

Os resultados ao criar um aspecto são:

  - **Se você falhar**, você não cria o aspecto (falha) ou você o cria, mas     é o inimigo que ganha uma invocação gratuita (sucesso a um custo).     Se você for bem sucedido a um custo, o aspecto final talvez deva ser     reescrito para beneficiar o inimigo. Isso ainda pode valer a pena,     já que aspectos são verdades (mais detalhes [aqui](../aspectos-pontos-destino/#aspectos-são-sempre-verdades)).
  - **Se você empatar**, você não cria o aspecto, mas ganha um impulso     (mais detalhes [aqui](../aspectos-pontos-destino/#impulsos)).
  - **Se você obtém sucesso**, você cria um aspecto de situação com uma     invocação gratuita nele.
  - **Se você obtém sucesso com estilo**, você cria um aspecto de situação e     ganha *duas* invocações gratuitas nele.

Sobre aspectos existentes conhecidos ou desconhecidos, os resultados são:

  - **Se você falhar**, e o aspecto for conhecido, o inimigo ganha uma     invocação gratuita. Se for desconhecido, eles podem escolher    revelá-lo para ganhar uma invocação gratuita.
  - **Se você empatar**, você ganha um impulso se o aspecto for desconhecido     e ele se mantém desconhecido. Se o aspecto for conhecido, você ganha     uma invocação gratuita.
  - **Se você obtém sucesso**, ganhe uma invocação gratuita no aspecto,    revelando-o caso for desconhecido.
  - **Se você obtém sucesso com estilo**, ganhe duas invocações gratuitas    também revelando-o caso for desconhecido.

> Edson está cara a cara com um Shoggoth, uma incansável e enorme fera carnuda. Ele sabe que a criatura é muito poderosa para atacá-la diretamente, então ele decide que é melhor distraí-la, ele anuncia: “Eu gostaria de fazer um coquetel molotov para atear fogo nessa coisa\!”. 
> 
> O narrador decide que acertar o Shoggoth é algo trivial, então essa é uma rolagem de Ofícios - com que rapidez ele pode encontrar e transformar algo inflamável? A dificuldade é definida em Bom (+3). Edson têm Ofício Mediano (+1), mas rolou `0+++`{: .fate_font}, garantindo um esforço de Ótimo (+4).
>
> Edson monta o molotov e o arremessa na besta. O shoggoth está agora _“Em Chamas”_ e Edson ganha uma invocação gratuita nesse aspecto. O Shoggoth está definitivamente distraído, e caso tente correr atrás dele, Edson pode usar essa invocação gratuita para ajudá-lo a escapar.

### Atacar

> `A`{: .fate_font} _Ataque para ferir o inimigo._

A ação ***Atacar*** é como você tenta tirar um oponente de jogo, seja tentando matar um monstro repulsivo ou nocautear um guarda inocente que não sabe a verdade sobre o que está protegendo. Um ataque pode ser: descarregar uma metralhadora, golpear com um belo soco ou realizar um nefasto feitiço.

Tenha em mente se é possível ferir seu alvo ou não. Nem todo ataque é igual. Você não pode simplesmente socar um _kaiju_ esperando machucá-lo. Determine se o seu ataque teria ao menos uma chance de ser bem sucedido antes de rolar os dados. Vários seres poderosos podem ter fraquezas específicas que precisarão ser exploradas ou alguns tipos de defesa que você terá que superar antes de começar a feri-los.

Os resultados de um ataque são:

  - **Se você falhar**, você erra. O ataque é defendido, esquivado ou     simplesmente absorvido pela armadura.
  - **Se você empatar**, talvez você acertou por um triz fazendo o defensor     recuar. De qualquer forma, você ganha um impulso (mais detalhes [aqui](../aspectos-pontos-destino/#impulsos))
  - **Se você obtém sucesso**, você causa um golpe igual à diferença entre o     total do seu ataque contra o esforço da defesa. O defensor deverá    absorver esse golpe com estresse ou consequências ou será tirado de    ação.
  - **Se você obtém sucesso com estilo**, você golpeia exatamente como um    sucesso normal, mas você poderá reduzir a tensão da rolagem em um    para ganhar um impulso.

> Raquel encontrou por acaso um cadáver ressurreto por poderes arcanos para cumprir algum propósito sombrio. Ela decide golpeá-lo. Ela tem Lutar em Ótimo (+4), mas rolou `--00`{: .fate_font}, gerando um esforço Razoável (+2).

### Defender

> `D`{: .fate_font} _Defenda-se para sobreviver a um ataque ou para interferir na ação de um oponente._

Um monstro tenta comer seu rosto? Um inimigo tenta empurrar você para fora do caminho enquanto ele foge da sua fúria? E sobre o cultista que tenta esfaquear seus rins? ***Defender***, defender, defender...

Defender é a única ação reativa no ***Fate Condensado***. Você usa Defender para impedir que algo aconteça com você fora do seu turno, portanto você frequentemente enfrentará uma rolagem de oposição ao invés de uma dificuldade definida. Seu inimigo faz uma rolagem, e você rola imediatamente para se defender, desde que você seja o alvo da ação e/ou possa justificar sua oposição (o que geralmente faz de você o alvo). Aspectos e Façanhas podem oferecer justificativa.

Os resultados ao se defender são:

  - **Se você falhar** contra um ataque, você recebe dano, que você deverá     absorver com estresse ou consequência (mais detalhes [aqui](../desafios-disputas-conflitos/#estresse)). De qualquer modo,     seu oponente foi bem-sucedido em realizar o que desejava, conforme a    ação usada.
  - **Se você empatar**, siga as orientações para empate da ação oposta.
  - **Se você obtém sucesso**, você não sofre dano ou nega a ação da     oposição.
  - **Se você obtém sucesso com estilo**, você não sofre dano, nega a ação     da oposição e recebe um impulso por ter conseguido uma vantagem por     um momento.

> Continuando o exemplo anterior, o cadáver tenta se defender do ataque de Raquel. O Narrador rola `-00+`{: .fate_font}, o que não muda o nível de Atletismo Medíocre (+0) da criatura.
>
> Como o esforço de Raquel foi maior, seu ataque é bem sucedido por duas tensões, e o cadáver está um pouco mais perto de cair e nunca mais se levantar. Tivesse o cadáver rolado melhor, então a defesa teria sido bem-sucedida, e a monstruosidade desmorta teria evitado sofrer o dano.

#### Quais perícias podem ser usadas para Atacar e Defender?

A lista de perícias padrão trabalha com as premissas abaixo:

  - Lutar e Atirar podem ser usadas para realizar ataques físicos;
  - Atletismo pode ser usada para se defender de qualquer ataque físico;
  - Lutar pode ser usada para se defender contra Ataques Físicos corpo a     corpo;
  - Provocar pode ser usada para realizar ataque mental;
  - Vontade pode ser usada para se defender de ataques mentais.

Outras perícias podem receber permissão para Atacar ou Defender dentro de circunstâncias especiais, conforme determinado pelo Narrador ou por um consenso da mesa. Algumas façanhas podem conceder permissão garantida e mais ampla em certas circunstâncias em que geralmente não poderiam ser realizadas. Quando uma perícia não pode ser usada para Atacar ou Defender, mas pode ajudar com isso, prepare o caminho usando ações de Criar Vantagem com ela, e então use as Invocações gratuitas obtidas no seu próximo Ataque ou Defesa. 

------


- [« Começando](../comecando/)
- [Aspectos e Pontos de Destino »](../aspectos-pontos-destino/)
