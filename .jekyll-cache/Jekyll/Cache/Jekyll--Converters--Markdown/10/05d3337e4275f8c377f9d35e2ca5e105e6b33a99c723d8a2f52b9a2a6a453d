I"]<h1 id="6---ações-e-resoluções">6 - Ações e Resoluções</h1>

<h2 id="é-hora-da-ação">É Hora da Ação!</h2>

<p><strong>Você rola os dados quando houver algum empecilho interessante entre você e seu objetivo.</strong> Se não houver nenhuma oposição interessante, você simplesmente consegue realizar o que desejava.</p>

<p>Como falamos nos capítulos anteriores, personagens em partidas de <em>Fate</em> resolvem seus problemas de forma proativa. Jogadores devem realizar muitas ações durante o jogo – invadir o covil do vilão, pilotar uma espaçonave por um campo minado, reunir pessoas para um protesto ou organizar sua rede de informantes para saber tudo o que se passa nas ruas.</p>

<p>Sempre que agir, há uma boa chance de algo ou alguém se meter em seu caminho. A história não seria nada interessante se o vilão aparecesse repentinamente e se entregasse – ele certamente possuirá medidas de segurança memoráveis para manter você distante de seus planos. As minas podem ser instáveis e prestes a explodir ao seu redor. Ou os manifestantes estão com medo da polícia. Alguém pode estar subornando os informantes para que fiquem de bico calado.</p>

<p>É aí que entram os dados.</p>

<ul>
  <li>Escolha a perícia que seja apropriada para a ação.</li>
  <li>Role quatro dados <em>Fate</em>.</li>
  <li>Junte os dados com símbolos iguais. Um <code class="language-plaintext fate_font highlighter-rouge">+</code>  é +1, um <code class="language-plaintext fate_font highlighter-rouge">-</code>  é -1 e um <code class="language-plaintext fate_font highlighter-rouge">0</code>  é 0.</li>
  <li>Adicione o seu nível de perícia à rolagem. O total é o seu resultado de acordo com a escala.</li>
  <li>Se você invocar um aspecto, adicione +2 ao seu resultado ou lance os dados novamente.</li>
</ul>

<blockquote>

  <p>Fräkeline procura rápida uma forma de passar pelos guardas de Nova Quantum. Amanda diz que ela precisa realizar uma ação de superar com oposição passiva (contra uma dificuldade), já que são PdNs simples e não valem um grande conﬂito.</p>

  <p>Maira olha a lista de perícias de Fräk e escolhe Recursos, esperando ter dinheiro suficiente para satisfazê-los. Ela possui Recursos em nível Regular (+1), então adicionará um ao resultado que conseguir nos dados. Ela lança os dados e consegue: <code class="language-plaintext fate_font highlighter-rouge">0-++</code></p>

  <p>Seu total é +2 (+1 nos dados e +1 de sua perícia Regular), o que corresponde a Razoável (+2) na escala.</p>
</blockquote>

<h3 id="oposição">Oposição</h3>

<p>Como dissemos em <a href="../basico/"><em>O Básico</em></a>, ao rolar os dados comparamos o resultado à oposição. A oposição pode ser ativa, o que significa que outra pessoa está rolando os dados contra você, ou passiva, significando que você rolará contra um nível de dificuldade de acordo com a escala que representa a inﬂuência do ambiente na situação. O Narrador deve decidir qual é a fonte de oposição mais aceitável.</p>

<blockquote>
  <p>Amanda decide rolar uma oposição ativa contra Maira, representando os guardas. Ela decide que a perícia mais adequada para isso é Vontade – eles estão tentando resistir à tentação do suborno.</p>

  <p>Os guardas são PdNs sem importância que não possuem nenhuma razão aparente para possuírem um valor alto em Vontade, então ela fornece a eles o nível Medíocre (+0). Ela rola os dados e consegue: <code class="language-plaintext fate_font highlighter-rouge">0+++</code></p>

  <p>… para o incrível resultado de +3!</p>

  <p>Isso fornece um resultado Bom (+3), vencendo o resultado de Maira por um.</p>
</blockquote>

<hr />

<h3 id="narradores">Narradores</h3>

<h3 id="ativa-ou-passiva">Ativa ou Passiva?</h3>

<p>Se um PJ ou um PdN importante puder interferir consideravelmente em alguma ação, então você terá a oportunidade de rolar uma oposição ativa. <em>Isso não consta como uma ação para o personagem opositor; é apenas uma consequência básica da resolução das ações</em>. Em outras palavras, um jogador não precisa fazer nada especial para ganhar o direito de se opor a uma ação contra ele, contanto que o personagem esteja presente e possa interferir. Se houver alguma dúvida, possuir um aspecto de situação apropriado pode ajudar a justificar porque um personagem consegue se opor a alguém.</p>

<p>Se não houver personagens no caminho, então dê uma olhada nos aspectos de situação da cena atual para ver se algum deles justifica algum tipo de obstáculo ou avaliar as circunstâncias (como <em>terreno acidentado</em>, uma <em>fechadura complexa, tempo se esgotando, uma situação complicada</em>, etc.). Se soar interessante, escolha uma oposição passiva por selecionar uma dificuldade na escala.</p>

<p>Algumas vezes você irá lidar com casos mais complicados, como quando algo inanimado parece poder oferecer oposição ativa (como uma arma automatizada) ou um PdN que não tem como oferecer resistência direta (como quando ele não sabe o que os PJs estão fazendo). Siga os seus instintos – use o tipo de oposição que se encaixa à circunstância ou que torna a cena mais interessante.</p>

<hr />

<hr />

<h3 id="narradores-1">Narradores</h3>

<h3 id="o-quão-difíceis-devem-ser-as-rolagens-de-perícias">O Quão Difíceis Devem Ser as Rolagens de Perícias?</h3>

<p>Você não deve se preocupar com a dificuldade da rolagem em casos de oposição ativa – é só usar o nível de perícia do PdN e rolar os dados como os jogadores fariam, deixando o destino resolver a situação. Temos algumas instruções sobre níveis de perícias de PdNs em <a href="../criando-a-oposicao/#os-tipos-de-pdns"><em>Conduzindo o Jogo</em></a>.</p>

<p>No caso de oposição passiva, você precisa decidir qual o nível na escala que o jogador precisa alcançar em sua jogada. É mais uma arte que uma ciência, mas temos algumas ideias que podem lhe ajudar.</p>

<p>Qualquer coisa com dois ou três níveis a mais que a perícia do jogador – perícia Razoável (+2) e oposição Ótima (+4), por exemplo – significa que o jogador provavelmente irá falhar ou precisará invocar aspectos para ser bem-sucedido.</p>

<p>Qualquer coisa com dois ou mais níveis abaixo da perícia do PC – perícia Razoável (+2) e oposição Medíocre (+0), por exemplo – significa que o jogador provavelmente não precisará invocar aspectos e possui uma boa chance de ser bem-sucedido com estilo.</p>

<p>Entre esses valores, há uma boa chance de empatar ou ser bem-sucedido e a mesma chance dele precisar ou não invocar aspectos para tal.</p>

<p>Portanto, dificuldades baixas são melhores quando você quer dar aos PJs uma chance de mostrarem o quanto são incríveis, dificuldades no nível de suas perícias são a melhor forma de prover tensão mas sem exigir demais e altas dificuldades são a melhor forma de enfatizar o quão difíceis ou anormais são as circunstâncias e fazê-los parar para pensar um pouco.</p>

<p>Por último, algumas dicas simples:</p>

<p>Regular leva esse nome por uma razão – se a oposição não possui nada de especial, então a dificuldade não precisa passar de +1.</p>

<p>Se houver ao menos uma razão para a oposição ser mais especial, mas está difícil decidir o nível adequado de dificuldade, escolha Razoável (+2). É algo entre os níveis de perícias dos PJs, então será um desafio interessante para perícias abaixo de Ótimo (+4), além do que é bom dar aos PJs a chance de exibirem suas habilidades mais altas também.</p>

<hr />

<ul>
  <li><a href="../vontade/">« Vontade</a></li>
  <li><a href="../as-quatro-resolucoes/">As Quatro Resoluções »</a></li>
</ul>
:ET