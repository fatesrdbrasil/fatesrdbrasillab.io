---
title: Fate Condensado - Introdução
layout: default
---
# Introdução

Este é o ***Fate Condensado***, uma versão do Sistema ***Fate Básico*** mais compacta possível. Um sistema completo de jogo de interpretação de papéis; enquanto outros livros podem aprimorar seu uso, você não precisará de nenhum outro para jogar. Dito isso, vamos ao que você precisa!

## O que eu preciso para jogar?

Para jogar ***Fate Condensado*** você precisará de dois a seis amigos, um de vocês atuando como Narrador, alguns dados, alguns marcadores, material para escrita, papel e algo para escrever notas curtas (por exemplo, pequenas notas adesivas).

***Fate Condensado*** usa Dados Fate™ quando os personagens decidem agir. Os Dados Fate são dados de seis lados com dois lados em branco com valor igual a `0`{: .fate_font}, dois lados com `+`{: .fate_font}, e dois lados com `-`{: .fate_font}. Um conjunto de quatro dados já é o suficiente, mas um conjunto por jogador é o ideal. Outras alternativas existem, como usar dados comuns de seis lados
(1-2 = `-`{: .fate_font}, 3-4 = `0`{: .fate_font}, 5-6 = `+`{: .fate_font}), ou o Baralho do Destino, que usa cartas ao invés de dados. Usaremos a palavra rolar e variantes durante o texto em nome da simplicidade.

> ### Para Veteranos: mudanças do ***Fate Básico***
> 
> Condensar a essência de um sistema de mais de 300 páginas em menos de 70 páginas de texto leva a algumas mudanças. No momento em que isto é escrito, faz oito anos desde que o Sistema ***Fate Básico*** foi criado, então uma pequena iteração do design deve ser esperada. Em particular, destacamos o seguinte:
>
>  - Mudamos para caixas de estresse de um ponto para ajudar a reduzir confusões ([veja aqui](../comecando/#estresse-e-consequências));
>  - Iniciativa estilo Balsera (também conhecida como "ordem de ação eletiva" ou "iniciativa pipoca") é o padrão ao invés de usar perícias para determinar a ordem de turnos. ([veja aqui](../desafios-disputas-conflitos/#ordem-de-turnos)).
> - A evolução do personagem funciona de um modo um pouco diferente: eliminamos os marcos significativos e ajustamos os maiores (renomeado para progresso) para compensar ([veja aqui](../avanco/)).
>  - Removemos a noção de oposição ativa como algo separado da ação Defender ([veja aqui](../agindo-rolando-dados/#defender)). Isso teve alguns pequenos efeitos em cascata, especialmente no resultado de empate da ação superar ([veja aqui](../agindo-rolando-dados/#superar)).
> - A ação de criar vantagem foi ajustada para oferecer maior clareza e dinamismo na hora de descobrir a existência de aspectos desconhecidos ([veja aqui](../agindo-rolando-dados/#criar-vantagem)).
> - Defesa total é apresentada como regra opcional e foi alterada um pouco para acomodar o escopo expandido da ação defender ([veja aqui](../regras-opcionais/#defesa-total)). Outras alternativas às regras padrão são apresentadas também [aqui](../regras-opcionais/).

------


- [« Início](../)
- [Começando »](../comecando/)
