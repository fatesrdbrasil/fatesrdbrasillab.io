---
title: Fate Condensado - Desafios, Disputas e Conflitos
layout: default
---
# Desafios, Disputas e Conflitos

Muitas vezes durante uma cena, você será capaz de resolver uma ação com uma única rolagem de dados: você arromba o cofre, evita a segurança ou convence o repórter a lhe dar suas anotações? Em outros casos, você enfrentará eventos prolongados que exigem mais rolagens para serem resolvidos. Para esses casos, oferecemos três ferramentas de resolução: ***Desafios, Disputas*** e ***Conflitos***. Cada uma funciona de maneira um pouco diferente, dependendo do tipo de evento e da oposição envolvida.

  - **Um Desafio é uma situação complicada ou dinâmica**. Você terá oposição     de algo ou alguém, mas não existe um “outro lado” dominante. É assim     que você poderia interpretar um pesquisador procurando por pistas em     tomos antigos, o negociador do grupo distraindo o bibliotecário e o     brutamontes impedindo que horrores nomináveis invadam a biblioteca,     tudo ao mesmo tempo.
  - **Uma Disputa é uma situação onde dois ou mais grupos buscam objetivos    mutuamente exclusivos, mas não prejudicam ativamente um ao outro.**    Disputas são perfeitas para perseguições, debates e corridas de    todos os tipos. E o fato das partes envolvidas não estarem tentando    ferir uma à outra, não significa que elas não possam eventualmente    sofrer danos\!
  - **Um Conflito é quando os personagens podem e querem ferir uns aos    outros.** Lutar na lama com um cultista enquanto facadas são    desferidas nas barrigas, perfurar com uma rajada de balas uma horda    de carniçais enquanto eles arranham sua carne com suas garras e uma    troca de farpas maledicentes com seu rival sob o olhar atento da    rainha. Todas essas cenas são conflitos.

## Definindo as Cenas

Independentemente do tipo de cena, o Narrador começa definindo as peças essenciais no seu lugar, de modo que os jogadores saibam quais recursos estão disponíveis e quais complicações estão em jogo.

### Zonas

As ***zonas*** são uma representação do espaço físico: um mapa simples dividido em algumas sessões específicas. Um conflito em uma remota casa de fazenda pode ter quatro zonas: primeiro andar, segundo andar, jardim de frente e bosque aos fundos. Duas a quatro zonas normalmente são o suficiente para lidar com a maioria dos conflitos. Cenas maiores ou mais complexas podem exigir mais zonas. Tente manter seu mapa de zonas em esboço simples, algo que caiba em um cartão de anotações ou possa ser facilmente desenhado em um quadro branco.

As zonas ajudam a guiar a história moldando o que é possível. Quem você pode atacar e para onde você pode se mover dependem da zona onde esteja.

***Qualquer um em uma zona pode interagir com todos ou qualquer coisa dentro dela.*** Isto significa que você pode bater, esfaquear ou se envolver fisicamente com pessoas ou coisas em sua zona. Precisa abrir aquele cofre de parede no quarto? Você deverá estar nessa mesma zona. Qualquer coisa fora da sua zona geralmente está fora do seu alcance; você precisará se mover para lá ou usar algo que possa estender seu alcance até lá (telecinese, uma pistola, etc).

Mover-se entre zonas é fácil, desde que não haja nada em seu caminho. **Você pode se mover para uma zona adjacente além da sua ação durante seu turno (mais detalhes [aqui](#ordem-de-turnos)), desde que nada esteja em seu caminho.** Se sua movimentação está obstruída, será necessário usar sua ação para se mover. Faça uma rolagem de superar para escalar uma parede, correr por um grupo de cultistas ou saltar pelos telhados. Caso falhe, você ficará em sua zona ou mova-se ao custo de algo. Você também pode usar sua ação para se mover para *qualquer lugar* no mapa, entretanto o narrador tem o direito de definir uma dificuldade alta se for um movimento épico.

Se algo não é arriscado ou interessante o suficiente para merecer uma rolagem, então não é um impedimento ao seu movimento. Por exemplo, você não precisa usar sua ação para abrir uma porta destrancada, pois isso já faz parte do movimento.

Atirar te permite atacar à distância. Ataques desse tipo podem atingir inimigos em zonas adjacentes ou até mais longe, caso as zonas não ofereçam obstáculos no caminho. Se há uma criatura revirando o quarto de cima, localizado no fim do corredor, você não pode atirar estando ao pé da escada. Preste atenção na maneira como as zonas e os aspectos de situação são definidos para decidir o que é justo no jogo ou não.

### Aspectos de Situação

Ao definir uma cena, o narrador deverá pensar em características ambientais interessantes e dinâmicas que restrinjam a ação ou forneçam oportunidades para mudar as situações ao usá-las. Três a cinco detalhes são mais que suficientes. Use essas categorias como um guia:

  - **Tom, humor ou clima** - escuridão, relâmpagos ou ventos uivantes;
  - **Restrições à movimentação** - escadas conectando as zonas, coberto de     lodo e cheio de fumaça;
  - **Coberturas e obstáculos** - veículos, pilares ou caixotes;
  - **Características perigosas** - caixas de TNT, barris de petróleo e     estranhos artefatos estalando com eletricidade;
  - **Objetos utilizáveis** - armas improvisadas, estátuas ou estantes de    livros para derrubarem ou portas para bloquearem.

Qualquer um pode invocar e forçar esses aspectos, então lembre-se de levá-los em conta quando lutar com aquele cultista pelo chão no meio do *Lodo Cáustico que Cobre Tudo*.

Mais aspectos de situação podem ser escritos à medida que a cena se desenrola. Se fizer sentido que haja *Sombras Profundas* nas alcovas das catacumbas, vá em frente e escreva isso quando um jogador perguntar se existem sombras que eles podem usar para se esconder. Outros aspectos entram em jogo porque os jogadores usam a ação criar vantagem. Coisas como *Chamas Por Todos os Lados* não acontecem simplesmente sem ação de um personagem. Bem, geralmente não.

#### Invocações gratuitas nos aspectos de cena?

Cabe ao narrador decidir se um aspecto de situação que surge da definição da cena fornecerá uma invocação gratuita aos jogadores (até mesmo aos PdN). Alguns dos aspectos de cena podem fornecer a um esperto jogador justamente a vantagem de que ele precisa naquele momento, e uma invocação gratuita pode ser um grande incentivo para levar os  jogadores a interagir com o ambiente. Invocações gratuitas também podem estar nos aspectos de cena desde o começo dela, devido a preparações feitas com antecedência.

#### Aspectos de Zonas

Conforme mencionado [aqui](#zonas), alguns aspectos de situação podem se aplicar a zonas específicas no mapa e não em outras. Assim adiciona-se uma textura extra, oportunidade e desafio ao mapa que talvez fariam falta.

### Ordem de turnos

Geralmente, você não precisará saber quem está agindo e precisamente quando, mas em disputas e conflitos a ordem de turnos pode se tornar importante. Essas cenas ocorrem em uma série de ***turnos***. Em um turno, cada personagem envolvido pode realizar uma ação de Superar, Criar Vantagem ou Atacar, e pode se mover uma vez (as disputas funcionam de maneira um pouco diferente, veja mais [aqui](#disputas)). Como Defender é uma reação à ação de outra pessoa, personagens podem se defender quantas vezes for necessário durante os turnos de outros personagens, desde que possam justificar sua capacidade de interferir baseado no que já foi estabelecido na história.

No início de uma cena, o Narrador e os jogadores decidem quem será o primeiro a agir baseados na situação e então o jogador ativo escolhe quem vai em seguida. Os personagens do Narrador agem na ordem de turnos, assim como os Jogadores, com o Narrador decidindo quem vai em seguida após os PdN terem agido. Depois que todos jogaram seu turno, o último jogador escolhe quem vai agir em seguida no início da próxima rodada.

> Carolina e Raquel descobriram por acaso um pequeno grupo de cultistas, liderados por um acólito com uma máscara dourada, realizando algum ritual arcano. Como os cultistas estão focados em seu trabalho, o Narrador declara que os jogadores agirão primeiro nesse conflito. Os jogadores decidem que Carolina agirá primeiro: ela cria uma vantagem contra o cultista mascarado, correndo e gritando na direção deles. Agora ele está *Distraído*. Simples, mas eficaz. Para que esse Aspecto de Situação seja melhor usado, o jogador de Carolina decide que Raquel deve agir em seguida. Raquel arremessa uma adaga contra o acólito mascarado e imediatamente invoca o Aspecto *Distraído* para melhorar seu ataque. Não é o suficiente para tirar o acólito de ação com um só golpe, mas essa jogada dupla deixa o cultista cambaleando.
>
> Infelizmente, agora que todos os PJs na cena agiram, Ruth não tem escolha a não ser escolher um dos cultistas para agir em seguida. Ela escolhe o acólito mascarado. O Narrador sorri, pois sabe que assim que o acólito agir, ele pode fazer os cultistas agirem até o final da rodada, e então eles poderão decidir que o acólito mascarado inicie a próxima rodada. Os PJs podem ter acertado um bom primeiro golpe, mas agora os cultistas podem revidar.

Esse método de determinar a ordem de turnos tem vários nomes em discussões online: Ordem de Turnos Eletiva, Iniciativa Pipoca, Passar o Bastão ou “Estilo Balsera”, a última em homenagem a Leonard Balsera, um dos autores do ***Fate Básico***, que plantou a semente dessa ideia. Você pode aprender mais sobre esse método e suas estratégias (em inglês) em <https://www.deadlyfredly.com/2012/02/marvel/>

## Trabalho em Equipe

O Fate oferece três métodos para o trabalho em equipe: combinar a mesma perícia de vários personagens em uma única rolagem, empilhar invocações gratuitas criando vantagens para facilitar o êxito de um membro da equipe, e invocar Aspectos a favor de um aliado.

Ao combinar perícias, descubra quem tem o melhor nível da perícia entre todos os envolvidos. Cada participante adicional que tenha ao menos Regular (+1) nessa perícia adiciona +1 ao nível de perícia do personagem com perícia mais alta. Oferecer suporte dessa maneira usa sua ação do turno. Os apoiadores sofrem os mesmos custos e consequências da pessoa que realiza a rolagem de dados. O bônus total máximo que uma equipe pode fornecer dessa forma é igual ao nível mais alto de perícia da pessoa com maior pontuação.

De outro modo, você pode criar uma vantagem em seu turno e permitir que um aliado use as invocações gratuitas quando fizer sentido em utilizá-las. Fora do seu turno, você pode invocar um Aspecto para adicionar um bônus nas rolagens de outros personagens.

## Desafios

Muitas das dificuldades que seus personagens irão encarar durante uma cena podem ser resolvidas com apenas uma rolagem: desarmar uma bomba, encontrar o tomo da sabedoria anciã ou decifrar um código. Mas às vezes as coisas são mais fluídas, mais complicadas, e não tão simples quanto encontrar o tomo da sabedoria anciã porque o iate que você está procurando está ancorado no porto de Hong Kong enquanto uma ventania furiosa está do lado de fora e a biblioteca do barco está pegando fogo, algo que claramente não é sua culpa.

Em circunstâncias complicadas, sem oposição, você pode usar um ***desafio***: uma série de rolagens de superar que tentam lidar com um problema maior. Os desafios permitem que todo o grupo trabalhe em conjunto em uma cena e mantém as coisas dinâmicas. 

Para definir um Desafio, o Narrador considera a situação e escolhe uma série de perícias que podem contribuir para o sucesso do grupo. Trate cada ação como uma rolagem separada de Superar. As ações de trabalho em equipe são permitidas, mas podem trazer custos e complicações, como falta de tempo ou resultados ineficazes.

Narradores, façam o possível para dar a cada personagem na cena uma oportunidade de contribuir. Foque em um número de perícias a serem usadas igual ao número de personagens envolvidos. Reduza esse número se você acha que alguns dos personagens poderão ser retirados de ação ou distraídos por outras prioridades, ou se quiser abrir espaço para trabalho em equipe. Para desafios mais complicados, construa o desafio com mais ações necessárias do que personagens disponíveis, além de ajustar as dificuldades das ações.

Após as rolagens, o narrador avalia os sucessos, falhas e custos de cada ação conforme interpreta como a cena prossegue. Pode ser que os resultados levem a outro desafio, uma disputa ou mesmo um conflito. Uma combinação de sucessos e falhas deve permitir que os personagens avancem com uma vitória parcial enquanto enfrentam novas complicações entrelaçadas.

## Disputas

Uma ***disputa*** ocorre quando dois ou mais lados estão em oposição direta, mas não estão em um conflito. Isso não significa que um lado *não queira* ferir o outro. Um exemplo de disputa pode ser um grupo tentando escapar
de uma ameaça antes que ela impeça qualquer chance de vitória.

No começo de uma disputa, todos os envolvidos declaram suas ações e o que esperam alcançar. Se mais de um PJ estiver envolvido, eles podem estar do mesmo lado ou em lados opostos, dependendo dos objetivos, por exemplo: em uma prova de atletismo, provavelmente será cada um por si. **Em uma disputa os PJs não podem ou não estão tentando ferir o inimigo.** Ameaças externas como uma erupção de um vulcão ou a ira de um deus, podem atacar qualquer um ou todos os lados. Essas ameaças também podem estar envolvidas na disputa.

Disputas ocorrem em uma série de rodadas. Cada lado realiza uma ação de superar para realizar algo visando alcançar seu objetivo em cada turno. Apenas um personagem de cada lado realiza essa rolagem em cada turno, mas seus aliados podem tanto oferecer bônus de Trabalho em Equipe ou Criar Vantagens para ajudar, (o que traz alguns riscos - veja abaixo). As ações de superar podem ter dificuldades passivas, como no caso de estarem enfrentando dificuldades ambientais distintas ou comparando entre si os resultados quando estiverem em oposição direta.

Ao final de cada rodada, compare os esforços das ações de cada lado. O lado com maior esforço marca uma vitória. Se o vencedor tiver sucesso com estilo (e ninguém mais), ele marca duas vitórias. O primeiro lado a marcar três vitórias vence a disputa (você sempre pode decidir, em vez disso, realizar disputas extendidas que exijam mais vitórias, embora não recomendamos mais de cinco).

Quando há um empate para o maior esforço, ninguém marca vitórias e uma ***reviravolta inesperada*** acontece. O Narrador introduzirá um novo Aspecto de Situação que refletirá como a cena, terreno ou situação mudou.

Em Disputas em que uma ameaça tenta ferir qualquer um dos competidores, todos de um dos lados sofrem um golpe quando a sua rolagem for menor que a rolagem de ataque da ameaça ou a dificuldade estática. Eles sofrem tensões de dano igual às tensões da falha. Assim como em um conflito, se um personagem não consegue absorver todas as tensões de um dano, ele é tirado de ação.

### Criando Vantagens durante uma Disputa

Durante uma disputa, seu lado pode tentar criar vantagens antes de realizar sua rolagem de superar. O objetivo, ou qualquer um que possa razoavelmente interferir, pode se opor com uma rolagem de defesa normalmente. Cada participante pode tentar criar uma vantagem além de rolar ou oferecer um bônus de trabalho em equipe (mais detalhes [aqui](#trabalho-em-equipe)). Se você falhar ao criar vantagem, você tem uma opção: ou seu lado abdica a rolagem de superar, ou você pode sofrer um sucesso a custo (preservando sua rolagem de superar ou o bônus de trabalho em equipe) dando ao outro lado uma invocação gratuita. Se você ao menos empatar, proceda normalmente com sua rolagem ou bônus de trabalho em equipe.

## Conflitos

Quando os heróis lutam de verdade, seja com as autoridades, cultistas, ou algum horror inominável, e podem vencer, você tem um ***conflito***. Em outras palavras, use conflitos quando a violência e a coerção forem meios razoáveis para os fins dos PCs.

Conflitos podem parecer os mais diretos, afinal de contas, a história dos jogos de RPGs é toda construída em cima de simuladores de combate. Mas tenha em mente uma parte importante de sua descrição: os personagens envolvidos têm a capacidade de *ferirem uns aos outros*. Se isso for unilateral (por exemplo: digamos que você tente socar uma montanha viva) não há a menor chance de você feri-la. Isso não é um conflito. Isso é uma Disputa, provavelmente onde os personagens estão tentando fugir ou encontrar meios de contra-atacar.

Conflitos podem ser físicos ou mentais. Conflitos físicos podem ser tiroteios, duelos de espada ou o atropelamento de seres extradimensionais com caminhões. Conflitos mentais incluem discussões com entes queridos, interrogatórios e ataques profanos sobre a sua mente.

A escolha do tempo é importante ao usar algumas formas de Trabalho em Equipe (mais detalhes [aqui](#trabalho-em-equipe)). Você pode invocar um Aspecto em nome de seu aliado para melhorar seu resultado a qualquer momento. Você pode ajudar um aliado *antes do turno dele começar* criando uma vantagem ou dando um bônus de +1 em sua ação. Se ele agiu antes de você neste turno, você não pode criar uma vantagem para ajudá-lo, mas você pode usar seu turno (pulando esse turno) para oferecer a ele um bônus de +1 de Trabalho em Equipe.

### Recebendo dano

Quando um ataque é bem sucedido, o defensor deve absorver o dano, que é igual a diferença entre o esforço do Atacante e o esforço do Defensor.

Você pode absorver tensões de Dano marcando caixas de Estresse e sofrendo Consequências. Se você não puder ou não desejar absorver todas as tensões, você é ***tirado de ação*** (mais detalhes [aqui](#sendo-tirado-de-ação): você é removido de cena e o atacante é quem decide o que acontecerá.

> Uma série de decisões lamentáveis colocaram Carlos em um porão úmido, confrontando um carniçal que deseja muito devorá-lo. O Carniçal ataca golpeando com suas garras afiadas; esse Ataque usa sua perícia Lutar de nível Razoável (+2). O Narrador rola `00++`{: .fate_font}, aumentando o esforço para Ótimo (+4). Carlos tenta pular fora do caminho com seu nível Bom (+3) em Atletismo, mas rola `000-`{: .fate_font} reduzindo seu esforço para Razoável (+2). Como o esforço de ataque do carniçal foi dois passos maior que o esforço de defesa de Carlos, Carlos deve absorver duas tensões. Ele marca as duas primeiras de suas três caixas de estresse físico: o combate já está se mostrando perigoso.

#### Estresse

Em poucas palavras, o ***Estresse*** é uma “armadura narrativa”. É um recurso usado para manter seu personagem ativo e em condições de lutar quando seus inimigos o atingem. Quando você marca caixas de Estresse para absorver um golpe, é como dizer “essa foi por pouco” ou “caramba, essa me deixou sem ar, mas estou bem”. Entretanto, é um recurso limitado: a maioria dos personagens tem apenas três caixas de estresse físico e três de estresse mental, embora personagens com valores mais altos de Vontade e Vigor tenham mais.

Você encontrará duas ***Barras de Estresse*** na sua ficha de personagem, uma para o dano físico e outra para dano mental. Quando você recebe um golpe, você pode marcar caixas vazias de estresse do tipo apropriado para absorver o dano e permanecer na luta. Cada caixa de estresse que você marca absorve uma tensão. Você pode marcar várias caixas de estresse, se necessário.

Essas caixas são binárias: ou estão vazias e podem ser usadas ou estão marcadas e não podem ser usadas. Mas tudo bem, você limpará as barras de estresse assim que a cena terminar (desde que os monstros não o comam primeiro).

#### Consequências

***Consequências*** são novos Aspectos que você escreve em sua ficha de personagem quando seu personagem recebe um golpe, representando o dano real e os ferimentos que seu personagem sofre.

Quando você sofre uma consequência para absorver um golpe, escreva um aspecto em uma caixa de consequência vazia que descreva o dano ocorrido ao seu personagem. Use a gravidade das consequências como um guia: se você foi mordido por uma Cria Estelar, uma consequência suave seria *Mordida feia*, enquanto uma consequência moderada seria *Mordida que não para de sangrar*, e uma consequência severa seria *Perna Aleijada*.

Enquanto o estresse transforma o golpe em um quase acerto, sofrer uma consequência significa que você foi atingido com força. Por que você sofreria uma consequência? Porque às vezes o estresse não é o bastante. Lembre-se: você tem que absorver toda a tensão do golpe para permanecer no conflito. Você só tem algumas caixas de estresse. A boa notícia é que as consequências podem absorver golpes realmente grandes.

Cada personagem começa com três caixas de consequências: uma suave, uma moderada e uma severa. Sofrer uma consequência suave absorve duas tensões, uma moderada absorve quatro tensões e uma severa absolve seis tensões.

Portanto, se você sofrer um grande golpe de cinco tensões, você poderá absorver tudo usando apenas uma caixa de estresse e uma caixa de consequência moderada. Isso é muito mais eficiente do que gastar cinco das suas caixas de estresse.

A desvantagem das consequências é que elas são aspectos, e aspectos sempre são verdades (mais detalhes [aqui](../aspectos-pontos-destino/#aspectos-são-sempre-verdades)). Então, se você possui a consequência *Barriga Baleada*, isso significa que você não poderá fazer coisas que uma pessoa baleada na barriga não faria (como correr rápido). Se as coisas ficarem realmente complicadas devido a isso, você também pode até mesmo receber um forçar aspecto em sua consequência. E assim como os aspectos que você cria quando usa a ação Criar Vantagem, o personagem que criou a consequência, isto é, quem te atingiu, recebe uma invocação gratuita nessa consequência. Ai que dor\!

> Carlos ainda está lutando contra o carniçal. A criatura o ataca com suas garras, dessa vez conseguindo `00++`{: .fate_font}, adicionando o seu Lutar Razoável (+2), e invoca seu aspecto *Fome por carne* para um adicional de +2, resultando em um golpe devastador de nível Fantástico (+6). A rolagem `--00`{: .fate_font} de Carlos, adicionado ao seu nível Bom (+3) em Atletismo, resulta apenas em uma defesa Regular (+1); por isso, ele precisa absorver cinco tensões. Ele opta por sofrer uma consequência moderada. Seu jogador e o narrador decidem que o carniçal fez uma *Ferida aberta no peito* de Carlos. Essa consequência absorve quatro das tensões, deixando uma, do qual Carlos absorve com sua última caixa de estresse restante.

#### Sendo Tirado de Ação

Se você não consegue absorver todas as tensões de um dano com estresse e consequências, você é ***tirado de ação***.

Ser tirado de ação é ruim. Quem tirou você de ação decide o que acontece. Em situações perigosas e em confrontos com inimigos poderosos, isso pode significar que você morreu, mas essa não é a única possibilidade. O resultado deve estar de acordo com o escopo e a escala do conflito em questão: você não morrerá de vergonha se perder uma discussão, mas mudanças em sua ficha de personagem (e mais) são possíveis. O resultado também deve caber dentro dos limites estipulados pelo grupo: se o grupo entende que os personagens nunca devem ser mortos sem o consentimento do jogador, isso é perfeitamente válido.

Mas mesmo quando a morte está em jogo (é melhor deixar isso claro antes de uma rolagem de dados), o Narrador deve se lembrar que isso geralmente é um resultado chato. Um PJ que foi tirado de ação pode ter sido sequestrado, se perdido, colocado em perigo, ser forçado a sofrer consequências... a lista segue. A morte de um personagem significa que alguém terá que fazer um novo personagem e trazê-lo para a história, mas um destino pior que a morte é limitado apenas pela sua imaginação.

Siga a ficção ao descrever como alguém, ou algo, é retirado de ação. Um cultista foi retirado de ação por uma rajada de tiros de metralhadora? Um borrifo de gotas vermelhas enche o ar antes de caírem molhando o chão. Você foi arremessado do caminhão quando ele cruzava o viaduto da Rua 26? Você desaparece saltando pela borda e é esquecido enquanto o conflito continua ao longo da Represa Ryan? Quando estiver discutindo os termos sobre o que ocorre com um personagem ao ser tirado de ação, lembre-se da possibilidade da morte, mas muitas vezes é mais interessante enganar a morte.

> O carniçal acerta com muita sorte, dando um ataque Lendário (+8) contra  a defesa Ruim (-1) de Carlos. Nesse ponto do conflito, todas as caixas de stress de Carlos estão cheias, assim como sua caixa de consequência moderada. Mesmo que ele tome uma consequência leve e uma consequência severa de uma só vez, absorvendo 8 tensões, não seria o suficiente. Como resultado, Carlos foi tirado de ação. O carniçal decide o seu destino. O Narrador poderia em seu direito decidir que o carniçal matasse Carlos ali mesmo... mas ser morto não é o resultado mais interessante.
> 
> Em vez disso, o Narrador declara que Carlos sobreviveu, foi nocauteado e arrastado para o covil da criatura, com as consequências intactas. Carlos acordará perdido e fragilizado no meio da escuridão das catacumbas abaixo da cidade. Como ele foi retirado de ação, Carlos não tem escolha além de aceitar os termos colocados a ele. 

#### Concedendo

Então, como você evita uma morte horrível, ou algo pior? Você pode interromper qualquer ação em um conflito para ***conceder***, desde que os dados não tenham sido rolados ainda. Apenas se renda. Diga a todos que você desiste e que você não consegue seguir adiante. O seu personagem é derrotado e é retirado de ação, mas **você ganha um Ponto de Destino** e mais um ponto extra para cada consequência sofrida nesse conflito.

Além disso, conceder significa que *você* declara os termos de sua derrota e como será tirado de ação. Você pode escapar dos monstros e viver para lutar outro dia. Entretanto, ainda é uma derrota. Você terá que dar ao seu oponente algo que ele queira. Você não pode conceder e descrever como heroicamente salvou o dia: isso não é mais uma possibilidade.

Conceder é uma ferramenta poderosa. Você pode conceder para fugir com um plano de ação pronto para a próxima batalha, uma pista de para onde ir ou alguma vantagem futura. Você simplesmente não pode vencer *essa* luta.

Você deve conceder antes que seu oponente role os dados. Você não pode esperar os resultados dos dados e conceder quando parecer óbvio que você não vai vencer: isso é ser um mau perdedor.

Espera-se alguma negociação aqui. Procure uma solução que funcione para todos na mesa. Se a oposição não concordar com os termos da sua concessão, eles podem pressionar por uma reformulação desses termos ou pedir que você sacrifique algo diferente ou algo extra. Como conceder ainda é uma derrota para você, isso significa que a oposição deve ganhar pelo menos parte do que ela busca.

Quanto mais significativo for o custo que você paga, maior será o benefício que o seu lado deverá receber como parte da concessão: se a derrota certa estiver sobre todo grupo, um membro que conceda pode optar por realizar uma última resistência heróica (e fatal), e isso poderia significar que todos os demais sejam poupados\!

### Terminando um Conflito

Um conflito chega ao fim quando todos de um lado concedem ou são retirados de ação. No final de um conflito, qualquer jogador que concedeu receberá seus pontos de destino pela concessão (mais detalhes [aqui](#concedendo)). O narrador também paga os devidos pontos de destino aos jogadores por invocações hostis (mais detalhes [aqui](../aspectos-pontos-destino/#invocações-hostis)) que aconteceram durante o conflito.

### Recuperando-se de Conflitos

Ao fim de cada cena, cada personagem limpa suas caixas de estresse. As consequências levam mais tempo e esforço para serem removidas.

Para iniciar o processo de recuperação, a pessoa que está tratando você precisará ter sucesso em uma ação de superar com uma perícia apropriada. Machucados físicos normalmente são tratados por conhecimentos médicos via Conhecimentos, enquanto consequências mentais são tratadas com Empatia. Essa ação de superar enfrenta dificuldade igual a gravidade da consequência: Razoável (+2) para consequência suave, Ótima (+4) para moderada e Fantástica (+6) para severa. Estas dificuldades aumentam em dois quando você tenta tratar a si mesmo (é mais fácil ter outra pessoa fazendo isso).

Se você for bem-sucedido nesta rolagem, reescreva a consequência para indicar que ela está sendo curada. Um Braço Quebrado pode ser reescrito como Braço Engessado, por exemplo.

O Sucesso aqui é somente o primeiro obstáculo - leva tempo para recuperar de uma consequência.

  - Consequências **leves** demoram uma cena inteira após o tratamento para     serem removidas.
  - Consequências **moderadas** duram mais tempo, levando uma sessão inteira    após o tratamento para serem removidas.
  - Consequências **severas** são removidas somente quando você atinge um    progresso (mais detalhes [aqui](../avanco/#progressos)) após o tratamento.

------

- [« Aspectos e Pontos de Destino](../aspectos-pontos-destino/)
- [Avanços »](../avanco/)
