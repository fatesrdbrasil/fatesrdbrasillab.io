I"&.<h2 id="o-que-é-uma-cena">O Que É Uma Cena?</h2>

<p>Uma <strong>cena</strong> é uma unidade de tempo de jogo que dura de alguns minutos a meia hora ou mais, durante a qual os jogadores tentam alcançar um objetivo ou realizar algo significativo no cenário. O conjunto de cenas jogadas formam uma sessão de jogo e, por extensão, também vão formando partes dos cenários, arcos e campanhas.</p>

<p>Assim, você pode olhar para as cenas como a unidade básica de tempo do jogo, além de provavelmente já ter uma boa ideia de como elas são. Não é muito diferente de uma cena em um filme, série de TV ou livro – os personagens principais estão fazendo coisas o tempo todo, normalmente todos no mesmo espaço. Uma vez que a ação siga um novo objetivo, mude de local relacionado àquele objetivo ou salta no tempo, você está na próxima cena.</p>

<p>Como Narrador, um dos trabalhos mais importantes é administrar o início e o fim das cenas. A melhor forma de controlar o ritmo do que acontece em suas sessões é mantendo um controle rígido sobre quando as cenas começam e terminam – deixe as coisas ﬂuírem enquanto os jogadores estiverem investindo na cena e envolvidos nela, mas assim que as coisas começarem a perder o ritmo, siga para a próxima cena. Nesse caso, você pode se inspirar no que os bons editores de filme fazem – você “corta” uma cena e começa uma nova, garantindo assim que a história continue suavemente.</p>

<h3 id="iniciando-uma-cena">Iniciando Uma Cena</h3>

<p>Ao iniciar uma cena, deixe duas coisas claras:</p>

<ul>
  <li>Qual o propósito da cena?</li>
  <li>Que coisas interessantes estão prestes a ocorrer?</li>
</ul>

<p>Responder a primeira pergunta é bastante importante, pois quanto mais específico for o propósito de sua cena, mais fácil será para saber quando ela tem que finalizar. Uma boa cena gira em torno de resolver um conﬂito específico ou atingir um alvo específico – uma vez que os PJs forem bem-sucedidos ou falharem no que quer que estejam tentando, a cena acaba. Se a sua cena não possuir um propósito claro, você corre o risco de torná-la mais longa que o necessário e acabar com o ritmo da sessão.</p>

<p>Na maioria das vezes, os próprios jogadores lhe dirão qual o propósito da cena, porque eles sempre estarão dizendo a você o que desejam fazer a seguir. Então se eles disserem “Bem, vamos até o esconderijo do ladrão para ver se podemos botar uma pressão nele”, você saberá o propósito da cena – ela acaba quando os PJs conseguirem pressionar o ladrão ou acabará se os personagens se meterem em uma situação que impossibilite-os se cumprir esse objetivo.</p>

<p>Às vezes, porém, eles serão bastante vagos sobre isso. Se você não possuir um certo instinto para notar suas intenções, faça perguntas até que eles digam a você diretamente o que desejam. Então se um jogador disse “Certo, vou até a taverna para encontrar o meu contato”, isso pode soar um pouco vago – você sabe que haverá um encontro, mas não sabe do que se trata. Você pode perguntar “Qual o seu interesse em encontrá-lo? Você já negociou algum preço para a informação que deseja?” ou outra questão que ajude o jogador a destrinchar sua vontade.</p>

<p>Outras vezes você terá que criar um propósito para a cena, como o início de um novo cenário ou a cena logo depois de um momento de suspense. Independentemente de como faça isso, tente voltar as perguntas sobre a história que criou mais cedo e introduzir uma situação que contribua diretamente para responder alguma delas. Dessa forma, sempre que for começar uma cena, estará movendo a história adiante.</p>

<blockquote>
  <p>Amanda terminou a sessão anterior com um suspense: o empregador misterioso de Fräkeline é um agente dos Bruxos de Mondoor, e o Cetro de Coronilla é uma joia necessária para o ritual que trará a bruxaria de volta. Além disso, Bandu é capturado pela guarda do Museu, que o acusa de ter roubado o Cetro, curiosamente a mesma relíquia que Fräk havia sido enviada para roubar.</p>

  <p>Agora Amanda pensa sobre como vai iniciar a próxima sessão. Toda a situação da última sessão assustou os jogadores, então ela definitivamente vai focar nisso. Anya retornará, confusa sobre o papel de Fräk no roubo. A cena será um uma revelação, mostrando que ambas estão do mesmo lado.</p>
</blockquote>

<p>Há outra questão importantíssima – uma cena deve começar <em>logo antes</em> de algo interessante acontecer. Filmes e séries são especialmente bons nisso – normalmente, você não assiste determinada cena por mais de três segundos antes que algo interessante aconteça para mudar a situação e abalar as coisas.</p>

<p>“Cortar” direto para um momento antes do início da ação ajuda a manter o ritmo e chamar a atenção dos jogadores. Não é preciso detalhar cada momento da caminhada dos PJs da estalagem até o esconderijo do ladrão – isso é perder tempo de jogo onde nada interessante acontece. Ao invés disso, você pode começar a cena com os PJs na porta do esconderijo, frente a uma série de intricadas fechaduras na porta do ladrão.</p>

<p>Se você sentir dificuldades nesta questão, pense em algo que possa complicar o propósito deles ou criar um problema. Você também pode usar o truque ninja que falamos logo antes e fazer perguntas aos jogadores para ajudá-lo a elaborar o que acontecerá de interessante a seguir.</p>

<p>Se você possuir um propósito claro em cada cena e começar quando algo significativo estiver para acontecer, é pouco provável que algo saia errado.</p>

<blockquote>
  <p>Amanda inicia a cena com Fräkeline e Esopo voltando para seu alojamento tarde da noite, conversando distraidamente sobre os eventos recentes. Léo sugere que eles não devem ir para lá – não após o roubo. Ele imagina que todos, desde os Bruxos de Moondor ao Império Mareesiano estão procurando por Fräkeline, então eles estão em algum tipo de esconderijo seguro.</p>

  <p>Então são surpreendidos por três estranhos armados que os emboscam assim que chegam próximo à porta.</p>

  <p>“Uou!” Maira pergunta “Como eles sabem que estamos aqui?”</p>

  <p>“Difícil dizer”, Amanda fala, fornecendo um ponto de destino para Maira e Léo. “<strong><em>Este é um Centro de Comércio, Antro de Vilania</em></strong>”.</p>

  <p>“Justo”, Léo diz e ambos aceitam forçar o aspecto.</p>

  <p>“Fräk, antes que consiga entrar para o local um dos indivíduos segura uma espada contra sua garganta. O capuz cai aos poucos e revela Anya! Ela parece brava quando diz “Onde está o Cetro, escória imperial?”.</p>
</blockquote>

<h3 id="finalizando-cenas">Finalizando Cenas</h3>

<p>Você pode finalizar as cenas da mesma forma que as iniciou, mas em um processo contrário: no momento em que notar que a cena alcançou o seu propósito, passe adiante e chame o fim da cena imediatamente após as ações interessantes terminarem.</p>

<p>Esta é uma abordagem bastante eficiente porque ajuda a manter o interesse na <em>próxima</em> cena. Mais uma vez, você vê isso o tempo todo em bons filmes – uma cena termina com parte da ação resolvida, mas também com algum assunto inacabado; é bem aí que cortam para a próxima cena.</p>

<p>Muitas de suas cenas acabarão da mesma forma. Os PJs podem vencer um conﬂito ou alcançar um objetivo, mas sempre haverá algo a ser feito a seguir – conversar sobre a resolução, descobrir o que vão fazer em seguida, etc.</p>

<p>Ao invés de prolongar a cena, no entanto, faça uma sugestão para eles seguirem para uma nova, o que pode ajudar a responder uma das questões não resolvidas na cena atual. Tente fazer com que revelem o que desejam fazer na próxima cena e então volte às duas questões para iniciar uma cena que mostramos acima – qual o propósito da próxima cena e qual a ação iminente interessante que pode acontecer? Parta diretamente para isso.</p>

<p>A única restrição com relação a isso é se os jogadores estiverem muito, mas muito empolgados mesmo com as interações na cena atual. Às vezes as pessoas se empolgam em algo e não conseguem parar e você não deve se preocupar se as coisas tomarem esse rumo. Se o interesse pela cena começar a cair, aproveite a chance para intervir e perguntar sobre a cena seguinte.</p>

<h3 id="os-pilares-competência-proatividade-e-drama">Os Pilares (Competência, Proatividade e Drama)</h3>

<p>Sempre que estiver tentando criar ideias para o que deve acontecer na próxima cena, pense mas ideias básicas do <em>Fate</em> que apresentamos no capítulo <a href="../basico/"><em>O Básico</em></a> – competência, proatividade e drama.</p>

<p>Em outras palavras, pergunte-se se sua cena está fazendo ao menos uma das seguintes coisas:</p>

<ul>
  <li>Dando aos PJs a chance de mostrarem no que eles são realmente bons, seja contra pessoas despreparadas ou contra uma boa quantidade de adversários dignos.</li>
  <li>Dando aos PJs a chance de fazer algo que possa ser descrito em uma frase simples. “Tentando descobrir informações” é muito vago; “Invadir o gabinete do prefeito” é mais específico. Não precisa ser uma ação física – “convencer o informante a falar” também é uma ação válida.</li>
  <li>Introduzindo uma escolha difícil ou complicação para os PJs. Sua melhor ferramenta para fazer isso é forçar os aspectos dos PJs, mas se a situação é problemática por si só, talvez não seja necessário.</li>
</ul>

<blockquote>
  <p>O primeiro impulso de Fräkeline é entender o que Anya está falando – mas Amanda sabe que os impulsos de Esopo são… um pouco mais violentos.</p>

  <p>“Eu atiro com minha lança cascavéis!”, Léo grita.</p>

  <p>“Mas… nós nem começamos a conversar”, diz Maira. “Mesmo assim! Para que conversar quando <strong><em>Bater Sempre Funciona</em></strong>?”</p>

  <p>Léo abre a mão e Amanda cede um ponto de destino por forçar o aspecto.</p>
</blockquote>

<h3 id="use-os-aspectos">Use os Aspectos</h3>

<p>Outro bom jeito de elaborar cenas interessantes de ação é usar os aspectos dos jogadores para criar uma complicação ou evento baseados neles. Isso é especialmente adequado para aqueles aspectos dos PJs que você não incluiu no problema do cenário, porque isso muda um pouco o foco para eles, mesmo que a história principal não foque nos PJs tanto assim.</p>

<blockquote>
  <p>A cena se inicia com um grande julgamento. Bandu está preso, parado em pé diante dos oficiais do Museu do Consenso. Enquanto eles o cobrem de perguntas, eventualmente um dos estudiosos presentes no salão lança um insulto ou uma palavra de desânimo (o que rende um ponto de destino por seu <strong><em>Me Ofendo Por Pouco</em></strong>). A coisa toda parece uma reunião de parlamentares. Fräk e Esopo estão escondidos na ventilação, assistindo tudo a uma distância segura.</p>

  <p>Amanda se vira para os dois e pergunta, “Vocês vão deixar seu companheiro nessa situação?”</p>

  <p>“Não importa. <strong><em>Eu Não Entendo Absolutamente Nada</em></strong>!” grita Léo, narrando como Esopo quebra o teto e cai no centro do salão, na mira dos guardas.</p>

  <p>“Não posso ficar parada depois dessa… porque eu sei que o <strong><em>Esopo Precisa Ser Protegido</em></strong>!”, anuncia Maira, fazendo Fräkeline saltar do telhado no meio da confusão.</p>

  <p>Amanda ri, dizendo “Melhor cena!”, e lança dois pontos de destino, um para Maira e outro para Léo.</p>
</blockquote>

<ul>
  <li><a href="../organizando-a-primeira-cena/">« Organizando a primeira cena</a></li>
  <li><a href="../o-cenario-em-jogo/">O Cenário em Jogo »</a></li>
</ul>

:ET