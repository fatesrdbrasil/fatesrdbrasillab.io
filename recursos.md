---
layout: page
title: Recursos
---

Nessa página você irá encontra recursos úteis para facilitar o desenvolvimento de seus jogos baseados em Fate, além de informação útil em geral para jogadores, narradores e desenvolvedores de jogos baseados em Fate

### Fate Condensado versão _Ashcan_ (sem imagens) - por _Fábio Emilio Costa_

+ [ODP](/assets/downloads/FateCondensadoAshcan.odt) | [PDF](/assets/downloads/FateCondensadoAshcan.pdf) | [EPUB](/assets/downloads/FateCondensadoAshcan.epub) | [Brochura para impressão](/assets/downloads/FateCondensadoAshcan-Brochura.pdf)

### Referência Rápida para o Fate - por _Lu "Cicerone" Cavalheiro_

+ [PDF](/assets/downloads/ReferenciaRapida.pdf) 

### Guia de Referência em Três Colunas para o Fate - por _Fábio Emilio Costa_ e _Lu "Cicerone" Cavalheiro_

+ [PDF](/assets/downloads/guia-tres-colunas-atualizado.pdf) 

### Explicação das Regras do Fate - por [_Fate Masters_](http://fatemasters.gitlab.io/regras/FateCoreUpToFourPlayers/) e _Up to Four Players_

+ [PDF Traduzido](/assets/downloads/RegrasDoFate/RegrasDoFate-UpToFourPlayers.pdf) | [Tradução](http://fatemasters.gitlab.io/regras/FateCoreUpToFourPlayers/)  | [Original](http://fatemasters.gitlab.io/regras/FateCoreUpToFourPlayers/)

### Logo _Movido pelo Fate_

+ **Claro:** [PostScript](/assets/downloads/movido_pelo_fate_fundo_claro-BR.ps) | [PNG](/assets/downloads/movido_pelo_fate_fundo_claro-BR.png) | [SVG](/assets/downloads/movido_pelo_fate_fundo_claro-BR.svg)
+ **Escuro:** [PostScript](/assets/downloads/movido_pelo_fate_fundo_escuro-BR.ps) | [PNG](/assets/downloads/movido_pelo_fate_fundo_escuro-BR.png)  | [SVG](/assets/downloads/movido_pelo_fate_fundo_escuro-BR.svg)

### Sparks em Fate - Tradução para o Português por Paulo Guerche

+ [Monitor](/assets/downloads/Sparks_Fate_Basico-Monitor.pdf)
+ [Impressão](/assets/downloads/Sparks_Fate_Basico-Monitor.pdf)
+ [Livreto](/assets/downloads/Sparks_Fate_Basico-Monitor.pdf)
+ **Ficha para informações do Spark:**
  + [PDF](/assets/downloads/FichaSparksemFate.pdf)
  + [ODP](/assets/downloads/FichaSparksemFate.odp)

### Baby's First Fudge Dice (Diagrama para adaptar _d6_ de pontinhos como Dados Fate):

+ [JPG](/assets/downloads/babys_first_fudge_dice.jpg)

### Grid Solo (para usar geradores randômicos em Fate):

+ [Versão Colorida em PDF](/assets/OraculoFate/GridColorido.pdf)
+ [Versão Para Impressão em PDF](/assets/OraculoFate/GridColorido.pdf)
