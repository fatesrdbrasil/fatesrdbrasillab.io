---
title: Conduzindo o Jogo
layout: default
---

# 8 - Conduzindo o Jogo

## O Que Fazer

Se você é o narrador, então o seu trabalho é um pouco diferente de todos os outros participantes. Este capítulo oferece um conjunto de ferramentas para tornar o seu trabalho mais fácil durante o jogo.

Nós já conversamos um pouco sobre o trabalho do narrador no capítulo inicial, mas vamos detalhar um pouco mais algumas de suas responsabilidades.

### Começando e Terminando Cenas

Uma de suas maiores responsabilidades durante o jogo é decidir, definitivamente, quando uma cena começa e termina. Isso pode não parecer muito importante, mas é, porque significa que você é a pessoa responsável pelo ritmo da sessão. Se começar uma cena muito cedo, a ação principal pode demorar a aparecer. Se não terminá-la a tempo, ela se arrasta e demora até que algo significante aconteça.

Às vezes, os jogadores ajudarão nesse quesito, se estiverem animados para a próxima cena de ação, mas em outras vezes estarão naturalmente inclinados a perder tempo brincando ou se preocupando com detalhes menores. Quando isso ocorrer, é sua função interromper, como um bom diretor, e dizer, "acho que essa cena já deu o que tinha que dar. O que faremos em seguida?

Há mais dicas sobre como começar e finalizar cenas no próximo capítulo, [*Cenas, Sessões e Cenário*](../cenas-sessoes-e-cenario/).

------

### Drama É Bem Melhor Que A Realidade

Quando jogar *Fate*, não se preocupe demais em tentar dar sentido a tudo no mundo ou se apegar a um senso de realismo exacerbado. O jogo funciona através de drama e ficção; use isso a seu favor. Deve haver poucos momentos durante o jogo nos quais os PJs não estarão em conﬂitos ou não terão problemas para resolver, mesmo que a história seja "mais realista" e com mais tempo para descansar.

Ao decidir o que vai acontecer a seguir, e a ideia que fizer mais sentido também for um pouco entediante, então faça algo que pareça mais empolgante do que sensato. Você sempre pode encontrar uma forma de explicar mais tarde algo que não faça sentido imediato.

------

### Interpretando o Mundo e os PdNs

Como narrador, é seu trabalho decidir como tudo e todos no mundo respondem ao que os PJs fazem, assim como a aparência do ambiente. Se os PJs falharem em uma rolagem, é você quem decide a consequência. Quando um PdN tenta assassinar um amigo de algum PJ, é você quem decide como ele fará isso. Quando o PJ se aproximar de um vendedor, você decide se ele acordou com o pé direito hoje, a personalidade que ele ou ela tem ou o que está à venda no dia. Você determina o clima quando os PJs decidem se abrigar em uma caverna escura.

Felizmente, você não precisa tirar isso do nada - há uma boa porção de ferramentas para ajudá-lo a decidir o que é apropriado. O processo que apresentamos em [*Estruturando o Jogo*](../estruturando-o-jogo/) deve fornecer os contextos necessários para sua partida, seja na forma de aspectos ou questões presentes e iminentes, locais específicos para visitar, ou PdNs com motivações fortes que você possa usar.

Os aspectos dos PJs também te ajudarão a decidir como o mundo responderá a eles. Como foi dito no capítulo [*Aspectos e Pontos de Destino*](../aspectos-e-pontos-de-destino/), os melhores aspectos possuem sentido duplo. Você tem muito poder de exploração sobre essa faca de dois gumes, forçando-os a partir de eventos. Dessa forma, você mata dois pássaros com uma pedrada só - isso adiciona detalhes e surpresa ao seu mundo de jogo, mas também mantêm os PJs no centro da história que está contando.

Essa parte do seu trabalho também significa que quando você tem PdNs em cena, você fala e toma decisões por eles assim como os jogadores fazem com seus personagens – você decide quando eles realizam uma ação que exija a rolagem dos dados e você segue a mesma regra usada para os jogadores para determinar quando o turno de um PdN termina. No entanto, seus PdNs serão um pouco diferentes dos PJs, dependendo do quão importantes eles são para a história.

------

### Deixe Os Jogadores Ajudarem

Você não tem que arcar com todo o peso da criação do mundo e seus detalhes. Lembre-se, quando mais colaborativo for o jogo, mais investimento emocional os jogadores terão no resultado, por terem feito parte da criação.

Se um personagem possui um aspecto que o conecte a alguém ou algo no mundo, dê ênfase a esse detalhe. Então se alguém possuir o aspecto ***Cicatrizes da Grande Guerra***, coloque esse jogador como fonte de informação sempre que a Grande Guerra for mencionada. “Você percebe que o sargento veste um símbolo de veterano, uma condecoração rara da Guerra. Que tipo de proeza você precisa fazer pra receber uma dessas? Você tem uma?” Alguns jogadores passarão a bola de volta a você e não há problema nisso, mas é importante que continue a fazer ofertas como essa, para incentivar uma atmosfera colaborativa.

Além disso, um dos principais usos da [ação criar vantagem](../as-quatro-acoes/#c-criar-vantagem) é precisamente dar aos jogadores uma forma de adicionar detalhes ao mundo através de seus personagens. Use isso a seu favor quando sentir que falta algo na história ou simplesmente queira distribuir um pouco do controle narrativo. Uma boa forma de fazer isso durante o jogo é responder às perguntas dos jogadores com outra pergunta, se eles pedirem por informação.

Michel: “Há uma forma de deter esse construto de computadores sem destruir os equipamentos pelos quais ele é formado?”

Amanda: “Bem, você sabe que ele possui uma força vital para se manter. Se houver uma forma de fazer isso, como você acha que é? Digo, você é quem sabe sobre consciência, mente e essas coisas”.

Michel: “Hm... acredito que se ele tem uma consciência, posso ser capaz de encontrar e enviar um comando, como se fosse uma ordem de segurança do próprio sistema dele”.

Amanda: “Sim, soa legal. Faça uma rolagem de Conhecimentos para ver se você consegue fazer isso”.

------

### Julgando as Regras

Também é o seu trabalho tomar decisões sobre o que se enquadra ou não nas regras. Mais frequentemente você decidirá quando é preciso uma rolagem durante o jogo, que tipo de ação está sendo realizada (superar, atacar, etc.) e quão difícil a rolagem será. Em conﬂitos, isso pode se tornar um pouco mais complicado, como determinar se um aspecto de situação pode forçar alguém a realizar uma ação de superar ou decidir se uma ação de criar vantagem pode ser realizada.

Você também julga a validez de atos de invocar e forçar que surgirem no jogo, como já falamos em [*Aspectos e Pontos de Destino*](../aspectos-e-pontos-de-destino/), então certifique-se que todos na mesa estejam esclarecidos sobre como a coisa funciona. Com as invocações as coisas são mais simples – contanto que o jogador possa explicar porque o aspecto é relevante. Quanto a forçar, a coisa pode complicar um pouco mais, pois pode ser necessário conversar om o jogador sobre os detalhes da complicação que ele receberá.

Providenciamos algumas dicas sobre como julgar as regras, logo abaixo.

------

### Você É O Presidente, Não Deus

Olhe para a sua posição como árbitro de regras com o pensamento “sou o primeiro entre iguais”, como em um comitê e não como uma autoridade absoluta. Se houver um desacordo na aplicação de alguma regra, tente encorajar um breve debate e deixe todos se expressarem livremente, ao invés de tomar uma decisão unilateral. Você muitas vezes notará que o próprio grupo se policiará – se alguém tentar forçar algo além do limite, algum outro jogador comentará antes mesmo que você.

Em suma, seu trabalho é dar a “palavra final” em qualquer questão relacionada às regras, em vez de ditar as coisas à sua maneira. Tenha isso em mente

------

### Criando Cenários (E Quase Qualquer Coisa)

Finalmente, você é o responsável por todas as coisas que os PJs encontrarão no jogo. Isso não inclui apenas PdNs e suas perícias e aspectos, mas também inclui os aspectos de cena, ambiente e objetos, assim como os dilemas e desafios que fazem parte de um cenário em *Fate*. Você proverá ao seu grupo as razões para que joguem – quais problemas encararão, quais questões terão que resolver, quem se opõe a eles e o que precisam fazer para vencer tudo isso.

Esse trabalho é mais profundamente abordado em seu próprio capítulo. Veja [*Cenas, Sessões e Cenário*](../cenas-sessoes-e-cenario/).

- [« Trabalho em Equipe](../trabalho-em-equipe/)
- [Durante a Criação do Jogo »](../durante-a-criacao-do-jogo/)

