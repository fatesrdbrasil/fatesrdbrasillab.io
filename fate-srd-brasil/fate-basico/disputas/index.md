---
title: Disputas
layout: default
---

## Disputas

**Quando dois ou mais personagem possuem objetivos mutuamente exclusivos, mas não estão tentando se ferir diretamente, eles estão em uma disputa**. Disputas de queda de braço, corridas ou outras competições esportivas e debates público são bons exemplos de disputas.

Narradores devem se fazer as perguntas a seguir ao organizar uma disputa:

-  Quais são os “lados”? É cada um por si na disputa ou há grupos de pessoas se opondo a outros grupos? Se houver vários personagens lados, eles rolarão juntos usando as regras de [Trabalho em Equipe](../trabalho-em-equipe/).
-  Qual é o ambiente da disputa? Há alguma característica significativa ou notável do ambiente que você queira definir como aspecto de situação?
-  Como os participantes da disputa estão se opondo uns aos outros? Estão rolando uns contra os outros diretamente (como em uma corrida ou partida de pôquer) ou estão tentando superar algo no ambiente (como um obstáculo ou um grupo de jurados)?
-  Quais perícias são apropriadas para a disputa? Todos terão que rolar a mesma perícia ou várias perícias são aplicáveis?

> Bandu foi derrubado em uma batalha contra o grupo obscuro de assassinos que emboscaram ele e Fräkeline nos arredores da cidade. Fräk dá conta do último deles, finalizando o conﬂito, e vai então em direção ao amigo caído.
>
> É aí que o líder dos assassinos, um ladrão que ela conhece bem chamado Valin Vorta, o Célere, aparece com um cinto de teleporte próximo a Bandu, que está caído inconsciente! Ele começa a recarregar seu sistema de teleporte, com a intenção de levar Bandu junto consigo. Fräk começa a correr. Será que ela consegue chegar antes que Valin consiga escapar?
>
> Amanda olha as perguntas acima para organizar a disputa.
>
> A cena do conﬂito anterior teve um aspecto de situação chamado ***Poças de Óleo***, e Amanda continuar assim.
>
> Claramente Valin e Fräkeline estão se opondo diretamente, então oferecerão oposição ativa.
>
> Valin rolará sua perícia Ofícios nessa disputa, pois está utilizando um equipamento. Como a situação requer um movimento simples, Amanda e Maira concordam que Atletismo é a perícia mais apropriada para Fräkeline.

Agora a ação pode começar.

Uma disputa ocorre em uma série de **rodadas de altercação**. A cada rodada, cada participante faz uma rolagem de perícia para determinar a qualidade do seu resultado naquela etapa da disputa. É basicamente uma ação de superar.

Jogadores que fizerem rolagens em uma disputa devem comparar o seu resultado ao de todos os outros.

-  **Se você conseguir o resultado maior, você vence aquela rodada.** Se estiver rolando diretamente contra outros participantes, isso significa que você conseguiu o maior valor entre todos os participantes da disputa. Se todos estiverem rolando contra algo no ambiente, significa que você conseguiu mais tensões que todos.
Vencer o desafio significa que você acumula uma **vitória** (que você pode representar com marcas em um pedaço de papel) e descreve como assumiu a liderança.
-  **Se for bem-sucedido com estilo e ninguém mais for,** então você marca **duas** vitórias.
-  **Se houver um empate, ninguém recebe uma vitória e uma reviravolta inesperada ocorre.** Isso pode significar muitas coisas dependendo da situação – o terreno ou o ambiente sofre alguma mudança, os parâmetros do desafio são alterados ou surge um imprevisto que afeta os participantes. O Narrador deve criar um aspecto de situação para reﬂetir essa mudança.
-  **O primeiro participante a acumular três vitórias vence a disputa.**

> Fräk possui Atletismo Ótimo (+4). Valin possui Ofícios de nível Bom (+3). Na primeira rodada, Maira rola mal para Fräkeline e termina com um resultado Regular (+1). Amanda consegue 0 nos dados e mantêm o resultado Bom (+3). Amanda vence, assim Valin vence a primeira rodada e acumula uma vitória. Amanda descreve Valin conseguindo ativar a primeira bateria de seu teleporte, fazendo seu cinto brilhar.
>
> Na segunda rodada, Maira vira o jogo conseguindo uma rolagem Ótima (+4), enquanto Amanda consegue apenas um resultado Razoável (+2) para Valin. Isso é um sucesso, que Maira marca no papel como uma vitória. Maira descreve Fräkeline correndo a toda velocidade, se aproximando de Valin.
>
> Ambos empatam na terceira rodada com um resultado Bom (+3)! Amanda precisa agora introduzir uma reviravolta inesperada. Ela pensa sobre isso por um momento e diz “Certo, parece que os vários equipamentos de Valin estão em conﬂito, deixando ***Distorções Quânticas*** espalhadas pelo ar”. Ela escreve o novo aspecto de situação em um cartão e o coloca na mesa.
>
>Na quarta rodada há mais um empate, desta vez com um resultado Ótimo (+4). Maira diz “Agora ele vai ver. Eu quero invocar dois aspectos – um porque sou a ***Famosa Ladra Robótica***, mas as ***Distorções Quânticas*** também, pois creio que elas interferirão no teleporte”. Ela passa dois pontos de destino para Amanda.
>
> Isso dá a ela o resultado final Lendário (+8), um sucesso com estilo que lhe fornece duas vitórias. Ela alcança o alvo de três vitórias enquanto Valin possui apenas uma, assim Fräk vence a disputa!
>
> Amanda e Maira descrevem como Fräkeline consegue agarrar Bandu antes que Valin escape num feixe luminoso.

### Criando Vantagens em Uma Disputa

Durante qualquer rodada é possível tentar criar uma vantagem antes de rolar os dados. Se o alvo for outro participante, este pode se defender normalmente. Se alguém puder interferir na sua ação, criarão uma oposição ativa como sempre.

Isso traz um risco adicional - **falhar numa ação de criar vantagem significa perder sua rolagem da disputa**, ou seja, você não poderá progredir na rodada atual. Se ao menos empatar, você poderá a disputa normalmente. Se estiver tentando conceder um bônus usando as regras de [Trabalho em Equipe](../trabalho-em-equipe/), falhar na ação de criar vantagem significa que o personagem que lidera a equipe não recebe os benefícios de sua ajuda nesta rodada.

> Fräkeline tenta jogar lama nos olhos de Valin ao mesmo tempo em que corre para salvar Bandu. Maira diz que quer criar vantagem, fazendo Valin o alvo de um novo aspecto chamado ***Lama nos Olhos*** (sim, bem criativo).
>
> Ela rola Atletismo para isso e consegue um resultado Ótimo (+4). Valin rola seu Atletismo para e defender e consegue um resultado Bom (+3).
>
> Valin recebe uma boa dose de lama nos olhos, como Fräk esperava, e Maira marca uma invocação grátis desse aspecto.
>
> Como Maira não falhou ela poderá realizar sua rolagem da disputa normalmente. Amanda decide que ficar parcialmente cego não impedirá Valin de continuar sua preparação, então ele também rolará normalmente.

### Ataques em uma Disputa

Se alguém estiver tentando atacar em uma disputa, então ele está tentando inﬂigir dano diretamente a situação deixa de ser uma disputa. Você deve parar imediatamente o que estiver fazendo e se preparar para um conflito.

- [« Desafios](../desafios/)
- [Conflitos »](../conflitos/)

