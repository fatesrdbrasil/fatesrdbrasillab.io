---
title: Regras Vs Deliberações
layout: default
---

## Regras Vs Deliberações


Claro, você pode alterar as regras, mas deveria? Às vezes a resposta é sim. Se estiver jogando uma partida de super-heróis, precisará adicionar alguns poderes. Se seu jogo for sobre corridas de rua, é bom ter algumas regras que
tratem de veículos. 

Porém, às vezes não é necessária uma nova regra. Nesses casos você só precisa de uma deliberação.

Uma deliberação é uma decisão tomada pelo seu grupo de jogo – normalmente sob orientação do Narrador – sobre como algo funciona no jogo. Deliberações cobrem casos especiais que não são especificados nas regras, casos que exigem um pouco de interpretação. Uma nova regra, por outro lado, é uma mudança em um ou mais dos subsistemas existentes no jogo ou a adição de algo novo. Enquanto uma deliberação é uma interpretação de como o jogo funciona, uma nova regra é uma mudança no funcionamento do jogo.

Por exemplo, quando ao dizer a um jogador que o seu personagem não pode dar um tiro longo com seu rifle porque o barco onde ele está balança muito, você está deliberando. Ao definir que ninguém pode dar um tiro longo de um barco, você está criando uma nova regra. Notou a diferença? Um afeta a situação atual e pode influenciar outras situações, enquanto a outra afeta todas as situações do tipo.

Então quando usar uma ou outra? Adicione uma nova regra para algo que acontece com frequência. Sempre que se deparar com algo problemático ou desejar fazer algo inovador, e se isso acontecer com frequência, é uma boa ideia criar uma nova regra. Use deliberações se não tiver certeza que a situação ocorrerá novamente ou se acha que é um caso raro.

Se criar uma regra nova para cada situação, acabará com tantas regras para administrar que acabará se perdendo. Por outro lado, se criar regras para casos isolados, terá a liberdade de alterá-las depois. Claro, é possível alterar uma regra depois, mas alguns jogadores podem não gostar disso – e com razão! Pior, você pode criar regras ainda mais complicadas de lembrar e aplicar, especialmente se alterá-las o tempo todo.

Eis o segredo – deliberações podem se tornar novas regras. Se você fizer uma deliberação a respeito de tiros com rifles em um barco e notar que essa situação está se repetindo, transforme-a em uma regra. Se for necessário deliberar diversas vezes sobre a mesma coisa, seus jogadores provavelmente se lembrarão disso, diminuindo as chances de uma regra ser esquecida.
