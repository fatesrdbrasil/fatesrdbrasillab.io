# 7 - Desafios, Disputas e Conflitos

## Foco na Ação

Na maioria das vezes uma rolagem simples de perícia será o suficiente para decidir como determinada situação será resolvida. Não há nível de detalhe ou tempo pré-determinados para as ações representadas por uma perícia. Logo, você pode usar uma única rolagem de Atletismo para saber se consegue escalar um rochedo que pode levar dias para ser desbravado ou usar a mesma rolagem simples para saber se consegue desviar de uma árvore caindo rapidamente em seu caminho, prestes a esmagá-lo.

Às vezes, porém, você estará em uma situação onde faz algo realmente dramático e interessante, como nas cenas marcantes de um livro ou filme. Quando isso acontece, é uma boa ideia focar na ação e lidar com ela usando várias rolagens de perícia, pois a variedade de resultados tornará as coisas realmente dinâmicas e surpreendentes. A maioria das cenas de luta se enquadra nisso, mas você pode focar em qualquer coisa que considerar suficientemente importante – perseguição de carros, processos judiciais, jogos de pôquer com apostas altas e por aí vai.

Há três formas de focar na ação em *Fate*:

-  Desafios, quando um ou mais personagens tentam realizar algo dinâmico ou complicado.
-  Disputas, quando dois ou mais personagens estão competindo pelo mesmo objetivo.
-  Conﬂitos, quando dois ou mais personagens estão tentando ferir uns aos outros.

## Desafios

Uma simples ação de superar é suficiente para lidar com um empecilho ou obstáculo – o herói precisa destrancar a porta, desarmar a bomba, descobrir uma informação vital, etc. Também é bastante útil quando os detalhes de como algo aconteceu não são tão importantes ou não vale a pena gastar tempo com isso, quando o que você precisa saber é que o personagem consegue realizar algo sem reforços ou gastos adicionais.

Às vezes, no entanto, as coisas ficam um pouco complicadas. Não é suficiente apenas abrir a porta, porque você também precisa conter a horda de zumbis que está atacando e realizar a magia que providenciará cobertura. Desarmar a bomba não é suficiente, porque você também precisa evitar que o dirigível caia enquanto evita que o cientista desacordado que você está resgatando se machuque.

**Um desafio é uma série de ações de superar que você usa para resolver uma situação especialmente complicada ou dramática**. Cada uma dessas ações usa uma perícia **diferente** para lidar com as tarefas ou as partes da situação e o conjunto dos resultados determinará o que acontece.

Narradores que estiverem em dúvida se o momento é adequado para um desafio devem se fazer as seguintes perguntas:

-  Cada tarefa individual pode gerar tensão e drama, independente das outras tarefas? Se todas as tarefas forem parte de um único objetivo, como "desconectar o detonador", "parar o relógio" e "se livrar do explosivo" quando desarmar uma bomba, então essa tarefa deverá ser uma única ação de superar, usando esses detalhes para explicar o que aconteceu se a rolagem der errado.
-  A situação exige perícias diferentes para ser resolvida? Conter os zumbis (Lutar) enquanto empurra uma barricada (Vigor) e conserta a carroça (Ofícios) para poder fugir seria uma situação adequada para um desafio.

Para organizar um desafio, simplesmente identifique as tarefas individuais u objetivos que a situação exige e resolva cada uma delas com uma rolagem separada de uma ação de superar (às vezes, apenas uma certa sequência de rolagens fará sentido para você; isso também é válido). Dependendo da situação um personagem pode precisar realizar diversas rolagens ou vários personagens podem participar.

> Bandu está tentando ativar as defesas da estação especial onde estão a fim de garantir a proteção do grupo de sobreviventes que vieram resgatar. Normalmente isso não seria muito interessante, exceto que ele está tentando realizar essa tarefa antes de serem devorados por uma horda de alienígenas selvagens que nasceram dos ovos no compartimento de carga.
>
> Amanda vê vários componentes nessa cena. Primeiro há o sistema de computadores, depois bloquear a entrada e, por último, manter os sobreviventes calmos. Isso indica rolagens de Conhecimentos, Ofícios e algum tipo de perícia social – Michel escolhe Comunicação.
>
> Assim, Michel rolará essas três perícias separadamente, uma rolagem para cada componente que Amanda identificou. Ela define uma oposição passiva Boa (+3) para cada uma delas – ela quer que ele tenha uma chance justa, mas deixando espaço para várias possibilidades de resultado.
>
> Agora estão prontos para começar.

Para conduzir o desafio, faça as jogadas de superar na ordem que parecer mais interessante, mas não decida nada sobre como a situação será resolvida antes de saber o resultado dos dados – é importante ter a liberdade de escolher a sequência dos eventos de cada rolagem na ordem que fizer mais sentido e for mais interessante. Jogadores que conseguirem um impulso em alguma de suas rolagens devem se sentir livres para usá-lo em outra rolagem do desafio, desde que faça sentido.

Narradores devem, depois de terminadas as rolagens, interpretar o conjunto de sucessos, falhas e custos de cada ação para determinar como a cena acontece. Esses resultados podem levar a outros desafios, disputas ou até mesmo conﬂitos.

> Michel respira fundo. "Vamos lá", ele diz, e pega os dados.
>
> Ele decide que primeiro irá garantir a segurança da sala, faz a rolagem de Ofícios, que possui num nível Bom (+3), e consegue 0 nos dados. Isso é um empate, o que lhe permite alcançar o objetivo a um custo menor. Amanda diz “Que tal se eu criar um impulso chamado ***Trabalho Apressado*** para usar contra você se for necessário – afnal você está com pressa”.
>
> Michel suspira e concorda, em seguida vai para a segunda rolagem do desafio: acalmar as pessoas no local com sua Comunicação Boa (+3). Ele faz a rolagem e consegue um resultado horrível de -3 nos dados! Agora ele tem a opção de falhar ou de ser bem-sucedido a um grande custo. Ele opta pelo sucesso, deixando Amanda pensar em algo para seu custo.
>
> Ela pensa um pouco. Como acalmar as pessoas ali a um grande custo? Então ela sorri. “Isso é mais um detalhe de história do que mecânico, mas sabe como é... você está usando Comunicação, então provavelmente você está bastante inspirado agora. Vejo que você poderia acidentalmente convencer alguns desses astronautas que os aliens não são uma grande ameaça, e que é totalmente viável lutar contra eles. Afinal de contas, você está cuidando das defesas da estação, não é mesmo?”
>
> Michel diz, “Mas eles precisam ficar dentro da sala pra se proteger!” Amanda sorri maliciosamente. Michel suspira de novo. “Certo. Algumas pessoas entendem errado a ideia e existe a chance de serem mortas. Já posso ouvi-los dizendo... Bandu, porque você deixou meu marido morrer? Fazer o quê. ” Amanda sorri ainda mais.
>
> Michel segue para a parte final do desafio – a ativação das defesas da sala com sua perícia Conhecimentos nível Ótimo (+4). Amanda invoca o impulso que conseguiu mais cedo e diz “´Sim, você está bastante desconcentrado vendo os alienígenas derrubarem sua barricada. Muito distraído”. Isso eleva a dificuldade para Excepcional (+5). Ele rola e consegue +2 o que o deixa com um resultado Fantástico (+6), o suficiente para ser bem-sucedido sem nenhum custo.
>
> Amanda concorda e juntos eles finalizam a descrição da cena – Bandu finaliza os comandos a tempo e as armas da nave começam a trabalhar. Alguns alienígenas a ponto de entrarem começam a rosnar e se afastar, feridos pelos lasers, e Bandu suspira de alívio... até ouvir os gritos de pânico dos astronautas do lado de fora da sala...
>
> Mas isso é a próxima cena.

Se você possuir algum impulso que não foi usado no desafio, sinta-se livre para mantê-lo pelo resto da cena ou até quando fizer sentido, caso os eventos do desafio se interliguem à próxima cena.

### Vantagens em Desafios

Você pode tentar criar vantagem em um desafio para ajudá-lo ou ajudar alguém. Criar vantagem não ajuda a completar um dos objetivos do desafio, mas falhar nessa rolagem pode criar um custo ou problema que trará um impacto negativo em um dos outros objetivos. Tenha cuidado ao usar essa tática; vantagens podem ajudar a completar tarefas de forma mais eficaz, mas tentar criá-las sempre traz riscos.

### Atacando em Desafios

Como você está indo contra uma oposição passiva em um desafio, nunca será preciso usar atacar. Se estiver em uma situação onde pareça razoável usar uma ação de ataque, você estará iniciando um conflito.

## Disputas

**Quando dois ou mais personagem possuem objetivos mutuamente exclusivos, mas não estão tentando se ferir diretamente, eles estão em uma disputa**. Disputas de queda de braço, corridas ou outras competições esportivas e debates público são bons exemplos de disputas.

Narradores devem se fazer as perguntas a seguir ao organizar uma disputa:

-  Quais são os “lados”? É cada um por si na disputa ou há grupos de pessoas se opondo a outros grupos? Se houver vários personagens lados, eles rolarão juntos usando as regras de [Trabalho em Equipe]().
-  Qual é o ambiente da disputa? Há alguma característica significativa ou notável do ambiente que você queira definir como aspecto de situação?
-  Como os participantes da disputa estão se opondo uns aos outros? Estão rolando uns contra os outros diretamente (como em uma corrida ou partida de pôquer) ou estão tentando superar algo no ambiente (como um obstáculo ou um grupo de jurados)?
-  Quais perícias são apropriadas para a disputa? Todos terão que rolar a mesma perícia ou várias perícias são aplicáveis?

> Bandu foi derrubado em uma batalha contra o grupo obscuro de assassinos que emboscaram ele e Fräkeline nos arredores da cidade. Fräk dá conta do último deles, finalizando o conﬂito, e vai então em direção ao amigo caído.
>
> É aí que o líder dos assassinos, um ladrão que ela conhece bem chamado Valin Vorta, o Célere, aparece com um cinto de teleporte próximo a Bandu, que está caído inconsciente! Ele começa a recarregar seu sistema de teleporte, com a intenção de levar Bandu junto consigo. Fräk começa a correr. Será que ela consegue chegar antes que Valin consiga escapar?
>
> Amanda olha as perguntas acima para organizar a disputa.
>
> A cena do conﬂito anterior teve um aspecto de situação chamado ***Poças de Óleo***, e Amanda continuar assim.
>
> Claramente Valin e Fräkeline estão se opondo diretamente, então oferecerão oposição ativa.
>
> Valin rolará sua perícia Ofícios nessa disputa, pois está utilizando um equipamento. Como a situação requer um movimento simples, Amanda e Maira concordam que Atletismo é a perícia mais apropriada para Fräkeline.

Agora a ação pode começar.

Uma disputa ocorre em uma série de **rodadas de altercação**. A cada rodada, cada participante faz uma rolagem de perícia para determinar a qualidade do seu resultado naquela etapa da disputa. É basicamente uma ação de superar.

Jogadores que fizerem rolagens em uma disputa devem comparar o seu resultado ao de todos os outros.

-  **Se você conseguir o resultado maior, você vence aquela rodada.** Se estiver rolando diretamente contra outros participantes, isso significa que você conseguiu o maior valor entre todos os participantes da disputa. Se todos estiverem rolando contra algo no ambiente, significa que você conseguiu mais tensões que todos.
Vencer o desafio significa que você acumula uma **vitória** (que você pode representar com marcas em um pedaço de papel) e descreve como assumiu a liderança.
-  **Se for bem-sucedido com estilo e ninguém mais for,** então você marca **duas** vitórias.
-  **Se houver um empate, ninguém recebe uma vitória e uma reviravolta inesperada ocorre.** Isso pode significar muitas coisas dependendo da situação – o terreno ou o ambiente sofre alguma mudança, os parâmetros do desafio são alterados ou surge um imprevisto que afeta os participantes. O Narrador deve criar um aspecto de situação para reﬂetir essa mudança.
-  **O primeiro participante a acumular três vitórias vence a disputa.**

> Fräk possui Atletismo Ótimo (+4). Valin possui Ofícios de nível Bom (+3). Na primeira rodada, Maira rola mal para Fräkeline e termina com um resultado Regular (+1). Amanda consegue 0 nos dados e mantêm o resultado Bom (+3). Amanda vence, assim Valin vence a primeira rodada e acumula uma vitória. Amanda descreve Valin conseguindo ativar a primeira bateria de seu teleporte, fazendo seu cinto brilhar.
>
> Na segunda rodada, Maira vira o jogo conseguindo uma rolagem Ótima (+4), enquanto Amanda consegue apenas um resultado Razoável (+2) para Valin. Isso é um sucesso, que Maira marca no papel como uma vitória. Maira descreve Fräkeline correndo a toda velocidade, se aproximando de Valin.
>
> Ambos empatam na terceira rodada com um resultado Bom (+3)! Amanda precisa agora introduzir uma reviravolta inesperada. Ela pensa sobre isso por um momento e diz “Certo, parece que os vários equipamentos de Valin estão em conﬂito, deixando ***Distorções Quânticas*** espalhadas pelo ar”. Ela escreve o novo aspecto de situação em um cartão e o coloca na mesa.
>
>Na quarta rodada há mais um empate, desta vez com um resultado Ótimo (+4). Maira diz “Agora ele vai ver. Eu quero invocar dois aspectos – um porque sou a ***Famosa Ladra Robótica***, mas as ***Distorções Quânticas*** também, pois creio que elas interferirão no teleporte”. Ela passa dois pontos de destino para Amanda.
>
> Isso dá a ela o resultado final Lendário (+8), um sucesso com estilo que lhe fornece duas vitórias. Ela alcança o alvo de três vitórias enquanto Valin possui apenas uma, assim Fräk vence a disputa!
>
> Amanda e Maira descrevem como Fräkeline consegue agarrar Bandu antes que Valin escape num feixe luminoso.

### Criando Vantagens em Uma Disputa

Durante qualquer rodada é possível tentar criar uma vantagem antes de rolar os dados. Se o alvo for outro participante, este pode se defender normalmente. Se alguém puder interferir na sua ação, criarão uma oposição ativa como sempre.

Isso traz um risco adicional - **falhar numa ação de criar vantagem significa perder sua rolagem da disputa**, ou seja, você não poderá progredir na rodada atual. Se ao menos empatar, você poderá a disputa normalmente. Se estiver tentando conceder um bônus usando as regras de [Trabalho em Equipe](), falhar na ação de criar vantagem significa que o personagem que lidera a equipe não recebe os benefícios de sua ajuda nesta rodada.

> Fräkeline tenta jogar lama nos olhos de Valin ao mesmo tempo em que corre para salvar Bandu. Maira diz que quer criar vantagem, fazendo Valin o alvo de um novo aspecto chamado ***Lama nos Olhos*** (sim, bem criativo).
>
> Ela rola Atletismo para isso e consegue um resultado Ótimo (+4). Valin rola seu Atletismo para e defender e consegue um resultado Bom (+3).
>
> Valin recebe uma boa dose de lama nos olhos, como Fräk esperava, e Maira marca uma invocação grátis desse aspecto.
>
> Como Maira não falhou ela poderá realizar sua rolagem da disputa normalmente. Amanda decide que ficar parcialmente cego não impedirá Valin de continuar sua preparação, então ele também rolará normalmente.

### Ataques em uma Disputa

Se alguém estiver tentando atacar em uma disputa, então ele está tentando inﬂigir dano diretamente a situação deixa de ser uma disputa. Você deve parar imediatamente o que estiver fazendo e se preparar para um conflito.

## Conflitos

Os personagens em um conﬂito estão tentando ferir uns aos outros. Pode ser uma luta de socos e chutes, um tiroteio ou um duelo de espadas. Também poderia ser um interrogatório, um ataque psíquico ou discussão acalorada entre pessoas queridas. **Desde que os personagens envolvidos tenham a intenção e a capacidade de ferir uns aos outros, estarão em uma cena de conﬂito.**

Conﬂitos podem ser de natureza física ou mental, dependendo do tipo de risco envolvido. Em conﬂitos físicos, você sofre hematomas, arranhões, cortes e outros ferimentos. Em conﬂitos mentais é possível perder confiança, autoestima, compostura, entre outros traumas psicológicos.

Organizar um conﬂito pode ser mais complexo que organizar uma disputa. Aqui estão os passos:

-  Monte a cena, descrevendo o ambiente em que o conﬂito ocorre, criando os aspectos de situação e as **zonas** e estabelecendo quem está do lado de quem.
-  Determine a ordem dos turnos.
-  Comece a primeira rodada:
    - No seu turno, realize uma ação e resolva-a.
    - No turno de outros, defenda ou responda às ações conforme necessário.
    - Ao final do turno de todos os envolvidos, comece uma nova rodada.

Você sabe que o conﬂito terminou quando todos os envolvidos de um dos lados **concedem** ou **são tirados de ação**.

### Montando a cena

Antes de dar início ao conﬂito, o Narrador e os jogadores devem conversar sobre as circunstâncias da cena. Isso envolve responder rapidamente algumas perguntas, como:

-  Quem está no conﬂito?
-  Como estão posicionados do ponto de vista de cada um?
-  Onde o conﬂito está acontecendo? Isso é importante?
-  Como é o ambiente?

Não é preciso uma lista completa e detalhada com medidas exatas de distância nem nada do gênero. Apenas o suficiente para que a situação fique clara para todos.

Narradores devem utilizar essas informações para criar aspectos de situação que ajudarão a detalhar a área do conﬂito.

> Esopo, Bandu e Fräkeline estão invadindo o embarcadouro da estação espacial ZX19, seguindo a trilha da nave de Esopo. A nave, infelizmente, já foi movida para outro lugar, mas Rakir, um encarregado do Clã do Dedo, ficou para trás para detê-los, e trouxe consigo quatro capangas.
>
> Os participantes desse conﬂito são claros – os PJs contra Rakir e seus capangas, estes sendo PdNs sob o controle de Amanda. O grupo conversa um pouco sobre o que há no ambiente do porto: caixotes por todos os lados, grandes e abertos, deve haver também um segundo andar, e Amanda menciona uma porta de embarque aberta a espera de uma nave, protegida do vácuo espacial por um campo de força.

#### Aspectos de Situação

Ao montar o ambiente da cena, o Narrador deve ficar atento à possíveis detalhes divertidos que possam se tornar aspectos de situação, especialmente se achar que alguém poderá se valer deles de modo interessante durante o conﬂito. Porém, não exagere - encontre de três a cinco coisas atraentes sobre o local do conﬂito e transforme-as em aspectos.

Boas opções de aspectos de situação incluem:

-  Qualquer coisa sobre a atmosfera geral, clima ou iluminação – escuro ou mal iluminado, sob uma tempestade, assustador, caindo aos pedaços, ofuscantemente claro, etc.
-  Qualquer coisa que possa afetar ou restringir os movimentos – imundo, lama por toda parte, escorregadio, áspero, etc.
-  Coisas que podem ser usadas como cobertura ou esconderijo – veículos, obstruções, mobília grande, etc.
-  Coisas que possa derrubar, quebrar ou usar como armas improvisadas – prateleiras, estátuas, etc.
-  Coisas inﬂamáveis.

> Considerando a cena do porto, Amanda tenta pensar em bons aspectos de situação. Ela decide que há caixas suficientes para que a movimentação seja difícil, então adiciona ***Caixas Pesadas*** e ***Local Apertado***. A porta de embarque está aberta, o que significa que há um buraco enorme com um campo de energia, então ela também adiciona ***Porta Para o Espaço*** como um aspecto de situação, imaginando que alguém pode tentar empurrar outra pessoa dali.

À medida que a cena se desenrola, os jogadores podem sugerir detalhes sobre o ambiente que sejam perfeitos como aspectos. Se o Narrador descrever a cena como sendo pouco iluminada, um jogador deve ser capaz de invocar as ***Sombras*** para ajudar em suas rolagens de Furtividade, mesmo que isso ainda não tenha sido definido como aspecto. Se o recurso exigir alguma intervenção por parte dos personagens em cena para que se torne um aspecto, então isso é realizado com uma ação de criar vantagem. Normalmente o celeiro não entra ***Em Chamas! ***sem alguém para chutar a lamparina. Normalmente.

#### Zonas

Se o conﬂito se passa em uma área grande, o Narrador pode dividi-la em zonas para facilitar o processo.

Uma zona é uma representação abstrata de um espaço físico. A melhor definição de uma zona é uma área próxima o suficiente para que você possa interagir diretamente com alguém (em outras palavras, dar uns poucos passos e socá-lo na cara).

De modo geral, um conﬂito dificilmente envolverá mais do que algumas poucas zonas. Duas a quatro costumam bastar, salvo para conﬂitos realmente grandes. *Fate* não é um jogo de miniaturas - zonas devem criar uma sensação coerente do ambiente, mas qualquer coisa que não possa ser ilustrada num guardanapo já é complicada demais.

-  Se você descrever a área como sendo maior que uma casa, você provavelmente poderá dividi-la em duas ou mais zonas – pense em uma catedral ou o estacionamento de um shopping.
-  Se houver escadas, uma rampa, cerca ou parede, podem ser zonas separadas, como o segundo andar de uma casa.
-  “Abaixo de X” e “Acima de X” podem ser diferentes zonas, especialmente se para mover-se entre elas exige algum esforço – pense no espaço aéreo ao redor de algo grande, como um dirigível.

Ao estabelecer as zonas, verifique se há aspectos de situação que possam complicar o movimento entre elas. Eles podem ser importantes depois, quando alguém tentar se mover de uma zona para outra. Se isso significa que é necessário adicionar mais aspectos de situação, crie-os agora.

> Amanda decide que o ambiente precisa ser dividido em mais de uma zona. Ela pensa que o pátio principal é grande o suficiente para ser dividido em duas, e as ***Caixas Pesadas*** e ***Local Apertado*** que havia mencionado anteriormente tornam o movimento entre as zonas mais complicado.
>
> Ela sabe que há um segundo andar também, então faz disso uma zona adicional. Ela acrescenta uma ***Escada de Acesso*** à cena.
>
> Se por algum motivo alguém decidir sair do local, ela pensa que poderá haver uma quarta zona, mas não precisa de aspectos para ela agora.
>
> Amanda faz um pequeno esboço em uma folha para que todos vejam.

------

### Aspectos de Situação e Zonas em Conflitos Mentais

Em um conﬂito mental, nem sempre fará sentido usar aspectos de situação e zonas para descrever o espaço físico. Faz sentido que em um interrogatório, por exemplo, onde as características físicas do espaço geram medo, mas não em uma discussão violenta com alguém próximo. Fora isso, quando as pessoas estão tentando ferir umas às outras emocionalmente, normalmente usam as fraquezas de seus alvos contra eles mesmos – em outras palavras, seus próprios aspectos.

Logo, você pode não precisar de aspectos de situação ou zonas para muitos dos conﬂitos mentais. Não se sinta obrigado a incluí-los.

------

### Estabelecendo os Lados

É importante saber os objetivos de cada um antes do conﬂito iniciar. As pessoas lutam por um motivo e, se estão dispostas a se ferir, em geral é por um motivo importante.

O mais comum é que os personagens dos jogadores estejam de um lado, combatendo PdNs opositores, mas nem sempre será assim – PJs podem lutar uns contra os outros e se aliarem a PdNs no processo.

Tenha certeza que todos concordam com a divisão dos lados, quem está com quem e onde cada um está situado na cena (como quem ocupa qual zona) antes do conﬂito iniciar.

Aos Narradores também pode ser útil decidir como esses grupos serão divididos para se enfrentar – será que um personagem que será intimidado pelos capangas do vilão ou a oposição se distribuirá igualmente entre os PJs? Você pode mudar de ideia depois do começo do conﬂito, mas uma ideia inicial básica já é o bastante para começar.

> Em nosso exemplo do embarcadouro, os lados são óbvios – Rakir e seus capangas contra os PJs.
>
> Michel pergunta a respeito de sinais da nave que procuram e Amanda responde: “se você acha que pode se esconder durante a luta para investigar, vá em frente. Veremos o que acontece.”
>
> O conﬂito começa com todos no piso térreo das docas. Amanda decide que Rakir e um comparsa atacarão Esopo, outros dois irão atrás de Fräkeline e o último irá perseguir Bandu.

### Ordem dos Turnos

A sua vez em um conﬂito é determinada de acordo com suas perícias. Em um conﬂito físico, compare sua perícia Percepção com a dos outros participantes. Em um conﬂito mental, compare Empatia. Quem possuir o maior valor agirá primeiro e assim segue em ordem decrescente.

Se houver algum empate, compare alguma perícia secundária ou terciária. Para conﬂitos físicos comece com Atletismo e passe para Vigor. Em conﬂitos mentais, compare Comunicação, depois Vontade.

Para simplificar as coisas para o Narrador, ele pode usar seu PdN mais vantajoso para determinar a sua posição na ordem dos turnos e fazer com que todos os outros PdNs ajam na mesma hora.

> Fräkeline possui Percepção Boa (+3), a mais alta entre todos os envolvidos, então agirá primeiro.
>
Bandu é Regular (+1) em Percepção, agindo em segundo.
>
> Esopo e Rakir possuem o mesmo valor em Percepção, mas Esopo possui Atletismo Bom (+3) e Rakir é apenas Razoável (+2), então Esopo é o terceiro e Rakir, o último.

### Rodadas

O funcionamento das rodadas num conﬂito é um pouco mais complicado que numa disputa. Numa rodada, cada personagem tem seu turno de ação. O Narrador age uma vez para cada PdN controlado no conﬂito.

Na maioria das vezes você estará atacando outro personagem ou criando uma vantagem em seu turno, afinal esse é o objetivo de um conﬂito – derrotar o seu oponente ou tornar as coisas mais fáceis para conseguir derrotar o seu oponente.

No entanto, se houver um objetivo secundário durante a cena do conﬂito, uma rolagem de superar pode ser necessária. Isso será mais comum quando você quiser se mover entre zonas quando há um aspecto de situação atrapalhando esse movimento.

De qualquer forma, só será possível fazer uma rolagem de perícia em seu turno por rodada, a menos que esteja se defendendo da ação de outros - você pode fazer isso quantas vezes quiser. Você pode inclusive usar ações de defesa para os outros, contanto que duas condições sejam preenchidas: deve ser viável inserir-se entre o ataque e seu alvo e você terá que sofrer os efeitos das rolagens que falharem.

#### Defesa Total

Se desejar, poderá abrir mão de sua ação dentro da rodada para se concentrar apenas em se defender. Você não poderá realizar nada proativamente, mas poderá rolar todas as ações de defesa da rodada atual com um bônus de +2.

> Na primeira rodada da luta, Fräkeline age primeiro. Maira fará Fräk atacar o capanga que a está enfrentando. Essa é a sua ação dentro dessa rodada – ela ainda pode rolar para se defender sempre que for preciso, mas não pode realizar nada proativo até seu próximo turno.
>
> No turno de Michel, ele decide usar uma defesa completa com Bandu – normalmente ele poderia realizar uma ação de defesa e uma ação normal na rodada, mas em lugar disso, ele optou por receber +2 nas suas rolagens de defesa até o próximo turno.
>
> No turno de Léo, ele usará a ação de criar vantagem para adicionar o aspecto ***Encurralado*** a Rakir, na esperança de cercá-lo entre os caixotes. Essa é a sua ação na rodada.
>
> Amanda age por último, e decide que todos seus PdNs apenas atacarão seus alvos escolhidos.

### Resolvendo Ataques

Um ataque bem-sucedido causa um dano em seu alvo equivalente ao valor de tensões obtidas. Logo, se você consegue três tensões em seu ataque, o dano causado é igual a 3.

Se for atingido por um ataque, uma de duas coisas acontece: você absorve o dano e permanece no conﬂito ou é **tirado de ação**.

Felizmente, há duas formas de absorver dano e assim permanecer na luta – você pode receber **estresse** e/ou **consequências**. Também é possível **conceder** o conﬂito antes de ser tirado de ação, para ter controle sobre o que acontece com o seu personagem.

------

Narrador, se houver muitos PdNs sem importância em uma cena, sinta-se livre para que ofereçam oposição passiva para agilizar as coisas. Também considere usar bandos (p. XX) ao invés de PdNs individuais para simplificar.

------

#### Estresse

Uma de suas opções é minimizar os efeitos do dano através do estresse.

A melhor maneira de entender o estresse é que ele representa as várias razões pelas quais você evitou receber a força total de um ataque. Talvez você se contorça para esquivar, ou um ferimento pareça pior do que realmente é, ou você se esgota esquivando do golpe no último segundo.

Estresse mental pode significar que você quase pôde ignorar um insulto, reprimiu uma reação emocional instintiva ou algo semelhante.

------

Se, por qualquer razão, você decidir renunciar a sua defesa e receber dano (como, digamos, para se atirar na frente de uma flecha que vai em direção a um
amigo), você pode.

Como não está defendendo, o ataque é rolado contra uma oposição Medíocre (+0), o que significa que você provavelmente irá tomar uma pancada e tanto.

------

Caixas de estresse também representam uma perda de ímpeto – você tem apenas algumas chances de se salvar até ter que encarar o problema.

Sua ficha de personagem contém uma certa quantidade de caixas de estresse, cada uma com valores diferentes. Por padrão, todos os personagens possuem duas caixas de estresse. Você pode conseguir algumas adicionais; caixas com valores maiores dependem de suas perícias (normalmente Vigor e Vontade).

Ao receber estresse, marque a caixa com o valor igual à tensão que você recebeu. Se esta caixa já estiver marcada, marque a caixa de maior valor depois dela. Se não houver mais caixa de maior valor disponível e você não puder receber mais nenhuma consequência, então você estará fora do conﬂito.

Você só pode marcar uma caixa por golpe recebido.

Lembre-se que você possui *dois conjuntos de caixas de estresse!* Uma delas é para estresse físico, a outra para mental; você inicia com duas caixas em cada um desses conjuntos. Se você receber estresse de origem física, marque a caixa de estresse físico. Se for dano mental, marque a caixa de estresse mental.

Após o conﬂito, quando conseguir um tempo para respirar, todas as caixas de estresse ficam disponíveis novamente para uso.

> Rakir atinge Esopo com riﬂe sônico, causando um dano pesado de 3 tensões na rodada atual.
>
> Olhando sua ficha de personagem, Léo percebe que possui apenas duas caixas de estresse disponíveis, uma de 2 pontos e uma de 4 pontos.
>
> \[X]\[2]\[X]\[4]
>
> Como sua caixa de 3 pontos já está marcada, o dano será absorvido pela caixa de maior valor. Ele marca a quarta caixa, relutante.
>
> \[X]\[2]\[X]\[X]
>
> Amanda e Léo descrevem o resultado – Esopo é lançado pela onda e atinge as caixas. Um pouco mais que isso e ele teria caído da plataforma.
>
> Esopo ainda possui uma caixa de estresse, de valor 2 na sua ficha. Isso significa que suas forças estão quase no fim, e o próximo golpe que receber vai doer bastante...

#### Consequências

A segunda forma de absorver dano é receber uma consequência. Uma consequência é algo mais sério que o estresse – representa um ferimento mais sério ou contratempo que você acumula em conﬂitos, algo que poderá dificultar as coisas para o seu personagem após o fim do conﬂito.

Consequências possuem três níveis de severidade – suave, moderada e severa. Cada uma possui uma quantidade de tensões ligadas a ela: duas, quatro e seis, respectivamente. Sua ficha de personagem apresenta uma certa quantia de caixas de consequência disponíveis:

-  Suave (2)
-  Moderada (4)
-  Severa (6)

Ao preencher uma caixa de consequência, você reduz o valor da tensão o ataque no valor da consequência. Você pode usar mais de uma caixa de consequência para isso, se estiverem disponíveis. Qualquer uma das tensões restantes devem ser absorvidas através de estresse para evitar tirado de ação.

Há uma penalidade, porém. A consequência escrita na caixa é um aspecto que representa o efeito duradouro decorrente do ataque. O oponente que o forçou a receber uma consequência tem direito a uma invocação grátis e o aspecto permanece na ficha de seu personagem até que você se recupere.

Enquanto estiver em sua ficha, a consequência é tratada como qualquer outro aspecto, exceto pelo fato dele possuir um teor tão negativo que, provavelmente, será mais usado contra o seu personagem.

Diferente de estresse, pode levar bastante tempo para que um personagem se recupere de uma consequência após o conﬂito. Além disso, você possui apenas *um* conjunto de consequências; não há caixas específicas para consequências físicas e mentais. Isso significa que, se você receber uma consequência mental suave e sua caixa já estiver preenchida com uma consequência física suave, você está encrencado! Você terá que receber uma consequência moderada ou severa para absorver o dano (assumindo que ainda estejam livres). A exceção a isso é a caixa de consequência extra que você recebe ao possuir um nível Excepcional (+5) em Vigor ou Vontade, essa caixa só pode receber consequências físicas ou mentais, respectivamente.

Mesmo assim, ainda é melhor do que ficar fora do conﬂito, não?

> Fräk foi encurralada por três capangas nesta rodada que, com a ajuda de uma rolagem sortuda dos dados e alguns aspectos de situação, conseguem causar um dano de 6 tensões em um ataque. Ela conseguiu se esquivar bem até agora, então ainda possui todas as caixas de estresse e consequência disponíveis.
>
> Há duas formas de receber o dano. Ela pode receber uma consequência severa, que nega 6 pontos de estresse. Ela também pode receber uma consequência moderada (4 tensões) e usar sua caixa de 2 pontos de estresse.
>
> Ela acha que não receberá outro dano tão grande assim novamente e decide receber uma consequência severa, mantendo assim as suas caixas de estresses disponíveis para danos menores.
>
> Amanda e Maira concordam em chamar essa consequência severa de ***Quase Dilacerada***. Fräkeline recebeu um corte violento de um dos capangas, rangendo os dentes de dor...

##### Nomeando Uma Consequência

Aqui seguem algumas orientações para o momento de nomear uma consequência:

Consequências suaves não exigem atenção médica imediata. Elas doem bastante e oferecem algumas inconveniências, mas não são nada que exija repouso total. Na forma mental, consequência suaves expressam coisas como pequenas gafes sociais ou mudanças perceptíveis de humor. Exemplos: ***Olho Roxo***, ***Mão Machucada***, ***Sem Fôlego***, ***Ansioso***, ***Ranzinza***, ***Temporariamente Cego***.

Consequências moderadas representam feridas mais sérias que necessitam de esforço e dedicação para serem recuperadas (incluindo cuidados médicos). No espectro mental, expressam danos em sua reputação ou problemas emocionais que você não pode simplesmente ignorar e dormir tranquilamente. Exemplos: ***Corte Profundo***, ***Queimadura de Primeiro Grau***, ***Exausto***, ***Bêbado***, ***Apavorado***.

Consequências severas vão direto para o pronto-socorro (ou algo equivalente em seu jogo), são extremamente perigosas, impedem o personagem de fazer muitas coisas e o deixarão fora de ação por um bom tempo. De forma mental, elas expressam traumas sérios ou ofensas capazes de mudar relacionamentos. Exemplos: ***Queimaduras de Segundo Grau***, ***Fratura Exposta***, ***Vísceras Para Fora***, ***Timidez Paralisante***, ***Fobia Traumática***.

##### Recuperando uma Consequência

Para poder voltar a usar uma caixa de consequência ocupada é preciso se recuperar primeiro. Isso requer duas coisas – ser bem-sucedido em uma ação que permita a você se recuperar de tal consequência e então esperar o tempo apropriado em jogo para que a recuperação ocorra.

A ação em questão é uma ação de superar; o obstáculo é a consequência que você recebeu. Se for uma lesão física, então a ação é algum tipo de tratamento médico ou primeiros socorros. Para consequências mentais, a ação envolve terapia, aconselhamento ou simplesmente uma noite com os amigos.

A dificuldade para esse obstáculo é baseada na tensão da consequência. Suave tem dificuldade Razoável (+2), moderada é Ótima (+4) e severa é Fantástica (+6). Se estiver tentando realizar a ação de recuperação em você mesmo, aumente a dificuldade em dois.

Tenha em mente que as circunstâncias têm que ser apropriadas, livres de distrações e pressões para que a rolagem possa acontecer – você não limpa e faz curativo em um corte profundo com ogros tentando adentrar as cavernas à sua procura. O Narrador tem a palavra final.

Se for bem-sucedido em uma ação de recuperação ou alguém for bem-sucedido para você, poderá renomear o aspecto da consequência demonstrando que ele está sendo recuperado. Então, por exemplo, ***Perna Quebrada ***poderia se tornar ***Mancando***, ***Moral Ofendida*** poderia se tornar ***Ofensas Controladas*** e assim por diante. Isso não livra a caixa de consequência, mas serve de indicador de que você está se recuperando e muda a forma como o aspecto pode ser usado.

Independentemente se você alterar ou não um aspecto de consequência – pois às vezes não fará sentido alterá-lo – simplesmente marque-o com uma estrela para que todos possam lembrar que a recuperação foi iniciada.

Depois é só esperar o tempo necessário.

- Para consequências suaves, só é preciso esperar uma **cena** inteira após a ação de recuperação para remover o aspecto e liberar a caixa.
- Para consequências moderadas, espere uma **sessão** inteira após a ação de recuperação (ou seja, se esta ação acontecer no meio da sessão, você deve se recuperar por volta da metade da próxima).
- Para consequências severas, espere um **cenário** inteiro após a ação de recuperação.

------

### Qual Perícia Eu Uso para Me Recuperar?

Em *Caroneiros de Asteroide*, recuperações físicas só ocorrem com o uso da perícia Conhecimentos que Bandu, O Oculto possui. Isso torna os conﬂitos físicos perigosos e sugere que o treinamento médico atual é bastante raro. Para recuperação mental, usamos a perícia Empatia.

Se você quer que seja fácil se recuperar, é possível adicionar essa ação a uma determinada perícia. Conhecimentos é uma boa opção, mas também pode ser uma função de Ofícios. Se for importante o suficiente em seu jogo, adicione perícias como Medicina ou Sobrevivência.

Da mesma forma, se você quer dificultar a recuperação mental, crie uma façanha para Empatia ou mesmo Comunicação, em vez de incluir isso em uma perícia.

------

> Frãkeline concorda com a consequência severa ***Quase Dilacerada*** como resultado da última luta.
>
> Em um lugar seguro, Bandu tenta cuidar do ferimento como pode. Ele possui uma façanha chamada “**Médico Autodidata**” o que o permite usar sua perícia Conhecimentos para recuperar. Ele faz uma rolagem de Conhecimentos contra uma dificuldade Fantástica (+6) e é bem-sucedido.
>
> Isso permite que Fräkeline renomeie o aspecto ***Quase Dilacerada*** para ***Em Recuperação*** e dê início ao processo de cura. Após o fim do próximo cenário ela estará apta a apagar o aspecto de sua ficha e usar a consequência severa novamente em outro conﬂito.

------

### Poções e Outras Curas Instantâneas

Muitos gêneros de jogo possuem algum tipo de mecanismo que permite a recuperação rápida de ferimentos. Cenários de fantasia trazem poções e magias de cura universais. Na ficção científica, há regeneradores hipodérmicos avançados ou biogel. Normalmente esses mecanismos existem porque muitos jogos expressam lesões através de penalidades numéricas que afetam drasticamente a eficácia do personagem.

Em *Fate*, no entanto, uma consequência é basicamente como qualquer outro aspecto; ela entra em jogo quando alguém paga um ponto de destino para invocá-la (após a invocação gratuita inicial é claro), ou quando é forçada.

Na melhor das hipóteses, curas poderosas devem apenas eliminar a necessidade de rolar os dados na ação de recuperação, ou reduzir a gravidade de uma consequência em um nível ou mais. Logo, uma poção de cura pode tornar uma consequência severa em moderada, diminuindo assim o tempo de recuperação. Antes de apagá-la, o PJ deve passar ao menos uma cena com a consequência, para que possa ser invocada ou afete outras coisas.

------

##### Consequências Extremas

Em adição ao conjunto padrão de consequências suaves, moderadas e severas, todos os PJs também possuem uma última opção para continuarem na luta – a **consequência extrema**. Só é possível usar isso uma vez entre um marco maior e o próximo.

Uma consequência extrema absorverá 8 tensões de dano, mas a um custo bastante sério – **você deve substituir um de seus aspectos padrões por uma consequência extrema (exceto o conceito, que não pode ser alterado)**. Isso mesmo, uma consequência extrema é tão séria que pode alterar quem você é.

Diferente de outras consequências, uma consequência extrema não pode ser recuperada - você está preso a ela até o próximo marco maior. Depois disso você pode renomear a consequência extrema para reﬂetir que o pior já passou, contanto que não troque para o que quer que seu aspecto anterior tenha sido. Receber uma consequência extrema é uma mudança permanente; trate-a como tal.

### Conceder

Quando tudo falhar, você pode simplesmente conceder a vitória. Talvez esteja preocupado em não ser capaz de absorver o próximo golpe, ou talvez tenha decidido que continuar a lutar não vale o sofrimento. Qualquer que seja a razão, você pode interromper qualquer ação antes da rolagem e declarar a concessão do conﬂito. Isso é super importante - uma vez que os dados caiam na mesa, o que acontecer está decidido e você ou receberá mais estresse, sofrerá consequências, ou será tirado de ação.

Conceder o conﬂito dá ao outro o que ele queria de você ou, no caso de mais de dois combatentes, deixa de considerá-lo como um problema para o opositor. Você está fora do conﬂito, ponto final.

Isso não é de todo ruim. Primeiro, **você ganha um ponto de destino ao conceder**. Melhor ainda, se você recebeu qualquer consequência nesse conﬂito, você recebe um ponto de destino adicional por cada uma delas. Esses pontos de destino poderão ser usados uma vez que o conﬂito tenha acabado.

Segundo, **você evita que coisas piores aconteçam**. Sim, você perdeu e a narração reﬂetirá isso, mas você não pode usar esse privilégio para questionar a vitória do seu oponente – o que você determinar que acontece deve ser aprovado por todos no grupo.

Isso pode fazer a diferença entre, digamos, ser considerado morto e acabar sem nada, acorrentado e nas garras do inimigo - o tipo de coisa que pode acontecer se você for tirado de ação. Isso não é pouca coisa.

> No conﬂito do embarcadouro, Rakir dá uma boa surra em Esopo, atingindo vários tiros devastadores.
>
> Antes do próximo turno de Amanda, Léo diz "Eu vou conceder o combate. Não quero arriscar receber mais consequências."
>
> Ele recebe um ponto de destino por conceder, e mais dois pontos pelas duas consequências que recebeu, um total de três pontos de destino.
>
>Amanda pergunta, "Então, o que você quer evitar?" Léo responde, "Bom, pra começar, não quero ser morto ou capturado."
>
> Amanda ri e diz, "Justo. Digamos que Rakir te nocauteia, mas não se dá ao trabalho de matá-lo, porque ainda precisa lidar com Fräk e Bandu. Apesar de que sinto que alguns dentes deveriam ser perdidos…"
>
> Michel comenta, "A verdade é que Esopo não tem dentes, mas a arma dele talvez o interesse."
>
> Amanda concorda. "Sim, boa observação. Ele passa por cima de você e pega sua arma como troféu."
>
> Léo diz, "Ô, vida! Roubado mais uma vez…"

### Tirado de Ação

Se você não possuir mais caixas de estresse ou consequências disponíveis para receber as tensões de um golpe, isso significa que você foi **tirado de ação**.

Ser tirado de ação é ruim – não quer dizer apenas que você não pode mais lutar, mas que a pessoa que tirou você do conﬂito decide como ocorre a sua derrota e o que acontece com você após o conﬂito. Obviamente, ele não pode narrar qualquer coisa que não faça sentido para a situação (como você morrer, literalmente, de vergonha), mas isso concede ao oponente um grande poder sobre o seu personagem e não há nada que você possa fazer a respeito.

#### Morte do Personagem

Se você pensar bem, não há muito o que impeça que alguém, depois de tirar seu personagem de ação, diga que seu personagem morre. Se estivermos falando de um conﬂito físico envolvendo perigosas armas afiadas, parece lógico que um possível resultado de derrota seja seu personagem morrer.

Na prática, porém, essa hipótese pode ser bastante controversa dependendo do seu grupo. Alguns acham que a morte de um personagem sempre é possível, se as regras permitirem – se esse é o resultado dos dados, que assim seja.

Outros são mais cautelosos e acham muito prejudicial à sua diversão perder um personagem no qual investiram horas e horas de jogo só porque alguém gastou vários pontos de destino ou porque tiveram muita sorte nos dados.

Nós recomendamos a última abordagem, principalmente pelo seguinte motivo: na maioria das vezes, a morte repentina de uma personagem é bastante chata se comparada a fazer o personagem sofrer bastante. Além disso, todas as ligações com a história que o personagem criou ficam sem resolução e será preciso investir mais um monte de tempo e esforço pensando em como inserir um novo personagem no meio do caminho.

Isso não significa que não há como um personagem morrer durante o jogo. Nós apenas recomendamos que você guarde essa possibilidade para conﬂitos extremamente dramáticos e significativos para tal personagem – em outras palavras, conﬂitos nos quais o personagem arrisque sua vida conscientemente para poder vencer. Jogadores e Narradores que se encontrarem em um conﬂito desse tipo devem conversar sobre isso durante a montagem da cena para que todos exponham suas opiniões.

Mesmo que esteja lidando com um grupo de jogadores casca grossa que frequentemente arriscam suas vidas conscientemente, procure no mínimo deixar claras as intenções letais dos oponentes. Isso é especialmente importante para Narradores, pois os PJs saberão que os vilões estão jogando pesado e podem optar por fugir para manter os seus personagens vivos.

### Movimento

Em um conﬂito, é importante identificar onde cada um está em relação ao outro, e é por isso que dividimos o espaço onde o conﬂito ocorre em zonas. Quando você usa zonas, pessoas tentarão se mover entre elas para alcançar outras ou determinados objetivos.

Normalmente, não é difícil se mover entre uma zona e outra – **se não houver nada impedindo, você pode se mover uma zona e realizar uma ação por rodada**.

Se você deseja se mover mais de uma zona (até qualquer outro lugar no mapa) e houver algum aspecto que deixe claro que existe dificuldade para um movimento livre ou se outro personagem estiver em seu caminho, então você terá que realizar uma ação de superar usando a perícia Atletismo. Isso contará como sua ação da rodada.

O Narrador, assim como em outras rolagens de superar, determinará a dificuldade. Ela talvez possa ser o número de zonas pelas quais o personagem está se movendo ou os aspectos de situação no caminho podem justificar uma certa força de oposição passiva. Se outro personagem oferecer oposição, role contra uma oposição ativa e sinta-se livre para invocar o aspecto de situação que está obstruindo o caminho em seu próprio benefício.

Se falhar nessa rolagem, o que quer que seja impedirá você de continuar se movendo. Se empatar, você se move, mas o seu oponente recebe alguma vantagem temporária de alguma forma. Se for bem-sucedido, você se move sem sofrer consequências. Se bem-sucedido com estilo, você recebe um impulso além do movimento.

> Ainda no conﬂito, Fräkeline decide ir atrás do capanga que está atirando do andar superior. Isso exige que ela atravesse uma zona até chegar às escadas para o segundo andar e então subir, o que significa que seu oponente está a duas zonas de distância.
>
> No momento, ela está em um conﬂito com um capanga, cujo nível em Lutar é Razoável (+2).
>
> Maira explica suas intenções a Amanda, que responde, “Certo, o capanga com quem você está lutando tentará manter você onde está, assim você enfrentará uma oposição ativa”.
>
> O Atletismo de Fräkeline é Ótimo (+4). Ela lança os dados e consegue +0, mantendo o resultado Ótimo. O capanga rola a oposição e consegue -1, para um resultado Regular (+1). Isso fornece a Fräk três tensões e um sucesso com estilo.
>
> Maira e Amanda descrevem Fräk lançando uma finta sobre o capanga, depois saltando sobre um engradado e se agarrando à escada que ela sobe em apenas dois pulos. Ela recebe um impulso que chamaremos de ***Momentum***.
>
> O capanga no topo da escada engole seco, preparando seu riﬂe...

### Vantagens Em Conﬂito

Lembre-se que aquele aspecto que você criou usando a ação criar vantagem obedece a todas as regras dos aspectos de situação – o Narrador pode usá-lo para justificar ações de superar, eles duram enquanto forem relevantes e, em alguns casos, podem representar uma ameaça tanto quanto um oponente.

Ao criar uma vantagem em um conﬂito, pense em quanto tempo o aspecto permanecerá e quem terá acesso a ele. É difícil para qualquer um além de você e seus amigos justificar o uso de um aspecto criado por você sobre alguém, mas também é fácil justificar uma forma de se livrar dele usando uma ação de superar. É mais difícil justificar se livrar de um aspecto do ambiente (sério, quem vai conseguir mover a ***Estante Gigante*** que acabou de cair?), mas qualquer um na cena poderia encontrar uma forma de tirar proveito disso.

Em termos de opções para criar vantagem, o céu é o limite. Praticamente qualquer modificador de situação que possa imaginar pode ser expresso como uma vantagem. Se estiver com dificuldade, aqui estão alguns exemplos:

-  **Cegueira Temporária:** Jogar areia ou sal nos olhos de um inimigo é uma ação clássica. Isso pode colocar o aspecto ***Cegueira*** no alvo, o que pode exigir que ele tente se livrar do aspecto com uma ação de superar antes de fazer qualquer outra coisa, dependendo da gravidade. ***Cegueira*** também pode apresentar oportunidades para alguém forçar esse aspecto, então tenha em mente que o seu oponente pode tirar vantagem disso para a reposição de pontos de destino.
-  **Desarmado:** Você tira a arma de um oponente, desarmando-o até que ele consiga recuperá-la. O alvo precisa de uma ação de superar para recuperar a arma.
-  **Posicionamento:** Há diversas formas de tirar vantagem de posicionamento, como ***Terreno Elevado*** ou ***Agachado***, os quais podem ser invocados para receber vantagem de acordo com o contexto.
-  **Sem Fôlego e outras dores menores:** Alguns golpes em uma luta são debilitantes por causarem dor, mais ainda quando causam ferimentos. Golpes em pontos vitais e regiões baixas, além de outros "truques sujos” se enquadram nessa categoria. Você pode usar alguma vantagem para representar isso, como deixar o seu oponente com uma ***Dor Alucinante***, ***Atordoado*** ou algo similar e então entrar com um ataque que explore esse aspecto para causar um dano maior.
-  **Cobertura:** Você pode usar uma vantagem que represente um posicionamento com cobertura e invocá-lo em sua defesa. Isso pode ser algo bem amplo como ***Sob Cobertura*** ou algo mais específco como ***Por Trás dos Barris***.
-  **Alterando o Ambiente:** Você também pode usar as vantagens para receber benefícios do ambiente, criando barreiras com a ***Sucata Espalhada*** ou atear fogo em coisas para que fquem ***Em Chamas***. Esse último é o favorito do *Fate*.

### Outras Ações Em Um Conﬂito

Como dito acima, você pode se encontrar em uma situação onde queira fazer algo enquanto seus amigos lutam. Talvez desarmar uma armadilha mortal, buscar informações ou buscar por passagens secretas.

Para isso, o Narrador deve preparar um desafio personalizado para o jogador. Uma das tarefas provavelmente será "defender-se" - em qualquer turno em que alguém te ataca ou tenta criar vantagem em você, você deve primeiro ser bem sucedido em defender antes de fazer uma das ações do desafio. Desde que ninguém obtenha um sucesso num ataque ou crie uma vantagem sobre você, você pode usar sua ação para resolver um dos objetivos do desafio.

> Fräkeline está tentando abrir uma porta para que ela e seus companheiros fujam do Mortuário de Da'oth, onde estão cercados por guardiões robóticos.
>
> Amanda diz, “O teste de Ofícios é de dificuldade Razoável (+2) e será necessária outra rolagem, agora de Vigor com dificuldade Razoável (+2), para empurrar a porta, que é pesada. A outra ação é se defender."
>
> Na primeira rodada, Fräk é bem-sucedida em se defender do ataque, então usa sua ação para hackear a porta. Ela falha, e decide ser bem-sucedida a um custo. Amanda acha mais fácil que ela receba uma consequência, porque está em um combate. Ela abre a porta, mas não antes de um dos guardiões do templo lhe causarem um ***Ferimento Superficial***.
>
> Na próxima rodada ela falha na defesa, então não pode rolar o desafio.
> 
> Na terceira rodada, ela se defende e passa com estilo na rolagem de Vigor para abrir a porta. Ela avisa seus companheiros e escolhe ***Largada Acelerada*** como impulso pelo sucesso com estilo.

------

### Dando Cobertura e Outras Imposições

Quando estiver tentando prevenir alguém de receber um ataque, a principal forma de fazer isso é criando vantagem. Você pode usar seu turno para tornar mais difícil acertar seu companheiro.

Também é possível se inserir diretamente entre o ataque e o alvo, de forma que o vilão precise passar por você para chegar a seu amigo. Nesse caso você defende normalmente e recebe o estresse e as consequências como esperado.

Se você deseja defender alguém sem se interpor diretamente entre o alvo e o ataque, você precisa de uma façanha.

------

### Encerrando Um Conﬂito

Na maioria das vezes, quando todos os membros de um lado do conﬂito forem tirados de ação ou se concederem, o conﬂito estará finalizado.

Narradores, ao perceber o final de um conﬂito, podem distribuir os pontos de destino por rendição. Jogadores devem receber os pontos de destino por todos os aspectos que foram forçados contra eles, anotar quaisquer consequências que sofreram durante a luta e apagar as marcações nas caixas de estresse.

> Após muito esforço e insanidade, o conﬂito no porto da estação ZX19 finalmente acaba quando Amanda declara que Rakir e seu último capanga concedem o conﬂito.
>
> Como foi uma concessão, Rakir consegue escapar e poderá aparecer em conﬂitos futuros. Como Léo concedeu anteriormente neste conﬂito, Rakir leva sua arma como troféu pessoal. Tendo concedido, Léo também recebe três pontos de destino; um por conceder e dois pelas consequências suave e moderada que recebeu. Todas as invocações usadas contra ele foram grátis, então isso é tudo que ele recebe.
>
> Michel recebe dois pontos de destino porque Amanda fez um dos capangas xingar Bandu, invocando “***Me Ofendo por Pouco***” duas vezes durante o conﬂito.
> 
> Maira não recebe nenhum ponto de destino, porque todas as invocações contra ela foram gratuitas, vindas de impulsos e vantagens. Como venceu, ela não é recompensada pelas consequências que recebeu.

------

### Ações Livres

Às vezes faz sentido que seu personagem faça algo diferente em conjunto com outra ação, ou a fim de contribuir com aquela ação mais tarde. Você saca sua arma antes de atirar, grita um aviso antes de chutar a porta, ou olha rapidamente dentro da sala antes de entrar atacando. Essas pequenas ações servem para dar vida a descrição, para adicionar drama à cena.

Narradores, não caiam na armadilha de policiar cada detalhe que os jogadores falam. Lembre-se, se algo não é significativamente interessante para oferecer oposição, você não deveria exigir uma rolagem - simplesmente deixe que o jogador consiga o que deseja. Recarregar uma arma ou procurar algo no bolso é parte da realização de uma ação. Você não deveria exigir detalhes mecânicos para realizar esse tipo de coisa.

------

#### Transição Para um Conﬂito ou Disputa

Você pode se encontrar num conﬂito onde os participantes deixem de querer continuar ferindo uns aos outros devido a alguma mudança nas circunstâncias. Se isso acontecer e ainda houver algo a mais para resolver, é possível fazer uma transição instantânea para uma disputa ou desafio, conforme necessário. Nesse caso, segure os pontos de destino que seriam entregues ao final do conﬂito e outros efeitos até que a disputa ou desafio tenha terminado.

> Anteriormente ([p. XX]()), Fräkeline conseguiu abrir a grande porta da tumba para que o grupo pudesse escapar. Eles decidem correr e deixar os guardiões comendo a própria poeira.
> 
> Agora os guardiões e os PJs estão se opondo da mesma forma, mas sem ferir uns aos outros, logo se trata de uma disputa. Em vez de começar outro confronto, Amanda apenas organiza a perseguição.
> 
> Mesmo que os PJs tenham recebido alguma consequência e tenham pontos de destino pendentes, eles não os receberão até sabermos se eles conseguiram escapar ou se foram pegos.

## Trabalho Em Equipe

Personagens podem ajudar uns aos outros em ações. Há duas formas de fazer isso em *Fate* - combinando perícias, para quando todos estiverem fazendo o mesmo tipo de esforço na ação (como usar Vigor em conjunto para derrubar uma parede em ruínas) e acumulando vantagens, para quando o grupo faz os preparativos para que uma única pessoa tenha sucesso (como causar múltiplas distrações para que alguém use Furtividade para entrar numa fortaleza).

Quando combinar perícias, avalie quem possui o maior valor entre os participantes. Qualquer outro participante que possuir a mesma perícia em um nível ao menos Regular (+1) adiciona +1 na jogada do personagem com maior nível e apenas esse personagem faz a rolagem. Logo, se três pessoas estiverem lhe ajudando e você possui o maior nível na perícia, você receberá um bônus de +3 na sua rolagem.

Se você falhar em uma rolagem de perícia combinada, todos os participantes compartilham os custos – qualquer complicação que afetar um personagem afetará todos eles ou todos recebem consequências. Como alternativa, você pode impor um custo que afete todos os personagens igualmente.

> Para fugir dos guardiões, o grupo decide que é mais fácil apenas combinar perícias.
>
> Dos três PJs, Fräkeline possui o maior valor em Atletismo (+4). Esopo possui um nível Bom (+3) e Bandu, Regular (+1), então cada um deles oferece um bônus de +1. Com ajuda de seus companheiros, Fräkeline rolará com um nível Fantástico (+6).
>
> Os guardiões do templo possuem apenas Regular (+1) em Atletismo, mas há cinco deles, então eles rolarão com Excepcional (+5) para essa disputa.

Quando você acumula vantagens, cada pessoa faz uma ação de criar vantagem normalmente e concede qualquer invocação gratuita que conseguiu a um único personagem. Lembre-se que múltiplas invocações de um mesmo aspecto podem ser cumulativas.

> Bandu e Fräkeline querem ajudar Esopo a acertar a nave de Rakir, que está fugindo, com um tiro de canhão orbital.
>
> Tanto Fräkeline quanto Bandu rolam para criar vantagem em seus turnos, resultando em três invocações gratuitas de ***Tiros de Distração*** com os canhões secundários, com o sucesso na rolagem de Bandu e o sucesso com estilo de Fräk, que adiciona mais duas invocações grátis.
>
> No turno de Esopo, ele usa toda essa vantagem para um ataque Fantástico (+6).

