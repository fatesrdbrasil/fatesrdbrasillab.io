---
title: Crie Problemas
layout: default
---

## Crie Problemas

Criar um cenário começa com buscar um problema para os PJs resolverem. Um bom problema é **relevante para os PJs, não pode ser resolvido sem seu envolvimento pessoal e não pode ser ignorado sem que haja consequências terríveis**.

Isso pode parecer uma tarefa difícil. Felizmente, você possui uma excelente ferramenta de contar histórias para ajudá-lo a criar problemas adequados para as suas partidas: aspectos.

Os aspectos dos seus PJs possuem bastante conteúdo histórico – eles representam o que é importante sobre (e para) cada personagem; eles mostram características do mundo do jogo às quais os PJs estão conectados e descrevem os diferentes lados da personalidade de cada um.

Há também os aspectos ligados ao seu jogo – problemas iminentes, aspectos de local e quaisquer outros colocados sobre as características da campanha. Explorá-los pode ajudá-lo a reforçar a sensação de um mundo dinâmico e manter a premissa central do seu jogo em foco.

Por causa de todos esses aspectos, você já possui uma tonelada de histórias possíveis na palma de sua mão – agora só é preciso liberar seu potencial.

Podemos imaginar um problema relacionado a um aspecto como se fosse um ato de forçar de grande escala baseado em um evento. Organizar as coisas dá um pouco mais de trabalho, mas a estrutura é similar – possuir um aspecto sugere algo problemático para um ou todos PJs. Mas, diferentes de um ato de forçar, é algo que não podem resolver ou lidar naquele exato momento.

------

### Você Nem Sempre Precisa Destruir O Mundo

Como verá nos exemplos, nem todas as nossas urgências envolvem necessariamente o destino do mundo ou mesmo de grande parte dele. Problemas interpessoais podem possuir tanto impacto em um grupo de PJs quanto ter que impedir o vilão da semana – ganhar o respeito de alguém ou resolver uma questão pendente entre dois personagens pode ganhar foco em um cenário tanto quanto um grande plano maléfico de um vilão especial.

Se você deseja uma história clássica de ação, veja se pode criar dois problemas para o seu cenário – um que foque em algo externo (como o plano do vilão) e outro que lide com questões pessoais dos personagens. Este último servirá como um drama secundário em seu cenário e cria um tempo para que os personagens se desenvolvam enquanto lidam com outros problemas.

------

### Problemas e Aspectos de Personagem

Quando estiver tentando criar algum problema a partir de um aspecto de personagem, tente encaixá-lo de alguma forma no parágrafo abaixo:  

- Você possui o aspecto \_\_\_, o que implica em \_\_\_ (aqui você pode criar uma lista de coisas). Por isso, \_\_\_ pode se tornar um grande problema para você.

O segundo espaço em branco é o que torna isso mais difícil do que forçar um aspecto baseado num evento – você precisa pensar em todas as implicações do aspecto. Aqui vão algumas questões que podem ajudar.

-  Quem pode ter um problema com o personagem por causa desse aspecto?
-  O aspecto indica alguma ameaça àquele personagem?
-  O aspecto descreve alguma conexão ou relacionamento que poderiam causar problemas ao personagem?
-  O aspecto se refere a algo na história do personagem que poderia voltar para persegui-lo?
-  O aspecto descreve algo ou alguém importante para o personagem que você pode pôr em perigo?

Você pode seguir em frente desde que o que for colocado na terceira lacuna preencha os critérios do início desta seção.

> Fräkeline possui o aspecto ***Famosa Ladra Robótica*** que sugere que sua reputação seja conhecida por muitos. Por causa disso, um sósia começou a cometer crimes em seu nome, provavelmente fazendo com que cidadãos irritados passem a persegui-la em várias cidades, ou até mesmo que haja assassinos na sua cola.
> 
> Esopo tem o aspecto ***O Explorador Sabe Meus Segredos***, o que significa que ele se sente na obrigação de manter o Explorador por perto, em segurança no alcance de suas antenas. Por causa disso, Esopo precisa ajudá-lo a se livrar de alguns velhos companheiros de crime que estão atrás de seu dinheiro.
> 
> Bandu possui ***O Império Quer Minha Cabeça***, o que significa que soldados do Império estão constantemente atrás dele. Isso resulta numa série de tentativas de assassinato e captura começam, planejadas por facínoras em todos os cantos do universo.

### Problemas e os Aspectos de Jogo

Os problemas criados a partir das questões do jogo terão um alcance maior que os problemas baseados nos personagens, afetando todos os seus PJs e possivelmente um número considerável de PdNs também. Eles são menos pessoais, mas isso não os torna menos interessantes.

-  Como \_\_\_ é uma questão do cenário, isso implica em \_\_\_. Portanto, \_\_\_ provavelmente criaria um grande problema para os PJs. Pergunte-se:
-  Que desafos essa questão representa aos PJs?
-  Quem está guiando as coisas por trás dessa questão e que tipo de estrago ele pode ter que fazer para que seu plano seja concretizado?
-  Quem mais se importa em lidar com a questão e como sua “solução” pode ser ruim para os PJs?
-  Qual seria um bom passo para resolver a questão e o que dificulta que isso seja executado?

------

### Dê-Lhe Um Rosto

Embora nem todos os problemas do cenário precisem ser causados diretamente um PdN que serve a um "vilão supremo" que os PJs precisam derrotar, é mais fácil se for assim. Você no mínimo deve ser capaz de apontar um PdN que se beneficia consideravelmente daquele problema não ser resolvido da forma que os PJs gostariam.

------

> Como o ***Clã do Dedo*** é uma questão do cenário, isso implica no seu poder por toda a região do jogo. Portanto um golpe de estado planejado por membros do Clã na cidade em que os PJs estão provavelmente será um grande problema para eles.
> 
> Como ***A Bruxaria Retornará*** é uma questão do cenário, isso implica nos Bruxos de Moondor estarem constantemente tentando cumprir suas antigas profecias e adquirindo novos servos. Portanto uma série de rituais buscando despertar um poder antigo e perigoso que repousa na escuridão do espaço causará problemas para os PJs.
> 
> Como os ***Bruxos de Moondor*** possuem Problemas de Alinhamento, conﬂitos em diversos planetas estão a ponto de acontecer. Uma guerra espacial colocará a vida de inocentes em risco, criando um grande problema para os PJs.

### Problemas e Aspecto Pareados

É aqui que a coisa fica ainda mais interessante. Você também pode criar problemas a partir da *relação entre dois aspectos ao invés de um só*. Isso permite a deixar as coisas em um nível pessoal, mas alargar o âmbito do seu problema para impactar vários personagens ou unir a história de um PJ à história do jogo.

Há duas formas básicas de parear aspectos: conectar aspectos de dois personagens e conectar o aspecto de um personagem a uma questão.

#### Aspectos de Dois Personagens

-  Como \_\_\_ possui o aspecto \_\_\_ e \_\_\_ possui o aspecto \_\_\_, sugere que em \_\_\_. Portanto \_\_\_ provavelmente será um grande problema para eles.

Pergunte-se:

-  Os aspectos colocam os personagens em desacordo ou sugere alguma tensão entre eles?
-  Existe algum tipo específico de problema ou dificuldade ao qual os dois estejam propensos por causa de tais aspectos?
-  Será que um dos personagens tem uma relação ou conexão que poderia tornar-se um problema para outros?
-  Será que os aspectos apontam elementos da história dos personagens que possam se cruzar com fatos recentes?
-  Existe alguma forma da vantagem de um PJ se tornar o infortúnio de outro, por causa dos aspectos?

> Porque Bandu possui ***O Império Quer Minha Cabeça*** e Esopo é um *Mercenário Lagosta*, isso sugere que Esopo pode ser contratado para eliminar seu próprio colega de equipe.
> 
> Como Fräkeline possui ***Se É Valioso, Poderia Ser Meu*** e Esopo possui ***Não Entendo Absolutamente Nada***, isso sugere que eles são provavelmente os piores parceiros possíveis para um roubo enquanto disfarçados. Portanto, um contrato para entrar no Baile Real de Lachesis 4 sem serem notados e chegar até as joias reais provavelmente será um grande problema.
> 
> Como Bandu é um ***Mentalista Andarilho*** e Fräkeline possui ***Falo Comigo Mesma No Passado***, isso sugere que, num momento de confronto, Bandu pode acabar entendendo a verdade sobre as viagens no tempo de Fräk/Zoidai. E como essa informação não pode se tornar pública, faz sentido que Zoidai faça o possível para eliminá-lo.

#### Aspecto de Personagem e Questão

-  Como você possui o aspecto \_\_\_ e \_\_\_ é uma questão do cenário, isso sugere que \_\_\_. Portanto \_\_\_ pode ser um grande problema para você. 

Pergunte-se:

-  Será que uma questão sugere uma ameaça no relacionamento entre os PJs?
-  Será o próximo passo para lidar com uma questão é algo que afeta um personagem em particular por causa de seus aspectos?
-  Será que alguém ligado à questão tem um motivo em especial para ter o PJ como alvo por causa do aspecto?

------

### Quantos Problemas São Necessários?

Para um jogo simples, um ou dois problemas são suficientes. Você verá abaixo que mesmo um único problema pode criar material suficiente para duas ou três sessões. Não pense que você precisa conectar cada PJ a cada cenário - ao invés disso, alterne o foco entre os personagens para que todos tenham seu momento, e a partir disso jogue-os dentro da trama principal que.

------

> Como Fräkeline possui ***Falo Comigo Mesma No Passado*** e o ***Império Mareesiano*** é um problema, isso sugere que o Império pode ter alguma inﬂuência sobre Fräkeline, chantageando-a. Portanto o Império a contrata para um trabalho extremamente perigoso e moralmente duvidoso ameaçando revelar seu segredo e torná-la uma inimiga pública pela galáxia, o que pode ser um grande problema para ela.
> 
> Como Bandu possui ***Funciono Sob Pressão***, e o ***Império Mareesiano*** é um problema, isso sugere que Bandu possui algo que a Imperatriz deseja. Ele pode ser forçado a aceitar utilizar seus poderes mentais contra inimigos do Império. Isso pode acabar voltando a raiva de terceiros para si e trazer problemas relacionados.
> 
> Como Esopo possui ***Não Entendo Absolutamente Nada*** e ***A Bruxaria Retornará!***, Esopo pode se envolver com os Bruxos de Moondor, eventualmente ajudando-os na drenagem de seu mar de origem, o que provavelmente será uma grande dor de cabeça para ele e seus compatriotas.

- [« O Que é Um Cenário?](../o-que-e-um-cenario/)
- [Perguntas Narrativas »](../perguntas-narrativas/)

