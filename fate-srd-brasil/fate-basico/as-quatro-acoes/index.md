---
title: As Quatro Ações
layout: default
---

## As Quatro Ações

**Quando você faz uma rolagem de perícia, você escolhe uma das quatro ações: superar, criar vantagem, atacar ou defender.**

Existem quatro tipos de ação que você pode realizar em *Fate*. Quando você faz uma rolagem de perícia, você precisa decidir qual deles você tentará. A descrição de cada perícia diz a você quais são as ações apropriadas àquela perícia e em quais circunstâncias normalmente, a ação que você precisa escolher ficará bem óbvia de acordo com a descrição da perícia, seguindo seus instintos e a situação de jogo, mas às vezes você precisará conversar com o grupo para encontrar a mais apropriada.

As quatro ações são: **superar**, **criar vantagem**, **atacar** e **defender**.

### `O`{: .fate_font} Superar

**Use a ação superar para alcançar objetivos variados de acordo com a perícia usada.**

Cada perícia engloba um grupo específico de esforços variados, situações nas quais aquela perícia se torna ideal. Um personagem com Roubo tenta arrombar uma janela, um personagem com Empatia tenta acalmar uma multidão e um personagem com Ofícios tenta consertar o eixo quebrado de uma carroça após uma perseguição desesperada.

Quando o seu personagem estiver em uma dessas situações e houver algo entre ele e seu objetivo, você usa a ação superar para lidar com isso. Pense nesta ação como o “faz tudo” de toda perícia – se o que deseja realizar não se enquadra em nenhuma outra categoria, provavelmente é uma ação de superar.

A oposição que precisa ser superada pode ser passiva ou ativa, dependendo da situação.

-  **Quando falhar em uma ação de superar,** há duas opções. Você pode simplesmente falhar, o que significa que não alcança seu objetivo, ou consegue o que deseja a um custo severo.
-  **Quando empatar em uma ação de superar,** você consegue o seu objetivo ou consegue o que deseja, mas a um custo menor.
-  **Quando for bem-sucedido em uma ação de superar,**  você consegue o que deseja sem nenhum custo.
-  **Quando for bem-sucedido com estilo em uma ação de superar,** você consegue um impulso além de alcançar o que desejava.

------

Ocasionalmente você pode se deparar com situações que sugiram benefícios ou penalidades diferentes das apresentadas no livro. Tudo bem se for preciso voltar às descrições básicas das 4 resoluções e tentar criar algo que faça sentido.

Por exemplo, em uma ação de superar é dito que você consegue um impulso quando é bem-sucedido com estilo, mas se essa ação de superar estiver encerrando uma cena ou você não consegue pensar em um impulso legal, poderia escolher adicionar algum detalhe à história ao invés de receber o impulso.

------

> Esopo espreita pelos armamentos da fortaleza de Mareesa, A Imperatriz, tentando sabotar os canhões anti-gravidade. Se ele for bem-sucedido, o exército rebelde que o contratou tem uma chance muito maior de vitória, permitindo que suas naves se aproximem.
>
> Amanda diz “Certo, então você chega ao controles do canhão e começa a desligar o equipamento, mas começa a ouvir passos ecoando na sala anterior – parece que o próximo guarda da patrulha chegou um pouco mais cedo”.
>
> “Droga”, diz Léo. “Fui encontrar o único guarda disciplinado da armada. Preciso desativar isto e dar o fora – o General Ephon me alertou que se me encontrassem, ele negaria minha existência”.
>
> Amanda dá de ombros e diz “Quer acelerar o trabalho? Você vai enfrentar uma oposição passiva aqui – tempo curto e lidar com máquinas complexas, eu acho que a dificuldade é de nível Ótimo (+4)”.
>
> Esopo tem a perícia Ofícios em nível Regular (+1). Léo resmunga e diz, “Bandu é quem devia estar cuidando disto.” Ele lança os dados e consegue +2, para um resultado Bom (+3). O que não é suficiente.
>
> Léo pega um ponto de destino e diz “Bem, como sempre digo... ***Bater Sempre Funciona***”, se referindo a um de seus aspectos. Amanda ri e acena com a cabeça, e com a invocação, ele consegue um resultado Excepcional (+5). Isso é bom para um sucesso, mas não o suficiente para um sucesso com estilo, então Esopo consegue seu objetivo sem nenhum custo.
>
> Ele descreve como consegue sabotar o equipamento simplesmente por arrancar o máximo de partes que conseguir, antes de mergulhar num canto escondido quando os guardas se aproximam...

------

### `C`{: .fate_font} Criar Vantagem

**Use a ação de criar vantagem para criar aspectos de situação que trazem benefícios ou para receber os benefícios de qualquer aspecto a qual tenha acesso.**

A ação de criar vantagem abrange uma grande variedade de esforços no sentido de usar suas habilidades para tirar vantagem (daí o nome) do ambiente ou da situação.

Às vezes isso significa fazer algo para mudar drasticamente as circunstâncias ao seu redor (como jogar areia nos olhos do adversário ou pôr fogo em algo), mas também pode significar que você consegue novas informações que possam ajudá-lo (como conseguir ler a fraqueza de um monstro através de pesquisas) ou tirar vantagem de algo que estudou previamente (como a predisposição de um oponente para um temperamento ruim).

Ao criar vantagem, você deve especificar se está criando um novo aspecto de situação ou se está tirando vantagem de um aspecto existente. No primeiro caso, você vai colocar esse novo aspecto de situação em um personagem ou no ambiente?

A oposição pode ser ativa ou passiva, dependendo das circunstâncias. Se o alvo é outro personagem, sua rolagem sempre contará como uma ação de defesa. Se você estiver usando a ação criar vantagem para criar um aspecto...

-  **Quando falhar,** o aspecto não é criado, ou é, mas outra pessoa recebe a invocação gratuita – ou seja, você consegue, mas outra pessoa ganha vantagem disso. Pode ser o seu oponente em um conﬂito ou qualquer personagem que possa se beneficiar em seu detrimento. Pode ser preciso reformular o aspecto de forma que o outro personagem receba os benefícios – trabalhe nisso com o beneficiado da forma que fizer mais sentido.
-  **Quando empatar,** você consegue um impulso no lugar do aspecto de situação que estava tentando criar. Isso significa que você pode precisar renomear o aspecto para reﬂetir sua natureza temporária (***Terreno Acidentado*** pode virar ***Pedras no Caminho***).
-  **Quando for bem-sucedido,** você cria o aspecto de situação com uma invocação grátis.
-  **Quando for bem-sucedido com estilo,** você consegue criar o aspecto de situação e duas invocações grátis no lugar de apenas uma.

> Nas profundezas dos Esgotos de Kuna Korden, Fräkeline está em uma posição complicada, tendo que lutar com alguns membros do Povo Toupeira.
>
> As primeiras rodadas não foram muito bem e ela recebeu alguns golpes certeiros. Maira diz “Amanda, você disse que há latões, vigas de ferro e outras coisas espalhadas, certo?”
>
> Amanda diz "Sim, há" e Maira pergunta “Posso chutar algo na direção deles para dificultar um pouco as coisas? Imagino que por serem grandes e cegos, não são tão ágeis quanto eu”.
>
> Amanda diz “Por mim tudo bem. Isso é uma ação de criar vantagem usando Atletismo. Um dos touperídeos vai tentar uma ação de defesa, pois está perto o suficiente”.
>
> Fräkeline possui Atletismo Ótimo (+4). Maira lança os dados e consegue +1, atingindo um resultado Excepcional (+5). O homem-toupeira mais próximo faz a rolagem para se defender e consegue um resultado Razoável (+2). Fräk é bem-sucedida com estilo! Maira acrescenta o aspecto ***Entulhos no Caminho*** à cena e pode invocar esse aspecto duas vezes de graça.
>
> Amanda descreve a dificuldade dos homens-toupeira ao tentar se locomover e agora Fräk tem um pouco de vantagem sobre eles...

Se estiver criando vantagem em um aspecto existente...

-  **Quando falhar,** você concede uma invocação grátis a alguém. Pode ser seu oponente em um conﬂito ou outro personagem que possa se beneficiar.
-  **Quando empatar ou for bem-sucedido,** você consegue invocar o aspecto gratuitamente.

-  **Quando for bem-sucedido com estilo,** você consegue duas invocações grátis do aspecto.

> Bandu foi contratado pelo raiduque de Rafatalakan para observar um traficante de especiarias que opera no mercado galáctico da Nebulosa Cadente.
>
> Michel diz, “Vou usar Comunicação para criar vantagem, pra fazer ele se abrir para mim. Não sei exatamente o que estou procurando em termos de aspectos – apenas alguma observação valiosa que eu possa usar depois ou informar a Fräk.” Ele possui a façanha **Mentiroso Amigável** então pode fazer isso sem usar a perícia Enganar, escondendo suas reais intenções.
>
> Amanda diz, “Tudo bem pra mim. Ele é um comerciante, então seu nível de Enganar é bastante alto. Vou considerar isso uma oposição passiva, pois ele não desconfia de você. Tente bater uma dificuldade Ótima (+4)”.
>
> Michel rola os dados. Sua perícia Comunicação é Boa (+3) e ele consegue +1 nos dados, o que lhe concede um empate.
>
> Amanda observa suas anotações, sorri e diz “Certo, você nota o seguinte. Esse traficante é obviamente uma pessoa bastante social, se entrosa com outros mercadores e clientes em potencial por onde passa. Isso muitas vezes toma a forma de um ﬂerte, um ar sugestivo de paquera sempre que ele fala com rapazes – ele parece não conseguir evitar”.
>
> Ela mostra um dos cartões de anotações com o aspecto ***Gamado Em Rapazes Bonitos*** escrito nele, para indicar que esse aspecto do mercador agora é público. Michel percebe que pode invocar esse aspecto uma vez, gratuitamente. “Rapazes bonitos, é?” Michel diz. “Ele me achou bonito?”.
>
> Amanda sorri. “Bem, ele achou você amigável...”
>
> Michel balança a cabeça. “As coisas que faço pelos negócios...”

### `A`{: .fate_font} Atacar

**Use a ação de atacar para ferir alguém em um conﬂito ou tirá-lo da cena.**

Atacar é a mais direta das quatro ações – quando quiser ferir alguém em um conﬂito, use um ataque. Um ataque não é necessariamente físico por natureza; algumas perícias permitem a você machucar alguém mentalmente.

Na maioria das vezes seu alvo oferece uma oposição ativa contra o seu ataque. Oposição passiva em um ataque significa que você pegou seu alvo desprevenido ou de alguma outra forma incapaz de reagir com eficiência, ou por ser um PdN sem importância e não valer a pena se preocupar com os dados.

Além disso, sendo passiva ou não, a oposição sempre conta como uma ação de defesa, então você pode dar uma olhada nessas duas ações, já que estão intimamente ligadas.

-  **Quando falhar em um ataque,** você não causa nenhum dano em seu alvo (também significa que o seu alvo é bem-sucedido em sua ação de defesa, o que pode gerar outros efeitos).
-  **Quando empatar em uma ação de ataque,** você não causa dano algum, mas consegue um impulso.
-  **Quando for bem-sucedido em um ataque,** você inﬂige **dano** em seu alvo igual ao número de tensões que conseguir. Isso força o alvo a tentar gastar caixas de estresse ou receber consequências; se isso não for possível, seu alvo tem que sair do conﬂito.
-  **Quando for bem-sucedido com estilo,** prossiga como num sucesso normal, mas você também tem a opção de reduzir o valor do dano em um e receber um impulso.

> Fräkeline está travando um combate com Damien, um dos famosos Mareesianos, a guarda de elite da Imperatriz Galáctica. Com seu jeito particular de lutar, Fräk tenta atacá-lo com seu chicote de energia.
>
> A perícia Lutar de Fräkeline é Boa (+3). Damien é Ótimo (+4) em Lutar. Maira rola os dados e consegue +2 para um ataque Excepcional (+5).
>
> Amanda rola dos dados por Damien e obtém -1, rolagem de nível Bom (+3). Maira vence por dois, inﬂigindo 2 tensões.
>
> Ela acha, porém, que isso não é bom o suficiente. “Também vou invocar ***Famosa Ladra Robótica***” ela diz, “porque sou conhecida por não deixar nada mal resolvido”.
>
> Maira paga o ponto de destino, tornando o seu resultado final Épico (+7). Ela consegue 4 tensões, obtendo assim um sucesso com estilo, provocando um grande golpe. Ela escolhe inﬂigir as 4 tensões em dano, mas também poderia ter inﬂigido apenas 3 e escolhido um impulso.
>
> Agora Damien precisa sofrer esse estresse ou receber consequências para se manter na luta!

### `D`{: .fate_font} Defender

**Use a ação de defender para evitar ataques ou evitar que alguém crie uma vantagem sobre você.**

Sempre que for atacado em um conﬂito ou quando tentarem criar uma vantagem sobre você, haverá a chance de se defender. Como no ataque, isso não significa necessariamente se defender de ataques físicos – algumas perícias permitem se defender de ataques mentais.

Como a rolagem de defesa é uma reação, sua oposição será quase sempre ativa. Se você rolar defesa contra uma oposição passiva, será porque o ambiente é hostil de alguma forma (como num incêndio), ou o PdN não é tão importante para o narrador se importar com os dados.

-  **Quando falhar na sua defesa,** você sofre as consequências do que quer que esteja tentando evitar. Você recebe o dano ou as características da vantagem criada sobre você.
-  **Quando empatar na sua defesa,** você concede um impulso ao seu oponente.
-  **Quando for bem-sucedido na sua defesa,** você consegue evitar o ataque ou a tentativa de criar vantagem sobre você.
-  **Quando for bem-sucedido com estilo,** encare como um sucesso normal, mas você também ganha um impulso, o que pode permitir que você vire o jogo.

------

### Posso Me Defender de Ações de Superar?

Tecnicamente, não. A ação de defesa foi criada para que você evite receber estresse, consequências ou aspectos de situação – basicamente, para proteger você de todo tipo de coisa ruim que possa ser representada pela mecânica do jogo.

Porém, é possível rolar uma oposição ativa se você puder atrapalhar *qualquer* ação, como mostram as [regras sobre a oposição](../acoes-e-resolucoes/#oposição). Logo, se alguém realizar uma ação de superar que possa falhar porque você está no caminho de alguma forma, diga “Ei! Não é bem assim!”, pegue os dados e role como oposição. Você não ganha nenhum benefício extra como quando usa a ação defender, mas você também não precisa se preocupar com as desvantagens de falhar.

------

> Bandu, O Oculto está discutindo com outro estudioso, Raithua Kaluki, no Museu do Consenso Universal a respeito da origem de uma relíquia em exposição.
>
> Raithua não quer apenas que Bandu seja desacreditado, mas também que ele perca a autoconfiança forçando-o a dar um passo em falso e duvidar de si mesmo. O grupo concorda que, como Kaluki é bastante reconhecido no meio, ele pode tentar afetá-lo dessa forma, então o conﬂito inicia.
>
> Assim que Bandu termina o seu argumento inicial, Amanda descreve como Kaluki usa um ataque de Provocar, encontrando brechas nas teorias de Bandu e forçando-o a se reavaliar. Kaluki tem nível Bom (+3) em Provocar.
>
> Bandu defende com Vontade, que ele possui em um nível Razoável (+2). Amada rola os dados por Kaluki e consegue +1, para um total Ótimo (+4). Michel lança os dados por Bandu e consegue +2 alcançando um resultado Ótimo (+4). Bandu não precisa se preocupar em receber qualquer tipo de dano, mas ele fornece um impulso a Kaluki, o qual Amanda decide ser ***Lapso Momentâneo.***

------

### Sem Efeitos Cumulativos!

Você notará que a ação defender possui resultados que espelham algumas resoluções das ações ataque e criar vantagem. Por exemplo, é dito que quando você empatar em uma jogada de defesa, você fornece ao seu oponente um impulso. Em Atacar, explicamos que quando você empatar, você recebe um impulso.

Isso não significa que o atacante recebe dois impulsos – é o mesmo resultado de dois pontos de vista diferentes. Nós apenas escrevemos assim para que os resultados fossem consistentes quando você lê as regras, independente de que maneira a coisa foi resolvida.

------

- [« As Quatro Resoluções](../as-quatro-resolucoes/)
- [Desafios, Disputas e Conflitos »](../desafios-disputas-e-conflitos/)

