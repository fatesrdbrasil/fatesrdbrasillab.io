---
title: A Ficha de Personagem
layout: default
---

## A Ficha de Personagem

Jogadores, sua ficha de personagem contém tudo que você precisa saber sobre seu PJ – habilidades, personalidade, pontos importantes sobre sua história e outros recursos que os personagens usarão em jogo. Aqui há um exemplo de uma ficha de personagem para que possa anotar todos os componentes.

### [Aspectos](../aspectos-e-pontos-de-destino/)

Aspectos são frases que descrevem algum detalhe significante sobre um personagem. Eles são o que definem *porque o seu personagem é importante*, porque estamos interessados em ver o seu personagem no jogo. Aspectos podem cobrir uma grande variedade de elementos, como personalidade ou traços descritivos, crenças, relacionamentos, problemas ou qualquer outra coisa que nos ajude a identificar o personagem como pessoa, e não apenas um conjunto de atributos.

Aspectos entram em jogo através do gasto de pontos. Quando o seu aspecto beneficiá-lo, você pode gastar pontos de destino para **invocar** aquele aspecto na forma de um bônus. Quando seus aspectos complicarem a vida de seu personagem, você pode ganhar um ponto de destino de volta – isso é chamado de **forçar**. Veja a seção abaixo na página XX para mais informações sobre isso.

> A personagem de Maira, Fräkeline, tem o aspecto ***Se É Valioso, Poderia Ser Meu*** em sua ficha, que descreve sua tendência de supervalorizar bens materiais e tomar decisões perigosas quando créditos e objetos valiosos estão envolvidos. Isso adiciona um elemento interessante e divertido à personagem, trazendo um punhado de problemas e trazendo personalidade ao jogo.

Aspectos podem descrever coisas que são benéficas ou prejudiciais – na verdade, os melhores aspectos fazem ambos.

Além disso, não são só personagens que têm aspectos; o ambiente onde se encontram seus personagens também podem ter aspectos ligados a ele.

### [Perícias](../pericias-e-facanhas/)

**Perícias** são o que você usa durante o jogo para realizar ações complicadas ou interessantes com os dados. Cada personagem tem um número de perícias que representa suas capacidades básicas, incluindo coisas como percepção, poder físico, treinamento profissional, educação e outras habilidades.

No início do jogo, o personagem possui perícias divididas por níveis, que vão de Regular (+1) a Ótimo (+4). Quanto mais alto melhor, mostrando que o personagem é capaz ou é mais fácil de ser bem-sucedido quando usa aquela perícia.

Se por alguma razão for preciso realizar uma rolagem usando uma perícia que seu personagem não possui, sempre será possível rolar com o nível Medíocre (+0). Há algumas exceções a isso, como perícias mágicas que a maioria das pessoas não dominam. Abordaremos as perícias detalhadamente em seu próprio capítulo.

> Bandu, O Oculto tem a perícia Conhecimentos em nível Ótimo (+4), o que o torna a pessoa certa para conhecer fatos obscuros e convenientes. Porém, ele não possui a perícia Furtividade, logo quando for necessário ser sorrateiro (e Amanda fará o possível para isso acontecer) ele terá que fazer uma rolagem de nível Medíocre (+0). Más notícias para ele.

### [Façanhas](../pericias-e-facanhas/#o-que-É-uma-façanha)

**Façanhas** são truques especiais que seu personagem sabe e que lhe permitem obter algum benefício extra de uma perícia ou alterar alguma regra do jogo para funcionar a seu favor. Façanhas são como movimentos especiais em um jogo eletrônico, permitindo que você realize algo único ou exótico comparado a outros personagens. Dois personagens podem ter o mesmo nível em uma perícia, mas suas façanhas podem fornecer benefícios bem variados.

> Esopo tem uma façanha chamada ***Um Trago Por Um Fato***, que lhe dá um bônus para conseguir informações com sua perícia Comunicação, contanto que esteja bebendo com seu alvo em um bar.

### [Extras](../extras/)

**Extras** são os poderes, ferramentas, veículos, organizações e locais para as quais seu grupo pode querer criar algumas regras (caso aspectos, perícias e façanhas não sejam o bastante).

### [Recarga](../economia-de-pontos-de-destino/)

**Recarga** é o número de pontos de destino que você recebe no início de cada sessão. Seu total de pontos de destino sempre é restaurado a esse valor, a menos que você tenha terminado a sessão anterior com um valor maior.

### [Estresse](../conflitos/#estresse)

**Estresse** é uma das duas opções que você tem para evitar perder um conﬂito – ele representa o cansaço temporário, atordoamento, ferimentos superficiais e coisas semelhantes. Há um certo número de níveis de estresse que você pode gastar para se manter na luta. Esses níveis são recuperados ao final do conﬂito, desde que você tenha um momento para descansar e recuperar o fôlego.

### [Consequências](../conflitos/#consequências)

**Consequências** são a outra opção que você possui para se manter em um conﬂito, mas elas têm um impacto mais duradouro. Todas as vezes que você adquire uma consequência, um novo aspecto descrevendo seus ferimentos é adicionado à sua ficha. Diferente do estresse, é preciso de tempo para se recuperar de uma consequência, o que deixa seu personagem vulnerável à complicações ou a outros que queiram tirar vantagem da sua nova fraqueza.

------

### Um Exemplo de Jogo

Todos os exemplos de regras neste livro se referem ao mesmo cenário de campanha. O nome é Caroneiros de Asteroide, uma ficção espacial com um pequeno grupo de aventureiros com alguns problemas a resolver. Eles viajam pelo universo e entram em confusões a mando dos vários senhores que os contrataram.

Os participantes são Léo, Maira, Michel e Amanda. Amanda é a Narradora. Léo joga com um mercenário-lagosta chamado Esopo. Maira joga com a esperta, audaciosa e veloz ciborgue Fräkeline, que adora surrupiar coisas valiosas. Michel interpreta Bandu, O Oculto, um mentalista criado por uma nave espacial.

Veja a sessão [*Estruturando o Jogo*](../estruturando-o-jogo/) para ver como este jogo foi criado. Nós incluímos as fichas dos personagens como exemplo de PJs no final deste livro.

------

- [« Jogadores e Narrador do Jogo](../jogadores-narrador-de-jogo/)
- [Realizando Ações »](../realizando-acoes/)
