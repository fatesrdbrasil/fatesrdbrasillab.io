---
title: Fragmentos de Poder
layout: default
---

## Fragmentos de Poder

Suponhamos que você tenha uma ideia para um sistema de magia que faz sentido dentro de sua proposta. Você pode explicar isso em uma linguagem normal e tem uma noção aproximada de como ele vai funcionar em seu jogo. Agora é hora de começar a pensar na mecânica e como reprsentar seu sistema em jogo.
 
Um pequeno aviso: não se sinta obrigado a resolver todos os problemas mecânicos. Se seu sistema de magia pode ser facilmente descrito e claramente compreendido, talvez não seja necessário mais do que uma perícia ou duas para representá-lo. Tome cuidado ao tentar pular diretamente para a mecânica — certifique-se de que realmente haja um problema antes de você introduzir uma mecânica para resolvê-lo, assim o produto final será muito mais consistente.

Quando chega a hora de introduzir uma mecânica nova, há duas coisas que ela precisa fazer — ou ao menos levar em consideração — um **resultado** e uma **limitação**.

O resultado é obvio — você quer ser capaz de lançar bolas de fogo, então os efeitos tratam de como mirar, quão grande elas são, quanto dano podem causar e assim por diante. A limitação é menos atraente, porém mais importante: ela responde à pergunta do por que você faria qualquer coisa _além_ de lançar bolas de fogo.

O que é importante notar é que o efeito pode não existir em apenas uma parte das regras. Ele estará entremeado com outras partes das regras de formas evidentes se estiver buscando por elas, mas que são facilmente negligenciadas se não estiver pensando nisso.

Sistemas de magia tendem a ser construídos com limitações que servem como estrutura para os resultados. Isto é, haverá um amplo conjunto de regras que controlam quando e como a magia pode ser usada — quais magias são conhecidas, com que frequência podem ser conjuradas, quem pode conjurá-las, etc. — enquanto as regras para resultados —como explodir coisas com bolas de fogo — normalmente são partes menores no texto, limitadas àquele feitiço em particular e outros similares.

Essa pode parecer uma distinção problemática, mas se você está criando o seu próprio sistema de magia, então isto é algo que realmente precisa ser considerado. O segundo ajuste lhe fornece imenso poder e flexibilidade para suas próprias criações, pois permite escolher o eixo da mudança.

Como exemplo, considere o clássico sistema de memorização de magias para conjuração. O recipiente — e, por extensão, o grande limitador — controla quais e quantas magias o personagem pode conjurar, enquanto os resultados são cada uma das magias individualmente. Isso permite uma variedade enorme, pois as limitações podem ser modificadas sem alterar os resultados — talvez introduzindo outra classe de personagem que recebe as mesmas magias com custos diferentes — ou você pode alterar os resultados sem mexer nas limitações, adicionando ou removendo magias.

Alterar resultados não é grande coisa. Trocar uma magia é fácil de ser feito e é uma ótima maneira de fazer coisas legais. Alterar as limitações é muito mais impactante, repleto de consequências possivelmente inesperadas. É também onde mora o grande poder dos ajustes. Talvez o importante seja o seguinte: ao entender essa divisão, você entenderá como pode construir um sistema de magia completamente novo simplesmente ao alterar um dos lados, ao invés de ter de reconstruir tudo a partir do zero.

### Limitações

As limitações em geral tomam uma de duas formas — uso ou oportunidade. __Limitações de uso__ afetam _quem_ pode usar a magia, enquanto as __limitações de oportunidade__ lidam com _como e quando_ a magia pode ser usada. 

A primeira coisa a se considerar a respeito de um sistema de magia é quem pode usá-la. Na ficção, a resposta pode ser “qualquer um”, mas mesmo nesse caso provavelmente será necessário pensar em alguma forma de representar esse sistema. Deixando de lado as limitações dentro da ficção, a porta de entrada para o uso da magia pode tomar a forma de um ou mais dos seguintes:

- Uma nova perícia
- Um aspecto específico
- Uma façanha
- Custo de recarga
- Oportunidade
- Custo de recursos

----

#### Façanhas E Recarga

Note que tratamos façanhas e recarga como duas coisas separadas. Isso porque são elementos separados, e embora o sistema padrão estabeleça que o custo de uma façanha é uma recarga, isso é apenas um sistema. Se estiver construindo seu próprio sistema, terá a liberdade de tratar isso de outra forma.

Isso abre várias portas em termos do que é uma façanha. Na prática, uma façanha é uma regra por si só — ou uma exceção à regra — e embora sua quantidade e natureza sejam restritas pelo custo padrão da recarga, você pode se libertar dessa estrutura. Como exemplo, _Senhores da Tempestade_ e _Seis Vizires_ na prática trazem um pacote de façanhas e apenas cobram um custo de recarga pelo pacote, basicamente oferecendo um desconto pelo conjunto temático.

---

#### Novas Perícias

Embora a adição de uma nova perícia “mágica” pareça ser a opção mais barata para permitir o uso de magia, tenha em mente o custo de oportunidade — o personagem está desistindo de outra perícia para usufruir da magia, então há uma troca. É possível incorporar a magia em uma ou mais perícias existentes, mas ao fazer isso você provavelmente exigirá algum outro custo.

#### Aspectos Específicos

Exigir um aspecto é tanto potente quanto um pouco trivial. Obviamente, aspectos falam bastante sobre o personagem, mas a menos que o aspecto necessário seja particularmente chato — e por que você iria querer algo assim? —, então não é um custo tão elevado. 

Isso não quer dizer que não há sentido em exigir um aspecto. A magia muitas vezes possui componentes do cenário, então um aspecto pode reforçar elementos da ficção. Aspectos também podem ser usados para ilustrar uma escolha em um cenário com vários estilos de magia, especialmente se você espera que todos os personagens possuam magia de alguma forma. Nesse caso, aspectos têm menos a ver com custo e mais com a diferenciação, o que pode ser algo bem forte.

#### Novas Façanhas

Façanhas são portais óbvios e legais para qualquer sistema de magia, pois assim como aspectos diferentes podem servir de entrada para estilos diferentes de magia, façanhas diferentes podem surtir o mesmo efeito. Por exemplo, se estiver usando um sistema que exija pontos de magia, façanhas diferentes podem representar formas diferentes de gerar esses pontos.

Claro, como as façanhas também podem ser resultados de um sistema de magia, você tem em mãos a possibilidade de criar interações bastante sofisticadas. É perfeitamente possível construir um sistema de magia de níveis hierárquicos como uma árvore, usando nada mais do que façanhas que dependem umas das outras como pré-requisitos. Tais sistemas são divertidos, mas muitas vezes precisam de uma elaboração complexa para que funcionem bem.

#### Recarga

Recarga é provavelmente o custo mais sério de todos e traz consigo muitos sentidos possíveis — mais especificamente, o que a diminuição de recarga significa?

Quando introduzimos a ideia de recarga em _Dresden Files RPG_, ela servia a dois propósitos : concedia aos personagens uma grande variedades de poderes em troca de liberdade, mas também destacou que a linha entre homem e monstro é tênue e que era possível ser consumido por seu poder — isto é, reduzir sua recarga a zero. Embora não haja a obrigação de fazer com que a recarga zero signifique a mesma coisa, a chave é que a recarga zero representa a perda de poder de atuação. Mecanicamente, significa que o personagem está incapaz de fazer qualquer coisa além do que seus aspectos estabelecem.

Os diversos sentidos da perda de poder de atuação são parte implícita de todos os sistemas de magia. Reduzir a recarga a zero pode certamente significar a morte de um personagem, mas geralmente é mais interessante significar que ele é privado de sua natureza mágica; personagens assim se tornam grandes vilões ou obstáculos mais à frente.

O problema é que embora a recarga como custo devesse _parecer_ um preço perigoso, isso não é verdade. Permitir que a recarga caia para zero é basicamente uma escolha do jogador de aposentar o personagem, algo que não acontece em jogo. Se estiver tudo bem para você, legal, mas se deseja que isso tenha um risco em jogo, então terá de encontrar uma forma de fazer essa escolha aparecer com mais frequência.

---

Em um sistema em que cada aspecto mágico reduz a recarga em um, mas aumenta os pontos de magia, você pode ditar que um personagem pode sempre escolher se desfazer de um de seus aspectos, convertendo-o em um aspecto mágico, aumentando assim sua reserva de mana. Além disso, assim que isso acontecer, a linha de estresse de mana do personagem é recuperada totalmente. Agora você tem uma razão para fazer com que isso aconteça em jogo, à medida que o personagem se entrega a seu poder para vencer um combate imbatível.


---

#### Oportunidade


Uma opção interessante para limitar os efeitos é exigir que não estejam disponíveis em todas as condições, mas fazer com que dependam de oportunidades apropriadas. Isso pode ser definido de várias formas, mas em geral se enquadra em duas categorias.

__Oportunidade no mundo__ são aquelas que dependem dos elementos do cenário de jogo. Talvez só seja possível conjurar magias em certos momentos ou lugares, ou em resposta a certos eventos. Isso é clássico em várias ficções de horror, onde coisas só podem ser chamadas “quando as estrelas estiverem alinhadas”.

Essas oportunidades tendem a funcionar mais como ganchos de história do que qualquer outra coisa, pois conduzem o jogo em direção a essas oportunidades. Se os mortos podem ser revividos no Colo de Shialla, então há um bom motivo para ir para lá. Se um Ancião Sombrio só pode ser invocado durante o eclipse lunar nas Planícies de Sangue, então a aventura praticamente se escreve sozinha.

No entanto, oportunidades no mundo são bastantes restritivas para os jogadores — são um custo alto para se fazer magia e, se for a única maneira de fazer tais coisas, então provavelmente não precisará de nenhum outro. Não quer dizer que esse meio seja inviável para um jogo — pode funcionar muito bem com personagens de nível alto em Conhecimento —, mas traz restrições bastante específicas.

Alternativamente, isso pode ser usado em conjunto com outro sistema de magia para superar as limitações comuns — dessa forma, magias que são mais poderosas ou impossíveis de se usar de outra forma, podem ser conjuradas nas circunstâncias adequadas. Estruturalmente, isso ainda não passa de usar a magia como auxílio ao enredo, e isso não é um problema.

**Oportunidades de jogo** são uma abordagem totalmente diferente. Nesse caso, algo envolvendo as regras do jogo deve acontecer para tornar a magia possível. Pode ser intencional, como gastar uma ação para invocar um poder, ou incidental, como uma habilidade que transforma um impulso em algo único.

Oportunidades intencionais tendem a funcionar como lombadas para magia e por isso bastante populares como mecânica de equilíbrio. A ideia é que se a magia pode ser usada com menos frequência, é aceitável que seja mais poderosa. Infelizmente, essa abordagem possui algumas fraquezas ocultas. Frequência é algo difícil de se equilibrar por ser tudo ou nada — tende a significar que o jogador do conjurador ou estará entediado, fazendo nada além de acumulando poder, ou estará mais poderoso que todos. Se você procura por algo assim, tente encontrar uma forma de tornar a parte não mágica de uma conjuração divertida e envolvente. Um truque é permitir que a “carga” possa ser acumulada ao longo de momentos de ação interessante.

Como alternativa, a magia pode ser uma expansão de regras existentes, trazendo mais cor ao jogo. Considere usar a magia para expandir resultados convencionais. Se você ataca com o Kung Fu da Terra, então um impulso poderia lhe fornecer um aspecto, mas também poderia fazer o seu adversário recuar uma zona. Isso pode parecer um pouco avesso às expectativas, se um jogador pensar em termos de “quero fazer ele recuar”, mas faz muito mais sentido quando pensa em termos de expansão das capacidades normais.

Há também a questão da **oportunidade de acesso** , que existe mais no âmbito da criação do personagem. A magia pode exigir uma perícia ou um aspecto mágico apropriado, como discutido anteriormente.

#### Recursos


Recursos envolvem um custo literal na conjuração da magia. Os clássicos podem incluir mana, pontos de magia ou magias descartáveis, mas você não está limitado a apenas isso. Uma magia pode exigir a invocação de um aspecto — e conjurar a magia em vez de receber o bônus de +2 —, receber estresse ou até mesmo consequências. Obviamente, o preço terá impacto na
frequência de uso da magia.

Recursos também podem adicionar um sabor extra ao sistema de magia — talvez eles nem sempre sejam necessários, mas podem ser usados para tornar a magia mais potente. Coisas como estresse e consequências podem ser bons para isso, representando magias tão perigosas quanto poderosas. Uma ressalva, no entanto — tente não ligar isso a magia explicitamente de combate. Quando isso se torna uma questão de matemática do tipo “eu posso receber X estresse para causar Y estresse no oponente”, a ideia começa a soar menos mágica.

Recursos e oportunidades podem se sobrepor um pouco nas perícias do jogador. Uma rolagem secundária de perícia para gerar mana pode exigir tanto recursos (mana) quanto uma oportunidade (a segunda rolagem de perícia). Isso pode parecer vago, mas é algo bom — interligar diversos elementos do personagem é uma coisa boa.

Um outro recurso óbvio são os pontos de destino e é razoável que sejam usados como combustível de magia da mesma forma que fazem com a invocação de aspectos. Você pode até mesmo fazer com que “mana” seja uma subcategoria de pontos de destino.

### Efeitos

Falamos sobre todas as coisas que podem controlar o uso da magia, mas o que a magia pode fazer quando usada? Em termos de ficção e cores, o leque de possibilidades é amplo, mas no momento estamos procurando as ferramentas mecânicas disponíveis para expressar os efeitos mecânicos. Embora seja possível criar mecânicas inteiramente novas para esse tipo de coisa, é melhor começar com os elementos que já temos. Assim como nas limitações, há uma lista básica de elementos com os quais podemos trabalhar:

- Fiat
- Aspectos
- Perícias
- Façanhas
- Estresse e Consequências
- Extras

#### Fiat (decreto)

Isto é uma palavra em latim que basicamente significa “você descreve algo e isso acontece”. Há diversas áreas de magia que podem ser tratadas dessa forma. Em alguns casos, pode ser que apenas isto seja necessário, especialmente se todos na mesa concordarem, mas é uma fonte de desconforto em potencial quando as expectativas diferem. Ainda assim, há diversos efeitos que são melhores de lidar desta maneira, mesmo que sejam mecanicamente importantes — o efeito mágico pode ser uma porta de entrada para outras regras.

Por exemplo, se a magia permite ao personagem respirar debaixo d’água, isso é um efeito de decreto — simplesmente é verdade. Não há necessidade de atribuir um aspecto ou uma perícia para representar isso, a não ser algum outro elemento da magia entre em jogo — como a inabilidade de respirar fora d’água. Seria possível atribuir um aspecto que nunca é invocado ou forçado, mas o efeito é praticamente o mesmo. Dito isso, uma vez que o personagem esteja embaixo d’água, ainda há outros elementos mecânicos a considerar — perícias físicas, por exemplo.

Magia com efeitos mecânicos triviais decretados podem ser absurdamente potentes ou importantes em um cenário. Considere algo como a juventude eterna – não há muitas mecânicas envolvidas, mas é um efeito que pode conduzir toda uma campanha. O fato dos efeitos decretados não exigirem que você pense muito na mecânica deveria significar que é necessário pensar mais nos elementos não mecânicos.

#### **Aspectos**

Os aspectos podem ser bem parecidos com os efeitos decretados, mas com um pouco mais de peso. Se tudo o que os aspectos fazem é reforçar a “oficialidade” de um decreto, tudo bem, mas você também pode considerar efeitos mecânicos a partir deles.

Muitos desses efeitos podem ser tratados com as regras normais para invocar e forçar. A magia apenas permite uma maior flexibilidade em termos de lógica e aparência do aspecto usado, e embora isso possa não ser tão óbvio no papel, pode ser algo de uma força incrível na mesa.

Dito isto, é perfeitamente razoável usar a magia para adicionar alguns efeitos ao invocar e ao forçar aspectos. Esses efeitos adicionados podem ser decretados ou possuir algum efeito mecânico. Um aspecto como **_Passo das Sombras_** pode ser invocado para atravessar várias zonas de uma vez. Um aspecto como **_Armadura de Luz_** pode ser invocado — e consumido — para evitar uma consequência.

Uma coisa a ser considerada é se o efeito cria um novo aspecto ou não, ou se adiciona um novo efeito a um aspecto existente. Às vezes a lógica do efeito deixará a resposta óbvia, mas em caso de dúvida tente expandir o alcance do aspecto. Ao fazer isso você não apenas evita o excesso de aspectos, como também incentiva a interação com aspectos já existentes.

#### Perícias

Magia pode ser usadas em lugar de perícias e, na verdade, um dos sistemas de magia mais simples e comum é usar uma perícia mágica para substituir outra perícia, como lançar rajadas de energia como equivalente a uma perícia com armas. Isso é suficientemente bom, mas só serve de verdade como uma ferramenta de uso geral.

Mais interessante, a magia pode ser usada para ampliar o alcance de perícias existentes, levando-as a um nível sobrenatural. Isso pode ser uma ampliação de capacidade, como sua percepção se estendendo para visão infravermelha ou planos espirituais, ou podem ser ganchos mecânicos como “O Martelo do Terror”, alterando a perícia com armas para que, ao conseguir um impulso, você também inflija dano mental.

#### Façanhas

A magia pode adicionar novas façanhas ou alterar as que já existem, mas o que você encontrará com maior frequência é a adição de façanhas mágicas — façanhas com efeitos mágicos que, em geral, são mais potentes que as façanhas normais, mas que vêm acompanhadas de custos e requisitos. 

O exemplo mais óbvio para isso é uma façanha que permite a você fazer algo mágico, como teleporte na linha de visão ou se transformar num rato, mas a um custo. O custo pode ser tão simples quanto gastar pontos de mana ou pode ser algo complexo e ritualístico. Como façanhas possuem suas próprias regras, os componentes de custo e equilíbrio são mais claros do que outras abordagens, então é importante pensar sobre eles desde o começo.

#### Estresse e Consequências

Um efeito fácil e óbvio de magia é mexer com estresse e consequências. Magias de cura podem recuperá-los ou magias de proteção podem melhorar o potencial de resistência. Apenas tenha cuidado para que não deixar os magos monstruosamente imbatíveis — a não ser que essa seja sua intenção.

#### **Extras**

Magia é uma ótima forma de ter permissão para criar extras. Seja invocando de um cão de fogo ou invocar relâmpagos que crepitam ao longo da lâmina de sua espada, extras podem ser uma maneira razoável de lidar com isso.

Isso é um ótimo exemplo de como criar “extensões” em um sistema de magia. As regras de extras são robustas, mas também bem abertas. Se todo o sistema de magia se tratar apenas da criação de extras, eles rapidamente sairiam do controle, logo é importante impor algumas limitações. As limitações devem ser consistentes o bastante para dar a sensação de um sistema de magia coerente. Se você se certificar de que cada extra novo faça sentido no contexto dos que já existem, perceberá que a lógica do seu sistema de magia surgirá de forma bem orgânica.

------

- [« Suas Ferramentas](../suas-ferramentas/)
- [Subsistemas »](../subsistemas/)
