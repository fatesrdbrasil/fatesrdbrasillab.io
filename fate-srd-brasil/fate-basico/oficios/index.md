---
title: Ofícios
layout: default
---

### Ofícios

Ofícios é a perícia para trabalhar com máquinas e aparelhos, para o bem ou para o mal.

A perícia é chamada Ofícios porque é o que usamos nos exemplos, mas ela pode varias muito dependendo do cenário e da tecnologia disponível. Em um cenário moderno ou de ficção-científica, pode ser que chamar de Engenharia ou Mecânica seja melhor, por exemplo.

- `O`{: .fate_font} **Superar:** Ofícios é usado para construir, quebrar ou consertar equipamentos, desde que você tenha tempo e as ferramentas necessárias. Muitas vezes, ações usando a perícia Ofícios acontecem como parte de uma situação mais complexa, tornando-a uma perícia popular para desafios. Por exemplo, se estiver tentando consertar uma porta quebrada, falhar ou conseguir não muda muita coisa; é melhor considerar como sucesso e seguir em frente. Agora, se estiver tentando consertar o carro enquanto um grupo de lobisomens está no seu encalço...
- `C`{: .fate_font} **Criar Vantagem:** Você pode usar Ofícios para criar aspectos que representem características úteis de uma máquina, colocando nela pontos fortes que você pode usar a seu favor (***Blindada***, ***Construção Robusta***) ou uma vulnerabilidade que possa explorar (***Falha no Sinal***, ***Feito às Pressas***).
Criar vantagens usando Ofícios pode ser também uma forma de sabotar e interferir em objetos mecânicos que estejam presentes na cena. Por exemplo, você pode criar uma ***Polia Improvisada*** para ajudá-lo a chegar na plataforma superior ou jogar algo na balista que está atirando em você para que ela tenha os ***Mecanismos Emperrados*** e atrapalhe seu funcionamento.
- `A`{: .fate_font} **Ataque:** Você provavelmente não vai usar Ofícios para atacar em um conflito, a menos que o conﬂito seja especificamente sobre usar máquinas, como em um cerco armado. Narradores e jogadores devem conversar sobre essa possibilidade se alguém estiver realmente interessado em usar essa perícia. Normalmente, armas construídas provavelmente precisarão de outras perícias na hora de atacar – quem fabrica uma espada ainda precisa da perícia Lutar para usá-la!
- `D`{: .fate_font} **Defesa:** Como nos ataques, Ofícios não é usada para defender, a menos que você a use para controlar algum tipo de mecanismo de defesa.

#### Façanhas Para Ofícios

-  **Sempre Criando Coisas Úteis:** Você não precisa gastar um ponto de destino para declarar que possui as ferramentas apropriadas para um trabalho em particular usando Ofícios, mesmo em situações extremas (como quando está numa prisão e longe do seu equipamento). Esse tipo de oposição deixa de existir.
-  **Melhor Que Novo:** Sempre que obtiver um sucesso com estilo numa ação de superar para consertar um dispositivo, você pode criar imediatamente um novo aspecto de situação (com uma invocação grátis) que reﬂita as melhorias realizadas, ao invés de apenas um impulso.
-  **Precisão Cirúrgica:** Quando usa Ofícios em um conﬂito envolvendo algum tipo de mecanismo, você pode filtrar alvos indesejados em ataques de zona inteira sem ter que dividir o dano (normalmente é necessário dividir sua rolagem entre os alvos).

------

Se construir coisas e criar itens é uma parte importante do seu jogo, veja o capítulo [*Extras*](../extras/) para uma discussão sobre mais usos da perícia Ofícios.

------

------

### Tantos Ofícios...

Se for importante para o seu jogo lidar com *tipos diferentes de tecnologia*, é possível ter vários tipos desta perícia na sua lista. Um jogo futurista, por exemplo, pode ter Engenharia, Cibernética e Biotecnologia, todas com as mesmas ações disponíveis para o seu tipo de tecnologia. Em tais jogos, nenhum personagem pode ter conhecimento em todos os campos sem adquirir diversas perícias individuais.

Se pretende fazer isso, certifique-se de ter um bom motivo – se a *única* coisa que essa divisão causar for nomes diferentes para os mesmos efeitos, você deve mantê-las em uma só, de forma genérica, e usar façanhas para representar as especialidades.

------

- [« Lutar](../lutar/)
- [Percepção »](../percepcao/)
