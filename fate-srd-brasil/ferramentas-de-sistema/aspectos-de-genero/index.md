---
title: Aspectos de Gênero
layout: default
---

## Aspectos de Gênero

Há momentos em que você pode querer reforçar algo sobre o gênero do jogo. Criar questões no jogo que ressaltem os temas do gênero é uma boa maneira, e você poderá ter uma boa diversidade de material para encorajar os seus jogadores a criar personagens ligados ao gênero. No entanto, às vezes você quer algo mecanicamente único.

Ao usar aspectos de gênero, você está alterando o funcionamento dos aspectos a fim de reforçar o gênero escolhido. Às vezes essas mudanças serão aplicadas a todos os aspectos do jogo, enquanto outras vezes essas mudanças serão restritas a um grupo específico de aspectos ou um único aspecto. As maneiras pelas quais você pode alterar os aspectos são variadas demais para serem listadas aqui, então optamos por providenciar alguns exemplos.

### Aspectos Menos Poderosos

Esta é uma mudança radical de todos os aspectos do jogo, e é uma ótima opção para emular gêneros onde os PJs são mais fracos que as forças que eles enfrentam, como horror ou noir. As mudanças são simples: ao invocar um aspecto, você pode rolar novamente os dados ou pode receber +2 em sua rolagem, mas não pode escolher +2 mais de uma vez caso invoque mais de um aspecto. Isso significa que um aspecto pode apenas melhorar um pouco o esforço máximo de um PJ. Isso fará com que a falha seja mais comum, o que indica que a falha precisa sempre ser interessante e precisa mover a ação adiante, assim como um sucesso. 

Se quiser permitir que PJs recebam bônus múltiplos de aspectos em certas situações, considere ligar isso a façanhas, como:

**Sentidos Aguçados:** Ao invocar um aspecto em uma rolagem de Notar, você pode invocar quantos aspectos desejar, contanto que tenha pontos de destino suficientes para pagar o custo.

### Aspectos de Missões

Esta é uma mudança pequena, mas uma que pode reforçar os objetivos compartilhados dos PJs. Funciona bem em cenários de fantasia ou em qualquer outro cenário onde os PJs ajam em grupo para alcançar algum tipo de objetivo maior, como matar o dragão ou recuperar o vilarejo tomado por bandidos. 

Sempre que os PJs aceitarem uma missão, o grupo trabalhará em conjunto para criar um bom aspecto que a represente. Por exemplo, se os PJs estão tentando salvar a vila de um rei tirano, o grupo poderia criar o aspecto de missão **_Martim Coração-Partido Deve Ser Detido!_**. Este aspecto pode ser invocado por qualquer PJ do grupo, assim como pode ser forçado como se estivesse na planilha de personagem, concedendo um ponto de destino a todos. 

Se os PJs resolverem o aspecto da missão, terão alcançado um marco. A importância do marco depende da dificuldade e da duração para alcançar o objetivo. Veja o Fate Sistema Básico para [mais informações sobre marcos](../../fate-basico/evolucao-e-alteracao/#o-que-é-um-marco).

------

- [« Aspectos](../aspectos/)
- [Aspectos de Equipamento »](../aspectos-de-equipamento/)
