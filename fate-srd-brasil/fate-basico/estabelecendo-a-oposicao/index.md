---
title: Estabelecendo a Oposição
layout: default
---

## Estabelecendo a Oposição

Você pode já ter pensado em alguns PdNs ou um grupo deles que são responsáveis pelo que está havendo, mas se não tiver, é preciso começar a montar o elenco de personagens-chave para responder suas perguntas narrativas. Também é preciso acertar suas motivações e objetivos, por que se opõem aos objetivos dos PJs e o que procuram.

No mínimo, você deve responder a algumas perguntas para cada PdN importante em seu cenário:

-  O que esse PdN *precisa*? Como os PJs o ajudam a conseguir isso ou atrapalham seus objetivos?
-  Por que o PdN não pode conseguir o que precisa de forma não problemática? (Em outras palavras, porque isso precisa ser um problema?)
-  Por que ele não pode ser ignorado?

Sempre que puder, tente unificar os PdNs, pois assim não terá muitos personagens para acompanhar. Se um de seus PdNs serve a apenas um propósito em seu cenário, considere se livrar dele e unir o seu papel ao de outro PdN. Isso não apenas reduz sua carga de trabalho, mas também permite que desenvolva um pouco mais a personalidade de cada PdN, tornando-o mais vívido em suas motivações.

Para cada PdN que possuir, veja se precisa transformá-lo em um PdN principal ou de suporte. Estruture-o de acordo com as dicas dadas em [*Estruturando o Jogo*](../estruturando-o-jogo/).

> **Uma Conspiração Galáctica: Oposição**
> 
> Amanda observa as perguntas narrativas e pensa nos PdNs que precisa para respondê-las. Ela cria um lista de suspeitos:
> 
> -  O empregador misterioso de Fräkeline (não aparece)
> -  O curador-chefe do Museu (suporte)
> -  O rival de Fräk pelo Cetro (suporte)
> -  Uma advogada que não faz parte da conspiração (suporte)
> -  Um advogado corrupto associado ao Império (suporte)
> -  O chefe da guarda, secretamente filiado ao Império, que elaborou a conspiração para capturar Bandu (principal)
> 
> São seis PdNs, quatro de suporte, um principal e um que não estará presente no cenário – Amanda ainda não quer criar os detalhes de quem contratou Fräk. Ela também não quer ter que controlar cinco PdNs, então estuda as possibilidades de fusão.
> 
> Uma possibilidade que lhe vem à mente é transformar o concorrente de Fräkeline e a advogada neutra no mesmo personagem, que ela chama de Anya. Anya pode não estar envolvida *nessa* conspiração, mas ela certamente tem motivos mais profundos. O que está acontecendo então? Amanda decide que a motivação de Anya é benéfica; ela deseja pegar o Cetro de Coronilla para mantê-lo longe das mãos corruptas do Império Mareesiano. Ela não sabe nada sobre Fräk e irá confundi-la com uma agente imperial até que as coisas sejam esclarecidas.
> 
> Então ela decide que o chefe da guarda do Museu e o arquiteto da conspiração são a mesma pessoa – ele não confia em ninguém que não seja do Império, então faz o possível para conseguir capturar seus inimigos. Amanda gosta dessa ideia porque seu poder político faz dele um oponente formidável e lhe dá um lacaio, um advogado corrupto. Mas qual é sua motivação para arranjar problemas com Bandu?
> 
> Ela decide, ainda, que seu motivo não é pessoal, mas ele está começando algumas atividades que terão efeito na galáxia, e sabe que entregar Bandu ao Império lhe trará bastante poder e prestígio, acelerando seus planos.
> 
> Quanto ao advogado corrupto, a primeira coisa que vem à mente é um bajulador patético, chorão, que está na palma da mão de seu chefe. Mas ela quer adicionar mais alguns detalhes para dar profundidade, então decide que o chefe da guarda tem algum material para chantageá-lo, o que ajuda a assegurar sua lealdade. Ela ainda não sabe qual é a informação, mas espera que PJs curiosos a ajudarão a criar os detalhes mais tarde.
> 
> Ela nomeia o chefe de Stamatios Wolfram e o advogado corrupto de Festus. Ela agora tem os PdNs que precisava e criará suas fichas.

------

### Vantagens Podem Salvar Seu Trabalho

Ao elaborar os PdNs para o cenário, você não precisa ter nada permanentemente definido quando chegar à mesa – se houver algo que ainda não sabe, sempre será possível estabelecer esses fatos criando Aspectos para os PdNs com ideias vindas das vantagens criadas pelos jogadores. Veja mais conselhos sobre como improvisar esse tipo de coisa durante o jogo [a seguir](../organizando-a-primeira-cena/#truque-do-narrador-ninja-para-o-início-da-sessão).

------

- [« Perguntas Narrativas](../perguntas-narrativas/)
- [Organizando a Primeira Cena »](../organizando-a-primeira-cena/)

