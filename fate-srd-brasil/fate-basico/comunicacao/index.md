---
title: Comunicação
layout: default
---

### Comunicação

A perícia Comunicação é a responsável por fazer conexões positivas com pessoas e incitar emoções positivas. É a perícia para que os outros gostem e confiem em você.

- `O`{: .fate_font} **Superar:** Use Comunicação para cativar ou inspirar pessoas a fazer o que você quer ou estabelecer uma boa conexão com elas. Cative o guarda para que ele o deixe passar, ganhe a confiança de alguém ou torne-se o centro das atenções na taverna local. Fazer isso com PdNs menos importantes é apenas uma questão de realizar uma ação de superar, mas você pode precisar entrar em uma disputa para conquistar a confiança um PdN mais importante ou PJ.
- `C`{: .fate_font} **Criar Vantagem:** Use Comunicação para estabelecer um humor positivo em um alvo, cena ou para fazer com que alguém passe a confiar legitimamente em você. Você pode levar alguém na conversa para que ela tenha ***Confiança Elevada*** ou animar uma multidão a ponto de deixá-la ***Eufórica de Alegria***, ou simplesmente deixar alguém ***Falante*** ou ***Colaborativo***.
- `A`{: .fate_font} **Ataque:** Comunicação não é usada para causar dano, logo esta perícia não é utilizada para realizar ataques.
- `D`{: .fate_font} **Defesa:** Comunicação pode ser usada como defesa contra qualquer perícia usada com a intenção de ferir sua reputação, azedar um clima bom que tenha criado ou fazê-lo ficar mal na frente dos outros. A perícia não serve, no entanto, como defesa contra-ataques mentais. Isso exige a perícia Vontade.

#### Façanhas para Comunicação

- **Boa Impressão:** Você pode, duas vezes por sessão, elevar um impulso obtido com Comunicação a um aspecto de situação com uma invocação grátis.
- **Demagogo:** +2 em Comunicação quando fizer um discurso inspirador em frente a uma multidão (se houverem PdNs importantes ou PJs na cena, é possível atingir a todos simultaneamente com uma rolagem ao invés de dividir as tensões).
- **Popular:** Se estiver em uma região onde você seja popular e bem-visto, é possível usar Comunicação no lugar de Contatos. Você pode estabelecer sua popularidade gastando um ponto de destino para declarar os detalhes da história ou devido a uma explicação prévia.

- [« Atirar](../atirar/)
- [Condução »](../conducao/)
