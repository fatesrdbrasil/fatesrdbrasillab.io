---
title: Esferas de Perícias
layout: default
---

## Esferas de Perícias

__Esferas de perícias__ são conjuntos de perícias que representam uma determinada área de competência. Esferas são ótimas para agilizar a criação de personagens, tornando-a ainda mais rápida que no Fate Sistema Básico. Em lugar de escolher as 10 perícias individuais, os jogadores distribuirão níveis por três esferas diferentes e as perícias de cada esfera recebem o valor respectivo.

Dê uma olhada nas perícias que seu jogo possui e pense em arquétipos que gostaria de ver em sua partida. Para cada um desses arquétipos, escolha cinco ou seis perícias relevantes. Cada um desses conjuntos será uma esfera. Não há problema se uma perícia aparecer em mais de uma esfera, mas se for comum a quase todas, esteja ciente de que os personagens poderão ficar um pouco homogêneos.

O ideal é que haja entre quatro e oito esferas de perícias — o suficiente para uma boa variedade, mas pouco o bastante para que a escolha seja rápida. 

---
Isa está planejando um jogo de fantasia clássica com exploração de masmorras, caça a tesouros e a ocasional matança de dragões. As perícias do jogo serão Atletismo, Roubo, Contatos, Enganar, Empatia, Intimidação, Lutar, Projétil, Vigor, Comunicação, Furtividade, Sobrevivência e Vontade. Com base nessa lista, ela cria quatro esferas.

| __Combate__ | __Social__  | __Ladinagem__ | __Silvestre__ |
|-------------|-------------|---------------|---------------|
| Atletismo   | Contatos    | Atletismo     | Atletismo     |
| Intimidação | Enganar     | Roubo         | Conhecimento  |
| Lutar       | Empatia     | Enganar       | Notar         |
| Projétil    | Comunicação | Notar         | Furtividade   |
| Vigor       | Vontade     | Furtividade   | Sobrevivência |

Isa percebe que Atletismo aparece em três dos quatro grupos, mas é uma perícia tão amplamente aplicável que não consegue imaginá-la faltando em qualquer um desses grupos. Além disso, os PJs exploradores de tumbas precisarão dela.


### Classificações


A pirâmide de perícias padrão, apresentada no Fate Sistema Básico, não se aplica a essas esferas de perícias. Ao invés disso, cada jogador escolhe três esferas e as classifica — uma como Bom (+3), uma como Razoável (+2) e outra como Regular (+1) — e o nível de cada esfera se torna o nível básico de todas as perícias pertencentes à esfera. Perícias nesse nível são chamadas de __treinadas__. Perícias um nível acima das da esfera são chamadas de __focadas__ e perícias dois níveis acima das da esfera são chamadas __especializadas__.

---

Um dos jogadores na mesa de Isa, William, quer jogar com um personagem ladino, então ele escolhe Ladinagem como
Bom (+3). Isso significa que Atletismo, Roubo, Enganar, Notar e Furtividade começarão treinadas, com um nível de Bom (+3).

---

### Perícias Reforçadas

Além de sua Ladinagem em Bom (+3), Will também pegou Social Razoável (+2) e Combate Regular (+1). Tanto Combate quanto
Ladinagem possuem Atletismo, então ele reforça essa perícia de treinada para focada – de Bom (+3) para Ótimo (+4) e a coloca no grupo Ladinagem (onde essa perícia possuía o nível mais alto). Da mesma forma, Social e Ladinagem compartilham Enganar, então essa perícia passa para focada no grupo Ladinagem. Assim, os grupos de Will ficam desta forma:

|    | BOM (+3)  LADINAGEM           | RAZOÁVEL (+2) SOCIAL                    | REGULAR (+1) COMBATE                        |
|----|-------------------------------|-----------------------------------------|---------------------------------------------|
| +5 |                               |                                         |                                             |
| +4 | Atletismo,Enganar             |                                         |                                             |
| +3 | Roubo, Percepção, Furtividade |                                         |                                             |
| +2 |                               | Contatos, Empatia, Comunicação, Vontade |                                             |
| +1 |                               |                                         | Intimidação, Corpo a Corpo, Projétil, Vigor |

### Melhorando Perícias


Após isso, cada jogador recebe 7 pontos para melhorar suas perícias.

|                       Melhoria | Custo |
|-------------------------------:|-------|
|        De treinada para focada | 1     |
|   De focada para especializada | 2     |
| De treinada para especializada | 3     |

Nenhuma perícia pode ser melhorada acima de especializada.

---

Will deseja ser especialmente furtivo, então ele gasta três pontos para especializar Furtividade, de Bom (+3) para Excepcional (+5). Ele também quer ser eficiente em combate, então gasta mais 3 pontos para se tornar especialista em Lutar, indo de Médio (+1) para Bom (+3). Seu último ponto vai para Contatos, de Razoável (+2) para Bom (+3), para que tenha um pouco mais de conexões.

---


### Esferas com Extras


Algumas esferas de perícias fornecem acesso a __extras__ — ou por conter um extra de perícia , ou por permitir o uso do extra de um aspecto. Para cada extra associado a uma esfera, essa mesma esfera passa a ter uma perícia a menos. Por exemplo, se as esferas no seu jogo possuírem cinco perícias cada, uma esfera com um extra de perícia passa a ter apenas quatro. Extras de aspecto são considerados a mais além do número padrão de aspectos em seu jogo.

---

Isa adiciona quatro esferas de perícia à sua lista: Arcana e Deuses, para dois tipos de magia diferentes, e Anã e Élfica, para... anões e elfos. Os dois primeiros possuem dois extras cada — uma perícia e um aspecto, ambos relacionados a magia — e os outros dois grupos possuem apenas um aspecto extra.

| Arcana          | Deuses          | Anã             | Élfica          |
|:---------------:|:---------------:|:---------------:|:---------------:|
| Conhecimento    | Favor           | Atletismo       | Atletismo       |
| Magia           | Conhecimento    | Conhecimento    | Conhecimento    |
| Vontade         | Vontade         | Corpo a Corpo   | Projétil        |
| _Perícia Extra_ | _Perícia Extra_ | Vigor           | Percepção       |
| _Aspecto Extra_ | _Aspecto Extra_ | _Aspecto Extra_ | _Aspecto Extra_ |


------

- [« Outras Funções de Perícias](../outras-funcoes-de-pericias/)
- [Façanhas »](../facanhas/)
