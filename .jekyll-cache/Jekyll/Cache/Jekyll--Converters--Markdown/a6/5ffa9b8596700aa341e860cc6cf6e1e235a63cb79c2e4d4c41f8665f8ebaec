I"C
<ul>
  <li><em>Autor:</em> Fred Hicks</li>
  <li><em>Link Original:</em> <a href="http://www.deadlyfredly.com/2015/09/fae-q-approach-alts/">http://www.deadlyfredly.com/2015/09/fae-q-approach-alts/</a></li>
</ul>

<p>Lembrei-me recentemente dos meus comentários <a href="https://plus.google.com/116948490555602883045/posts/iCTQmXn9YNw">sobre esse <em>post</em> escrito por John Rogers</a> no Google+ ano passado e me peguei pensando sobre o assunto. Desde que nós publicamos o <em>Fate Acelerado</em>, ou FAE, eu percebi que esse jogo tem dividido opiniões. Algumas pessoas o amam de paixão, enquanto outros realmente estão de saco cheio do mesmo e alguns poucos estão confusos quanto a ele ser considerado <em>Fate Básico</em> ou algo à parte (como fica evidente nas ocasionais <em>threads</em> <em>“Fate Básico</em> versus <em>Fate Acelerado”</em> que eu vejo por aí, como se eles de fato fossem opostos).</p>

<p>Para deixar claro (de novo), o FAE <em>é</em> Fate Básico. Ele tem suas engrenagens ajustadas de uma maneira deliberadamente diferente dos padrões apresentados no Básico (que foram ajustado para a campanha exemplo dos <em>Caroneiros de Asteróides</em> que foram usados no exemplo do Básico). Essa direções deliberadamente diferentes foram todas focadas em velocidade e suporte para uma grande quantidade de personagens diversos, alguns quase idênticos e outros muito diferentes. Ela deixa de lado algumas das opções mais complexas do Básico em favor da simplicidade, e procura ter um pouco mais de linhas-mestras, como exemplificado na criação de Façanhas, sempre focando na velocidade e facilidade. Isso nos oferece um <em>chassis</em> bem simples que pode ser usado em uma grande variedade de produtos isolados baseados em Fate, já que estamos falando de em torno de 12 mil palavras, contra as quase 90 mil do Fate Básico. Apesar de todas essas intenções, não vejo como elas são um desvio tão grande do Básico (Não quer dizer que não hajam desvios, apenas que os mesmos são menores).</p>

<p>Acho que o ponto do FAE que realmente frita os neurônios das pessoas, na realidade, são as Abordagens.</p>

<p>Olhando bem por cima, elas são simplesmente uma maneira de oferecer uma lista ainda menor de perícias que os padrões que o Fate Básico encoraje, na casa de 2 a 3 vezes. Novamente, uma escolha deliberada com o objetivo de reduzir o tamanho do texto. Eu <em>amo</em> listas curtas de perícias, pois acho fácil de trabalhar com elas, mais facilmente verificáveis, e elas se aproximam mais do que comumente chamamos de “atributos” na maioria dos jogos.</p>

:ET