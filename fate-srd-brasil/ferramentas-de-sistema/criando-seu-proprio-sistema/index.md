---
title: Criando Seu Próprio Sistema
layout: default
---


## Criando Seu Próprio Sistema


Agora que viu uma boa variedade de exemplos, aqui está a sua oportunidade deixar o jogo do seu jeito. Isso deveria ser um processo simples, mas nunca é bem assim – é aquele tipo de coisa que parece ser complicado no começo, mas vai se tornando cada vez mais fácil à medida que for fazendo, até chegar ao ponto que se torna tão instintivo que você não consegue compreender como isso pode ter sido difícil um dia. Onde quer que esteja nessa sequência, esperamos que possamos ajudar com estas ferramentas.

### Equilíbrio

Primeiro, deixe de lado suas noções de equilíbrio. É um conceito importante, mas não da forma como normalmente é colocado. Equilíbrio não existe no que é abstrato — é um elemento específico de jogo e sempre deve ser visto através da visão de jogo. Nada está desequilibrado por si só, o contexto que é responsável por isso. Um poder que torna um per- sonagem um deus onipotente pode parecer desequilibrado, mas quando todos os personagens o possuem, o jogo se torna mais interessante. Tudo depende do contexto.

Sendo assim, como podemos equilibrar os poderes? Pense em três coisas — equilíbrio no grupo, no cenário e no jogo.

#### Equilíbrio no Grupo

Quando desenvolver um sistema de poder, você precisa fazer uma das seguintes suposições:

- Apenas alguns personagens poderão usar.
- Todo personagem poderá usar.

Se estiver desenvolvendo um tipo de poder que apenas alguns personagens poderão usar, então precisa pensar sobre como o poder se compara a outras coisas que os personagens podem fazer, e o que os personagens estão dando em troca desse poder. Simplificando, isso significa que você precisa de uma resposta satisfatória para a pergunta “porque eu não escolheria este poder?”

O sistema dos Senhores das Tempestades foi desenvolvido com isso em mente. O motivo para não se tornar um Senhor da Tempestade é porque você precisa sacrificar um espaço de perícia — e um aspecto — para se tornar um, e como a maior parte de suas magias são aplicáveis a combate, torna-se uma troca bem equilibrada. Mesmo isso assume que haverá coisas como regras de armas e armaduras no jogo. Sem tais regras, a perícia Tempestades se torna apenas uma perícia superior de combate e não há razão para não selecioná-la.

Mas note que o truque para o equilíbrio vem de outra parte do jogo em si. Isso ilustra algo importante sobre o equilíbrio em grupo — objetivo é certificar que os jogadores permaneçam ativos e engajados. Ao tornar uma certa parte do jogo mais legal que as outras, é esperado que os jogadores se concentrem nisso, então ou isso deve ser apoiado, ou será necessário criar outras formas de tornar os personagens interessantes.

Se você, por outro lado, assumir que todos os jogadores terão poderes, o céu é o limite. Você diminuiu o risco dos jogadores ofuscarem uns aos outros. O sistema dos Seis Vizires foi desenvolvido dessa forma e ilustra bem os pontos fortes e perigos desta abordagem. As habilidades desse sistema são incrivelmente fortes e poderiam distorcer o foco caso apenas um dos personagem as possuam, mas desde que todos tenham potência semelhante, isso deixa de ser um perigo. No entanto, como os poderes são potentes e diversificados, deve-se tomar cuidado para quem nenhum deles domine o jogo. 

Nada faz com que uma abordagem seja melhor que outra — a lógica do seu sistema de poder revelará se ele deve ser para alguns personagens ou para todos. Entretanto, essa distinção precisa estar clara quando for desenvolver o sistema. Um sistema que tente fazer ambos está implorando por resultados abusivos e inconsistentes.

#### Equilíbrio no Cenário

Equilíbrio no cenário pode parecer uma ideia estranha, mas é fundamental para a boa elaboração de poderes, pois criação de poderes e criação de cenários são a mesma coisa. Suas regras de poder determinam como seu mundo funciona e é preciso pensar dessa forma. Perguntas que você deve considerar incluem:

- Quem pode usar este sistema de poder?
- Quantos usuários existem?
- Quão potentes/capazes são?
- Qual o impacto do poder nas pessoas que o possuem?
- Quais são os resultados mais comuns do poder no cenário?
- Quais são os resultados em grande escala de tais poderes dentro do  cenário?

Obviamente, quanto mais restritivo for o poder, menos você precisará se preocupar com essas coisas, mas você corre o risco do poder ser um apenas tom no cenário em lugar de fazer parte dele. Como bônus, quanto mais pensar sobre as ramificações lógicas do poder, melhor será para equilibrá-lo.

Os _Conjuradores do Vazio_ são uma boa ilustração de um sistema de magia equilibrado com o cenário. Note que há muito poucas questões mecânicas no uso da magia nesse sistema — quase tudo são elementos do cenário, tanto em termos de comportamento dos praticantes como no impacto dos poderes. Qualquer um pode usar o poder, então os praticantes criam obstáculos para impedir que outros o usem e, ao fazer isso, escondem informações sobre quantos são e sua própria habilidade. O impacto do poder é terrível, mas —por enquanto— são controlados por sorte ou boas intenções. Se isso for mudado, o poder é mudado.

**Dica:** Quer ousar numa ideia de sistema? Olhe as respostas para o equilíbrio do cenário, mude uma delas e veja o que acontece. Por exemplo, não há resultados em grande escala no sistema padrão dos Conjuradores do Vazio, mas e se isso for mudado? E se algo comeu Manhattan? Algo tão grande e terrível que não poderia ser escondido? O que mudaria?

#### Equilíbrio no Jogo

Em grande parte isto se trata de uma extensão do equilíbrio no grupo, mas depende de como os poderes influenciam jogo. Alguns jogos com poderes são sobre os poderes em questão, tais como os jogos de superpoderes ou jogos sobre magos como o tradicional _Ars Magica_. Outros jogos, como os clássicos de aventura ou jogos de horror, simplesmente incluem poderes no escopo geral do jogo. Descubra o que o seu sistema faz e ajuste os poderes apropriadamente.

De forma prática, em um nível mundano, preste atenção no funcionamento do seu sistema na mesa de jogo. Se a mecânica exigir mais de sua atenção — porque requer mais rolagens, por exemplo — então existe uma boa chance de estar chamando mais atenção do que os jogadores que não possuem poderes. Isso pode ser administrado pelo Narrador de forma equilibrada, mas seria melhor se o problema não existisse de início.

------

- [« Senhores do Vazio](../senhores-do-vazio/)
- [Suas Ferramentas »](../suas-ferramentas/)
