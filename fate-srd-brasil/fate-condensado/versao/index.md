---
title: "Fate Condensado - Que versão é essa?"
layout: default
---
# Que versão é essa?

Desde o final de 2012, existe apenas uma versão do Fate da Evil Hat: ***Fate Básico***, a quarta edição do sistema.

Ah, mas e o ***Fate Acelerado***, pode perguntar o atento fã de Fate? O fato é que também é Fate Core, é o mesmo sistema apenas com ***diferentes ajustes*** (ou seja, opções de configuração) para trilhas de estresse, lista de perícias, façanhas e design de PdN. Quaisquer diferenças aparentes nas *funções essenciais* do sistema são devidas a acidentes de desenvolvimento paralelo e podem ser consideradas não intencionais - com um pedido sincero de desculpas para advogados de regras que existam por aí\! Se houver um conflito entre os designs de regras, ***Fate Básico*** é a autoridade.

Essas duas perspectivas do ***Fate Básico*** se unem aqui no ***Fate Condensado***, bem literalmente, de fato. Condensado começou como um texto Acelerado, menos todos os ajustes do Acelerado, substituídos pela versão do Básico. A partir desse ponto de partida, aplicamos 8 anos de experiência de jogos da comunidade para refinar e esclarecer. Esse esforço produziu algumas pequenas diferenças como notado na [lista no ínicio desse documento](../introducao/#para-veteranos-mudanças-do-fate-básico), mas quer você escolha jogar com ou sem elas, o sistema ainda é ***Fate Básico*** no final do dia. Nós sentimos que o Condensado é uma melhoria com certeza, mas não deve existir uma guerra entre edições aqui (e por favor não inicie uma). É tudo ***Fate Básico***.

## O que veio antes

Fate começou como uma modificação do sistema Fudge por volta dos anos 2000, culminada por algumas conversas acaloradas entre Fred Hicks e Rod Donoghue sobre como eles poderiam fazer para rodar outra partida de Amber. As versões que surgiram entre essa época e 2005 foram gratuitas, digitais e liberadas para a comunidade online do Fudge com uma recepção surpreendentemente entusiasmada. Elas foram de “Fate Zero” para Fate 2.0.

Então, Jim Butcher ofereceu a Fred e Rob uma chance de criar um RPG baseado em Dresden Files, desencadeando o estabelecimento da Evil Hat como uma empresa e uma nova tomada no sistema Fate, visto primeiramente no ***Espírito do Século*** (2006) e eventualmente no ***The Dresden Files RPG*** (2010). A versão de Fate encontrada em ambos (e graças ao licenciamento aberto em vários outros) foi o Fate 3.0. O esforço de extrair o sistema (feito por Leonard Balsera e Ryan Macklin) para apresentá-lo por si só, levou-o a passar por várias melhorias, da qual surgiu o ***Fate Básico***.

## Licenciamento

De uma forma ou de outra, Fate sempre teve um licenciamento aberto. Você pode encontrar detalhes do licenciamento do Fate para seus projetos (em inglês) em <http://www.faterpg.com/licensing>

------

- [« Regras Opcionais](../regras-opcionais/)
