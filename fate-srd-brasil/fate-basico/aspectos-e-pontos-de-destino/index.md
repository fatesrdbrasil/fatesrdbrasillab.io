---
title: Aspectos e Pontos de Destino
layout: default
---

# 4 - Aspectos e Pontos de Destino

## O Que É Um Aspecto?

Um **aspecto** é uma frase que descreve algo único ou notável sobre alguma coisa. Ele é a principal forma pela qual você gasta e ganha pontos de destino, além de inﬂuenciar a história por criar oportunidades para os personagens ganharem bônus, complicar a vida de um personagem ou adicionar valores à rolagem ou oposição passiva de um personagem.

## O Que São Pontos de Destino?

Narradores e Jogadores, ambos possuem um conjunto de pontos chamados **pontos de destino** que podem ser usados para inﬂuenciar o jogo. Eles podem ser representados por marcadores, como mencionamos no início do livro. Jogadores, vocês começam com uma quantidade de pontos igual a **recarga** de seu personagem. Esse é o valor de início sempre que começar uma sessão com menos pontos do que o valor. Narradores recebem uma quantidade de pontos para gastar a cada cena.

Quando surgir a oportunidade de usar os seus aspectos, haverá a chance de gastar ou ganhar pontos de destino.

- [« Criação Rápida de Personagem](../criacao-rapida-de-personagem/)
- [Tipos de Aspectos »](../tipos-de-aspectos/)
