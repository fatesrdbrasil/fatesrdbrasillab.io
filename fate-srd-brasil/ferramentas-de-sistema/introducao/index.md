---
title: Capítulo 1 - Porque Estamos Aqui
layout: default
---

# Capítulo 1 - Porque Estamos Aqui

Pense no Fate como uma máquina, construído para produzir um resultado específico. Como muitas máquinas, Fate pode ser ajustado para produzir outros resultados. Possui um sistema de indicadores, uma rede de regras conectadas que podem ser ajustadas e modificadas para alcançar o resultado que você procura. É robusto, flexível e, o mais importante, adaptável.

Qual seria o sentido de “adaptável” neste caso? Em primeiro lugar, significa que você pode mudar as regras. Isso é possível em qualquer jogo

- se estiver jogando Banco Imobiliário e permitir que alguém receba dinheiro ao cair em “Parada Livre”, seria uma adaptação. Isso altera a mecânica da economia do jogo deixando a partida mais favorável, uma vez que fica mais difícil perder no jogo por falta de dinheiro. Fate é assim
- ele pode ser alterado. Quer que o Narrador possua mais pontos? Que os jogadores tenham perícias mais altas? Que Aspectos funcionem de outro jeito? Tudo isso é possível com um pouco de esforço de sua parte.

O segundo aspecto dessa adaptabilidade é que Fate não cria resistência contra suas alterações. Na verdade, o sistema é adaptável o suficiente para que pequenas mudanças sejam feitas sem comprometer as regras e até mesmo alterações maiores exijam apenas alguns ajustes em outras partes do sistema. Não é apenas que você pode alterar o Fate, mas que é fácil alterá-lo – se você estiver disposto a pôr a mão na massa.

Para obter o clima desejado, algumas alterações podem ser necessárias. Fate é um sistema ajustável e o Fate Sistema Básico apresenta o sistema padrão de jogo. Talvez essas mecânicas não sejam apropriadas para o seu jogo. Talvez seja necessário fazer ajustes ou mesmo adicionar novos elementos, mas você não esteja seguro sobre quais mudanças fazer ou quais outras partes do sistema essas mudanças afetarão. Não se preocupe, é por isso que estamos aqui.

Ferramentas do Sistema é um livro sobre como alterar o Fate, o que acontece quando você o faz e o que pode fazer quando começar a brincar com o funcionamento das coisas. Se isso é algo que você gosta, então este livro foi feito para você.

------

- [« Prefácio](../prefacio/)
- [A Regra de Bronze »](../regra-de-bronze/)
