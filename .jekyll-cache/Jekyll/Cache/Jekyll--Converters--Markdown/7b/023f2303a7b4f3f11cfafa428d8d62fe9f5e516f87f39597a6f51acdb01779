I"�	<p>Fate System Toolkit</p>

<h1 id="scale">Scale</h1>

<p>If you expect your game’s themes and issues to give rise to conflicts between
entities of different size or scale—dragon vs. knight, snub fighter vs. space
station, or start-up vs. multinational corporation, for example—consider
making use of these rules. If not, you’re probably better off giving this
section a pass.</p>

<p>First, establish how many <strong>scale steps</strong> you’ll need. Three or four should do
it. What those steps represent is going to depend on the nature of the setting
and the stories you expect to tell, but they’ll always reflect incrementally
larger or more potent things in the setting and will commonly be involved in
conflicts.</p>

<blockquote>
  <p>Mike’s game about warring secret societies of varying size and influence has
scale steps called Local (confined to a single city), Regional (multiple
cities), and Widespread (all cities).</p>
</blockquote>

<p>When two entities enter into a conflict with one another, the differences in
their scale come into play. For every step that separates them, apply one or
both of the following effects to the larger of the two:</p>

<ul>
  <li>+1 to the attack roll <em>or</em> +1 to the defense roll</li>
  <li>Deal +2 shifts of harm on a successful attack <em>or</em> reduce incoming harm by 2</li>
</ul>

<p>How to apply these effects depends on what makes sense in context.</p>

<blockquote>
  <p>If the Hatchet Gang (scale: Local) stages a daring raid against the
Benevolent Association of Celestial Wanderers (scale: Regional), the Hatchet
Gang will have a harder time of it, in terms of resources and personnel,
against the better funded Association. It’s reasonable to give the Association
a +1 bonus to its defensive Fighting roll.</p>
</blockquote>

<p>Of course, if the conflict is between two entities of roughly equivalent size
or scale, then none of these effects applies. They only come into play when
the scale is <em>unequal</em>.</p>

<p><strong>Scale as an Extra</strong>: Most things in your setting will have a scale rating just based on what they are as a matter of common sense—all small gangs are Local, for example—but you can give players the chance to alter the scale of their characters, possessions, or holdings as an extra they can take, if it fits with their high concept.</p>

<ul>
  <li><a href="/fate-srd/fate-system-toolkit/other-dice">« Other Dice</a></li>
  <li><a href="/fate-srd/fate-system-toolkit/sidekicks-vs-allies">Sidekicks vs. Allies »</a></li>
</ul>

:ET