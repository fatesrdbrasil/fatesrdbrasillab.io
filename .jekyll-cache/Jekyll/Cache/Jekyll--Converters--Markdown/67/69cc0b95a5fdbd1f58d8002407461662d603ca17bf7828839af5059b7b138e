I"5<p>Fate System Toolkit</p>

<h1 id="sidekicks-vs-allies">Sidekicks vs. Allies</h1>

<p>Players can select both sidekicks and allies as extras, but differentiating a
boy wonder from a squad of professional agents can be tough. Here are a few
ways to distinguish <strong>sidekicks</strong>, who work closely with the PCs, from trained
**allies **that they may call upon in specific situations.</p>

<h2 id="permissions-and-costs">Permissions and Costs</h2>

<p>While both sidekicks and allies are extras (see <em><a href="../../fate-
core/extras">Extras</a></em> in Fate Core), sidekicks tend to be unlocked through personal
relationships and allies tend to arise from organizational or resource-driven
permissions. For example, Sherlock Holmes might select the aspect
<span class="aspect">Good Friend of Dr. Watson</span> to add Watson to his sheet as a
sidekick, while selecting the aspect <span class="aspect">Patron of the Baker Street
Irregulars</span> would add the mostly nameless children he uses as spies and
couriers. As for costs, sidekicks tend to impose social and relational costs
on the characters—asking them to contribute to important causes or aid them in
times of need—while allies usually require more direct payment, favors, or
other resources.</p>

<h2 id="stress-tracks-and-consequences">Stress Tracks and Consequences</h2>

<p>One great place to differentiate sidekicks and allies is with stress tracks
and consequences in conflicts. Sidekicks are like supporting characters—give
them a limited stress track with one mild (or perhaps moderate) consequence
slot and one extreme consequence that might change the character’s sheet in a
dramatic moment.</p>

<p>Allies, on the other hand, are usually faceless mobs that have a stress box
for each member of the gang. Stress inflicted on allies typically knocks them
out of the fight completely instead of leaving them with consequences that
carry from scene to scene. This distinction makes it harder to take out a gang
of allies in a single attack, but makes sidekicks more flexible and resilient
across a variety of social and physical situations in which they can utilize
their consequence slots to protect or defend the PCs.</p>

<h2 id="permanent-vs-temporary-allies">Permanent vs. Temporary Allies</h2>

<p>Sidekicks are almost always permanent parts of a character’s sheet, but allies
might be temporary, depending on the needs of the PC. For example, an
intelligence officer might take the aspect <span class="aspect">Sent by the
Feds</span> to have a set of Federal Agents on call, at the cost of a point of
refresh. It’s equally feasible that such a character could forgo such
permanent allies in favor of creating an advantage for an upcoming scene,
using Contacts or Resources. Of course, permanent allies should be much more
powerful, with more stress boxes, additional aspects, and additional stunts,
than temporary allies who are little better than a weak nameless mob.</p>

<ul>
  <li><a href="/fate-srd/fate-system-toolkit/scale">« Scale</a></li>
  <li><a href="/fate-srd/fate-system-toolkit/wealth">Wealth »</a></li>
</ul>

:ET