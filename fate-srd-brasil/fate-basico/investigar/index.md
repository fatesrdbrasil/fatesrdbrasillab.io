---
title: Investigar
layout: default
---

### Investigar

Investigar é a perícia usada para encontrar coisas. Ela anda lado a lado com Percepção - enquanto Percepção está ligada a estar atento ao que está acontecendo ao seu redor e a observação física, Investigar gira em torno do esforço concentrado e análise profunda.

- `O`{: .fate_font} **Superar:** Os obstáculos de Investigar são informações difíceis de descobrir por algum motivo. Analisar uma cena de crime em busca de pistas, buscar em um ambiente desorganizado por algo que precisa, até mesmo se debruçar sobre um livro antigo e mofado tentando encontrar a passagem que esclarece tudo.
Correr contra o tempo para coletar uma evidência antes que os policiais apareçam ou um desastre ocorra é uma forma clássica do uso de Investigar em um desafio.
- `C`{: .fate_font} **Criar Vantagem:** Investigar é provavelmente uma das perícias mais versáteis que podem ser utilizadas com criar uma vantagem. Se estiver disposto a dedicar algum tempo, poderá encontrar qualquer informação sobre qualquer um, descobrir qualquer detalhe sobre um lugar ou objeto ou criar aspectos sobre quase qualquer coisa no mundo de jogo que o seu personagem poderia razoavelmente trazer à tona.
Se isso soa grandioso demais, considere o seguinte como algumas das possibilidades para o uso de Investigar: espionagem de conversas, procurar pistar em uma cena de crime, examinar registros, verificar a veracidade sobre uma informação, conduzir uma vigilância e pesquisar uma história falsa.
- `A`{: .fate_font} **Atacar:** Esta perícia não é usada em ataques.
- `O`{: .fate_font} **Defender:** Ela também não é usada como defesa.

#### Façanhas Para Investigar

-  **Atenção aos Detalhes:** Você pode usar Investigar em lugar de Empatia para se defender contra o uso da perícia Enganar. O que os outros aprendem através de reações e intuição, você aprendeu através da observação minuciosa de micro expressões.
-  **Bisbilhoteiro:** Em uma rolagem bem-sucedida de Investigar usando a ação de criar vantagem na tentativa de espionar uma conversa, você pode descobrir ou criar um aspecto adicional (porém sem receber uma invocação grátis).
-  **O Poder Da Dedução:** Uma vez por cena você pode gastar um ponto de destino (e alguns minutos de observação) para realizar uma rolagem especial de Investigar, representando suas habilidades de dedução. Para *cada* tensão que conseguir nessa rolagem você descobre ou cria um aspecto ou na cena, ou no alvo observado, mas pode invocar apenas um deles gratuitamente.

- [« Furtividade](../furtividade/)
- [Lutar »](../lutar/)
