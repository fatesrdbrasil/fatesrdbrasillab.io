---
title: Durante o Jogo
layout: default
---

## Durante o Jogo

Agora que você já passou pelo processo de organização da partida com os jogadores, vamos detalhar um pouco mais como abordar as várias funções que você exercerá durante o jogo.

### A Regra de Ouro

Antes de entrarmos em detalhes, aqui está nossa Regra de Ouro do *Fate*:

-  **Primeiro decida o que você quer fazer, depois consulte as regras para ajudá-lo a fazer isso.**

Pode parecer óbvio, mas mencionamos porque a ordem é importante. Em outras palavras, não pense nas regras como uma camisa de força, ou restrição de ações. Ao invés disso, use-as como um conjunto de ferramentas em potencial para representar o que você quiser. Sua intenção, qualquer que seja, sempre tem prioridade sobre as regras.

Na maioria das vezes, a definição de uma ação será bastante simples – sempre que tentar ferir alguém, é preciso atacar. Sempre que tentar evitar se ferir, é preciso defender.

Às vezes, porém, haverá uma situação onde o tipo de ação apropriada não é totalmente claro. Como Narrador, em tais situações, não diga que essas ações são proibidas. Em vez disso, tente buscar pelas intenções da ação, tentando esclarecer melhor a situação e enquadrá-la a uma das ações básicas do jogo.

------

### A Regra de Prata

A continuação da regra de ouro: nunca deixe as regras atrapalharem algo que faça sentido narrativo. Se você ou seus jogadores narram algo em jogo e faz sentido aplicar algumas regras fora das circunstâncias normais, vá em frente.

O exemplo mais comum são as [consequências](../conflitos/#consequências). As regras dizem que, por padrão, uma consequência é algo que o jogador escolhe receber após receber um ataque em um conﬂito.

Mas digamos que você está em uma cena em que um jogador decide que, como parte de sua tentativa de intimidação, seu personagem vai socar através de uma mesa com tampo de vidro com as mãos nuas.

Todos acham a ideia legal, então ninguém está interessado no que aconteceria se o jogador falhasse na rolagem. No entanto, todos concordam que o personagem machucaria a mão no processo (e isso faz parte do que o torna intimidador).

Assim, eles decidem que ele recebe uma consequência leve chamada ***Vidros em Minhas Mãos***, porque isso tem sentido narrativo, embora não haja conﬂito e ninguém atacou o PJ.

Assim como na Regra de Ouro, tenha certeza que todos entenderam o que está sendo dito e feito.

------

> Devido a uma falha na rolagem anterior, Fräkeline acidentalmente ativou uma armadilha enquanto procurava por um cofre na mansão de um nobre em Nova Quantum. Amanda descreve o salão preenchido por lâminas em movimento aleatório por todos os lados, com a porta do cofre do outro lado da sala.
>
> Maira diz, “Bem, não faz diferença. Vou em direção do cofre, mantendo os olhos nas lâminas.”
>
> Amanda pensa um pouco, pois sabe que os dados terão que resolver a situação. Se Fräkeline está se movendo pelo salão, isso parece bastante com uma ação de superar para realizar o movimento, mas considerando as lâminas, também parece que ela precisa se defender. Há também duas formas de ela lidar com a armadilha – pode ser apenas uma dificuldade passiva contra Maira para tentar impedi-la de atravessar, mas por causar dano, parece também um ataque – que possui oposição ativa.
>
> Então Amanda pergunta, “Maira, precisamos rolar os dados, mas qual é o seu objetivo exatamente na cena? Vai tentar evitar sofrer dano ou atravessar o salão para chegar ao cofre?”
>
> Maira nem hesita. “O cofre é o principal, com certeza.”
>
> Amanda pergunta,  “Então você está disposta a receber dano no processo?”
>
> Maira responde, “Sim. Estou me colocando em perigo, como de costume.”
>
> Amanda define, “Certo, podemos fazer isso em só uma rolagem. Veja só: Você rolará Atletismo contra uma dificuldade Fantástica (+6). Se você conseguir, consegue passar pelas lâminas e não recebe nenhum dano. Se não conseguir, você ficará presa no meio do salão e precisará tentar novamente. Essa falha também vai contar como uma falha em uma ação de defesa, e você receberá dano das lâminas."
>
> Maira recua um pouco, mas concorda com a cabeça e pega os dados.

Nesse exemplo, Amanda combinou o efeito das ações de superar e defesa para determinar o que acontece com Fräkeline. Não há problema em fazer isso, porque se encaixa perfeitamente em suas intenções e faz sentido para a situação que foi descrita. Ela talvez decida fazer ambas as rolagens separadamente, e isso seria perfeitamente aceitável também – mas ela preferiu fazer apenas uma rolagem.

Se estiver em dúvida durante a partida, volte à Regra de Ouro e lembre-se que você tem a ﬂexibilidade para fazer a mesma coisa, caso precise. Apenas esteja certo de que, ao fazer isso, você e todos os participantes estejam de acordo.

### Quando Rolar os Dados

**Role os dados quando o sucesso ou falha de determinada ação possa contribuir com algo interessante no jogo.**

A ideia é fácil de entender do ponto de vista do sucesso, na maioria das vezes – quando o personagem supera um obstáculo, vence um conﬂito ou é bem-sucedido em seu objetivo, isso abre o caminho para a próxima etapa. Com a falha, no entanto, é um pouco mais difícil, porque é fácil olhar para uma falha em termos puramente negativos – você falha, perde, não consegue o que queria. Não há nada a ser construído após uma falha, e isso pode paralisar um pouco as coisas.

A pior, *pior* coisa que você pode fazer é permitir que uma falha signifique que *nada acontece* – nenhum conhecimento novo, nenhum caminho novo a ser tomado e nenhuma mudança na situação. Isso é bastante entediante e desencoraja os jogadores a investirem em falhas – algo que você deseja que eles façam, visto a importância das mecânicas de consequências e concessão. Não faça isso.

Se você não consegue imaginar uma resolução interessante para ambos os resultados, então não peça a rolagem. Se falhar é uma opção desinteressante, dê aos PJs o que eles desejam e peça por uma rolagem depois, quando você *consegue* pensar em uma falha interessante. Se o sucesso for uma opção entediante, então veja se consegue transformar sua ideia de falha em uma ação de forçar, usando o momento como oportunidade para oferecer pontos de destino aos jogadores.

------

### Aspectos de Situação São Seus Amigos

Quando está tentando imaginar se há uma boa razão para pedir uma rolagem de superar aos jogadores, dê uma olhada nos aspectos da cena. Se a existência de algum aspecto sugere algum problema ou dificuldade aos PJs, peça uma rolagem de superar. Se não, e você não consegue pensar em uma consequência interessante para uma falha, deixe de lado.

Por exemplo, se um personagem tenta atravessar uma sala rapidamente sem chamar atenção, e você possui um aspecto de situação chamado ***Pedras Barulhentas No Chão***, então faz sentido pedir uma rolagem de superar para que ele possa se mover. Se não houver nenhum aspecto interessante, então deixe-o realizar o movimento e faça algo mais interessante à frente.

------

### Tornando a Falha Algo Sensacional

Se os PJs falharem em uma rolagem e você não está seguro sobre como fazer disso algo interessante, tente alguma das seguintes ideias.

#### Culpe as Circunstâncias

Os PJs são pessoas extremamente competentes (lembre-se, essa é uma das ideias do *Fate*). Eles não fazem papel de tolos com muita ou mesmo média frequência. Às vezes, tudo o que precisamos é transformar a falha em algo dinâmico – no lugar de narrar como os PJs estragam a coisa, jogue a culpa da falha em algo que os PJs não poderiam ter evitado. Há um mecanismo secundário na trava que inicialmente parecia simples (Roubo) ou o contato não apareceu na hora certa (Contatos) ou o tomo antigo é muito difícil de ler (Conhecimentos) ou um deslocamento sísmico repentino lhe tira da corrida (Atletismo).

Dessa forma, os PJs ainda parecem competentes e incríveis, mesmo não alcançando o que desejam. Mais importante, lançar a culpa sobre as circunstâncias cria a oportunidade de sugerir um novo caminho, o que permite que as falhas gerem momentos importantes em sua história. O contato não apareceu para o encontro? Onde ele está? Quem o estava seguindo até o local marcado? O tomo antigo é difícil de ler? Talvez alguém possa restaurá-lo. Dessa forma, você não perde tanto tempo com a falha e ainda pode pensar em algo novo.

#### Sucesso a um Custo

Você também pode oferecer aos PJs o que eles queriam, mas por um preço – nesse caso, a falha na rolagem significa que eles não foram capazes que alcançar seus objetivos sem receber uma consequência.

Um **custo menor** é o bastante para complicar a vida de um PJ. Como mencionado na seção anterior, a ideia é usar a falha para alterar um pouco a situação, ao invés de simplesmente invalidar a tentativa do personagem. Algumas sugestões:

-  Anuncie um perigo iminente: *“A tranca abre com um clique suave, mas o barulho da porta não é tão suave assim. Se eles não haviam notado sua presença, agora notaram”.*
-  Introduza em empecilho: *“Sim, o líder da guilda pode lhe apresentar um mago que poderia traduzir o tomo antigo - um homem chamado Berthold. Você o conhece. Inclusive, na verdade a última vez que o viu foi a alguns anos atrás, quando ele o pegou com sua esposa".*
-  Apresente uma escolha difícil ao jogador: *“Você consegue alcançar dois de seus companheiros antes que o teto desabe, mas não todos eles. Quais serão?”*
-  Adicione um aspecto ao personagem ou à cena: *“De alguma forma você consegue cair de pé, mas com uma **Torção no Tornozelo**”.*
-  Forneça um impulso a um PdN. *“Você se surpreende quando Nikolai aceita sua oferta, mas ele faz isso com um sorriso irônico que lhe incomoda. Claramente **Nikolai Tem Um Plano**”.*
-  Marque uma das caixas de estresse do PJ. Cuidado com isso – só é um custo válido se o PJ correr o risco de tomar mais dano na mesma cena. Se achar que isso não vai acontecer, faça outra escolha.

Um **custo sério** faz mais do que complicar a vida do PJ ou indicar coisas piores por vir - é um preço alto, possivelmente irrevogável, naquele exato momento.

Uma forma de fazer isso é aumentando a seriedade de um custo menor. Ao invés da suspeita de um guarda ter ouvido você abrindo a tranca, alguns guardas aparecem na sala com armas apontadas. Ao invés de ter apenas que escolher qual dos aliados irá salvar, algum deles pode acabar soterrado nos escombros. Ao invés de simplesmente ter que enfrentar uma situação embaraçosa com Berthold, ele não superou a situação e quer vingança.

Outras opções podem incluir:

-  Reforçar a oposição. Você pode limpar umas das caixas de estresse de um PdN, melhorar uma de suas perícias em um nível ou dar a ele um novo aspecto com uma invocação grátis.
-  Trazer uma *nova* oposição ou obstáculo, como inimigos adicionais ou um aspecto de situação que torne as coisas mais complicadas.
-  Adiar o sucesso. A tarefa atual demorará muito mais tempo que o previsto.
-  Dar uma consequência ao PJ que faça sentido nas circunstâncias – suave, se houver uma caixa disponível, ou então moderada.

Se você não estiver seguro sobre o quão sério um custo deve ser, você talvez queira usar a margem de falha como base. Por exemplo, na cena da abertura da tranca acima – aquela em que os guardas ouvem o PJ e aparecem na sala – se o jogador falhou em sua rolagem de Roubo por 1 ou 2, haverá mais PJs que guardas. Não é uma luta difícil, mas ainda sim é uma luta. Se a falha for de 3 a 5, é uma luta de igual pra igual, do tipo que consumirá recursos como pontos de destino e consequências. Mas se a falha foi de 6 ou mais, eles estarão em menor número e em perigo de verdade.

#### Deixe os Jogadores Fazerem o Trabalho

Você também pode lançar a questão aos jogadores e deixar que eles decidam as circunstâncias da falha. Esse é um grande movimento em favor do espírito de narrativa compartilhada e alguns jogadores serão bastante severos com seus personagens em favor da história, especialmente se isso significar que eles mantêm o controle sobre o destino do seu personagem.

Também é algo interessante de se fazer caso você não tenha ideias interessantes. “Certo, então você falhou em Roubo por 2. Você mexe um pouco na fechadura e algo dá errado. O que é?”. “Você falhou na rolagem de Percepção. O que você não notou conforme se esgueirava até o quarto da rainha?” É melhor quando a pergunta é específica, como nos exemplos – se você dizer apenas “Certo, me diz como você falhou!” o jogo pode travar simplesmente por colocar o jogador contra a parede sem necessidade. Você quer *deixar* o jogador contribuir, não *obrigá-lo*.

### O Nível de Dificuldade

Quando você determinar uma oposição passiva para uma ação, tenha em mente as dicas que passamos em [*Ações e Resoluções*](../acoes-e-resolucoes/) – qualquer coisa dois níveis ou mais acima do nível de perícia de um personagem provavelmente custará pontos de destino e dois pontos ou mais abaixo do nível da perícia será bastante fácil.

Ao invés de “simular o mundo” ou ser “realista” demais, tente definir a dificuldade de acordo com a necessidade dramática – *as coisas em geral são mais desafiadoras quando a aposta é alta e menos desafiadoras quando a aposta é baixa.*

Na prática, isso significa estabelecer uma dificuldade constante e acrescentar uma penalidade ocasional para representar a pressa do personagem ou outra condição desfavorável. Psicologicamente, porém, a diferença entre uma dificuldade alta e uma baixa *com penalidades altas*  não deve ser subestimada. Um jogador que enfrenta uma dificuldade alta muitas vezes se sentirá devidamente desafiado, enquanto esse mesmo jogador enfrentando uma dificuldade baixa com uma grande penalidade, possivelmente definida pelo Narrador, se sentirá desencorajado.

A principal utilidade de definir uma dificuldade baixa é mostrar a grandiosidade dos PJs, deixar que brilhem naquele momento e nos lembrar de por que aquele personagem está sendo destacado. Você também pode definir dificuldades baixas nos períodos em que sabe que os PJs estão com poucos pontos de destino, dando-lhes a chance de aceitar forçar seus aspectos para ganharem mais pontos. Você também deve definir dificuldades baixas para qualquer coisa que esteja no caminho do andamento da cena para os PJs – você não quer que eles fiquem entediados por estarem presos na entrada da fortaleza do vilão, se o foco da cena é confrontar o grande inimigo.

Por fim, algumas ações devem possuir uma dificuldade baixa por padrão, especialmente se não houver oposição direta ou resistência. Esforços sem oposição para criar vantagem em um conﬂito nunca devem ser maiores que Regular (+1) ou Razoável (+2), principalmente se estiverem tentando criar um aspecto para um objeto ou local. Lembre-se que a oposição nem sempre virá na forma de um PdN tentando atrapalhar as coisas – se o gênio do mal escondeu uma prova em seu esconderijo, longe de olhares indiscretos, você pode considerar isso uma forma de oposição, mesmo que o vilão não esteja fisicamente presente.

Se os PJs estiverem transbordando em pontos de destino ou estiverem em um momento crucial na história onde a vida de alguém está em jogo ou o destino de muitos estiver na jogada, ou então eles finalmente enfrentarão as pessoas que estiveram procurando durante um cenário ou dois, sinta-se livre para aumentar as dificuldades. Você também deve aumentar as dificuldades para indicar quando um determinado oponente é extremamente preparado para encontrar os PJs ou para reﬂetir que a situação não é tão favorável – se os PJs não estiverem preparados, não possuírem as ferramentas necessárias para o trabalho ou o tempo é curto, etc.

Definir a dificuldade exatamente no mesmo nível que a perícia do PJ é, como você deve imaginar, o meio termo entre esses dois extremos. Faça isso quando quiser alguma tensão sem dificultar demais as coisas ou quando as probabilidades estão levemente a favor dos PJs, mas quiser que haja um risco perceptível.

------

### Importante: Justificando Suas Escolhas

A única outra restrição na hora de definir dificuldades, voltando ao assunto da Regra de Prata abordada acima – você precisa ter certeza que suas escolhas fazem sentido no contexto narrativo que está criando. Embora não queiramos que você enlouqueça tentando criar todos os detalhes do mundo e acabe se atrelando a uma lista de limitações desnecessárias (“as fechaduras no vilarejo da Floresta do Vale Estreito geralmente são de boa qualidade, devido a sua proximidade a uma mina rica em ferro”), também não encare isso como se fosse apenas um jogo de números. Se a única razão para definir a dificuldade Excepcional (+5) é porque é duas vezes maior que o nível da perícia do PJ e sua intenção é acabar com seus pontos de destino, você corre o risco de perder credibilidade.

Nesse sentido, podemos encarar a questão de definir dificuldades como sendo muito parecida com a questão de invocar aspectos – precisa haver uma boa razão que justifique sua escolha na história. Não há problema se a justificativa é algo que você precise inventar ao invés de algo já sabido. Aspectos de situação são uma ótima ferramenta nesse sentido – se os jogadores sabem que a caverna em que estão é ***Escura Como Breu*** e ***Extremamente Apertada***, fica fácil justificar porque é tão difícil permanecer quieto enquanto se usa Furtividade dentro dos túneis. Ninguém se incomodará por você usar os aspectos da situação como motivo para aumentar a dificuldade em +2 para cada um, pois seria o bônus que dariam se invocados.

De qualquer forma, não pule a justificativa - deixe os jogadores cientes o que está havendo logo que informar a dificuldade, ou dê de ombros de forma misteriosa e deixe que descubram logo em seguida (ou seja, assim que você tiver decidido).

Você também pode usar dificuldades “fora de contexto” para indicar a presença de questões ainda sem resposta durante o jogo – por algum motivo estranho, o estábulo que está tentando entrar possui uma dificuldade Épico (+7) na tranca da porta. O que pode haver de tão importante lá dentro que precise ser escondido?

Pode ser também que você esteja tentando passar no teste final de iniciação na Ordem Ametista e a dificuldade no teste é apenas Razoável (+2) – qual é o lance? Eles estão pegando leve com você? Sua entrada é uma questão política? Quem está facilitando as coisas? Ou será que a reputação dos estudiosos da Ordem é uma grande mentira?

------

### Lidando Com Sucessos Extraordinários

Às vezes, um PJ irá exceder a dificuldade, recebendo um monte de tensões em uma rolagem. Algumas das ações básicas já possuem um padrão de efeito para rolagens realmente boas, como atingir um golpe devastador numa boa rolagem de ataque.

Em outro casos, não é tão óbvio. O que acontece quando você consegue muitas tensões em uma rolagem de Ofícios ou Investigação? Você quer ter certeza que os resultados possuam algum significado e reﬂitam a competência do PJ.

Aqui estão algumas opções:

-  **Narrativa Cinematográfica:** Pode parecer supérﬂuo, mas é importante celebrar uma grande rolagem com uma narrativa adequada sobre um grande sucesso. Essa é uma boa hora para pegar as sugestões que demos acima sobre como tornar uma falha incrível e aplicar aqui. Deixe o sucesso afetar algo mais, além do que o PJ estava realizando e traga o jogador para o processo de criar o sucesso deixando que ele adicione detalhes legais. “Três tensões nesta rolagem de Roubo – diz ai, será que alguém é capaz de selar essa cripta novamente?”, “Então, você conseguiu cinco tensões na rolagem de Contatos – me diga, aonde vai Nick, o Informante quando foge de sua esposa e o que você diz a ele quando o encontra lá?”.
-  **Adicionar um aspecto:** Podemos expressar efeitos adicionais de uma boa rolagem adicionando um aspecto ao PJ ou à cena, basicamente permitindo que criem uma vantagem gratuitamente. “Certo, sua rolagem de Recursos para subornar a guarda foi bem-sucedida com quatro tensões. Ela deixa você passar livremente pelo portão e também agirá como um ***Reforço*** se precisar de ajuda mais tarde”.
-  **Reduzir o tempo:** Se for importante fazer algo rápido, então é possível usar as tensões extras para diminuir o tempo que leva para realizar uma ação.

### Lidando Com o Tempo

Nós organizamos o tempo de duas maneiras em *Fate*: **tempo do jogo** e **tempo da história**.

#### Tempo do jogo

O tempo do jogo se refere à organização do jogo em tempo real, com os jogadores sentados à mesa. Cada unidade de tempo do jogo corresponde a uma certa quantidade de tempo no mundo real. Essas unidades são:

- **Rodada:** O tempo necessário para que todos os participantes de um conﬂito tenham sua vez, o que inclui realizar uma ação e responder a qualquer ação realizada contra eles. Isso em geral toma apenas alguns minutos.
- **Cena:** O tempo necessário para se resolver um conﬂito, lidar com alguma situação específica ou completar um objetivo. As cenas variam de tamanho, de alguns minutos se for apenas alguma descrição ou diálogo a meia hora ou mais em caso de uma grande batalha contra um PdN importante.
- **Sessão:** A soma de todas as cenas jogadas naquele dia. A sessão termina quando você e seus amigos fecham os livros e vão para casa. Para a maioria das pessoas, uma sessão dura cerca de 2 a 4 horas, mas não há limite – se você tiver poucas obrigações, então o seu limite será a necessidade de comer e dormir. Um marco menor em geral ocorre depois de uma sessão.
- **Cenário:** Uma ou mais sessões de jogo, mas normalmente não mais que quatro. Na maioria das vezes, as sessões que finalizam um cenário resolverão algum tipo de problema ou dilema apresentado pelo Narrador ou encerram uma história (veja [*Cenas, Sessões e Cenário*](../cenas-sessoes-e-cenario/) para mais detalhes sobre cenários). Um marco significativo normalmente ocorre ao final de um cenário. Você pode imaginar isso como se fosse um episódio de uma série de TV – é o número de sessões necessárias para contar uma história.
-  **Arco:** São vários cenários, normalmente entre 2 e 4. Um arco típico normalmente culmina em um evento que causa mudanças significativas no mundo do jogo, causadas pela resolução dos cenários até então. Podemos comparar um arco às temporadas de séries de TV, onde cada episódio avança em direção a um clímax memorável. Arcos nem sempre serão distinguíveis, assim como nem todas as séries de TV possuem um enredo que dure toda uma temporada – é possível ir de uma situação a outra sem precisar de um enredo muito estruturado. Marcos maiores normalmente ocorrem ao final de um arco.
-  **Campanha:** A soma de todas as vezes que vocês sentaram à mesa para jogar aquele jogo de *Fate* específico – cada sessão, cada cenário, cada arco. Tecnicamente, não há limite sobre quanto tempo dura uma campanha. Alguns grupos jogam por anos; outros chegam ao fim de um arco e param. Presumimos que um grupo típico jogará por alguns arcos (ou algo em torno de 10 cenários) antes de ter um grande final e mudar para outro jogo (esperamos que outro jogo em *Fate*!). Você pode organizar uma campanha como um tipo de “superarco”, onde há um conﬂito de grande escala dividido em partes menores, ou pode simplesmente ser apenas formada pelas histórias individuais menores detalhadas nos seus cenários.

#### Tempo da História

**Hora da história** é o tempo na visão dos personagens, a perspectiva do que acontece “na história” – o tempo que leva para completar qualquer coisa que você e os jogadores disseram realizar. Na maioria das vezes, você não terá pensado nisso em detalhes e mencionará apenas brevemente (“certo, levará uma hora para chegar ao aeroporto, de táxi”) ou será algo que faz parte de uma rolagem de perícia (“Legal, então após 20 minutos procurando pela sala, você encontra o seguinte...”).

Na maior parte das circunstâncias, o tempo da história não tem ligação nenhuma com o tempo no mundo real. Por exemplo, uma rodada de combate pode levar alguns minutos em tempo real, mas ocorre em apenas alguns segundos em um conﬂito. Da mesma forma, é possível avançar um período longo de tempo simplesmente dizendo que isso aconteceu (“O contrato leva duas semanas para ficar pronto e retornar a você – você faz algo enquanto espera ou podemos avançar o tempo?”). Isso deve ser usado apenas como conveniência; é um recurso narrativo para dar verossimilhança e consistência à história.

Às vezes, no entanto, é interessante usar o tempo da história de forma criativa para criar tensão e surpresas durante o jogo. Veja como.

##### Prazos

Nada cria mais tensão do que um prazo para fazer algo. Os heróis possuem apenas alguns minutos para desarmar a armadilha mortal, certa quantidade de tempo para atravessar a cidade antes que algo exploda ou tanto tempo para agir antes que os vilões façam algo contra alguém que você ama, e por aí vai.

Algumas das ações básicas do jogo foram criadas para lidarem com a pressão do momento, como desafios e disputas – elas limitam a quantidade de rolagens que um jogador pode fazer antes que algo aconteça, por bem ou por mal.

No entanto, não é necessário limitar-se a apenas essas duas opções. Se você definiu um beco sem saída muito complicado de lidar em algum de seus cenários, pode começar a marcar o tempo tomado por cada coisa no jogo e usar como uma forma de manter a pressão (“Ah, então você quer olhar todos os arquivos da história da cidade? Bem, você tem três dias até o ritual – eu posso permitir a você uma rolagem de Conhecimentos, mas esta tentativa levará ao menos um dia”). Lembre-se, quase tudo leva tempo.

Mesmo uma tentativa básica de criar vantagem usando Empatia exige que você se sente com o seu alvo por um momento, e se toda ação dos PJs estiver contando, pode ser tempo que eles não têm para gastar.

Claro, não há graça se não houver nada que eles possam fazer para melhorar a situação do prazo, além do que não será divertido se o andamento das coisas até o prazo final for totalmente previsível.

##### Usando o Tempo da História Em sucessos e Falhas

Ao usar o tempo da história para criar pressão, sinta-se livre para incorporar saltos imprevistos no tempo quando o PJ se sair bem ou mal em uma rolagem.

Demorar mais tempo é uma ótima forma de tornar uma falha algo interessante, como descrito anteriormente, especialmente usando a opção de “sucesso a um custo” – forneça aos jogadores exatamente o que querem, mas ao custo de mais tempo do que eles realmente esperavam gastar, arriscando chegar tarde demais no momento final. Outra forma seria algo que não altere o prazo, mas que agregue mais problemas que devem ser resolvidos até lá.

Da mesma forma, recompense sucessos excelentes ao reduzir a quantidade de tempo que algo leva para ser concluído enquanto há um prazo a ser cumprido. Aquela pesquisa histórica (Conhecimentos) que poderia levar dias poderá ser concluída em algumas horas. Enquanto busca um bom comerciante (Contatos) para conseguir suprimentos, você acaba encontrando outro que pode atender a seu pedido no mesmo dia, ao invés de uma semana.

Se o tempo for limitado, também deveria ser possível usar invocações e ações de forçar para manipular o tempo, tornando as coisas mais fáceis ou mais complicadas, respectivamente (“Ei, sou um ***Rato de Garagem***, então consertar esse carro não deverá tomar tanto tempo, não é?”, “Então, mas veja só. Sua ficha diz ‘***Não me Canso de Diversão e Jogos***’... se você está procurando um sujeito num cassino, não faria sentido que você se distraísse? Todas aquelas máquinas e tal...).

------

### Quanto Tempo Vale Uma Tensão?

Assim como qualquer outra rolagem, o número de tensões que consegue (ou a diferença pela qual você falha) deve servir de medida para quão drasticamente você consegue manipular o tempo. Então como decidir quanto tempo premiar ou cortar?

Isso depende do tempo que acha que a ação tomaria. Geralmente expressamos o tempo em duas partes: uma medida de quantidade específica ou abstrata seguida por uma unidade de tempo, então transformamos isso em uma unidade de tempo, como “alguns dias”, “vinte segundos”, “três semanas”, e assim por diante.

Recomendamos administrar o tempo de forma abstrata e expresse todas as ações de jogo como sendo *metade*, *uma*, *algumas* ou *várias vezes* uma unidade de tempo. Então imagine que algo levará seis horas para ser realizado, pense nisso como “algumas horas”. Se achar que algo levará 20 minutos, você pode dizer “alguns minutos” ou “meia hora” ou algo próximo disso.

Isso serve como ponto de partida. A partir daí, cada tensão significa um salto para cima ou para baixo na medida de tempo que usar. Então, se seu ponto de partida for "várias horas", e acelerar as coisas beneficiará os PJs, trate desta forma: uma tensão diminui o tempo para "algumas horas", duas, para "uma hora", e três, para "meia hora".

Ultrapassar um dos extremos do espectro diminui a duração da ação para vários incrementos da próxima unidade de tempo ou aumenta até a metade da próxima unidade, dependendo da direção. Então, se quatro tensões fossem conseguidas na rolagem sugerida anteriormente, você poderia saltar de "várias horas" para "vários minutos". Falhar com uma tensão causa o inverso, podendo tornar uma atividade de "várias horas" em "metade de um dia".

Isso permite que você lide com saltos de tempo de forma rápida, independente de qual seja seu ponto de partida e se as ações imaginadas levam instantes ou gerações.

------

##### Tempo da História e o Alcance de Uma Ação

É fácil pensar que a maior parte das ações dos PJs se limita a qualquer coisa que o personagem possa afetar diretamente, funcionando num nível mais pessoal. De fato, na maior parte do tempo, será exatamente assim – afinal de contas, *Fate* é um jogo sobre competências individuais que se destacam perante adversidades dramáticas.

No entanto, considere o que um PJ pode fazer com essa competência se tiver *todo o tempo do mundo* para completar uma ação em particular. Imagine uma rolagem de Comunicação que representa um mês de negociação onde o PJ começa a falar com todos os envolvidos em detalhes ao invés de focar apenas em uma única reunião. Imagine uma rolagem de Investigação que dure uma semana, traçando cada detalhe da rotina de um alvo.

Ao permitir que cada rolagem represente um grande período de tempo, você pode “focar” em eventos que vão muito além do personagem do jogador que está realizando a rolagem e afetar o cenário de forma significativa.

Essa rolagem de Comunicação que equivale a um mês pode resultar em um novo rumo político para o país que o PJ está representando. A rolagem de Investigação pode trazer à tona um dos criminosos mais notórios do cenário, que que vem perseguindo os PJs por uma campanha inteira.

Essa é uma ótima maneira de agregar interação a pausas longas no tempo da história ao invés de segurar o andamento do jogo com narrações longas ou tentar criar explicações retroativas do que aconteceu durante aquele período. Se os PJs possuírem metas a longo prazo, veja se consegue transformar isso numa disputa, desafio ou conﬂito que cubra todo o evento ou apenas uma rolagem de uma perícia para ver se algo inesperado acontece. Se a rolagem falhar, as consequências criadas por isso serão importantes para o andamento do jogo.

Lembre-se de definir as dificuldade apropriadamente caso decida criar um conﬂito ou disputa – se um conﬂito leva um ano, então cada rodada pode equivaler a um ou dois meses e todos devem descrever suas ações e os resultados de suas ações nesse contexto.

> Durante um marco maior em uma campanha, Esopo alterou seu conceito para ***Soldado Ordenador Lagosta***, ao entrar – acidentalmente – para a força policial universal.
>
> Amanda deseja avançar a campanha seis meses e sugere que se Esopo permanecer na ordem, ele e seus companheiros seguirão caminhos diferentes. Ela vê a oportunidade de criar material para a próxima sessão, então diz “Acho que poderíamos descobrir se Esopo começa o próximo cenário como uma lagosta policial ou não.”
>
> Eles decidem fazer isso por meio de um conﬂito, onde cada rolagem representa um confronto entre Esopo e um grupo de antigos companheiros de crime, numa missão da ordem. As coisas não correm bem e ele concede, recebendo uma consequência moderada para a próxima sessão. Amanda sugere que Esopo é levado pelos criminosos, que tentarão convencê-lo a trabalhar para eles. Léo escreve ***Tentando Arrumar Minha Vida***, reﬂetindo seu interesse em mudar de rumo.
>
> Quando virmos Esopo novamente, ele estará nas garras dos bandidos, lutando para decidir sobre sua lealdade.

##### Aproximando e Afastando

Não há regra que diga que você precise manter consistência no tempo da história para as rolagens. Um truque legal é usar o resultado de uma rolagem como uma transição para outra rolagem que representa uma ação que dura um tempo muito menor ou vice versa. Essa é uma forma excelente de abrir uma nova cena, disputa ou conﬂito ou apenas introduzir uma mudança no andamento.

> Durante o intervalo já mencionado de seis meses, Fräkeline esteve investigando de associados dos Bruxos de Moondor, que fizeram aparições maiores no último arco da campanha. Ela decide ir sozinha, mesmo com Bandu se oferecendo para ajudar, e consegue a informação que precisa com uma rolagem bem-sucedida na sua perícia recém-adquirida de Conhecimentos nível Regular (+1).
>
> Amanda descreve Fräk se perdendo em pesquisas por alguns meses. “Adorei. Você viaja por algum tempo e passa o resto na rede, procurando informações. Uma noite, você acorda com um barulho em seu quarto de hotel e percebe que seu computador foi levado.”
>
> Maira diz, “Ah, não, eu pego meu chicote e corro atrás de quem for que estiver me roubando.”
>
> Amanda responde, “Ótimo! Vamos fazer uma disputa de Atletismo pra ver se você alcança o culpado.” O Narrador deve notar que o foco agora é no momento – saímos direto da rolagem de meses para rolar os segundos que serão necessários para Fräk entrar na perseguição).
>
> A disputa vai mal para Fräkeline e o ladrão foge. Maira imediatamente diz, “Não importa, alguém na cidade deve saber de algo ou ele pode ter deixado alguma pista. Vou rolar Investigação.”
>
> Maira faz uma rolagem e é bem-sucedida com estilo. Amanda narra que “uma semana depois, você está no posto de troca Nova Tralha num planetóide chamado Aurora Lunar, onde dizem que ela costuma frequentar (é uma garota, a propósito). Ah, e você conseguiu algumas tensões, então já posso dizer que o nome dela é Brilhantina – é conhecida por estas bandas. Isso lhe dá o aspecto ***Sei o Seu Nome***, que você pode usar para diminuir a confiança dela”. (Percebeu o que aconteceu? O foco se afastou e uma rolagem pulou uma semana, mas Amanda e Maira estavam jogando continuamente na mesa).
>
> Maira diz, “Eu chuto uns caixotes, bato o chicote no chão e grito o nome dela.”
>
> Amanda diz, “Todos se afastam e uma mulher numa das barracas se vira, desembainhando uma espada de energia.
>
> “É agora!", Maira diz e pega os dados para se defender, iniciando um conﬂito com foco bastante aproximado.

### Julgando o Uso de Perícias e Façanhas

A esta altura você já leu todos os conselhos necessários para lidar com perícias e façanhas – as descrições em [*Perícias e Façanhas*](../pericias-e-facanhas/), as ações descritas e exemplos em [*Desafios, Disputas e Conﬂitos*](../desafios-disputas-e-conflitos/) e as dicas acima sobre a definição da dificuldade e como lidar com sucessos e falhas.

A única outra situação complicada que pode surgir é quando você se deparar com o limite entre perícias – um jogador deseja usar determinada perícia para uma ação que parece ir um pouco além do razoável ou uma situação aparece em jogo onde faça sentido usar uma perícia em algo que normalmente não faz parte de sua descrição.

Nesses casos, converse com o grupo e veja o que todos pensam. Isso pode acabar de três formas:

-  Vai muito além do plausível. Considere criar uma nova perícia.
-  Não é tão inaceitável e todos podem usar a perícia da mesma forma sob as mesmas condições de agora em diante.
-  Não é um problema desde que o personagem possua uma façanha que permita a ação.

Boa parte dos critérios de base para essas discussões virão do trabalho feito em conjunto com os jogadores ao montar a lista de perícias durante a criação do jogo. Veja [*Perícias e Façanhas*](../pericias-e-facanhas/) para mais conselhos sobre como descobrir os limites de uma perícia e a fronteira entre uma perícia e uma façanha.

Se for decidido que certo uso de uma perícia precisa de uma façanha, permita que o jogador em questão gaste um ponto de destino para tomar aquela façanha "emprestada" apenas durante aquela rolagem se quiser. Então, se ele desejar manter o bônus, será preciso gastar um ponto de recarga para comprá-la (presumindo que haja recarga disponível) ou esperar um marco maior para tal.

#### Aspectos e Detalhes: Descobrir vs Criar

Do ponto de vista do jogador, não há quase nenhuma forma de saber o que você preparou de antemão ou o que inventou no momento, especialmente se você é do tipo de Narrador que não mostra ou consulta anotações. Assim, quando um jogador tentar descobrir algo que você ainda não criou, você pode tratar isso como se ele estivesse criando um novo aspecto ou detalhe da história. Se ele for bem-sucedido, ele encontra o que está procurando. Se falhar, você pode usar o que ele estava procurando como inspiração para ajuda-lo a criar uma informação real.

Se você se sente confortável com improvisação, isso significa que começar o jogo com poucas coisas preparadas de antemão e deixar que as reações e perguntas dos jogadores construam tudo para você. Talvez seja necessário perguntar algo antes, para saber melhor o tipo de informação que o jogador está tentando obter, mas não há limites depois disso.

> Bandu, O Oculto, está nos arredores de Nova Quantum, que vem emanando uma radiação peculiar há alguns dias que está atrapalhando as comunicações e impedindo o trânsito planetário de pessoas e mercadorias. Ele foi pago pela regente da cidade para encontrar uma solução.
>
> Michel diz, “Vou passar algum tempo na cidade procurando estudiosos, pesquisando um pouco sobre a história do local. Eu gostaria de usar Conhecimentos para criar uma vantagem.”
>
> Amanda pensa um pouco. Ela realmente não tinha nada planejado para a cidade, pois todas as suas energias foram focadas nos detalhes da natureza da interferência e o que seria necessário para livrar-se dela, pois ela é mantida por uma força mais poderosa do que os PJs imaginam.
>
> “Que tipo de informação você procura?” Ela pergunta. “Apenas referências em livros ou...?”
>
> Michel diz “Bem, o que realmente quero é saber se alguém sabe qual a origem desse problema, se há registros a respeito, ou alguma lenda antiga que faça referência.”
>
> Amanda diz, “Legal, faça uma rolagem de Conhecimentos dificuldade Razoável (+2)”. Inesperadamente Michel rola -4 e fica com um resultado Medíocre (+0), o que significa que ele falhou. Michel decide não gastar pontos de destino na rolagem.
>
> Na esperança do tornar o fracasso em algo fascinante, ela diz, “Bem, você não consegue um aspecto, mas o que você acaba encontrando é o oposto do que procurava – o local tem um posicionamento impecável, sem montanhas próximas para atrapalhar transmissões, fora que nunca antes houve interferência como essa.”
>
> Michel diz “Se o local é tão bom, de onde estão vindo as distorções?”
>
> Amanda sorri. “Acho que você vai ter que investigar mais profundamente se quiser saber”.
>
> Em suas anotações ela escreve brevemente sobre o fato do subsolo conter ruínas de uma antiga civilização que foram reativadas e o ritualista da região estar mantendo isso em segredo, alterando um pouco a sugestão de Michel e adicionando material extra se ele decidir insistir no assunto.

#### Perícias e Situações Específicas

Ao ler as descrições das perícias, podemos notar que em alguns locais sugerimos uma abstração para algo que usa medidas específicas na vida real. Vigor e Recursos são bons exemplos – muitas pessoas interessadas em treinamento físico têm alguma ideia de quanto peso elas podem erguer e pessoas gastam quantias específicas de dinheiro de uma fonte finita para comprar coisas.

Então quanto um personagem que possui Vigor Ótimo (+4) pode erguer? Quanto pode gastar um personagem com Recursos Razoável (+2) antes de falir?

A verdade é que não temos a mínima ideia e hesitamos em buscar respostas específicas.

Embora possa parecer contraditório, descobrimos que criar minúcias como essas acabam deixando o jogo menos verossímil. Assim que você estabelecer um detalhe como “Vigor Ótimo (+4) é capaz de erguer um carro por cinco segundos” você que pessoas ultrapassem. Adrenalina e outros fatores
permitem que pessoas ultrapassem suas capacidades físicas normais ou fiquem aquém delas – não é possível predeterminar cada um desses fatores sem que isso absorva grande parte do foco do jogo. Isso acaba levando as pessoas a discutirem e até brigarem ao invés de participarem na cena.

Isso também é entediante. Se decidir que Recursos Razoáveis (+2) podem comprar qualquer coisa que custe 200 peças de ouro ou menos, então estará removendo boa parte do potencial para emoção e drama. Com isso, toda vez que surgir um problema de Recursos, sempre haverá a questão da ação valer ou não 200 peças de ouro, independente do foco da cena. Isso também torna tudo uma questão de falha/sucesso em uma situação, o que significa que você deixa de precisar de boa razão para fazer um teste de perícia, o que, novamente, não é realista – quando pessoas gastam dinheiro, não é uma questão do quanto gastam, é mais uma questão do quanto ainda podem gastar.

Lembre-se, uma rolagem de perícia é uma *ferramenta narrativa*, a ser usada para responder a seguinte pergunta: “Posso resolver o problema X usando Y, neste exato momento?” Quando houver um resultado inesperado, use o seu senso de realismo e drama para explicar e justificar isso, usando nossas sugestões acima. “Ah, você falhou em sua rolagem de Recursos para chantagear o guarda? Acho que você passou mais tempo na taverna ontem à noite do que imaginava... espere, onde foi parar sua algibeira? E quem é aquele indivíduo andando sorrateiramente um pouco depois dos guardas? Ele acabou de piscar para você? Aquele canalha... certo, o que você faz?”

### Lidando Com Conﬂitos e Coisas Estranhas

As situações mais complicadas que você encontrará como Narrador serão conﬂitos, sem dúvida. Comparados a qualquer outra situação em *Fate*, conﬂitos são os que usam mais regras simultaneamente em um espaço curto de tempo. Eles requerem atenção a muitas coisas ao mesmo tempo - a posição relativa dos personagens, quem está contra quem, o estresse e consequências recebidos pelos PdNs e assim por diante.

Também é neles que seu cérebro apaixonado por filmes toma a liderança, especialmente se seu jogo incluir muitos conﬂitos cheios de ação.

As sequências de ação encontradas em filmes e afins nem sempre estão de acordo com a estrutura de turnos que o *Fate* possui, então pode ser difícil de comparar ao tentar visualizar o que está acontecendo. Às vezes, alguém tentará fazer algo absurdo que nem sequer por sua cabeça quando criou o conﬂito, deixando-o meio perdido sobre como lidar com a situação.

Aqui seguem algumas dicas para ajudá-lo a lidar com isso com graça e agilidade.

#### Afetando Múltiplos Alvos

Não há duvidas de que, ao jogar *Fate* por um certo tempo, alguém tentará afetar várias pessoas simultaneamente em um conﬂito. Explosões são o principal exemplo num conﬂito físico, mas não são de forma alguma o único – considere gás lacrimogêneo ou algum tipo de ataque atordoante de alta tecnologia. Também podemos estender isso para os conﬂitos mentais.

É possível usar Provocar, por exemplo, para estabelecer domínio sobre uma sala com sua presença ou Comunicação para fazer um discurso inspirador que afeta todos os ouvintes.

A maneira mais fácil de fazer isso é criar uma vantagem na cena, ao invés de em um alvo específico. Uma ***Sala Cheia de Gás*** pode afetar todos dentro dela e talvez não seja muito exagerado assumir que um ***Ambiente Animador*** pode ser contagiante. Nesse contexto, o aspecto representa uma desculpa para pedir um teste de perícia (usando uma ação de superar) para qualquer um na cena que tente se livrar disso. No geral não causará *dano*, mas tornará as coisas mais difíceis para quem for afetado.

> Esopo, ainda sem aprender a lição, entra num bar procurando confusão.
>
> “Quem é o maior sujeito aqui? O mais ameaçador?”, pergunta Léo a Amanda.
>
> “Isso é fácil”, responde ela. “Você imediatamente vê um monstro de 3 metros e meio de altura, claramente um guerreiro-peixe do planeta Gox. Ele é chamado de Shagas, o Testilhão, e está acompanhado por mais três carnívoros.”
>
> “Opa, esse parece bom. Vou matá-lo”, informa Léo.
>
> “Gostei. Seus três capangas te interceptam. Eles não têm 3 metros de altura, mas parecem saber o que estão fazendo”.
>
> Léo suspira, “Eu não tenho tempo para esses idiotas. Quero deixar claro que eles não devem se intrometer. Sabe, ﬂexionar os músculos e falar palavras de morte enquanto faço cara de mau. Quero que saibam que essa briga é entre mim e Shagas.”
>
> “Parece que você quer colocar um aspecto na cena. Faça uma rolagem de Provocar”.
>
> Léo rola um -3 e adiciona seu Razoável (+2) em Provocar e fica com um total Ruim -1. Ele precisaria de um Medíocre (+0), então falha. Porém, Amanda gostou da ideia de Esopo e Shagas se enfrentarem sem ninguém no caminho, então ela decide ceder a vitória na rolagem, mas a um custo.
>
> “Certo, o que vai ser?” Ela pergunta.
>
> Léo não hesita. Ele escreve uma consequência mental suave chamada ***Esse Cara é Maior do Que Eu Pensava...***
>
> “Legal. Eles olham para você e então para Shagas. Ele balança a mão com desdém e grunhe, 'Vão encontrar outro para matar, esse é meu'".

As coisas podem ficar complicadas quando alguém quer filtrar alvos específicos, ao invés de afetar uma zona ou cena inteira. Quando isso acontecer, divida o seu resultado total contra todos os alvos, que se defenderão normalmente. Quem falhar na defesa ou recebe estresse ou ganha um aspecto, dependendo do que estava tentando fazer (nota: ao criar vantagem para colocar um aspecto mais de um alvo, você ganha uma invocação grátis para cada um).

> Fräkeline está preparando seu chicote de energia, pois sente que os três inimigos a sua frente a escolheram como alvo – provavelmente por culpa de Esopo e seu talento para entrar em brigas.
>
> Fräk usa sua perícia Lutar, e se sai extremamente bem, obtendo um resultado Épico (+7).
>
> Ela sabe que quer atingir um deles em cheio, então opta por dividir seu ataque em Excepcional (+5), Regular (+1) e Regular (+1). Isso dá um total de +7 que é o valor de sua rolagem. Agora Amanda tem que defender pelos três PdNs.
>
> O primeiro se defende com um resultado Medíocre (+0) e recebe 5 de estresse. Esse é um PdN sem importância (veja abaixo), então Amanda decide que ele saí da luta e o descreve gritando enquanto é golpeado em cheio pelo chicote.
>
> O segundo se defende com um resultado Razoável (+2), acima da rolagem do ataque. Ele continua adiante, destemido.
>
> O terceiro consegue uma rolagem Medíocre (+0), recebendo apenas um ponto de estresse. Amanda marca sua única caixa de estresse e o descreve sacrificando seu escudo de aço no processo.

Atacar uma zona inteira ou todos em uma única cena é algo que precisa ser analisado de acordo com as circunstâncias, como qualquer outro [uso imprevisto de perícias](../criando-a-oposicao/#dano-e-excedentes). Dependendo das circunstâncias do cenário, isso pode ser algo totalmente normal de ser feito (por exemplo, porque todos usam granadas e explosivos), mas pode ser impossível o requerer uma façanha. Não são necessárias regras especiais, desde que isso seja justificável – você rola os dados para atacar e todos na zona defendem normalmente. Dependendo das circunstâncias, é até possível que você precise se defender de sua própria rolagem, se estiver na mesma zona do ataque!

------

### Forçando Múltiplos Alvos

Uma nota rápida: jogadores que queiram forçar para vencer um conﬂito não podem simplesmente afetar quantos alvos quiserem, seja por um aspecto ou vários que justifiquem a tentativa. Um jogador deve gastar um ponto de destino para cada alvo que queiram forçar. Um ponto de destino força um indivíduo, ponto final.

------

#### Riscos Ambientais

Nem todos os participantes de um conﬂito são PJs e PdNs. Muitas coisas não autoconscientes podem ser ameaças em potencial aos PJs ou impedi-los de cumprirem seus objetivos, seja um desastre natural, seja uma armadilha ou segurança de alta tecnologia.

Então o que acontece quando os PJs vão de encontro a algo que não é um personagem?

Simples: trate-o como um personagem (esta é a Regra de Bronze do *Fate*: v*ocê pode tratar tudo como um personagem*. Abordaremos diversas formas de trabalhar isso no capítulo *Extras*, mas vamos manter a conversa no tópico por enquanto).

-  Esse risco é algo que pode ferir os PJs? Dê uma perícia a ele e ataque como um oponente normal.
-  É mais uma distração ou inconveniência do que uma ameaça real? Deixe que crie aspectos.
-  Será que ele possui formas de descobrir os aspectos dos personagens? Dê-lhe uma perícia para isso.

Em contrapartida, deixe que os PJs usem suas perícias contra a ameaça como se fosse um oponente. Um sistema de segurança automatizado pode ser vulnerável a “ataques” com a perícia Roubo de um personagem ou uma armadilha pode ser evitada vencendo uma disputa de Atletismo. Se fizer sentido que o risco precise de muito esforço para ser superado, crie caixas de estresse para ele e deixe que receba uma ou duas consequências suaves.

Em outras palavras, use qualquer recurso que faça sentido na narração – se o fogo for grande demais para os PJs apagarem, a cena deve ser focada em evitar ou escapar do problema, funcionando como um desafio.

> Fräkeline, Esopo e Bandu, seguindo as pistas obtidas por Fräk a respeito dos Bruxos de Moondor, viajam até as profundezas de Dignidade, um planeta esquecido nos confins do universo, “onde nada digno habita”. O local é uma grande fortaleza, aparentemente abandonada, mas cheia de armadilhas e seres perigosos.
>
> Eles descem ao último piso do sistema de galerias e o encontram cheio de fletes de escuridão que se contorcem como serpentes, absorvendo a luz por onde passam. Bandu faz uma rolagem de sabedoria e Amanda lhe diz que são espíritos mágicos famintos — não entidades separadas, mas manifestações puras da fome, prontas para devorar qualquer coisa com o que entram em contato. Ele joga uma pedra no corredor e observa enquanto os pequenos tentáculos reduzem-na a cinzas.
>
> “Acho que falo por todos quando digo ‘ai’”, diz Michel.
>
> Ele pergunta sobre a possibilidade de banir o monstro.
>
> Amanda balança a cabeça. “Você está no lugar de origem dessas coisas, elas estão por todo canto. Mesmo com centenas de anos sem alimento, eles continuam ali. Você pode, no entanto, usar seus poderes mentais para tentar afastá-los enquanto atravessam.”
>
> Maira diz, “Estou disposto a encará-los. Vamos lá”.
>
> Amanda decides que mesmo podendo colocá-los em um conﬂito direto, será mais fácil e rápido lidar com a situação como um desafio. Ela diz que, para passar pelas sombras, cada um deles precisará de um teste resistido de Vontade contra as sombras para resistir à sua aura mágica e Furtividade para passar. Bandu pode realizar uma rolagem de Conhecimentos para tentar afastar os tentáculos usando seus poderes. Além disso, ela diz que os tentáculos podem oferecer oposição ativa contra cada tentativa e que a rolagem de Vontade será considerada um ataque. Os três cerram os dentes e começam a andar pelo túnel...

### Lidando com Aspectos

Assim como as perícias e façanhas, todo o capítulo Aspectos e Pontos de Destino foi desenvolvido para ajudá-lo a julgar o uso de aspectos durante o jogo. Como Narrador, é importante saber administrar o ﬂuxo de pontos de destino entre os jogadores e o Narrador, criando oportunidades para gastar esses pontos livremente, gerando sucessos situações incríveis, assim como inserindo complicações para que os jogadores recebam mais pontos.

#### Invocações

Por isso recomendamos não exigir padrões muito específicos quando um PJ quiser invocar um aspecto – a ideia é que os pontos sejam gastos para manter o ﬂuxo do jogo, enquanto se houver muitos requisitos os jogadores se sentirão desencorajados de gastar livremente.

Por outro lado, sinta-se livre para pedir esclarecimentos se não entender bem o que o jogador está tentando fazer, no sentido de como o aspecto se relaciona com os acontecimentos do jogo. Às vezes o que parece óbvio para uma pessoa não é para outra, então cuidado para não deixar sua vontade de distribuir pontos passar por cima da narrativa. Se o jogador estiver com dificuldade em justificar a aplicação de um aspecto, peça que elabore melhor a ação ou expresse suas ideias de outra forma.

Você também pode se deparar com jogadores que se perdem na abrangência dos aspectos – eles não os invocam porque não têm certeza se estão passando dos limites ao aplicar um aspecto de uma certa maneira. Quanto melhor você se certificar que todos entendem o que um aspecto significa antes da partida, menos terá que lidar com isso depois. Para fazer um jogador pensar em invocar aspectos, sempre pergunte se ele está satisfeito com o resultado da rolagem ("Isso é um Ótimo. Quer deixar assim, ou tornar ainda melhor?"). Deixe claro que invocar um aspecto é quase sempre uma opção em qualquer rolagem, para incitar a vontade de falar sobre outras possibilidades. Eventualmente, se houver um diálogo consistente, tudo deve correr bem.

#### Forçando

Durante o jogo, procure por oportunidades de forçar os aspectos dos PJs nos seguintes momentos:

-  Sempre que um mero sucesso num teste de perícia parecer sem graça;
-  Sempre que o jogador possuir apenas um ou nenhum ponto de destino;
-  Sempre que alguém tentar fazer algo e você tiver uma ideia envolvendo um aspecto para aquilo não dar certo.

Lembre-se que há basicamente dois tipos de invocação no jogo: baseadas em decisões, quando alguma complicação ocorre como resultado da ação de um personagem; e baseadas em eventos, quando alguma complicação ocorre simplesmente porque o personagem está no lugar errado e na hora errada.

Dos dois, o mais proveitoso será forçar baseando-se em eventos - já é seu trabalho decidir como o mundo responde aos PJs, então haverá bastante liberdade para que você crie coincidências infelizes em suas vidas. Na maioria das vezes, os jogadores aceitarão sem problemas ou pedirão ajustes pequenos.

Forçar a partir de decisões pode ser um pouco complicado. Procure evitar sugerir decisões aos jogadores e foque em responder às suas decisões com complicações. É importante que os jogadores mantenham o senso de autonomia sobre o que os seus personagens fazem e dizem, então não tome decisão por eles. Se os jogadores estiverem interpretando seus personagens de acordo com seus aspectos não será difícil conectar as complicações propostas a eles.

Durante o jogo também é bom deixar claro quando determinada ação de forçar é “fixa”, o que significa que não há como escapar sem pagar um ponto de destino. Isso não ocorre quando jogadores tentam forçar aspectos sobre si mesmos, pois eles estão querendo ganhar pontos afinal de contas. Quando o Narrador propõe forçar um aspecto, é preciso dar espaço para os jogadores negociarem sobre a complicação antes de tomar uma decisão final. Seja transparente nesses assuntos – deixe-os saber quando a fase de negociações acabou.

##### Forçando de Forma Simples

Para forçar um aspecto de forma eficiente é preciso ter o cuidado de propor complicações que possuam peso dramático adequado. Fique longe de coisas superficiais demais que não afetam o personagem de fato a não ser para dar mais cor à cena. Se você não conseguir pensar de uma forma imediata e tangível de afetar o jogo para aquela complicação, você provavelmente precisará virar a mesa. Se ninguém disser algo como “agora ferrou!” o nível provavelmente precisa ser elevado. Não basta que alguém fique bravo com o PJ — a pessoa precisa ficar brava *e disposta a fazer algo a respeito na frente de todos.* Não basta que um parceiro de negócios os deixe de fora — ele os deixa de fora *e conta aos seus associados para não fazer mais negócios com eles*.

Além disso, tenha em mente que alguns jogadores tendem a dar sugestões fracas para forçar aspectos quando querem pontos de destino, pois não querem prejudicar seus personagens. Sinta-se livre para modificar de forma a complicar mais as coisas se a proposta inicial não é dramática o bastante.

##### Encorajando os Jogadores a Forçar

Como cada personagem tem cinco aspectos, é difícil ser o único responsável por forçar durante a partida, porque há muita coisa para se lembrar e acompanhar. Os jogadores precisam querer procurar momentos adequados para forçar aspectos sobre seus próprios personagens.

Situações sugeridas podem ajudar muito no processo de criar esse hábito nos jogadores. Ao perceber uma oportunidade interessante para forçar, ao invés de propor diretamente, faça uma pergunta que sugira a ideia. “Então, você está no baile real e possui o aspecto ***Não Entendo Absolutamente Nada***. Léo, você acha que algo poderia dar errado para seu personagem?” Deixe que o jogador tenha o trabalho de gerar a complicação e então premie com o ponto de destino.

Também relembre os jogadores de que podem forçar aspectos sobre os PdNs, se souberem algum de seus aspectos. Dê apenas uma sugestão incompleta como acima quando um PdN estiver prestes a tomar uma decisão e peça que os jogadores preencham os detalhes. “Então, você sabe que o Duque Orsin possui ***Confiança Excessiva...*** Você acha que ele sairá ileso do torneio de justa? Está disposto a pagar um ponto de destino para ver isso acontecer?” Seu objetivo principal deve ser recrutar os jogadores como parceiros para criar drama, ao invés de fazer tudo sozinho.

- [« Durante a Criação do Jogo](../durante-a-criacao-do-jogo/)
- [Criando a Oposição »](../criando-a-oposicao/)

