---
title: Enganar
layout: default
---

### Enganar

Enganar é a perícia para mentir e ludibriar as pessoas.

- `O`{: .fate_font} **Superar:** Use Enganar para blefar e se livrar de alguém, para fazer alguém acreditar em uma mentira ou para conseguir algo de alguém por fazê-lo acreditar em uma de suas mentiras. Para PdNs genéricos só é necessária uma rolagem para superar, mas para PJs e PdNs importantes é necessário uma disputa contra Empatia. O vencedor da disputa pode justificar a vitória por adicionar um aspecto de situação em seu alvo, se acreditar na sua mentira puder ser útil em alguma cena futura.
Enganar é a perícia que você usa para se disfarçar ou descobrir o disfarce de alguém. Você precisa ter tempo e material necessários para conseguir o efeito desejado (nota: isso funcionou bem no cenário de *Caroneiros de Asteroide*, mas em outros jogos pode ser que Enganar não seja adequada e seja necessária uma façanha). Você pode usar enganar para realizar pequenos truques com as mãos e desorientações.
- `C`{: .fate_font} **Criar Vantagem:** Use Enganar para criar distrações momentâneas, histórias inventadas ou impressões falsas. É possível fintar em uma luta de espadas, fazendo o oponente ficar ***Desbalanceado***. O truque “o que é aquilo!” pode criar uma ***Chance de Escapar***. Você poderia inventar uma ***História Falsa do Nobre Rico*** quando participar de um baile real, ou enganar alguém para fazê-lo revelar um de seus aspectos ou outra informação.
- `A`{: .fate_font} **Atacar:** Enganar é uma perícia que cria uma grande variedade de oportunidades vantajosas, mas não causa dano diretamente.
- `D`{: .fate_font} **Defender:** Você pode usar Enganar para espalhar informações falsas e despistar um uso de Investigação contra você ou então para se defender de tentativas de usar Empatia para descobrir suas intenções verdadeiras.

#### Façanhas Para Enganar

-  **Mentiras Sobre Mentiras:** +2 para criar uma vantagem de Enganar sobre alguém que já tenha acreditado em uma de suas mentiras anteriormente nessa sessão.
-  **Jogos Mentais:** Você pode usar Enganar no lugar de Provocar para realizar ataques mentais, desde que consiga inventar uma mentira bem elaborada como parte do ataque.
-  **Um Indivíduo, Muitas Faces:** Ao conhecer alguém novo, você pode gastar um ponto de destino para declarar que já conhecia essa pessoa antes, mas sob determinado disfarce. Crie um aspecto de situação para representar sua história enganosa e você pode usar Enganar no lugar de Comunicação sempre que interagir com essa pessoa.

------

### Perícias Sociais e Outros Personagens

Muitas das perícias sociais possuem ações que alteram o estado emocional de outros personagens ou os fazem aceitar algum fato da história (ao acreditar em suas mentiras, por exemplo).

Um sucesso ao usar uma perícia social *não* dá o direito de forçar outro personagem a agir de forma contrária a sua natureza, ou como a pessoa que o controla o vê. Se outro PJ for afetado por uma de suas perícias, o jogador sugere como seu personagem responde. Ele não pode anular sua vitória, mas pode escolher como ela funcionará.

Então, você pode ser bem-sucedido em Provocar o personagem com gritos e intimidações na intenção de amedrontá-lo e fazê-lo hesitar, criando uma vantagem, mas se o outro jogador não imaginar o seu personagem agindo dessa forma, vocês devem discutir alguma alternativa – talvez você o irrite tanto que ele fique desequilibrado, ou você o constrange ao fazê-lo passar vexame em público.
Não importam os detalhes, desde que você ganhe sua vantagem. Use-a como uma oportunidade de criar uma história com outra pessoa, ao invés de excluí-la.

------

- [« Empatia](../empatia/)
- [Furtividade »](../furtividade/)
