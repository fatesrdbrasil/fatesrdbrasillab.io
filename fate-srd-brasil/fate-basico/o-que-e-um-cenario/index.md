---
title: O Que É Um Cenário?
layout: default
---

## O Que É Um Cenário?

Como mencionado em [*Estruturando o Jogo*](../estruturando-o-jogo/), um cenário é uma unidade de tempo de jogo geralmente com duração de três ou quatro sessões e é formado por uma certa quantidade de cenas. O fim de um cenário deve desencadear um marco significativo, permitindo que os seus PJs se aprimorem.

------

### Criando Um Cenário, Passo A Passo

 -  Crie os problemas
 -  Crie perguntas relacionadas à história
 -  Estabeleça a oposição
 -  Organize a primeira cena

------

Em um **cenário**, os PJs enfrentarão e tentarão resolver algum tipo de problema (ou problemas) grande, urgente e com final em aberto. O Narrador geralmente dá início ao cenário apresentando esse problema aos jogadores, e as cenas subsequentes deixam os PJs escolherem como lidar com ele, seja procurando informações, adquirindo recursos ou indo direto à fonte do problema.

Ao longo do caminho, haverão também PdNs que se oporão aos objetivos dos PJs, interferindo em suas tentativas de resolver o problema. Isso pode ser de forma mais direta, do tipo “dois caras armados” derrubando a porta para matar todos ou simplesmente alguém com interesses diferentes dos PJs querendo negociar para que lidem com a questão de outra forma.

Os melhores cenários não possuem um final "certo". Talvez os PJs não resolvam o problema ou o façam de forma que crie más repercussões. Talvez consigam fazer tudo com perfeição. Talvez eles contornem o problema ou mudem a situação para minimizar o seu impacto. Você não saberá até jogar.

Assim que o problema for resolvido (ou não puder ser mais resolvido), o cenário termina. A sessão seguinte dará início a um novo cenário, que pode estar diretamente relacionado ao cenário anterior ou apresentar um problema inédito.

- [« Cenas,Sessões e Cenário](../cenas-sessoes-e-cenario/)
- [Crie Problemas »](../crie-problemas/)

