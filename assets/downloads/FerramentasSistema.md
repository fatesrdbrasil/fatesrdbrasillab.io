# Prefácio - ”Um” E Não “O”

Este livro é um conjunto de algumas Ferramentas do Sistema Fate, não As Ferramentas do Sistema Fate.

O que isso significa?

Ferramentas do Sistema foi inicialmente criado como uma das metas do financiamento coletivo do Fate Sistema Básico, realizado pela Evil Hat através do Kickstarter. A ideia original era conter apenas o sistema de magia do livro, mas à medida que o financiamento foi crescendo, o livro também cresceu. Seguindo-o de perto vinham inúmeros pedidos do que deveria ser acrescentado ao livro mas que ultrapassavam completamente o que era possível.

Foi uma grande honra saber que o público nos consideravam capazes de desenvolver tantos tópicos diferentes. Impossível, talvez, mas uma honra. Dito isso, sabíamos que precisávamos atender todos esses pedidos em espírito, mesmo se não pudéssemos atendê-los de forma plena. Assim, expandindo para além de nosso suplemento sobre magia, nasceu o Ferramentas
do Sistema.

Fazendo jus ao nome, nossas Ferramentas não oferecem tudo que você quer de imediato, mas sim um conjunto de ideias, abordagens e outras questões interessantes que podem lhe levar aonde desejar. Colocando de outra maneira, quando nos pediram a lua, decidimos ensinar várias formas de construir foguetes. (Nós construímos foguetes agora. Foguetes são demais.)

Quando estiver procurando a medida certa para seu jogo, não se atenha às aparências. Digamos que algo aqui fale sobre tratar gigantes como se fossem mapas ou um grupo de vários personagens ao invés de um único personagem ”aumentado”. Embora a embalagem diga “aqui está o método para lidar com monstros gigantes”, isso é apenas a forma como a ideia foi apresentada. Isso também lhe traz um método para lidar com os “chefes de fase” em geral ou uma operação de ataque contra um satélite orbital da morte. Da mesma forma, maneiras de abordar super-heróis são abordadas em uma variedade de conceitos e sessões dentro do livro, mas apenas uma é rotulada como “super-heróis”.

Isso não quer dizer que todas as ferramentas possíveis estão presentes aqui, mas há ideias e bases sólidas suficientes para que você certamente encontre algo que o guiará. Além disso, se tiver alguma ideia nova, por favor não a guarde para si. Há muitos lugares onde conversas legais e excitantes estão acontecendo neste momento e você poderia participar com suas ideias, independente se for em redes sociais, grupos dedicados ao Fate Sistema Básico ou qualquer lugar dentro ou fora da internet. O público do Fate forma uma comunidade forte, cheia de pessoas inteligentes, um grupo inteiro de indivíduos que são ainda mais espertos do que as pessoas que criaram o sistema em primeiro lugar.

Acima de tudo, Ferramentas do Sistema é um guia de treinamento para o seu cérebro. Neste livro você encontrará dezenas de formas de adicionar algo novo, algo a mais, algo diferente à sua partida de Fate. Em alguns casos será exatamente o que você precisava. Na maioria das vezes, será um ponto de partida para outras ideias e ajustes. Mãos à obra. Você possui uma coleção incrível de ferramentas para trabalhar. Agora vejamos o que é capaz de fazer com elas!


> Fred Hicks
> 15 de julho de 2013

# Capítulo 1 - Porque Estamos Aqui

Pense no Fate como uma máquina, construído para produzir um resultado específico. Como muitas máquinas, Fate pode ser ajustado para produzir outros resultados. Possui um sistema de indicadores, uma rede de regras conectadas que podem ser ajustadas e modificadas para alcançar o resultado que você procura. É robusto, flexível e, o mais importante, adaptável.

Qual seria o sentido de “adaptável” neste caso? Em primeiro lugar, significa que você pode mudar as regras. Isso é possível em qualquer jogo

- se estiver jogando Banco Imobiliário e permitir que alguém receba dinheiro ao cair em “Parada Livre”, seria uma adaptação. Isso altera a mecânica da economia do jogo deixando a partida mais favorável, uma vez que fica mais difícil perder no jogo por falta de dinheiro. Fate é assim
- ele pode ser alterado. Quer que o Narrador possua mais pontos? Que os jogadores tenham perícias mais altas? Que Aspectos funcionem de outro jeito? Tudo isso é possível com um pouco de esforço de sua parte.

O segundo aspecto dessa adaptabilidade é que Fate não cria resistência contra suas alterações. Na verdade, o sistema é adaptável o suficiente para que pequenas mudanças sejam feitas sem comprometer as regras e até mesmo alterações maiores exijam apenas alguns ajustes em outras partes do sistema. Não é apenas que você pode alterar o Fate, mas que é fácil alterá-lo – se você estiver disposto a pôr a mão na massa.

Para obter o clima desejado, algumas alterações podem ser necessárias. Fate é um sistema ajustável e o Fate Sistema Básico apresenta o sistema padrão de jogo. Talvez essas mecânicas não sejam apropriadas para o seu jogo. Talvez seja necessário fazer ajustes ou mesmo adicionar novos elementos, mas você não esteja seguro sobre quais mudanças fazer ou quais outras partes do sistema essas mudanças afetarão. Não se preocupe, é por isso que estamos aqui.

Ferramentas do Sistema é um livro sobre como alterar o Fate, o que acontece quando você o faz e o que pode fazer quando começar a brincar com o funcionamento das coisas. Se isso é algo que você gosta, então este livro foi feito para você.

## Regras Vs Deliberações


Claro, você pode alterar as regras, mas deveria? Às vezes a resposta é sim. Se estiver jogando uma partida de super-heróis, precisará adicionar alguns poderes. Se seu jogo for sobre corridas de rua, é bom ter algumas regras que
tratem de veículos. 

Porém, às vezes não é necessária uma nova regra. Nesses casos você só precisa de uma deliberação.

Uma deliberação é uma decisão tomada pelo seu grupo de jogo – normalmente sob orientação do Narrador – sobre como algo funciona no jogo. Deliberações cobrem casos especiais que não são especificados nas regras, casos que exigem um pouco de interpretação. Uma nova regra, por outro lado, é uma mudança em um ou mais dos subsistemas existentes no jogo ou a adição de algo novo. Enquanto uma deliberação é uma interpretação de como o jogo funciona, uma nova regra é uma mudança no funcionamento do jogo.

Por exemplo, quando ao dizer a um jogador que o seu personagem não pode dar um tiro longo com seu rifle porque o barco onde ele está balança muito, você está deliberando. Ao definir que ninguém pode dar um tiro longo de um barco, você está criando uma nova regra. Notou a diferença? Um afeta a situação atual e pode influenciar outras situações, enquanto a outra afeta todas as situações do tipo.

Então quando usar uma ou outra? Adicione uma nova regra para algo que acontece com frequência. Sempre que se deparar com algo problemático ou desejar fazer algo inovador, e se isso acontecer com frequência, é uma boa ideia criar uma nova regra. Use deliberações se não tiver certeza que a situação ocorrerá novamente ou se acha que é um caso raro.

Se criar uma regra nova para cada situação, acabará com tantas regras para administrar que acabará se perdendo. Por outro lado, se criar regras para casos isolados, terá a liberdade de alterá-las depois. Claro, é possível alterar uma regra depois, mas alguns jogadores podem não gostar disso – e com razão! Pior, você pode criar regras ainda mais complicadas de lembrar e aplicar, especialmente se alterá-las o tempo todo.

Eis o segredo – deliberações podem se tornar novas regras. Se você fizer uma deliberação a respeito de tiros com rifles em um barco e notar que essa situação está se repetindo, transforme-a em uma regra. Se for necessário deliberar diversas vezes sobre a mesma coisa, seus jogadores provavelmente se lembrarão disso, diminuindo as chances de uma regra ser esquecida.

## A Regra de Bronze

Há mais uma pergunta a ser feita para si mesmo antes de criar uma regra para algo — será que eu posso representar isso com a [Regra de Bronze][]? 

No Fate, você pode tratar praticamente qualquer coisa como um personagem. Sua arma? Claro. A tempestade que se aproxima? Com certeza. A cena em si? Porque não? Qualquer coisa pode receber aspectos, perícias, façanhas e caixas de estresse, e eis
o que torna essa técnica ainda mais incrível — não precisam ser as mesmas coisas que os Pjs recebem. Não faz sentido que
a tempestade tenha as perícias Lutar e Atletismo, mas que tal Frieza e Nevasca? Sua arma não precisa de caixas de estresse
físico e mental, mas que tal caixas de estresse de munição? As cenas também já estão repletas de aspectos!

Se puder representar algo novo como um personagem, será ainda mais fácil do que criar uma regra completamente nova. Nem tudo funciona bem dessa forma e há coisas que você pode não querer representar dessa maneira, mas é uma ferramenta
poderosa que pode ser aplicada a uma grande variedade de situações.

Há uma outra forma de abordar esta técnica – novas regras para personagens podem ser representadas usando características já existentes de personagens. Você pode representar a magia através de perícias, poderes usando aspectos ou façanhas
e ser corrompido por sedutoras forças ancestrais usando estresse.

# Capítulo 2 - Aspectos

## Aspectos

Jogar uma partida de Fate é bem simples. Você precisa: Aspectos são o que fazem Fate _rodar_. São o método mais claro e interessante para descrever quem é seu personagem e formam a base para o manuseio dos pontos de destino. Invocar um aspecto lhe dá um certo controle sobre o seu destino, uma forma de abrandar os caprichos dos dados. Aspectos forçados são os melhores amigos do Narrador no que diz respeito a complementar a história e criar situações interessantes, injetando drama nas cenas ou simplesmente complicando os planos dos jogadores um pouco mais.

### Invocar por um efeito

O Fate Sistema Básico fala um pouco sobre este conceito, explicando que “invocar por um efeito” não passa de uma forma extravagante de forçar. Isso é verdade, mas talvez não seja o significado completo desse termo.

Quando invoca por um efeito, você gasta um ponto de destino – ou usa uma invocação grátis – para criar um efeito mecânico específico, algo diferente do que um aspecto comum é capaz de fazer. Ao criar um aspecto, observe-o e decida se é preciso ou não que algum efeito atrelado a ele. Talvez o seu mago possa invocar **_Sintonia Com a Terra_** para evitar cair ou perder o equilíbrio ou talvez o seu detetive psíquico use **_Espionagem Mental_** para ler os pensamentos superficiais de alguém. 

Mecanicamente, o efeito de um aspecto deve valer o ponto de destino gasto – o equivalente a duas tensões de potência, assim como qualquer outro efeito de invocar um aspecto. O efeito de um aspecto deve realizar algo, como nos exemplos acima, em vez de fornecer apenas um bônus. Uma invocação normal de um aspecto já concede um bônus, então você não precisa de um efeito especial que faça a mesma coisa. Um efeito de aspecto é um pouco semelhante a ter uma façanha extra de exceção às regras que sempre precisa ser paga, tanto do ponto de vista do que o efeito consegue causar quanto da complexidade acrescentada ao personagem.

--- 

#### QUANTOS EFEITOS?


Falando em complexidade de personagem, Narradores precisam decidir quantos desses efeitos cada personagem do jogador possuirá. A forma mais simples de usar esta regra é permitir que cada personagem adicione um efeito especial ao seu conceito, já que esse é provavelmente o aspecto de maior peso e que mais define o personagem. Você pode fornecer mais efeitos de aspectos aos PJs, mas fazer isso seria como permitir muitas façanhas – nem todos serão usados e a quantidade de opções pode deixar o jogo travado. 

PdNs podem possuir efeitos de aspectos também, mas é melhor dá-los apenas aos PdNs principais e talvez a algum PdN de suporte muito importante. 

Em ambos os casos é melhor limitar o número de efeitos de as-
pectos por personagem a um ou dois no máximo.

---

Em termos de custo, não há problema em permitir que os PJs tenham um ou dois desses efeitos de graça. Eles possuem o mesmo poder de uma invocação normal e são mais situacionais, fazendo com que não sejam usados com tanta frequência. Narradores, se for passar disso vocês estarão em seu direito de pedir aos jogadores que gastem sua recarga para adquirir efeitos adicionais.

**Aspecto: _Sempre Armado_**

**Efeito:**  Gaste um ponto de destino para revelar uma arma pequena e ocultável – como uma faca ou pistola pequena – guardada secretamente em alguma parte de seu corpo, mesmo se tiver sido desarmado recentemente.

**Aspecto: _Ninja do Clã da Serpente_**

**Efeito:** Gaste um ponto de destino para desaparecer repentinamente, mesmo se houver pessoas olhando para você. Isso lhe dá motivo para realizar uma rolagem de Furtividade na tentativa de se esconder. 

**Aspecto: _Explorador Élfico de Olhos Aguçados_**

**Efeito:** Gaste um ponto de destino para conseguir ver a grandes distâncias – até dois quilômetros de distância – claramente e em grande detalhe, mesmo à noite.

### Invocação Proporcional

Ao invocar um aspecto, ele lhe concede um bônus de +2 ou uma nova rolagem dos dados. Isso é suficiente na maioria das vezes, mas não cria vantagens mecânicas por invocar um aspecto que se aplique particularmente bem a uma determinada situação, assim como não dissuade os jogadores de invocar aspectos que praticamente não se aplicam à situação. Nestes casos, outra opção seria a invocação proporcional.

Invocações proporcionais dividem aspectos em três categorias: invocações questionáveis, relevantes e perfeitas.

**Invocações questionáveis** mal se aplicam à situação do momento, como invocar **_Forte Como um Touro_** em um concurso de bebidas ou usar **_Lixo Empilhado_** para amenizar sua queda da janela do segundo andar. Se uma invocação for questionável, você recebe apenas a oportunidade de rolar novamente. Isso significa que nunca será possível melhorar sua rolagem além do resultado nos dados, mas será possível usar esta invocação para tentar melhorar uma rolagem desastrosa. Significa também que invocações questionáveis múltiplas normalmente não serão tão úteis.

Ao fazer uma **invocação relevante** , você está invocando algo que claramente se aplica à situação atual sem a necessidade de muitas justificativas. Talvez esteja usando **_Gatilho Mais Rápido do Oeste_** em um duelo ou esteja escondido atrás de um Muro de Concreto em busca de proteção. Isso concede o mesmo que uma invocação normal — um +
ou rolar novamente.

Uma **invocação perfeita** é aquela que faz todos dizerem “Incrível!” ou rir e concordar de forma entusiasmada quando você a anuncia. Ela se encaixa perfeitamente à situação, a escolha certa para o momento. Há pouca coisa que pode motivar o seu personagem de forma tão intensa quanto invocar **_Udru Khai Matou Minha Família_** quando está tentando pegá-lo enquanto ele foge. Quando faz uma invocação perfeita, você é _automaticamente bem-sucedido_ em sua ação, sem a necessidade de rolagem. Se invocar o aspecto após a rolagem, apenas ignore a rolagem. Se for preciso saber quantas tensões conseguiu, assuma que obteve uma. Isso significa que você pode invocar um segundo aspecto para ser bemsucedido com estilo, se fizer muita diferença.

A única exceção é o ataque. Quando você faz uma invocação perfeita de um aspecto em um ataque, não é preciso rolar os dados. Em vez disso, seu ataque é definido como seu nível de perícia +3. Assim, se estiver atacando com Lutar Bom (+3) e fizer uma invocação perfeita, seu esforço terá sido Fantástico (+6), valor que seu adversário terá de superar. Se você já tiver rolado os dados, a invocação perfeita irá _melhorar_ sua jogada, adicionando +1 a ela.

### Destruindo Aspectos de Situação

Alguns aspectos de situação sugerem destrutibilidade ou uso limitado, como __*Pilha de Botijões*__ ou __*Colunas de Suporte Apodrecidas*__. Eles podem ter um efeito narrativo, mas não necessariamente um efeito mecânico. Se você gostaria que tais aspectos possuíssem uma diferença mecânica de aspectos como ___Escuro Como o Breu___ ou ___Cobertura Por Toda Parte___, pode permitir que os PJs finalizem tais aspectos de situação.

Quando um jogador destrói um aspecto de situação, ele declara sua intenção e explica como ao usar esse aspecto ninguém mais poderá usá-lo novamente. Se puder fazer isso de forma que agrade a todos, ele poderá invocar esse aspecto uma vez gratuitamente.

Uma vez que tenha invocado o aspecto, ele desaparece e a situação muda para a pior – ou, ao menos, se torna mais perigosa. Destruir um aspecto de situação cria outro aspecto de situação que representa como o antigo aspecto foi destruído e como isso piorou as coisas. Esse último ponto é a chave — é preciso destruir coisas para complicar outras. Não seria justo destruir o aspecto ___Pilha de Botijões___ e substituí-lo por ___Marcas Chamuscadas___. Substitua-o por algo grande, incrível e destruidor, como ___Todo o Prédio Está em Chamas!___ ou ___O Celeiro Está Desmoronando!___. O novo aspecto de situação sempre deve deixar as coisas mais tensas para todos, e deve ser uma ameaça iminente.

## Aspectos de Gênero

Há momentos em que você pode querer reforçar algo sobre o gênero do jogo. Criar questões no jogo que ressaltem os temas do gênero é uma boa maneira, e você poderá ter uma boa diversidade de material para encorajar os seus jogadores a criar personagens ligados ao gênero. No entanto, às vezes você quer algo mecanicamente único.

Ao usar aspectos de gênero, você está alterando o funcionamento dos aspectos a fim de reforçar o gênero escolhido. Às vezes essas mudanças serão aplicadas a todos os aspectos do jogo, enquanto outras vezes essas mudanças serão restritas a um grupo específico de aspectos ou um único aspecto. As maneiras pelas quais você pode alterar os aspectos são variadas demais para serem listadas aqui, então optamos por providenciar alguns exemplos.

### Aspectos Menos Poderosos

Esta é uma mudança radical de todos os aspectos do jogo, e é uma ótima opção para emular gêneros onde os PJs são mais fracos que as forças que eles enfrentam, como horror ou noir. As mudanças são simples: ao invocar um aspecto, você pode rolar novamente os dados ou pode receber +2 em sua rolagem, mas não pode escolher +2 mais de uma vez caso invoque mais de um aspecto. Isso significa que um aspecto pode apenas melhorar um pouco o esforço máximo de um PJ. Isso fará com que a falha seja mais comum, o que indica que a falha precisa sempre ser interessante e precisa mover a ação adiante, assim como um sucesso. 

Se quiser permitir que PJs recebam bônus múltiplos de aspectos em certas situações, considere ligar isso a façanhas, como:

**Sentidos Aguçados:** Ao invocar um aspecto em uma rolagem de Notar, você pode invocar quantos aspectos desejar, contanto que tenha pontos de destino suficientes para pagar o custo.

### Aspectos de Missões

Esta é uma mudança pequena, mas uma que pode reforçar os objetivos compartilhados dos PJs. Funciona bem em cenários de fantasia ou em qualquer outro cenário onde os PJs ajam em grupo para alcançar algum tipo de objetivo maior, como matar o dragão ou recuperar o vilarejo tomado por bandidos. 

Sempre que os PJs aceitarem uma missão, o grupo trabalhará em conjunto para criar um bom aspecto que a represente. Por exemplo, se os PJs estão tentando salvar a vila de um rei tirano, o grupo poderia criar o aspecto de missão **_Martim Coração-Partido Deve Ser Detido!_**. Este aspecto pode ser invocado por qualquer PJ do grupo, assim como pode ser forçado como se estivesse na planilha de personagem, concedendo um ponto de destino a todos. 

Se os PJs resolverem o aspecto da missão, terão alcançado um marco. A importância do marco depende da dificuldade e da duração para alcançar o objetivo. Veja a página 234 do Fate Sistema Básico para mais informações sobre marcos.

## Aspectos De Equipamento


Equipamentos ficam em segundo plano no Fate. Suas perícias ganham foco no que diz respeito a mostrar o que você pode fazer; o equipamento está lá para permitir que você as use. Você possui um alto nível de Atirar? Então tem uma arma! Priorizou Condução? Então possui um carro! Há coisas que podem ser assumidas e que não possuem efeitos mecânicos. O capítulo de Extras no Fate Sistema Básico explica como adicionar mecânica de jogo a equipamentos, coisas como potência de armas ou seus próprios aspectos ou perícias. No entanto, há um meio termo que não requer redução de recarga e permite que todos os equipamentos importantes tenham importância significativa.

Se quiser seguir por esse caminho, mantendo equipamentos simples e em segundo plano, mas aumentando seu peso em jogo, pode usar aspectos de equipamento. Neste método, a maior parte dos equipamentos se comporta como apresentado no padrão Fate – ele permite o uso de perícias e justifica as ações. No entanto, se o equipamento tem o potencial para ser importante em
algum ponto da história, ele se torna um aspecto.

Um aspecto de equipamento pode ser tão genérico ou descritivo quanto desejar. Se para você armas são importantes, mas intercambiáveis, talvez tenha apenas um ___Revólver___ ou ___Rifle de Precisão___. Se desejar ser um pouco mais específico, talvez algo como ___Colt .45 Em Perfeitas Condições___ ou um ___XM Silencioso___. Quer ser mais profundo? Crie algo como ___Revólver de Serviço de Meu Pai___ ou ___Meu XM21 Bem Utilizado e Modificado___. A questão é a seguinte, se for importante, se torna um aspecto. Coisas como sua jaqueta ou  sapatos, óculos de sol, chaves do carro — talvez até carro — não precisam de aspectos, a menos que isso se torne importante na história.

Um aspecto de equipamento funciona como qualquer outro aspecto no jogo: você pode invocá-lo e outros podem forçá-lo — ou invocá-lo — contra você. É possível invocar um aspecto de equipamento a qualquer momento em que isso possa ser útil: invoque seu ___Jóias Importadas___ quando quiser chamar atenção e impressionar alguém ou invoque ___Passe de Imprensa___ para ter acesso à cena do crime.
 
Para Narradores, a regra final no que diz respeito a aspectos de equipamento é: podem ser removidos. Se o PJ está começando a depender demais de um aspecto de equipamento ou se achar que está na hora de agitar um pouco mais as coisas, encontre uma razão para fazer esse aspecto desaparecer. Fazer isso é um ato de forçar, então o jogador pode recusar — e tudo bem se isso acontecer —, afinal você não quer tirar do jogo a parte favorita dos jogadores. Além disso, a retirada do aspecto não significa necessariamente que o jogador perde aquilo permanentemente. Um ___Passe de Imprensa___ pode desaparecer quando o personagem é suspenso e um ___Revólver___ pode não ser útil quando a munição acabar. Em ambos os casos, há uma forma narrativa para ganhar o aspecto de volta e às vezes isso pode impulsionar uma nova aventura!

---

#### MUITOS ASPECTOS?

Se cada PJ possui cinco aspectos e talvez quatro ou cinco aspectos de equipamento, além disso há os aspectos de cena e consequências, entre outros.. Será que não são aspectos demais? Pode ser que sim! Eis um segredo: aspectos de equipamento são basicamente aspectos de situação portáteis. Dessa forma, podem fazer uma grande parte do trabalho pesado que os aspectos de situação realizam, se sentir que há muita coisa acontecendo ao mesmo tempo numa cena. Não é uma boa ideia eliminar os aspectos de situação completamente, mas se cada PJ possuir um certo número de aspectos de equipamento, comece com menos aspectos de situação. 

Também é uma boa ideia limitar o número de aspectos de equipamento que cada PJ possui. Nem todo equipamento precisa de
aspectos, então limitar os PJs a apenas três, dois ou mesmo um é perfeitamente razoável.

Por último, em um jogo focado em equipamento ao invés do relacionamento entre os PJs ou aventuras anteriores, aspectos de equipamento podem substituir os aspectos das “Três Fases” (página 33 do Fate SiS tema BáSico).

## Condições


Consequências são uma ótima forma de lidar com ferimentos, cicatrizes emocionais e outras condições persistentes na história de seu jogo. Elas não são para todos, no entanto. Algumas pessoas têm dificuldade em criar boas consequências no improviso, enquanto outros querem algo mais definido e concreto. Outros só querem alguma coisa diferente. 

__Condições__ são como consequências, exceto pelo fato de que são predefinidas, desta forma:

Há três tipos de condições: momentânea, estável e duradoura. Uma __condição momentânea__ desaparece quando você consegue uma chance para respirar e se acalmar. No exemplo acima, Irritado e Amedrontado são condições momentâneas. Uma __condição estável__ permanece até que um evento específico ocorra. Se estiver Exausto, permanecerá assim até que consiga dormir. Se
estiver Faminto, permanecerá assim até que coma algo. Ferido e Lesionado são __condições duradouras__. Elas permanecerão por uma sessão inteira e exigem que alguém realize uma jogada de superar contra uma oposição passiva de ao menos Ótimo (+4) antes de começar a se recuperar dela. As __condições duradouras__ possuem duas caixas ao seu lado e você marca ambas quando receber a condição. Quando iniciar a recuperação, apague uma das caixas. Apague a segunda (e se recupere totalmente) após mais uma sessão completa de jogo. Você só pode receber uma condição duradoura se ambas as caixas estiverem
livres.

Você sofre uma determinada condição quando o Narrador disser, normalmente como resultado de uma situação narrativa, mas também pode usá-las para absorver estresse. Ao receber estresse, você pode reduzir o estresse em 1 se marcar a condição momentânea. É possível marcar quantas condições desejar para um único golpe.

Uma vez que esteja sofrendo uma condição, ela se torna um aspecto na planilha de seu personagem, como qualquer outro. Desta forma, as condições são bem semelhantes às consequências, pois você pode invocá-las e elas podem ser invocadas ou forçadas contra você. Assim como uma consequência, quando você recebe uma condição, alguém pode invocá-la gratuitamente
uma vez contra seu personagem.


| Momentâneas                                              | Estáveis                                                 | Duradouras                                                                                                        |
|----------------------------------------------------------|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| Irritado                                                 | Exausto                                                  | Ferido                                                                                                            |
| Amedrontado                                              | Faminto                                                  | Lesionado                                                                                                         |
| Passam quando tiver uma chance de respirar e acalmar-se. | Ficam marcadas até um evento específico (dormir, comer). | É necessária uma ação de recuperação para remover. A segunda caixa só pode ser marcada se a primeira não estiver. |

Se você pretende usar condições em seu jogo, algo que pode fazer para reforçar o tema ou o estilo de seu jogo é criar suas próprias condições. Você não precisa seguir o mesmo padrão de duas caixas momentâneas, duas estáveis e duas duradouras, como apresentado aqui, mas você deve manter o mesmo número total de tensões que pode ser absorvido, 14. Tenha certeza também de seguir as regras para diferentes tipos de condições: condições momentâneas desaparecem rápido, condições estáveis exigem um motivo narrativo e condições duradouras exigem tratamento. As condições acima são genéricas o suficiente para abranger uma grande variedade de cenários, mas modelar as condições para a sua ambientação pode ser uma forma útil de fazer os personagens se sentirem parte daquele mundo. 

Como exemplo, aqui estão algumas condições do cenário Fight Fire de Jason Morningstar (encontrado no Fate Worlds: Worlds on Fire):

| Momentâneas                                              | Estáveis                                                 | Duradouras                                                                                                        |
|----------------------------------------------------------|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| Sem Fôlego                                               | Desidratado                                              | Ferido                                                                                                            |
| Em Pânico                                                |                                                          | Lesionado                                                                                                         |
| Contundido                                               |                                                          |                                                                                                                   |
| Desorientado                                             |                                                          |                                                                                                                   |
| Passam quando tiver uma chance de respirar e acalmar-se. | Ficam marcadas até um evento específico (dormir, comer). | É necessária uma ação de recuperação para remover. A segunda caixa só pode ser marcada se a primeira não estiver. |

# Capítulo 3 - Perícias

## Altere a Lista!

As perícias no Fate Sistema Básico foram desenvolvidas para oferecer uma ampla variedade de ações, facilmente aplicadas a diversos cenários diferentes. Dito isto, perícias são a primeira coisa que esperamos que você altere. Sua lista de perícias deve ser compatível com o cenário, reforçando o tema; se a lista padrão de perícias já fizer isso, você tem sorte! Em alguns casos, elas podem funcionar _bem o suficiente_ , mas pode ser preciso adicionar uma perícia ou duas, retirar outra ou renomear algumas. Se estiver precisando realizar modificações mais profundas na lista de perícias — ou
mesmo no próprio sistema — então este capítulo foi feito para você.

## Usando Como Foram Escritas

A coisa mais fácil na maioria dos casos é simplesmente ajustar a lista de perícias para refletir as necessidades de seu jogo. Há algumas maneiras de abordar isso.

### Adicionando e Removendo Perícias

Construir coisas não é algo importante no seu jogo? Descarte a perícia Ofícios. Quer que as pessoas possam manobrar em gravidade zero? Adicione uma perícia que o faça. Essa é a forma mais fácil de modificar a lista de perícias, pois basta checar perícia por perícia e decidir o que é necessário ou não e o que precisa ser adicionado.

Tenha algumas coisas em mente, porém. Primeiro, se muitas perícias forem cortadas, os jogadores podem ter acesso a maior parte das perícias da lista. Se houver muitas perícias repetidas entre os PJs, você pode perder oportunidades para dar destaque aos PJs. Por outro lado, se adicionar muitas perícias, pode acontecer de os PJs não possuírem perícias suficientes
dessa lista, ou seja, não possuírem uma perícia necessária para uma determinada situação. Uma solução fácil é aumentar ou diminuir o número de perícias que cada PJ recebe durante a criação de personagem.

Considere que o Fate Sistema Básico possui 18 perícias. Como regra geral, a pirâmide de perícias permite que um personagem se destaque em 3 delas e tenha nível em mais 7, deixando 8 disponíveis. Não é uma relação precisa, mas vale a pena ter em mente quando for alterar a pirâmide. Veja [_Mudanças Estruturais_][] para mais detalhes.

Finalmente, lembre-se que alterações nas perícias trazem repercussões nas caixas de estresse e ordem dos turnos.

### Renomeando e Fazendo Alterações


Se as perícias padrão funcionam bem para você do ponto de vista mecânico, mas algumas delas não encaixam perfeitamente no tema do jogo, mude alguns detalhes. Conhecimento pode se tornar Acadêmico ou Erudição, Condução se torna Cavalgar ou Pilotar e assim por diante. Isso também pode alterar o que uma determinada perícia pode fazer. Por exemplo, ao trocar Conhecimentos por Conhecimento Arcano e determinar que é a perícia padrão para conjurar magias, então fará sentido usá-la tanto em ataques quanto em defesas.

### Expandindo e Comprimindo


Um truque útil para alterar a lista de perícias é analisá-las do ponto de vista dos tipos de atividades que podemos realizar com elas, além do seu nível de importância no jogo. Se algumas atividades parecerem menos importantes, então podemos comprimir as atividades em uma perícia funcional – em um jogo não violento, você pode comprimir Atirar e
Lutar em uma única perícia de combate. 

Por outro lado, se algumas atividades se mostram importantes, é possível dividi-las em várias partes. Se as artes marciais são importantes em seu jogo, talvez queira expandir Lutar em várias perícias, como Luta Armada, Estilos Pesados ou Estilos Suaves. A vantagem dessa abordagem é as linhas gerais das perícias ainda funcionam - você apenas ajustou o foco.

## Outras Soluções

Às vezes a lista de perícias tradicional simplesmente não é o que você precisa. Talvez você queira uma lista completamente nova ou talvez precise de uma abordagem diferente. A boa notícia é que mesmo se fizer algo drasticamente diferente, as orientações das perícias existentes podem continuar a ser de grande ajuda, já que a ação por baixo delas ainda podem funcionar da mesma forma que a perícia original. Independente se a perícia se chama Lutar, Kung Fu ou Durão, a mecânica básica de socar alguém permanecerá praticamente a mesma.

### Nova Lista de Perícias

Se for necessário criar uma nova lista de perícias no mesmo estilo da existente — apenas com perícias novas — então o caminho vai depender muito do motivo da mudança, algo que ninguém saberá melhor que você. As orientações sobre como adicionar e remover perícias são a sua melhor referência para isso.

### Profissões

Enquanto perícias normais representam o que você faz, as profissões refletem quem o personagem é, deixando suas habilidades subentendidas. Se por exemplo as profissões forem Lutador, Erudito, Lenhador, Ladrão, Artesão e Diplomata, então poderíamos rolar Lutador em situações onde normalmente usaria Lutar ou Atirar, Ladrão onde normalmente rolaria Furtividade ou Roubo, e assim por diante. Listas de profissões em geral são mais curtas e abertas que a lista de perícias, o que pode tornar a pirâmide bem pequena. É possível usar uma lista de profissões fixas ou usá-las de forma geral, como trataremos abaixo.

### Abordagens

Se a lista padrão de perícias representa o que você sabe fazer e as profissões representam o que você é, as abordagens refletem como faz as coisas. Isto é, ao invés de possuir mais perícias, um personagem pode ter níveis de Poderoso, Estiloso, Esperto ou Determinado que pode rolar dependendo da situação. Em uma luta de espadas você agride ferozmente a defesa de seu oponente (Poderoso), balança em um lustre (Estiloso), finta e manobra em busca de uma abertura (Esperto) ou é paciente até que seu adversário comenta um erro (Determinado)?

Fãs do Fate Acelerado reconhecerão esse método como o usado pelo sistema, através das abordagens Cuidadoso, Inteligente, Estiloso, Poderoso, Ágil e Sorrateiro.

### Perícias Livres

Então, e se não houvesse lista de perícia e os jogadores pudessem simplesmente dizer as perícias que possuem? Pode parecer meio caótico a princípio, mas você notaria padrões familiares, com nomes novos e interessantes. Tal abordagem é funcional, com apenas uma ressalva – todos precisam saber o que a perícia representa. Caso contrário é possível que um jogador acabe com uma super-perícia que é útil em diversas situações, deixando o restante do grupo de lado.

A forma mais fácil de evitar isso é ter certeza que todos entendem a diferença entre uma perícia, uma profissão e uma abordagem. A maior parte dos problemas surgirá se você tiver um jogador escolhendo perícias enquanto outro escolhe profissões e abordagens, já que profissões e abordagens são mais abrangentes quando aplicadas em jogo, diferente das perícias. Abordagens e profissões podem ser um pouco mais amplas, mas ainda devem ser tratadas com cautela. 

Além de estimular a criatividade dos jogadores, esta abordagem permite um truque especial — usar aspectos como perícias.

### Perícias e Aspectos

Se estiver usando perícias livres, uma de suas perícias poderia compartilhar o nome com um aspecto. Se você é um ___Cavaleiro do Cálice___ e também possui Cavaleiro do Cálice Bom (+3), isso cria um certo ar de completude. Isso não é nem um pouco obrigatório, mas às vezes parece ser a melhor escolha. Também é possível usar aspectos para deixar de lado as perícias, mas isso é um pouco mais complicado e falaremos sobre mais adiante.

## Mudanças Estruturais

Até agora, todos esses sistemas assumem algo que se assemelha à lista de perícia nos pontos mais importantes – há uma lista organizada de uma certa forma e um grupo progressivo de bônus. Mas esses detalhes não precisam ser universais – você pode fazer algumas loucuras ao sair do padrão.


### Alternativas à Pirâmide

A alteração mais óbvia é substituir a pirâmide por um sistema de colunas — como usado nos avanços —, um sistema de pontos ou algo como pacotes de perícias ligados ao histórico do personagem. Isso é algo que você pode fazer livremente, mas antes de fazê-lo tenha certeza de que entende o motivo do uso da pirâmide. ela é rápida, exige que os personagens sejam capazes (o topo), mas bem estruturados (a base) e garante que nem todo personagem seja capaz de fazer tudo (as perícias que você não pode comprar). Não é incoerente alterar isso; um jogo de superespiões, por exemplo, pode ter personagens com bases muito amplas de perícias, mas certifique-se de saber qual problema está tentando resolver ao realizar a mudança.

### Passos Maiores

Nada exige que as perícias progridam vagarosamente. Suponhamos que cada “nível” de uma perícia fosse, na verdade, dois níveis na escala – agora você poderia ter uma pirâmide de 1 perícia Excepcional (+5), 2 em Bom (+3) e 3 ou 4 em
Regular (+1).

Por que fazer isso? Suponhamos que você possua uma lista pequena de perícias, como no caso das profissões ou abordagens. Você terá uma pirâmide de potência heroica — mais ainda do que o padrão — para apenas algumas perícias. Isso também separa claramente as “categorias” dos níveis de perícias, algo que pode ser tematicamente apropriado em certos gêneros.

### Apenas Aspectos

Se quiser tomar uma medida bastante extrema, poderia abrir mão inteiramente das perícias e usar aspectos para tudo. Isso exige uma pequena alteração na forma como os aspectos funcionam. Agora, além do que já fazem, eles concedem um bônus de +1 a todas as situações nas quais se apliquem. Assim, se meus aspectos forem **_Acrobata, Mulherengo, Forte_** e **_Espadachim_** , posso contar com um +2 — uma perícia Razoável na prática - na maioria das lutas com espadas envolvendo
força, o que se eleva para Bom (+3) se eu fizer de modo acrobático.

Se tomar esse rumo será preciso um raciocínio extra na a seleção dos aspectos, de forma que todos compreendam o que representa cada um deles e como se aplicam. O instinto de muitos jogadores será usar aspectos amplos sobre armas,
aspectos simples, como forte ou esperto, porque se aplicam facilmente na maioria das situações. No entanto, a verdadeira vantagem será dos jogadores que tomam mais tempo para criar aspectos mais profundos. **_Cavaleiro_** é bastante abran-
gente, mas **_Cavaleiro das Estrelas_** pode ser mais interessante e **_Cavaleiro Renegado das Estrelas_** pode ser mais profundo e intenso. Como costuma ser o caso, os personagens mais interessantes serão também os mais poderosos.

## Outras Funções de Perícias


Perícias permitem que você realize várias coisas, mas elas podem ser ainda mais flexíveis do que aparentam. Se der uma olhada na lista de perícias verá que cada uma delas possui uma ação de superar e criar vantagem marcadas. Isso abre muitas possibilidades, mesmo para perícias que aparentemente não possuem formas interessantes de uso.

Se um personagem possui um nível alto em uma perícia, Bom (+3) ou maior, isso significa que ele é um perito naquela área. Ele possui grande conhecimento dentro das possibilidades cobertas pela perícia. Este conheci- mento cobre as ferramentas e aparatos usados e o conhecimento de outras pessoas que usam a perícia no mesmo nível.

As três perícias usadas para atacar – Lutar, Atirar e Provocar – são mais usadas em combate. Isso não quer dizer que o combate é a única situação onde essas perícias são úteis. Um personagem com a perícia Atirar pode identificar armas, saber detalhes sobre sua manutenção e como obtê-las. O mesmo vale para Lutar, no que diz respeito a armas brancas. 

Isso abre diversas possibilidades para as ações de superar e criar vantagem. Um especialista em Atirar pode usar a ação superar para encontrar um vendedor de armas quando entra em um novo local, por exemplo. Atirar pode ser usado para criar vantagem por identificar o tipo de arma pelo som do disparo e descobrir assim os detalhes e limitações do modelo ou notar que a arma de um inimigo não recebeu a manutenção devida e pode sofrer um mal funcionamento.

Provocar pode parecer uma perícia difícil de expandir, mas com ela um especialista pode criar vantagem para descobrir o blefe de um oponente ou para identificar qual pessoa num grupo de oponentes é a mais perigosa. 

Outras perícias podem ser usadas da mesma forma. Especialistas em Roubo podem encontrar ferramentas ou parceiros na área, ou mesmo identificar técnicas de outros ladrões pelos sinais que deixam para trás. Especialistas em Condução podem identificar veículos e criar vantagem para falar sobre as sutis vantagens e desvantagens ao compará-los com outros veículos. Perícias que normalmente não permitem o ataque ou defesa – Roubo, Ofícios, Investigar, Conhecimento e Recursos – possuem muitas opções para ações adicionais e, nas circunstâncias adequadas, podem ser usadas para atacar e defender. Na situação certa, Ofícios poderia ser usada para construir uma armadilha perigosa, por exemplo. Roubo poderia ser usado para defender, se um personagem cria contramedidas baseadas em seu conhecimento. Essas aplicações podem ser bem específicas e limitadas, mas pode ser bastante recompensador permitir aos jogadores usar suas perícias de uma forma que amplie um pouco suas capacidades.

O segredo está na criatividade. Em praticamente qualquer perícia, um alto nível abre muitas possibilidades. Como Narrador, use flexibilidade e lógica como guia à medida que seus jogadores forem lhe surpreendendo. Eles devem ter a chance de sentirem-se excepcionalmente eficientes na área de atuação de seus personagens. Quando um jogador tenta algo novo com uma perícia, considere com cautela e esteja aberto a uma interpretação criativa da perícia. Como jogador, tente novas possibilidades no uso de suas perícias. Se surgir uma situação na qual usar uma perícia de uma maneira diferente parece uma opção, tente!  O Fate se torna mais interessante quando coisas inesperadas acontecem e os jogadores usam métodos inesperados para superar as situações em que se encontram.

## Esferas de Perícias

__Esferas de perícias__ são conjuntos de perícias que representam uma determinada área de competência. Esferas são ótimas para agilizar a criação de personagens, tornando-a ainda mais rápida que no Fate Sistema Básico. Em lugar de escolher as 10 perícias individuais, os jogadores distribuirão níveis por três esferas diferentes e as perícias de cada esfera recebem o valor respectivo.

Dê uma olhada nas perícias que seu jogo possui e pense em arquétipos que gostaria de ver em sua partida. Para cada um desses arquétipos, escolha cinco ou seis perícias relevantes. Cada um desses conjuntos será uma esfera. Não há problema se uma perícia aparecer em mais de uma esfera, mas se for comum a quase todas, esteja ciente de que os personagens poderão ficar um pouco homogêneos.

O ideal é que haja entre quatro e oito esferas de perícias — o suficiente para uma boa variedade, mas pouco o bastante para que a escolha seja rápida. 

---
Isa está planejando um jogo de fantasia clássica com exploração de masmorras, caça a tesouros e a ocasional matança de dragões. As perícias do jogo serão Atletismo, Roubo, Contatos, Enganar, Empatia, Intimidação, Lutar, Projétil, Vigor, Comunicação, Furtividade, Sobrevivência e Vontade. Com base nessa lista, ela cria quatro esferas.

| __Combate__ | __Social__  | __Ladinagem__ | __Silvestre__ |
|-------------|-------------|---------------|---------------|
| Atletismo   | Contatos    | Atletismo     | Atletismo     |
| Intimidação | Enganar     | Roubo         | Conhecimento  |
| Lutar       | Empatia     | Enganar       | Notar         |
| Projétil    | Comunicação | Notar         | Furtividade   |
| Vigor       | Vontade     | Furtividade   | Sobrevivência |

Isa percebe que Atletismo aparece em três dos quatro grupos, mas é uma perícia tão amplamente aplicável que não consegue imaginá-la faltando em qualquer um desses grupos. Além disso, os PJs exploradores de tumbas precisarão dela.


### Classificações


A pirâmide de perícias padrão, apresentada no Fate Sistema Básico, não se aplica a essas esferas de perícias. Ao invés disso, cada jogador escolhe três esferas e as classifica — uma como Bom (+3), uma como Razoável (+2) e outra como Regular (+1) — e o nível de cada esfera se torna o nível básico de todas as perícias pertencentes à esfera. Perícias nesse nível são chamadas de __treinadas__. Perícias um nível acima das da esfera são chamadas de __focadas__ e perícias dois níveis acima das da esfera são chamadas __especializadas__.

---

Um dos jogadores na mesa de Isa, William, quer jogar com um personagem ladino, então ele escolhe Ladinagem como
Bom (+3). Isso significa que Atletismo, Roubo, Enganar, Notar e Furtividade começarão treinadas, com um nível de Bom (+3).

---

### Perícias Reforçadas

Além de sua Ladinagem em Bom (+3), Will também pegou Social Razoável (+2) e Combate Regular (+1). Tanto Combate quanto
Ladinagem possuem Atletismo, então ele reforça essa perícia de treinada para focada – de Bom (+3) para Ótimo (+4) e a coloca no grupo Ladinagem (onde essa perícia possuía o nível mais alto). Da mesma forma, Social e Ladinagem compartilham Enganar, então essa perícia passa para focada no grupo Ladinagem. Assim, os grupos de Will ficam desta forma:

|    | BOM (+3)  LADINAGEM           | RAZOÁVEL (+2) SOCIAL                    | REGULAR (+1) COMBATE                        |
|----|-------------------------------|-----------------------------------------|---------------------------------------------|
| +5 |                               |                                         |                                             |
| +4 | Atletismo,Enganar             |                                         |                                             |
| +3 | Roubo, Percepção, Furtividade |                                         |                                             |
| +2 |                               | Contatos, Empatia, Comunicação, Vontade |                                             |
| +1 |                               |                                         | Intimidação, Corpo a Corpo, Projétil, Vigor |

### Melhorando Perícias


Após isso, cada jogador recebe 7 pontos para melhorar suas perícias.

|                   __Melhoria   |   Custo   |
|-------------------------------:|-----------|
|        De treinada para focada | 1         |
|   De focada para especializada | 2         |
| De treinada para especializada | 3         |

Nenhuma perícia pode ser melhorada acima de especializada.

---

Will deseja ser especialmente furtivo, então ele gasta três pontos para especializar Furtividade, de Bom (+3) para Excepcional (+5). Ele também quer ser eficiente em combate, então gasta mais 3 pontos para se tornar especialista em Lutar, indo de Médio (+1) para Bom (+3). Seu último ponto vai para Contatos, de Razoável (+2) para Bom (+3), para que tenha um pouco mais de conexões.

---


### Esferas com Extras


Algumas esferas de perícias fornecem acesso a __extras__ — ou por conter um extra de perícia , ou por permitir o uso do extra de um aspecto. Para cada extra associado a uma esfera, essa mesma esfera passa a ter uma perícia a menos. Por exemplo, se as esferas no seu jogo possuírem cinco perícias cada, uma esfera com um extra de perícia passa a ter apenas quatro. Extras de aspecto são considerados a mais além do número padrão de aspectos em seu jogo.

---

Isa adiciona quatro esferas de perícia à sua lista: Arcana e Deuses, para dois tipos de magia diferentes, e Anã e Élfica, para... anões e elfos. Os dois primeiros possuem dois extras cada — uma perícia e um aspecto, ambos relacionados a magia — e os outros dois grupos possuem apenas um aspecto extra.

| Arcana          | Deuses          | Anã             | Élfica          |
|-----------------|-----------------|-----------------|-----------------|
| Conhecimento    | Favor           | Atletismo       | Atletismo       |
| Magia           | Conhecimento    | Conhecimento    | Conhecimento    |
| Vontade         | Vontade         | Corpo a Corpo   | Projétil        |
|  Perícia Extra  |  Perícia Extra_ | Vigor           | Percepção       |
|  Aspecto Extra  |  Aspecto Extra_ | _Aspecto Extra_ | _Aspecto Extra_ |

# Capítulo 4 - Façanhas

## Novas Classes de Façanhas

As façanhas, da forma como são apresentadas no Fate Sistema Básico, criam diversas formas de personalizar seu personagem, adicionando ajustes mecânicos interessantes ao jogo. Extraímos mais de cada perícia ao criar façanhas, e esse processo pode ser bem divertido. Se desejar mexer um pouco mais em suas façanhas, esta seção foi feita para você.

### Façanhas Flexíveis

Esta é a opção mais fácil de implementar, pois não passa de uma pequena mudança na sua forma de pensar em façanhas. No Fate Sistema Básico, façanhas estão intimamente ligadas às perícias. E se você quiser que suas façanhas sejam livres dessa ligação, ou ligadas a múltiplas perícias, ou que sejam ligadas a algo completamente diferente como um aspecto, aparato ou então uma barra de estresse? 

Alguns exemplos:

**Escudo Aliado:** Você pode invocar **Escudeira Anã** quando um aliado sofrer um ataque próximo a você. Quando o fizer, redirecione o ataque a si mesmo. Sua defesa é Regular (+1) contra esse ataque.

**Fúria Descontrolada:** Quando sofrer uma consequência física, você pode invocar essa consequência de graça em seu próximo ataque. Se você sofrer múltiplas consequências físicas, você recebe uma invocação grátis de cada.

**Bugigangas Úteis:** Seus bolsos estão repletos de pequenos objetos úteis. Sempre que precisar de algo, você o possui, contanto que não seja algo _muito_ fora do comum (como um mapa para encontrar o tesouro) ou grande demais para caber em seu bolso, algibeira ou mochila. Quando você diz que possui algo, o Narrador deveria aprovar com maior facilidade. 

Isso na verdade não se trata de uma alteração na mecânica do jogo, apenas uma mudança na aparência da façanha. Qualquer um dos três exemplos acima poderia estar relacionado a uma perícia — Provocar, Lutar ou Recursos, por exemplo — mas não ter que pensar a qual perícia ligar sua façanha lhe dá mais liberdade criativa em seu funcionamento, indo além dos bônus de +2 e trocas de perícia.

### Façanhas de Aspectos

Para façanhas conectadas a aspectos, pode ser que você encare seus efeitos como invocações gratuitas em situações raras. Outras façanhas de aspectos podem exigir uma invocação, como Escudo Aliado (acima), mas fornecer algo extra ou particularmente incomum quando o aspecto é invocado. Tais efeitos devem ser mais potentes do que as invocações padrão. Você pode até mesmo criar uma façanha que esteja conectada a ações específicas de forçar — mas tenha cuidado para não neutralizar a desvantagem com o benefício.

**Investida do Touro:** Como você é ***Forte Como um Touro***, uma vez por cena, como uma ação simples, pode se mover duas zonas em linha reta e realizar um ataque físico.

**Arruaceiro de Aço:** Quando o seu aspecto ___Não Consigo Manter a Boca Fechada___ é forçado para torná-lo alvo de um ataque, você pode limpar imediatamente quaisquer consequências suaves que possua, em lugar de receber um ponto de destino.

### Gatilho de Efeito


Quando usa esta mecânica, você cria façanhas que são ativadas por condições narrativas específicas, exigem uma rolagem de perícia e possuem um efeito específico como resultado. Façanhas assim são ótimas para encorajar os jogadores a fazer coisas que você espera, já que serão recompensados por isso.

__Um Amigo em Todo Lugar:__ Sempre que entrar em um local povoado, você pode declarar que já o visitou antes e realizar uma rolagem de Contatos contra uma oposição Razoável (+2). Se bem-sucedido, você possui um amigo que lhe deve um favor – nada muito valioso ou ameaçador. Se bem-sucedido com estilo, seu amigo fará qualquer coisa que estiver a seu alcance.

__Não Estou Para Brincadeiras:__ Quando você deixa claro o quão perigoso é, faça uma rolagem de Provocar contra a Vontade do alvo. Se bem-sucedido, o alvo não lhe atacará ou se aproxima de você de boa vontade, a não ser que você tome ação contra ele primeiro. Se bem-sucedido com estilo, qualquer um que possuir Vontade abaixo do nível do alvo também responderá dessa forma.

__Passo do Tornado:__ Quando você assume a posição marcial do tornado, role Atletismo contra uma oposição Razoável (+2). Se bem-sucedido, você pode correr em superfícies verticais e saltar distâncias improváveis sem a necessidade de uma rolagem, até o fim de seu próximo turno. Se bem-sucedido com estilo, você ganha esses benefícios até o fim da cena.

Você provavelmente notou que nenhuma dessas façanhas mostra o que acontece quando empata ou falha; isso é intencional. Estes gatilhos de efeito tendem a ser poderosos, então seus problemas devem equilibrá-los. Um empate deve ser semelhante a um sucesso, mas com algum custo. Em caso de falha, sinta-se livre para aplicar as reviravoltas apropriadas.

### Façanhas Amplas

Se você está à procura de maior variedade em suas façanhas do que um simples +2 ou semelhante, considere a ideia de ampliar a façanha permitindo que ela forneça +1 em duas ou três coisas. Poderiam ser três ações diferentes com a mesma perícia, ou se ramificar por várias perícias relacio- nadas. Se permitir façanhas amplas assim, tome cuidado com a sobreposição em façanhas combinadas: você não quer duas façanhas amplas causando o efeito total de três +2 pelo preço de apenas duas façanhas.

### Façanhas Combinadas

Se quiser oferecer façanhas particularmente poderosas, considere construir o benefício de múltiplas façanhas juntas para produzir um único grande efeito. Por exemplo, você pode criar uma façanha que produza um efeito monstruoso de 4 tensões — combinando duas façanhas com o custo de duas recargas. (Talvez reconheça esse método como o utilizado na construção de poderes em The Dresden Files RPG). Entretanto, esse tipo de benefício concentrado pode acabar com o equilíbrio do jogo rapidamente. Considere limitar o acesso a essas “superfaçanhas”, seja sua quantidade – como “todos podem ter apenas uma façanha dupla” — ou durante por seleção e permissão — “apenas estas façanhas estão disponíveis para lobisomens”.


## Custos de Façanhas

Em Fate Sistema Básico você recebe três façanhas gratuitas, recarga inicial 3 e pode comprar até duas façanhas adicionais ao custo de uma recarga cada. Essa é a forma padrão de lidar com façanhas, mas isso não quer dizer que seja a única forma. Cada um desses componentes — número inicial de façanhas, recarga inicial e custo por façanha — é uma característica que aumentada ou diminuída, tornando as façanhas mais ou menos comuns. As alterações feitas determinarão, em parte, seu tipo de jogo, assim como terão um impacto nos Marcos.

Mais façanhas significa PJs mais poderosos. Um PJ com mais façanhas pode conseguir grandes bônus nas rolagens, usar suas perícias mais elevadas com regularidade e quebrar as regras com frequência. Isso leva a partidas mais do tipo _pulp_, fora da realidade e até mesmo fantásticas. Se é isso que você deseja, ótimo! Considere, também, que fornecer mais façanhas aos PJs, além de conceder benefícios e exceções às regras, também torna os personagens mais complexos. Se os seus jogadores não estão acostumados a essa complexidade, isso pode reduzir o ritmo de jogo e tornar as coisas _menos_ empolgantes em vez de mais.

Por outro lado, fornecer aos PJs poucas façanhas pode tornar os personagens mais simples e fáceis de usar durante o jogo, mas os torna menos competentes. Um PJ com apenas uma façanha, por exemplo, possui apenas uma carta na manga, um único movimento especial. Isso pode significar um jogo mais denso ou com personagens claramente definidos e de nicho fixo, mas pode desapontar jogadores que querem ser bons em muitas coisas. Os PJs ainda serão competentes, mas não _tão_ competentes.

### Ajustando as Façanhas Iniciais

Esta é uma forma fácil de conceder mais ou menos façanhas aos PJs. Reduzir a zero significa que um jogador começa sem nenhuma façanha e tem que pagar o preço para cada façanha que queira adquirir. Adquirir façanhas pode parecer caro, mas também as torna individualmente mais importantes para os PJs. Ajustá-las da outra forma, fornecendo aos PJs mais façanhas iniciais, dá aos jogadores muitas escolhas divertidas para fazer logo de começo sem nenhum custo. Isso também aumenta o nível básico de poder de cada PJ, tornando mais provável que se tornem muito competentes em uma grande variedade de tarefas, ou incrivelmente bons em uma ou duas. Além disso, causa a impressão de que façanhas são mais baratas, o que encoraja os jogadores a adquirir ainda mais. 

Ao ajustar as façanhas iniciais, fique de olho no que os seus jogadores desejam. Todos os jogadores aceitaram o fato dos personagens serem menos poderosos ou mais complexos? Nem todos fazem questão de ganhar sempre ou gostam de tomar dúzias de decisões antes do jogo começar.

### Ajuste na Recarga Inicial

Destas três opções, ajustar a recarga é provavelmente a que possui maior impacto ao jogar, porque afeta diretamente a economia de pontos de destino que é central ao jogo. Conceder mais pontos de destino a cada sessão significa que os jogadores provavelmente resistirão contra tentativas de forçar e invocarão aspectos com mais frequência. Os jogadores ganham mais controle sobre a história, o que pode ser uma coisa boa, mas também um pouco mais difícil para criar desafios à altura.

Reduzir a recarga tem o efeito oposto – os jogadores possuem menos controle sobre a história, pois têm menos pontos de destino em mãos. Precisam aceitar forçar aspectos e não invocarão aspectos com frequência. Isso também significa que confiarão mais em invocações grátis de vantagens criadas, o que pode prolongar as cenas de luta mais complexas.

### Ajustando o Custo das Façanhas

Façanhas custando um ponto de recarga é uma das possibilidades, mas não a única. Você pode ajustar esse valor levemente ao aumentar ou reduzir o custo de recarga que um jogador deve pagar por uma façanha. Talvez façanhas custem dois pontos de recarga ou talvez um único ponto de recarga compre duas façanhas.

Há outras formas de lidar com os custos de façanhas. Talvez o jogador tenha que abrir mão de um ponto de perícia para comprar uma façanha, baixando uma perícia Bom (+3) para Razoável (+2) para assim poder comprar a façanha que deseja. Talvez seja preciso abrir mão de um aspecto ou dedicar um aspecto para conseguir a façanha. Conectar um aspecto a uma façanha pode criar um vínculo entre o jogador e a nova façanha e estabe- lece um bom limite máximo no número de façanhas por jogador.

Outra forma é tornar as façanhas gratuitas ou com baixo custo, mas incluir o custo dentro de cada façanha. Talvez cada façanha custe um ponto de destino para ativar, cause recebimento de estresse, custe uma ação para ativar ou ceda um impulso ao inimigo. Você pode incluir quantos custos desejar no uso de uma façanha.

Tenha em mente que façanhas possuem seus custos e fornecem benefícios porque custam um ponto _permanente_ de recarga do personagem, reduzindo seus pontos de destino. Esse ponto de destino não poderá ser convertido em um bônus de +2 ou algum outro benefício durante o jogo. Então, se você planeja alterar o custo de uma façanha, pense em como ajustar os benefícios obtidos para equilibrar com o que foi “perdido” no custo.

### Equilíbrio Entre Opções

A maioria das soluções exige o ajuste de múltiplos detalhes para encontrar o ponto ideal para cada grupo em particular. Isso é completamente normal. Este é o seu jogo e cabe a você e a seu grupo determinar o que funciona bem para vocês.

# Capítulo 5 - O Grande Jogo

## Eu Quero Emoção!

Às vezes uma pequena mudança é o suficiente, mas às vezes não. Pode haver um momento no qual você precise de algo grande, alterações dramáticas do sistema, algo que abale as estruturas.

## Opções Para Criação de Personagem

O sistema de criação de personagens no Fate Sistema Básico apresenta um estilo específico de elaboração de personagens, um que lhe dá bastante liberdade, mas também provê estrutura e guia os jogadores a criarem um grupo coerente de indivíduos. Talvez você queira algo um pouco menos aberto ou algo um pouco mais focado.

### Profissões e Raças

Qualquer um que já tenha jogado RPGs de fantasia está ao menos vagamente familiarizado com esta ideia. Para aqueles que não conhecerem, uma **profissão** é um conjunto de perícias e habilidades que partem do conceito de um personagem em particular, frequentemente ligado ao que você _faz_ no mundo de jogo. Uma **raça** descreve o que você _é_ — de onde
veio, quem é seu povo e quais são seus talentos naturais.

---

#### A PALAVRA “RAÇA”

O termo “raça” é impreciso e problemático neste contexto. Nós não estamos usando essa palavra como as pessoas a usam hoje em dia; o que queremos dizer é algo mais próximo de uma espécie. Ao criar uma raça em Fate, o que está fazendo na verdade é criar regras para os tipos de seres além dos humanos, não uma nacionalidade ou etnia.

Então porque usamos a palavra “raça”? Porque é comum nos RPGs. As pessoas sabem o que significa porque diversos jogos no passado já a usavam, o que causa um entendimento imediato sobre o que estamos falando. É o melhor termo para isso? Não. Mas não temos outra palavra com o mesmo impacto na história deste hobby, então usaremos esta, cientes de que é um termo problemático.

---

Em Fate, uma profissão é representada por um conjunto de perícias distribuídas na pirâmide, com valores específicos. Alguns espaços são deixados em branco e outros são definidos. Ao escolher uma profissão, você recebe a pirâmide relacionada a ela. Preencha o que permanecer me branco com o que desejar. Uma profissão também inclui alguns aspec- tos; escolha ao menos um desses ou crie novos baseados no tema representado pelos aspectos fornecidos. Não pegue mais do que dois aspectos de sua profissão. Por último, uma profissão possui uma lista de façanhas disponíveis. Essas são exclusivas; se você faz parte daquela profissão, poderá escolhê-las. Se não for da profissão, então essas façanhas estão fora de cogitação.

Uma raça em Fate vem com um certo número de aspectos; escolha ao menos um, mas não mais que dois. Assim como na profissão, você pode criar seus próprios aspectos se desejar. Sua raça também lhe concede uma perícia racial com alguns detalhes diferenciados (veja os exemplos abaixo). Coloque sua perícia racial em qualquer dos espaços vazios que possuir na pirâmide para determinar o diferencial de seu personagem em seu meio; a descrição explica como você pode usar a sua perícia racial. Você pode diferenciá-la ainda mais modificando-a como uma façanha.

---

### Profissão: Guerreiro

#### Aspectos

___Mercenário Experiente, Cavaleiro do Reino, Mestre de Armas, Defensor Robusto, Veterano de Guerra.___

#### Perícias

+ Ótimo (+4): Lutar
+ Bom (+3): Atletismo ou Vigor e outra perícia à escolha
+ Razoável (+2): Atirar ou Provocar e duas outras
+ Regular (+1): Quaisquer quatro perícias

#### Façanhas


__Escudeiro:__ Sempre que receber uma consequência suave você pode optar por destruir sua armadura ou escudo, contanto que esteja usando o item. Uma vez que sua armadura ou escudo tenham sido destruídos, você precisará realizar os devidos reparos ou adquirir novos.

__Força Bruta:__ Você pode gastar um ponto de destino para superar um obstáculo automaticamente, mesmo que já tenha rolado os dados, contanto que esteja tentando algo que use a força bruta.

__Caçador de Recompensas:__ Sempre que entrar em alguma região, você pode procurar por um trabalho. Após mais ou menos uma hora, você encontra um trabalho que paga bem, mas ou é perigoso, ou não tão perigoso, mas não paga muito bem. Se gastar um ponto de destino, o trabalho não é muito perigoso e ainda paga bem.

__Defensor dos Fracos:__ Quando alguém é atacado fisicamente na mesma zona que você, gaste um ponto de destino para redirecionar esse ataque para você mesmo. Você se defende desse ataque com +1.

---

---

### Profissão: Ladrão

#### Aspectos

**_Especialista em Aquisições, Vigarista Astuto, Membro da Guilda dos Ladrões, Faca Sorrateira, Invasor(a) de Domicílios._**

#### Perícias

+ **Ótimo (+4):** Enganar ou Furtividade
+ **Bom (+3):** Atletismo ou Provocar e outra perícia à escolha
+ **Razoável (+2):** Lutar ou Atirar e duas outras
+ **Regular (+1):** Quaisquer quatro perícias

#### Façanhas

**Golpe Traiçoeiro:** Quando puder surpreender o seu inimigo ou atacar de um lugar escondido, você pode realizar ataques com Furtividade. Se bem-sucedido, você cria um impulso. Se bem-sucedido com estilo, você poderá invocar esse impulso duas vezes gratuitamente.

**Identidade Secreta:** Você possui uma identidade alternativa que pode assumir. Descreva sua identidade, escolha um aspecto de conceito e dificuldade para sua identidade alternativa, assim como a melhor perícia. Você pode assumir essa identidade com alguma preparação e o gasto de um ponto de destino. Enquanto estiver nessa identidade, o conceito e dificuldade substituirão o seu e você pode usar Enganar em lugar de sua perícia de nível mais alto. Você perde esses
benefícios assim que o disfarce cair, e talvez tenha que gastar algum tempo criando uma nova identidade.

**Criminoso Nato:** Sempre que entrar em uma região pela primeira vez, você pode gastar um ponto de destino para declarar que os criminosos locais o conhecem. Escolha uma das seguintes opções: Eles possuem algum trabalho promissor; você e seus companheiros terão acesso a quartos e comida gratuitos por algumas semanas; o ajudarão com algo _imediatamente_ , mas você fica devendo um favor.

**Nem Um Pouco Ameaçador:** Escolha Enganar ou Furtividade quando usar esta façanha. Quando você cria vantagem com a perícia escolhida para causar a impressão de não ser ameaçador ou o mais discreto possível, os inimigos procurarão outro alvo enquanto esse aspecto existir. Assim que você realizar um ataque bem-sucedido, o aspecto some.

---

---


### Raça: Elfo

#### Aspectos

___A Sabedoria de Séculos, “Conheço Esta Floresta”, A Grande Jornada, Magia no Sangue, Perfeição em Tudo.___

#### Perícia Racial: Elfo

Você pode usar a perícia Elfo para reconhecer fauna e flora úteis, conhecer caminhos pelas florestas ou notar perigos escondidos. Além disso, escolha uma das seguintes vantagens (você pode pegar mais de uma pelo custo de uma façanha ou recarga para cada). 

__Alta Magia Élfica:__ Você pode usar a perícia Elfo para conjurar magias ligadas à natureza, mesmo que não possua nenhuma habilidade mágica.

__Perfeição Em Batalha:__ Escolha Atirar ou Lutar. Quando estiver usando armamentos típicos de seu povo, você pode usar a perícia Elfo no lugar da perícia escolhida.

---

---

### Raça: Orc

#### Aspectos

___Sangue e Glória, Todos Temem a Horda, “A Dor é Para os Fracos”, Os Espíritos me Guiam, Guerreiro Dos Sete Clãs.___

#### Perícia Racial: Orc

Você pode usar esta perícia para resistir a dor, clamar aos espíritos por auxílio ou realizar feitos de força bruta. Adicionalmente, escolha uma das seguintes vantagens (você pode pegar mais de uma ao custo de uma façanha ou recarga para cada).

__Fúria Sanguinária:__ Ao usar a perícia Orc para criar vantagem representando uma fúria de batalha, você recebe uma invocação extra para o aspecto caso seja bem-sucedido ou bem-sucedido com estilo.

__Pele Resistente:__ Você pode usar Orc em lugar de Vigor para determinar seu estresse e consequências físicos, e também ganha uma consequência física suave adicional.

---

### A História de Origem

A história de origem é um método para dar um início rápido à criação de personagens já em jogo, usando trechos narrativos para cada jogador. Antes de começar a jogar uma história de origem, um personagem precisa de duas coisas: um conceito e uma perícia no nível mais alto. A maioria dos jogadores terá ao menos uma ideia geral do que quer ser, mas pode ter dúvidas nos detalhes. O conceito e a perícia mais alta definem o personagem de forma ampla, criando um ponto de partida para uma história de origem.

Jogar a história de origem com um personagem é como jogar uma partida normal, com a diferença que você tem o objetivo de definir quem o personagem é. Comece no meio da narrativa — ao começar com algo acontecendo, você dá ao jogador a oportunidade de fazer escolhas sobre o seu personagem.

Cada personagem inicia sua história de origem com um ponto de destino.

### **Escolhendo Perícias**

Durante a história, peça várias rolagens de perícia. Foque na perícia mais alta do personagem até certo ponto, mas também faça rolagens de outras perícias. Sempre que o jogador realizar uma rolagem de perícia e não quer ter o nível Medíocre (+0) nela, ele pode colocar essa perícia em um dos espaços disponíveis. Feito isso, ela passa a fazer parte do personagem.um pouco mais difícil para criar desafios à altura.

### **Escolhendo Aspectos**

Coloque o jogador em uma grande variedade de situações, expondo-o a dificuldades diferentes. Quando um jogador encontrar algum problema, como, por exemplo, quando precisar de um bônus de +2 ou rolar novamente, sugira um aspecto. Se ele aceitar a sua sugestão ou criar seu próprio aspecto, deixe-o invocá-lo uma vez de graça e conceda um ponto de destino!

### **Escolhendo Façanhas**

Você pode oferecer façanhas ao jogador da mesma forma que ofereceu os aspectos — apresentar algo que permita se safar de uma situação complicada ou permitir que consiga realizar algo que precisa ser feito. Assim como no Fate Sistema Básico, o personagem recebe três façanhas gratuitas e pode pegar façanhas adicionais ao reduzir a recarga – a menos que isso tenha sido alterado, claro. Se for uma façanha com uso limitado ou que custa um ponto de destino, deixe-o usar uma vez gratuitamente.

### **Envolvendo Outros Jogadores**
A história de origem do personagem do jogador é uma atividade comunitária! Outros jogadores podem aparecer como PdNs — você pode sugerir isso a eles, se precisar que alguém interprete um personagem em particular. Eles podem até mesmo aparecer com seus próprios personagens, independente se já tiverem jogado sua própria história ou não.

Outros jogadores podem sugerir aspectos, mas apenas se esses aspectos definirem um relacionamento com seus próprios personagens. Se dois jogadores definirem um relacionamento durante uma história de origem, ambos recebem um aspecto e um ponto de destino — que o outro jogador poderá usar em sua própria história.

### Finalizando um Histórico

Prossiga com a história de origem até uma conclusão lógica, mas tente não prolongá-la além de quinze ou vinte minutos antes de passar para a próxima. A ideia é jogar a história de cada um dos jogadores em uma única sessão

## Forçando Aspectos Para Estruturar Aventuras


Se você não consegue pensar em uma ideia legal para uma aventura, basta ir mais a fundo nos aspectos dos PJs! Aqui daremos algumas dicas para iniciar uma aventura rapidamente e também fornecer alguns pontos de destino extras logo de início.

Primeiro, dê uma olhada nos aspectos dos PJs e veja se tem alguma ideia de imediato. Se perceber que alguns deles oferecem um gancho interessante para uma aventura, em especial algo que esteja ligado ao mundo de jogo ou a alguma organização, diga ao jogador que você deseja usá-lo como base para uma aventura e ofereça-lhe um ponto de destino. A maioria dos jogadores aceitarão forçar sem muita negociação, mas se o jogador não estiver interessado, não o force a pagar um ponto de destino por recusar – simplesmente veja a possibilidade de forçar outro aspecto.

Assim que conseguir a base da sua aventura, apresente-a à mesa. Dê sugestões de outros aspectos e solicite sugestões dos jogadores. Converse com eles sobre o que aparecerá na aventura e dê pontos de destino por boas sugestões. Essas sugestões não precisam estar ligadas a aspectos, mas é melhor se estiverem. Afinal, se múltiplos aspectos estiverem ligados à história, você poderá continuar a forçar esses aspectos mais tarde!

Se você aceitar sugestões que não sejam baseadas em aspectos existentes, transforme essas sugestões em aspectos. Elas podem ser aspectos de situação ou aspectos espalhados por toda a aventura.

Depois de conseguir um bom ponto de partida, comece a jogar! Agora você tem uma situação inicial do interesse dos jogadores , que por sua vez possuem alguns pontos de destino extras que podem investir para realizar coisas incríveis logo no início!

## Eventos de Aspectos

Este é um truque que você pode usar para criar a estrutura de uma aventura — pode até mesmo combinar esta técnica com a anterior para injetar eventos baseados nas ideias e sugestões dos jogadores.

Um evento de aspecto possui dois componentes: a **lista de eventos** e o aspecto gradativo. A lista de eventos é uma série de coisas que acontecerão, direcionando ao aspecto gradativo. Pense no **aspecto gradativo** como o que acontecerá se os jogadores não intervierem. Um bom evento possui de três a seis aspectos além do aspecto gradativo.

Quando a aventura começar, marque o primeiro aspecto e traga-o para a história. Este é o incidente inicial, a questão na qual os PJs se envolverão. Trata-se de um aspecto como qualquer outro — você e os jogadores podem forçar e invocar normalmente. Ele permanece em jogo até deixar de ser relevante; nesse momento é só riscá-lo da lista.

Sempre que a história sugerir que as coisas devem seguir para uma nova etapa, ou assim que a ação diminuir, verifique qual é o próximo aspecto e traga-o para a história. Agora ele passa a ser um aspecto que pode ser forçado e invocado, além de ser um novo elemento da história. Continue a fazer isso por quanto tempo for preciso. Acelere o ritmo da troca de aspectos caso os jogadores estejam distraídos ou não se envolvam, ou diminua o ritmo se estiverem muito engajados e proativos.

Se o aspecto gradativo for marcado, isso indica que as coisas estão ficando realmente feias. Isso normalmente indica que os vilões estão a um passo da vitória e que os jogadores precisam virar o jogo.

Se nem todos os aspectos gradativos tiverem sido riscados quando os PJs terminarem de resolver a situação, não é um problema! Isso significa que os PJs estavam envolvidos no jogo, foram incríveis e se saíram bem. Se todos os aspectos entrarem em jogo e as coisas acabarem mal para os PJs, tudo bem também! Os acontecimentos da história atual podem refletir na próxima, agora com novos desafios.

Eis um exemplo:
+ Explosões e Fogo!
+ Uma Onda de Assassinatos
+ Pânico Generalizado
+ Sob Ameaça Terrorista
+ Três Horas Para Explodir
+ Cratera Fumegante_**

## Nível de Poder

O Fate Sistema Básico oferece um nível de poder padrão que emula um tipo específico de ação. É heroico e voltado para a ação, mas também é um pouco difícil, com heróis frágeis o suficiente para se ferir gravemente com algumas pancadas, mas resistentes o suficiente para se manter vivos por um tempo. Ele também cria um certo nível de competência através dos níveis de perícias e pontos de destino recebidos.

Entretanto, esse nível de poder não é adequado para todo tipo de jogo. Às vezes a ideia é emular aventuras cheias de ação exagerada ou então um noir realista, e as regras padrões não se encaixam muito bem. Bem, sem problemas – bastam alguns ajustes!

### Perícias


No Fate Sistema Básico, os personagens possuem dez perícias distribuídas em uma pirâmide de níveis que vão de Regular (+1) a Ótimo (+4). Isso emula indivíduos altamente competentes com algumas poucas coisas em que são muito bons e uma maior variedade de coisas nas quais são bons. Há duas coisas a serem ajustadas quando falamos em perícias: o número de perícias e o nível limite.

Ao aumentar o número de perícias disponíveis, você torna os personagens competentes em uma maior variedade de coisas. Se aumentar o nível limite das perícias, você permite que os PJs sejam muito bons em algumas poucas coisas, possibilitando personagens em níveis sobre-humanos. Reduzir essas coisas tende a um cenário mais realista; poucas perícias significa que os heróis se limitarão mais a seus nichos de atuação, mas aumenta a chance do grupo não possuir uma perícia importante. Reduzir o nível limite torna os PJs menos competentes, o que é bom se você está tentando emular um grupo de pessoas comuns.

A maioria dos jogos aumenta ou reduz ambos; ajustar um sem ajustar o outro pode resultar em excesso de perícias ou de ninguém atingir os níveis mais altos de capacidade.

### Recarga

Ajustar a recarga para mais ou menos tem impacto na competência e versatilidade dos PJs. Mais importante, afeta a frequência em que os PJs aceitam forçar seus aspectos. Um alto nível de recarga significa que os PJs recebem mais pontos de destino por sessão, o que significa que recusarão aspectos forçados com mais frequência. Baixo nível de recarga significa que forçar os aspectos é mais importante, mas também significa que os PJs estão à mercê do Narrador.

Recarga também afeta quantas façanhas extras cada jogador pode comprar, o que tem um impacto nas especializações, proteção de estilos distintos, além da competência.

### Façanhas

Não deve ser uma surpresa que ceder mais façanhas gratuitas torna os PJs mais poderosos enquanto reduzi-las os torna menos poderosos. Isso também tem impacto na recarga — menos façanhas gratuitas significa que jogadores precisarão gastar sua preciosa recarga para comprar façanhas, e vice-versa.

Uma segunda forma de afetar o nível de poder através de façanhas sem alterar a quantidade é ajustando o nível de poder da façanha em si. Em Fate Sistema Básico, uma façanha vale cerca de duas tensões. Ajustar para 3 ou 4 tensões significa que cada façanha possui um impacto individual maior, enquanto ajustar para menos deixa-as menos importantes.

### Estresse

O Fate Sistema Básico define estresse com duas barras para cada tipo, com a possibilidade de aumentar esse número de acordo com certas perícias. Você pode aumentar essa quantidade, tornando os personagens mais resistentes, capazes de tomar algumas pancadas, ou diminuí-las para deixá-los mais frágeis. Esta mudança tem um impacto direto em quanto tempo durarão os combates. Mais estresse significa que os PJs podem aguentar mais lesões, o que torna as lutas menos arriscadas e provavelmente mais comuns. Baixar o estresse significa que as lutas serão mais perigosas, o que significa que os jogadores pensarão duas vezes antes de iniciar um combate. Também significa que eles precisam considerar conceder com mais frequência para evitar serem retirados de combate.

### Aspectos

Ajustar o número de aspecto para mais ou para menos de cinco não afeta tanto o nível de poder como outras características, mas causa um impacto na versatilidade do personagem. Mais aspectos significa que há mais truques disponíveis para os jogadores na hora do desespero, enquanto reduzir o número de aspectos diminui suas saídas. No entanto, tenha cuidado ao ajustar essa quantidade. Quanto mais aspectos houver, menos importante cada um será, e isso faz com que sejam esquecidos ou ignorados. Reduzir o número de aspectos significa que cada aspecto do indivíduo se torna mais importante e menos negligenciado, mas também significa que você terá que equivaler essa ausência com aspectos de situação. Em geral, mais do que sete aspectos pode tornar o personagem sobrecarregado, enquanto menos de três pode deixá-lo com pouca profundidade.

Também deve-se considerar ajustar a recarga junto com o número de aspectos. Se possuir vários aspectos e pouca recarga, a maior parte deles não será invocada.

### Exemplos

Eis alguns exemplos de níveis de poder que você pode usar em seus jogos.


| Noir Realista                        | Aventura Pulp                        | Super-Heróis                                                |
|--------------------------------------|--------------------------------------|-------------------------------------------------------------|
|                                      |                                      |                                                             |
| Oito perícias:                       | Quinze perícias:                     | Dezoito perícias:                                           |
|                                      |                                      |                                                             |
| 3 em Regular (+1),                   | 5 em Regular (+1),                   | 5 em Regular (+1),                                          |
| 3 em Razoável (+2),                  | 4 em Razoável (+2),                  | 4 em Razoável (+2),                                         |
| 2 em Bom (+3)                        | 3 em Bom (+3),                       | 3 em Bom (+3)                                               |
| (equivalente a 15 pontos de perícia) | 2 em Ótimo (+4),                     | 3 em Ótimo (+4)                                             |
|                                      | 1 em Excepcional (+5)                | 2 em Excepcional (+5)                                       |
| Limite de Perícia: Bom (+3)          | (equivalente a 35 pontos de perícia) | 1 em Fantástico (+6)                                        |
| Recarga 2                            |                                      | (equivalente a 50 pontos de perícia)                        |
| 1 façanha grátis                     | Limite de Perícia:  Excepcional (+5) |                                                             |
| 2 caixas de estresse iniciais        | Recarga 4                            | Limite de Perícia:  Fantástico (+6)                         |
| 5 aspectos                           | 5 façanhas grátis                    | Recarga 6                                                   |
|                                      | 4 caixas de estresse iniciais        | 5 façanhas grátis                                           |
|                                      | 7 aspectos                           | 4 caixas de estresse iniciais; façanhas podem fornecer mais |
|                                      | 5 aspectos                           |                                                             |

# Capítulo 6 - Circunstâncias Especiais

## Perseguições

Em uma história de aventura, você eventualmente terá uma grande cena de perseguição. Isso é algo comum em aventuras e o Fate Sistema Básico visa esse gênero. O que você quer é um jeito de tornar a perseguição empolgante. Não é divertido — e perde-se boa parte da emoção — quando uma única rolagem determina quem escapa. Há algumas formas diferentes de abordar esse tipo de ação.

Usando apenas as regras padrão, podemos simular uma perseguição como um desafio (Fate Sistema Básico, página 137). Para uma perseguição básica na qual você não quer gastar muito tempo, as regras básicas são perfeitamente aplicáveis. Adicione alguns obstáculos nas rolagens dos jogadores e deixe-os superar os mesmos. Se forem bem-sucedidos em rolagens suficientes, conseguem escapar ou, se estiver perseguindo, conseguem alcançar seu alvo. É simples, mas não tão interessante.

Se houver mais oposição ativa, as regras de disputa (Fate Sistema Básico, página 141) são a próxima opção e podem funcionar bem. Elas permitem alguma tensão e, para fazer de uma forma na qual cada jogador não precise fazer rolagens individuais, fazemos a disputa como se fosse entre dois times.. O primeiro time a alcançar três vitórias vence. Se for
o grupo em fuga, conseguem escapar; se for o grupo perseguidor, alcançam o alvo, trazendo as devidas consequências. Ser pego frequentemente desencadeia um conflito.

As regras de disputas funcionam bem, mas três vitórias são razoavelmente fáceis de conquistar se o grupo de personagens for razoavelmente grande, fora que às vezes você pode querer um pouco mais de drama em suas cenas de perseguição. Eis um método alternativo, chamado de barra de perseguição. É uma mistura das regras de conflito e disputa.

Para começar, crie uma barra de estresse para perseguição. Esse será o seu cronômetro para a cena. O lado em fuga está tentando esvaziar as caixas de estresse, enquanto o lado perseguidor está tentando preenchê-las. O tamanho da caixa de estresse determina quão longa será a cena e onde você inicia nessa barra determina a dificuldade da fuga.

A primeira decisão a ser tomada é quando tempo durará a perseguição. Se procura por uma cena de tempo mediano, uma barra de estresse com 10 caixas pode servir. Se quiser menos de 10, talvez seja melhor realizar uma disputa normalmente. Se deseja que seja mais longa e envolvente, adicione mais estresse. Uma cena de perseguição com 14 caixas de estresse pode se tornar o maior evento da sessão e uma com 18 ou 20 pode ser o foco de toda uma sessão de jogo. Você provavelmente não deseja ir muito além disso ou arriscará prolongar sua cena de perseguição a ponto de entediar os jogadores.

Definir quantas caixas de estresse iniciarão marcadas determinará o quão próximo o perseguidor está de alcançar o grupo em fuga. Recomendamos começar com o estresse no meio (em 5 de 10 caixas, por exemplo). Você pode tornar as coisas mais difíceis para quem está em fuga colocado o estresse mais próximo do final da perseguição, por exemplo na sétima das 10 caixas. Da mesma forma, você pode facilitar as coisas para quem está escapando por iniciar a perseguição com poucas caixas marcadas. É bom evitar isso a não ser que os PJs sejam os perseguidores. Se a perseguição for menos complicada, use um desafio ou disputa ao invés da linha de estresse de perseguição.
    
Depois de criar a linha de estresse, determine quem age primeiro. Você pode fazer isso julgando a situação ou avaliando na planilha de personagem, de cada um dos lados, quem possui o maior nível na perícia relevante. Cada um dos lados terá seu turno, então quem inicia possui alguma vantagem, mas nada além disso.

Cada lado, no seu turno, realiza uma rolagem de perícia na tentativa de aumentar ou diminuir a linha de estresse. Isso é uma ação de superar e pode ser contra oposição passiva ou, mais comumente, por oposição ativa do outro lado da perseguição. Essas ações podem significar praticamente qualquer coisa e é mais emocionante se elas forem variadas e criativas. Rolagens de Condução para manobrar um veículo em uma perseguição podem ser descritas como os personagens realizando manobras para se livrar de obstáculos ou cortar o trânsito, por exemplo, enquanto Atletismo para perseguições a pé poderia representar os personagens escalando telhados e realizando manobras arriscadas nos ares. Várias outras perícias podem entrar em jogo permitindo diferentes tipos de ações. Você pode usar Enganar para fintar seu oponente, Lutar para nocautear alguém, Notar para perceber perigos e evitá-los enquanto seu oponente não conse- gue, Vigor para empurrar obstáculos no caminho, lançando-os na direção do oponente. Se um jogador tiver uma ideia para uma boa ação em qualquer perícia, ela deveria ser permitida.

Quando realizar a rolagem, o resultado determina o que acontece com a barra de estresse.

- Se falhar, seu oponente pode escolher criar um impulso que funcione contra você ou mover a linha de estresse uma caixa a seu favor.
- Se empatar, você pode escolher mover uma caixa de estresse a seu  favor, mas se o fizer, seu oponente ganha +1 em sua próxima jogada.
- Se bem-sucedido, você move uma caixa de estresse a seu favor.
- Se bem-sucedido com estilo, você move duas caixas de estresse a seu     favor ou então apenas uma caixa e ganha um impulso que você pode     usar contra seu oponente em sua próxima jogada.

Cada um dos lados terá seu turno para realizar as manobras e rolagens de perícias. Certifique-se que cada um dos personagens dos jogadores tenha a chance de contribuir na tentativa de escapar. Às vezes a rolagem será muito boa ou muito ruim, mas tudo bem. Mantenha a tensão com boas descrições, falando sobre os detalhes das manobras e seus resultados. Quando um lado ou outro for eliminado ou preencher a linha de estresse, a perseguição chega ao fim. Ou alguém foi capturado, ou alguém escapou.

## Conflito Social

As perícias sociais do Fate Sistema Básico (Comunicação, Empatia, Enganar e Provocar) já permitem uma variedade de formas de abordar conflitos sociais, mas o seu grupo também pode implementar um sistema de **motivações** e **instintos** para criar mais oportunidades de interações sociais entre PJs e PdNs.

### Motivações e Instintos

A lógica social de qualquer PdN — mesmo os sem importância — possui dois componentes: uma motivação e um instinto derivado dessa motivação. Por exemplo, um professor pode querer proteger seus estudantes ( **_Motivação: Proteger Seus Estudantes do Perigo_** ) ao manter os personagens dos jogadores impossibilitados de interrogar um estudante em particular ( **_Instinto: Negar Acesso aos PJs_** ). Qualquer tentativa exigirá que os PJs alterem a motivação do PdN ou convencê-lo de que algum outro instinto serve melhor para aquela motivação.

### Descobrindo e Modificando Motivações

Claro, motivações às vezes são difíceis de detectar. Enquanto alguns PdN anunciarão claramente porque estão se opondo aos PJs — “Estou aqui para vingar o meu pai!” — a maioria das pessoas não costuma declarar suas questões pessoais para o mundo. A fim de descobrir as motivações de um PdN, o PJ precisa criar vantagem usando a perícia social apropriada. Os personagens podem conseguir algumas pistas em diálogo (Empatia), desafiá-lo através de declarações provocativas (Provocar) ou mesmo fingir ser alguém que o PdN possa confiar (Enganar) na intenção de descobrir a motivação do PdN.

Uma vez que os PJs descubram a motivação do PdN, podem tentar alterar essa motivação usando uma variedade de perícias sociais, substituindo a motivação por algum interesse urgente baseado na nova informação (Comunicação) ou na falsa tentativa de convencer o alvo de que sua motivação é baseada em suposições erradas (Enganar). Por exemplo, um guarda leal (___Motivação: Seguindo as Ordens do Chefe, Instinto: Matar os PJs___) poderia ficar assustado por sofrer uma ameaça impressionante (Provocar), feita na intenção de mudar sua motivação para Salvar Minha Própria Vida.

### Modificando Instintos


Ao invés de tentar influenciar as motivações de um PdN, os PJs também podem criar situações que exijam a atenção imediata ou sugiram que um instinto diferente possa servir melhor à motivação original. Por exemplo, os PJs podem iniciar um incêndio (Provocar) na entrada do hotel para distrair o porteiro ( ___Motivação: Manter o Hotel Funcionando Tranquilamente___) ou tentar convencer um traficante a vender drogas mais barato (Comunicação) na promessa que as remessas futuras recompensarão o prejuízo (___Motivações: Ganhar Rios de Dinheiro___). Essa tentativa de mudar os instintos podem mudar um PdN de ___Instinto: Manter os PJs Distantes___ para ___Instinto: Apagar o Fogo!___ ou ___Instinto: Vender as Drogas a Preço de Mercado___ para ___Instinto: Vender as Drogas Baratíssimo___. No final das contas, essas interações sociais dependem da capacidade dos PJs de oferecer o novo instinto como algo melhor para a motivação original ao invés de mudar a motivação original completamente.

---

Talvez tenha notado que a maioria das motivações são aspectos de situação que podem ser invocados ou forçados por pontos de destino. Lembre-se da Regra de Bronze!

---

### Desafios, Disputas e Conflitos
 
A maioria das tentativas de modificar uma motivação ou instinto de um PdN requerirá uma rolagem de superar contra uma oposição ativa; o PdN rolará uma perícia social apropriada para perceber o engodo ou resistir à conversa traiçoeira. Em alguns casos fará mais sentido usar a mecânica do desafio — tentar convencer um juiz a liberar alguém da prisão antes dele passar para o próximo caso — ou a mecânica de conflito — o sindicato e representantes de empresas conduzem uma negociação que deixa ambos os lados esgotados. Os jogadores também devem ter em mente que seus oponentes possuem pontos sociais fortes e fracos: é fácil convencer um guarda leal que seja estúpido, com Empatia Regular (+1) e Vontade Ótimo (+4) que o seu chefe quer que ele deixe você passar para um encontro secreto (Enganar para modificar o instinto) do que para convencer que ele deve abandonar o seu chefe (Comunicação para alterar a motivação).

Michael Romero e Marika Davis são detetives do departamento de homicídios de Los Angeles e estão investigando o assassinato de um socialite rico, Richard Bentley. Após analisar o caso, descobrem que a esposa de Bentley, Sandra Orastin, é a assassina. Quando aparecem para prendê-la, um de seus seguranças deseja impedir que cheguem perto o bastante para fechar o caso ( **_Instinto: Impedir Que os Tiras Prendam Minha Chefe_** ). 

Sabendo que um conflito físico poderia levar à fuga de Sandra, Marika tenta conversar para passar pelo guarda, iniciando com uma rolagem de Provocar para descobrir a motivação do guarda (criar vantagem com Provocar). Ela possui Provocar Bom (+3) e o resultado dos dados é +3, totalizando um Fantástico (+6). O guarda, um PdN sem importância, possui Enganar Regular (+1) e consegue +2 nos dados, revelando sua motivação: ele não quer ser demitido por deixar sua chefe ser presa. O Narrador adiciona o aspecto **_Motivação: Manter Meu Emprego_** com duas invocações grátis, já que Amarika foi bem-sucedida com estilo. Amarika pergunta a ele porque quer proteger uma assassina e o guarda explica que ele precisa fazer o seu trabalho, mesmo não gostando.

Michael, aproveitando o aspecto que Amarika descobriu, decide que vai tentar alterar o instinto do guarda. Ele sugere que se o guarda quer manter o emprego, ele deve ajudar a polícia a prender Orastin. Afinal, ele vai perder o emprego de qualquer forma se Michael e Amarika tiverem que prendê-lo também.

Michael possui um nível melhor em Comunicação (+3) do que em Provocar (+1), então ele decide focar em convencer o guarda, em vez de assustá-lo (ação de superar usando Comunicação). A rolagem de Michael é medíocre (+0), mas ele usa a invocação grátis em **_Motivação: Manter o Meu Emprego_** para melhorar a sua jogada, totalizando Excepcional (+5). O guarda, com Vontade Razoável (+2), consegue uma rolagem terrível (-2), e Michael o convence que trabalhar com a polícia é a melhor forma de preencher sua motivação. Michael ganha um impulso adicional, **_Ajuda dos Guardas_** por seu sucesso com estilo.

# Capítulo 7 - Ferramentas Personalizadas

# Estresse


As caixas de estresse possuem um propósito bem específico no Fate Sistema Básico, mas essa não é a única forma de lidar com elas. Aqui apresentamos outras opções.

___Caixas de uma tensão:___ Cada caixa de estresse absorve apenas uma tensão de dano, mas marque quantas desejar de uma vez. Essa opção torna os personagens consideravelmente mais frágeis.

___Marque duas:___ Marque até duas caixas de uma vez, some seus valores e reduza o dano na mesma quantidade. Essa opção torna os personagens significativamente mais fortes – um personagem com cinco caixas de estresse pode receber até 9 de dano em uma cena sem precisar recorrer às consequências.

___Recuperação reduzida:___ O estresse se recupera gradualmente, não inteiro de uma vez. Ao invés de limpar uma caixa de estresse automaticamente ao fim de uma cena, apague-a e marque a caixa à esquerda dela (assumindo que haja alguma).

___Apenas bônus:___ Os personagens não possuem caixas de estresse no início, mas recebem caixas extras de estresse de perícias como Vigor e Vontade. Isso torna os personagens mais frágeis e enfatiza as consequências. 

___Esforço extra:___ Marque uma caixa de estresse voluntariamente a qualquer momento para receber um bônus. O bônus é igual ao valor da caixa marcada. Isso pode gerar resultados imprevisíveis em seu jogo. Isto não só pode criar criar mais variações nas rolagens, mas também pode substituir parcialmente a economia de pontos de destino. Este recurso pode se tornar um pouco mais viável quando combinado com qualquer uma das opções acima.

___Sem estresse:___ Os personagens não possuem caixas de estresse e não recebem caixas extras de perícias. Ao invés disso, essas perícias concedem uma consequência suave extra (física ou mental) quando for seu nível Bom (+3) ou duas quando for Ótimo (+4). Esta é a opção mais perigosa e dinâmica – cada acerto resulta em uma consequência e possui impacto narrativo – mas pode criar uma quantidade de aspectos difícil de se manejar.

## Consequências


Assim como o estresse, as consequências podem preencher uma variedade de papéis em seu jogo, além do padrão apresentado no Fate Sistema Básico.

___Esforço extremo:___ Receba voluntariamente uma consequência a qualquer momento em troca de um bônus igual ao valor da consequência – uma consequência suave concede +2, uma moderada +4 e uma severa +6. Isso pode ser perigoso, tanto para o PJ quanto para a economia de pontos de destino, como o Esforço Extra, apresentado anteriormente.

___Consequências de grupo:___ Ao invés de mantê-las individualmente, PdNs principais e de suporte compartilham uma reserva de consequências de grupo. Isso lhe fornece a opção de “sacrificar” um PdN de suporte agora para tornar um PdN principal mais desafiador no futuro. Para cada dois PdNs de suporte que espera incluir no cenário, adicione duas consequências de grupo suaves e uma moderada, até o máximo de três moderadas. Para cada PdN principal, adicione outra consequência de grupo suave, moderada e severa. Se um PdN de suporte ou principal conceder o conflito, baseie a recompensa da concessão no número total de consequências que o PdN recebeu durante a cena.

---

O antagonista no jogo de super-heróis de Isa pertence a uma organização terrorista chamada QUIMERA. Minotauro e Scylla são os principais PdNs, enquanto Cerberus, Talos e Cerastes são PdNs de suporte. Com isso, Isa recebe uma reserva de quatro consequências suaves, três moderadas e duas severas para esse grupo de PdNs. 

Em uma das primeiras cenas, os PJs enfrentam Cerberus, Talos e Minotauro. Durante o conflito, Cerberus e Minotauro recebem uma consequência suave cada — então, Cerberus é derrotado, Talos concede a luta e Minotauro consegue escapar. Isa poderia optar por usar mais algumas consequências para manter Cerberus e Minotauro na luta, mas preferiu guardá-las para as próximas cenas. Além disso, ela recebe um ponto de destino pela concessão — apenas um, já que Talos não recebeu uma consequência.

Quando chegam à batalha mais importante, Isa possui uma consequência de grupo suave, uma moderada e duas severas
para usar com Scylla, Minotauro e Cerastes, os três PdNs importantes na cena, à medida que tentam realizar suas maquinações malignas.

---

**Consequências Colaterais:** Adicionalmente ao uso comum das consequências, os jogadores também podem fazer uso de três consequências compartilhadas, uma em cada grau de severidade. Isto representa o dano ao ambiente ou novas complicações na história, coisas como Inocentes Feridos ou Histeria Anti-Mutante. Os jogadores podem usá-las para evitar serem feridos, direcionando esse ferimento para o mundo ao seu redor. Você se livra de uma consequência colateral como qualquer outra, usando uma ação de superar com qualquer perícia que seja apropriada, com duas exceções. A primeira é que isso deve ser feito durante a cena em que a consequência surge. A segunda é que não há demora — ela desaparece imediatamente com uma rolagem boa o suficiente em uma perícia. Essa opção é mais adequada a alguns gêneros, como super-heróis, onde os PJs podem se preocupar mais com o mundo que os cerca.

## Zonas

Mapear as zonas durante um conflito permite uma melhor visualização do espaço físico de uma luta, criando a oportunidade de dominar o campo de batalha para personagens com altos níveis em Atletismo e Físico. Aqui seguem algumas ideias para tornar as zonas uma parte emocionante do seu jogo:

### Movimento Entre Zonas

Em um jogo com muitas zonas, você talvez queira permitir que os personagens sacrifiquem seu turno para moverem-se um número de zonas igual a seu nível de Atletismo ou para remover um número de obstáculos físicos igual a seu nível de Vigor. Personagens rápidos e fortes querem ser rápidos e fortes, mas rolar os dados nem sempre é a melhor forma de representar isso em Fate.

### Zonas Estreitas Podem Ser Dramáticas

Enquanto as zonas dividem grandes áreas como estacionamentos e estádios — mostrando o quão difícil é atravessar um campo de futebol correndo — elas também podem ser usadas para dividir lugares pequenos de formas interessantes. Por exemplo, uma luta em um navio pode acontecer nos alojamentos, o que exigiria que os aliados atravessassem várias zonas para ajudar um amigo em apuros. Ao forçar os jogadores a escolher entre gastar ações se movendo ou oferecer ajuda a alguém distante, as zonas podem tornar um conflito corriqueiro em algo bastante dramático.

### Aspectos de Zonas Perigosos

Zonas também podem criar emoção ao restringir movimento e prover desafios que os personagens precisem superar. Por exemplo, uma zona em um campo de batalha pode possuir o aspecto ___Sob Fogo Pesado___, exigindo que os personagens realizem rolagens de Atletismo para evitarem receber dano conforme correm através do tiroteio. Algumas zonas também podem desaparecer após um número específico de turnos. Pontes em ruínas, barcos naufragando e portas fechando impulsionam os personagens a se mover rápido à medida que o campo de batalha muda ao seu redor. Além disso, dão aos jogadores a chance de forçar os PdNs para essas zonas, de forma que tenham que também tenham que lidar com as ameaças.

### Zonas Mentais e Sociais

Nem todos os conflitos acontecem no mundo físico; personagens em Fate frequentemente adentram conflitos sociais e mentais que podem ser mapeados de formas interessantes. Por exemplo, um cirurgião psíquico pode se orientar através dos sonhos de um paciente, obstruídos por aspectos de situação que devem ser superados através de uma série de rolagens de Investigação e Empatia. As zonas podem detalhar os obstáculos que impedem o paciente de acessar antigas memórias, vestígios de trauma e abuso que os PJs devem enfrentar antes de poder ajudar o paciente a superar o seu passado. Da mesma forma, um Narrador pode detalhar diversos grupos sociais diferentes em uma universidade, indicando qual grupo os PJs devem impressionar antes de ganhar acesso aos alunos mais populares. Em essência, estas zonas mentais e sociais servem para restringir
os jogadores, direcionando-os aos conflitos ao limitar seu movimento.

## Recarga

Se quiser trabalhar melhor o desenvolvimento dos personagens no seu jogo, você pode permitir que eles dediquem uma cena à ativação de sua recarga, adicionando algum drama entre cenas envolventes de ação. Cenas de recarga podem ser:

**Cenas de Flashback:** Sempre que for apropriado, um jogador pode declarar que um evento específico ativou uma cena de flashback. Com a ajuda do grupo, o jogador narra o flashback sobre o passado do personagem, revelando um detalhe surpreendente ou interessante que tenha conexão com o presente, renovando o foco do personagem ou colocando-o de volta nos eixos.

**Cenas de Recuperação:** A critério do Narrador, os de personagens voltam às suas vidas normais para descanso e repouso, no intuito de perseguir seus próprios interesses ou retomar as energias. São cenas fantásticas para o Narrador adicionar novos PdNs à história.

**Cenas de Reflexão:** Um jogador declara uma cena de reflexão enquanto os outros personagens fazem perguntas que revelam informações importantes e interessantes sobre a história do personagem refletindo. Isto é perfeito para histórias cheias de ação, onde o tempo real fora da aventura não é realista.

Essas cenas podem trazer pontos de destinos adicionais aos PJs no meio da sessão, deixando as coisas empolgantes, ou repor completamente a recarga do início da sessão. Sinta-se livre para usar apenas um tipo ou permitir que seus jogadores usem todas.

## Modificando O Cenário: Grandes Mudanças

Embora os personagens em Fate sejam sempre protagonistas, às vezes os jogadores decidem que querem modificar o cenário, limpando as ruas da cidade do crime ou assegurando o reforço de uma fortificação para evitar ataques futuros.

### Descubra os Problemas Antes de Resolvê-los

Primeiramente os jogadores devem reunir as informações necessárias. Talvez os personagens decidam analisar sua própria experiência acumulada — “Não é O’Banner o principal distribuidor de drogas da cidade?” — ou talvez precisem fazer rolagens de Contatos, Investigar ou Conhecimento para descobrir antigas alianças e conhecimento perdido. De qualquer forma, o grupo deve construir uma série de aspectos do cenário que representem o problema existente.

Para limpar as ruas, um grupo poderia listar:

+ ___Traficantes de Drogas___
+ ___Interceptadores e Transportadores___
+ ___Financiadores de Drogas___
+ ___Policiais Corruptos___
+ ___O Rei Do Crime, Marty O’Banner___

### Uma Parte de Cada Vez

Para cada aspecto do cenário que o grupo desejar mudar, precisarão rolar contra uma dificuldade apropriada para resolver o problema. Por exemplo, prender todos os ___Traficantes de Drogas___ pode exigir que os personagens realizem rolagens de Recursos para mobilizar a força policial ou Vigor para capturar pessoalmente os bandidos. Ao mesmo tempo que os personagens podem invocar seus próprios aspectos, o Narrador pode pagar pontos de destino para invocar os aspectos de cenário existentes — como ___Policiais Corruptos___ – para complicar o avanço dos jogadores. 

Para cada rolagem, use o resultado para jogar os personagens no meio de uma situação — como um episódio de uma série de TV — que reflita o resultado da rolagem. Em uma rolagem ruim, os Traficantes de Drogas se esconderam bem e será preciso uma busca profunda para encontrá-los. Com uma rolagem bem-sucedida, o grupo encontra os bandidos, mas precisa fazê-los falar. Seja como for, se os personagens conseguirem resolver a situação, mude o aspecto de cenário para algo positivo, trocando ___Traficantes de Drogas___ por ___Algumas Ruas Mais Limpas.___

Uma vez que comecem progredir, deixe o grupo invocar livremente aspectos de cenários resolvidos anteriormente, quando aplicável, já que o trabalho pode se tornar mais fácil conforme concluem as tarefas da lista.

### Precisamos de Uma Montagem Cinematográfica!

Se não estiver interessado em interpretar as mudanças do dia a dia que ajudam a melhorar um sistema de justiça criminal ou para a fortificação de um castelo, você pode fazer uma montagem com cenas sobre o pro- gresso dos personagens. Determine um limite de tempo — incluindo um número limitado de ações — que represente o tempo e recursos disponíveis aos personagens para resolver a situação antes de retomar a história principal.

Para cada rolagem, os personagens tentam resolver um dos aspectos usando a perícia apropriada. Personagens podem trabalhar juntos ou podem se dividir para tentar resolver vários problemas ao mesmo tempo. Narre as mudanças e o sucesso do grupo como uma montagem cinematográfica, pausando apenas o tempo suficiente para cada rolagem a fim de saber se o grupo foi bem-sucedido ou falhou em melhorar a situação.

### Outros Dados

Ainda não possui os seus dados ou baralho Fate? Abaixo seguem algmas alternativas para fazer seu jogo acontecer.

### D6-D6


Pegue dois dados de seis lados de cores diferentes. Determine um deles como o dado positivo e outro como o negativo, e então, faça a rolagem. Subtraia o dado negativo do positivo para ter um resultado entre -5 e +5. Resultados duplicados sempre são zero. Essa opção cria resultados mais oscilantes do que os quatro dados Fate, e o intervalo de resultados é mais amplo, mas é similar o bastante para funcionar.

### Ligue os Pontos


Esta técnica foi vista pela primeira em “Baby’s First Fudge Dice” por Jonathan Walton. Você pode pesquisar no Google para encontrar uma explicação completa da ideia (em inglês) — procure o link que direciona para a página sinisterforces.com.
Com um marcador permanente e alguns dados de seis lados (com pontinhos, não números), você pode fazer seus próprios dados Fate ligando os pontos. Os lados com 2 e 3 serão o -, 4 e 6 serão 0 e 5 e 1 serão o + — O 1 precisa ser desenhado à mão livre. Tcha-ram — dados Fate artesanais!

### 3d6

Se você não tiver problemas com tabelas (ou se tiver uma boa memória), pode também rolar três dados de seis lados e consultar esta tabela.

| 3  | 4  | 5-6 | 7-8 | 9-12 | 13-14 | 15-16 | 17 | 18 |
|----|----|-----|-----|------|-------|-------|----|----|
| -4 | -3 | -2  | -1  | 0    | +1    | +2    | +3 | +4 |

### 4d6

Esta opção não dá trabalho algum, mas talvez não seja tão intuitiva: role quatro dados de seis lados e considere 1 e 2 como -, 3 e 4 como 0 e 5 e 6 como +.

## Escala

Se você espera que os temas e questões de seu jogo causem conflitos entre entidades de diferentes tamanhos e escalas — dragão contra cavaleiro, nave pequena contra estação espacial ou pequenas empresas contra megacorporações, por exemplo — considere estas regras. Caso contrário, é melhor pular esta sessão.

Primeiro, defina a **escala** desejada; três ou quatro níveis devem bastar. O que essa escala representa dependerá da natureza da campanha e das histórias que deseja contar, mas sempre reflete coisas coisas progressivamente maiores ou mais poderosas no cenário e ela frequentemente aparecerá nos conflitos.

Quando duas entidades importantes do cenário entram em conflito, a diferença entre seus níveis entra em jogo. Para cada nível de diferença entre eles, aplique um ou ambos os efeitos a seguir no maior dos dois: +1 em uma rolagem de ataque _ou_ +1 em uma rolagem de defesa Causar +2 tensões de dano em um ataque bem-sucedido ou reduzir um dano recebido em 2
A aplicação desses efeitos depende do que fizer sentido no contexto.

Claro, se o conflito for entre duas entidades de mesmo porte ou nível de escala, então nenhum dos efeitos se aplicam. Eles só entram em jogo quando os níveis são _desiguais_.

**Escala como Extra:** A maioria das coisas em seu cenário terá um nível de escala baseado no que são de acordo com o senso comum — todas as gangues são Locais, por exemplo — mas você pode dar aos jogadores a chance de alterar a escala de seus personagens, posses ou recursos, na forma de um extra que possam receber, se isso se enquadrar no seu conceito de personagem.

---

O jogo de Eric é sobre guerra entre sociedades secretas de tamanho e influência variadas. Sua escala possui níveis chama-
dos: Local (limitado a uma única cidade), Regional (várias cidades) e Generalizado (todas as cidades).

Se a Gangue do Machado (nível: Local) inicia um ataque ousado contra a Associação Beneficente dos Viajantes Celestiais (escala: Regional), a Gangue do Machado passará por apuros, em termos de recursos e de pessoal, contra a Associação que é melhor financiada. Faz sentido conceder à Associação um bônus de +1 em uma rolagem de defesa de Lutar.

---

## Companheiros e Aliados


Jogadores podem escolher tanto companheiros quanto aliados na forma de extras, mas diferenciar um garoto prodígio de um esquadrão de agentes profissionais pode ser complicado. Seguem algumas formas de distinguir companheiros, que trabalham em colaboração com os PJs, de aliados treinados que podem ser chamados em situações específicas.

### Permissões e Custos


Embora tanto companheiros quanto aliados sejam extras (veja Extras, página 247 no Fate Sistema Básico), companheiros tendem vir de um relacionamento pessoal e aliados surgem de organizações e autorizações conseguidas com recursos. Por exemplo, Sherlock Holmes pode selecionar o aspecto ___Meu Bom Amigo Dr. Watson___, para adicionar Watson como seu companheiro, e ter o aspecto Contatos Incomuns em Baker Street para representar as crianças que usa como espiãs e mensageiras. Quanto ao custo, companheiros costumam impor custos sociais e de relacionamento com os personagens — pedindo que contribuam a causas importantes ou pedindo ajuda em momentos difíceis — enquanto aliados exigem algum pagamento direto, favores ou outros recursos. 

### Estresse e Consequências

Um ótimo recurso para diferenciar companheiros e aliados é usar caixas de estresse e consequências em conflitos. Companheiros são como personagens de suporte — forneça-lhes uma linha de estresse limitada com uma consequência suave (talvez uma moderada) e uma extrema, que podem mudar a planilha do personagem em um momento dramático. 

Aliados, por outro lado, são como capangas sem muita importância, que possuem uma caixa de estresse para cada membro do grupo. Infligir estresse em aliados os retira do conflito completamente ao invés de deixá-los com consequências que durarão várias cenas. Essa distinção torna mais difícil de derrotar uma gangue de aliados com um único ataque, mas faz com que companheiros sejam mais flexíveis e resistentes em diversas situações sociais e físicas, podendo utilizar suas consequências para defender os PJs.

### Aliados Permanentes ou Temporários

Companheiros quase sempre são uma parte permanente da planilha do personagem, mas aliados podem ser temporários, dependendo das necessidades do PJ. Por exemplo, um oficial da inteligência pode ter o aspecto Enviado Pelos Federais para possuir um grupo de agentes federais de plantão, ao custo de um ponto de recarga. É igualmente plausível que tal personagem possa abrir mão desses aliados permanentes para criar uma vantagem para a próxima cena, usando Contatos ou Recursos. Claro, aliados permanentes devem ser mais poderosos e com mais caixas de estresse, aspectos e façanhas adicionais do que aliados temporários, que são pouco melhores que um multidão genérica.

## Riquezas

A importância do dinheiro em um jogo depende bastante do gênero. O Fate Sistema Básico sugere o uso da perícia Recursos, mas para alguns gêneros em que adquirir riqueza frequentemente seja a motivação principal, como um jogo cyberpunk ou a clássica fantasia medieval em masmorras, você pode querer algo um pouco mais flexível.

### Estresse de Riqueza

Uma alternativa é adicionar uma nova linha de estresse: Riqueza. Quando tenta realizar algo que pode se tornar mais fácil com a aplicação de dinheiro, você marca uma caixa de estresse de riqueza para receber um bônus em sua rolagem (veja a variante de estresse Esforço Extra, na página 49). 

Por exemplo, se estiver debatendo o preço de um propulsor usando Comunicação — ou Provocar, se estiver barganhando de forma desagradável — e falhar em sua rolagem, você poderia marcar uma caixa de estresse de riqueza para pagar caro pelo produto. Talvez esteja planejando um assalto e falhou em sua rolagem de Roubo? Marque uma caixa de estresse de riqueza para conseguir as ferramentas caras que precisa para o serviço. Não consegue encontrar o assassino que procura usando Contatos? Talvez marcar uma caixa de estresse de riqueza refresque a memória dos seus informantes, e assim por diante. Em outras palavras, representa ser bem-sucedido a um alto custo que neste caso é um custo literal.

### Riqueza Inicial

Isto depende bastante do seu jogo — os PJs são profissionais com emprego fixo ou vivem no aperto? —, mas começar com uma base de duas caixas de estresse de riqueza seria uma média boa. PJs recebem +1 caixa de estresse para cada aspecto que possuam relacionado a isso, até um máximo de três caixas extras de estresse.

### Obtendo Riqueza

Estresse de riqueza não desaparece por conta própria. Em vez disso, essas caixas só são restauradas quando você ganha mais bens – ouro, créditos, trocas ou qualquer coisa que tenha valor em seu jogo. Adquirir uma quantia de riqueza permite que você limpe qualquer número de caixas de estresse cujo o valor total não exceda o valor dessa quantia. Se uma ou mais caixas marcadas em sua ficha forem de um valor maior do que a quantia recebida, não faça nada com as caixas que excederem esse valor — não é riqueza o suficiente para fazer diferença a você. Por exemplo, se você adquire 3 de riqueza, poderá limpar sua primeira, segunda ou terceira caixa de estresse, se qualquer uma delas estiver marcadas, ou a primeira e segunda – mas não uma quarta, se for sortudo o bastante para ter uma.

A única forma de adicionar caixas de estresse de riqueza é através de um marco, ao trocar um aspecto existente por um que esteja ligado a algum tipo de riqueza.

## Armas e Armaduras Alternativas

No sistema Fate padrão, armas e armaduras — assim como qualquer outro equipamento — são simplificados de forma que sua diferença seja apenas cosmética. Possuir uma arma permite que você ataque alguém usando Atirar, mas nada além disso. Se quiser que armas e armaduras sejam mais importantes em seu jogo, o capítulo Extras no Fate Sistema Básico (página 247), apresenta regras para potência de Armas e Armaduras. Se mesmo essas regras não suprirem suas necessidades, eis algumas novas alternativas.

### Dano Mínimo e Máximo

No Fate Sistema Básico, armas são _perigosas_, se você usar as regras do capítulo Extras. Um espadachim pouco treinado com sorte nos dados ao usar uma espada de duas mãos pode partir um homem ao meio sem muito esforço, e isso pode ser assustador. Isso leva a um jogo mais realista e letal. Se essa for sua praia, talvez prefira armas e armaduras com valores que as deixem menos letais, mas ainda importantes.

Ao usar essas regras, os valores de potência de uma arma ainda começam em 1 e aumentam a partir disso, mas podem ir um pouco mais além (digamos 5 ou 6, para as armas mais letais). Em vez de simplesmente adicionar o nível da arma às tensões que conseguiu, o valor da arma fornece um número _mínimo_ de tensões de estresse que você pode provocar com aquela arma. Por exemplo, uma espada longa com Arma:3 causa no mínimo três tensões, mesmo se você rolar 1, 2 ou 3 de tensões de dano em seu ataque. Se rolar acima disso, simplesmente considere o valor e ignore o dano mínimo.

Já as armaduras funcionam da forma contrária; elas especificam o número máximo de tensões de estresse que você pode rece-
ber em um ataque. As armaduras começam com valor 4 (para armaduras leves), decrescendo até 1 (armas de placas muito pesadas ou trajes de combate avançado). A exceção a essa regra é quando o ataque é bem-sucedido com estilo — neste caso você deve ignorar o valor de Armadura. O atacante causa o dano total quando consegue um sucesso com estilo.

_Valores de Armadura são mais importantes que o valor de Arma._ Isso significa que se alguém com Arma:5 atacar alguém com Armadura:3, causará apenas 3 de estresse a não ser que seja bem-sucedido com estilo.

### Aspectos de Armas e Armaduras

[Anteriormente neste livro][] mencionamos que você poderia transformar aparatos importantes em aspectos. Isso funciona para a maioria dos equipamentos, mas algumas pessoas talvez queiram algo mais ao lidar com armas e armaduras. Esta modificação nas regras foi feita para ser usada em conjunto com aquele sistema, mas não há nada que o impeça de usá-la sozinha — isso só significaria que _apenas_ armas e armaduras são aspectos, outros aparatos, não.

As armas são divididas em **leves** , **médias** e **pesadas**. Uma arma leve pode ser uma faca pequena ou porrete, uma arma média pode ser algo como uma espada ou pistola e uma arma pesada talvez seja uma espingarda, um rifle de precisão ou uma enorme espada de duas mãos.

Quando for bem-sucedido em um ataque usando uma arma, você pode fazer uma invocação especial com ela. Isso custa um ponto de destino, mas não fornece +2 nem um novo lance de dados. Em vez disso, você pode forçar o seu oponente a receber uma consequência ao invés de estresse. Com uma arma leve, você pode forçar alguém a receber uma consequência suave. Uma arma média força uma consequência moderada e uma arma pesada força uma consequência severa. Se bem-sucedido com estilo,
eleve a severidade da consequência em um tipo — suave se torna moderada, moderada se torna severa, severa para ou derrotar o adversário, ou uma consequência extrema (à escolha ~~da vítima~~ do defensor). Se a caixa da consequência já estiver em uso, eleve a consequência em um nível de severidade.

Essa invocação especial também funciona um pouco como forçar um aspecto. Quando invoca um aspecto de arma desta forma, você oferece um ponto de destino ao seu alvo. Se ele aceitar o ponto, você impõe a consequência. Ele pode _recusar_ o ponto e pagar um dos seus próprios para não receber a consequência, recebendo então o estresse que teria recebido de qualquer forma.

Aspectos de armadura também são divididos em leve, médio e pesado, além de também permitirem invocações especiais. Você pode invocar uma armadura leve para absorver uma consequência suave; você não recebe o estresse que foi absorvido e não preenche a caixa de consequência. Armaduras médias podem absorver consequências moderadas. Armaduras pesadas absorvem consequências severas ou duas consequências suaves ou moderadas em qualquer combinação. Você pode usar sua armadura para absorver consequências além desses limites, mas se o fizer, o aspecto da armadura muda imediatamente para algo que represente um defeito e ela não estará mais disponível para absorver ataques. Será necessário consertá-la, o que pode envolver custos e algumas horas ou dias, dependendo do tipo de armadura usada.

### Dados Vermelhos e Azuis


Se você tiver ao menos três cores de dados Fate e quiser algo que não seja tão previsível para suas armas e armaduras, poderá usar a regra dos Dados Vermelhos e Azuis em lugar dos valores de Arma e Armadura. Usamos os termos “vermelho” e “azul” aqui apenas por conveniência; use as duas cores que preferir, contanto que não sejam usadas para mais nada. 

__Vermelho para armas:__ quanto mais Vermelho sua arma possui, maior e/ou mais letal ela será. Uma arma Vermelho:1 pode ser uma adaga, as garras de um goblin ou uma pistola de baixo calibre, enquanto uma arma Vermelho:4 poderia ser uma espada larga, a mordida de um dragão ou um tiro a queima roupa de uma espingarda. 

Quando realizar um ataque, para cada ponto Vermelho que sua arma possuir, substitua um dado Fate normal por um dado Fate Vermelho. Se o ataque for um empate ou melhor, cada dado vermelho com resultado + aumenta a tensão do golpe em +1.

__Azul para armaduras:__ Quanto mais Azul sua armadura possuir, mais proteção ela oferece. Armaduras leves como de couro curtido ou couro firme são Azul:1. Armaduras pesadas, como cotas de malha ou armaduras de placas, são Azul:2 ou 3.

Ao se defender, para cada ponto de Azul que você possuir, substitua um dos dados Fate normal por um dado Fate Azul. Se sua defesa falhar ou empatar, cada um de seus dados azuis com resultado + absorve uma tensão de dano. 

Como você sempre lança quatro dados Fate, o número máximo de dados Vermelhos ou Azuis que você pode rolar será quatro.

Para misturar ainda mais as coisas, você pode conceder dados Azuis a armas defensivas, como um bastão ou faca de combate, e dados Vermelhos a armaduras “agressivas” , como armaduras de placas com espinhos ou campo de força eletroestático. Você também pode ter façanhas que permitam trocar dados Fate Azuis por Vermelhos quando defender — por exemplo um mestre de esgrima com um contra-ataque matador, ou um escudo mágico flamejante.

## Ações Suplementares

Ações suplementares não aparecem em nenhum lugar no Fate Sistema Básico, apesar de terem aparecido em versões mais antigas do Fate. Fizemos isso por dois motivos. Primeiro, sentimos que é melhor usar bônus do que penalidades, sendo que ações suplementares impõem uma penalidade. Segundo, ações suplementares existem para tornar as coisas mais “realistas”, mas não tornam as coisas necessariamente mais divertidas ou a história mais empolgante. Elas dificultam fazer coisas legais, ao invés de facilitar.

Isso não significa, porém, que o conceito não tenha valor. Se quiser usar ações suplementares, poderá usá-las como são mostradas nas versões anteriores do Fate — realizar uma ação secundária que irá lhe distrair de sua ação principal impõe uma penalidade de -1 na ação principal. A maioria dessas ações são ações gratuitas no Fate Sistema Básico. Deixamos que você determine quais tipos de ações gratuitas passariam a ser suplementares.

Se quiser algo um pouco mais no estilo do Fate Sistema Básico, que não imponha uma penalidade, tente o seguinte.

---

### Ação Suplementar

Ao realizar alguma ação menor em conjunto com sua ação principal — mover-se uma zona, pegar uma arma, procurar cobertura ou algo mais que o Narrador determine como ação suplementar — você cria um impulso, algo como ___Distraído___, ___Visão Obstruída___ ou ___Mira Instável___, que permanecerá até o início de seu próximo turno. Você só pode executar uma ação desse tipo, então criará apenas um impulso por vez. Qualquer um que esteja agindo contra você ou se defendendo contra um ataque seu pode usar esse impulso — que então desaparece como um impulso normal. Além disso, o Narrador pode forçá-lo contra você uma vez de graça — o que significa que não precisa lhe oferecer um ponto de destino por isso, mas você ainda precisará pagar um ponto para resistir —, também fazendo com que o impulso desapareça em seguida.

# Capítulo 8 - Magia

## Introdução

Quando o seu personagem realiza uma ação em jogo, espera-se que ela faça sentido para você. Você pode imaginá-la com certa clareza e possui o senso instintivo sobre como as coisas funcionam para que possa se divertir sem esquentar muito a cabeça. Considere o número de elementos envolvidos ao dar um soco em alguém: suas mãos estão livres? Você consegue
movê-las? Está perto o suficiente? Já preparou os punhos? 

Não paramos nem falamos sobre esses passos em jogo porque está subtendido que eles fazem parte da ação de dar um soco. Essa clareza diminui à medida que avançamos para áreas fora de nossa experiência, mas de modo geral é possível compreender a cadeia de ações que levam a um resultado.

Na magia isso não existe. Não temos a mesma base de experiência para usar de base quando começamos a lançar raios e trovões. Portanto, tentamos encontrar regras e lógica que tornem a magia mais familiar para nós, o que é um paradoxo. A magia é, em sua natureza, algo fictício e os escritores e criadores estão mais interessados em como ela os ajuda a contar histórias do que nas regras internas.

Jogos, por outro lado, precisam de regras. A consistência das regras cria a funcionalidade — sem lógica e coerência, é pura loucura.

A boa notícia é que há um ponto simples no qual você pode se escorar. Embora seja verdade que a magia seja uma conveniência entre autores, aqueles que a usam sem pensar no que fazem acabam produzindo uma fantasia simplista e sem graça. Dar regras à magia não serve apenas para tornar o jogo melhor, mas também para tornar a ficção melhor. Se você consegue encontrar o ponto onde essas duas prioridades se sobrepõem, então estará no caminho de criar um grande sistema de magia.

---

Qualquer ação suficientemente confusa é indistinguível de magia.

---

### O Básico


O primeiro fator diz respeito à natureza da magia. Magia neutra é uma força, como a eletricidade ou gravidade, que funciona semelhante a uma ferramenta, enquanto uma força diferenciada ou responde, ou tende a certos resultados. O exemplo mais comum disso seria uma magia que tende à luz e às trevas e que talvez funcione de formas diferentes em cada extremo do espectro. Neste caso a magia não é necessariamente uma força inteligente, mas possui tendências. Por exemplo, o fogo tende a queimar, terra tende a ser estável. Magia opiniosa deriva de alguém. Talvez de um deus ou anjo, talvez de alguma monstruosidade horrível fora do espaço e do tempo. Quem quer que sejam, possuem intenções próprias e a magia é uma de suas ferramenta na busca de seus objetivos. Há muito espaço para nuances — a magia pode ser neutra em seu uso, mas a fonte pode ser opiniosa. Por outro lado, se a magia realmente invoca ou canaliza esses seres, então a manifestação real da magia pode ser moldada por seus desejos.

### O Que é Magia?

Então, o que é magia e como ela funciona?

Não há uma resposta simples para isso e, apesar dessa ser a ideia, isso acaba sendo bastante frustrante. Poderíamos dizer que magia é uma forma de fazer coisas que de outra forma seria impossível, ou um meio alternativo de fazer coisas já possíveis, mas não é o bastante. Você poderia fazer como a Arthur C. Clarke e tratá-la como um tipo diferente de ciência. Talvez considerá-la como um sistema de preços, riscos e recompensas. Você pode considerá-la algo que vem de outra pessoa — alguém horrível ou incrível, dependendo do caso.

Você ainda deixaria lacunas, mas para facilitar a aplicação vamos organizar alguns tópicos chaves e reduzi-la a cinco fatores.

+ __Tom:__ A magia é uma força neutra, diferenciada ou possui opiniões próprias?
+ __Custo:__ A magia exige um preço, um risco ou nenhum dos dois?
+ __Limites:__ A magia segue regras rígidas? É flexível e livre? Quais são seus limites?
+ __Disponibilidade:__ A magia é universalmente disponível a ponto de qualquer um no cenário poder usá-la? É rara o bastante para apenas algumas pessoas a possuírem, possivelmente incluindo todos os PJs? É rara o bastante para apenas um ou dois PJs terem acesso, talvez?
+ __Fonte:__ De onde vem a magia?

#### Tom

O primeiro fator diz respeito à natureza da magia. _Magia neutra_ é uma força, como a eletricidade ou gravidade, que funciona semelhante a uma ferramenta, enquanto uma _força diferenciada_ ou responde, ou tende a certos resultados. O exemplo mais comum disso seria uma magia que tende à luz e às trevas e que talvez funcione de formas diferentes em cada extremo do espectro. Neste caso a magia não é necessariamente uma força inteligente, mas possui tendências. Por exemplo, o fogo tende a queimar, terra tende a ser estável. _Magia opiniosa_ deriva de alguém. Talvez de um deus ou anjo, talvez de alguma monstruosidade horrível fora do espaço e do tempo. Quem quer que sejam, possuem intenções próprias e a magia é uma de suas ferramenta na busca de seus objetivos. Há muito espaço para nuances — a magia pode ser neutra em seu uso, mas a fonte pode ser opiniosa. Por outro lado, se a magia realmente invoca ou canaliza esses seres, então a manifestação real da magia pode ser moldada por seus desejos.

#### Custo

O segundo fator trata dos custos no uso da magia. Para alguns, é essencial que a magia tenha um custo, que haja uma negociação em troca do poder. Esse custo pode ser literal ou simbólico, mas quando existe então fica subentendido que o poder tem um preço. Contraste isso com a magia que tem algum _risco_ associado a ela. Tal como um preço, isso coloca um limitador natural no uso da magia, mas com diferentes conjuntos de prioridades, especialmente se a magia for algo corriqueiro. O risco pode ser óbvio — como magias que possuem risco de explodir na sua cara — ou pode ser sutil — uma toxicidade cumulativa —, mas com que faça com que cada uso de magia seja considerado com cautela. Além disso, custos funcionam bem com magias diferenciadas e opiniosas, enquanto riscos são melhores com magia neutra ou opiniosa — onde o risco é “chamar a atenção dos seres com opiniões próprias”.

Não ter custo é uma questão curiosa e que não deve ser aplicada de forma muito literal. Geralmente há _algum_ custo, mesmo se for um chapéu pontudo e o custo da oportunidade de estudar magia ao invés de obter aquela pós-graduação. Estes são custos mundanos. É por isso que esta abordagem funciona melhor com magias neutras altamente regulamentadas. Ela se enquadra bem na vertente de “magia como ciência” ou em listas concretas de magias e efeitos — ou com regras para coisas como implantes cibernéticos, que são basicamente sistemas de magia com outra aparência. Seja qual for o caso, se não há custo nem ou risco, normalmente há algum outro fator limitador, mesmo que a magia seja bastante onipresente – como limites nos tipos de magia que uma pessoa possa usar.

#### Limites

O terceiro fator é um pouco enganoso, pois também está ligado à tolerância em sua mesa de jogo. Sistemas de magia rigorosos com listas de magia e efeitos pré-definidos atraem a alguns jogadores, enquanto outros preferem sistemas mais soltos e interpretativos. Há muitas possibilidades entre uma coisa e outra, como sistemas mais livres, mas limitados por algo como elementos ou esferas.

Seja qual for sua preferência, isso lhe ajuda a pensar sobre o que a magia não pode fazer. É mecanicamente mais fácil fazer um sistema onde a magia pode fazer _tudo_ — criar uma perícia mágica e então deixar os jogadores rolagem para tudo que descrever magicamente — mas a tendência é que isso se torne entediante. Limites formam boa parte do que torna magia algo mágico e, em retorno, são boa parte de como ela pode ser implementada no jogo.

#### Disponibilidade

O quarto fator lhe diz algo sobre o cenário, claro, mas também responde a uma pergunta importante sobre o equilíbrio e o foco do jogo. Um sistema de magia que esteja disponível a todos os personagens pode ser bem diferente de outro onde apenas um dos jogadores possa acessá-la. Se apenas um personagem possuir acesso à magia, então é importante que essa magia não seja tão poderosa ao ponto desse personagem ofuscar os outros, roubando a atenção, mas também não tão fraca que o jogador se sinta um idiota por ter feito essa escolha. Se, por outro lado, todos possuírem magia, há muito mais terreno para explorar. Quando todos podem fazer coisas incríveis, o equilíbrio deixa de uma dor de cabeça tão grande.

#### Fonte

O último fator é o mais e também o menos importante — não importa muito _qual_ a resposta, o que importa é que você _tenha uma_. Quanto melhor você entender a origem da magia, mais fácil será para entender o que ela pode fazer e, o mais importante, o que ela não pode. Não é obrigatório dividir essas explicações com seus jogadores e na verdade esta é uma questão em que aconselhamos discrição. Não é porque não pode confiar essa informação aos jogadores, mas porque seu sistema de magia vai parecer menos impactante quando tudo for explicado. Um pouco de mistério é essencial para a atmosfera mágica.

Observe que nenhum desses fatores pergunta “O que a magia faz?”, já que a resposta a isso é outra pergunta: O que ela _precisa fazer_? Esperamos que você tenha alguma ideia a respeito disso, porque se não souber, nada vai funcionar. “Porque eu preciso ter um sistema de magia” não é uma boa resposta.

### Magia e Fate

O objetivo destas regras fornecer as ferramentas para traduzir suas explicações e imaginação em uma estrutura que possa ser compartilhada. Isso pressupõe que você possua algo que _deseja_ compartilhar.

Fate é um jogo de representação. Ou seja, se você tem uma ideia na cabeça, ele te dá as ferramentas necessárias para expressá-la em jogo. Precisa que os personagens sejam capazes de realizar algo? Certifique-se que possuam uma perícia para isso. Há algum truque que deseja que realizem? Crie uma façanha. Gostaria de reforçar um elemento temático? Coloque um aspecto.

Essas mesmas ferramentas estão disponíveis para quando você quiser colocar magia em seu jogo. Porém, como para o restante do jogo, não há apenas uma ferramenta certa para isso. Dependendo de como a magia funcionaria em seu jogo, mecânicas diferentes podem ser adequadas para apresentá-la.

Os sistemas de magia que seguem possuem duas finalidades. Primeiro, cada um deles é um sistema de magia funcional que pode ser colocado em seu jogo ou modificado para servir às suas necessidades. Isso é importante, mas é quase secundário para o outro fim. Cada um desses sistemas são, também, um exemplo de como aplicar as mecânicas para criar certos efeitos.

Assim terminamos esse vai e volta. Se você conhece bem as regras do Fate, então será fácil tomar decisões para algo simples como um soco e apenas um pouco mais complicado criar seu próprio sistema de combate. Ao chegar no final desta seção, o objetivo é que você se sinta igualmente confortável em pegar conceito de magia que você vem imaginando e traduzi-la em mecânicas com a mesma facilidade que toma decisões corriqueiras.

#### Perícias Mágicas

Perícias são um caminho fácil para lidar com magia, qualquer que seja a perícia usada. A questão principal é se usará perícias existentes ou se será necessário criar uma nova perícia mágica. Cada uma dessas abordagens possui seus pontos fortes específicos e vale a pena considerá-los quando estiver desenvolvendo um sistema. Se pretende usar perícias existentes, então acabará com o desafio de cobrir todas as perícias. É possível, claro, optar por tornar apenas uma perícia mágica, mas é preciso ter cuidado para não criar perícias poderosas demais dessa forma.

Criar uma perícia nova pode resolver vários problemas, especialmente por poder criar várias perícias, se desejar diferenciar as diferentes áreas da magia. Há também um custo sutil nisso, já que comprar essa perícia significa negligenciar alguma perícia “real”.

Como não há resposta certa, quando em dúvida, crie uma nova perícia. Modificar uma perícia existente é mais trabalhoso e é algo que só deve ser feito quando você tiver uma visão realmente clara de sua funcionalidade.

#### Aspectos Mágicos


Aspectos possuem dois papéis importantes na maioria dos sistemas de magia — como porta de entrada e como uma expressão. Como porta de entrada, quase todo sistema de magia vai exigir que o personagem possua ao menos um aspecto que reflita sua tradição mágica ou fonte de poder. Embora haja exceções — como em mundos onde a “magia” é apenas uma tecnologia com caráter especial — saber fazer magia normalmente é importante o suficiente para o personagem se considerar usar um aspecto.

Aspectos também são uma ótima maneira de representar efeitos mágicos. A forma mais simples e fácil de incorporas isso é fazer um sistema de magia onde ela simplesmente amplia as possibilidades de criação de aspectos através de vantagens ou impulsos.

#### Façanhas Mágicas

Façanhas podem certamente servir como base para um sistema de magia, especialmente se as façanhas simplesmente fizerem coisas claras e explícitas. No entanto, esse modelos na maioria das vezes é melhor para um sistema de __poderes__. A diferença é pontual, mas poderes cabem melhor a monstros e super-heróis. Ainda assim, façanhas podem ser um jeito interessante de dar um toque especial no sistema de magia, mas os custos devem ser considerados com cuidado. Frequentemente, um sistema de magia tem um custo de recarga intrínseco, o que torna escolher façanhas algo perigoso. Ou os custos devem ser ajustados, ou as façanhas devem realmente valer a pena.

#### Extras Mágicos


Extras, do jeito que são escritos, são basicamente seu próprio sistema de magia. Um sistema de magia em si talvez forneça explicações e justificativas extras específicos, mas o sistema por si só é robusto e fácil de ser usado para qualquer tipo de efeito.

## Sistemas

Quer magia? Temos magia pra você bem aqui!

### Senhores das Tempestades

#### **Notas de Criação**

Fãs de _The Dresden Files RPG_ perceberão algumas similaridades entre estas regras e as regras de Evocação de DFRPG. Não é coincidência.

Este é um sistema estruturado, em sua maior parte, com um pouco de liberdade interpretativa dentro dos limites estabelecidos. Ele depende da perícia “Tempestade” e ao menos um aspecto. A fonte da magia é bem explicita, mas seu uso é limitado. Do jeito que está, ela é equilibrada mesmo se apenas um jogador escolher jogar como um Senhor da Tempestade, mas também comporta vários num mesmo grupo. 

Por padrão, é assumido que personagens que não sejam Senhores da Tempestade usem as regras opcionais de valores de [Armaduras e Armas][]. Se você busca equilibrar um Senhor da Tempestade no meio de outros personagens comuns, o Senhor da Tempestade não usa esses bônus. Se o seu jogo não usa as regras de valores para Armaduras e Armas, aumente o custo de Recarga de 1 para 2.

#### **Descrição**

As Cinco Grandes Tempestades se enraivecem no coração da criação, cada uma grande o bastante para partir estrelas, mas mantidas sob controle em uma dança precisa de criação e destruição. Terremoto, Dilúvio, Geleira, Inferno e Trovão, cada uma representa as fontes ilimitadas de poder, e feiticeiros mortais encontraram uma forma de explorar essas tempestades para alimentar suas próprias ambições.

#### **Versão Resumida**

Não quer ler todas as regras? Use esta versão resumida:

- Se o seu jogo usa as regras de valores de Armadura/Arma, reduza a Recarga em 1. Se não, reduza a recarga em 2.
- Escolha um tipo de Tempestade entre Terremoto, Inundação, Geleira, Inferno e Trovão e crie um aspecto **_Senhor do(a) [tipo]_** ,    por exemplo **_Senhor do Terremoto_** , **_Senhor da Avalanche_** , etc. 
- Compre a perícia Tempestade.
- Use a perícia Tempestade para atacar, defender e criar vantagem,    desde que a descrição de sua ação inclua o elemento do seu tipo    de tempestade.

#### Mecânica

Personagens em busca do poder da Tempestade deverão fazer o seguinte:

- Se o seu jogo usa as regras para valores de Armadura/Arma, reduza    a Recarga em 1. Se não, reduza em 2.
- Escolha um aspecto que reflete com qual Tempestade o personagem tem sintonia: Terremoto, Dilúvio, Geleira, Inferno e Trovão.     Isso pode ser tão simples quanto “Sintonia com Terremoto”, mas     não se limita apenas a isso. Contanto que o aspecto mostre clara    mente com qual tempestade o personagem possui vínculo, o termo    em si pode variar.
- (Opcional) Comprar níveis maiores na perícia Tempestade.

#### Aspectos De Tempestade

Aspectos de Tempestade serão bastante úteis quando realizar rolagens da perícia Tempestade, mas também carregam parte da sintonia com sua tempestade específica. Isso assume a forma de efeitos passivos, bem como coisas específicas pelas quais aquele aspecto pode ser forçado ou invocado.

##### Terremoto

Terremotos derrubam montanhas e fazem brotar outras novas. Para usar o poder do Terremoto é preciso uma profunda estabilidade pessoal e embora isso pode promover a força, também pode tornar um pouco mais difícil de ganhar embalo.

+ __Efeito Passivo:__ O personagem nunca perde o equilíbrio, não importa quão precária seja a situação, a menos que seja derrubado ativamente.
+ __Invocar:__ Resistir — Qualquer ação que dependa de paciência, determinação ou persistência pode se beneficiar de Terremoto.
+ __Forçar:__ Atraso — Quando uma ação rápida for necessária, Terremoto pode forçar um atraso.

##### Dilúvio

O Dilúvio não pode ser contido. Ele ataca de todas as direções com uma força esmagadora, sutileza ou com uma paciência infinita, sempre em conformidade com as necessidades da situação. Nada resiste ao Dilúvio e a única esperança é seguir com ele e esperar o melhor. Aqueles que possuem sincronia com ele compartilham de um pouco dessa flexibilidade.

+ __Efeito Passivo:__ Enquanto for possível nadar, o personagem pode se manter flutuando facilmente por quanto tempo for necessário, até mesmo dormindo na água.
+ __Invocar:__ Flexibilidade — Ao fazer algo ousado, como usar uma perícia para algo fora do comum, use isso como um bônus.
+ __Forçar:__ Desordem — A água é sútil e potente, mas também faz a maior sujeira. Forçar deixa traços de sua passagem nos momentos mais inconvenientes.

##### Geleira

Onde houver terra, a Geleira estará por perto, inevitável e inflexível, estilhaçando-se mil vezes até superar o que estiver em seu caminho. A sintonia com a Geleira concede parte dessa inevitabilidade ao Senhor da Tempestade.

+ __Efeito Passivo:__ Baixas temperaturas dentro do normal não incomodam o personagem.
+ __Invocar:__ Empurrão — Seja para abrir uma porta ou para tirar um serviçal do caminho, o personagem se beneficia quando se move adiante enquanto tira as coisas de seu caminho.
+ __Forçar:__ Esforço Demasiado — A Geleira não consegue ser tão flexível, e o Senhor da Avalanche pode se pegar dedicando muito esforço numa mesma atividade ou raciocínio.

##### Inferno

O Inferno consome. Seu apetite é interminável e não há nada que não seja combustível para suas chamas infinitas e turbulentas. 

+ __Efeito Passivo:__ Temperaturas altas dentro da normalidade não incomodam o personagem.
+ __Invocar:__ Destruir — Não combater ou ferir, destruir. O Inferno só deseja isso, nada mais, nada menos.
+ __Forçar:__ Consumir — Recursos, comida, boas opiniões e fortuna, um Senhor do Inferno possui o mau hábito de usá-los sem pensar duas vezes.

##### Trovão

Aqueles que diferenciam o Trovão dos relâmpagos mostram que não possuem real compreensão. O Trovão é a expressão súbita e poderosa da força, seja o raio que corta o céu ou o som que o apresenta. Aqueles que o invocam compartilham de sua potência.
+ __Efeito Passivo:__ Sua voz é forte. Se você consegue ver alguém bem o suficiente para identificá-lo, você pode gritar alto o suficiente para ser ouvido por ele — e todos no caminho — independente das condições.
+ __Invocar:__ Agir Decisivamente — Quando for necessária uma ação rápida devido a uma mudança de circunstâncias — e não apenas numa rodada após outra em um combate — então invoque isto por um bônus.
+ __Forçar:__ Oprimir — Às vezes sensibilidade, sutileza e precisão são necessárias. Às vezes um Senhor da Tempestade subestima isso.

#### A Perícia Tempestade

A perícia Tempestade é usada para invocar o poder das tempestades para todo tipo de atividade — frequentemente violenta. A forma exata que a invocação toma depende do tipo de tempestade que está sendo chamada, mas, em geral, o Senhor da Tempestade dá forma à energia da tempestade em questão e em seguida a molda à sua vontade. Isso pode tomar forma de uma jaula de raios, uma estaca afiada de gelo, uma onda de força através da terra ou qualquer coisa que o jogador consiga imaginar.

Embora existam regras específicas e limitações sobre o que um Senhor da Tempestade pode fazer baseado no tipo de tempestade que esteja invocando, elas têm algumas características em comum.

Em cada caso, a força chamada deve ser expressa de __forma externa__ ao personagem que está invocando, de uma forma __literal__. Isso quer dizer que a perícia Tempestade __não concede__ ao seu usuário a “Força da Terra” para realizar um golpe poderoso, mas permite que acerte algo com BASTANTE força usando uma pedra. Qualquer descrição de efeito deve girar em torno de como a invocação, projeção ou manipulação grosseira da força em questão poderia alcançar o efeito descrito. 

+ __Superar:__ A perícia Tempestade tende a ser um pouco grosseira em todas ações de superar exceto as mais diretas, como derrubar as coisas em seu caminho. Por outro lado, ela definitivamente é boa nisso. 
+ __Criar Vantagem:__ Esta é uma ação comum dentro da perícia Tempestade, por conjurar paredes de fogo ou abrir poços no chão. As tempestades mais sólidas — Terremoto e Geleira — tendem a ser mais fortes nesse tipo de efeito, já que seus resultados tendem a ser mais duráveis. 

A maioria dos efeitos pode ser tratada como uma rolagem normal de criar vantagem, usando um aspecto na cena para refletir tal vantagem, mas há alguns casos especiais. Especificamente, a perícia Tempestades pode ser usada para criar uma barreira do elemento apropriado. Nesse caso, o conjurador escolhe duas zonas e faz uma rolagem contra uma dificuldade 0. O resultado da rolagem indica a dificuldade de superar a barreira criada.

Outros efeitos de vantagem dependem do elemento específico que foi invocado.

+ __Atacar:__ Todas as Tempestades são boas nisso. Como regra básica você pode realizar um ataque dentro de sua zona sem penalidades, com -1 por zona de distância. Estes são ataques normais, mas talvez haja efeitos adicionais dependendo da Tempestade utilizada.
+ __Defender:__  Os elementos também podem ser usados na defesa de ataques, aparando com armas de gelo ou erguendo uma parede de água temporária para interceptar um golpe. Os detalhes dependem do tipo de Tempestade usado.

---

### Barreiras

Barreira é um termo para alguma espécie de obstáculo entre uma zona e outra, como uma parede de gelo. Quando um perso-
nagem tenta penetrar, contornar ou destruir a barreira, o valor da barreira é a dificuldade para conseguir realizar a ação.

Em geral, uma barreira existe entre duas zonas, mas é possível que uma barreira possa ser mais longa, até mesmo rodeando uma zona por completo. A criação de uma barreira é um caso especial de criar vantagem usando a perícia Tempestade. Ela cria uma barreira igual à rolagem da perícia. Logo, se o personagem rolou Bom (+3), então o valor da barreira será 3. Um personagem que obtém uma rolagem inferior a +1 não consegue formar uma barrei- ra eficaz. Cada tipo de tempestade pode modificar a rolagem ou oferecer opções extras.

Quando um ataque é feito através de uma barreira, o defensor pode usar o valor da barreira em lugar de uma rolagem de defesa. O defensor deve decidir isso antes de rolar os dados e usar a barreira abre mão da possibilidade de um sucesso com estilo. Se o ataque incluir uma tentativa de atravessar a barreira — saltando sobre ela, por exemplo — então o atacante usa a menor perícia entre as duas envolvidas (a perícia para superar a barreira e a perícia de ataque) para realizar a tentativa (a menos, é claro, que um aspecto apropriado seja aplicado para facilitar a tentativa de superar).

Por exemplo, uma parede de gelo (Barreira Ótima +4) é lançada entre um Senhor da Tempestade e um Senhor do Vazio na zona adjacente. Se o Senhor do Vazio lançar um raio de sombras, o Senhor da Tempestade pode deixar os dados de lado e utilizar um valor efetivo de Ótimo (+4), como se fosse uma rolagem de defesa. Se o Senhor do Vazio saltar por cima da barreira e atacar usando sua espada, então ele rolará a perícia mais baixa entre Atletismo (saltar) e Lutar (atacar), e o Senhor da Tempestade ainda pode usar sua barreira Ótima (+4) para se defender.

Outro detalhe importante: Barreiras funcionam para os dois lados, então o criador não recebe nenhum benefício especial para atacar o alvo do outro lado da barreira — ambos os lados se beneficiam da barreira.

---

##### Terremoto

+ **Superar:** Se o que estiver sendo superado for uma barreira física e o invocador vencer o alvo por 2, a barreira pode ser removida.
+ **Criar Vantagem:** Receba +1 nas rolagens para criar barreiras usando Terremoto. Quando criar uma barreira, você pode optar por receber um -4 na rolagem — -3 com o bônus — para criar uma barreira que cerque uma zona completamente. Receba um -1 adicional se desejar fechar o topo também.
+ **Atacar:** Você só pode realizar ataques em alvos que estejam no ou próximos ao chão — estilhaços ainda podem atingir quem estiver voando baixo, então qualquer coisa que estiver ao alcance das mãos pode ser atin- gido. Sofra -1 em seu ataque para tentar atacar todos os alvos que estejam em sua zona (exceto você). Por -2 você pode atacar todos os alvos em sua zona e em uma zona adjacente. Você pode expandir esse efeito indefinidamente, desde que esteja disposto a continuar recebendo -2 para cada zona.
+ **Defender:** A terra demora a responder e isso reflete em -1 em todas as ações de defesa.

##### Dilúvio

+ **Superar:** Receba +1 em qualquer tentativa de superar uma barreira física.
+ **Criar Vantagem:** Qualquer barreira criada com água diminui em 1 por rodada a não ser que o conjurador se concentre nela, recebendo -1 em todas as ações subsequentes enquanto estiver mantendo a barreira.
+ **Atacar:** O dano de seus ataques ignora qualquer armadura. Você pode receber -2 por atacar todos os alvos em uma zona (exceto você).
+ **Defender:** Sem regras especiais.

##### Geleira

+ **Superar:** Se o que precisa ser superado for uma barreira física e o conjurador superar a dificuldade por 2, ele consegue remover completamente a barreira.
+ **Criar Vantagem:** Receba +1 nas rolagens para criar barreira usando Geleira. Ao criar uma barreira, você pode criar barreiras contínuas extras. Cada seção adicional de barreira — uma seção equivale a uma barreira entre duas zonas quaisquer — reduz seu valor em 1. Assim, se você conseguir um +6 na jogada e deseja criar uma barreira com 3 seções, ela terá um valor total de 4 (6-2, pois lembre-se que a primeira é grátis).
+ **Atacar:** Você pode optar por causar metade do dano (arredondado para cima) para congelar o alvo no lugar. Isso cria uma barreira ao movimento do alvo, com uma dificuldade para superar igual ao dano causado.
+ **Defender:** Se bem-sucedido com estilo numa jogada de defesa, você pode desistir do impulso para melhorar o valor de qualquer uma de suas barreiras em 1.

##### Inferno

+ **Superar:** Se superar uma barreira física, reduza-a em 1.
+ **Criar Vantagem:** Qualquer barreira criada com Inferno diminui seu valor em 2 por rodada a não ser que o conjurador se concentre nela, recebendo -1 em qualquer ação subsequente enquanto mantiver a barreira. Qualquer um que falhe em superar uma barreira de inferno tem a opção de forçar sua passagem por ela, recebendo dano igual ao número de tensões adicionais necessárias para uma rolagem bem-sucedida de superar.
+ **Atacar:** Você pode sofrer -1 no ataque para atingir todos em uma zona (exceto você).
+ **Defender:** Nenhuma regra especial.

##### Trovão

+ **Superar:** Nenhuma regra especial.
+ **Criar Vantagem:** Qualquer barreira criada com Trovão desaparece após 1 rodada a não ser que o conjurador se concentre nela, recebendo -1 em todas as ações subsequentes enquanto mantiver a barreira.
+ **Atacar:** Trovão possui duas formas de ataque:
  + _Raios Concatenados:_ Arcos de raios vão de um alvo a outro com precisão. Para cada -1 que receber, você pode incluir um alvo adicional no ataque. A penalidade por distância é determinada pelo alvo mais distante, sendo -1 por zona a partir da zona inicial.
  + _Relâmpago:_ Quando atingir um único alvo, o som do trovão se espalha quando o relâmpago atinge. Se conseguir um impulso em seu ataque, você gera um impulso extra chamado Atordoado.
+ **Defender:** Nenhuma regra especial.

### Variações e Opções

#### Compreensão Profunda

Talvez cada uma das tempestades esteja ligada a uma Calmaria correspondente — Montanha, Neve, Mar, Labareda e Vento. Aqueles que dominarem uma Tempestade podem, com o tempo, ganhar outro aspecto — ao custo de 1 de recarga — para refletir tal Calmaria, o que permitirá que ele internalize os pontos fortes da Tempestade para aprimorar-se em uma variedade de formas, assim como gerar mais efeitos sutis.

#### Múltiplas Tempestades

Nada proíbe um conjurador de possuir sintonia com várias Tempestades; o custo será apenas reduzir em 1 a recarga para cada aspecto escolhido.

#### Rituais

É possível possuir a perícia Tempestades mas não possuir sintonia com uma tempestade específica. É assim que funcionam os conjuradores menores. A vantagem disso é poder utilizar qualquer Tempestade que desejar. A desvantagem é levar alguns minutos para fazer o que um verdadeiro Senhor da Tempestade pode fazer em uma única rodada.

#### Invocações

As Tempestades não são espaços vazios. Seres nativos das Tempestades nadam confortavelmente dentro de cada uma delas e podem ser invocados para servir àqueles que sabem como chamá-los. Veja o Sistema de [Invocação][] para uma explicação detalhada.

#### Senhores Da Luz E Da Sombra

Aqueles que estudam tais coisas sugerem que há uma conexão entre as grandes Tempestades. Nascidas de quase pura energia (Trovão), elas se fundem (Inferno) e se tornam uma massa (Dilúvio) antes de solidificarem (Geleira) em algo concreto (Terremoto). Também poderia ser o inverso, onde a matéria começa bruta e ascende em direção a se tornar energia. Seja como for, a teoria é que existem uma sexta e sétima Tempestades, fechando o círculo das grandes Tempestades. Energia e Luz de um lado, Permanência e Trevas no outro. Há aqueles que alegam controlar tais forças de forma similar às tempestades, mas são raros e boa parte de seus esforços foram gastos em conflitos entre os dois lados em busca de descobrir qual é o princípio e qual é o final.

#### Senhores Do Vazio

Com toda sua fúria destrutiva, Tempestades são parte da realidade, pois tanto criam como destroem. Em um nível cósmico, elas se opõem ao Vácuo, o nada que busca consumir tudo. Senhores da Luz e das Trevas tendem a caracterizar o seu oposto dessa maneira e não há como dizer quem está certo. O Vazio é uma força de destruição e embora não seja maligno em natureza, aqueles que o habitam são; é o lar de demônios e monstros que gostariam de nada mais que consumir as Tempestades e, com elas, nosso mundo. Veja a sessão sobre os [Senhores do Vazio][] para mais ideias.

## Os Seis Vizires

### Notas de Criação

O sistema dos Seis Vizires assume uma sistema de magia diferenciada no qual cada um dos Vizires possui suas próprias propriedades e tendências, mas não se manifestam como seres de vontade própria. De modo geral, a magia não possui um custo associado, embora existam elementos sociais envolvidos. Isso é equilibrado por sua raridade relativa (embora esteja totalmente disponível aos PJs) e o fato de ser potente, mas não totalmente flexível. É também um sistema fortemente estruturado, com efeitos mágicos estritamente criados dentro do tema de cada Vizir (expressos através de perícias).

A fonte da magia são os Seis Vizires. O que eles realmente são é uma pergunta interessante e a resposta a isso pode render uma campanha. Neste cenário, eles são seis constelações que servem à Imperatriz e concedem poder àqueles que possuem afinidade com elas.

Do ponto de vista mecânico, este sistema se baseia fortemente em perícias, já que a expressão da magia está ligada
a elas. Há outros elementos — aspectos agem como portal de acesso a poder e os efeitos mágicos em si funcionam de forma similar às façanhas —, mas isto é basicamente um modelo para perícias aprimoradas.

### Descrição

O povo da Planície Sem Fim não se curva a ninguém exceto aos céus. Seu povo se espalha por toda a sorte de lugares, tanto que alguns dizem que esta verdade é a única coisa que ainda têm em comum. E talvez estejam certos — das torres abobadadas das Cidades Fluviais, através das Nações Nômades das Terras Hípicas e às vilas ambulantes das selvas do sul, o
Povo das Estrelas oferece suas orações e maldições a esses astros.

Com frequência, essas orações são voltadas aos Seis Vizires, as constelações que dançam em volta da corte da Imperatriz, cujo lugar no céu é sempre o mesmo. Cada uma delas carrega responsabilidades divinas e cada uma carrega pedidos honrosos à Imperatriz.

Embora todos concordem a respeito das constelações que compõem os Vizires e seus títulos, existem diversas opiniões em outros aspectos. De acordo com a região, eles são nomeados e representados de formas diferentes. Dependendo de onde você estiver, o Gigante pode ser descrito como um grande homem de pedra, uma donzela cujo machado carrega a fúria do inverno ou até mesmo um elefante. Os contos dos Vizires — sendo até mesmo estee nome pode variar — dizem muito sobre cada povo.

De tempos em tempos, alguém é abençoado por um dos Vizires e recebe uma marca cujo formato remete à constelação apropriada. A natureza dessa benção varia. Para uma família de Achinst, a primeira filha de uma família em particular sempre nasce com a marca do Gigante. Um mosteiro ocidental é dirigido por um escolhido do Soldado e dizem que esse manto é passado para aquele que o derrotar em combate. Histórias não faltam, mas não há como saber a verdade absoluta.

O que se sabe é que cada um dos abençoados ganha poder de acordo com o domínio do Vizir em questão. O temperamento do escolhido normalmente parece ser compatível com o Vizir que o escolhe, mas é incerto se isto é uma causa ou efeito da seleção.

### **Versão Resumida**

Se quiser começar logo sem ler tudo, faça o seguinte:

- Substitua a perícia Condução por Cavalgar.
- Reduza a Recarga em 1.
- Escolha um Vizir pelo qual você foi marcado (Observador, Gigante,    Sombra, Soldado, Conselheiro ou Aldeão) e crie um aspecto    **_Marcado pelo(a) [Vizir]_** (como em **_Marcado pelo Gigante_** ).
- Descreva onde fica a marca física no corpo de seu personagem.
- Quando usar a perícia associada à marca, seu esforço será mágico,     como atos sobre-humanos. Isso não se traduz em um bônus, mas    significa apenas um resultado geral mais impressionante, dependendo da situação.
  -     **+ Observador:** Investigar, Conhecimento, Notar
  -     **+ Gigante:** Atletismo, Vigor, Vontade
  -     **+ Sombra:** Roubo, Enganar, Furtividade
  -     **+ Soldado:** Lutar, Cavalgar, Atirar
  -     **+ Conselheiro:** Provocar, Comunicação, Recursos
  -     **+ Aldeão:** Contato, Ofícios, Empatia

### Mecânica

#### Perícias

Pressupõe-se um cenário fantástico, embora não necessariamente o padrão europeu. Como pode imaginar, é algo mais no espírito russo — em tudo — do que europeu. Em todo caso, isso requer que a perícia Condução seja substituída por Cavalgar, que funciona basicamente da mesma forma, mas com outro tipo de veículo.

#### A Marca Do Vizir

O seu personagem foi marcado por um Vizir, reduza sua recarga em 1. Personagens marcados por um Vizir precisam de um aspecto que reflita isso, como ___Escolhido pelo Conselheiro___ ou ___Marcado pelo Aldeão___. O nome exato do aspecto fica a cargo do jogadore, se o jogador possuir uma relação complicada com o Vizir, esta é uma ótima forma de representá-la. O significado exato do aspecto depende da marca, mas todas as marcas têm algumas coisas em comum. Primeiramente, o personagem recebe uma marca física em alguma parte do corpo, no formato da constelação do Vizir.

A forma exata e local variam — podem ser cicatrizes, marcas de nascença, uma tatuagem brilhante ou qualquer outra coisa —, mas o formato é consideravelmente consistente. Desenhos decorativos e marcas temporárias também são populares entre não-marcados em algumas regiões, mas um verdadeiro portador pode reconhecer outra marca genuína com um único olhar.

O marcado sempre sabe onde fica sua constelação, mesmo que seja dia ou se ela estiver além do horizonte. Por si só, não é muito útil para navegação, mas combinado com um pouco de conhecimento, pode permitir um estranho senso de direção.

Existem também elementos sociais em ser marcado, embora eles variem de um lugar para outro. Normalmente, ser marcado é algo bom, exceto quando a marca é de um Vizir indesejado em determinada região.

### As Marcas

Resumimos cada Vizir logo baixo. Algumas informações são autoexplicativas, como alguns termos e formas como os Vizires são representados. Outros elementos têm mais impacto nas regras. 

Cada marca possui uma virtude e um vício, que são relevantes ao uso da marca como aspecto. Por exemplo, o Gigante possui a virtude Força e seu vício é a Fúria. O aspecto Marcado pelo Gigante pode ser usado como se fosse o aspecto Força ou Fúria.

Cada marca também possui três “domínios” — as perícias ligadas a esse Vizir em particular. São eles que formam estrutura para as bênçãos — leia-se Façanhas — que os Vizires fornecem. Os domínios não possuem nenhum efeito mecânico além de esboçar a forma do domínio do Vizir — além de prover alguma orientação para jogos que usem listas de perícias modificadas.

As próprias bênçãos são elementos autossuficientes de regras. Personagens escolhem duas bênçãos de seu Vizir. Essas não custam pontos adicionais de recarga, mas lembre-se que o personagem já gastou um de recarga para poder ser um escolhido.

#### O Observador

+ **Virtude:** Atenção
+ **Vício:** Inatividade
+ **Também Conhecido Como:** O Auditor, O Inquisidor, O Sábio, O Espião ou O Vigia.
+ **Representado Como:** Uma figura sem sexo definido vestindo um manto, um magistrado, uma bibliotecária, uma coruja ou um olho. 
+ **Domínios:** Investigar, Conhecimento, Notar.

O Observador assiste e informa à Imperatriz. Ele vê tudo, e fornece a outros o conhecimento e percepção necessários para agir adequadamente. Ele mesmo raramente atua de forma direta. Em algumas histórias, o motivo é porque ele é um agente da lei — um investigador — que resolve um mistério para que as autoridades competentes possam agir. Em outros, ele está preso a um desejo de ser neutro ou por saber dos danos que poderia causar com suas próprias ações.

##### Bençãos

###### _Investigação_

**As Peças do Quebra-Cabeça:** Ao parar por alguns minutos para estudar um item em particular e sua posição, você pode reconstruir, razoavelmente, a cadeia de eventos que o levaram até ali. Essa reconstrução será precisa, mas não revelará nada além dos detalhes necessários. Por exemplo, pode revelar que foi carregado à mão em dado momento, mas não revela por quem.

**O Cofre do Olhar:** Você pode olhar para uma cena e recordá-la nos mínimos detalhes. Na prática, isso permite a você perguntar ao Narrador sobre essas memórias mesmo depois de muito tempo do fato ocorrido e fazer rolagens de Investigar com calma. Isso _inclui qualquer_ coisa que você poderia perguntar se estivesse naquele local e momento, como o conteúdo
de algum recipiente. Se a resposta para a questão exigir uma rolagem — como arrombar uma fechadura para ver o conteúdo de um baú — você pode realizar normalmente, como se ainda estivesse lá.

É possível armazenar mais de uma memória na sua mente, mas o custo disso é um ponto de destino por cena já memorizada.

###### _Conhecimento_

**A Benção das Mil Línguas:** Você pode aprender qualquer língua rapidamente. Se tiver um tutor, consegue em apenas um dia. Se apenas houver a oportunidade de ouvir e ler, uma semana. Se o material for realmente escasso, pode levar um mês.

**O Observador Vê Todos os Caminhos:** Você pode não saber tudo, mas sempre sabe como descobrir. Quando busca por uma informação consideravelmente específica, você pode dar ao Narrador um ponto de destino para descobrir o local mais próximo onde pode encontrá-la, independente do quão obscura ou perdida seja a informação. Em suma, você nunca fica sem saída quando quiser saber de algo. 

Não há garantia do quão fácil será obter a informação, mas é para isso que serve a aventura.

###### _Notar_


**O Observador Olha em Todas as Direções:** Você nunca é surpreendido. Mesmo que apenas por um momento, você sempre está prevenido para o inesperado.

**As Estrelas Iluminam a Noite:** Contanto que haja uma fonte mínima de luz, você pode ver como se fosse dia. Nos raros casos de escuridão completa, você pode enxergar tão bem como se possuísse uma fonte de luz.

#### O Gigante

+ **Virtude:** Força
+ **Vício:** Fúria
+ **Também Conhecido Como:** O Terremoto, O Trabalhador, O Pilar, O Titã.
+ **Representado Como:** Uma estátua de pedra, uma donzela gélida, um ogro, um elefante ou um touro.
+ **Domínios:** Atletismo, Vigor e Vontade.

O Gigante representa mãos fortes prontas para o trabalho, mas também representa a força descomunal. Na maioria das vezes isso significa força física, mas vai mais além disso. É dito que o Gigante é quem move os céus a mando da Imperatriz. Nos contos, o Gigante frequentemente é apresentado como bem-intencionado e poderoso, mas nem sempre tem controle do poder em suas mãos. Ele muitas vezes serve um papel secundário com outro Vizir — frequentemente o Conselheiro ou o Observador —, agindo a serviço de consciências maiores.

##### Bençãos

###### Atletismo

**Minhas Mãos Movem o Mundo:** Através de uma combinação de velocidade, destreza e ritmo excepcional, você sempre tem um caminho à sua frente. Em um ambiente estático, você é capaz de realizar manobras acrobáticas ousadas para chegar a quase todo lugar que possa ser acessado fisicamente. Em um ambiente mais fluido é impossível segurá-lo ou cercá-lo, já que você sempre encontra uma brecha.

**Passo do Gigante:** Você corre rápido como um cavalo, possui um salto vertical igual a sua própria altura e pode correr por um dia e uma noite sem precisar de descanso — porém, precisará dormir e comer bastante depois disso.

###### Vigor

**Apetite do Gigante:** Você pode comer _qualquer coisa_ sem sofrer danos. Não apenas alimentos — se puder mastigar e engolir, ou beber, você poderá ingerir com segurança e até mesmo se alimentar daquilo. Venenos, decomposição, cacos de vidro e outras inconveniências são ignoradas. Como um bônus, sabores são bem distintos e memoráveis para você, o que permite truques repugnantes como comparar o sabor de amostras de sangue para saber se vêm da mesma fonte, assim como truques mais úteis, como identificar um veneno familiar.

**Nada Pode Deter o Gigante:** Se estiver amarrado ou acorrentado, você consegue quebrar as amarras, contanto que sejam naturais ou fabricadas. Nenhuma porta ou trava pode aguentar mais que um golpe seu. Barreiras sem abertura demoram mais, mas usando apenas punhos, pés e o que puder carregar, você equivale a uma equipe de demolidores completa.

###### Vontade

**A Mente É a Maior Montanha:** No que se refere a perícias sociais, você não existe. Você não pode ser influenciado, amigado, intimidado ou con- vencido de qualquer outra forma. Sua fala não revela nada sobre você ou a veracidade de suas palavras. Para fins das habilidades do Aldeão, seu nível em Enganar é maior que a Empatia do Aldeão.

**Nunca Derrotado:** Você ganha uma caixa de consequência física de -8, que é recuperada da mesma forma que a consequência de -2.

#### A Sombra

+ **Virtude:** Sigilo
+ **Vício:** Ganância
+ **Também Conhecida Como:** O Assassino, O Espião, O Captor, O Ladrão, O Trapaceiro.
+ **Representada Como:** Uma figura encapuzada de qualquer sexo, o vento noturno, uma sombra humanoide, uma cobra, um rato ou um corvo.
+ **Domínios:** Roubo, Enganar e Furtividade

Dependendo da hora e local, a Sombra pode ser tanto um ladrão trapaceiro quanto uma ameaça sinistra e ambas as visões são um pouco verdadeiras. Agindo como a mão invisível da Imperatriz, a Sombra é responsável pelas coisas necessárias que não deveriam ser ditas. Tratar dessas coisas é sempre incerto e indesejável, até o dia em que a necessidade surge; quando então são profundamente bem-vindas, de fato.

##### Bençãos

###### Roubo


**O Zelo com as Miudezas:** Depois de ser bem-sucedido em roubar algo pequeno o suficiente pra caber em seu bolso, o item está desaparecido até que você o revele novamente. Nenhum tipo de procura — nem mesmo uma revista completa — revelará o item furtado. Você só pode ter um item escondido dessa maneira por vez.

**A Súplica das Fechaduras:** Basta sussurrar seu nome em uma fechadura para tentar abri-la, como se estivesse usando suas ferramentas. Se bem-sucedido, a ação demora apenas um momento. Se não tiver êxito, você ainda poderá tentar novamente na forma tradicional.

###### Enganar

**Corroboração da Coincidência:** O destino favorece suas mentiras com pequenas coincidências e evidências circunstanciais que tendem a lhe dar credibilidade. Você pode usar um impulso para a cena antes de sua rolagem de Enganar, desde que possa descrever como isso lhe ajuda a parecer mais honesto. Se bem-sucedido, o impulso se transforma em um aspecto de cena.

**O Nome É uma Máscara para o Mundo:** Sempre que ouvir o nome de alguém dito pelos próprios lábios da pessoa, você pode duplicar seu rosto, voz e jeito até que o sol tenha nascido duas vezes. Você nunca pode imitar a mesma pessoa duas vezes.

###### Furtividade

**Apenas o Vento Me Vê Sair:** Ao custo de um ponto de destino, você pode sair de uma cena, passando despercebido.

**Roubando Palavras ao Vento:** Você não faz barulho quando não deseja ser ouvido. Fazer permite a você não só se mover em silêncio absoluto, mas também pode ser usado de forma seletiva, como falar de forma que apenas uma pessoa possa lhe ouvir.

---

#### O Poder Dos Nomes

Mesmo aqueles que não entendem completamente a natureza da Sombra, entendem que dar a um estranho o seu nome é um gesto de confiança. O quanto as pessoas se preocupam com isso varia de cultura para cultura, mas normalmente é algo ao menos
levado em consideração.

---

#### O Soldado

+ **Virtude:** Disciplina
+ **Vício:** Servidão
+ **Também Conhecido Como:** O Cavaleiro, A Espada e O Senhor da Guerra.
+ **Representado Como:** Um guerreiro culturalmente adequado de qualquer sexo, uma arma, um leão ou um tigre.
+ **Domínios:** Lutar, Cavalgar e Atirar.

O Soldado serve através da violência e da guerra, com virtudes de aço. O Soldado valoriza a astúcia, bravura e lealdade, mas pode ser facilmente conduzido. Histórias heroicas do Soldado falam sobre batalhas lutadas e vencidas, mas outras histórias o põem contra os heróis, pelo simples motivo de seguir ordens cegamente.

##### Bençãos

###### Lutar

**Um Exército no Fio da Minha Lâmina:** Você não recebe penalidades — mas também nenhum bônus — por estar em desvantagem numérica, não importa o quão absurda seja a diferença.

**Lanças de Madeira Verde:** Você pode treinar uma tropa — personagens sem nome em um grupo de até cerca de 100 unidades — por uma semana e melhorar suas perícias Lutar em +1. Você pode repetir isso várias vezes, melhorando qualquer unidade até um nível máximo igual ao seu nível de Lutar -2.

###### Cavalgar

**Apenas o Vento Sob Nós:** Enquanto mantiver uma montaria firme, qualquer cavalo que montar consegue cavalgar sobre água como se fosse solo firme, podendo até cavalgar no ar por algumas centenas de metros — a descida em seguida é semelhante a cavalgar por um declive suave. 

**Montamos Como Um:** Montado, você luta e age sem penalidades e nada pode derrubá-lo. A qualquer momento em que receber uma consequência física, você pode optar que o cavalo a receba em seu lugar — o cavalo possui caixas de estresse semelhantes às de seu personagem.

###### Atirar

**Ao Horizonte:** Qualquer coisa que puder ver é considerado efetivamente como se estivesse a uma zona de distância quando você atira.

**Às Estrelas:** Qualquer projétil disparado ao ar pode pousar em qualquer lugar que você conheça ou próximo a alguém cujo nome você saiba. Mensagens e pequenos itens podem ser entregues dessa forma. Isso não pode ser usado para atacar diretamente, mas se o tiro for dado com a intenção de ferir, é possível matar algum cavalo ou PdN sem importância.

#### O Conselheiro

+ **Virtude:** Liderança
+ **Vício:** Teimosia
+ **Também Conhecido Como:** Avô ou Avó.
+ **Representado Como:** Um ancião sábio de qualquer sexo, um grande cão ou uma serpente.
+ **Domínios:** Provocar, Comunicação e Recursos.

O Conselheiro é o ouvido ao qual todos falam e a voz que todos ouvem. Enquanto a Imperatriz pode governar as estrelas, o Sábio é aquele que faz o dia a dia acontecer. Nas histórias, ele atua mais como assessor ou líder do que como herói, embora às vezes o Conselheiro assuma o papel do viajante perspicaz, ensinando às comunidades que visita lições que já
deveriam saber.

##### Bençãos

###### Provocar

**Coroa Ameaçadora:** Você é muito aterrorizante para ser atacado. Até que realize um ataque físico em uma cena, personagens com Vontade abaixo de Bom (+3) simplesmente não podem lhe atacar. Aqueles com Vontade suficiente ainda vacilam em seu primeiro ataque, errando automaticamente. 

**Caminhando com as Tempestades:** O clima de uma cidade — ou local de tamanho similar — será como você desejar.

###### Comunicação

**Cada Coisa em Seu Lugar:** Você sempre sabe a dinâmica de poder de um ambiente e pode se inserir em qualquer lugar que desejar. Use com cautela — embora isto afete como as pessoas interagem com você, não equivale a autoridade real, e se colocar em uma posição muito alta — especialmente acima de pessoas acostumadas a estar acima de tudo — pode causar rea-
ções desagradáveis.

**Quem Você é de Verdade:** A cada dois minutos de conversa que tiver com alguém, este revela um de seus aspectos. No entanto, para cada dois aspectos aprendidos, você revela um seu a essa pessoa e todos que estiverem ouvindo (arredondado para baixo, então o primeiro aspecto descoberto é gratuito).

###### Recursos

**Rios de Ouro:** Dinheiro não passa de um detalhe para você. Mesmo despido e lançado em uma ilha deserta, em pouco tempo estará vivendo no luxo novamente. Se lançado na prisão, subornará os guardas em momentos. Nenhuma situação restringirá seu acesso a sua perícia Recursos.

**Guerra no Papel:** Você pode tomar medidas contra organizações por meios indiretos. Na prática, é possível lutar no mesmo nível de qualquer organização que seja menor que uma nação, sem a necessidade de recrutar aliados ou possuir sua própria organização. Sim, isso significa que você pode efetivamente “matar” uma cidade ou mesmo um exército se tiver tempo suficiente.

#### O Aldeão

+ **Virtude:** Resistência
+ **Vício:** Tacanho
+ **Também Conhecido Como:** O Peão ou o Cidadão.
+ **Representado Como:** Um fazendeiro, uma moça leiteira, um remador, um martelo, um arado, uma mula ou um macaco.
+ **Domínios:** Contatos, Ofícios e Empatia.

Enquanto o Conselheiro é um líder perspicaz, o Aldeão representa a sabedoria e virtudes dos cidadãos comuns. Suas histórias são aquelas com a pessoa que aparenta ser tola, mas que no fim triunfa por seus valores simples e convencionais. Ele é a força da comunidade.

##### Bençãos

###### Contatos

**Os Laços de Um Homem Vão Além do Horizonte:** Não há nenhum lugar onde você não conheça alguém, incluindo lugares onde nunca esteve antes. Haverá um amigo onde quer que vá.

**A Força de Um Torna-se a Força de Muitos:** Assim que começar a criar contatos, esse esforço se torna autossuficiente; você fala com pessoas, estas falam com outras e estas falam com outras. Na prática, você sempre conseguirá a resposta para suas dúvidas; é apenas questão de tempo.

###### Ofícios

**A União da Forma:** Você pode combinar materiais de formas impossíveis, dando a um os atributos do outro. Você pode fazer papel com a resistência do aço ou o aço com a leveza do papel. Isso exige que dois aspectos — um para o material e outro para o atributo adicional — sejam invocados simultaneamente na rolagem de Ofícios para criar o produto final.

**As Mãos São As Maiores Ferramentas:** Você pode produzir um trabalho manual de mestre com a mais improvisada das ferramentas. Com uma oficina adequada, você pode criar incríveis dispositivos no melhor estilo Da Vinci.

###### Empatia

**Arquitetura do Coração:** Ler os comportamentos em salão inteiro é trivial para você. Mais que isso, você pode perceber emoções-chave em um ambiente e facilmente entender como suas ações afetariam o humor geral. Se fizer uma rolagem de Empatia para ler uma sala — dificuldade Medíocre (+0) — conte quantas tensões você acumula. Ao longo da cena, você pode perguntar ao Narrador como as pessoas reagiriam para cada situação hipotética. Isso pode ser feito uma quantidade de vezes igual à quantidade de tensões conseguidas.

**O Olho Interior Enxerga a Verdade Interior:** Sem a necessidade de uma rolagem, você pode dizer se outro personagem está mentindo, contanto que o nível de Enganar do alvo seja igual ou menor que sua Empatia. Caso o alvo tenha um alto nível em Enganar, você sabe que não pode distinguir, mas nunca recebe um “falso positivo”.

### Evolução

Se um personagem for bem-sucedido em alguma grande missão que se enquadre no caráter de seu Vizir — nada pequeno, mas sim uma tarefa que pode ser o centro de toda uma campanha — ele pode pegar um segundo aspecto ligado ao seu Vizir. Ele perde outro ponto de recarga e escolhe mais duas bênçãos.

### Variações e Opções

Como base, este é um modelo de poderes bastante mítico. Aqueles marcados pelos Vizires são heróis lendários, mas há diversas opções sobre como alterar isso e criar outros efeitos.

#### Nível De Poder

A maneira mais fácil de reduzir o poder das bênçãos é por fazer com que elas exijam o uso de um ponto de destino. Se desejar aumentar seu poder, remova o custo de recarga e simplesmente permita aos jogadores escolher mais bênçãos — ou quem sabe até mesmo possuir mais de uma marca.

#### Estrutura

Seis é um número bastante arbitrário de Vizires. As mesmas bênçãos/façanhas baseadas em perícias podem ser redistribuídas de acordo com outro esquema. Talvez, por exemplo, haja dezoito equivalentes Arcanos maiores, cada um ligado a uma perícia — personagens ligados a cada um dos Arcanos recebem as bênçãos dessa perícia. Por outro lado, os Vizires poderiam ser totalmente descartados, permitindo que os jogadores simplesmente escolham uma certa quantidade de bênçãos.

#### Vizires Ativos

Se os Vizires forem realmente presentes e ativos, o tom do jogo muda completamente. Eles não apenas interagirão mais diretamente com seus escolhidos, mas também estarão envolvidos em suas próprias questões políticas. Vizires ativos podem ser uma forma poderosa de botar o jogo em movimento.

#### Outros Vizires

Talvez existam outros Vizires por aí — constelações secretas com poderes e conhecimentos não revelados ao mundo em geral. Talvez sejam inimigos ocultos ou aliados perdidos; talvez ambos.

#### Vizires Malignos

Os Vizires da forma que foram apresentados são, no geral, forças positivas, com seus elementos negativos servindo principalmente como consequências naturais de seus pontos fortes. Entretanto, cada Vizir poderia vir acompanhado de um reflexo sombrio; uma entidade que abraça sua natureza maligna e cujas poucas características positivas sirvam como extensão dessa natureza. Esses Vizires Sombrios podem ter seus próprios planos, escolhidos e, o melhor de tudo, não há garantia de que aqueles que ostentam a marca sequer tenham consciência de qualquer diferença.

## A Arte Sutil

### Notas de Criação

Mecanicamente falando, este sistema tem como base a ação criar vantagem. Isto é, ele simplesmente expande onde, quando e que tipo de vantagens pode ser criadas. Como é um sistema mais discreto do que os outros apresentados, ele não responde a algumas das questões principais. Isso torna o sistema mais adequado para jogos que não se focam em magia. Na verdade, ele funciona melhor em jogos onde há a dúvida de se a magia é real ou não, embora em outros jogos também possa representar uma forma de magia “menor”.

### Descrição

Quando imaginamos sociedades mágicas, a primeira coisa que vem à mente são locais de encontro antigos e ordens secretas. Esses lugares certamente existem, mas não possuem mais tanta magia — apagada pelo próprio sucesso dos locais. Considere que os benefícios da magia são efêmeros, enquanto os benefícios da colaboração, conspiração e parcerias são concretos. Para aquelas pessoas importantes, bem-sucedidas, que se encontram em salas enfumaçadas de madeira escura e carpetes altos, a magia é apenas um item decorativo. Eles não precisam mais acreditar e, na maioria dos casos, já não o fazem há muito tempo.

Há exceções. Muitas das organizações bem estabelecidas começaram repletas de crenças e intenções firmes, mas essa crença desapareceu sob o peso do próprio sucesso desses grupos. É fácil acreditar que seus amuletos de prosperidade estão ajudando nas suas escolhas de investimento quando você não tem a menor ideia do que está fazendo, mas à medida que é bem-sucedido — e aprende — então é natural começar a atribuir seu sucesso ao seu talento e escolhas inteligentes.

Como resultado, a maioria dos grupos de praticantes de magia são mais informais. Redes de amigos, amigos de amigos ou estranhos com conexões tênues que partilham de um interesse em comum. Muitas reuniões giram em torno de bebidas, encontros e noites no sofá de estranhos em apartmentos baratos.

A realidade nua e crua é que a magia cria suas raízes mais fortes nos desenraizados. Pessoas inteligentes e capazes, mas sem direção ou propósito, se apegam à magia pela solução rápida que ela representa. É uma arma desconhecida e, para alguns, a melhor à disposição.

#### Magia E Realidade

O importante é que não há prova “real” de que essa magia funciona. A introdução desses aspectos não altera a realidade de nenhum modo real ou replicável. Eles certamente alteram um pouco a balança, mas só podem obter resultados no limite do possível e razoável. Para um observador externo, essa “magia” se parece muito com viés de confirmação. Se você amaldiçoar alguém e algo ruim acontecer a essa pessoa, é fácil assumir crédito por isso, mas um cínico diria que coisas ruins acontecem com pessoas o tempo todo.

Isso pode ser difícil e, como resultado, os que acreditam tendem a se unir. Eles formam subculturas onde podem discutir o que fazem, trocar dicas e reforçar sua crença de que isso é algo que importa. De muitas maneiras, esses grupos são mais importantes do que a magia em si, mas eles não são exatamente o que se espera.

### **Versão Resumida**

Não quer ler tudo? Faça o seguinte:

- Compre uma perícia chamada “Magia”.
- Tire meia hora em um quarto escuro com o nome de alguém, um    boneco de vodu ou apetrechos similares e faça uma rolagem de     Magia para criar vantagem contra o alvo do feitiço.
- Coloque um aspecto apropriado a uma maldição ou bênção no alvo.     Ele dura três dias, ou sete se você for bem-sucedido com estilo.

### Mecânica

Este sistema adiciona uma perícia: Magia. Sua descrição se encontra abaixo.

#### Perícia: Magia

Magia é a perícia para lançar bênçãos ou maldições sobre uma pessoa ou lugar. Embora a perícia em si seja genérica, suas manifestações específicas não são. Um praticante deve possuir um conjunto de regras e diretrizes que segue para usar magia. Essas regras podem ser baseadas no mundo real, ser totalmente inventadas ou qualquer coisa entre os dois, mas devem ser consistentes e sempre demandar tempo, esforço e um ritual. Há outras limitações descritas abaixo.

+ **Superar:** Há poucos obstáculos úteis que a magia possa superar, embora muitos praticantes pensem o contrário. É um erro comum achar que a perícia Magia pode ser usada para “detectar” trabalhos mágicos, mas isso seria tão confiável quanto tentar adivinhar.

Um uso concreto para a ação superar é superar o ceticismo de outros. A perícia magia também representa quão bem você “vende” a ideia da magia, ou ao menos o que acredita. Isso funciona como um uso bem específico da perícia Enganar, mesmo que o personagem não sinta que está enganando alguém.

+ **Criar Vantagem:** A principal atividade com a perícia Magia é criar vantagem. Assumindo um único alvo — uma pessoa ou coisa de talvez até o tamanho de uma casa —, aproximadamente meia hora e os aparatos necessários para o ritual, a rolagem é realizada contra uma dificuldade Regular (+1). Se bem-sucedido, o alvo recebe o aspecto de bênção ou maldição (veja os detalhes abaixo) por três dias e três noites. Outras modificações possíveis seriam:
  - Se o alvo não estiver presente, então a dificuldade aumenta entre     +1 e +3. +3 se é dito apenas o nome do alvo, +1 se um laço simbólico do alvo estiver presente — seu sangue, um item precioso para     ele, etc. — e se não for claramente algum desses casos, então seria     apropriado um +2.
  - Se o alvo é grande — um grupo pequeno com menos de doze membros ou um lugar grande como um prédio ou parque — a dificuldade aumenta em +3. Esse é o tamanho máximo de um algo que um     feitiço pode afetar, embora a maioria dos praticantes desconheça isso     e todos os anos horas de magia são desperdiçadas visando partidos     políticos, times de futebol e _hipsters_.
  - Alguns feitiços possuem um alvo secundário, como um feitiço que     faça com que seu chefe fique enfurecido com alguém. A ausência desse segundo alvo também reflete na dificuldade — +0 se estiver presente, +3 se souber apenas o nome, como acima. A única exceção é que se o alvo secundário aceitar algum tipo de símbolo da magia — uma poção, alguma bugiganga — então ele estará efetivamente “presente”. Tais artefatos devem ser usados em no máximo três dias.
  - Um sucesso com estilo estende a duração para uma semana.
  - Nenhum alvo pode sofrer mais de um feitiço por vez. O feitiço mais    recente substitui o mais antigo.
  - Algumas bênçãos e maldições possuem seus próprios modificadores    adicionais.
  - Na prática, um feitiço em uma área cria um aspecto de cena que    pode ser usado normalmente por qualquer um no local.
+ **Atacar:** Não existem ataques mágicos.
+ **Defender:** Não existe defesa mágica.

#### Feitiços

Aspectos colocados num alvo são geralmente chamados de bençãos ou maldições, dependendo do efeito desejado, mas coletivamente todos são considerados feitiços. A lista não é aberta — há um grupo fixo de feitiços e o conhecimento destes é a moeda da comunidade mágica. Feitiços são complicados a ponto de ser muito difícil memorizá-los e ainda executá-los com exatidão; é por isso que são mantidos em livros, bancos de dados e outros tipos de arquivos. Afanar o livro de feitiços de outro mago pode ser informativo, mas também pode ser tão útil quanto roubar suas anotações sobre química orgânica — mesmo que não estejam propositalmente complicadas, podem ser bem difíceis de entender. Além disso, claro, não há como distinguir uma magia real de uma falsa.

Para maior clareza, o alvo do feitiço é a pessoa, lugar ou coisa que recebe o efeito do feitiço. Às vezes um feitiço também terá um sujeito: uma pessoa, lugar ou coisa que será o foco do efeito do feitiço sobre o alvo. Por exemplo, um feitiço de amor para fazer Jake se apaixonar por Andy seria lançado sobre Jake (o alvo), com foco em Andy (o sujeito).

**Aborrecimento:** O alvo incomoda os outros. Se o feitiço possuir sujeito, então o alvo do feitiço ficará mais facilmente aborrecido com esse sujeito.

**Carisma:** Tratando de amor, isso melhora a presença e comportamento geral do alvo. Às vezes este feitiço é ridicularizado — especialmente aqueles que precisariam desse tipo de feitiço —, mas é frequentemente usado em silêncio.

**Clareza:** Popular entre aqueles que se consideram magos sofisticados. Para muitos este feitiço é seu café da manhã, aguçando seus pensamentos e sentidos. É também um “contrafeitiço” popular, usado para remover maldições.

**Imperícia:** Sabe aqueles dias em que você deixa cair um copo, derrama o café no colo e rasga a camisa em um trinco? Isto faz esse tipo de dia acontecer. 

**Confusão:** As pessoas tendem a interpretar mal o alvo — ou, se for um local, perder-se facilmente.

**Amor:** Um dos mais conhecidos, mas também um dos mais controversos, especialmente quando usado com um sujeito. Sem um sujeito, simplesmente faz com que o alvo seja mais amigável com o mundo, mas com um sujeito, este inclina-se em direção ao alvo. Muitas pessoas veem isso como desagradável na melhor das hipóteses, e assédio na pior. É um tema delicado e um grande número de feiticeiros contorna isso ao explicitamente lançar feitiços falsos.

**Saúde:** O equivalente mágico aos antiácidos com vitamina C e zinco.

**Sorte:** Este é o feitiço mais comum em circulação, podendo assumir a forma de boa ou má sorte.

**Obscuridade:** O alvo é facilmente ignorado — pelo sujeito, se houver. Se isto é uma bênção ou maldição, depende do seu ponto de vista.

**Prosperidade:** Outra bênção popular. Questões financeiras favoráveis surgem pelo caminho do alvo. É raro isso se transformar em uma grande colheita, mas pode se mostrar como uma extensão no pagamento de um empréstimo ou uma cerveja grátis.

**Ira:** Pequenas coisas incomodam o alvo mais do que o normal, como se tivesse acordado com o pé esquerdo. Se o seu feitiço tiver um sujeito, então o alvo do feitiço é mais facilmente enfurecido por ele.

**Segurança:** Mantém o alvo — ou a área — mais seguro do que estaria. Estes não são todos os feitiços disponíveis, mas fornecem alguma inspiração no tom necessário para outras ideias.

#### Façanhas Mágicas


**De Cor:** Você pode escolher três feitiços que conhece bem a ponto de não precisar consultar suas anotações para conjurá-los.

**Mau Olhado:** Você pode tentar colocar Má Sorte em um alvo com nada mais do que um gesto simples. Isso dura apenas um dia.

**Decorador de Interiores:** Você pode até chamar de feng shui na fatura, mas é tudo decoração. Se puser um feitiço em um local, você pode organizar os móveis e a decoração para tal. Se o fizer, o efeito dura por uma estação inteira — ou até que alguém reorganize os móveis.

### Variações e Opções

#### Mas Funciona?

É inteiramente possível que este “sistema” seja uma mentira. A magia não faz nada e tudo não passa de vontades realizadas e viés de confirmação. Um Narrador poderia até mesmo colocar isso em jogo, silenciosamente deixando de representar os aspectos que os jogadores aparentemente criaram. Isso é, de modo geral, uma péssima ideia. Isso sufoca a ideia do jogador e paralisa seu conceito — a não ser que ele também goste da ideia de tudo ser uma farsa. 

Se quiser enfatizar essa ideia — mesmo que não queira adotá-la totalmente — então as coisas tornam a magia impossível de ser comprovada são boas oportunidades para forçar.

#### Movimentando As Coisas

Também é possível que tudo seja um sistema mais abertamente mágico. Nesse caso você pode introduzir efeitos únicos, improváveis ou estranhos. Isso exigirá expandir a lista de feitiços para incluir coisas mais concretas como “alimentos apodrecem quando você os toca”, o que também faz com que a magia seja mais concretamente perceptível através da perícia magia. Neste caso, a duração dos efeitos deve ser estendida para o período de um mês lunar.

#### Maldições De Combate

Assumindo um estilo mais evidente de magia, uma variante permitiria “conjurações de combate” de bênção e maldições. É algo bem diferente da imagem tradicional que temos de magos conjurando raios, mas se adapta bem a cenários com baixo nível de magia. Neste caso, o feitiço pode ser conjurando em qualquer alvo que possa ser visto e, de modo geral, isso permite efeitos interessantes para criar vantagem.

Por padrão, devem ser efeitos invisíveis, mas ainda assim podem gerar más escolhas, armas que erram e assim por diante. No entanto, se o Narrador considerar apropriado, então pode ser permitido um pouco mais de cor para tornar tudo ainda mais mágico — como um feiticeiro do fogo criando vantagens flamejantes.

#### Duelos Entre Feiticeiros

Se maldições de combates forem possíveis, então também existirão duelos de feiticeiros. Um duelo como esse ocorre quando dois feiticeiros se encontram e travam olhares, usando Magia em lugar de Lutar e infligindo dano mental até que um ou outro abandone o conflito. Para um observador xterno, tudo o que acontece é uma troca de olhares seguida de um dos dois
sucumbindo — possivelmente morto, de acordo com a decisão do vencedor. Entre ambos os feiticeiros, a batalha pode tomar qualquer forma.

Às vezes, seres de poder maior podem ser trazidos para um duelo por magia mortal. Isso exige alguma preparação do praticante, como a criação de um objeto de foco. Esses conflitos ainda ocorrem principalmente no éter, mas podem envolver uma troca de energias mais chamativa ou outro efeito. Nesses casos, se o poder maior for derrotado, o resultado raramente é fatal, mas o ser pode receber consequências.

## Invocadores da Tempestade

### Notas de Criação

Esta é uma expansão do sistema das “Cinco Tempestades” descrita na sessão [Senhores da Tempestade][]. É um sistema completo que pode ser facilmente transformado em um jogo próprio, mas é igualmente fácil de se usar em conjunto com os outros sistemas de magia das Cinco Tempestades para formar um sistema mais completo.

### Descrição

Cada uma das cinco Tempestades — Terremoto, Dilúvio, Geleira, Inferno e Trovão — no centro da realidade é o lar de uma miríade de seres que têm sua morada nesses ambientes mortais. Para os leigos, eles são elementais — seres compostos dos elementos das Tempestades, mas dotados de inteligência e vontade próprias. Apesar de serem os mais numerosos dentro das tempestades, são apenas o começo.

Os elementais compõem a base do que parece um ecossistema dentro das Tempestades. Os mais numerosos são crias quase inteligentes, pequenas criaturas de natureza elemental. Cada grupo acima é mais poderoso, mas menor em número, até um limite atingido onde as criaturas começam a assumir forma e natureza distintas. Estas muitas vezes se assemelham a versões fantásticas de criaturas mundanas, enquanto algumas mais poderosas lembram humanos. Há príncipes e rainhas entre esses seres e dizem que os mais poderosos entre eles estão par a par com os próprios deuses.

Com a perícia e ferramentas adequadas, um Conjurador pode invocar esses elementos para realizar seus pedidos, com um perigo que corresponde à potência da criatura invocada. No entanto, os maiores seres das Tempestades estão fora do alcance de qualquer mero Conjurador.

Invocadores são diferentes dos Conjuradores pois possuem um pacto com uma das grandes forças das Tempestades, podendo usufruir desse acordo, influenciando seres ainda maiores das tempestades. No entanto, esses pactos têm seu preço.

### Versão Resumida

Não quer ler todas as regras? Use este resumo:

- Compre uma Perícia de Conjuração.
- Faça alguns rituais para invocar um elemental de Água, Fogo, Gelo,    Relâmpago ou Terra com uma dificuldade de Regular (+1) a Ótimo    (+4). Essa é a dificuldade que precisa ser vencida na rolagem de perícia, além de também representar o nível de habilidade do ser invocado. A invocação dura uma semana e só é possível manter um de    cada vez.
- Os elementais vêm em quatro tamanhos:
  - **Crias** (Regular [+1], 0 estresse, nenhuma consequência), pequenos orbes do tamanho de um punho. Não são tão espertos, mas são rápidos, sorrateiros e seguem ordens simples.
  - **Lacaios** (Razoável [+2], 0 estresse, 1 consequência suave), do tamanho de cães, mais fortes do que aparentam e capazes de transportar cargas pesadas ou realizar trabalhos simples.
  - **Serviçais** (Bom [+3], 2 de estresse, 2 consequências suaves), de tamanho e formato aproximado a humanos, entendem ordens mais complexas e são soldados eficientes.
  - **Assessores** (Ótimo [+4], 2 de estresse, 1 consequência suave e 1 moderada) são seres poderosos carregam consigo um aspecto da Tempestade, então são criaturas visualmente bastante incríveis.
- Se você tiver um pacto com um ser maior para se tornar mais poderoso, então:
  - Reduza sua Recarga em 1.
  - Adicione o aspecto ___Pacto com o Príncipe da [escolha a Tempestade]___.
  - Mude sua perícia de Conjuração para uma perícia de Invocação.
  - Com uma rolagem bem-sucedida, você agora pode invocar um elemental Excepcional (4 de estresse, 2 de armadura, -2/-
4/-6 de consequências), com uma descrição à sua escolha. Você ainda pode realizar conjurações normais com o elemento com tem o pacto, mas por que o faria?

### Mecânica


Este sistema adiciona duas perícias e alguns aspectos que refletem a magia de invocação. Também adiciona alguns aspectos específicos que refletem os pactos com as forças das Tempestades.

#### **Perícia:** Conjuração

Conjuração é a arte de invocar elementais das Cinco Tempestades. É um processo lento e exige a criação de um círculo de invocação e o uso de sacrifícios apropriados para ganhar o serviço de uma dessas criaturas. Qualquer um pode escolher a perícia Conjuração sem nenhum custo de recarga.

+ **Superar:** Conjuração também pode ser usada como uma perícia Conhecimento quando tratar de criaturas da Cinco Tempestades. Também pode ser usada para renovar o vínculo de uma criatura elemental já invocada sem o tempo e esforço gastos na invocação inicial. Isso simplesmente requer uma rolagem de superar contra o nível da criatura — veja o resumo dos elementais nas próximas páginas.
  + Falha: A criatura é imediatamente libertada e irá fugir ou lutar, dependendo da situação e como foi tratada.
  + Empate: O vínculo não é renovado e é encerrado normalmente.
  + Sucesso: Renova o vínculo por uma semana.
  + Sucesso Com Estilo: Renova o vínculo por um mês.
+ **Criar Vantagem:** Invocar um elemental é uma forma específica de criar vantagem. Fazer isso exige um círculo de convocação e uma quantidade e tipo de sacrifícios baseados no ser que será invocado. Para uma Cria, um punhado de material relacionado será o bastante, mas um ritual para conjurar um Assessor exigirá muito mais.

Role contra uma dificuldade baseada no tipo de criatura a ser invocada — veja na próxima página.

  + **Falha:** A criatura é invocada, mas se torna livre. Crias e Lacaios tendem a fugir — causando problemas em outros lugares — enquanto Serviçais e Assessores podem se voltar contra o Conjurador, se conseguirem uma oportunidade.
  + **Empate:** O elemental aparece, mas realizará apenas um único serviço simples que tome menos de uma noite.
  + **Sucesso:** O elemental aparece e estará a seu serviço por uma semana.
  + **Sucesso Com Estilo:** O elemental aparece e permanece vinculado por um mês.
+ **Atacar:** Conjuração pode ser usada como perícia de ataque contra criaturas conjuradas, com o “dano” contribuindo para o banimento.
+ **Defender:** Conjuração pode ser usada na defesa contra ataques realizados por criaturas invocadas. A defesa pode ser reforçada ao permanecer em um círculo, com um valor de +1 para um círculo desenhado às pressas a +4 para um círculo matematicamente perfeito, feito de um metal precioso e gravado com runas antigas de poder.

O nível de Conjuração do personagem é também o limite do número de elementais vinculados consigo ao mesmo tempo. Suas dificuldades somadas não podem exceder o nível de Conjuração, então um Conjurador Ótimo (+4) pode chamar quatro Crias, uma Cria e um Servo ou alguma outra combinação que totalize quatro. É bastante comum — e ruidoso — um Conjurador possuir diversos elementais invocados ao mesmo tempo.

#### Elementais

##### Resumo Dos Elementais

| __Elemental__ | __Nível__   | __Estresse__ | __Consequências__ |
|:-------------:|:-----------:|:------------:|:-----------------:|
| Cria          | Regular     | 0            | Nenhuma           |
| Lacaio        | Razoável    | 0            | -2                |
| Serviçal      | Bom         | 2            | -2/-2             |
| Assessor      | Ótimo       | 3            | -2/-4             |
| Nomeado       | Excepcional | 4            | -2/-4/-6          |

##### Bônus Elementais

| __ELEMENTO (TEMPESTADE)__ | __CRIA__ | __LACAIO, SERVIÇAL OU ASSESSOR__ | __NOMEADO__                               |
|:-------------------------:|:--------:|:--------------------------------:|:-----------------------------------------:|
| Água (Dilúvio)            | Nenhum   | Consequência suave adicional     | Consequência suave adicional, Armadura: 1 |
| Fogo (Inferno)            | Nenhum   | Arma:1                           | Arma:2                                    |
| Gelo (Geleira)            | Nenhum   | Armadura: 1                      | Armadura: 2                               |
| Relâmpago (Trovão)        | Nenhum   | 1 zona de alcance de ataque      | Alcance 2                                 |
| Terra (Terremoto)         | Nenhum   | +2 Estresse                      | +4 Estresse                               |

Para a maior parte das ações, a maioria dos elementais possui apenas uma única perícia: Elemental, cujo nível é igual à sua classificação; portanto uma Cria do Fogo possui a perícia padrão ‘Cria do Fogo: Regular (+1)’. Certos elementais têm outras perícias específicas, mas na ausência dessas um elemental rolará sua perícia padrão ou rolará com uma penalidade de -2 para outras ações.

Todos os elementais possuem a habilidade de fundir-se em seus elementos naturais, recebendo um bônus de +4 em Furtividade desde que alguma quantidade desse elemento esteja presente para que desapareçam dentro dele. Além disso, recebem benefícios baseados em seu elemento como descrito na tabela _Bônus Elementais_.

###### _Cria_

Crias parecem um punhado vivo de seu elemento e possuem pouco  poder e inteligência. No entanto, sua invocação é a mais simples dentre os elementais, além de serem bem adequados para tarefas simples, especialmente as que envolvem Atletismo e Furtividade — eles recebem um bônus de +4 em ambos. São quase inúteis em combate, visto que não possuem caixas de estresse e não podem receber consequências. 

A diferença entre elementos é altamente estética entre as Crias, mas suas variações podem ser bem exóticas. Crias são os elementais mais fáceis de se encontrar na natureza e muitos dos que viveram por muito tempo fora das tempestades acabaram ”se adaptando”, adotando características da fauna e flora locais. Essas crias “nativas” podem ser vinculadas a um conjurador como quaisquer outras.

###### _Lacaio_

Lacaios possuem um corpo elemental do tamanho de um cão e não são mais espertos do que as Crias — muitas vezes são até menos — mas são consideravelmente mais fortes e pacientes. Eles recebem um bônus de +2 em Resistência e em qualquer rolagem de Vigor relacionada ao transporte de cargas. Eles são bem adequados para a realização de tarefas chatas e demoradas, mas não são grandes lutadores, pois não possuem caixas de estresse e podem apenas receber uma consequência suave de -2.

###### _Serviçal_

Serviçais são do tamanho de um humano e por vezes sua forma é levemente similar a um humanoide, com alguns braços e pernas, embora raramente com algo que lembre uma cabeça. Embora não sejam gênios, são inteligentes e capazes de seguir instruções complicadas ou lutar por seus mestres. Servos possuem 2 caixas de estresse e podem receber duas consequências de -2.

###### _Assessor_

Assessores são como a maioria das pessoas imagina um elemental: algum elemento da Tempestade com vida. São maiores que uma pessoa e parecem ser um turbilhão vivo feito de sua Tempestade original. Também são hábeis e inteligentes em combate. Assessores possuem 3 caixas de estresse e podem receber uma consequência de -2 e outra de -4.

### Pactos Elementais

Um Conjurador pode fazer um pacto com um dos poderes da Tempestade. Fazer isso concede poder muito maior sobre os elementais daquele domínio, mas isso vem a um grande custo. O Conjurador é agora um Invocador, limitado a apenas aquele elemento, e agora também responde a um ser de grande poder e motivações questionáveis. Apesar do preço, devido ao poder que deriva desse pacto, nunca há falta de indivíduos que o busquem, mesmo que nem todos sobrevivam ao processo. Tenha cuidado com os Príncipes das Cinco Tempestades.

Mecanicamente, o pacto toma a forma de um aspecto que reflete a negociação. Ele pode ser invocado para ajudar nas invocações e conjurações — bem como para nomeá-las, em certas ocasiões — e pode ser forçado de qualquer forma que sirva ao interesse do outro lado do pacto. Isso pode variar, de estipulações aparentemente aleatórias — como precisar carregar um determinado acessório — a proibições — o Príncipe do Magma odeia banhos! — ou direitos de visita. O jogador pode optar por anular um pacto —perdendo os benefícios e sendo atacado imediata mente por qualquer conjuração que tenha no momento —, mas fazer isso cria um inimigo — e apenas como referência, ___A Inimizade do Príncipe do Trovão___ é um ótimo aspecto para substituir o antigo!

O jogador e o Narrador devem trabalhar nos detalhes do outro lado da negociação. A ideia base é que ela seja feita com o Príncipe ou a Rainha de uma Tempestade, com títulos como “Príncipe do Magma”, “Dama das Geleiras” ou “Condessa do Céus Partidos”, mas as opções são realmente infinitas.

O pacto reduz a recarga do personagem em um. É possível fazer mais de um pacto, mas fazer isso garante que daquele ponto em diante você será o ponto de impacto das nas batalhas entre esses dois Reinos de Tempestade.

#### **Perícia: Invocação**

A perícia Invocação substitui a perícia Conjuração quando um personagem faz um pacto. Ela possui o mesmo nível e funciona de forma semelhante à perícia Conjuração, com as seguintes mudanças:

- O personagem pode apenas invocar criaturas da Tempestade com a    qual tiver um pacto.
- Elementais invocados agora não têm custo.
- Invocações que falharem sempre resultam na fuga da criatura.
- A perícia do personagem é tratada como +4 acima ao determinar     quantos elementais podem ser controlados ao mesmo tempo.
- O personagem agora pode invocar e se vincular a uma criatura elemental nomeada baseada no patrono de seu pacto. A natureza dessa     criatura é parte da identificação do patrono. Criaturas nomeadas são     tipos específicos de criaturas fantásticas — pássaros de fogo, tatus de     relâmpago ou qualquer coisa que o jogador e o Narrador concordarem que seja legal.
- Esses elementais nomeados são de nível Excepcional (+5), possuem    4 caixas de estresse e consequências de -2/-4/-6, assim como seus    bônus elemental — veja a tabela na página anterior.

### Elementais em Combate

Controlar um elemental além do personagem não é muito trabalhoso, mas um Invocador ou Conjurador talentoso pode se aventurar com vários elementais ao seu lado e lidar com todos eles em um combate pode ser bastante complicado de manejar. Por essa razão, um Conjurador pode usar uma variação da regra de Trabalho em Equipe, da seguinte forma:

- O ataque e defesa básicos são determinados pelo elemental mais     poderoso sob o controle do personagem. Ele recebe +1 por estar sob     o controle do personagem.
- Criar vantagem e superar dificuldades ainda usam a perícia do personagem, mas com +1 para cada elemental que não seja uma Cria     e que esteja na luta, se o elemental puder ajudar naquela situação.
- Elementais funcionam como um reserva de consequências para o     jogador. A qualquer momento em que receba dano, ele poderá perder     elementais como se houvesse recebido consequências, de acordo com     os seguintes valores:
  - Lacaio: -2
  - Serviçal: -4
  - Assessor: -6
  - Nomeado: Qualquer valor único

### Variações e Opções

#### Poder Emprestado


Um Invocador com um pacto pode comprar a perícia Tempestades como se fosse um Senhor da Tempestade e tomar parte do poder de seu patrono emprestado. No entanto, há limites para este uso. O invocador não pode usar a perícia Tempestades enquanto uma criatura nomeada estiver invocada e ao usar a perícia ele renuncia o benefício de +4 no número de criaturas invocadas possíveis. O lado positivo é que ele pode sacrificar qualquer elemental que não seja uma Cria para receber +1 em sua rolagem de Tempestades, embora este bônus não seja cumulativo. Esta variação não custa uma recarga adicional.

#### Mestres Das Crias

É possível que alguns conjuradores renunciem os aspectos mais amplos da conjuração em favor de se especializar em Crias, tanto por sua utilidade como para os torneios secretos de Crias que se tornaram tão populares.

Nesse caso, substitua a perícia de Conjuração por uma de Treinamento de Crias, para saber quantas crias um personagem pode controlar, mas isso não permite conjurá-las. Em vez disso, ele poderá domar crias “selvagens”, aquelas que ficaram presas no mundo mortal por tempo suficiente para adotar a forma de alguma criatura. Um Mestre das Crias encontra e captura essas Crias e então as domestica, tornando-as Crias de Batalha que serão treinadas para lutar.

Em um exemplo usando a Regra de Bronze, Crias de Batalha são criaturas Regulares (+1) sem caixas de estresse e consequências ao menos no que diz respeito à sua interação com o mundo, mas entre si são diferenciadas minuciosamente. Ou seja, cada Cria pode ter um conjunto completo de perícias, poderes e habilidade úteis em batalha contra outras crias e, dentro dessas batalhas, essas diferenças são bem importantes. Entretanto, para um observador externo, a mais grandiosa e a mais simples das Crias de Batalha são praticamente a mesma coisa.

##### Treinando Sua Cria De Batalha

Crias de batalha começam com 2 caixas de estresse, uma consequência suave e 4 perícias em Medíocre (+0): Força, Velocidade, Perícia e Resistência (sim, existe uma perícia chamada Perícia. Lide com isso). Essas são, respectivamente, usadas para atacar, superar, criar vantagem e defender durante os combates. Resistência também é a defesa contra ataques.

Crias podem evoluir ao vencer batalhas ou através de treinamento. Exatamente como o avanço ocorre depende da situação, mas cada avanço pode ser usado para:
+ **Melhorar uma perícia.** Todas as perícias podem ser melhoradas para Bom (+3). Uma perícia pode ser elevada a Ótimo (+4) e uma pode atingir Excepcional (+5).
+ **Melhorar a resistência.** Um avanço pode ser gasto para adicionar uma consequência de -2, melhorar uma de -2 para -4 ou melhorar uma de -4 para -6. O máximo de consequências para uma Cria deBatalha é -2/-4-/6.
+ **Adquirir um aprimoramento.** Algumas possibilidades seriam:
  + **Carapaça:** +1 de Armadura.
  + **Carapaça Resistente:** (Requer Carapaça e Resistência Boa): +1 de Armadura.
  + **Ataque de Sopro:** Pode atacar dois alvos de uma vez.

Há muitas outras possibilidades de aprimoramento. Em geral, trate-as como façanhas simples.

Em qualquer situação que não seja um combate com outras crias, ela ainda é considerada como uma cria de nível Regular, sem estresse, sem consequências e sem melhorias — exceto num sentido puramente estético. Isso se aplica independente de quantas melhorias ela tenha recebido.

## Senhores do Vazio


### Notas de Criação

Tal como acontece com os Invocadores das Tempestades, este sistema está intimamente ligado ao sistema das Cinco Tempestades e, da mesma forma, foi criado que possa ser usado por si só com facilidade. Embora, neste caso, possa parecer algo ainda mais diferente. Esta é a magia das trevas e de coisas terríveis que se encontram fora dos limites da realidade. Classicamente, é o espaço para horrores inimagináveis e cheios de tentáculos, mas a esperança aqui é torná-la mais palpável. Baseado em fontes como os livros da série _Twenty Palaces_ de Harry Connolly, a ideia é que o indivíduo não precise apoiar tanto no “desconhecido” para pensar em coisas perturbadoras.

Seria fácil dizer que se trata de magia para vilões, mas estaríamos fazendo um desserviço a você e seus jogadores — para não falar sobre o desserviço aos vilões. Esta magia vem a um custo horrível, quase desumano, mas quase desumano não é o mesmo que desumano. Os cultistas que invocam coisas das trevas não são simplesmente loucos que buscam destruir o mundo. Eles querem algo e estão dispostos a pagar o preço. 

O que querem e o preço a ser pago torna difícil distingui-los de alguns heróis.

### Descrição

Ninguém sabe ao certo o que é o Vazio. Algumas literaturas sugerem que é tudo o que não pertence ao universo, enquanto outros textos sugerem que é o fim do universo onde o tempo e espaço são indistinguíveis. Para alguns é simplesmente o inferno. O que quer que seja, é um lugar ruim. Escuridão em todos os sentidos da palavra. Às vezes alguém tropeça em suas bordas e se ela não o matar, o marcará terrivelmente.

Felizmente, isso é raro. O Vazio não interage com o mundo, a menos que alguém realmente vá a sua procura e, mesmo assim, é algo bem difícil de ser encontrado. Na verdade, é provavelmente impossível de ser encontrado, exceto por uma verdade perigosa: há coisas que querem sair de lá.

Não há uma descrição única do que são essas coisas e o que querem. Algumas são pouco mais do que animais, embora animais possuídos de poderes horríveis. Outros são claramente possuidores de algum nível de inteligência, de sub- a sobre-humanos.

Os mais espertos são uma ameaça óbvia – eles buscam meios para tornar o Vazio mais fácil de ser encontrado, oferecendo pactos e buscando espalhar conhecimento que deveria ser ocultado. Seus objetivos diferem. Alguns claramente buscam cruzar o limiar para o nosso mundo, outros parecem querer atrair outros para seu mundo de trevas. Outros são sim-
plesmente um mistério.

Ainda assim, a ameaça desses animais não deve ser subestimada — a ameaça que representam muitas vezes é ecológica. Uma única criatura pode não oferecer grande risco, mas dado o tempo para se reproduzir e se espalhar, eles podem representar uma ameaça de extinção.

Mas eles são tão úteis.

O poder do Vazio toma várias formas. Mais comumente é a forma de invocação e vínculo com alguma criatura útil. Enquanto as devidas precauções forem tomadas, essas criaturas podem ser mantidas de forma bastante segura, mas precauções muitas vezes têm limites. Isso é duplamente verdadeiro para criaturas muito poderosas. Elas não são muito mais difíceis de serem invocadas — embora a formação do vínculo seja outro assunto — e farão o possível para contornar as restrições impostas sobre si.

Às vezes, verdadeiro poder também pode ser adquirido de relações com o Vazio. Isso pode assumir a forma de conhecimento, como uma magia, truque ou algo mais direto, normalmente através de algum tipo de infecção. Esta última pode ser tão ruim quanto soa — não há nenhuma garantia de que o poder recebido hoje não é o mesmo que o fará explodir em uma horda de minhocas devoradoras de carne amanhã.

### **Versão Resumida**

Encontre as instruções para realizar uma invocação das trevas.

- Faça uma rolagem de Conhecimento contra a dificuldade de invocação. Após isso, você adiciona qualquer bônus necessário para tornar a    rolagem bem-sucedida. O Narrador recebe 1 Ponto de Condenação    para cada +1 concedido.
- Você usa a coisa horrível que invocar para seu próprio benefício e em    detrimento do mundo.
- O Narrador gasta os Pontos de Condenação para tornar sua ideia    ruim ainda pior.

### Mecânica

Há duas considerações mecânicas diferentes para lidar com os poderes do Vazio. A primeira é uma questão de como o contato é feito, como as coisas são invocadas e assim por diante. A segunda é como os efeitos dessas coisas são expressados.

#### Invocações

Parece banal, mas invocar algo do Vazio é aproximadamente comparável à montagem de um móvel grande. Você tem instruções extensas e se segui-las à risca e possuir todas as ferramentas corretas, devem produzir o resultado prometido. Infelizmente, mesmo na melhor das circunstâncias, esse tipo de coisa pode ser confuso. O autor raramente é um escritor qualificado e não podemos esquecer que é o tipo de pessoa que escreve livros sobre como invocar monstruosidades profanas.

Isto é, na verdade, o porquê de qualquer um que entenda do assunto ser cético a respeito de grimórios. Qualquer um que tente montar um livro a partir dessas coisas deve ter prioridades suspeitas, além de que não há real garantia que algo funcionará. Há diversos grimórios conhecidos em circulação em número suficiente para serem reconhecidos; a capacidade de identificar quais são falsos é uma perícia bastante útil para a sobrevivência. Mesmo assim deve-se continuar atento — não é raro um praticante incluir rituais ruins conhecidos em seus livros.

Os verdadeiros tesouros são os cadernos de anotações. A realidade é que praticantes desleixados são praticantes mortos, enquanto aqueles que sobrevivem por mais tempo costumam documentar tudo — sucessos, falhas e todo o resto. Infelizmente essas anotações tendem a ser rascunhos pessoais na melhor das hipóteses e códigos cifrados na pior delas, então nunca é fácil.

Tudo isso serve para dizer que, em muitos casos, a dificuldade em descobrir como invocar algo tem menos a ver com a dificuldade da tarefa em si, que normalmente não é difícil, mas tem muito a ver com obter instruções úteis e confiáveis. Essa limitação é a grande razão pela qual mesmo os praticantes bem-sucedidos normalmente possuem apenas alguns truques na manga. Cada nova invocação que aprendem exige um período similar a uma “roleta russa” com apostas cada vez mais arriscadas.

Portanto, o ato de invocar propriamente dito é realizado com uma rolagem de Conhecimento para ver quão bem você segue as instruções, assim como quão bem você toma precauções, aplica seu julgamento durante o processo e procede com cautela. Assumindo que o ritual usado seja correto — e não há garantia disso — há duas dificuldades em jogo: a dificuldade do ritual em si e a dificuldade de compreender o ritual através do texto. Para fins de melhor controle, essas são a __dificuldade de invocação__ e a __dificuldade de compreensão__.

Como regra geral, a dificuldade de invocação é normalmente baixa, mesmo para criaturas poderosas. Lembre-se, elas _querem vir_ aqui e o único desafio real é fazer isso com segurança. Compreender as instruções é muito mais difícil.

As dificuldades para invocar tendem a ser consistentes, mas as de compreensão dependem totalmente do material consultado. A menor dificuldade de compreensão do texto pode ser igual à dificuldade de invocação. Em todo caso, ambas as dificuldades são desconhecidas para o jogador.

Para invocar com eficácia, o personagem precisa realizar todos os passos necessários de acordo com o ritual e fazer uma única rolagem de Conhecimento contra ambas as dificuldades.

Após as rolagens:

- Se o jogador não atingir nenhuma das dificuldades, o feitiço não    funciona, ou funciona sem a segurança adequada e o que quer que tenha sido chamado agora está à solta. Essa decisão fica a cargo do Narrador, dependendo de quão divertido achar que será.
- Se o jogador alcançar a dificuldade de invocação, mas não a de     compreensão, o feitiço funciona! Exatamente como esperado! Bem,     mais ou menos como esperado.

Anote por quanto o jogador falhou em alcançar a dificuldade de compreensão. O valor é convertido em Pontos de Perdição. Perceba que isso se refere explicitamente a _bater_ a dificuldade — um empate ainda rende um Ponto. Pontos de Perdição são a moeda de troca para coisas que dão errado. Podem ser pequenas ou sutis, podem ser grandes e dolorosas, mas não são evidentes de imediato e podem ser reveladas conforme a vontade do Narrador.

---

Então, porque a invocação é tão “segura” para os PJs? É um dos pontos básicos do design de jogos. Não crie um sistema de magia onde os feitiços podem matá-lo, mas são úteis quando bem-sucedidos. Isso é totalmente razoável de uma perspectiva mais “realista”, mas é terrível para o jogo. A magia segue o princípio básico da utilidade — se for colocada em jogo, alguém a usará e então caberá a você fazê-la funcionar, de um jeito ou de outro. Neste caso específico, é mais interessante ter jogadores lidando com as consequências do sucesso. Isso pode ser pior do que morrer e ninguém precisará criar outro personagem; ao menos não imediatamente.

---

---

Por exemplo: David tenta um ritual com uma dificuldade de invocação Razoável (+2) e uma dificuldade de compreensão Fantástica (+6). Ele consegue um Ótimo (+4) então o feitiço funciona, mas o Narrador acumula 3 Pontos de Perdição já que David precisaria de mais 3 para superar a dificuldade.

---

Se for bem-sucedido com estilo em um ritual, você consegue sem acumular Perdição e a dificuldade de compreensão cai em um ponto nas próximas execuções do mesmo ritual. Isto não pode reduzir a dificuldade de compreensão abaixo da dificuldade de invocação e se aplica apenas a você. Qualquer outro ainda precisa usar a dificuldade padrão para a compreensão do texto original — ou suas anotações, se for apropriado.

##### Rituais Extensos

As regras até agora pressupõem a invocação de seres de tamanho pequeno a médio vindos do Vazio. Há criaturas maiores que também podem ser invocadas e controladas. As regras atuais e dificuldades não mudam, mas as exigências para o ritual são bem mais extravagantes. Topos de montanhas isoladas, círculos de correntes de ouro, mil origamis em forma de grou com asas banhadas no sangue menstrual de uma assassina e coisas desse tipo. Assim como em uma invocação normal, a rolagem não é a parte essencial do processo — e na verdade as regras são as mesmas — o que importa são as coisas que levam a esse momento.

Pode parecer contraintuitivo — se coisas maiores estão tentando atravessar para nosso mundo, porque os rituais requerem mais componentes? É apenas uma questão de praticidade — pense nos rituais como andaimes. Quanto maior for a coisa que quer trazer, mais forte deverá ser o andaime — e isso antes de começar a considerar a questão do vínculo.

Claro, muitas dessas coisas são grandes, espertas e poderosas suficiente para encontrar brechas. Algumas criaram ou levam à criação de artefatos — espelhos, estátuas, patas de macaco, caixas de quebra-cabeça e afins – que podem abrir caminhos em direção a eles. 

Felizmente, estes em geral são limitados de alguma forma; caso contrário a criatura já teria atravessado. Contudo, são iscas excelentes, por assim dizer.

#####  Vínculos

A criatura invocada não pode fazer muita coisa. Parte da invocação é a vinculação, ao menos deve ser, se tudo for feito corretamente. Basicamente, o Invocador deve liberá-lo — totalmente ou em parte — para usufruir de suas habilidades. Para criaturas estúpidas, isso significa apenas quebrar o círculo e deixá-las fazer seu trabalho. Para criaturas mais inteligentes, significa liberá-las no intuito de usar seus poderes sob condições estritas.

Tecnicamente, essas coisas são bem faustianas em suas negociações. Eles geralmente não podem negociar — a menos, é claro, que possam — mas tentarão explorar todas as brechas nos limites impostos a eles. No entanto, nada é menos divertido do que aparecer com condições exatas para essas coisas, portanto isso não deve ser exigido. Narradores devem falar com os jogadores sobre suas intenções, e em seguida, respeitá-las. Pontos de Perdição e consequências naturais devem prover complicações até mais do que suficientes.

##### Pontos De Perdição

Pontos de Perdição são uma moeda simples para contabilizar todas as coisas que os personagens não levaram em consideração. Pense neles como brechasinfernais, um balde de coisas só esperando o momento para quando algo der errado na invocação. Basicamente, eles dão carta branca ao Narrador para piorar a situação — não que ele não possa fazer isso de qualquer maneira, mas gastar Pontos de Perdição transfere a culpa. Algumas coisas que podem ser feitas com o gasto de um Ponto de Perdição são:

- Permitir à criatura usar um poder além do âmbito do vínculo,    mesmo que apenas um pouco de cada vez.
- Permite à criatura invocar outras criaturas.
- Permite à criatura estabelecer contato com alguém que possa estar    interessado em um acordo melhor
- A mera presença da criatura impõe um aspecto de cena ao ambiente, em um raio de crescimento constante.

###### _Rituais E Aspectos_

Aspectos parecem ser uma excelente maneira de garantir um ritual seguro e evitar toda essa questão de Pontos de Perdição do Narrador. Tecnicamente realmente são, mas há algumas coisas a considerar.

Ao fazer uma dessas invocações, você está fazendo algo terrível. Tudo que é trazido do outro lado é uma abominação e uma ameaça ao mundo. Quando invoca um aspecto para ajudá-lo a fazer isso, você poderia muito bem estar contaminando aquele aspecto.

Todos entendemos a questão de fazer coisas ruins por bons motivos e isso pode ser a motivação que leva alguém a invocar. Porém, ao tomar essa decisão você ultrapassa um limite e diz algo profundo sobre esse aspecto. Isso diz algo sobre o personagem e também sobre como o aspecto aparece em jogo. Uma vez que abre a porta para coisas abomináveis em nome do amor, você convida o Narrador a ver o quão longe você está disposto a ir.

Talvez isso seja incrível. Pode ser exatamente o que você espera ver em jogo — é um tema ótimo e poderoso. Apenas comentamos aqui para que você abra essa porta com os olhos bem abertos.

### Criaturas e Poderes

Eis a dura realidade — não há como catalogar plenamente todas as manifestações dessas coisas horríveis. Oferecemos aqui uma variedade de exemplos, mas a verdade é que, para isso, você deve procurar em _cada uma_ das estruturas do Fate que puder busca de ideias. Estas sugestões são isoladas, então se as regras para uma criatura em particular não combinarem
com algo no jogo, esse será o contexto no qual ela **fará sentido**. Esta é a sua chance de pirar.

O único limitador é que você deve monitorar algumas coisas, evitando uma morte em massa de seus PJs assim que as coisas perigosas forem soltas. Uma das formas seria limitar a variedade de coisas ruins a um número que os PJs consigam lidar. Outra seria certificar-se de que os PJs possuam as defesas corretas. São apenas alguns pontos a se levar em consideração.

#### Exemplos De Criaturas

##### Besouros Devoradores de Feridas

**Dificuldade de Invocação:** Regular (+1)

Estes besouros pouco mais longos que um polegar têm uma carapaça macia e branca, como se tivessem acabado de sair de algum ciclo de crescimento, mas que nunca endurece. Para usá-los, basta deixá-los andar um pouco sobre a pele — é um tanto asqueroso no começo, mas com o tempo você nem perceberá que estão lá. Literalmente. A não ser que esteja fazendo um grande esforço para tentar localizá-los, você simplesmente não estará ciente de sua presença. Se não souber onde esperar que apareçam, é como aquelas sensações estranhas que surgem e desaparecem rapidamente.

O besouro recebe esse nome por sua capacidade de comer feridas. Ele desfaz coisas em menor escala e mecanicamente possui um efeito bastante potente: uma vez por dia, o seu portador pode remover uma consequência física, apagando o ferimento. Após comer três dessas feridas, ele deixará um pequeno ovo em alguma parte de seu corpo — o que também será imperceptível — e, em uma semana, outro besouro nascerá e andará despercebido — podendo assim, comer novas feridas.

Se, por outro lado, um besouro passar uma semana sem feridas para se alimentar, ele encontra alimento em outro lugar e devora um dos aspectos do hospedeiro. Isso não causa qualquer mudança direta — nada é esquecido ou removido, só se tornam menos importantes. Uma vez que todos os aspectos de uma pessoa tenham sido devorados, ela basicamente se rende a um tédio apático, com momentos ocasionais de atividade intensa ou tendência suicida — que são controlados rapidamente — como se tentasse obter algo que perdeu mas não consegue encontrar.

Seu lar se torna bagunçado e mais repleto de tranqueiras, que se assemelham a ninhos com caminhos partindo dos arredores do brilho da televisão. Não por coincidência, esses ambientes cheios de detritos são favoráveis ao besouro, que usa essa desordem para viajar com mais segurança para longe de seu hospedeiro — já que são presas fáceis tanto para botas quanto predadores — na tentativa de encontrar um novo hospedeiro, de preferência aqueles que estejam dormindo e nunca notarão seu novo hóspede.
 
Os besouros em si não são uma grande ameaça — até mesmo uma infestação menor raramente se estende a mais que um prédio. No entanto, eles também são simbióticos com algumas infestações e invasores agressivos. Um humano transformado em uma máquina de matar é perigoso, mas um coberto de besouros devoradores de feridas é um problema muito maior.

##### Vermes Trovejantes

**Dificuldade de Invocação:** Razoável (+2)

Há um nome maior para esta coisa, mas é bem complicado. Se parece com um cruzamento entre um raio e uma centopeia acelerada, que nunca diminui o ritmo o suficiente para que se possa dar uma boa olhada. Ele não consegue se manter em repouso por muito tempo, embora possa ser mantido em um frasco de vidro preparado adequadamente. Lançado, é como uma luz estrondosa, tão perigoso quanto um relâmpago, e então desaparece.

Se, no entanto, for lançado através de algo pelo qual possa viajar, como um cabo de força, ele pode se manter indefinidamente. Dessa forma ele pode manipular dispositivos eletrônicos de forma rudimentar e pode dar um bote como uma cobra a partir de qualquer ponto de exposição elétrica como tomadas, pontos de luz e assim por diante. Esse ataque — Excepcional (+5) contra Percepção, Arma: 7 — é bastante potente, mas dispersa a criatura e muitos praticantes os usam como armas descartáveis.

Se algum conseguir se libertar, ele tende a ocupar um “ninho” de fios, como uma casa ou escritório — eles parecem ter dificuldades em atravessar grandes distâncias através da rede elétrica. O único sinal óbvio disso são estranhezas elétricas, pelo menos até a criatura se multiplicar. Um verme trovejante pequeno é do tamanho de uma centopeia verdadeira e surge de tomadas, procurando por qualquer aparelho eletrônico que conseguir encontrar — normalmente dispositivos pequenos —, permanecendo lá até que seja conectado a outra rede onde possa crescer e eventualmente produzir sua própria prole. Se não, crescerá até que o dispositivo não possa mais contê-lo, então matará a próxima pessoa que encontrar. O que vier primeiro.

Deixando de lado a ameaça à infraestrutura que essas coisas representam, eles possuem outro péssimo hábito — cadáveres frescos são uma rede interessante para eles. Coisas vivas não atraem muito o seu interesse, mas corpos recentemente falecidos que possuem um sistema nervoso sofisticado? É como uma festa. Uma festa trôpega, não morta, crepitante e com dedos que eletrocutam — ao menos até que se esvaia em chamas. Os vermes podem difíceis de detectar, mas o corte total da eletricidade os mata instantaneamente.

##### Olhos de Lázaro

***Dificuldade de Invocação:*** Razoável (+2)

Esta esfera branca, aproximadamente do tamanho de um ovo, é inofensiva e inerte a maior parte do tempo. Se, no entanto, for colocada na órbita do olho de uma pessoa recentemente falecida, estenderá tentáculos até o seu cérebro fazendo com que o corpo se regenere. Desde que a carne — e o mais importante, o cérebro — esteja em grande parte intacto, então ao longo das próximas 12 horas a pessoa voltará à vida. Se o cérebro não estiver intacto, então o corpo se reanimará e caminhará trôpego, até que morra de fome. Assustador, mas na maioria das vezes, inofensivo. Porém, se o cérebro estiver intacto, então a pessoa realmente volta à vida – personalidade, memória e tudo mais. A única diferença é que o Olho de Lázaro nunca consegue ser da mesma cor que o original, então os olhos ficam diferentes.

O problema é que o Olho de Lázaro se alimenta de massa encefálica e só há duas formas de obtê-la — do hospedeiro ou de outra pessoa. O método primário é estimular o hospedeiro a comer cérebros — ou então comer o cérebro do hospedeiro. Essa vontade pode começar por animais, mas eventualmente eles forçarão o hospedeiro a cometer canibalismo para conseguir cérebros humanos. Cérebros de corpos recém falecidos serão suficientes por um tempo, mas nunca saciará a fome como um que acabou de ser morto — ou melhor ainda, um ainda vivo.

No entanto, isso não muda a natureza da pessoa que o faz — ela faz essa coisa monstruosa plenamente consciente da monstruosidade, mas porque precisa. Ela não pode parar, só precisa tomar medidas para não atacar seus amigos ou entes queridos. além disso, o Olho de Lázaro sobrecarrega o sistema suprarrenal do hospedeiro, ampliando seus reflexos e geralmente ajudando-o a se tornar o predador que precisa ser — recebendo +1 em todas as rolagens de perícias físicas, -1 para todas as rolagens mentais que não sejam sensoriais. Ele também bota ovos em seu estômago que podem surgir de sua
boca sempre que o hospedeiro precisar, como para salvar um ente querido ou ajudar um ente querido a juntar-se a ele, para que possam estar seguros.

Se o Olho não for alimentado com regularidade — uma vez por mês inicialmente, mas com maior frequência ao tongo do tempo — então ele se volta à sua única fonte de alimento à mão, comendo o cérebro do hospedeiro, normalmente começando por suas memórias e funções mais complexas. Quando isso acontece, o resultado é uma máquina primitiva de matar — +3 em todas as perícias físicas e -3 em todas as perícias mentais que não sejam sensoriais — fazendo com que quebre crânios com suas
próprias mãos em busca de seu conteúdo. Se continuarem a passar fome, Olho e seu hospedeiro morrem.

##### Freet

**Dificuldade de Invocação:** Boa (+3)

Estas formas corrompidas de ifrit parecem sapos de fogo, mas suas chamas apodrecem e escurecem conforme queimam, o oposto da chama da purificação. Como seres de energia, são difíceis de ferir fisicamente, mas sofrem dano ou são dispersos com água limpa. Não possuem caixas de estresse ou consequências, mas ignoram qualquer dano exceto imersão ou ou jatos fortes de água, como uma mangueira de incêndio — é semelhante a apagar uma fogueira (embora seja uma que está tentando comer o seu rosto), mas fazer isso poluirá profundamente os arredores. Quando invocadas, essas criaturas normalmente são enviadas atrás de um alvo para matá-lo. _Freets_ possuem Lutar e Furtividade em nível Ótimo (+4). Se bem-sucedidos em um ataque contra um alvo, infligem uma **_Podridão Ardente_** — uma infecção terrível e dolorosa que consome o alvo aos poucos. Até que o aspecto seja removido, o personagem recebe 1 de estresse todos os dias e não pode recuperar estresse ou consequências de forma natural. A única maneira de remover esse aspecto é destruir a criatura e todas suas crias, se houver.

No que tange a prole, _Freets_ põem seus ovos no fogo, pequenas faíscas que dão um certo mau odor às chamas. Após três horas, o fogo se extingue, gerando um número de _Freets não vinculados_ de acordo com o tamanho do fogo. Uma lareira pode produzir um, enquanto um incêndio florestal pode produzir dezenas.

##### O Cavalheiro Elegante


***Dificuldade de Invocação:*** Ruim (-1)

Ele é conhecido por vários nomes e invocá-lo é simplesmente uma questão de dizer a palavra certa no contexto certo. Ele, no entanto, só aparecerá de acordo com sua própria agenda, e uma vez que o tenha invocado, ele pode aparecer para você a qualquer momento em que esteja só.

Ele é tema de muitas histórias que possuem alguns pontos em comum. Ele não está fisicamente presente, e muitas vezes pode aparecer apenas em reflexos, como uma silhueta sombria ou de outras formas impossíveis, apesar de às vezes aparecer normalmente. Ele é magro, embora sua aparência seja bastante debatida, e está sempre bem-vestido, embora de uma forma estranha. Ele aparece quando o Invocador está só, apesar disso ter mais a ver com quem pode vê-los do que com solidão literal. Ele já surgiu em festas e outros eventos grandes onde é possível se perder em uma multidão, embora ninguém
exceto seu parceiro possa vê-lo.

O Senhor Elegante está disposto a ajudar. Tenha em mente que ele não pode fazer nada fisicamente para ajudar, mas seu conhecimento é grande. Seu conhecimento de segredos, tanto arcanos como mundanos, não tem fim e ele se alegra em compartilhá-los. Por outro lado, ele também deixa claro que requererá alguns favores ocasionais em troca da informação.

Os favores são aparentemente benignos, embora os praticantes mais experientes tenham notado que costumam girar em torno de preservar e proteger informações e rituais sobre o Vazio que de alguma forma poderiam ser perdidos. É raro o Senhor Elegante levar parceiro diretamente em direção a esses rituais, possivelmente porque ele se beneficia mais ao man-
tê-lo no escuro. Um favor bastante comum é “fazer uma apresentação” e ensinar o nome do Senhor Elegante a alguém. É possível recusar fazer o favor e o Senhor educadamente irá se despedir e nunca mais retornar. A não ser, talvez, que você se encontre em uma situação muito ruim depois, mas perceberá que o preço subiu consideravelmente.

Para alguns, esta é a dimensão desse relacionamento levemente perturbador, um pouco de olho por olho e nada mais. Contudo, ele se torna mais complexo para aqueles que ele considera interessantes. Se você interessa ao Cavalheiro, ele estará mais do que disposto a ajudar e essa ajuda abrirá grandes portas, trazendo também grandes problemas. Esses problemas tendem a ficar piores e piores até o ponto em que seu único desejo é encontrar uma saída.

E isso é algo que o Sr. Elegante tem o prazer de fazer. Ninguém sabe o que acontece depois disso.

##### Cogumelo Apodrecido

**Dificuldade de Invocação:** Boa (+3)

Quando invocado, surge como um cogumelo horrendo e coberto de pústulas, com talvez 30 centímetros de altura. Repouse sua mão sobre ele e uma pústula semelhante aparecerá em sua mão — indolor, mas nojenta. Repouse essa mão sobre uma pessoa doente — ou sobre você mesmo — e a doença desaparecerá, à medida que pústula se fecha e um Cogumelo menor cresce nas costas de sua mão. Ele pode ser removido — dolorosamente — e plantado, onde eventualmente crescerá até o tamanho do primeiro. Desde que o Cogumelo cresça e permaneça saudável, a doença permanece em remissão, embora a saúde de todos os cogumelos subsequentes dependam da saúde do primeiro.

Mais uma coisa desagradável: as doenças seguem o seu curso enquanto estiverem dentro do Cogumelo — seu retorno abrupto incluirá todo o progresso da doença desde o momento da intervenção, muitas vezes com efeitos terríveis e dramáticos.

O cogumelo também pode curar ferimentos, até mesmo lesões traumáticas, mas isso é um pouco mais problemático. O personagem curado parece bem por fora, mas por dentro, a “cura” toma a forma de um fungo esponjoso amarelado que funciona como o substituto da carne. Isso não é diretamente prejudicial — a não ser que esteja repondo massa encefálica, caso no qual os resultados são imprevisíveis, mas raramente bons. No entanto, aqueles curados dessa forma geram esporos com frequência. Cogumelos crescem onde eles repousam. São cogumelos normais, embora venenosos, mas aceleram a degradação normal que um Cogumelo Apodrecido causa.

A mera presença de um Cogumelo Apodrecido é prejudicial — não para o invocador, mas para a área como um todo. Apenas um não é tão perigoso — gripes podem ser um pouco mais sérias na cidade, mas raramente algo perceptível. Cada cogumelo adicional piora a situação. Como se não bastasse, se alguém morrer enquanto recebe o benefício de um cogumelo apodrecido — ou seja, enquanto a doença estiver contida —, outro Cogumelo Apodrecido crescerá em seu túmulo.

### Variações e Opções

#### Mantendo O Controle

Então, se o Vazio é tão perigoso, porque que ainda não fizemos uma grande besteira? Cedo ou tarde algo virá, se multiplicará e dominará o planeta. É questão de matemática.

Há algumas respostas possíveis, qualquer uma ou todas podem ser verdade.

Primeiro, o próprio mundo pode se mover para rejeitar o Vazio. Brechas se consertam com o tempo, antigas invocações se tornam inúteis após o uso excessivo e há uma tendência constante para manter o Vazio distante, o que contrabalança seu esforço contínuo de tentar entrar. 

Segundo, o mundo não possui apenas humanos. Em um mundo mágico, isso pode significar feéricos, espíritos ou até mesmo deuses que tomem medidas para impedir as piores incursões, mas isso vale mesmo em um mundo razoavelmente moderno. Os grandes cérebros dos quais a humanidade tanto se orgulha também são uma razão para estarmos tão vulneráveis a tais coisas. Uma criatura que drene a essência de sua alma e o faça desejar de carne humana para preencher o Vazio pode devastar uma pequena cidade, mas para um coiote é apenas um lanche delicioso.  Animais são menos impressionados por tais ameaças e seria perturbador contar o número de vezes que o mundo foi salvo por ratos e aranhas.

Por último, pode haver pessoas que trabalham ativamente contra esse tipo de coisa. Limitar informações é difícil, mas não impossível, e isso tem todas as características de uma guerra secreta. A necessidade de controlar e destruir informações enquanto se permanece ciente delas e capaz de responder a altura é paradoxalmente difícil. É a daí que nascem boas histórias.

## Criando Seu Próprio Sistema


Agora que viu uma boa variedade de exemplos, aqui está a sua oportunidade deixar o jogo do seu jeito. Isso deveria ser um processo simples, mas nunca é bem assim – é aquele tipo de coisa que parece ser complicado no começo, mas vai se tornando cada vez mais fácil à medida que for fazendo, até chegar ao ponto que se torna tão instintivo que você não consegue compreender como isso pode ter sido difícil um dia. Onde quer que esteja nessa sequência, esperamos que possamos ajudar com estas ferramentas.

### Equilíbrio

Primeiro, deixe de lado suas noções de equilíbrio. É um conceito importante, mas não da forma como normalmente é colocado. Equilíbrio não existe no que é abstrato — é um elemento específico de jogo e sempre deve ser visto através da visão de jogo. Nada está desequilibrado por si só, o contexto que é responsável por isso. Um poder que torna um per- sonagem um deus onipotente pode parecer desequilibrado, mas quando todos os personagens o possuem, o jogo se torna mais interessante. Tudo depende do contexto.

Sendo assim, como podemos equilibrar os poderes? Pense em três coisas — equilíbrio no grupo, no cenário e no jogo.

#### Equilíbrio no Grupo

Quando desenvolver um sistema de poder, você precisa fazer uma das seguintes suposições:

- Apenas alguns personagens poderão usar.
- Todo personagem poderá usar.

Se estiver desenvolvendo um tipo de poder que apenas alguns personagens poderão usar, então precisa pensar sobre como o poder se compara a outras coisas que os personagens podem fazer, e o que os personagens estão dando em troca desse poder. Simplificando, isso significa que você precisa de uma resposta satisfatória para a pergunta “porque eu não escolheria este poder?”

O sistema dos Senhores das Tempestades foi desenvolvido com isso em mente. O motivo para não se tornar um Senhor da Tempestade é porque você precisa sacrificar um espaço de perícia — e um aspecto — para se tornar um, e como a maior parte de suas magias são aplicáveis a combate, torna-se uma troca bem equilibrada. Mesmo isso assume que haverá coisas como regras de armas e armaduras no jogo. Sem tais regras, a perícia Tempestades se torna apenas uma perícia superior de combate e não há razão para não selecioná-la.

Mas note que o truque para o equilíbrio vem de outra parte do jogo em si. Isso ilustra algo importante sobre o equilíbrio em grupo — objetivo é certificar que os jogadores permaneçam ativos e engajados. Ao tornar uma certa parte do jogo mais legal que as outras, é esperado que os jogadores se concentrem nisso, então ou isso deve ser apoiado, ou será necessário criar outras formas de tornar os personagens interessantes.

Se você, por outro lado, assumir que todos os jogadores terão poderes, o céu é o limite. Você diminuiu o risco dos jogadores ofuscarem uns aos outros. O sistema dos Seis Vizires foi desenvolvido dessa forma e ilustra bem os pontos fortes e perigos desta abordagem. As habilidades desse sistema são incrivelmente fortes e poderiam distorcer o foco caso apenas um dos personagem as possuam, mas desde que todos tenham potência semelhante, isso deixa de ser um perigo. No entanto, como os poderes são potentes e diversificados, deve-se tomar cuidado para quem nenhum deles domine o jogo. 

Nada faz com que uma abordagem seja melhor que outra — a lógica do seu sistema de poder revelará se ele deve ser para alguns personagens ou para todos. Entretanto, essa distinção precisa estar clara quando for desenvolver o sistema. Um sistema que tente fazer ambos está implorando por resultados abusivos e inconsistentes.

#### Equilíbrio no Cenário

Equilíbrio no cenário pode parecer uma ideia estranha, mas é fundamental para a boa elaboração de poderes, pois criação de poderes e criação de cenários são a mesma coisa. Suas regras de poder determinam como seu mundo funciona e é preciso pensar dessa forma. Perguntas que você deve considerar incluem:

- Quem pode usar este sistema de poder?
- Quantos usuários existem?
- Quão potentes/capazes são?
- Qual o impacto do poder nas pessoas que o possuem?
- Quais são os resultados mais comuns do poder no cenário?
- Quais são os resultados em grande escala de tais poderes dentro do  cenário?

Obviamente, quanto mais restritivo for o poder, menos você precisará se preocupar com essas coisas, mas você corre o risco do poder ser um apenas tom no cenário em lugar de fazer parte dele. Como bônus, quanto mais pensar sobre as ramificações lógicas do poder, melhor será para equilibrá-lo.

Os _Conjuradores do Vazio_ são uma boa ilustração de um sistema de magia equilibrado com o cenário. Note que há muito poucas questões mecânicas no uso da magia nesse sistema — quase tudo são elementos do cenário, tanto em termos de comportamento dos praticantes como no impacto dos poderes. Qualquer um pode usar o poder, então os praticantes criam obstáculos para impedir que outros o usem e, ao fazer isso, escondem informações sobre quantos são e sua própria habilidade. O impacto do poder é terrível, mas —por enquanto— são controlados por sorte ou boas intenções. Se isso for mudado, o poder é mudado.

**Dica:** Quer ousar numa ideia de sistema? Olhe as respostas para o equilíbrio do cenário, mude uma delas e veja o que acontece. Por exemplo, não há resultados em grande escala no sistema padrão dos Conjuradores do Vazio, mas e se isso for mudado? E se algo comeu Manhattan? Algo tão grande e terrível que não poderia ser escondido? O que mudaria?

#### Equilíbrio no Jogo

Em grande parte isto se trata de uma extensão do equilíbrio no grupo, mas depende de como os poderes influenciam jogo. Alguns jogos com poderes são sobre os poderes em questão, tais como os jogos de superpoderes ou jogos sobre magos como o tradicional _Ars Magica_. Outros jogos, como os clássicos de aventura ou jogos de horror, simplesmente incluem poderes no escopo geral do jogo. Descubra o que o seu sistema faz e ajuste os poderes apropriadamente.

De forma prática, em um nível mundano, preste atenção no funcionamento do seu sistema na mesa de jogo. Se a mecânica exigir mais de sua atenção — porque requer mais rolagens, por exemplo — então existe uma boa chance de estar chamando mais atenção do que os jogadores que não possuem poderes. Isso pode ser administrado pelo Narrador de forma equilibrada, mas seria melhor se o problema não existisse de início.

## Suas Ferramentas

A seguir você encontra uma boa quantidade de componentes incompltos para sistemas de magia. Alguns são maneiras de criar poderes, outros são efeitos em potencial e resoluções. Desmanche-os e use suas partes, junte-os ou baseie-se neles para criar seu próprio sistema.

### Limites

#### Canalização

Para magias baseadas em perícias, adicione uma perícia para “Canalizar”. Quando desejar realizar algo mágico, você usa a perícia canalizadora para invocar o poder e utilizá-lo (esperamos). Para fazer isso, use uma ação de criar vantagem. Nesse caso, a vantagem que está tentando criar é um aspecto de **_Poder Invocado_**. Quando chegar a hora de conjurar a magia — em sua próxima ação, presume-se —, faça uma rolagem da perícia com a dificuldade Medíocre (+0), mas use quaisquer bônus obtidos pela ação de criar vantagem — então geralmente você receberá +2, +4 ou +6 por invocações gratuitas acumuladas e possivelmente pagando mais pontos de destino. Até agora, tudo ótimo — gere mana, gere um efeito. Agora, se quiser brincar mais um pouco:

- Se quiser lançar o feitiço _em uma ação_ , então precisará usar um     aspecto — quer de graça, quer gastando pontos de destino – sem     ganhar o bônus de +2. Isso faz com que a invocação rápida seja     bastante instável. Pode haver uma façanha que permita realizar a     magia de forma rápida e gratuita.

- A dificuldade para canalizar é Medíocre (+0) e a dificuldade real     da conjuração dependerá da magia. Em todo caso, um resultado     inferior a Regular significa que o poder saiu do controle. O perso-     nagem recebe estresse mental igual à diferença entre a rolagem e 0. 
- Regra opcional (Esgotamento): O conjurador pode optar por receber uma consequência previamente como parte da rolagem. Neste     caso, a caixa de consequência é marcada e o conjurador recebe um    bônus igual ao estresse que aquela consequência normalmente    absorveria.
- Outra regra opcional: Se quiser que o risco esteja na canalização    e não na conjuração, faça o seguinte. Peça que jogador que está    canalizando declare um nível de sucesso, algo entre Razoável (+2)    e Épico (+7), e então que faça uma jogada de superar contra a    dificuldade declarada. Se a rolagem falhar, ele recebe estresse igual    à diferença entre a rolagem e a dificuldade declarada. Se a rolagem    for um sucesso, então ele usará o nível de sucesso para a rolagem    da invocação. A regra sobre precisar de um aspecto ou façanha para    realizar conjurações rápidas ainda é válida, então ele terá que gerar    um impulso, comprar uma façanha ou gastar um ponto de destino para tal.

#### Pontos de Mana

Para um sistema que exige pontos de destino para realizar algo mágico, altere as regras de recarga. Cada vez que a recarga for reduzida em um, conceda um ponto de mana (PM) ao personagem como compensação. PMs são recuperados da mesma forma que pontos de destino e podem ser usados para alimentar magias ou aprimorar perícias mágicas, mas não podem mais ser usados como pontos de destino.

Aspectos “mágicos” podem gerar PMs em vez de pontos de destino quando usados como limitações ritualísticas. O que são essas limitações exatamente depende da natureza da magia, mas podem incluir coisas como orações diárias ou renunciar o uso de armaduras.

Se o sistema de magia usa mais pontos de mana, é fácil alterar a conversão de pontos de recarga — um único ponto de recarga pode fornecer 2 ou 3 PMs.

#### Magia de Sangue

Cada ponto de estresse físico que um personagem recebe gera um ponto de mana. Cada consequência física recebida concede pontos de mana igual ao nível da consequência — então uma consequência suave gera dois PMs — contato que a consequência seja um ferimento que sangre. Os PMs permanecem até que sejam usados para magia ou até que o estresse ou consequência sejam recuperados.

#### Poder Emprestado

Os humanos possuem a habilidade de manipular a magia, mas não a habilidade de criá-la. O poder deve vir de outra fonte, como itens, lugares ou seres de poder, mas cada um desses tem seu preço e suas exigências. Seguindo esse modelo, o termo “magos” é usado geralmente para representar aqueles que usam o poder, mas pode se tratar de um sacerdote, guerreiro sagrado, místico, druida ou o que for apropriado ao cenário. 

Em todos os casos abaixo, conectar a fonte de poder a um aspecto a torna mais firme. Poderes sem aspecto estão sujeitos a imprevistos e desconexões à vontade do Narrador.

**Itens de Poder** podem conter uma quantidade pequena de mana, mas deve ser mantidos por perto e ser usados durante a conjuração. A vasta maioria desses itens são acessórios descartáveis que se tornam inúteis após fornecer a carga. Criá-los leva cerca de um dia em um ambiente apropriado — como um laboratório para um alquimista, uma floresta para um druida e assim por diante — um custo moderado e uma rolagem de dificuldade Ótima (+4). Um sucesso faz com que o item seja preenchido com 1 PM e um sucesso com estilo o preenche com 2 PM. A quantidade permitida desses itens por personagem é igual ao seu nível na perícia mágica. Sim, isso significa que roubar os focos mágicos de um rival e escondê-los é uma forma excelente de roubar poder de um adversário. Também é possível criar um item poderoso que se reabastece todo dia. Fazer isso requer um mês de esforço a um grande custo e uma dificuldade similar. Um mago pode possuir apenas um único item desse tipo, o que o torna ainda pior se for roubado.

**Locais de Poder** concedem mana àqueles em sintonia com eles, de acordo com as regras específicas do local. Na maioria das vezes, eles concedem um único PM por dia ao nascer do sol, que deve ser usado naquele mesmo dia ou será perdido no próximo nascer do sol. No entanto, certos lugares possuem benefícios especiais (como conceder PMs extras, permitir que o mago mantenha uma reserva de 3 PMs ou permitindo ao mago respirar debaixo d'água) ou limitações (PMs que podem ser usados apenas para magias de fogo, perder todos os PMs se matar uma gaivota, etc.).

Uma vez que o personagem esteja em sintonia com o local, o benefício permanece indefinidamente, embora alguns locais forneçam benefícios extras se o personagem estiver presente, na maioria das vezes com ganho acelerado de mana. No entanto, obter e manter a sintonia é um tanto complicado.

Locais de poder são fortemente procurados por magos e outros seres mágicos, então normalmente há um proprietário com interesses adquiridos no lugar, especialmente porque a maioria desses locais possui um limite na quantidade de pessoas que podem se sintonizar com eles. Porém, mesmo sem se preocupar com tais guardiões, nem sempre fica óbvio como é possível sintonizar com um local, então conhecimento e pesquisa podem ser necessários.

Você perderá a sua sintonia com um local de poder se outra pessoa sintonizar e lhe expulsar, seja por tomar o seu lugar no local — se o local estiver no limite —, seja por ativamente remover sua conexão. Os detalhes dependerão do local.

Dessa forma, lugares de poder geralmente são valiosos para magos, mas também criam negociação e politicagem mágica. Ninguém quer gastar todo o tempo protegendo seus locais de poder, mas todos querem tantas sintonias quanto possível, e esse equilíbrio é o ponto central de muitas cabalas mágicas.

**Seres de Poder** oferecem muitos dos benefícios e efeitos dos locais de poder, mas sem os intermediários. O mago faz um acordo com o ser, compromete-se a respeitar suas regras e recebe uma certa quantia de poder — e possivelmente outros benefícios — em troca desse poder estar sujeito ao discernimento do ser e permitindo ao ser uma conexão constante com o mago — uma conexão que pode muito bem ser usada em outras negociações. 

A natureza precisa desse ser de poder pode variar — deuses, espíritos, bestas em forma de totem, lordes feéricos, construtos universais axiomáticos ou qualquer outra coisa. O truque com tais seres é saber como estabelecer o contato. Para alguns, isso é fácil, mas para outros pode envolver desvendar mistérios profundos.

Não há nada que impeça um mago de realizar pactos com múltiplos seres, pelo menos até que esses pactos entrem em conflito uns com os outros. Nesse ponto, o jogador pode descobrir que quebrar esses pactos também possui um preço.

### Efeitos

Vários destes efeitos fazem referência ao uso de uma perícia mágica, mas não veja isso como obrigatório — não significa que deve existir uma perícia mágica. Na verdade significa que qualquer perícia que você determinou que controla a magia deve ser usada nesse ponto.

#### O Que Há No Chapéu?

Se é porque o chapéu é mágico ou porque o seu portador é talentoso, não importa — o ponto é que o dono do chapéu coloca sua mão dentro e puxa algo para fora. As regras para isso são simples:

**Efeito 1:** Sem custo algum, o personagem pode retirar itens inúteis, pelo valor visual. Se o personagem gastar uma ação para puxar objetos inúteis, ele recebe um bônus de +1 em qualquer rolagem relacionada à conjuração em seu próximo turno.

**Efeito 2:** Ao custo de 1 PM, o personagem pode produzir algo útil, mas simples, como uma arma ou a ferramenta correta para a tarefa em questão. Não há rolagem de perícia relacionada, é apenas um facilitador para futuras rolagens.

**Efeito 3:** Ao custo de 2 PMs, o personagem pode puxar algo grande, perigoso ou estranho, que permita a ele usar sua perícia mágica em lugar de outra perícia, contanto que ele possa descrever como o objeto permite realizar tal rolagem específica. Por exemplo, um martelo gigante pode ser usado para atacar, uma mola pode ser usada para pular, uma nuvem de fumaça pode facilitar a furtividade e assim por diante. Se já tiver usado um determinado truque antes, receba -2 em sua rolagem.

**Efeito 4:** Ao custo de 3 PMs ou mais o personagem pode tirar uma criatura ou autômato capaz de ações independentes. Essa criatura ou autômato é um extra baseado em perícia — o conjurador seleciona a forma da criatura e sua perícia primária; o Narrador preenche quaisquer perícias secundárias que forem necessárias. Com 3 PMs, essa criatura pode ter uma perícia máxima em Razoável (+2), que pode ser aumentada em uma base de gasto de um PM para um ponto. Depois de determinado esse nível, o conjurador fará uma rolagem da magia, com dificuldade igual ao nível da criatura. Se a conjuração falhar, a criatura ainda é invocada, mas possuirá um certo número de complicações inesperadas equivalentes à margem da falha. Uma ou duas complicações podem ser inconvenientes, mas três provavelmente criará uma ameaça fora de controle ou outro problema grande.

**Efeito 5:** Um personagem também pode gastar todos os PMs restantes (mínimo de 1) e fazer um “puxão às cegas”. Isso traz algo grande, dramático e que, de uma forma ou de outra, termina a cena atual, mas é o Narrador que determina os detalhes exatos. Quando isso acontece, o Narrador secretamente rola um único dado Fate. Um + indica que a resolução foi a favor do jogador — um tufão os carrega para um local seguro, seus inimigos se transformam em rãs ou algo do gênero. Um resultado - funcionará contra o jogador de alguma forma — ele acaba se prendendo ou tornando a situação ainda pior. Em caso de 0 , a situação muda dramaticamente, embora não necessariamente para melhor ou para pior — os inimigos são transformados em um tipo diferente de inimigo, a paisagem se transforma em doce e assim por diante.

##### Variações

Tradicionalmente isso seria um chapéu, mas nada impede que seja um manto, uma bolsa ou algo similar.

Também é possível usar este modelo para representar grandes construtos de energia brilhante, como aqueles que aparecem nos quadrinhos. Nesse caso você remove o efeito 5, o bônus do efeito 1 e faz a falha do efeito 4 exigir mais PMs (numa base de um para um).

Para tornar isso ainda mais cósmico, você poderia reduzir o custo dos efeitos 2 e 3 em um. Isso fica bem perto do uso da feitiçaria como uma perícia para todos os fins, mas para certos gêneros — como super-heróis — isso pode cair bem.

Com uma mudança cosmética, as regras aqui podem se tornar um excelente sistema para lidar com certos aparatos da ficção científica, especialmente as não muito bem definidas, ferramentas para todo tipo de coisa, mesmo aquelas mais exóticas. Nesse caso, a manifestação física é substituída por descrições técnicas incompreensíveis e a interação por tecnologia. O efeito 0 é uma sequência de efeitos eletrônicos acidentais, o efeito 1 é praticamente inalterado, mas o efeito 3 basicamente permite que a “magia” seja usada para um super-hack, fazer qualquer coisa que o equipamento local seja capaz. O efeito 4 se aplica apenas quando existe algum aparelho para se controlar — como um robô ou carrega- dor mercadorias — e o efeito 5 está fora de cogitação, a não ser que o Narrador goste de situações como como “o que acontece se eu apertar todos os botões?”.

#### As Seis Pragas

Ao contrário do que parece, as seis pragas são os nomes dos seis maiores demônios do inferno. Seus nomes não podem ser perfeitamente enunciados por língua ou caneta, mas cada um possui um símbolo que pode conceder uma fração de seu poder sombrio. O poder é fácil de ser usado — basta fazer uma inscrição permanente através de uma tatuagem, marca com fogo ou escarificação para conceder o poder —, mas o conhecimento desses símbolos é mantido em segredo. Há rumores que só os demônios sabem o segredo e tomam a forma de mortais para compartilhá-lo, na esperança de enlaçar almas. As pragas possuem vários nomes, a maioria impronunciável — usamos os nomes mais comuns aqui, embora isso não garanta que todos as chamarão dessa forma.

**A Flecha** permite ao usuário realizar teletransporte até uma distância máxima de 30 metros (3 ou 4 zonas). O corpo do personagem precisa ser capaz de percorrer tal distância, portanto o teletransporte pode ser para cima, ou por cima de um poço, mas não pode atravessar uma parede ou grade, parando antes de ir de encontro a um obstáculo. Esse processo exige apenas um passo, então o personagem pode alcançar uma longa distância realizando saltos sequenciais. Cada salto custa um PM.

**A Pele de Ferro** fornece proteção contra ameaças físicas. Quando um personagem recebe dano físico, ele pode gastar PM para ganhar armadura igual ao número de PM gastos. Além disso, se o personagem sabe com alguma antecedência que um golpe está a caminho, ele pode gastar 3 PM para se fortificar e ignorar o dano. Embora isto seja inútil em uma luta, pode ser bastante útil para quedas perigosas ou conter o golpe mortal do carrasco, mas fique ciente que a proteção dura por uma fração de segundo. Ele pode sobreviver ao impacto de um trem, mas isso não garante que sobreviverá a queda ou, pior, se for arrastado para debaixo do trem.

**O Esplendor** , por 1 PM, concede o poder de conversar com qualquer cadáver que ainda esteja em boa forma física para falar. Convencer o cadáver a fazer outra coisa que não seja gritar é um problema totalmente diferente.

**O Cavaleiro** permite a um personagem gastar 1 PM para “saltar” para a mente de qualquer mamífero ao alcance da visão. Enquanto estiver em sua mente, ele não possui acesso aos seus pensamentos ou controle sob suas ações, mas pode acessar seus sentidos e permanecer com ele por cerca de dois minutos. O jogador pode optar por gastar um PM adicional para realizar outro “salto” para outro alvo em sua linha de visão. Isso também estende a caminhada por mais 2 minutos. Períodos extras de 2 minutos custam 1 PM. Enquanto isso acontece, o corpo do personagem permanece impotente, olhando para o nada, tornando-se uma presa fácil.

**A Visão** concede consciência de fenômenos sobrenaturais, mas acontece de forma irregular. Quando houver magia em funcionamento, o personagem sente um formigamento e pode gastar 1 PM para receber os detalhes sobre o que está acontecendo. A visão também pode carregar detalhes proféticos ou sonhos, embora esses sejam mais perturbadores do que úteis.

**O Terror** cria uma aura de horror em volta de um personagem por um 1 PM. A aura tem a duração de uma cena e os ataques do personagem podem infligir estresse físico ou mental, conforme desejado. Animais com um mínimo de autopreservação não entrarão na mesma zona e farão o possível para fugir dela. Por um PM adicional, o personagem pode encarar um alvo racional nos olhos e usar intimidação para criar o aspecto ___Medo de [Personagem]___ nele. Se bem-sucedido, ele pode invocar gratuitamente o aspecto pelo restante da cena.

Se um personagem não possuir PM suficientes para usar o poder, ele ainda poderá fazê-lo, mas sua alma estará em risco. Escolha um aspecto e o sublinhe – o personagem recebe o PM que precisa, mas esse aspecto agora está contaminado. Na história, isso significa distorcê-lo de forma sombria quando surgir a chance, e mecanicamente ele não produz mais pontos de destino quando forçado; em vez disso, ganhe um PM. Tais aspectos contaminados criam compulsões sombrias ocasionais, então o poder que vem com essa mácula traz um grande risco.

##### Variações

Pode haver versões mais poderosas de cada uma das marcas, embora seja melhor não discutir os requisitos necessários para obtê-las.

**Flecha:** Por 2 PMs, o alcance pode ser aumentado até onde se possa ver claramente.
**Pele de Ferro:** Golpes desarmados agora atingem como uma arma de metal e o personagem pode adicionar +2 por PM gasto para qualquer tarefa de força bruta.
**Esplendor:** Por 3 PMs o personagem pode animar qualquer cadáver em boas condições físicas, para se mover por uma cena. Ele seguirá instruções básicas e, mesmo não sendo muito útil em uma luta, é algo bem assustador.
**Cavaleiro:** Ao custo de 3 PMs por salto, o corpo do personagem desaparece. Ele reaparece atrás do último alvo.
**Visão:** Por 1 PM o personagem pode descobrir algum segredo sobre uma pessoa ou objeto. Não há garantia de que seja aquilo que queria saber e é permitido apenas um uso por alvo.
**Terror:** Ao gasto de 1 PM adicional (2 PMs no total) o personagem pode aumentar a intensidade de sua aura de medo. Agora ele causa dano mental igual a qualquer dano físico infligido num ataque.

## Fragmentos de Poder

Suponhamos que você tenha uma ideia para um sistema de magia que faz sentido dentro de sua proposta. Você pode explicar isso em uma linguagem normal e tem uma noção aproximada de como ele vai funcionar em seu jogo. Agora é hora de começar a pensar na mecânica e como reprsentar seu sistema em jogo.
 
Um pequeno aviso: não se sinta obrigado a resolver todos os problemas mecânicos. Se seu sistema de magia pode ser facilmente descrito e claramente compreendido, talvez não seja necessário mais do que uma perícia ou duas para representá-lo. Tome cuidado ao tentar pular diretamente para a mecânica — certifique-se de que realmente haja um problema antes de você introduzir uma mecânica para resolvê-lo, assim o produto final será muito mais consistente.

Quando chega a hora de introduzir uma mecânica nova, há duas coisas que ela precisa fazer — ou ao menos levar em consideração — um **resultado** e uma **limitação**.

O resultado é obvio — você quer ser capaz de lançar bolas de fogo, então os efeitos tratam de como mirar, quão grande elas são, quanto dano podem causar e assim por diante. A limitação é menos atraente, porém mais importante: ela responde à pergunta do por que você faria qualquer coisa _além_ de lançar bolas de fogo.

O que é importante notar é que o efeito pode não existir em apenas uma parte das regras. Ele estará entremeado com outras partes das regras de formas evidentes se estiver buscando por elas, mas que são facilmente negligenciadas se não estiver pensando nisso.

Sistemas de magia tendem a ser construídos com limitações que servem como estrutura para os resultados. Isto é, haverá um amplo conjunto de regras que controlam quando e como a magia pode ser usada — quais magias são conhecidas, com que frequência podem ser conjuradas, quem pode conjurá-las, etc. — enquanto as regras para resultados —como explodir coisas com bolas de fogo — normalmente são partes menores no texto, limitadas àquele feitiço em particular e outros similares.

Essa pode parecer uma distinção problemática, mas se você está criando o seu próprio sistema de magia, então isto é algo que realmente precisa ser considerado. O segundo ajuste lhe fornece imenso poder e flexibilidade para suas próprias criações, pois permite escolher o eixo da mudança.

Como exemplo, considere o clássico sistema de memorização de magias para conjuração. O recipiente — e, por extensão, o grande limitador — controla quais e quantas magias o personagem pode conjurar, enquanto os resultados são cada uma das magias individualmente. Isso permite uma variedade enorme, pois as limitações podem ser modificadas sem alterar os resultados — talvez introduzindo outra classe de personagem que recebe as mesmas magias com custos diferentes — ou você pode alterar os resultados sem mexer nas limitações, adicionando ou removendo magias.

Alterar resultados não é grande coisa. Trocar uma magia é fácil de ser feito e é uma ótima maneira de fazer coisas legais. Alterar as limitações é muito mais impactante, repleto de consequências possivelmente inesperadas. É também onde mora o grande poder dos ajustes. Talvez o importante seja o seguinte: ao entender essa divisão, você entenderá como pode construir um sistema de magia completamente novo simplesmente ao alterar um dos lados, ao invés de ter de reconstruir tudo a partir do zero.

### Limitações

As limitações em geral tomam uma de duas formas — uso ou oportunidade. __Limitações de uso__ afetam _quem_ pode usar a magia, enquanto as __limitações de oportunidade__ lidam com _como e quando_ a magia pode ser usada. 

A primeira coisa a se considerar a respeito de um sistema de magia é quem pode usá-la. Na ficção, a resposta pode ser “qualquer um”, mas mesmo nesse caso provavelmente será necessário pensar em alguma forma de representar esse sistema. Deixando de lado as limitações dentro da ficção, a porta de entrada para o uso da magia pode tomar a forma de um ou mais dos seguintes:

- Uma nova perícia
- Um aspecto específico
- Uma façanha
- Custo de recarga
- Oportunidade
- Custo de recursos

----

#### Façanhas E Recarga

Note que tratamos façanhas e recarga como duas coisas separadas. Isso porque são elementos separados, e embora o sistema padrão estabeleça que o custo de uma façanha é uma recarga, isso é apenas um sistema. Se estiver construindo seu próprio sistema, terá a liberdade de tratar isso de outra forma.

Isso abre várias portas em termos do que é uma façanha. Na prática, uma façanha é uma regra por si só — ou uma exceção à regra — e embora sua quantidade e natureza sejam restritas pelo custo padrão da recarga, você pode se libertar dessa estrutura. Como exemplo, _Senhores da Tempestade_ e _Seis Vizires_ na prática trazem um pacote de façanhas e apenas cobram um custo de recarga pelo pacote, basicamente oferecendo um desconto pelo conjunto temático.

---

#### Novas Perícias

Embora a adição de uma nova perícia “mágica” pareça ser a opção mais barata para permitir o uso de magia, tenha em mente o custo de oportunidade — o personagem está desistindo de outra perícia para usufruir da magia, então há uma troca. É possível incorporar a magia em uma ou mais perícias existentes, mas ao fazer isso você provavelmente exigirá algum outro custo.

#### Aspectos Específicos

Exigir um aspecto é tanto potente quanto um pouco trivial. Obviamente, aspectos falam bastante sobre o personagem, mas a menos que o aspecto necessário seja particularmente chato — e por que você iria querer algo assim? —, então não é um custo tão elevado. 

Isso não quer dizer que não há sentido em exigir um aspecto. A magia muitas vezes possui componentes do cenário, então um aspecto pode reforçar elementos da ficção. Aspectos também podem ser usados para ilustrar uma escolha em um cenário com vários estilos de magia, especialmente se você espera que todos os personagens possuam magia de alguma forma. Nesse caso, aspectos têm menos a ver com custo e mais com a diferenciação, o que pode ser algo bem forte.

#### Novas Façanhas

Façanhas são portais óbvios e legais para qualquer sistema de magia, pois assim como aspectos diferentes podem servir de entrada para estilos diferentes de magia, façanhas diferentes podem surtir o mesmo efeito. Por exemplo, se estiver usando um sistema que exija pontos de magia, façanhas diferentes podem representar formas diferentes de gerar esses pontos.

Claro, como as façanhas também podem ser resultados de um sistema de magia, você tem em mãos a possibilidade de criar interações bastante sofisticadas. É perfeitamente possível construir um sistema de magia de níveis hierárquicos como uma árvore, usando nada mais do que façanhas que dependem umas das outras como pré-requisitos. Tais sistemas são divertidos, mas muitas vezes precisam de uma elaboração complexa para que funcionem bem.

#### Recarga

Recarga é provavelmente o custo mais sério de todos e traz consigo muitos sentidos possíveis — mais especificamente, o que a diminuição de recarga significa?

Quando introduzimos a ideia de recarga em _Dresden Files RPG_, ela servia a dois propósitos : concedia aos personagens uma grande variedades de poderes em troca de liberdade, mas também destacou que a linha entre homem e monstro é tênue e que era possível ser consumido por seu poder — isto é, reduzir sua recarga a zero. Embora não haja a obrigação de fazer com que a recarga zero signifique a mesma coisa, a chave é que a recarga zero representa a perda de poder de atuação. Mecanicamente, significa que o personagem está incapaz de fazer qualquer coisa além do que seus aspectos estabelecem.

Os diversos sentidos da perda de poder de atuação são parte implícita de todos os sistemas de magia. Reduzir a recarga a zero pode certamente significar a morte de um personagem, mas geralmente é mais interessante significar que ele é privado de sua natureza mágica; personagens assim se tornam grandes vilões ou obstáculos mais à frente.

O problema é que embora a recarga como custo devesse _parecer_ um preço perigoso, isso não é verdade. Permitir que a recarga caia para zero é basicamente uma escolha do jogador de aposentar o personagem, algo que não acontece em jogo. Se estiver tudo bem para você, legal, mas se deseja que isso tenha um risco em jogo, então terá de encontrar uma forma de fazer essa escolha aparecer com mais frequência.

---

Em um sistema em que cada aspecto mágico reduz a recarga em um, mas aumenta os pontos de magia, você pode ditar que um personagem pode sempre escolher se desfazer de um de seus aspectos, convertendo-o em um aspecto mágico, aumentando assim sua reserva de mana. Além disso, assim que isso acontecer, a linha de estresse de mana do personagem é recuperada totalmente. Agora você tem uma razão para fazer com que isso aconteça em jogo, à medida que o personagem se entrega a seu poder para vencer um combate imbatível.


---

#### Oportunidade


Uma opção interessante para limitar os efeitos é exigir que não estejam disponíveis em todas as condições, mas fazer com que dependam de oportunidades apropriadas. Isso pode ser definido de várias formas, mas em geral se enquadra em duas categorias.

__Oportunidade no mundo__ são aquelas que dependem dos elementos do cenário de jogo. Talvez só seja possível conjurar magias em certos momentos ou lugares, ou em resposta a certos eventos. Isso é clássico em várias ficções de horror, onde coisas só podem ser chamadas “quando as estrelas estiverem alinhadas”.

Essas oportunidades tendem a funcionar mais como ganchos de história do que qualquer outra coisa, pois conduzem o jogo em direção a essas oportunidades. Se os mortos podem ser revividos no Colo de Shialla, então há um bom motivo para ir para lá. Se um Ancião Sombrio só pode ser invocado durante o eclipse lunar nas Planícies de Sangue, então a aventura praticamente se escreve sozinha.

No entanto, oportunidades no mundo são bastantes restritivas para os jogadores — são um custo alto para se fazer magia e, se for a única maneira de fazer tais coisas, então provavelmente não precisará de nenhum outro. Não quer dizer que esse meio seja inviável para um jogo — pode funcionar muito bem com personagens de nível alto em Conhecimento —, mas traz restrições bastante específicas.

Alternativamente, isso pode ser usado em conjunto com outro sistema de magia para superar as limitações comuns — dessa forma, magias que são mais poderosas ou impossíveis de se usar de outra forma, podem ser conjuradas nas circunstâncias adequadas. Estruturalmente, isso ainda não passa de usar a magia como auxílio ao enredo, e isso não é um problema.

**Oportunidades de jogo** são uma abordagem totalmente diferente. Nesse caso, algo envolvendo as regras do jogo deve acontecer para tornar a magia possível. Pode ser intencional, como gastar uma ação para invocar um poder, ou incidental, como uma habilidade que transforma um impulso em algo único.

Oportunidades intencionais tendem a funcionar como lombadas para magia e por isso bastante populares como mecânica de equilíbrio. A ideia é que se a magia pode ser usada com menos frequência, é aceitável que seja mais poderosa. Infelizmente, essa abordagem possui algumas fraquezas ocultas. Frequência é algo difícil de se equilibrar por ser tudo ou nada — tende a significar que o jogador do conjurador ou estará entediado, fazendo nada além de acumulando poder, ou estará mais poderoso que todos. Se você procura por algo assim, tente encontrar uma forma de tornar a parte não mágica de uma conjuração divertida e envolvente. Um truque é permitir que a “carga” possa ser acumulada ao longo de momentos de ação interessante.

Como alternativa, a magia pode ser uma expansão de regras existentes, trazendo mais cor ao jogo. Considere usar a magia para expandir resultados convencionais. Se você ataca com o Kung Fu da Terra, então um impulso poderia lhe fornecer um aspecto, mas também poderia fazer o seu adversário recuar uma zona. Isso pode parecer um pouco avesso às expectativas, se um jogador pensar em termos de “quero fazer ele recuar”, mas faz muito mais sentido quando pensa em termos de expansão das capacidades normais.

Há também a questão da **oportunidade de acesso** , que existe mais no âmbito da criação do personagem. A magia pode exigir uma perícia ou um aspecto mágico apropriado, como discutido anteriormente.

#### Recursos


Recursos envolvem um custo literal na conjuração da magia. Os clássicos podem incluir mana, pontos de magia ou magias descartáveis, mas você não está limitado a apenas isso. Uma magia pode exigir a invocação de um aspecto — e conjurar a magia em vez de receber o bônus de +2 —, receber estresse ou até mesmo consequências. Obviamente, o preço terá impacto na
frequência de uso da magia.

Recursos também podem adicionar um sabor extra ao sistema de magia — talvez eles nem sempre sejam necessários, mas podem ser usados para tornar a magia mais potente. Coisas como estresse e consequências podem ser bons para isso, representando magias tão perigosas quanto poderosas. Uma ressalva, no entanto — tente não ligar isso a magia explicitamente de combate. Quando isso se torna uma questão de matemática do tipo “eu posso receber X estresse para causar Y estresse no oponente”, a ideia começa a soar menos mágica.

Recursos e oportunidades podem se sobrepor um pouco nas perícias do jogador. Uma rolagem secundária de perícia para gerar mana pode exigir tanto recursos (mana) quanto uma oportunidade (a segunda rolagem de perícia). Isso pode parecer vago, mas é algo bom — interligar diversos elementos do personagem é uma coisa boa.

Um outro recurso óbvio são os pontos de destino e é razoável que sejam usados como combustível de magia da mesma forma que fazem com a invocação de aspectos. Você pode até mesmo fazer com que “mana” seja uma subcategoria de pontos de destino.

### Efeitos

Falamos sobre todas as coisas que podem controlar o uso da magia, mas o que a magia pode fazer quando usada? Em termos de ficção e cores, o leque de possibilidades é amplo, mas no momento estamos procurando as ferramentas mecânicas disponíveis para expressar os efeitos mecânicos. Embora seja possível criar mecânicas inteiramente novas para esse tipo de coisa, é melhor começar com os elementos que já temos. Assim como nas limitações, há uma lista básica de elementos com os quais podemos trabalhar:

- Fiat
- Aspectos
- Perícias
- Façanhas
- Estresse e Consequências
- Extras

#### Fiat (decreto)

Isto é uma palavra em latim que basicamente significa “você descreve algo e isso acontece”. Há diversas áreas de magia que podem ser tratadas dessa forma. Em alguns casos, pode ser que apenas isto seja necessário, especialmente se todos na mesa concordarem, mas é uma fonte de desconforto em potencial quando as expectativas diferem. Ainda assim, há diversos efeitos que são melhores de lidar desta maneira, mesmo que sejam mecanicamente importantes — o efeito mágico pode ser uma porta de entrada para outras regras.

Por exemplo, se a magia permite ao personagem respirar debaixo d’água, isso é um efeito de decreto — simplesmente é verdade. Não há necessidade de atribuir um aspecto ou uma perícia para representar isso, a não ser algum outro elemento da magia entre em jogo — como a inabilidade de respirar fora d’água. Seria possível atribuir um aspecto que nunca é invocado ou forçado, mas o efeito é praticamente o mesmo. Dito isso, uma vez que o personagem esteja embaixo d’água, ainda há outros elementos mecânicos a considerar — perícias físicas, por exemplo.

Magia com efeitos mecânicos triviais decretados podem ser absurdamente potentes ou importantes em um cenário. Considere algo como a juventude eterna – não há muitas mecânicas envolvidas, mas é um efeito que pode conduzir toda uma campanha. O fato dos efeitos decretados não exigirem que você pense muito na mecânica deveria significar que é necessário pensar mais nos elementos não mecânicos.

#### **Aspectos**

Os aspectos podem ser bem parecidos com os efeitos decretados, mas com um pouco mais de peso. Se tudo o que os aspectos fazem é reforçar a “oficialidade” de um decreto, tudo bem, mas você também pode considerar efeitos mecânicos a partir deles.

Muitos desses efeitos podem ser tratados com as regras normais para invocar e forçar. A magia apenas permite uma maior flexibilidade em termos de lógica e aparência do aspecto usado, e embora isso possa não ser tão óbvio no papel, pode ser algo de uma força incrível na mesa.

Dito isto, é perfeitamente razoável usar a magia para adicionar alguns efeitos ao invocar e ao forçar aspectos. Esses efeitos adicionados podem ser decretados ou possuir algum efeito mecânico. Um aspecto como **_Passo das Sombras_** pode ser invocado para atravessar várias zonas de uma vez. Um aspecto como **_Armadura de Luz_** pode ser invocado — e consumido — para evitar uma consequência.

Uma coisa a ser considerada é se o efeito cria um novo aspecto ou não, ou se adiciona um novo efeito a um aspecto existente. Às vezes a lógica do efeito deixará a resposta óbvia, mas em caso de dúvida tente expandir o alcance do aspecto. Ao fazer isso você não apenas evita o excesso de aspectos, como também incentiva a interação com aspectos já existentes.

#### Perícias

Magia pode ser usadas em lugar de perícias e, na verdade, um dos sistemas de magia mais simples e comum é usar uma perícia mágica para substituir outra perícia, como lançar rajadas de energia como equivalente a uma perícia com armas. Isso é suficientemente bom, mas só serve de verdade como uma ferramenta de uso geral.

Mais interessante, a magia pode ser usada para ampliar o alcance de perícias existentes, levando-as a um nível sobrenatural. Isso pode ser uma ampliação de capacidade, como sua percepção se estendendo para visão infravermelha ou planos espirituais, ou podem ser ganchos mecânicos como “O Martelo do Terror”, alterando a perícia com armas para que, ao conseguir um impulso, você também inflija dano mental.

#### Façanhas

A magia pode adicionar novas façanhas ou alterar as que já existem, mas o que você encontrará com maior frequência é a adição de façanhas mágicas — façanhas com efeitos mágicos que, em geral, são mais potentes que as façanhas normais, mas que vêm acompanhadas de custos e requisitos. 

O exemplo mais óbvio para isso é uma façanha que permite a você fazer algo mágico, como teleporte na linha de visão ou se transformar num rato, mas a um custo. O custo pode ser tão simples quanto gastar pontos de mana ou pode ser algo complexo e ritualístico. Como façanhas possuem suas próprias regras, os componentes de custo e equilíbrio são mais claros do que outras abordagens, então é importante pensar sobre eles desde o começo.

#### Estresse e Consequências

Um efeito fácil e óbvio de magia é mexer com estresse e consequências. Magias de cura podem recuperá-los ou magias de proteção podem melhorar o potencial de resistência. Apenas tenha cuidado para que não deixar os magos monstruosamente imbatíveis — a não ser que essa seja sua intenção.

#### **Extras**

Magia é uma ótima forma de ter permissão para criar extras. Seja invocando de um cão de fogo ou invocar relâmpagos que crepitam ao longo da lâmina de sua espada, extras podem ser uma maneira razoável de lidar com isso.

Isso é um ótimo exemplo de como criar “extensões” em um sistema de magia. As regras de extras são robustas, mas também bem abertas. Se todo o sistema de magia se tratar apenas da criação de extras, eles rapidamente sairiam do controle, logo é importante impor algumas limitações. As limitações devem ser consistentes o bastante para dar a sensação de um sistema de magia coerente. Se você se certificar de que cada extra novo faça sentido no contexto dos que já existem, perceberá que a lógica do seu sistema de magia surgirá de forma bem orgânica.

# Capítulo 9 - Subsistemas

## Kung Fu

---

O termo “kung fu” é bem abrangente nessa sessão, mas esta é apenas uma das várias artes marciais do mundo. As regras aqui podem se aplicar ao savate ou ninjutsu facilmente, ou a qualquer arte marcial louca que seja praticada em Marte.

---

O _Kung Fu_, como vemos na vida real, é coberto pela perícia Lutar. Falaremos aqui sobre o _Kung Fu_ cinematográfico, com grandes pulos, acrobacias e o estilo melodramático dos filmes de artes marciais de Hong Kong.

Esse tipo de _kung fu_ é tão dramático que não se encaixa em jogos mais realistas, e com certeza altera o tom até mesmo uma ação aventuresca do tipo _pulp_. Pode ser um pouco demais. Se pretende incluir este estilo de arte marcial louco e exagerado em seu jogo, deixe claro para seus jogadores que isso é algo que todos os personagens podem fazer – tanto PCs _quanto_ PdNs.

Em um filme de _kung fu_, os praticantes da arte estão um nível acima dos praticantes normais. Se o seu jogo se concentrará fortemente em mestres de _kung fu_, então faz sentido que a maioria — se não todos — dos personagens dos jogadores possuam essa habilidade. Se apenas um personagem pratica _kung fu_, certifique-se de que os outros personagens possuam sua própria forma de se destacar. Além disso, você pode querer suavizar algumas loucuras descritas nesta seção. Você não quer apenas um personagem dominando a ação de uma cena ao sair saltando por todos os lugares. Se todos têm a habilidade, então o risco de alguém monopolizar a atenção é menor. O _kung fu_ cinematográfico é muito poderoso. Se você o introduz, é provável que domine todo o seu cenário.

Os mestres do _kung fu_ podem correr pelas paredes, saltar grandes distâncias, parar projéteis em pleno ar ou apará-los com uma arma, lutar em condições precárias de apoio e até mesmo andar sobre a água. Praticamente qualquer pessoa que tenha treinamento nas artes marciais pode fazer isso. Se um personagem possui treinamento em kung fu , então essas ações são tratadas como uma ação normal de correr ou saltar. Um jogador pode descrever seu personagem realizando essas ações sem nenhuma dificuldade especial. O Narrador pode exigir rolagens, mas esse tipo de ação não é algo excepcionalmente difícil ou fora do comum. Basta adicionar esses detalhes para que seu jogo tenha a mesma sensação de um filme de _kung fu_.

Há duas maneiras de abordar o _kung fu_ em Fate. Um método é usar perícias existentes para cobrir as habilidades e luta com a arte marcial. A segunda é criar uma perícia _Kung Fu_ dedicada a isso. A decisão de qual método adotar depende da natureza do seu jogo. Se há apenas um mestre _kung fu_ em jogo, então a perícia _Kung Fu_ pode ser apropriada. Se todos possuem treinamento na arte, então talvez faça mais sentido que todos usem perícias já existentes para realizar as façanhas do _kung fu_. Se for por esse caminho, um praticante do kung fu deve possuir um aspecto relacionado ao seu treinamento, para poder disponibilizar as habilidades da arte ligadas às perícias existentes — algo como ___Treinado no Monastério Wudang___ ou ___Mestre no Estilo Louva-a-Deus___.

### Usando Perícias Existentes

Se preferir usar o método das perícias existentes, então Lutar obviamente ficará com a maior parte do trabalho. Qualquer ataque, desarmado ou com armas, depende da perícia Lutar, além de também poder ser usada em defesas. Como um mestre de _kung fu_ também pode aparar flechas e outras armas de projétil, esse feito também seria realizado com a perícia Lutar. Se estiver jogando em um cenário moderno ou se houver armas de fogo no universo de jogo, será necessário tomar uma decisão sobre o uso do _kung fu_ contra elas. Não parece fora do gênero sugerir que um praticante de _kung fu_ possa desviar ou mesmo aparar balas, especialmente se ele usa uma espada ou outra arma de metal.

Andar em paredes e saltar estão incluídas em Atletismo. Qualquer tipo de façanha física além de lutar será coberta por Atletismo ou possivelmente Vigor. Os praticantes de _kung fu_ tradicionalmente possuem treinamento para resistir a dano ou sobreviver a quedas, que são ações da perícia Vigor. A perícia Conhecimentos pode ser usada para identificar escolas rivais ou técnicas dos oponentes. Uma rolagem de Conhecimentos pode criar uma vantagem, se você deduzir corretamente quem é o professor ou escola de luta de seu oponente.

### A Perícia _Kung Fu_


Se adicionar uma perícia _Kung Fu_ ao jogo, qualquer habilidade que não se enquadrarem no que é considerado normal para Lutar ou Atletismo, como saltar e andar pelas paredes, exige uma rolagem de _Kung Fu_. Isso pode ser bastante útil se você desejar destacar as ações de _Kung Fu_. Os praticantes de _kung fu_ normalmente possuem movimentos especiais ou segredos conhecidos apenas por eles e seus mestres. Em um jogo com muitos artistas marciais, esses movimentos especiais são uma boa forma de diferenciar os personagens e criar a sensação de que há vários estilos diferentes. Movimentos especiais podem ser modelados usando façanhas e aspectos.

Apesar da maior parte dos movimentos no _kung fu_ provavelmente possuir nomes, um mestre de _kung fu_ pode possuir um ou dois movimentos característicos que representam seu estilo pessoal. Esses podem ser representado por aspectos, como o **_Toque da Morte_** ou **_Estilo da Lâmina Escondida_**. Quando invocado, o jogador pode descrever em detalhes a natureza incrível ou mística daquela ação. Esses aspectos devem fazer com que o personagem pareça incrível e você pode usá-los para mostrar que PdNs conhecem o seu personagem e seu estilo, como “Não se aproxime, ela é mestre do Estilo da Lâmina Escondida”. Eles podem ser forçados se um oponente conhecer um movimento que sirva de contra-ataque, assim como o jogador pode usar seu próprio conhecimento de _kung fu_ para analisar o estilo de um oponente e deduzir como contra-atacar.

Para uma versão mais mecanicamente completa, use o sistema de façanhas para criar movimentos. Você pode criar toda uma escola de _kung fu_ usando façanhas, com façanhas mais fáceis como pré-requisitos para acessar as mais avançadas. Use a descrição de suas façanhas para criar um estilo de _kung fu_ marcante, com os nomes e efeitos dos movimentos retratando o tema do seu estilo de luta.

#### Punho Bêbado

Abaixo segue um exemplo de uma árvore de façanhas de _kung fu_ para o estilo do Punho Bêbado.

**O Vacilo do Bêbado:** Você balança e vacila nos passos, evitando os golpes do adversário aparentemente por acaso. Quando bem-sucedido numa rolagem de Atletismo para se defender usando esta técnica, você recebe +1 no seu próximo ataque contra o oponente que tentou acertá-lo. Se for bem-sucedido com estilo, recebe +2.

**O Empurrão do Bêbado:** Seu empurrão rude e bruto contém mais poder do que parece possível. Você recebe +2 ao usar Vigor para criar vantagem contra um oponente ao tentar tirar-lhe o equilíbrio.

**Beber do Jarro:** Você faz uma pausa momentânea para tomar um gole de vinho de seu jarro, fortalecendo-se para a batalha. Quando você toma um gole durante uma luta, limpe sua menor caixa de estresse. Isso exige gastar uma ação inteira bebendo.

**A Queda do Bêbado (Exige o Vacilo do Bêbado):** Quando um inimigo o ataca você perde o equilíbrio e cai, rapidamente voltando a ficar de pé, mas seu inimigo agora se encontra perigosamente exposto. Role Atletismo para esquivar. Em caso de sucesso, coloque um impulso em seu adversário como Guarda Baixa ou então Desequilibrado que pode ser usado por qualquer um contra ele. Em um sucesso com estilo, coloque um segundo impulso em seu oponente. 

**A Dança do Bêbado (Exige o Empurrão do Bêbado):** Seus golpes são brutos e óbvios, mas, ao esquivar-se, seu adversário parece ser atingido por um cotovelo ou joelho acidentalmente. Faça uma rolagem de Lutar normal. Se atingir o seu oponente, você causa estresse normalmente. Se você errar ou empatar, seu oponente recebe um de estresse físico de qualquer forma.

**Derramar Vinho (Exige Beber do Jarro):** Você pega um copo e despeja uma dose do jarro. Essa tarefa difícil e elaborada causa uma pausa na batalha. Ninguém pode atacá-lo enquanto estiver derramando e você remove sua caixa de estresse mais baixa. Essa técnica exige uma ação completa e não pode ser realizada mais de uma vez em seguida.

**O Bêbado Tropeça (Exige A Queda do Bêbado):** Você cambaleia e tropeça sem controle, mas seu inimigo sempre o erra e acaba acertando algum obstáculo próximo, causando dano a si mesmo. Quando você se desvia de um golpe usando Atletismo, seu oponente recebe um de estresse ou dois se você for bem-sucedido com estilo.

**A Firmeza do Bêbado (Exige a Dança do Bêbado):** Você cambaleia e parece prestes a cair, então se aproxima e agarra o braço de seu adversário para firmar-se. Esse ato aparentemente desproposital bloqueia o _qi_ e paralisa o seu oponente. Você pode colocar um aspecto de situação como ___Qi Bloqueado___ em seu oponente com uma invocação grátis. Seu adversário não pode usar nenhuma manobra de _kung fu_ enquanto não remover o aspecto.

## Cibernética

A forma mais fácil de lidar com implantes cibernéticos em Fate é com as ferramentas que já existem. Quer ser um ciborgue? Coloque um aspecto ou dois representando o fato de você possuir uma ou duas melhorias. Seus braços mecânicos lhe dão força sobre-humana? Coloque sua perícia Vigor ou Lutar como a mais alta. Seus olhos possuem mapeamento térmico? Uma façanha pode ajudar com isso.
 
Esta seção é para quem estiver em busca de um sistema para implantes cibernéticos que se mantenha por si só ao invés de modificar sistemas já existentes ou quem quiser uma justificativa para as coisas que já colocaria, é para isso que essa sessão foi criada.

### Próteses vs Melhorias


Implantes cibernéticos trazem um custo para a pessoa. Muitos indivíduos possuem próteses cibernéticas ocasionais, um membro simples para imitar sua versão humana, anexado para compensar uma perda ou ferimento. Todas essas novas peças custarão algum dinheiro e tempo em uma clínica de reabilitação, deixando-o novo em folha — praticamente. Se tudo o que quer for uma ou duas próteses, não é preciso pagar a recarga nem possuir qualquer aspecto ligado às próteses, a não ser que queira isso.

Melhorias são totalmente diferentes. Enquanto uma prótese é desenvolvida para fazer parte do corpo, imitando funções naturais da melhor forma possível, uma melhoria é instalada para _aperfeiçoar_ o corpo humano, tornar uma pessoa mais forte, rápida, durável, com armas naturais ou capaz de coisas que os humanos não conseguem por si só. Enquanto há pessoas que obtêm próteses para repor uma parte perdida e voltar à sua forma completa, muitas outras removem partes do próprio corpo para para instalar melhorias. 

Essas melhorias precisam de uma fonte de energia, algo além da bioenergia que o corpo humano produz. Para esse fim, as pessoas que recebem sua primeira melhoria ganham um coração cibernético. Não é literalmente um coração. Ele não substitui seu coração humano a menos que seja necessário, mas funciona como um bom reserva se possuir um verdadeiro que funcione. Um coração cibernético é, antes de mais nada, uma fonte de energia. É um capacitor que se recarrega com as próprias fontes de bioenergia do seu corpo, que pode ser plugado a uma tomada como complemento, e permite que você alimente uma ou mais melhorias.

Isso significa duas coisas em jogo. Primeiro, é obrigatório dedicar ao menos um aspecto ao fato de ser um ciborgue. Você voluntariamente transcendeu suas limitações humanas — uma característica que define qualquer pessoa que tenha escolhido esse caminho. O segundo custo mecânico é reduzir a recarga em 1, em troca da seguinte melhoria.

__Coração Cibernético:__ Você pode instalar e usar outras melhorias cibernéticas. O coração também atua como um suporte para seu coração natural e filtra toxinas e impurezas de seu sangue. Se você for submetido a veneno ou toxina, gaste um ponto de destino para ignorá-los. Se você for morto por algo que poderia ter sido evitado pelo coração cibernético, gaste um ponto de destino conceder a luta ao invés de ser tirado de ação.

### Tipos de Melhorias

Há dois tipos de melhorias: menores e maiores. Obtê-las custa dinheiro, mas também tempo, além de poder custar recarga também.

**Melhorias menores** são pequenas mudanças, coisas que não exigem muito do corpo ou que não exigem grandes alterações no seu sistema biológico. Um novo olho, uma mão, implante de pele ou o aperfeiçoamento de uma melhoria existente — estas coisas são melhorias menores. Se for comprar e instalar uma melhoria menor durante o jogo, você precisará de uma rolagem de Recursos com uma dificuldade Regular (+1) para cada melhoria adicional que estiver instalando. Também precisará se recuperar da cirurgia. Você recebe uma consequência moderada que se recupera normalmente. Independente se você escolher as melhorias menores durante o jogo ou durante a criação do personagem, para cada três delas que pegar, pagará 1 ponto de recarga. Ao receber sua primeira melhoria menor, pague o ponto de recarga. Ao receber sua quarta melhoria menor, pague novamente e assim por diante. Instalar uma prótese exige o mesmo custo em Recursos e consequências, mas não exige
o custo de recarga.

**Melhorias maiores** requerem cirurgias grandes e substituição de uma parte grande do corpo. Membros são sempre melhorias maiores, assim como órgãos e qualquer coisa que se conecte ao seu cérebro. Ao fazer uma melhoria maior durante o jogo, faça uma rolagem de Recursos contra dificuldade Bom (+3) para _cada_ melhoria maior que fizer. Além disso, fazer uma cirurgia desse porte impõe uma consequência severa, que se recupera normalmente. Seja durante a criação do personagem ou durante o jogo, _cada_ melhoria maior custa um ponto de recarga.

### Exemplos de Melhorias

**Olho Cibernético (menor):** Você recebe um bônus de +1 em rolagens da perícia Notar relacionadas à visão. Além disso, escolha um dos seguintes aspectos. Escolher um aspecto adicional é considerado como outra melhoria menor.

**_Filtro de  Imagem ;  Visão Em Pouca Luz  ; Visão de  Sonar ; Interface de  Mira ; Imagem Térmica ; Interface Visual de  Rede ; Ampliação de Imagem._**

**Pernas Cibernéticas (maior):** Suas pernas foram substituídas por pernas cibernéticas muito mais poderosas. Você pode se mover duas zonas como uma ação livre e recebe +2 nas rolagens de Atletismo para correr ou pular. Além disso, escolha umadas extensões a seguir. Extensões adicionais são consideradas melhorias menores.
+ **Compartimento Secreto:** Você possui um compartimento onde pode esconder algo como uma arma de mão ou um tablete de cocaína.
+ **Aderência Magnética:** Se estiver de pé sobre uma superfície metálica, você não pode ser derrubado. Você também pode caminhar em superfícies metálicas íngremes ou mesmo verticais, embora desajeitadamente.
+ **Chute Pneumático:** Se chutar alguém, seu golpe é considerado uma Arma: 2.

**Interface Neural (maior):** Você pode acessar a Rede de qualquer lugar, com um pensamento. Você pode invadir sistemas de baixa segurança automaticamente — eles simplesmente fazem o que você quiser. Mesmo sistemas de alta segurança são fáceis — você recebe um bônus de +2 nas rolagens de Computadores para invadir.

**Unhas Afiadas (menor):** Você tem três centímetros de lâminas afiadas que saem da ponta de seus dedos; podem ser recolhidas quando quiser. São consideradas Arma: 1.

**Blindagem Subdérmica (maior):** Você pode conter a maioria dos ataques físicos usando a perícia Vigor — socos, lâminas, cassetetes e armas de pequeno porte têm dificuldade em ultrapassar as placas de metal sob de sua pele. Além disso, uma vez por cena você pode ignorar uma consequência física menor ou moderada vinda de um ataque desse tipo. 

**Camuflagem Térmica e Óptica (menor):** Você pode gastar um ponto de destino para se tornar invisível aos espectros visuais e térmicos, contanto que não se mova.
 
### A Desvantagem das Melhorias

Embora não mencionado acima, cada melhoria tem suas desvantagens. Uma interface neural pode ser invadida, dando acesso ao seu cérebro. Camuflagem térmica pode entrar em curto circuito debaixo d’água, causando um choque elétrico violento. É por isso que você possui um aspecto ligado ao fato de ser um ciborgue.

Narradores, sintam-se livres para fazer cumprir a desvantagem de uma melhoria — qualquer que seja a decisão do grupo. Isso é uma ação de forçar o aspecto do ciborgue, o que significa que o jogador colocado em tal situação pode recusar a ação de forçar ou receber um ponto de destino pelo problema no qual você o colocou.
 
 
## Dispositivos e Ferramentas

No Fate Sistema Básico, equipamentos podem ser simples como um aspecto, como um **_Lançador de Arpéu Magnético_** , ou uma façanha, como “ _Lançador de Arpéu Magnético:_ +2 para superar usando Atletismo ao escalar ou balançar de um local ao outro quando houver uma âncora de metal por perto”. Porém, para um pouco mais de detalhes, isso pode ser combinado em um único extra como um _dispositivo_.

### Funções e Defeitos

Dispositivos já vêm com dois aspectos grátis – um **aspecto de função** e um **aspecto de defeito**. A função diz qual o propósito do dispositivo e o defeito representa algum problema existente. Você pode pensar na função como o conceito e seu defeito como a dificuldade ou uma consequência que nunca desaparece. Isso não influencia os aspectos pessoais de seu personagem.

---

#### Lançador De Arpéu Magnético

**Função: _Gancho Eletromagnético de Alta Potência_**
**Defeito: _Ainda Resolvendo Falhas_**

---

### Façanhas

Dê uma ou mais façanhas ao dispositivo para refletir as vantagens mecânicas que ele confere a seu usuário. Essas façanhas custam uma recarga cada.

---
#### Lançador De Arpéu Magnético

##### Aspectos

**Função: _Gancho Eletromagnético de Alta Potência_**
**Defeito: _Ainda Resolvendo Falhas_**

##### Façanhas

**CLANG!:** Gaste um ponto de destino para firmar a garra magnética em um objeto metálico de uma forma dramática, como um veículo se movendo em alta velocidade, um rifle sônico que esteja caindo ou a parede do outro lado de um abismo em uma estação espacial de construção questionável.

**Tiro Malandro:** +2 para criar vantagem usando Atirar quando usa a lançador de arpéu para balançar, desarmar um oponente ou criar uma barreira.

**Custo:** 2 De Recarga

--- 
 
Os dispositivos também não precisam ser dispositivos no sentido literal. Você pode facilmente, por exemplo, usar estas regras para criar itens mágicos em um cenário de fantasia.

---

#### O Anel Da Verdade


##### Aspectos
**Função: _Detector Mágico de Mentiras_**
**Defeito: _O Portador é Amaldiçoado a Dizer Sempre a Verdade_**

##### Façanhas

**Perfurar o Véu da Mentira:** Gaste um ponto de destino para, durante a cena, perguntar até três vezes ao Narrador se alguém está mentindo. O Narrador, por sua vez, deve responder verdadeiramente.

**Custo:** 1 De Recarga


---
 
### Defeitos Adicionais

É possível incluir falhas adicionais para reduzir o custo em recarga do dispositivo, ao valor de uma recarga por defeito adicional. O custo mínimo para um dispositivo com qualquer quantidade de façanhas é 1 recarga, independentemente de quantas falhas ele possua.

Narradores, esses defeitos não podem ser superficiais, como ***Alguma Interferência Quando Está no Fundo do Oceano***. Se acabar deixando algo assim passar despercebido, certifique-se de que o jogador saiba que terá que passar mais tempo do que o esperado nas profundezas do oceano; na mais funda das profundezas.
 
---

Um defeito adicional para o Lançador de Arpéu Magnético pode ser ***Pesado e Desengonçado.***

O Anel da Verdade pode ter o defeito adicional ***Procurado Pelos Servos de Ssask, Deus da Mentira.***
 
---
 
## Monstros

As regras para PdNs no Fate Sistema Básico são ótimas para criar pessoas — outros humanos cujos objetivos entram em conflitos com os dos PJs —, mas como Narrador você também pode querer incluir alguns monstros em seu jogo. Com estas ferramentas, você pode criar monstros que são inumanos e complexos, desafiando os jogadores a encontrar abordagens inteligentes quando lidam com este tipo de antagonista.

### Aspecto de Instinto

Para criar monstros interessantes, comece delineando algumas motivações básicas que levam o monstro a agir. O que o leva a correr riscos? Com o que eles se importam o suficiente para entrar em conflito com os jogadores? É provável que possuam motivações inumanas — desejos que humanos comuns provavelmente nunca teriam.

Pegue sua ideia inicial e concentre-a num **aspecto de instinto**. Personagens monstros podem usar o seu aspecto de instinto normalmente, mas recebem +3 em lugar de +2 ao invocá-lo. Monstros muitas vezes possuem motivações únicas para alcançar seus objetivos e os jogadores precisarão de esforço para superar esses oponentes.

---

Nicolas está organizando um jogo de horror urbano. Ao elaborar um grupo de zumbis, eles recebem o aspecto de instinto _**Famintos por Cérebros**_. A qualquer momento em que ele invocar esse aspecto, eles recebem +3 na rolagem.

---
 
### Habilidade dos Monstros

 
Monstros são diferentes dos outros PdNs porque suas habilidades tendem a desafiar as regras e perturbar o fluxo normal dos conflitos. Muitos monstros são cenas de combate inteiras esperando para acontecer, conforme os jogadores tentam descobrir como derrotar um inimigo que muda as regras do jogo. 

Alguns exemplos de habilidades interessantes para monstros são:

+ **Duro de Matar:** Monstros podem sobreviver a muito mais danos que outros personagens, seja por causa de mais caixas de estresse — como o monstro de Frankenstein — ou porque podem se regenerar rapidamente — como a Hidra.
+ **Imune a Dano:** Monstros frequentemente são imunes a todo dano, exceto um tipo — como vulnerabilidade a prata — ou até que uma certa condição específica seja alcançada — como destruir um item mágico específico.
+ **Tendência a Mudanças:** Monstros tendem a se transformar — como vampiros que se transformam em morcegos para fugir — ou transformar o ambiente — como invocar mais capangas menores no meio de uma luta.

Embora seja fácil de ver como essas características poderiam ser transformadas em façanhas, são demasiadamente poderosas para serem ativadas sem o gasto de pontos de destino. No entanto, se você adicionar tal custo, os jogadores podem forçar uma vitória contra a Hidra apenas por esperar que acabem seus pontos de destino. Além de criar uma fraqueza para o seu monstro, tais custos tornam os conflitos entediantes. Quem é que quer jogar até que o Narrador fique sem pontos de destino?

Ao invés de adicionar um custo, você pode adicionar uma **fraqueza** ao monstro para que ele seja capaz de ativar a façanha sem gastar pontos de destino. Se adicionar uma **fraqueza menor**, você ainda tem que pagar um ponto de destino no início da cena em que o monstro usa o poder, mas se adicionar uma **fraqueza maior**, precisará pagar pontos de destino para usar a façanha.

Quando os PJs descobrem e usam uma fraqueza menor, o monstro ainda poderá usar a façanha, mas agora ele deve pagar para cada vez que usar a façanha. Se os PJs descobrirem e usarem uma fraqueza maior, no entanto, o monstro perde a façanha completamente.

---
 
Nicolas decide criar um demônio chamado Masabra. Ele inclui uma façanha que permite que Masabra seja imune a ataques que causem dano físico ao custo de um ponto de destino. Nicolas quer que Masabra seja extremamente perigoso, então ele lhe dá uma fraqueza contra armas abençoadas, permitindo a Masabra usar sua façanha sem pagar pontos de destino. Se os PJs conseguirem armas abençoadas, Masabra perderá sua façanha quando os enfrentar.
 
---

### Monstros de Múltiplas Zonas

Para monstros muito grandes (MMG), você pode tratar o monstro em si como um mapa de várias zonas. Para derrotar tal monstro, os personagens precisam derrotar cada uma das zonas independentemente, enquanto superam os obstáculos entre as zonas. Ao organizar um monstro em partes, você pode dividir os PJs e dar ao monstro um número de ações extras — uma por zona — para transmitir o tamanho do inimigo e manter o conflito interessante.

---

O Dragão Ancião de Ormulto é um MMG no jogo que Nicolas está organizando. Ele é tão grande que possui quatro zonas: suas duas garras, a cabeça e a cauda. Quando os jogadores tentam impedi-lo de destruir um prédio residencial, precisam causar dano o suficiente à sua cabeça para derrotá-lo. No entanto, se não fizerem nada com relação às patas ou à cauda, elas podem causar uma destruição imensa sobre as pessoas que os heróis estão tentando salvar. Eles terão que se dividir entre as zonas para manter o monstro sob controle.

---

Além do tamanho dos MMG, você também pode criar façanhas que ajudem a transmitir o tema e estilo do monstro. Muito disso envolve transformações, façanhas que alteram o monstro ou mudam a natureza do combate, mudanças que serão familiares para jogadores que já enfrentaram os chefes de fase em jogos eletrônicos. Assim como acontece com os monstros menores, você pode ligar essas façanhas a fraquezas se quiser ativá-las gratuitamente.

Para marcar a importância dos passos necessários para derrotar um monstro gigantesco — como destruir uma determinada parte ou fechar o portal de onde vem sua força — MMGs recebem uma façanha de transformação adicional vinculada à sua derrota parcial.

---

Já que suas garras e cauda são muito mais fracas que sua cabeça, o Dragão Ancião de Ormulto possui uma façanha ligada à destruição dessas zonas chamada Sopro de Fogo. Se os PJs destruírem uma das partes, o Dragão ativa essa façanha para causar dois de estresse físico em cada personagem no mapa, independentemente da zona, e adiciona o aspecto de situação **_(Nome) Está em Chamas!_**, onde o (Nome) é uma construção importante ou pessoa próxima ao combate.

---
 
---

Muitas das regras apresentadas aqui também podem ser usadas para adicionar características interessantes a personagens não humanos que não sejam antagonistas na história. Um companheiro animal de um jogador poderia receber uma façanha com uma fraqueza que os PdNs podem tentar descobrir, ou mapear um espírito guardião invocado pelos jogadores ao longo de várias zonas.

---

## Equipe

 
E se você quiser jogar com uma equipe de personagens recrutados para invadir as praias da Normandia no Dia D? Seria possível usar o Fate para jogar com uma equipe de Fuzileiros Espaciais? 

Sim, senhor. É possível.

--- 

Estas regras, assim como tudo no Fate , são flexíveis o suficiente para funcionar em vários contextos, abrangendo unidades militares modernas, tropas da Segunda Guerra Mundial, exércitos em um cenário de fantasia e robôs futuristas.

---

### Criando Equipes

 
Primeiramente, crie os personagens normalmente (veja o Fate Sistema Básico p. 25). Ao elaborar seus personagens, nomeie a equipe e diga que papel ela possui na força a que pertencem. Seriam um bando de ex-criminosos tornados soldados ou uma força profissional altamente treinada e equipada? 

Independente da sua decisão, transforme a descrição em dois aspectos, um conceito do equipe e uma dificuldade do equipe, que sempre será o calo no pé do seu time. Qualquer jogador que pertencer à unidade pode invocar esses aspectos, bem como forçá-los.
 
---
 
Lara, Antônio e Michele decidiram que serão uma equipe improvisada, formada por um grupo de sobreviventes lutando contra os ataques de forças alienígenas que quase destruíram Nova Orleans. Após criar os personagens individualmente, eles nomeiam a equipe como Defensores da Ala Nove, escolhendo o conceito _**Cães de Guerra**_ para representar sua agressividade e a dificuldade _**Mais Problemas Que Soluções**_ para mostrar que os alienígenas estão na frente.
 
---
 
### Perícias de Equipe

Diferente das perícias individuais, perícias de equipe podem ser usadas para afetar todo o campo de guerra. Por exemplo, seu soldado pode liderar uma investida contra as linhas inimigas ou chamar reforços para enfraque cer uma defesa implacável.

Após criar os aspectos da unidade, forneça a ela:

- Uma perícia de equipe Razoável (+2)
- Duas perícias de equipe Regulares (+1)
- Uma Façanha de Equipe

---

Se deseja enfatizar a equipe em lugar dos personagens individuais, reduza as façanhas e recarga iniciais dos personagens de três para dois e limite a maior perícia a Bom (+3) ao invés de Ótimo (+4).

---

#### Operações

A perícia Operações mede a habilidade da unidade em trabalhar em conjunto no campo de batalha, eliminando unidades inimigas e firmando-se em posições estratégicas.

**Superar:** Operações permite superar obstáculos em equipe, como quando você aplica fogo de cobertura para alcançar um soldado ferido ou trabalha em conjunto para escalar uma muralha.

**Criar Vantagem:** Ao criar vantagem com Operações, sua equipe está plantando armadilhas ( **_Emboscada!_** ) ou investindo diretamente contra barricadas inimigas ( **_Capangas em Pânico_** ).

**Atacar:** Operações permite que sua equipe lance ataques coordenados contra alvos e deve ser rolado em lugar das perícias individuais Lutar e Atirar enquanto o grupo continuar agindo como equipe.

**Defender:** Role Operações para defender quando a equipe tentar se retirar de uma zona de combate ou em outras tentativas de evitar ataques inimigos em grupo.

##### Exemplos De Façanhas Para Operações

**Difícil de Cercar:** Ganhe +2 em qualquer rolagem de superar para recuar de uma zona de combate.

**Ataque Relâmpago:** Sua equipe é rápida, leve e mortal. Receba +2 nas rolagens de Operações quando seu ataque procura pegar o inimigo desprevenido.

 
#### Equipamento
 
A perícia Equipamento representa os recursos que sua equipe tem disponíveis para alcançar seus objetivos.

**Superar:** Assim como a perícia Recursos, a perícia Equipamento pode ser usada para superar uma situação que exija algumas ferramentas adicionais. O grupo pode chamar caminhões para transportá-los através de um terreno acidentado ou mesmo convocar um bombardeio.
 
**Criar Vantagem:** Sua equipe pode usar Equipamento para conseguir uma arma muito poderosa em uma missão específica ( _**Lança-Chamas!**_) ou tentar conseguir recursos essenciais para a navegação (_**Mapas Topográficos**_).
 
 
**Atacar/Defender:** A perícia Equipamento não é usada em ataques ou defesas.
 
 
##### Exemplos De Façanhas Para Equipamentos

**Equipamento de Alta Tecnologia:** Você pode usar Equipamento em lugar de Operações em uma situação onde possuir uma tecnologia superior pode fazer a diferença.

**Estoque Completo:** Você recebe +2 em todas as rolagens de Equipamento realizadas para criar vantagem quando acessar seus suprimentos preexistentes.
 
#### Reconhecimento
 
**Superar:** Reconhecimento normalmente não é usado para superar obstáculos, mas pode ser usado de forma semelhante a Notar, para dar à equipe a chance de evitar emboscadas e armadilhas.

**Criar Vantagem:** Sua equipe pode usar Reconhecimento durante a batalha para adentrar a névoa da guerra, conseguindo informações sobre lugares além de sua posição atual.

**Atacar/Defender:** Reconhecimento não é usada para defesas ou ataques.
 
##### Exemplos De Façanhas Para Reconhecimento

**Decifradores de Códigos:** Enquanto monitora comunicações inimigas, você pode rolar Reconhecimento ao criar vantagem para descobrir ou criar um aspecto adicional (no entanto, isso não lhe concede uma invocação grátis extra).

**Contraprogramação:** você usa Reconhecimento ao invés de Operações para preparar uma armadilha que utilize o sistema de comunicação do inimigo contra eles mesmos.

### Rolando Perícias de Equipe

Para rolar uma perícia de equipe (ou uma façanha de equipe), um jogador deve abrir mão de sua ação pessoal para reunir o grupo. A natureza da ação que reunirá todos depende da situação — pelotões militares em geral seguem ordens —, mas geral o jogador precisará da ajuda da maioria dos outros jogadores.
 
Se ele for bem-sucedido em coordenar a equipe, a dificuldade da tarefa cai em 1 para cada membro que sacrificar sua próxima ação para esse fim, conforme o grupo volta sua atenção para a realização da tarefa. Além de reduzir a dificuldade, o sucesso ou falha da rolagem afeta toda a unidade, já que as perícias da equipe podem remodelar o campo de batalha e ganhar o dia — ou fazer com que o grupo sofra junto. Dessa forma, estresse infligido contra equipe como um todo é infligido em cada membro igualmente.

---

Ao invés de realizar uma rolagem para tentar romper uma barreira alienígena sozinha, Lara decide que os Defensores da Ala Nove devem derrubá-la juntos. A dificuldade para completar essa tarefa cai de Fantástica (+6) para Ótima (+4). Se bem sucedida, toda a unidade se beneficiará do rompimento da barreira inimiga sem precisar rolar uma segunda vez. Se ela falhar, toda a unidade receberá estresse do contra-ataque alienígena.

---

### Combate Entre Equipes

Para realizar um ótimo combate entre equipes, utilize rolagens de Operações para se mover diretamente para o centro da ação. Ao invés de começar no início da luta — quando os conflitos são entediantes —, permita que a equipe elabore um plano de ataque e role Operações como uma ação de superar com uma dificuldade apropriada ao alvo e veja no que dá. Se forem bem-sucedidos, os jogadores devem narrar uma resolução boa para cada tensão acima do número alvo. Se falharem, o Narrador descreverá uma resolução negativa para cada tensão abaixo do número alvo. Seja como for, pule direto para a ação e repita isso quando as coisas começarem a esfriar no meio da luta.

---

Após romper a barreira alienígena, Lara está certa de que seu personagem pode chegar perto o suficiente da rainha alienígena para matá-la antes que o restante dos alienígenas se reagrupe. Ela reúne o batalhão para investir contra as linhas inimigas, rolando sua perícia Operações Razoável (+2) contra uma dificuldade Ótima (+4) determinada pelo Narrador. Ela consegue +2 em sua rolagem, juntamente de seu Operações (+2), e invoca o aspecto Cães de Guerra para receber outros +2; um total Fantástico (+6). Para sua primeira tensão, ela narra que foram bem-sucedidos em invadir as linhas inimigas e, para o segundo, afirma que a sua personagem chegou perto o suficiente para matar a rainha.

---

## Isto Significa Guerra: Combate Em Massa

---
 
#### Você Precisará De:

- Cartões para anotações que usará como ficha de unidades e para monitorar as zonas.
- Marcadores ou miniaturas para representar as unidades e líderes.
- Amigos, dados Fate e tudo o que precisaria para jogar uma partida de Fate SiS tema BáSico.

---
 
 
Esta modificação pode ser inserida em qualquer partida de Fate ou também serve como um minijogo enquanto se espera a pizza. Ela traz ferramentas simples para jogar e resolver conflitos em grande escala. Estas regras não são compatíveis com as regras de equipes apresentadas acima, já que abrangem um grupo maior do que um punhado pessoas.
 
### Treinamento Básico

Combatentes são **unidades** em um **campo** de batalha formado por **zonas**. São três conceitos abstratos, maleáveis o bastante para se encaixar em seus jogos e conflitos específicos.

As unidades são criadas como personagens, com perícias, aspectos e consequências, mas sem caixas de estresse ou façanhas. Uma unidade pode consistir em alguns navios de guerra, uma dezena de biplanos ou mil orcs urrantes, mas todos agem como um em batalha.

Represente cada zona do campo de batalha com um cartão de anotações, ou **cartão de zona**. Represente cada unidade com um marcador ou miniatura e coloque-o no cartão de zona para indicar sua posição atual no campo de batalha.

Uma unidade pode ter um **líder** ligado a ela, na forma de um PJ, PdN de suporte ou PdN principal. Líderes tornam suas unidades mais eficientes e podem se envolver pessoalmente com outros líderes no campo de batalha.
 
### Ações das Unidades
 
Quando ativada, uma unidade pode se mover uma zona gratuitamente, contanto que não haja nenhum obstáculo na zona de destino, como uma unidade inimiga ou um terreno obstruído. Ela também pode realizar uma ação — superar, atacar ou criar vantagem, como detalhado abaixo. 

Se a unidade possuir um líder, este poderá desistir de sua ação para conceder uma _segunda_ ação à unidade. Um jogador também pode gastar pontos de destino para conceder uma segunda ação a uma unidade sem líder.

Independentemente, nenhuma unidade pode realizar a mesma ação duas vezes no mesmo turno e atacar sempre finaliza o turno do jogador.

Se você possui uma unidade com um líder, ela pode se mover uma zona e atacar, criar vantagem e atacar e assim por diante, mas não pode atacar ou criar vantagem duas vezes seguidas, ou atacar e então criar vantagem ou se mover.

#### **Criar Vantagem**

Pode ser na forma de reconhecimento de terreno, intimidar outra unidade, usar o ambiente ou qualquer coisa que faça sentido no contexto. Aqui seguem algumas formas específicas de usar esta ação para tornar a sua batalha mais dinâmica.

**Emboscada:** Uma unidade pode usar Furtividade para colocar um aspecto de situação, como **_É Uma Armadilha!_** em jogo. Isso não pode ser tentado se a unidade possui um inimigo em sua zona.

**Intimidação:** Uma unidade pode usar Provocar para colocar um aspecto de situação em jogo, como **_Tropas Enfurecidas_** , **_O Poder da Frota Imperial_** ou **_Hesitante_**.

**Supressão:** Use Atirar para pôr o aspecto de situação **_Paralisado_** em uma unidade inimiga. Uma unidade com esse aspecto não pode se mover para outra zona a não ser que seja bem-sucedida em uma rolagem de superar contra a jogada de Atirar do atacante. O aspecto desaparece se o defensor for bem-sucedido ou se o atacante não usar uma ação para manter o aspecto turno após turno.

**Patrulha:** Uma unidade pode usar Notar para pôr um novo aspecto de zona em jogo em uma zona adjacente que ainda não possua uma unidade. A dificuldade é Razoável (+2), com +2 para cada aspecto que a zona já possua. Por exemplo, se a zona já possuir o aspecto **_Mata Fechada_** , a dificuldade para impor um segundo aspecto deve ser Ótima (+4).

**_Cerco:_** Uma unidade pode colocar o aspecto Cercado em jogo se possuir mais aliados do que inimigos em uma zona. Cada unidade aliada em uma zona recebe +1 nos próprios ataques enquanto esse aspecto estiver em jogo.


####  Superar

Ao usar uma perícia para se mover para uma zona com um obstáculo ou uma unidade inimiga:

Se a zona possuir um aspecto que dificulta o movimento, como _**Floresta Densa**_ ou _**Campo de Asteroides**_, a dificuldade é igual a duas vezes o número desses aspectos. Por exemplo, a dificuldade para entrar em um _**Terreno
Pedregoso**_ com um _**Rio Agitado**_ é igual a Ótimo (+4).

Se a zona possuir uma ou mais unidades inimigas, uma delas pode se opor ativamente a essa tentativa com uma ação de defesa, normalmente usando Atletismo, Condução ou Pilotagem. Cada unidade adicional em uma zona que seja aliada ao defensor fornece +1 à rolagem do defensor. De qualquer forma, use as resoluções padrões de superar para resolver a ação. 

**Ao usar uma perícia para se mover uma ou duas zonas adicionais livres de obstáculos:**

Em caso de empate ou sucesso, mova-se uma zona (com um custo menor, em caso de empate). Em caso de sucesso com estilo, a unidade pode abrir mão do impulso para se mover uma zona adicional.

 
#### Atacar

Se a batalha usar tanto Atirar como Lutar, ataques contra inimigos na mesma zona usam Lutar e ataques contra inimigos em zonas adjacentes usam Atirar. Se sua batalha usar apenas atirar — como em combates aéreos — então todos os ataques serão feitos com Atirar, independente da distância. Atacar um inimigo a duas zonas de distância concede +2 na rolagem de
defesa do defensor.

Provocar não pode ser usada para atacar, apenas para criar vantagem. Veja _Intimidação_ na página anterior.

Dependendo do local da batalha, você pode querer ajustar o funcionamento da defesa. Por exemplo, em uma guerra medieval talvez a única defesa contra Atirar seja Vontade — você não desvia de flechas, você mantém sua posição e e procura manter sua sanidade mental também. Isso pode retirar um pouco da funcionalidade de Atletismo, mas se aplica a todas as unidades por igual, logo ninguém fica em muita desvantagem.
 
### Qualidade da Unidade

 A qualidade de uma unidade — Regular, Razoável ou Boa — determina quantas perícias, aspectos e consequências ela possui.

+ **Regular:** Recrutas. Uma perícia Regular (+1). Um aspecto. Nenhuma consequência — um único golpe a retira do conflito.
+ **Razoável:** Soldados. Uma perícia Razoável (+2), duas Regulares (+1). Dois aspectos. Uma consequência moderada.
+ **Boa:** Soldados de elite. Uma perícia Boa (+3), duas Razoáveis (+2) e três Regulares (+1). Três aspectos. Uma consequência suave e uma moderada.

---

#### PERÍCIAS DE UNIDADES

Eis uma lista de perícias, retiradas do Fate Sistema BáSico, que as unidades podem
possuir.

- Atirar
- Atletismo
- Condução/Pilotar
- Furtividade
- Lutar
- Notar
- Provocar
- Vontade

Nem todas essas perícias serão apropriadas para todas as unidades em todo tipo de
conflito, é claro. Numa batalha entre naves espaciais, Lutar e Condução não serão muito
úteis, enquanto em uma batalha subterrânea entre anões e mortos-vivos a perícia Pilotar
provavelmente não será utilizada. Use o bom senso

---

### Aspectos de Unidade

 
O primeiro aspecto de uma unidade é o seu nome, que funciona como o seu conceito: _**Esquadrão Rebelde , Granadeiros Anões , 27ª Unidade de Infantaria Pesada ,**_ etc. 

Se a unidade é de categoria Razoável ou Boa, defina os aspectos secundários como preferir.
 
### Construindo Unidades

 
Cada jogador recebe um “kit de batalha” com pontos de construção para o combate — quanto maior o número, maior o número de unidades e maior a batalha. Gaste esses pontos para criar unidades ou para comprar pontos de destino adicionais que poderão ser gastos durante o combate. Cinco pontos de construção bastam para uma batalha pequena com unidades de baixa qualidade, enquanto 20 seria consideravelmente épico. 10-12 é um bom meio termo. Pontos de construção remanescentes podem ser gastos durante a batalha, mas quaisquer pontos que sobrarem após a batalha serão perdidos, assim como quaisquer pontos de destino comprados com pontos de construção.

Escreva os detalhes de cada unidade em seu próprio cartão de anotações. Se ela for derrotada, vire o cartão — mas deixe-o guardado, para que possa usar em uma batalha futura.

---

#### CUSTO DE UNIDADES

 
+ Regular: 1 ponto de construção
+ Razoável: 2 pontos de construção
+ Bom: 3 pontos de construção
+ Ponto de Destino: 3 pontos de construção
 
---
 
### Zonas

Uma zona pode ser uma única colina, uma campina de centenas de metros ou um setor espacial. As especificações dependem do seu jogo e da escala do conflito.

#### Número de Zonas

Como regra geral, o campo de batalha deve ter um número de zonas igual ao número de jogadores mais um. Isso inclui o Narrador, então um jogo com um Narrador e três jogadores deve possuir cinco zonas. Se parecer pouco espaço para o número de unidades envolvidas, adicione mais lguns cartões.

#### Acrescentando Aspectos de Zona

Por um ponto de destino, um jogador pode escrever um aspecto em um cartão de zona vazio que tenha sido colocado no campo de batalha, mas antes que a batalha comece. Ponha o cartão de zona de volta ao campo de batalha com a face do aspecto virada para baixo. Quando uma unidade se mover para aquela zona ou fizer uma patrulha por lá, vire o cartão e revele o aspecto.

Se um jogador colocar um novo aspecto em uma zona por criar vantagem durante o jogo, escreva no cartão para que todos vejam.

#### Criando o Campo de Batalha

Cada jogador tem seu turno para colocar cartões de zona, começando com aquele que tiver mais pontos de destino restantes em seu pacote. Cada cartão de zona deve estar adjacente a outro cartão já existente. Tente evitar um campo muito linear — muitas entradas e saídas em muitas das zonas tornará a batalha mais interessante e intensa.
 
### Líderes

 
Qualquer PJ ou PdN de suporte ou principal pode ser um líder. Use uma miniatura ou outro tipo de marcador para representar cada líder — algo que possa ser colocado junto ao cartão da unidade bem como direta- mente no campo de batalha, caso sua unidade seja derrotada ou quando ele agir independentemente. Derrotar uma unidade não derrota seu líder, apenas um líder pode atacar e derrotar diretamente outro líder.

Designar um líder a uma unidade, ou _removê-lo de uma_, não exige uma ação, mas um líder não pode fazer ambos no mesmo turno. 

Um líder designado a uma unidade pode realizar a sua ação junto com sua unidade. Ele pode conceder essa ação a sua unidade, para que ela realize duas ações, ou pode fazer outra coisa, como combater outro líder ou remover uma consequência da unidade.

Um líder designado a uma unidade lhe concede alguns benefícios.
 
- Todas as perícias da unidade que sejam inferiores à Vontade do líder recebem +1 de bônus enquanto o líder estiver posicionado. Se o seu jogo possuir outra perícia mais adequada para isso, use-a.
- O líder pode invocar seus próprios aspectos em prol da unidade.
- O líder pode usar Vontade para remover uma consequência, com as    dificuldades padrão mostradas no Fate Sistema Básico. Isso conta    como a ação do líder naquele turno.
- O líder pode usar sua ação para colocar um impulso em sua unidade,    como **_Avante!_**. Isso não exige uma rolagem a não ser que a unidade    possua uma consequência, o que nesse caso exige uma rolagem de    Vontade com uma dificuldade igual a duas vezes o número de consequências que a unidade possui. 

Um líder independente precisa ser ativado para poder realizar as coisas,
assim como uma unidade.

### Sequência de Jogo

1. Escolha um de seus líderes e role sua Vontade. Os resultados mais    altos agem primeiro, seguindo a ordem decrescente. Em caso de    empate, quem possuir Vontade mais alta age primeiro. Se o empate    permanecer, então o jogador com mais unidades começa.
2. Quando for o seu turno, escolha e ative uma de suas unidades ou     líderes independentes. Se escolher uma unidade que possua um    líder, este pode realizar sua ação normalmente. Se escolher um líder    independente, ele não poderá afetar unidades, mas poderá interagir    com outros líderes (em geral de forma violenta). Cada unidade e    cada líder de um dos lados deve agir antes de qualquer unidade ou    líder do mesmo lado agir novamente.
3. Quando todos os jogadores não aliados perdem suas unidades ou    concedem, a batalha termina.

### Vitória

Todos que façam parte do lado vitorioso recebem um ponto de destino. Cada jogador que derrotou um líder inimigo — em campo de batalha, persuadindo-o a conceder ou trocar de lado; o que for — recebe um ponto de destino para cada líder que tenham derrotado.

## Duelos Aventurescos

 
Aquele tipo de ação heroica e cinematográfica típica de histórias de espadachins aventureiros está bem próximo ao coração do Fate — é a pró- pria natureza do jogo. Porém, essas lutas temáticas, mano a mano, entre nosso herói e um vilão maligno quase sempre envolve várias investidas antes que um deles consiga acertar um golpe. Nesse meio tempo, pode ser que troquem alfinetadas inteligentes e provocações ofensivas, balancem em lustres, saltem de varandas, confundam o oponente com sua capa e mil coisas mais. Veja D’Artagnan e Jussac em _Os Três Mosqueteiros_, Errol Flynn e Basil Rathbone em _As Aventuras de Robin Hood_, o duelo poético entre Cyrano e Valvet em _Cyrano de Bergerac_ ou o duelo entre
Luke Skywaker e Darth Vader em _O Império Contra-Ataca_.

A ação de criar vantagem em Fate torna mais fácil modelar esse tipo de conflito, mas a maioria dos jogadores ainda tenderão a focar nos meios mais fáceis de despachar a oposição, especialmente se não houver outros PJs por perto para dar-lhes a desculpa de criar aspectos de situação. É isso que esta modificação na regra faz — exige que os jogadores dependam de outras perícias, não apenas Lutar, durante um conflito, com resultados bem variados.

Estas regras de duelos introduzem algo chamado __vantagem__. Apenas o duelista com a vantagem pode realmente usar uma perícia com a ação de ataque para causar dano. O outro duelista pode realizar qualquer outra ação, menos atacar — a não ser que ele consiga a vantagem.

E como se consegue essa vantagem? Ao ser bem-sucedido com estilo em qualquer perícia _exceto_ a que causa dano físico em conflitos (Lutar ou Atirar, ou qualquer que seja o equivalente em seu jogo). Uma vez que um combatente seja bem-sucedido com estilo em uma dessas perícias, ele obtém a vantagem. Isso _substitui_ a resolução da ação de sucesso com estilo, tal como conseguir um impulso ou uma invocação grátis. Você pode receber a vantagem ou a resolução padrão, mas não os dois.

Use um marcador para representar a vantagem, algo que possa ser facilmente passado de lá pra cá, como uma moeda, uma pequena espada de plástico para coquetéis, um cartão com uma mão desenhada, uma luva de esgrima — o que seu grupo preferir.

No início de um conflito físico entre dois (e apenas dois) participantes, determine a ordem dos turnos normalmente. Se envolver a rolagem de uma perícia e alguém for bem-sucedido com estilo, ele começa o conflito com a vantagem — já começou partindo para cima do adversário.

Após isso, os combatentes podem fazer qualquer uma das seguintes coisas a cada turno:
 
- Atacar, se possuir a vantagem.
- Tentar conseguir obter a vantagem, se ele não a tiver.
- Fazer outra coisa — colocar aspectos de situação, tentar escapar do conflito, etc.

É altamente recomendável usar a regra opcional de jogo [Sem Estresse][] juntamente com essas regras. Caso contrário, há um risco real do combate se arrastar demais, ao invés de derrubar e arrastar o inimigo para fora de combate.

---

Dekka, uma Agente da Lei do Império em Porthos V, está enfrentando seu arqui-inimigo Xoren, um ciborgue traiçoeiro e interessado em usurpar o Trono Celestial, no meio da cerimônia de coroação. Ambos são mestres na espada de fótons, como já haviam demonstrado em disputas passadas. Suas armas de alta tecnologia brilham e zunem com sons característicos protegidos por direitos autorais. Está valendo.

Dekka vence a iniciativa com um +5 contra o +3 de Xoren — um sucesso, mas não com estilo. Ela começa com algumas palavras, na esperança de descobrir algum aspecto usando Empatia. “Qual é o seu problema, Xoren? Há algum algoritmo da maldade em algum lugar de seu sistema central? Ou você realmente acredita que esse truque vai funcionar?” Ela consegue um +6 enquanto Xorem consegue +2. Sucesso com estilo!

“Maldade?”, ele responde. “Passe um dia como um ciborgue neste império miserável e entenderá a _verdadeira_ maldade!” ele diz.

Ela descobre o aspecto _Todos Devem Sofrer Por Minha Dor!_ e escolhe ficar com a vantagem em lugar de receber a invocação grátis extra.

Agora é a vez de Xoren. Sendo um ciborgue maligno, ele agarra um dos espectadores e arremessa contra Dekka, na esperança de criar vantagem com Vigor. Ele vence Dekka por uma margem de 4 tensões — o suficiente para conseguir a vantagem — mas ela usa sua invocação grátis de _Todos Devem Sofrer Por Minha Dor!_ , o que reduz para apenas 2 tensões de diferença. O jogador explica que Xoren, em sua fúria, acidentalmente deixou claro o que pretendia fazer. O Narrador aceita. Xoren
coloca o aspecto de situação _Cidadãos em Perigo_ , com uma invocação grátis.

Dekka ainda possui a vantagem e pretende usá-la. Empurrando o pobre espectador para o lado e saltado para frente, se aproximando de Xoren com sua lâmina de fótons, ela ataca com Lutar +4, superando sua defesa de Lutar +3. Ele aproveita o fato dela estar distraída pelos _Cidadãos em Perigo_ , usando sua invocação grátis para alcançar +5. Após mais alguns pontos de destino gastos, Dekka sai na frente com apenas uma tensão de diferença. Uma vez que eles estão usando a regra variante Sem Estresse, isso significa uma consequência suave para Xoren - _Confiança Abalada_.

---

## Veículos

Em um jogo que gira em torno de atividades pessoais, veículos ocupam um lugar peculiar. Eles estendem as capacidades dos personagens como ferramentas e armas, mas são algo separado do personagem, como aliados e recursos. É algo que fácil de ser esquecido, mas que pode ser essencial, dependendo das prioridades do jogador ou da campanha. Ao descrever veículos, esta seção geralmente assume que são carros ou caminhões, mas muitas dessas ideias podem ser facilmente usadas para cavalos, carruagens, espaçonaves e assim por diante.

### Veículos Incidentais

Em muitos jogos, os veículos são apenas incidentais no jogo. Isto é, aparecem de acordo com a necessidade, mas caso contrário não recebem muita atenção. Nesse caso, veículos são frequentemente usados apenas para permitir rolagens da perícia Condução. Quando os vilões estão fugindo em um carro e você entra em um para persegui-lo, toda a situação vai depender apenas da perícia.

Se houver a necessidade de diferenciar os veículos dentro deste contexto, isso normalmente deve ser feito através de aspectos. Um veículo normalmente terá entre um e três aspectos, cujos detalhes dependerão bastante do interesse de seus jogadores em carros. Aspectos como ___Grande___, ___Rápido___, ___De Trilha___ ou ___Lata Velha___ são totalmente válidos, bem como ___Motor V8___, ___Cambagem Negativa___, ___Transmissão de Cinco Marchas___, ___Injeção Eletrônica___.

Para a maioria dos jogos isso é o bastante, mas em jogos onde dirigir é crucial há uma boa chance desses veículos acabarem se tornando mais ou menos descartáveis.

### Veículos Pessoais

Normalmente um veículo pessoal será representado por um aspecto, mas detalhes além disso dependem muito do jogo. Um jogo mais realista talvez possua apenas um veículo pessoal, como um carro esporte de detetive, mas supercarros modificados cheios de aparatos podem ser mais adequados para outros tipos de jogos.

A regra básica para veículos precisa ser um pouco mais complexa do que a regra para veículos incidentais e, para carros mais complexos, extras se tornam mais apropriados.

### Reparos

Um item representado por um aspecto normalmente não pode ser destruído, porém, apesar disso pode ser um pouco difícil de acreditar em um veículo indestrutível. Como regra geral, permita que um veículo pessoal sofra dano normalmente, mas diga que o dano foi reparado sem problemas entre as sessões. 

A não ser, claro, que o jogador queira lidar com o dano. Possuir um carro que precise de reparo é ótimo como enfoque de cena e como motivador de ação de vez em quando — talvez ele precise de peças específicas. Se um jogador opta por lidar com o carro como avariado, então a primeira vez na sessão em que ele mencionar os reparos necessários - como puxar conversa enquanto trabalha no motor — isso na verdade é forçar o aspecto, o que concede um ponto de destino ao jogador.

### Veículo de Grupo

Uma ideia frequentemente usada na ficção e em jogos eletrônicos é um veículo comunitário, como algum tipo de nave, van, carro ou algo assim, mas que serve como transporte em grupo e muitas vezes como uma base de operações móvel.

Uma forma fácil de fazer isso é tornar o veículo em questão um aspecto que todos no grupo possuam — ou ao menos todos que tenham algum vínculo com o veículo. Fazer isso torna evidente a importância do veículo no jogo.

Também é possível seguir uma abordagem mais sutil, onde cada um dos personagens possui um aspecto que reflete a sua relação com o veículo — uma capitã de uma espaçonave e seu engenheiro podem possuir perspectivas bem diferentes sobre a função de sua nave.

Seja qual for o caso, um veículo de grupo pode possuir aspectos como um veículo comum, mas é mais flexível do ponto de vista do que esses aspectos deveriam ser. Cada aspecto de jogador que seja relacionado ao veículo permite ao jogador incluir um aspecto no veículo. Isso torna a construção mais orgânica e cria níveis de investimento diferentes de acordo com o interesse de cada personagem.

### Regras Rápidas para Veículos

#### **Diferenças Entre Veículos**

Eis uma sequência hierárquica simplificada de velocidade:

- A pé
- Bicicleta/Cavalo
- Carro/Motocicleta
- Helicóptero
- Avião

Quando uma perseguição envolve uma diferença de velocidade entre veículos, o mais rápido recebe um número de invocações grátis dos aspectos do veículo igual à diferença os níveis de velocidade. A diferença pode ser atenuada pelas circunstâncias — alguém a pé ou de bicicleta pode ultrapassar um carro em um congestionamento e um carro pode ajudá-lo a alcançar um avião antes da decolagem —, mas isso deve bastar para cobrir a maioria dos casos específicos.

#### Furtando Carros

Uma tentativa de furtar ou obter um carro de outra forma deve ser tratada como uma ação de superar, com a perícia apropriada — normalmente Roubo ou Recursos — contra uma dificuldade baseada na situação, levando em conta tanto a segurança como a disponibilidade. Em caso de sucesso, os aspectos criados são os aspectos do veículo furtado. Isso assume que o personagem está pegando o que há disponível — tentar roubar um caro específico seria uma ação de superar contra os detalhes específicos dessa situação.

#### Carros Personalizados

Personalizar carros é uma aplicação da perícia Ofícios que requer uma oficina e ferramentas apropriadas. A dificuldade base na rolagem de superar é 0, com +2 para cada aspecto do veículo. Em caso de sucesso, um aspecto pode ser adicionado ao veículo, ou removido, se for aceitável. O número máximo de aspectos de que um veículo pode ter é 5.

#### Dano em Veículos

Os veículos não possuem estresse, mas podem receber consequências — normalmente ao transformar uma rolagem malsucedida de Condução em um sucesso usando a regra opcional de Esforço Extra (veja página 49). Um veículo médio (3 aspectos ou menos) pode receber uma consequência suave. Um veículo excepcional (4 ou 5 aspectos) pode receber uma consequência suave e uma moderada. Um veículo com um aspecto como ___Blindado___ ou ___Categoria Militar___ talvez possa receber até mesmo uma consequência severa.

#### Façanhas Com Veículos

___Ladrão de Carros:___ Quando for roubar um carro, use Condução em lugar de Roubo.
 
## Supers

Os personagens em Fate são pessoas incrivelmente competentes e bem-sucedidas, mas às vezes seu grupo pode querer ir mais além, contando histórias de verdadeiros super-heróis que lutam contra o crime e supervilões que desejam conquistar o mundo. Esta seção lhe dará algumas ferramentas para fazer super-heróis funcionarem bem em seu grupo!

### Histórias de Origem

Super-heróis em Fate se parecem bastante com personagens normais, mas podem substituir sua dificuldade por uma história de origem, um aspecto que diz brevemente como o personagem adquiriu seus superpoderes e/ou porque esses poderes trazem problemas. Lembre-se que aspectos devem sempre possuir dois sentidos, representando tanto os pontos fortes como as fraquezas do personagem. Por exemplo, um herói pode ter o conceito **_Supersoldado Geneticamente Modificado_** para representar sua força e agilidade sobre-humanas e sua origem poderia ser **_Viajante Dimensional_** para mostrar que ao mesmo tempo ele é um peixe fora d’água em nosso mundo.

### Superperícias

Uma forma de criar superpoderes em Fate é permitir que jogadores escolham perícias como se fossem poderes ao invés de criar façanhas. Um herói nascido em um mundo alienígena avançado pode possuir Superconhecimento. Um adolescente tocado pelos poderes de um deus primordial pode possuir um Superatletismo. Heróis que usam tecnologia ou itens mágicos podem investir suas perícias em tais itens se tornarem uma perícias extras, carregando a Lanças do Destino (Vigor) ou construindo robôs mecânicos fantásticos (Atirar).

Heróis podem usar a perícia normalmente, mas também podem optar por usar o poder ao custo de um ponto de destino. Quando ativada, a superperícia permite ao herói fazer proezas fantásticas, dobrando seu bônus de perícia atual. Após rolar, o personagem deve reduzir o valor da perícia em um. Desde que a perícia não seja reduzida a zero, o valor é recuperado na próxima cena. Se o poder esgotar, no entanto, o herói terá de encontrar uma forma de descansar ou recarregar antes de usá-lo novamente.

---

Maxine, uma Valquíria com Vigor Excepcional (+5), pode gastar um ponto de destino para jogar um carro em um supervilão — ganhando 5 tensões adicionais em sua rolagem —, mas após lançar os dados a perícia reduz para Ótimo (+4). Se ela continuar usando sua superforça até que seu Vigor se esgote, ela precisará fazer uma jornada a Valhalla para beber do Chifre do Poder e recuperar suas forças.

---
 
Este sistema lembra os heróis da Era de Ouro, que conseguiam fazer proezas incríveis com poucas desvantagens além de ficarem cansados ou sem energia. Isso também dá ao Narrador uma chance de criar cenas de recarga interessantes (veja Recarga ), onde os heróis precisam recarregar seu poder, reconectar com suas vidas humanas ou descansar o suficiente para encarar o vilão com força total. Por exemplo, uma heroína adolescente pode precisar voltar para casa para se conectar com seu pai, enquanto um predador mutante noturno pode se retirar para seu covil para dormir durante o dia antes de retornar à caça.
 
### Criando Façanhas Superpoderosas
 
Se o seu grupo prefere poderes mais definidos, permita que jogadores comprem poderes em forma de façanhas, talvez até mesmo dando-lhes uma ou duas façanhas extras grátis. Para um jogo com baixo nível de poder, emulando os anti-heróis da década de 1980, as façanhas existentes no Fate Sistema Básico provavelmente serão suficientes — os heróis não possuem poderes incríveis, são apenas pessoas extraordinárias que vestem fantasias.

Se seu interesse é contar histórias mais épicas, você também pode criar façanhas que permitam habilidades poderosas, mas que em contrapartida exijam a aposta de pontos de destino. Obstáculos estáticos podem ser vencidos com o gasto de apenas um ponto de destino, mas outros personagens superpoderosos podem aceitar, cancelar ou aumentar a ação ao gastar seus próprios pontos de destino e narrando sua oposição. Isso continua até que um dos lados não esteja mais disposto a gastar pontos de destino. Se a ação terminar em um empate, os pontos são dados ao Narrador, mas se um personagem claramente vence, o perdedor recebe todos os pontos de destino. Note que isso pode resultar em reviravoltas dramáticas no sistema de pontos de destino, já que um jogador receberá uma mão cheia de pontos de destino ao fim de um conflito.
 
---
 
Sérgio é um herói com a façanha Entortar Barras, Erguer Portões, que permite que ele destrua obstáculos físicos ao apostar um ponto de destino. Se nenhum personagem superpoderoso se opuser, a ação é bem-sucedida sem a necessidade de uma rolagem. No entanto, se Maxine usar seus poderes nórdicos para eletrocutá-lo através das barras de metal que ele está tentando torcer, ela pode gastar um ponto de destino para cancelar a ação dele, ou gastar dois pontos para tentar aplicar o seu poder (Tempestade de Raios) enquanto cancela a façanha dele. Se ela gastar dois pontos de destino, Sérgio terá a chance de gastar novamente para cancelar ou aumentar, ou ele poderia aceitar a ação dela e receber os dois pontos de destino que ela investiu.
 
---
 
### Vilões e Capangas

O Fate já possui regras ótimas para a [criação de PdNs][]. Para histórias de super-heróis, o Narrador pode denominá-los lacaios, vilões e supervilões para dar uma noção melhor do perigo que cada um deles representa.

Preste atenção nas sugestões no Fate Sistema Básico para grupos de lacaios e tratá-los como obstáculos. Às vezes a melhor forma de mostrar um herói derrotando um grupo de capangas de baixo nível é em uma única rolagem ou com o uso de um superpoder.

Os vilões devem ser mais ou menos como os heróis, provavelmente possuindo um ou dois superpoderes. Você provavelmente vai querer um grupo de vilões que possa enfrentar de igual para igual os heróis por tempo o suficiente para tornar a disputa interessante, mas provavelmente não serão a verdadeira ameaça que os heróis estão combatendo.

Supervilões, no entanto, podem ser personagens completos, possuindo seu próprio conceito, origem, superpoderes e extras próprios. Não poupe esforços na história de origem do supervilão; é ela que transmite o “sabor” especial do personagem aos jogadores. Um cultista maligno pode ter sua origem como **_Perdi o Único Homem a Quem Pude Amar_** enquanto um supersoldado demoníaco pode possuir a origem **_Condenado por Forças Infernais a Vagar Pela Terra_**.

Heróis também podem possuir seus próprios ajudantes (veja Ajudantes Aliados) à medida que a história avança, comprando-os como extras assim de alcançarem algum marco.

---

O cenário de campanha **_Wild Blue_** , presente no **_Fate Worlds_** , traz algumas ideias incríveis para a criação de poderes como façanhas com custos narrativos, como viagem no tempo sem controle total, ou telepatia que sempre descobre informações indesejadas. Leia o livro para descobrir ainda mais uma forma de criar poderes em Fate! 

De forma similar, superpoderes podem ser chamados de _magia_. Confira o capítulo anterior para conhecer algumas ideias sobre como fazer algo mais envolvente e complexo com os seus poderes, construindo-o como um sistema de magia. Muitas outras maneiras para elaborar poderes estão disponíveis nos cenários criados exclusivamente para a edição brasileira. Procure por essas indicações!

---
 
## O Paradoxo do Horror

 
A princípio, o Fate não serve tão bem para jogos de horror. Está bem ali, na introdução do Fate Sistema Básico “... funciona melhor em qualquer tema que os personagens sejam proativos, capazes e possuam vidas dramáticas”. O horror faz o seu trabalho sombrio ao colocar os personagens em circunstâncias mortais e inescapáveis além de seu controle. No horror, os personagens são frequentemente forçados a reagir, em lugar de agir. Apesar de suas capacidades, os personagens enfrentam ameaças que são superiores a eles; sua competência não é o bastante para vencer. A derrota parece ser inevitável e o sucesso vem a um alto custo. Ao invés de focar na vitória, os personagens focam mais em sobreviver os próximos minutos e escapar vivos.

Como conciliar essas duas realidades? Como pegar um sistema criado para personagens proativos e capazes e transformá-lo num sistema que sirva às exigências dodo horror?

Esse é o paradoxo do horror em Fate. Eis a nossa solução.
 
### Os Elementos do Horror
 
No fundo, o que importa no horror é a resposta emocional visceral que os jogadores têm durante o jogo. O sistema é completamente capaz de fazer isso, se tiver sua ajuda. Para os propósitos do Ferramentas do Sistema, horror é uma combinação de uma atmosfera opressiva, circunstâncias impossíveis e desespero absoluto.

#### Atmosfera Opressiva

__Force à Vontade:__ Mesmo que forçar um aspecto não seja uma ferramenta para _forçar resultados_, ainda assim é uma ótima ferramenta para _fazer coisas darem errado_. Logo, faça isso abundantemente. Coloque aspecto nas cenas, na história, na campanha — force-os para fazer com que tudo dê errado para todos. Simplesmente por colocar ___A Morte Vem Para Todos___ em uma história e forçar ele na pior hora (para os jogadores) tornará as coisas muito piores, trazendo muita tensão. Sim, os jogadores afetados sairão com alguns pontos de destino — que precisarão para sobreviver —, mas também sentirão a pressão do momento — além de se perguntarem quando mais aspectos serão forçados. Machuque-os. Deixe-os preocupados

__Todo Caminho é Obscuro:__ Aspectos não são a única forma de evocar uma atmosfera. Isso também pode ser feito com obstáculos e zonas. Nunca é fácil fugir do horror, então o caminho para a salvação deve estar repleto de empecilhos (veja mais em obstáculos, abaixo). Além disso, quando estiver desenhando os mapas de zonas, deixe-os mais claustrofóbicos que o normal: mais zonas que cubram menos espaço. Embora as distâncias físicas não sejam muito diferentes, é simplesmente mais difícil fugir de lugares perigosos em jogos de horror — em uma casa normal, a porta da frente pode estar a apenas uma zona de distância. Em uma casa assombrada? Tente mais ou menos cinco ou dez... e _nunca_ deixe-os correr sem interrupções.
 
#### Circunstâncias Impossíveis

O fato de os personagens serem capazes não significa que as coisas devem ser fáceis para eles.

Qualquer obstáculo cuja dificuldade esteja dois níveis acima da perícia usada para superá-lo é capaz de requerer uma invocação de aspecto. Com as melhores perícias dos jogadores girando em torno de Bom ou Ótimo, isso significa que os níveis iniciais de dificuldade devem começar na mesma faixa e ultrapassá-la em seguida. Não pegue leve nisso. Pegue pesado mesmo!

Dificuldades altas trazem chances maiores de falha. Ao invés de permitir que isso paralise os jogadores e deixe-os apáticos, faça bastante uso do “sucesso a um custo” como uma alternativa — e torne os custos realmente pesados. O preço deve ser desconfortável. Ao pagarem o que é devido, os jogadores sentirão uma fisgada do horror. Cada passo em frente derrama um pouco mais de sangue. É como morrer aos poucos. 

Em lutas contra inimigos impossivelmente difíceis, os jogadores estarão mais inclinados a conceder, para tentar ganhar algum controle da situação. Não há razão para não aceitar isso, mas você deve sempre preferir concessões que realmente machucam, de verdade. É aqui que pagarão o preço e sentirão ainda mais a mordida do horror.

Tudo isso conduz a uma sensação de desespero.

Depois disso...

#### **Desespero Absoluto**

O desespero surge das escolhas difíceis feitas sob pressão e dos recursos, que são escassos e estão em risco constante. Os recursos limitados de um PJ incluem pontos de destino, estresse e consequências. Limite-os sem torná-los ausentes. Considere:

+ **Recarga baixa:** Se os jogadores possuírem apenas alguns pontos de destino, esses pontos serão preciosos. Eles precisarão gastá-los se quiserem ter sucesso. A tensão nessas escolhas aumenta a ansiedade.
+ ** Estresse mínimo:** Não forneça mais do que uma caixa ou duas, se é que vai fornecer alguma. No horror, personagens não devem possuir muita resistência antes de começar a apanhar e sangrar. Além disso, se você usar caixas de estresse mental ou de sanidade, você estará a apenas alguns passos dos traumas duradouros, do terror e da loucura.
+ **Consequências fracas:** Considere fazer com que as consequências absorvam menos danos. O padrão é -2/-4/-6 para suave, moderada e severa, o que é bem resistente. Para o horror, considere cortar esses valores à metade ou tente algo como -1/-2/-4. Não será necessário muito mais do que um golpe para que os personagens realmente se machuquem, tornando qualquer conflito ainda mais perigoso.

Alinhe seu modelo de sistema com as escolhas difíceis que os personagens precisam realizar sob pressão. Isso eliminará vitórias fáceis e sem consequências. Afinal de contas, os PJs por si só são recursos limitados: não podem estar em dois lugares ao mesmo tempo e a pressão aparece quando o tempo é curto. O tempo é o recurso limitado que você como Narrador pode controlar. No horror, nunca deve haver tempo o bastante.

Portanto, dê aos jogadores muita coisa para fazer, mas não dê tempo o bastante para fazê-las. As pessoas e coisas com as quais os personagens se preocupam também são recursos limitados — e para o Narrador, muitas vezes são coisas mais fáceis de ameaçar do que o próprio personagem. Salve seu marido ou seu filho: mas não ambos — você é apenas um e há duas bombas em extremos opostos da casa.

Seus jogadores o odiarão por isso e, se passaram por tudo isso porque queriam horror, então o amarão por isso também, entre os gritos de pavor.
