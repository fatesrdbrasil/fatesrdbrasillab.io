---
title: Kung Fu
layout: default
---

## Kung Fu

---

O termo “kung fu” é bem abrangente nessa sessão, mas esta é apenas uma das várias artes marciais do mundo. As regras aqui podem se aplicar ao savate ou ninjutsu facilmente, ou a qualquer arte marcial louca que seja praticada em Marte.

---

O _Kung Fu_, como vemos na vida real, é coberto pela perícia Lutar. Falaremos aqui sobre o _Kung Fu_ cinematográfico, com grandes pulos, acrobacias e o estilo melodramático dos filmes de artes marciais de Hong Kong.

Esse tipo de _kung fu_ é tão dramático que não se encaixa em jogos mais realistas, e com certeza altera o tom até mesmo uma ação aventuresca do tipo _pulp_. Pode ser um pouco demais. Se pretende incluir este estilo de arte marcial louco e exagerado em seu jogo, deixe claro para seus jogadores que isso é algo que todos os personagens podem fazer – tanto PCs _quanto_ PdNs.

Em um filme de _kung fu_, os praticantes da arte estão um nível acima dos praticantes normais. Se o seu jogo se concentrará fortemente em mestres de _kung fu_, então faz sentido que a maioria — se não todos — dos personagens dos jogadores possuam essa habilidade. Se apenas um personagem pratica _kung fu_, certifique-se de que os outros personagens possuam sua própria forma de se destacar. Além disso, você pode querer suavizar algumas loucuras descritas nesta seção. Você não quer apenas um personagem dominando a ação de uma cena ao sair saltando por todos os lugares. Se todos têm a habilidade, então o risco de alguém monopolizar a atenção é menor. O _kung fu_ cinematográfico é muito poderoso. Se você o introduz, é provável que domine todo o seu cenário.

Os mestres do _kung fu_ podem correr pelas paredes, saltar grandes distâncias, parar projéteis em pleno ar ou apará-los com uma arma, lutar em condições precárias de apoio e até mesmo andar sobre a água. Praticamente qualquer pessoa que tenha treinamento nas artes marciais pode fazer isso. Se um personagem possui treinamento em kung fu , então essas ações são tratadas como uma ação normal de correr ou saltar. Um jogador pode descrever seu personagem realizando essas ações sem nenhuma dificuldade especial. O Narrador pode exigir rolagens, mas esse tipo de ação não é algo excepcionalmente difícil ou fora do comum. Basta adicionar esses detalhes para que seu jogo tenha a mesma sensação de um filme de _kung fu_.

Há duas maneiras de abordar o _kung fu_ em Fate. Um método é usar perícias existentes para cobrir as habilidades e luta com a arte marcial. A segunda é criar uma perícia _Kung Fu_ dedicada a isso. A decisão de qual método adotar depende da natureza do seu jogo. Se há apenas um mestre _kung fu_ em jogo, então a perícia _Kung Fu_ pode ser apropriada. Se todos possuem treinamento na arte, então talvez faça mais sentido que todos usem perícias já existentes para realizar as façanhas do _kung fu_. Se for por esse caminho, um praticante do kung fu deve possuir um aspecto relacionado ao seu treinamento, para poder disponibilizar as habilidades da arte ligadas às perícias existentes — algo como ___Treinado no Monastério Wudang___ ou ___Mestre no Estilo Louva-a-Deus___.

### Usando Perícias Existentes

Se preferir usar o método das perícias existentes, então Lutar obviamente ficará com a maior parte do trabalho. Qualquer ataque, desarmado ou com armas, depende da perícia Lutar, além de também poder ser usada em defesas. Como um mestre de _kung fu_ também pode aparar flechas e outras armas de projétil, esse feito também seria realizado com a perícia Lutar. Se estiver jogando em um cenário moderno ou se houver armas de fogo no universo de jogo, será necessário tomar uma decisão sobre o uso do _kung fu_ contra elas. Não parece fora do gênero sugerir que um praticante de _kung fu_ possa desviar ou mesmo aparar balas, especialmente se ele usa uma espada ou outra arma de metal.

Andar em paredes e saltar estão incluídas em Atletismo. Qualquer tipo de façanha física além de lutar será coberta por Atletismo ou possivelmente Vigor. Os praticantes de _kung fu_ tradicionalmente possuem treinamento para resistir a dano ou sobreviver a quedas, que são ações da perícia Vigor. A perícia Conhecimentos pode ser usada para identificar escolas rivais ou técnicas dos oponentes. Uma rolagem de Conhecimentos pode criar uma vantagem, se você deduzir corretamente quem é o professor ou escola de luta de seu oponente.

### A Perícia _Kung Fu_


Se adicionar uma perícia _Kung Fu_ ao jogo, qualquer habilidade que não se enquadrarem no que é considerado normal para Lutar ou Atletismo, como saltar e andar pelas paredes, exige uma rolagem de _Kung Fu_. Isso pode ser bastante útil se você desejar destacar as ações de _Kung Fu_. Os praticantes de _kung fu_ normalmente possuem movimentos especiais ou segredos conhecidos apenas por eles e seus mestres. Em um jogo com muitos artistas marciais, esses movimentos especiais são uma boa forma de diferenciar os personagens e criar a sensação de que há vários estilos diferentes. Movimentos especiais podem ser modelados usando façanhas e aspectos.

Apesar da maior parte dos movimentos no _kung fu_ provavelmente possuir nomes, um mestre de _kung fu_ pode possuir um ou dois movimentos característicos que representam seu estilo pessoal. Esses podem ser representado por aspectos, como o **_Toque da Morte_** ou **_Estilo da Lâmina Escondida_**. Quando invocado, o jogador pode descrever em detalhes a natureza incrível ou mística daquela ação. Esses aspectos devem fazer com que o personagem pareça incrível e você pode usá-los para mostrar que PdNs conhecem o seu personagem e seu estilo, como “Não se aproxime, ela é mestre do Estilo da Lâmina Escondida”. Eles podem ser forçados se um oponente conhecer um movimento que sirva de contra-ataque, assim como o jogador pode usar seu próprio conhecimento de _kung fu_ para analisar o estilo de um oponente e deduzir como contra-atacar.

Para uma versão mais mecanicamente completa, use o sistema de façanhas para criar movimentos. Você pode criar toda uma escola de _kung fu_ usando façanhas, com façanhas mais fáceis como pré-requisitos para acessar as mais avançadas. Use a descrição de suas façanhas para criar um estilo de _kung fu_ marcante, com os nomes e efeitos dos movimentos retratando o tema do seu estilo de luta.

#### Punho Bêbado

Abaixo segue um exemplo de uma árvore de façanhas de _kung fu_ para o estilo do Punho Bêbado.

**O Vacilo do Bêbado:** Você balança e vacila nos passos, evitando os golpes do adversário aparentemente por acaso. Quando bem-sucedido numa rolagem de Atletismo para se defender usando esta técnica, você recebe +1 no seu próximo ataque contra o oponente que tentou acertá-lo. Se for bem-sucedido com estilo, recebe +2.

**O Empurrão do Bêbado:** Seu empurrão rude e bruto contém mais poder do que parece possível. Você recebe +2 ao usar Vigor para criar vantagem contra um oponente ao tentar tirar-lhe o equilíbrio.

**Beber do Jarro:** Você faz uma pausa momentânea para tomar um gole de vinho de seu jarro, fortalecendo-se para a batalha. Quando você toma um gole durante uma luta, limpe sua menor caixa de estresse. Isso exige gastar uma ação inteira bebendo.

**A Queda do Bêbado (Exige o Vacilo do Bêbado):** Quando um inimigo o ataca você perde o equilíbrio e cai, rapidamente voltando a ficar de pé, mas seu inimigo agora se encontra perigosamente exposto. Role Atletismo para esquivar. Em caso de sucesso, coloque um impulso em seu adversário como Guarda Baixa ou então Desequilibrado que pode ser usado por qualquer um contra ele. Em um sucesso com estilo, coloque um segundo impulso em seu oponente. 

**A Dança do Bêbado (Exige o Empurrão do Bêbado):** Seus golpes são brutos e óbvios, mas, ao esquivar-se, seu adversário parece ser atingido por um cotovelo ou joelho acidentalmente. Faça uma rolagem de Lutar normal. Se atingir o seu oponente, você causa estresse normalmente. Se você errar ou empatar, seu oponente recebe um de estresse físico de qualquer forma.

**Derramar Vinho (Exige Beber do Jarro):** Você pega um copo e despeja uma dose do jarro. Essa tarefa difícil e elaborada causa uma pausa na batalha. Ninguém pode atacá-lo enquanto estiver derramando e você remove sua caixa de estresse mais baixa. Essa técnica exige uma ação completa e não pode ser realizada mais de uma vez em seguida.

**O Bêbado Tropeça (Exige A Queda do Bêbado):** Você cambaleia e tropeça sem controle, mas seu inimigo sempre o erra e acaba acertando algum obstáculo próximo, causando dano a si mesmo. Quando você se desvia de um golpe usando Atletismo, seu oponente recebe um de estresse ou dois se você for bem-sucedido com estilo.

**A Firmeza do Bêbado (Exige a Dança do Bêbado):** Você cambaleia e parece prestes a cair, então se aproxima e agarra o braço de seu adversário para firmar-se. Esse ato aparentemente desproposital bloqueia o _qi_ e paralisa o seu oponente. Você pode colocar um aspecto de situação como ___Qi Bloqueado___ em seu oponente com uma invocação grátis. Seu adversário não pode usar nenhuma manobra de _kung fu_ enquanto não remover o aspecto.

------

- [« Subsistemas](../subsistemas/)
- [Cibernética »](../cibernetica/)
