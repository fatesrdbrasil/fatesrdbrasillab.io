# Guia do Veterano

Esta é uma nova versão do *Fate* que desenvolvemos para atualizar o sistema. Este
é um guia geral das maiores mudanças no sistema, com base nas versões anteriores
presentes em *Espírito do Século*, e *The Dresden Files Roleplaying Game*.

## Criação do Jogo e de Personagem

- A criação do jogo é uma variação da criação de cidade em *Dresden Files RPG*, mas bem simplificada. No mínimo, você cria dois aspectos chamados de "questões" para definir o clima do jogo, com a opção de se aprofundar se desejar criar aspectos para indivíduos e lugares.
- Há menos aspectos nesta edição do que em outros jogos *Fate*. Reduzimos as fases de criação para apenas três—uma aventura significativa e duas participações. Achamos que seria mais fácil conseguir cinco bons aspectos ao invés de sete ou dez. E como há aspectos de jogo e aspectos de situação, você possui muitas opções para invocar e forçar!
- Se seu jogo tiver muitos extras ou elementos específicos que você gostaria que os personagens descrevessem com aspectos (como nacionalidade ou raça), é possível aumentar o número de espaços para aspectos. Não recomendamos ir além de sete aspectos — percebemos que, passando disso, muitos aspectos não aparecem com tanta frequência em jogo.
- Se você jogou *The Dresden Files RPG*, notará que usamos uma pirâmide de perícias no lugar das colunas. Nesta versão do *Fate*, gostaríamos que a criação dos personagens fosse mais rápida e acessível, então colocamos uma pirâmide a partir de Ótimo (+4). Se prefere as colunas, vá em frente — você possui 20 pontos de perícias. Não descartamos completamente a coluna de perícias; ela foi apenas reservada para a evolução do personagem ([p. XX]()).
- 3 pontos de recarga e 3 façanhas grátis. As caixas de estresse funcionam exata-
mente como em *The Dresden Files RPG*.

## Aspectos

- Em outros jogos *Fate*, invocações grátis eram chamadas de *"tagging”* (algo como "marcar"). Achamos esse termo um pouco técnico demais. Você pode continuar chamando assim, se desejar — qualquer coisa que ajude você e sua mesa a entender as regras.
- Você pode ter visto o ato de jogadores forçarem aspectos ser chamado de “invocar por efeito”. Achamos que ficaria mais claro chamar apenas de forçar, não importa quem esteja realizando.
- Invocações grátis agora podem ser combinadas com uma invocação normal. Além disso, um aspecto pode receber mais de uma invocação grátis por vez.
- Invocar um aspecto que esteja ligado a outro personagem concede um ponto de destino àquele personagem ao final da cena.
- Forçar foi dividido em dois tipos específicos: decisões e eventos. Isso não muda a forma como forçar funciona e serve apenas para diferenciação, mas é importante mencionar.
- Aspectos de cena foram renomeados para Aspectos de situação, para evitar certas confusões sobre sua flexibilidade e aplicação.

## Ações e Outras Coisas

- A lista de ações foi bastante reduzida, caindo para apenas quatro: superar, criar vantagem, atacar e defender. Movimento agora é uma função de ação de superar, criar vantagem é um resumo das ações avaliar/declarar/manobrar em versões antigas, e os bloqueios podem funcionar de diversas formas.
- O jogo não é mais baseado em sucesso/falha. Agora há quatro resoluções: falha ou sucesso a um grande custo, empate (sucesso a um custo menor), sucesso e sucesso com estilo. Cada resolução agora possui um efeito mecânico ou narrativo, baseado na ação relacionada. Sucesso com estilo é praticamente uma evolução das versões anteriores do *Fate*, que aplicamos a tudo.
- Desafios e disputas foram simplificadas e redesenhadas.
- As Zonas foram substituídas pelo uso de aspectos de situação para determinar se é preciso uma rolagem para se mover. Mover-se uma zona com uma ação é grátis se não houver obstáculos no caminho.
- Ações suplementares e modificadores de perícias foram completamente removidos do sistema. Ou algo é interessante o suficiente para um rolagem, ou não.
- Trabalho em equipe foi bastante simplificado com relação às outras edições—qualquer um que possua ao menos um nível Razoável (+1) na mesma perícia acrescenta +1 à pessoa com o maior nível da perícia.

## Criação de Cenário

- Os conselhos estão bem melhores.

## Extras

- Há extras. Cada versão anterior do *Fate* possuía sua própria forma de lidar com poderes e acessórios, mas agora há várias opções para você escolher (o que combina com estilo "caixa de ferramentas" do sistema).

