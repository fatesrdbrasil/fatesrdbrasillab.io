---
title: Prefácio - ”Um” E Não “O”
layout: default
---

# Prefácio - ”Um” E Não “O”

Este livro é um conjunto de algumas Ferramentas do Sistema Fate, não As Ferramentas do Sistema Fate.

O que isso significa?

Ferramentas do Sistema foi inicialmente criado como uma das metas do financiamento coletivo do Fate Sistema Básico, realizado pela Evil Hat através do Kickstarter. A ideia original era conter apenas o sistema de magia do livro, mas à medida que o financiamento foi crescendo, o livro também cresceu. Seguindo-o de perto vinham inúmeros pedidos do que deveria ser acrescentado ao livro mas que ultrapassavam completamente o que era possível.

Foi uma grande honra saber que o público nos consideravam capazes de desenvolver tantos tópicos diferentes. Impossível, talvez, mas uma honra. Dito isso, sabíamos que precisávamos atender todos esses pedidos em espírito, mesmo se não pudéssemos atendê-los de forma plena. Assim, expandindo para além de nosso suplemento sobre magia, nasceu o Ferramentas
do Sistema.

Fazendo jus ao nome, nossas Ferramentas não oferecem tudo que você quer de imediato, mas sim um conjunto de ideias, abordagens e outras questões interessantes que podem lhe levar aonde desejar. Colocando de outra maneira, quando nos pediram a lua, decidimos ensinar várias formas de construir foguetes. (Nós construímos foguetes agora. Foguetes são demais.)

Quando estiver procurando a medida certa para seu jogo, não se atenha às aparências. Digamos que algo aqui fale sobre tratar gigantes como se fossem mapas ou um grupo de vários personagens ao invés de um único personagem ”aumentado”. Embora a embalagem diga “aqui está o método para lidar com monstros gigantes”, isso é apenas a forma como a ideia foi apresentada. Isso também lhe traz um método para lidar com os “chefes de fase” em geral ou uma operação de ataque contra um satélite orbital da morte. Da mesma forma, maneiras de abordar super-heróis são abordadas em uma variedade de conceitos e sessões dentro do livro, mas apenas uma é rotulada como “super-heróis”.

Isso não quer dizer que todas as ferramentas possíveis estão presentes aqui, mas há ideias e bases sólidas suficientes para que você certamente encontre algo que o guiará. Além disso, se tiver alguma ideia nova, por favor não a guarde para si. Há muitos lugares onde conversas legais e excitantes estão acontecendo neste momento e você poderia participar com suas ideias, independente se for em redes sociais, grupos dedicados ao Fate Sistema Básico ou qualquer lugar dentro ou fora da internet. O público do Fate forma uma comunidade forte, cheia de pessoas inteligentes, um grupo inteiro de indivíduos que são ainda mais espertos do que as pessoas que criaram o sistema em primeiro lugar.

Acima de tudo, Ferramentas do Sistema é um guia de treinamento para o seu cérebro. Neste livro você encontrará dezenas de formas de adicionar algo novo, algo a mais, algo diferente à sua partida de Fate. Em alguns casos será exatamente o que você precisava. Na maioria das vezes, será um ponto de partida para outras ideias e ajustes. Mãos à obra. Você possui uma coleção incrível de ferramentas para trabalhar. Agora vejamos o que é capaz de fazer com elas!


> Fred Hicks
> 15 de julho de 2013

------

- [Introdução »](../introducao/)
