---
title: Capítulo 3 - Perícias
layout: default
---

# Capítulo 3 - Perícias

## Altere a Lista!

As perícias no Fate Sistema Básico foram desenvolvidas para oferecer uma ampla variedade de ações, facilmente aplicadas a diversos cenários diferentes. Dito isto, perícias são a primeira coisa que esperamos que você altere. Sua lista de perícias deve ser compatível com o cenário, reforçando o tema; se a lista padrão de perícias já fizer isso, você tem sorte! Em alguns casos, elas podem funcionar _bem o suficiente_ , mas pode ser preciso adicionar uma perícia ou duas, retirar outra ou renomear algumas. Se estiver precisando realizar modificações mais profundas na lista de perícias — ou
mesmo no próprio sistema — então este capítulo foi feito para você.


------

- [« Condições](../condicoes/)
- [Usando como foram escritas »](../usando-como-foram-escritas/)
