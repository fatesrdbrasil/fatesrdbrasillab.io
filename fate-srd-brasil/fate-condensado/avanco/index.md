---
title: Fate Condensado - Avanço
layout: default
---
# Avanço

À medida que seus personagens avançam no enredo, eles crescem e mudam. No final de cada sessão, você ganhará um ***marco***, que permite modificar os elementos da ficha do seu personagem. Conforme você conclui cada arco de história, você ganha um ***progresso*** que permite adicionar novos elementos à sua ficha de personagem. (Saiba mais sobre sessões e arcos [aqui](#sessões-e-arcos))

## Marcos

Os Marcos acontecem ao fim de uma sessão, parte do caminho ao lidar com um arco de história. Eles são focados em ajustar seu personagem lateralmente ao invés de avançá-lo. Você pode não querer usar um marco, está tudo bem. Nem sempre faz sentido mudar seu personagem. A oportunidade está aí, caso precise.

Com um marco você pode fazer uma das opções abaixo:

  - Trocar os níveis entre duas perícias quaisquer ou substituir uma    perícia de nível Regular (+1) por uma que tenha nível Medíocre    (+0).
  - Reescrever uma façanha.
  - Comprar uma nova façanha ao custo de 1 recarga. (Lembre-se: você não    pode ficar com recarga abaixo de 1).
  - Reescrever qualquer um dos seus aspectos, exceto seu conceito.

## Progressos

Progressos são mais significativos, permitindo que seu personagem realmente cresça em poder. Um progresso permite que você escolha uma opção da lista de marcos e, além disso, você pode escolher todos os itens a seguir:

  - Reescrever o conceito do seu personagem, caso queira.
  - Se você tiver quaisquer consequências moderadas ou severas que ainda     não estejam em recuperação, você pode começar o processo de     recuperação e renomeá-las. As consequências que já estavam em     processo de recuperação podem ser removidas.
  - Aumentar uma perícia em um nível, mesmo de Medíocre (+0) para     Regular(+1).

Se o narrador sentir que uma grande parte da trama foi concluída e é o momentos dos personagens se “empoderarem”, ele pode também oferecer uma ou ambas das opções a seguir:

  - Ganhar um ponto de recarga que pode ser gasto imediatamente para    adquirir uma nova façanha, se desejar.
  - Aumentar uma segunda perícia em um nível.

### Aprimorando os níveis das perícias.

Ao aprimorar o nível de uma perícia, você deve manter uma estrutura em “coluna”. Cada andar não pode ter mais perícias que o andar abaixo. Isso significa que você precisará melhorar algumas perícias Medíocres (+0) primeiro ou você pode guardar alguns pontos ao invés de gastá-los imediatamente, permitindo vários aprimoramentos de uma só vez.

> Raquel quer aprimorar seu nível de Saberes de Regular (+1) para Razoável (+2), mas isso significa que ela teria quatro perícias no Razoável (+2) e apenas três no Regular (+1)... assim não funciona. Felizmente, ela guardou um segundo ponto de perícia de um progresso anterior, então ela também aumenta sua Empatia de Medíocre (+0) para Regular (+1). Agora ela tem: um Ótimo (+4), dois Bons (+3), quatro Razoáveis (+2) e quatro Regulares (+1).
>
>
> | Nível | A Pirâmide           | Inválido             | Válido               | Também Válido       |
> |:-----:|----------------------|----------------------|----------------------|---------------------|
> | +4    | `0`{: .fate_font}    | `0`{: .fate_font}    | `0`{: .fate_font}    | `0`{: .fate_font}   |
> | +3    | `00`{: .fate_font}   | `00`{: .fate_font}   | `00`{: .fate_font}   | `000`{: .fate_font} |
> | +2    | `000`{: .fate_font}  | `0000`{: .fate_font} | `0000`{: .fate_font} | `000`{: .fate_font} |
> | +1    | `0000`{: .fate_font} | `000`{: .fate_font}  | `0000`{: .fate_font} | `000`{: .fate_font} |
	
## Sessões e Arcos

Existem algumas suposições em jogo aqui, quando falamos sobre sessões e arcos. Gostaríamos de esclarecer essas suposições para que você possa fazer ajustes baseado em como seu jogo se difere delas.

Uma ***sessão*** é uma única sessão de jogo composta por várias cenas e algumas horas de jogo. Pense nisso como um único episódio de uma série de TV. Provavelmente durará em torno de 3 a 4 horas.

Um ***arco*** é uma série de sessões que geralmente contém elementos da trama levados de uma sessão para outra. Esses elementos da trama não precisam ser concluídos dentro de um arco, mas geralmente há desenvolvimentos e mudanças significativas que ocorrem ao longo dele. Pense nisso como um terço ou meia temporada de uma série de TV. Provavelmente é composto por cerca de quatro sessões de jogo.

Se seu jogo pender para fora desses “prováveis” intervalos, você pode querer mudar a forma como algumas partes do avanço funcionam. Se seus arcos duram por mais de quatro a seis sessões de jogo, você pode permitir que consequências Severas sejam recuperadas depois de 4 sessões se passem em vez de esperar até o fim do arco. Se você quiser que o avanço aconteça mais lentamente, permita melhorias como pontos de perícia e ganho de recarga com menos frequência. Se seu grupo tende a agendar sessões relativamente curtas, você pode não atingir um marco no final de todas as sessões. Tempere a gosto, molde o jogo à sua maneira\!


------

- [« Desafios, Disputas e Conflitos](../desafios-disputas-conflitos/)
- [ Sendo o Narrador »](../sendo-narrador/)
