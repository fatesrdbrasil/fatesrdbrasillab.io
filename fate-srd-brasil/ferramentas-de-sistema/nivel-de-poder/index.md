---
title: Nível de Poder
layout: default
---

## Nível de Poder

O Fate Sistema Básico oferece um nível de poder padrão que emula um tipo específico de ação. É heroico e voltado para a ação, mas também é um pouco difícil, com heróis frágeis o suficiente para se ferir gravemente com algumas pancadas, mas resistentes o suficiente para se manter vivos por um tempo. Ele também cria um certo nível de competência através dos níveis de perícias e pontos de destino recebidos.

Entretanto, esse nível de poder não é adequado para todo tipo de jogo. Às vezes a ideia é emular aventuras cheias de ação exagerada ou então um noir realista, e as regras padrões não se encaixam muito bem. Bem, sem problemas – bastam alguns ajustes!

### Perícias


No Fate Sistema Básico, os personagens possuem dez perícias distribuídas em uma pirâmide de níveis que vão de Regular (+1) a Ótimo (+4). Isso emula indivíduos altamente competentes com algumas poucas coisas em que são muito bons e uma maior variedade de coisas nas quais são bons. Há duas coisas a serem ajustadas quando falamos em perícias: o número de perícias e o nível limite.

Ao aumentar o número de perícias disponíveis, você torna os personagens competentes em uma maior variedade de coisas. Se aumentar o nível limite das perícias, você permite que os PJs sejam muito bons em algumas poucas coisas, possibilitando personagens em níveis sobre-humanos. Reduzir essas coisas tende a um cenário mais realista; poucas perícias significa que os heróis se limitarão mais a seus nichos de atuação, mas aumenta a chance do grupo não possuir uma perícia importante. Reduzir o nível limite torna os PJs menos competentes, o que é bom se você está tentando emular um grupo de pessoas comuns.

A maioria dos jogos aumenta ou reduz ambos; ajustar um sem ajustar o outro pode resultar em excesso de perícias ou de ninguém atingir os níveis mais altos de capacidade.

### Recarga

Ajustar a recarga para mais ou menos tem impacto na competência e versatilidade dos PJs. Mais importante, afeta a frequência em que os PJs aceitam forçar seus aspectos. Um alto nível de recarga significa que os PJs recebem mais pontos de destino por sessão, o que significa que recusarão aspectos forçados com mais frequência. Baixo nível de recarga significa que forçar os aspectos é mais importante, mas também significa que os PJs estão à mercê do Narrador.

Recarga também afeta quantas façanhas extras cada jogador pode comprar, o que tem um impacto nas especializações, proteção de estilos distintos, além da competência.

### Façanhas

Não deve ser uma surpresa que ceder mais façanhas gratuitas torna os PJs mais poderosos enquanto reduzi-las os torna menos poderosos. Isso também tem impacto na recarga — menos façanhas gratuitas significa que jogadores precisarão gastar sua preciosa recarga para comprar façanhas, e vice-versa.

Uma segunda forma de afetar o nível de poder através de façanhas sem alterar a quantidade é ajustando o nível de poder da façanha em si. Em Fate Sistema Básico, uma façanha vale cerca de duas tensões. Ajustar para 3 ou 4 tensões significa que cada façanha possui um impacto individual maior, enquanto ajustar para menos deixa-as menos importantes.

### Estresse

O Fate Sistema Básico define estresse com duas barras para cada tipo, com a possibilidade de aumentar esse número de acordo com certas perícias. Você pode aumentar essa quantidade, tornando os personagens mais resistentes, capazes de tomar algumas pancadas, ou diminuí-las para deixá-los mais frágeis. Esta mudança tem um impacto direto em quanto tempo durarão os combates. Mais estresse significa que os PJs podem aguentar mais lesões, o que torna as lutas menos arriscadas e provavelmente mais comuns. Baixar o estresse significa que as lutas serão mais perigosas, o que significa que os jogadores pensarão duas vezes antes de iniciar um combate. Também significa que eles precisam considerar conceder com mais frequência para evitar serem retirados de combate.

### Aspectos

Ajustar o número de aspecto para mais ou para menos de cinco não afeta tanto o nível de poder como outras características, mas causa um impacto na versatilidade do personagem. Mais aspectos significa que há mais truques disponíveis para os jogadores na hora do desespero, enquanto reduzir o número de aspectos diminui suas saídas. No entanto, tenha cuidado ao ajustar essa quantidade. Quanto mais aspectos houver, menos importante cada um será, e isso faz com que sejam esquecidos ou ignorados. Reduzir o número de aspectos significa que cada aspecto do indivíduo se torna mais importante e menos negligenciado, mas também significa que você terá que equivaler essa ausência com aspectos de situação. Em geral, mais do que sete aspectos pode tornar o personagem sobrecarregado, enquanto menos de três pode deixá-lo com pouca profundidade.

Também deve-se considerar ajustar a recarga junto com o número de aspectos. Se possuir vários aspectos e pouca recarga, a maior parte deles não será invocada.

### Exemplos

Eis alguns exemplos de níveis de poder que você pode usar em seus jogos.


| Noir Realista                        | Aventura Pulp                        | Super-Heróis                                                |
|--------------------------------------|--------------------------------------|-------------------------------------------------------------|
| Oito perícias:                       | Quinze perícias:                     | Dezoito perícias:                                           |
|                                      |                                      |                                                             |
| 3 em Regular (+1),                   | 5 em Regular (+1),                   | 5 em Regular (+1),                                          |
| 3 em Razoável (+2),                  | 4 em Razoável (+2),                  | 4 em Razoável (+2),                                         |
| 2 em Bom (+3)                        | 3 em Bom (+3),                       | 3 em Bom (+3)                                               |
|                                      | 2 em Ótimo (+4),                     | 3 em Ótimo (+4)                                             |
|                                      | 1 em Excepcional (+5)                | 2 em Excepcional (+5)                                       |
|                                      |                                      | 1 em Fantástico (+6)                                        |
| (equivalente a 15 pontos de perícia) | (equivalente a 35 pontos de perícia) | (equivalente a 50 pontos de perícia)                        |
|                                      |                                      |                                                             |
| Limite de Perícia: Bom (+3           | Limite de Perícia:  Excepcional (+5) | Limite de Perícia:  Fantástico (+6)                         |
|                                      |                                      |                                                             |
| Recarga 2                            | Recarga 4                            | Recarga 6                                                   |
| 1 façanha grátis                     | 5 façanhas grátis                    | 5 façanhas grátis                                           |
| 2 caixas de estresse iniciais        | 4 caixas de estresse iniciais        | 4 caixas de estresse iniciais; façanhas podem fornecer mais |
| 5 aspectos                           | 7 aspectos                           | 5 aspectos                                                  |


------

- [« Eventos de Aspectos](../eventos-de-aspectos/)
- [Circunstâncias Especiais »](../circunstancias-especiais/)
