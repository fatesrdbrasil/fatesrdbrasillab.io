I"�<h1 id="teamwork">Teamwork</h1>

<p>Characters can help each other out on actions. There are two versions of
helping in Fate—combining skills, for when you are all putting the same kind
of effort into an action (like using Physique together to push over a
crumbling wall), and stacking advantages, for when the group is setting a
single person up to do well (like causing multiple distractions so one person
can use Stealth to get into a fortress).</p>

<p>When you combine skills, figure out who has the highest skill level among the
participants. Each other participant who has at least an Average (+1) in the
same skill adds a +1 to the highest person’s skill level, and then only the
lead character rolls. So if you have three helpers and you’re the highest, you
roll your skill level with a +3 bonus.</p>

<p>If you fail a roll to combine skills, all of the participants share in the
potential costs—whatever complication affects one character affects all of
them, or everyone has to take consequences. Alternatively, you can impose a
cost that affects all the characters the same.</p>

<blockquote>
  <p>Continuing with our temple chase example, because it’s group vs. group,
everyone decides it’d be easier to just combine skills.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Of the three PCs, Cynere has the highest Athletics, at Great (+4). Landon
has Good (+3) Athletics and Zird has Average (+1) Athletics, so they each
contribute +1. Cynere rolls the contest on behalf of the PCs at Fantastic
(+6).</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Amanda’s temple guardians only have Average (+1) Athletics, but there are
five of them, so they roll Superb (+5) for the purposes of the contest.</p>
</blockquote>

<p>When you stack advantages, each person takes a create an advantage action as
usual, and gives whatever free invocations they get to a single character.
Remember that multiple free invocations from the same aspect can stack.</p>

<blockquote>
  <p>Zird and Cynere want to set Landon up for an extremely big hit on Tremendor,
the much-feared giant of the Northern Wastes.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Both Cynere and Zird roll to create an advantage on their turns, resulting
in three free invocations on a <span class="aspect">Flashy Distraction</span> they
make from Zird’s magical fireworks (which succeeded to create the advantage)
and Cynere’s glancing hits (which succeeded with style to add two more free
invocations).</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>They pass those to Landon, and on his turn, he uses them all for a gigantic
+6 to his attack.</p>
</blockquote>

<h3 id="revising-teamwork-in-fate">Revising Teamwork in Fate</h3>

<p>“Some people are critical of the teamwork rules in Fate; depending on the day
of the week, I’m one of them”, says Ryan Macklin, one of the authors of Fate.
In his article <a href="http://ryanmacklin.com/2015/08
/revising-teamwork-in-fate/">Revising Teamwork in Fate</a> he offers up some seasoned thinking on Teamwork
Rules.</p>

<ul>
  <li><a href="/fate-srd/fate-core/conflicts">« Conflicts</a></li>
  <li><a href="/fate-srd/fate-core/running-game">Running the Game »</a></li>
</ul>

:ET