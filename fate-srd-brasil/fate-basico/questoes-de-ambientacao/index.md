---
title: Questões De Ambientação
layout: default
---

## Questões De Ambientação

**Decida quais ameaças e urgências inerentes ao cenário estimularão os protagonistas a partirem para a ação.**

Todo cenário precisa ter algo acontecendo que motive os personagens, muitas vezes um perigo que precisem combater ou enfraquecer. Essas são as questões de um dado cenário.

O grupo pode criar umas duas questões e anotá-las em cartões ou em uma ficha de criação de jogo. Essas questões da ambientação são aspectos que poderão ser invocados ou forçados durante todo o jogo.

As questões devem reﬂetir a escala do jogo e o que os jogadores enfrentarão. Devem ser ideias abrangentes, pois não afetam apenas os personagens, mas muitas pessoas naquele mundo. Há dois tipos de questões:

-  **Questões Presentes**: são problemas ou ameaças que já existem no mundo, possivelmente há bastante tempo. Os protagonistas assumem a responsabilidade de encarar esses fatos na tentativa de mudar o mundo. Exemplo: um regime corrupto, crime organizado, miséria e doenças ou uma longa guerra.

-  **Questões Iminentes**: são questões que começaram a dar as caras e ameaçam deixar o mundo pior se acontecerem ou atingirem certo objetivo. Protagonistas neste tipo de ambiente estão tentando proteger o mundo do caos e da destruição. Exemplo: invasão por um país vizinho, o despertar de uma horda de zumbis, a imposição da lei marcial.

O número padrão de questões em uma partida de *Fate* é de duas: ou duas questões presentes (para uma história direcionada unicamente em tornar o mundo um lugar melhor), ou duas questões iminentes (para uma história sobre salvar pessoas ameaçadas), ou uma de cada. Essa última opção é muito comum na ficção: pense nos heróis corajosos que lutam contra alguma ameaça iminente enquanto não estão satisfeitos com o mundo ao seu redor.

------

Tanto jogar como criar personagens em Fate envolve a criação de aspectos. Se você é novo em Fate leia a sessão [Aspectos e Pontos de Destino](../aspectos-e-pontos-de-destino)

------

------

### Alterando o Número de Questões

É claro que não é preciso se limitar ao número padrão de duas questões – uma, duas ou três funcionam bem, mas isso pode mudar um pouco a estrutura da partida. Um jogo com apenas um tema vai girar apenas em torno daquela questão – a busca para livrar a cidade do mal ou para evitar que algo ruim aconteça. Já um com três mostra um mundo agitado, onde os personagens terão que lidar com vários fatores. Se achar que o seu jogo não precisa de um foco, converse com o grupo e comece com o número de características que se encaixe melhor na ideia.

------

> O grupo reunido pensa em quais tipos de problemas eles querem enfrentar durante o jogo. Léo diz “império perigoso”, e o grupo vai lapidando a ideia. Eles bolam a ideia do “Império Culinário de Mareesias”, uma ordem militar de alta etiqueta que cozinha e come aqueles que os atrapalham. Isso claramente é um problema.
>
> Maira quer que a história também tenha a tensão do perigo sempre à espreita, algo inesperado. Surge então a ideia do “Clã do Dedo”, um clã de assassinos amigáveis que acreditam em evolução e odeiam raças sem polegar. Amanda sugere que o sinal deles seja o polegar erguido, e todos acham divertida a ideia de que esses cultistas devem viver pedindo carona por aí.
>
> Michel e Amanda pensam em mais um antagonista, algo mais místico para contrastar que permita um pouco de horror Lovecraftiano e conﬂitos secretos. Léo grita “A Bruxaria Retornará!” e Michel gosta bastante da ideia, porque seu personagem estudioso ganha mais profundidade no jogo.

------

### Questões:

1. ***Império de Mareesias***
2. ***A Bruxaria Retornará***

------

### Transformando Questões em Aspectos

Como falamos anteriormente, questões na verdade são aspectos. Transforme suas ideias em aspectos que você possa usar em vários momentos da história (muitas vezes forçando-os contra os protagonistas ou invocando-os contra inimigos, mas os jogadores também poderão encontrar usos interessantes para eles). Anote-os, e se precisar escrever algo a mais para lembrar-se do contexto ou algum detalhe, anote-os juntos aos aspectos.

> Amanda escreve “Clã do Dedo” e “A Bruxaria Retornará!” como dois aspectos de jogo. Sob o Clã, anota “fazem extorsões e outras coisas desagradáveis”. E abaixo de A Bruxaria Retornará, “comandado pelos Bruxos de Moondor”, pois acha divertida a ideia de feiticeiros espaciais.
>

Se criar aspectos é novidade para você, concentre-se apenas nisso por enquanto. Você praticará
bastante ao escrever aspectos para seus personagens.
Depois que criar os personagens, comece a transformar essas questões em aspectos.

------

+ ***O Clã do Dedo***

Fazem extorsões e outras coisas desagradáveis.

------

------

+ ***A Bruxaria Retornará***

Comandada pelos Bruxos de Moondor.

------

------

### Alterando As Questões Em Jogo

No capítulo [*Criando Campanhas*](../criando-campanhas/) falaremos mais sobre isso em detalhe, mas as questões podem ser alteradas durante jogo pois às vezes uma questão pode se transformar em algo novo, outras vezes os personagens serão bem-sucedidos em combatê-las e ainda novas questões podem aparecer. As questões criadas agora são apenas um ponto de partida.

------

### Indo Além

Você também pode usar as questões para coisas mais simples, mas que ainda assim sejam partes importantes de sua ambientação. Um local importante (uma grande cidade ou nação ou mesmo um local menor, mas memorável) ou organização (uma ordem da cavalaria, a corte do rei ou uma corporação) também podem ter questões iminentes e/ou presentes.

Recomendamos começar com apenas uma questão para cada elemento do cenário, apenas para não sobrecarregar, mas você pode ir adicionando mais à medida que a campanha avança. Da mesma forma, não é preciso fazer isso já – se ao longo do jogo parecer que algum elemento merece atenção especial, você pode atribuir questões a ele.

> Os Bruxos de Moondor continuam a aparecer nas discussões que precedem o jogo, então o grupo decide que eles precisam entrar nas questões do cenário. Após algum debate o grupo decide que seria interessante se houvesse alguma algum pré-requisito desconhecido para que os vilões consigam atuar no universo e criam uma questão chamada “Problemas de Alinhamento” – alguns planetas precisam estar alinhados de determinada forma e com cultistas instalados em cada um.

------

### Bruxos de Moondor

Questão: ***Problemas de Alinhamento***

------

- [« A Grandeza do Jogo](../grandeza-do-jogo/)
- [Rostos e Lugares »](../rostos-e-lugares/)
