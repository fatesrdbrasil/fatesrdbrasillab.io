---
title: A Regra de Bronze
layout: default
---

## A Regra de Bronze

Há mais uma pergunta a ser feita para si mesmo antes de criar uma regra para algo — será que eu posso representar isso com a [Regra de Bronze](../../fate-basico/extras/#a-regra-de-bronze-ou-fate-fractal)? 

No Fate, você pode tratar praticamente qualquer coisa como um personagem. Sua arma? Claro. A tempestade que se aproxima? Com certeza. A cena em si? Porque não? Qualquer coisa pode receber aspectos, perícias, façanhas e caixas de estresse, e eis
o que torna essa técnica ainda mais incrível — não precisam ser as mesmas coisas que os Pjs recebem. Não faz sentido que
a tempestade tenha as perícias Lutar e Atletismo, mas que tal Frieza e Nevasca? Sua arma não precisa de caixas de estresse
físico e mental, mas que tal caixas de estresse de munição? As cenas também já estão repletas de aspectos!

Se puder representar algo novo como um personagem, será ainda mais fácil do que criar uma regra completamente nova. Nem tudo funciona bem dessa forma e há coisas que você pode não querer representar dessa maneira, mas é uma ferramenta
poderosa que pode ser aplicada a uma grande variedade de situações.

Há uma outra forma de abordar esta técnica – novas regras para personagens podem ser representadas usando características já existentes de personagens. Você pode representar a magia através de perícias, poderes usando aspectos ou façanhas
e ser corrompido por sedutoras forças ancestrais usando estresse.

------

- [« Regras vs Deliberações](../regras-vs-deliberacoes/)
- [Aspectos »](../aspectos/)
