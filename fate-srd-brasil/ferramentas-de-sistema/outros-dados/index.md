---
title: Outros Dados
layout: default
---

## Outros Dados

Ainda não possui os seus dados ou baralho Fate? Abaixo seguem algmas alternativas para fazer seu jogo acontecer.

### D6-D6


Pegue dois dados de seis lados de cores diferentes. Determine um deles como o dado positivo e outro como o negativo, e então, faça a rolagem. Subtraia o dado negativo do positivo para ter um resultado entre -5 e +5. Resultados duplicados sempre são zero. Essa opção cria resultados mais oscilantes do que os quatro dados Fate, e o intervalo de resultados é mais amplo, mas é similar o bastante para funcionar.

### Ligue os Pontos


Esta técnica foi vista pela primeira em [“Baby’s First Fudge Dice”](/assets/downloads/BabysFirstFudgeDice.pdf) por Jonathan Walton. Você pode pesquisar no Google para encontrar uma explicação completa da ideia (em inglês) — procure o link que direciona para a página sinisterforces.com.
Com um marcador permanente e alguns dados de seis lados (com pontinhos, não números), você pode fazer seus próprios dados Fate ligando os pontos. Os lados com 2 e 3 serão o `-`{: .fate_font}, 4 e 6 serão `0`{: .fate_font} e 5 e 1 serão o `+`{: .fate_font} — O 1 precisa ser desenhado à mão livre. Tcha-ram — dados Fate artesanais!

![Como desenhar os dados de pontinhos](/assets/downloads/babys_first_fudge_dice.jpg)

### 3d6

Se você não tiver problemas com tabelas (ou se tiver uma boa memória), pode também rolar três dados de seis lados e consultar esta tabela.

| 3  | 4  | 5-6 | 7-8 | 9-12 | 13-14 | 15-16 | 17 | 18 |
|----|----|-----|-----|------|-------|-------|----|----|
| -4 | -3 | -2  | -1  | 0    | +1    | +2    | +3 | +4 |

### 4d6

Esta opção não dá trabalho algum, mas talvez não seja tão intuitiva: role quatro dados de seis lados e considere 1 e 2 como `-`{: .fate_font}, 3 e 4 como `0`{: .fate_font} e 5 e 6 como `+`{: .fate_font}.

------

- [« como O Cenário: Grandes Mudanças](../modificando-cenario-grande-mudancas/)
- [Escala »](../escala/)
