---
title: Criando Personagens
layout: default
---

## Criando Personagens

**Cada jogador cria um protagonista.**

Você pode ir para a criação de personagens assim que finalizar a criação do cenário ou pode fazer no meio do processo – siga seus instintos. Se em um determinado momento você perceber que estão falando mais sobre os personagens que sobre o mundo, crie os personagens e volte à parte da criação do jogo onde havia parado. Caso contrário, siga adiante e termine toda a criação do mundo primeiro.

Vale mencionar que os protagonistas deveriam possuir algumas ligações com as faces e lugares que criou passo anterior. Se isso for difícil, então é interessante repensar os protagonistas ou revisar o mundo de jogo na tentativa de enquadrar melhor os novos personagens.

Quando estiver criando os personagens você também descobrirá mais sobre o mundo conforme as coisas que os personagens fazem e as pessoas que conhecem são reveladas. Se qualquer ideia interessante surgir, então ela deve ser adicionada às notas de criação antes do início do jogo.

------

### As Perícias e o Cenário

Grande parte do cenário é o que os personagens podem fazer nele. As várias perícias em [*Perícias e Façanhas*](../pericias-e-facanhas/) cobrem muitas situações, então é bom dar uma olhada para checar se alguma delas não se aplica ao que deseja ou se existe alguma que deveria ser acrescentada.

O capítulo [*Extras*](../extras/) explica melhor como criar novas perícias.

------

- [« Rostos e Lugares](../rostos-e-lugares/)
- [Criação de Personagens »](../criacao-de-personagens/)
