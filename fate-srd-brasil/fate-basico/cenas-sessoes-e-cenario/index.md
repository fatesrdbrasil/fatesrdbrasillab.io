---
title: Cenas, Sessões e Cenário
layout: default
---

# 9 - Cenas, Sessões e Cenário

## Certo, e Agora?

Você e seu grupo já criaram os personagens, estabeleceram coisas sobre o mundo e conversaram sobre o jogo que estão prestes a iniciar. Agora você tem uma pilha de aspectos e PdNs transbordando potencial dramático e esperando ganhar vida.

O que fazer com tudo isso?

Agora é hora de entrar na parte mais divertida do jogo: criar e jogar cenários.

- [« Criando a Oposição](../criando-a-oposicao/)
- [O que é um Cenário? »](../o-que-e-um-cenario/)

