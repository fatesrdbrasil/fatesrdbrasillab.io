---
title: Organizando a Primeira Cena
layout: default
---

## Organizando a Primeira Cena

Comece a cena sendo o menos sutil possível – pegue uma das perguntas narrativas, faça algo que coloque a questão em jogo e acerte seus jogadores com ela o mais forte possível. Você não precisa responder nada logo de cara (embora não haja nada de errado em fazer isso), mas você deveria mostrar aos jogadores que a pergunta precisa de resposta.

Dessa forma, você estará definindo o ritmo do jogo, garantindo que os jogadores não andarão em círculos. Lembre-se, eles deveriam ser pessoas proativas e competentes – forneça tudo que for necessário para que ajam dessa forma desde o primeiro instante.

Se você está em uma campanha em andamento, talvez precise das primeiras cenas para resolver as pontas soltas deixadas na sessão anterior. Tudo bem gastar algum tempo nisso, pois ajuda a manter o senso de continuidade que existe de uma sessão para outra. No entanto, logo que houver uma pausa no andamento, atinja-os com a cena inicial de forma rápida e impactante.

> **Uma Conspiração Galáctica: Cena de Abertura**
> 
> Amanda pensa sobre as questões de seu cenário e começa a elaborar a cena inicial. Algumas coisas óbvias vêm à sua mente:
> 
> -  Fräkeline recebe o contrato e os detalhes sobre o trabalho de um empregador misterioso, solicitando mais um acompanhante, e deve decidir se quer ou não assinar.
> 
> -  Bandu deve decidir se confia na informação e partir para saber mais sobre seu passado.
> 
> Amanda decide iniciar com a primeira cena, pois pensa que se Fräk rejeitar o contrato e descobrir que Bandu vai já ia para lá, isso pode criar uma cena divertida na qual ela tenta pedir uma segunda chance ao empregador misterioso. Mesmo que Fräk se mantenha firme em sua decisão, isso determinará se terão ou não que lidar algum problema no caminho para lá, enquanto os lacaios do empregador misterioso os perturbam no caminho.
> 
> Isso não significa que ela vai deixar a cena de Bandu de lado – só vai guardá-la para depois da primeira cena.

------

### Truque do Narrador Ninja Para o Início da Sessão

Conversar com os jogadores para contribuírem com algo para a primeira cena é uma ótima forma de fazê-los investir no que pode vir a acontecer com seus personagens. Se houver pontos ﬂexíveis sobre a cena de abertura, peça aos jogadores para preencherem as lacunas que ainda existirem. Jogadores inteligentes podem tentar usar isso como uma oportunidade para forçar aspectos e conseguir pontos de destino extra – é o estilo de jogo que chamamos de 'incrível'!

> Vamos dar uma olhada nas cenas acima. A trama não especifica onde estão os PJs quando são confrontados com suas escolhas. Então Amanda pode começar a sessão perguntando a Michel “Onde Bandu está exatamente quando a moça o aborda?”
> 
> Agora, mesmo que Michel responda apenas “em uma feira de rua” ou “um bar”, você já solicitou sua participação, que ajudou a definir a cena. Mas Michel é um cara incrível, então ele diz “Ah, provavelmente em um banho público, relaxando depois de um dia de investigação”.
> 
> “Perfeito!” diz Amanda, e pega um ponto de destino “Ela lhe conta a respeito da relíquia, e diz que ouviu boatos que guardas imperiais sabem que você está ali, e como o ***Império quer sua cabeça*** é melhor você fugir.”
> 
> Michel sorri e pega o ponto de destino “Sim, faz sentido".

Claro você também pode considerar seus ganchos de cenário como “pré-forçados”, e distribuir alguns pontos de destino no começo da sessão para indicar aos PJs algum problema com o qual terão de lidar imediatamente. Isso ajuda jogadores com recarga menor e pode alavancar o gasto de pontos de destino logo de cara. Apenas tenha a certeza de que eu grupo concorda em ser posto nessa situação – alguns jogadores não se sentem confortáveis com a perda de controle.

> Amanda quer começar dando alguns pontos de destino aos jogadores.
> 
> “Bandu, já é ruim quando o Império quer te pegar, mas quando você é acuado pela guarda local, sob a pressão, você avista uma nave de caravana para o Museu a alguns metros de você. Decidido a escapar, você corre para dentro da fila e desaparece dentro da nave (2 pontos de destino por ***O Império Quer Minha Cabeça*** e ***Funciono Sob Pressão***).”
> 
> “Esopo, sei que você pensa que ***Bater Sempre Funciona***, então como você vai explicar o que aconteceu quando você tentou consertar o sistema de navegação? (1 ponto de destino por Bater).”
> 
> “Fräkeline, quem fez esse contrato lhe conhece bem. Ele incluiu um chip de créditos rastreável no pacote. O problema é que você pode ser encontrada e presa se não assinar o contrato – e ninguém vai acreditar se você disser que o ganhou (2 pontos de destino por ***Famosa Ladra Robótica*** e ***Se É Valioso, Poderia Ser Meu***).

------

- [« Estabelecendo a Oposição](../estabelecendo-a-oposicao/)
- [O que é uma Cena? »](../o-que-e-uma-cena/)

