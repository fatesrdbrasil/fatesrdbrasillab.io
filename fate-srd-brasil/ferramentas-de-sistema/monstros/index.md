---
title: Monstros
layout: default
---


 
## Monstros

As regras para PdNs no Fate Sistema Básico são ótimas para criar pessoas — outros humanos cujos objetivos entram em conflitos com os dos PJs —, mas como Narrador você também pode querer incluir alguns monstros em seu jogo. Com estas ferramentas, você pode criar monstros que são inumanos e complexos, desafiando os jogadores a encontrar abordagens inteligentes quando lidam com este tipo de antagonista.

### Aspecto de Instinto

Para criar monstros interessantes, comece delineando algumas motivações básicas que levam o monstro a agir. O que o leva a correr riscos? Com o que eles se importam o suficiente para entrar em conflito com os jogadores? É provável que possuam motivações inumanas — desejos que humanos comuns provavelmente nunca teriam.

Pegue sua ideia inicial e concentre-a num **aspecto de instinto**. Personagens monstros podem usar o seu aspecto de instinto normalmente, mas recebem +3 em lugar de +2 ao invocá-lo. Monstros muitas vezes possuem motivações únicas para alcançar seus objetivos e os jogadores precisarão de esforço para superar esses oponentes.

---

Nicolas está organizando um jogo de horror urbano. Ao elaborar um grupo de zumbis, eles recebem o aspecto de instinto _**Famintos por Cérebros**_. A qualquer momento em que ele invocar esse aspecto, eles recebem +3 na rolagem.

---
 
### Habilidade dos Monstros

 
Monstros são diferentes dos outros PdNs porque suas habilidades tendem a desafiar as regras e perturbar o fluxo normal dos conflitos. Muitos monstros são cenas de combate inteiras esperando para acontecer, conforme os jogadores tentam descobrir como derrotar um inimigo que muda as regras do jogo. 

Alguns exemplos de habilidades interessantes para monstros são:

+ **Duro de Matar:** Monstros podem sobreviver a muito mais danos que outros personagens, seja por causa de mais caixas de estresse — como o monstro de Frankenstein — ou porque podem se regenerar rapidamente — como a Hidra.
+ **Imune a Dano:** Monstros frequentemente são imunes a todo dano, exceto um tipo — como vulnerabilidade a prata — ou até que uma certa condição específica seja alcançada — como destruir um item mágico específico.
+ **Tendência a Mudanças:** Monstros tendem a se transformar — como vampiros que se transformam em morcegos para fugir — ou transformar o ambiente — como invocar mais capangas menores no meio de uma luta.

Embora seja fácil de ver como essas características poderiam ser transformadas em façanhas, são demasiadamente poderosas para serem ativadas sem o gasto de pontos de destino. No entanto, se você adicionar tal custo, os jogadores podem forçar uma vitória contra a Hidra apenas por esperar que acabem seus pontos de destino. Além de criar uma fraqueza para o seu monstro, tais custos tornam os conflitos entediantes. Quem é que quer jogar até que o Narrador fique sem pontos de destino?

Ao invés de adicionar um custo, você pode adicionar uma **fraqueza** ao monstro para que ele seja capaz de ativar a façanha sem gastar pontos de destino. Se adicionar uma **fraqueza menor**, você ainda tem que pagar um ponto de destino no início da cena em que o monstro usa o poder, mas se adicionar uma **fraqueza maior**, precisará pagar pontos de destino para usar a façanha.

Quando os PJs descobrem e usam uma fraqueza menor, o monstro ainda poderá usar a façanha, mas agora ele deve pagar para cada vez que usar a façanha. Se os PJs descobrirem e usarem uma fraqueza maior, no entanto, o monstro perde a façanha completamente.

---
 
Nicolas decide criar um demônio chamado Masabra. Ele inclui uma façanha que permite que Masabra seja imune a ataques que causem dano físico ao custo de um ponto de destino. Nicolas quer que Masabra seja extremamente perigoso, então ele lhe dá uma fraqueza contra armas abençoadas, permitindo a Masabra usar sua façanha sem pagar pontos de destino. Se os PJs conseguirem armas abençoadas, Masabra perderá sua façanha quando os enfrentar.
 
---

### Monstros de Múltiplas Zonas

Para monstros muito grandes (MMG), você pode tratar o monstro em si como um mapa de várias zonas. Para derrotar tal monstro, os personagens precisam derrotar cada uma das zonas independentemente, enquanto superam os obstáculos entre as zonas. Ao organizar um monstro em partes, você pode dividir os PJs e dar ao monstro um número de ações extras — uma por zona — para transmitir o tamanho do inimigo e manter o conflito interessante.

---

O Dragão Ancião de Ormulto é um MMG no jogo que Nicolas está organizando. Ele é tão grande que possui quatro zonas: suas duas garras, a cabeça e a cauda. Quando os jogadores tentam impedi-lo de destruir um prédio residencial, precisam causar dano o suficiente à sua cabeça para derrotá-lo. No entanto, se não fizerem nada com relação às patas ou à cauda, elas podem causar uma destruição imensa sobre as pessoas que os heróis estão tentando salvar. Eles terão que se dividir entre as zonas para manter o monstro sob controle.

---

Além do tamanho dos MMG, você também pode criar façanhas que ajudem a transmitir o tema e estilo do monstro. Muito disso envolve transformações, façanhas que alteram o monstro ou mudam a natureza do combate, mudanças que serão familiares para jogadores que já enfrentaram os chefes de fase em jogos eletrônicos. Assim como acontece com os monstros menores, você pode ligar essas façanhas a fraquezas se quiser ativá-las gratuitamente.

Para marcar a importância dos passos necessários para derrotar um monstro gigantesco — como destruir uma determinada parte ou fechar o portal de onde vem sua força — MMGs recebem uma façanha de transformação adicional vinculada à sua derrota parcial.

---

Já que suas garras e cauda são muito mais fracas que sua cabeça, o Dragão Ancião de Ormulto possui uma façanha ligada à destruição dessas zonas chamada Sopro de Fogo. Se os PJs destruírem uma das partes, o Dragão ativa essa façanha para causar dois de estresse físico em cada personagem no mapa, independentemente da zona, e adiciona o aspecto de situação **_(Nome) Está em Chamas!_**, onde o (Nome) é uma construção importante ou pessoa próxima ao combate.

---
 
---

Muitas das regras apresentadas aqui também podem ser usadas para adicionar características interessantes a personagens não humanos que não sejam antagonistas na história. Um companheiro animal de um jogador poderia receber uma façanha com uma fraqueza que os PdNs podem tentar descobrir, ou mapear um espírito guardião invocado pelos jogadores ao longo de várias zonas.

---

------

- [« Dispositivos e Ferramentas](../dispositivos-e-ferramentas/)
- [Equipe »](../equipes/)
