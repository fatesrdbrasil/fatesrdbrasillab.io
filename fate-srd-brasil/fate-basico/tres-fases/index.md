---
title: As Três Fases
layout: default
---

## As Três Fases

**Descreva a primeira aventura de seu personagem. Descreva também como conheceu outros dois personagens. Crie um aspecto para cada uma dessas três experiências.**

**Importante:** antes de começar essa etapa é preciso já ter definido o conceito, a dificuldade e o nome do personagem.

Os três aspectos restantes do seu personagem são criados em **fases**. A primeira fase é sobre seu histórico atual: algo que tenha feito e que seja aventuresco. A segunda e terceira fase são sobre como outros PJs estão envolvidos nessa aventura e como você se envolve com eles.

Esta é uma ótima oportunidade para contar a história de seus personagens. Em cada fase, você irá escrever duas coisas. Use a ficha de criação de personagem (no fim desse livro ou em [*SolarEntretenimento.com.br*](http://www.solarentretenimento.com.br)) para anotar esses detalhes.

-  **Primeiro escreva um resumo do que ocorreu naquela fase.** Algumas frases ou um parágrafo são mais que suficientes – você não vai querer estabelecer muitos detalhes no começo pois talvez precise ajustar alguns detalhes na próxima fase.
-  **Segundo, escreva um aspecto que tenha ligação com essa fase.** O aspecto pode cobrir a ideia geral do resumo ou pode focar em alguma parte dele que ainda se reﬂete no seu personagem atualmente.

------

### Para Veteranos

Se você está acostumado com outros jogos que usam o *Fate*, irá notar que existem menos aspectos nesta edição. Achamos mais fácil criar cinco bons aspectos do que sete ou dez. Isso porque há os aspectos do cenário e você pode criar aspectos para as situações, logo não faltarão coisas para invocar ou forçar.

Se o seu jogo usa muitas características ou se existe um determinado elemento em sua partida que você deseja que os personagens descrevam com aspectos (como raças ou nacionalidade) é possível aumentar o número de aspectos. Não recomendamos passar de sete aspectos de personagem – notamos que eles perdem o peso no jogo além dessa quantia.

------

### Fase Um: Sua Aventura

A primeira fase é a primeira aventura de seu personagem – seu primeiro livro, episódio, caso, filme ou qualquer outra coisa que tenha ele como estrela.

Você precisa pensar e escrever os detalhes básicos dessa história para a próxima fase. A história não precisa ter muitos detalhes – na verdade, umas poucas frases já servem – pois seus amigos irão adicionar mais detalhes dessa primeira aventura nas próximas duas fases (bem como você à deles).

Se você sentir dificuldades, dê uma olhada em seu conceito e dificuldade. Encontre um dilema que possa colocar essas ideias em foco. Que tipo de problemas você deve arranjar por causa de seu conceito e sua dificuldade? Como os outros aspectos ajudam ou complicam sua vida?

Faça-se as seguintes perguntas. Se for difícil respondê-las, peça ajuda para os outros jogadores e o Narrador.

-  Algo ruim aconteceu. O que foi? Aconteceu a você, com alguém que você se importa ou com alguém que você foi forçado a ajudar?
-  O que você decidiu fazer sobre o problema? Qual era seu objetivo?
-  Quem foi contra você? Você esperava essa oposição? Será que parte dela surgiu repentinamente?
-  Você venceu? Perdeu? De qualquer forma, quais as consequências por tais acontecimentos?

Uma vez que tenha respondido as perguntas, terá uma aventura. Tente então criar um aspecto que tenha ligação com ela.

**Uma observação sobre tempo:** Como outros dois personagens estarão envolvidos nas fases seguintes, essa aventura não pode acontecer tão cedo na vida de seu personagem que não tenha como ele conhecer os outros protagonistas. Se um de vocês decidiu que você surgiu recentemente na história, então as aventuras envolvendo aquela pessoa aconteceram há pouco tempo.

Se alguns de vocês são amigos (ou velhos rivais!) há um bom tempo, então essas aventuras podem ter acontecido num passado distante. O melhor é não fazer essas aventuras acontecerem em um momento específico; você pode trabalhar melhor essa parte quando souber quem está envolvido na sua história.

> Léo vai começar a fase um. Ele dá uma olhada nas perguntas da história para ajudá-lo a imaginar os eventos e decide o seguinte: o problema é que Esopo pode ter arranjado brigas pela galáxia devido ao seu aspecto “***Não Entendo Absolutamente Nada***". Numa dessas ele acidentalmente falou algo que não devia a um chefe local do Clã do Dedo e acabou sendo espancado e tendo sua nave roubada. Com a ajuda de um ex-criminoso chamado de Explorador, ele tenta montar um grupo para recuperar sua propriedade. Ele anota “***O Explorador Sabe Meus Segredos***”, porque ele quer manter contato esse personagem.
> 
> Michel vai passar pela fase um. O problema que aconteceu com Bandu é que ele foi abandonado numa pequena nave no espaço. Por sorte, uma nave inteligente o salvou e o criou. Seus poderes mentais acabaram colocando-o na mira do Império (justificando seu aspecto “***O Império Quer Minha Cabeça***”). Ele ainda tenta encontrar sua origem.
>
> Maira pensa que Fräkeline pode ser algum tipo de hacker e ela pode ter conseguido alguma informação preciosa sobre os Bruxos de Moondor, que agora querem saber quem ela é.
>
> Amanda escreve O Explorador e Nave-Mãe de Bandu como personagens do narrador (PdNs) e anota as relações dos personagens com os antagonistas para pensar em algo mais tarde.

Assim como o conceito e a difculdade, esta (e as próximas fases) é uma ótima oportunidade para criar detalhes do cenário.

------

### Fases E Cartões De Anotações

Na fase um você cria a primeira aventura de seu personagem. Nas fases dois e três você trocará informações com os outros jogadores para que todos estejam envolvidos uns nas histórias dos outros. Imaginar como o seu personagem pode fazer parte da história de outro pode ser difícil, por isso recomendamos que você use cartões de anotações (ou qualquer pedaço de papel que tiver).

Durante a primeira fase, ao descrever sua primeira aventura, pegue um cartão ou pedaço de papel e escreva o nome de seu personagem e descreva um pouco da aventura. Em seguida, passe esse cartão para os outros jogadores durante as fases dois e três, para que eles possam ajudá-lo a desenvolver. Dessa forma você ainda terá sua ficha enquanto anota suas contribuições e aspectos, assim como outros saberão com quais histórias precisam estabelecer ligações.

------

### Fase Dois: Caminhos Que Se Cruzam

As duas próximas fases requerem sentar com o restante do grupo para que seus personagens possuam papeis pequenos na sua aventura e vice-versa.

Uma vez que todos tenham descrito sua primeira aventura (é aí que as fichas pautadas se tornam realmente úteis) vocês estarão prontos para a fase dois. Você pode passar sua ficha de criação ou suas anotações da esquerda para a direita ou então misturar tudo e distribuir aleatoriamente (trocando o cartão ou ficha com a pessoa à sua direita se ela possuir um que você ainda não pegou). Independentemente do que for decidido, cada um receberá a história de outro personagem.

Seu personagem terá papel coadjuvante na história que você estiver segurando e este é o momento de criá-la. Discuta brevemente com o jogador daquele personagem sobre como adicionar uma frase que conecte o seu personagem àquela história. Papeis coadjuvantes podem ser de três tipos: eles complicam uma situação, resolvem uma situação ou ambos.

+  **Complicando a situação:** Seu personagem fez parte da aventura de forma desastrosa (possivelmente por causa de alguma questão do cenário ou por sua dificuldade). Claro, como isso aconteceu no passado, sabemos que tudo terminou bem (ou quase isso, como indicará o aspecto que você vai criar). Ao descrever isso, não se preocupe em como a situação vai se resolver – isso ficará nas mãos de outra pessoa ou até mesmo pode ficar em aberto. Coisas como “Esopo não soube se comportar quando Fräkeline precisava de silêncio” ou “Bandu foi capturado por uma brigada misteriosa” já é o suficiente para fazer as ideias ﬂuírem.
+  **Resolvendo uma situação:** Seu personagem resolveu uma situação com a qual o personagem principal da aventura estava lidando ou seu personagem auxiliou o protagonista da história em um conﬂito (uma boa oportunidade para usar o seu conceito). Ao descrever isso não é preciso mencionar como a situação ocorreu, apenas como seu personagem cuidou da questão. Descrições como “Fräkeline deu cobertura a Esopo para que ele tivesse tempo de fugir” ou “Bandu usa seus poderes mentais para controlar seus captores” é o suficiente para dar-nos uma ideia do que aconteceu.
+  **Complicando e resolvendo:** Nesta versão, o personagem ou resolve uma situação mas cria outra, ou cria uma situação mas mais tarde resolve uma diferente. Misture os dois eventos usando o termo “depois” entre as frases, como em “Esopo identifica Bandu como procurado pelo Império, chamando a atenção de mercenários locais. Depois, ele o ajuda combatendo membros do Clã do Dedo enquanto Bandu confunde suas mentes, conseguindo fugir”.

A ideia é ser um pouco egoísta nesse momento. Você deve chamar atenção ao seu personagem para conseguir criar um bom aspecto: algo que você abe, fez, ganhou ou possui e alguém com quem você possui alguma relação (boa ou ruim).

Finalmente, escreva a ideia da aventura e a contribuição do seu personagem em suas anotações. Isso é importante pois seu personagem ganha um aspecto por seu papel coadjuvante na história. A pessoa a quem pertence a aventura inicial também deve anotar a contribuição, se houver espaço em sua ficha.

> Maira lê a aventura inicial de Michel, uma história aventuresca onde Bandu tenta retornar um artefato raro necessário para a sobrevivência de um povo distante.
>
> Ela pensa um pouco a respeito e decide que Fräk se emocionou com a história e ajudou Bandu em sua missão. Ela adiciona “***Não Resisto a Histórias Tristes***” como aspecto para representar a razão de seu envolvimento.

------

As três fases priorizam conectar os personagens através de suas histórias. Gostamos disso porque é cooperativo e incentiva a comunicação. No entanto, essa não é a única forma de fazer isso. Você pode fazer qualquer tipo de compartilhamento nessas fases. Seu passado, seu presente e suas esperanças para o futuro são outros três bons exemplos para essa fase. [*Fate RPG - Ferramentas do Sistema*](../../ferramentas-de-sistema) possui alguns outros exemplos.

------

### Fase Três: Caminhos Que Se Cruzam Novamente

Uma vez que todos tenham finalizado a fase dois, vocês trocarão histórias da mesma forma que anteriormente, até que cada um tenha uma aventura inicial que não seja a sua ou alguma que já tenha contribuído. Começa então a fase três, onde cada um contribui para outra história e adquire um segundo aspecto a partir disso. Siga os passos da fase dois.

> Maira está com a aventura inicial e Esopo na mão e precisa decidir como ela se encaixa nessa história.
>
> Ela decide que Fräkeline entrou em contato com Esopo através da informação que conseguiu dos Bruxos, informando que ele fazia parte de alguma profecia. Ela adiciona ***Esopo Precisa Ser Protegido*** como aspecto, representando seu comportamento para com o problema. O grupo ainda não sabe como Esopo se encaixa nessa profecia, mas eles imaginam que irão descobrir durante o jogo.

E assim você tem cinco aspectos e um bom pano de fundo para a história!

------

### Menos de Três Jogadores?

As três fases assumem que você possui ao menos três jogadores. Se você possui apenas dois, considere as seguintes ideias:

-  **Pule as três fases** e crie apenas mais um aspecto, agora ou durante o jogo.
-  Crie uma terceira história compartilhada e descreva a participação de cada um.
-  **O Narrador também pode criar um personagem**. Ele não jogará com esse personagem junto com os PJs – deve ser um PdN comum. Esse tipo de PdN pode ser um bom ponto de partida para o início de uma campanha – se um de seus amigos for preso durante a criação de personagem e desaparece ou até mesmo morre durante a campanha, isso é combustível instantâneo para o drama.

Se você tem apenas um jogador, pule todas as fases, deixe os aspectos em branco e crie durante o jogo.

------

- [« A Ideia do Personagem](../ideia-personagem/)
- [Perícias »](../pericias/)
