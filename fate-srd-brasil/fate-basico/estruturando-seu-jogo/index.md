---
title: Estruturando Seu Jogo
layout: default
---

## Estruturando Seu Jogo

O primeiro passo ao estruturar sua partida de *Fate* é decidir que tipo de pessoas são os protagonistas e como é o mundo que os cerca. Suas decisões aqui determinarão tudo que precisa saber para fazer a coisa funcionar: em que os protagonistas são bons, com o que devem se importar ou não, que tipos de problemas enfrentarão, que tipo de impacto esses personagens têm no mundo e assim por diante. Não são necessárias respostas detalhadas (já que os detalhes são descobertos durante o jogo), mas você deve ter ao menos uma ideia, para não ficar muito vago.

Primeiro começaremos com a ambientação. Falaremos sobre os protagonistas depois, em [*Criação de personagens*](../criacao-de-personagens).

- [« Estruturando o jogo](../estruturando-o-jogo/)
- [Adaptando O Cenário Ao Fate »](../adaptando-cenario-fate/)
