---
title: Armas e Armaduras Alternativas
layout: default
---

## Armas e Armaduras Alternativas

No sistema Fate padrão, armas e armaduras — assim como qualquer outro equipamento — são simplificados de forma que sua diferença seja apenas cosmética. Possuir uma arma permite que você ataque alguém usando Atirar, mas nada além disso. Se quiser que armas e armaduras sejam mais importantes em seu jogo, o capítulo Extras no Fate Sistema Básico, apresenta [regras para potência de Armas e Armaduras](../../fate-basico/mais-exemplos-de-extras/#potência-de-armas-e-armaduras). Se mesmo essas regras não suprirem suas necessidades, eis algumas novas alternativas.

### Dano Mínimo e Máximo

No Fate Sistema Básico, armas são _perigosas_, se você usar as regras do capítulo Extras. Um espadachim pouco treinado com sorte nos dados ao usar uma espada de duas mãos pode partir um homem ao meio sem muito esforço, e isso pode ser assustador. Isso leva a um jogo mais realista e letal. Se essa for sua praia, talvez prefira armas e armaduras com valores que as deixem menos letais, mas ainda importantes.

Ao usar essas regras, os valores de potência de uma arma ainda começam em 1 e aumentam a partir disso, mas podem ir um pouco mais além (digamos 5 ou 6, para as armas mais letais). Em vez de simplesmente adicionar o nível da arma às tensões que conseguiu, o valor da arma fornece um número _mínimo_ de tensões de estresse que você pode provocar com aquela arma. Por exemplo, uma espada longa com Arma:3 causa no mínimo três tensões, mesmo se você rolar 1, 2 ou 3 de tensões de dano em seu ataque. Se rolar acima disso, simplesmente considere o valor e ignore o dano mínimo.

Já as armaduras funcionam da forma contrária; elas especificam o número máximo de tensões de estresse que você pode rece-
ber em um ataque. As armaduras começam com valor 4 (para armaduras leves), decrescendo até 1 (armas de placas muito pesadas ou trajes de combate avançado). A exceção a essa regra é quando o ataque é bem-sucedido com estilo — neste caso você deve ignorar o valor de Armadura. O atacante causa o dano total quando consegue um sucesso com estilo.

_Valores de Armadura são mais importantes que o valor de Arma._ Isso significa que se alguém com Arma:5 atacar alguém com Armadura:3, causará apenas 3 de estresse a não ser que seja bem-sucedido com estilo.

### Aspectos de Armas e Armaduras

[Anteriormente neste livro](../aspectos-de-equipamento/) mencionamos que você poderia transformar aparatos importantes em aspectos. Isso funciona para a maioria dos equipamentos, mas algumas pessoas talvez queiram algo mais ao lidar com armas e armaduras. Esta modificação nas regras foi feita para ser usada em conjunto com aquele sistema, mas não há nada que o impeça de usá-la sozinha — isso só significaria que _apenas_ armas e armaduras são aspectos, outros aparatos, não.

As armas são divididas em **leves** , **médias** e **pesadas**. Uma arma leve pode ser uma faca pequena ou porrete, uma arma média pode ser algo como uma espada ou pistola e uma arma pesada talvez seja uma espingarda, um rifle de precisão ou uma enorme espada de duas mãos.

Quando for bem-sucedido em um ataque usando uma arma, você pode fazer uma invocação especial com ela. Isso custa um ponto de destino, mas não fornece +2 nem um novo lance de dados. Em vez disso, você pode forçar o seu oponente a receber uma consequência ao invés de estresse. Com uma arma leve, você pode forçar alguém a receber uma consequência suave. Uma arma média força uma consequência moderada e uma arma pesada força uma consequência severa. Se bem-sucedido com estilo,
eleve a severidade da consequência em um tipo — suave se torna moderada, moderada se torna severa, severa para ou derrotar o adversário, ou uma consequência extrema (à escolha ~~da vítima~~ do defensor). Se a caixa da consequência já estiver em uso, eleve a consequência em um nível de severidade.

Essa invocação especial também funciona um pouco como forçar um aspecto. Quando invoca um aspecto de arma desta forma, você oferece um ponto de destino ao seu alvo. Se ele aceitar o ponto, você impõe a consequência. Ele pode _recusar_ o ponto e pagar um dos seus próprios para não receber a consequência, recebendo então o estresse que teria recebido de qualquer forma.

Aspectos de armadura também são divididos em leve, médio e pesado, além de também permitirem invocações especiais. Você pode invocar uma armadura leve para absorver uma consequência suave; você não recebe o estresse que foi absorvido e não preenche a caixa de consequência. Armaduras médias podem absorver consequências moderadas. Armaduras pesadas absorvem consequências severas ou duas consequências suaves ou moderadas em qualquer combinação. Você pode usar sua armadura para absorver consequências além desses limites, mas se o fizer, o aspecto da armadura muda imediatamente para algo que represente um defeito e ela não estará mais disponível para absorver ataques. Será necessário consertá-la, o que pode envolver custos e algumas horas ou dias, dependendo do tipo de armadura usada.

### Dados Vermelhos e Azuis


Se você tiver ao menos três cores de dados Fate e quiser algo que não seja tão previsível para suas armas e armaduras, poderá usar a regra dos Dados Vermelhos e Azuis em lugar dos valores de Arma e Armadura. Usamos os termos “vermelho” e “azul” aqui apenas por conveniência; use as duas cores que preferir, contanto que não sejam usadas para mais nada. 

__Vermelho para armas:__ quanto mais Vermelho sua arma possui, maior e/ou mais letal ela será. Uma arma Vermelho:1 pode ser uma adaga, as garras de um goblin ou uma pistola de baixo calibre, enquanto uma arma Vermelho:4 poderia ser uma espada larga, a mordida de um dragão ou um tiro a queima roupa de uma espingarda. 

Quando realizar um ataque, para cada ponto Vermelho que sua arma possuir, substitua um dado Fate normal por um dado Fate Vermelho. Se o ataque for um empate ou melhor, cada dado vermelho com resultado + aumenta a tensão do golpe em +1.

__Azul para armaduras:__ Quanto mais Azul sua armadura possuir, mais proteção ela oferece. Armaduras leves como de couro curtido ou couro firme são Azul:1. Armaduras pesadas, como cotas de malha ou armaduras de placas, são Azul:2 ou 3.

Ao se defender, para cada ponto de Azul que você possuir, substitua um dos dados Fate normal por um dado Fate Azul. Se sua defesa falhar ou empatar, cada um de seus dados azuis com resultado + absorve uma tensão de dano. 

Como você sempre lança quatro dados Fate, o número máximo de dados Vermelhos ou Azuis que você pode rolar será quatro.

Para misturar ainda mais as coisas, você pode conceder dados Azuis a armas defensivas, como um bastão ou faca de combate, e dados Vermelhos a armaduras “agressivas” , como armaduras de placas com espinhos ou campo de força eletroestático. Você também pode ter façanhas que permitam trocar dados Fate Azuis por Vermelhos quando defender — por exemplo um mestre de esgrima com um contra-ataque matador, ou um escudo mágico flamejante.


------

- [« Riqueza](../riqueza/)
- [Ações Suplementares »](../acoes-suplementares/)
