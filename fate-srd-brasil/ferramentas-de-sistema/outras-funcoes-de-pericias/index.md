---
title: Outras Funções de Perícias
layout: default
---

## Outras Funções de Perícias


Perícias permitem que você realize várias coisas, mas elas podem ser ainda mais flexíveis do que aparentam. Se der uma olhada na lista de perícias verá que cada uma delas possui uma ação de superar e criar vantagem marcadas. Isso abre muitas possibilidades, mesmo para perícias que aparentemente não possuem formas interessantes de uso.

Se um personagem possui um nível alto em uma perícia, Bom (+3) ou maior, isso significa que ele é um perito naquela área. Ele possui grande conhecimento dentro das possibilidades cobertas pela perícia. Este conheci- mento cobre as ferramentas e aparatos usados e o conhecimento de outras pessoas que usam a perícia no mesmo nível.

As três perícias usadas para atacar – Lutar, Atirar e Provocar – são mais usadas em combate. Isso não quer dizer que o combate é a única situação onde essas perícias são úteis. Um personagem com a perícia Atirar pode identificar armas, saber detalhes sobre sua manutenção e como obtê-las. O mesmo vale para Lutar, no que diz respeito a armas brancas. 

Isso abre diversas possibilidades para as ações de superar e criar vantagem. Um especialista em Atirar pode usar a ação superar para encontrar um vendedor de armas quando entra em um novo local, por exemplo. Atirar pode ser usado para criar vantagem por identificar o tipo de arma pelo som do disparo e descobrir assim os detalhes e limitações do modelo ou notar que a arma de um inimigo não recebeu a manutenção devida e pode sofrer um mal funcionamento.

Provocar pode parecer uma perícia difícil de expandir, mas com ela um especialista pode criar vantagem para descobrir o blefe de um oponente ou para identificar qual pessoa num grupo de oponentes é a mais perigosa. 

Outras perícias podem ser usadas da mesma forma. Especialistas em Roubo podem encontrar ferramentas ou parceiros na área, ou mesmo identificar técnicas de outros ladrões pelos sinais que deixam para trás. Especialistas em Condução podem identificar veículos e criar vantagem para falar sobre as sutis vantagens e desvantagens ao compará-los com outros veículos. Perícias que normalmente não permitem o ataque ou defesa – Roubo, Ofícios, Investigar, Conhecimento e Recursos – possuem muitas opções para ações adicionais e, nas circunstâncias adequadas, podem ser usadas para atacar e defender. Na situação certa, Ofícios poderia ser usada para construir uma armadilha perigosa, por exemplo. Roubo poderia ser usado para defender, se um personagem cria contramedidas baseadas em seu conhecimento. Essas aplicações podem ser bem específicas e limitadas, mas pode ser bastante recompensador permitir aos jogadores usar suas perícias de uma forma que amplie um pouco suas capacidades.

O segredo está na criatividade. Em praticamente qualquer perícia, um alto nível abre muitas possibilidades. Como Narrador, use flexibilidade e lógica como guia à medida que seus jogadores forem lhe surpreendendo. Eles devem ter a chance de sentirem-se excepcionalmente eficientes na área de atuação de seus personagens. Quando um jogador tenta algo novo com uma perícia, considere com cautela e esteja aberto a uma interpretação criativa da perícia. Como jogador, tente novas possibilidades no uso de suas perícias. Se surgir uma situação na qual usar uma perícia de uma maneira diferente parece uma opção, tente!  O Fate se torna mais interessante quando coisas inesperadas acontecem e os jogadores usam métodos inesperados para superar as situações em que se encontram.

------

- [« Mudanças Estruturais](../mudancas-estruturais/)
- [Esferas de Perícias »](../esferas-de-pericias/)
