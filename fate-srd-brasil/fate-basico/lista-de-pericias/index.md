---
title: Lista De Perícias
layout: default
---

## Lista De Perícias

Aqui está uma lista básica de perícias que serve como exemplo para suas partidas de *Fate*, seguidas de exemplos de façanhas para cada uma delas. São elas que usamos para todos os exemplos nesse livro e devem servir de base para ajustar suas próprias listas, adicionando e restringindo perícias para melhor se enquadrar em seu cenário. Para saber mais sobre como criar suas próprias perícias, veja o capítulo [*Extras*](../extras/).

Cada descrição de perícia contém uma lista de ações de jogo que se aplicam à perícia descrita. Essa lista não é completa – veja as instruções do que fazer em casos especiais na [seção _Durante o Jogo_](../durante-o-jogo/#julgando-o-uso-de-perícias-e-façanhas).

------

### Criando Cenários a partir de Perícias

As perícias são uma das mecânicas primárias que reforçam a ambientação do cenário que você está criando para o seu jogo. As perícias fornecidas nesta lista são propositalmente genéricas para que possam ser usadas em vários tipos de cenário, assim como as façanhas por não estarem atreladas a cenários específicos.

Ao criar seu próprio cenário para usar com *Fate*, você também deve criar sua própria lista de perícias. A lista oferecida aqui é um bom ponto de partida, mas criar perícias específicas para o seu mundo pode ajudar a dar mais profundidade ao usar uma mecânica personalizada para reforçar a história. Façanhas também devem reﬂetir os tipos de habilidades disponíveis em seu jogo.

------

------

### Perícias e Equipamentos

Algumas perícias, como Atirar e Ofícios, pressupõem uso de equipamento específico. Presumimos que se você possui a perícia, também possui o equipamento para usá-la e que a eficácia do equipamento está inclusa no resultado da perícia. Se você deseja tornar equipamento algo especial, vale a pena dar uma olhada no capítulo [*Extras*](../extras/).

------

### Lista de Perícias

| **Perícia**   | **Superar** | **Criar Vantagem** | **Atacar** | **Defender** |
|--------------:|:-----------:|:------------------:|:----------:|:------------:|
| Atletismo     | X           | X                  |            | X            |
| Atirar        | X           | X                  | X          |              |
| Comunicação   | X           | X                  |            | X            |
| Condução      | X           | X                  |            | X            |
| Conhecimentos | X           | X                  |            | X            |
| Contatos      | X           | X                  |            | X            |
| Empatia       | X           | X                  |            | X            |
| Enganar       | X           | X                  |            | X            |
| Furtividade   | X           | X                  |            | X            |
| Investigar    | X           | X                  |            |              |
| Lutar         | X           | X                  | X          | X            |
| Ofícios       | X           | X                  |            |              |
| Percepção     | X           | X                  |            | X            |
| Provocar      | X           | X                  | X          |              |
| Recursos      | X           | X                  |            |              |
| Roubo         | X           | X                  |            |              |
| Vigor         | X           | X                  |            |              |
| Vontade       | X           | X                  |            | X            |

- [« Criando Façanhas](../criando-facanhas/)
- [Atletismo »](../atletismo/)
