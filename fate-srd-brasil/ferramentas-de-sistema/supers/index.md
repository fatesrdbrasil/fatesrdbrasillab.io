---
title: Supers
layout: default
---

## Supers

Os personagens em Fate são pessoas incrivelmente competentes e bem-sucedidas, mas às vezes seu grupo pode querer ir mais além, contando histórias de verdadeiros super-heróis que lutam contra o crime e supervilões que desejam conquistar o mundo. Esta seção lhe dará algumas ferramentas para fazer super-heróis funcionarem bem em seu grupo!

### Histórias de Origem

Super-heróis em Fate se parecem bastante com personagens normais, mas podem substituir sua dificuldade por uma história de origem, um aspecto que diz brevemente como o personagem adquiriu seus superpoderes e/ou porque esses poderes trazem problemas. Lembre-se que aspectos devem sempre possuir dois sentidos, representando tanto os pontos fortes como as fraquezas do personagem. Por exemplo, um herói pode ter o conceito **_Supersoldado Geneticamente Modificado_** para representar sua força e agilidade sobre-humanas e sua origem poderia ser **_Viajante Dimensional_** para mostrar que ao mesmo tempo ele é um peixe fora d’água em nosso mundo.

### Superperícias

Uma forma de criar superpoderes em Fate é permitir que jogadores escolham perícias como se fossem poderes ao invés de criar façanhas. Um herói nascido em um mundo alienígena avançado pode possuir Superconhecimento. Um adolescente tocado pelos poderes de um deus primordial pode possuir um Superatletismo. Heróis que usam tecnologia ou itens mágicos podem investir suas perícias em tais itens se tornarem uma perícias extras, carregando a Lanças do Destino (Vigor) ou construindo robôs mecânicos fantásticos (Atirar).

Heróis podem usar a perícia normalmente, mas também podem optar por usar o poder ao custo de um ponto de destino. Quando ativada, a superperícia permite ao herói fazer proezas fantásticas, dobrando seu bônus de perícia atual. Após rolar, o personagem deve reduzir o valor da perícia em um. Desde que a perícia não seja reduzida a zero, o valor é recuperado na próxima cena. Se o poder esgotar, no entanto, o herói terá de encontrar uma forma de descansar ou recarregar antes de usá-lo novamente.

---

Maxine, uma Valquíria com Vigor Excepcional (+5), pode gastar um ponto de destino para jogar um carro em um supervilão — ganhando 5 tensões adicionais em sua rolagem —, mas após lançar os dados a perícia reduz para Ótimo (+4). Se ela continuar usando sua superforça até que seu Vigor se esgote, ela precisará fazer uma jornada a Valhalla para beber do Chifre do Poder e recuperar suas forças.

---
 
Este sistema lembra os heróis da Era de Ouro, que conseguiam fazer proezas incríveis com poucas desvantagens além de ficarem cansados ou sem energia. Isso também dá ao Narrador uma chance de criar cenas de recarga interessantes (veja Recarga ), onde os heróis precisam recarregar seu poder, reconectar com suas vidas humanas ou descansar o suficiente para encarar o vilão com força total. Por exemplo, uma heroína adolescente pode precisar voltar para casa para se conectar com seu pai, enquanto um predador mutante noturno pode se retirar para seu covil para dormir durante o dia antes de retornar à caça.
 
### Criando Façanhas Superpoderosas
 
Se o seu grupo prefere poderes mais definidos, permita que jogadores comprem poderes em forma de façanhas, talvez até mesmo dando-lhes uma ou duas façanhas extras grátis. Para um jogo com baixo nível de poder, emulando os anti-heróis da década de 1980, as façanhas existentes no Fate Sistema Básico provavelmente serão suficientes — os heróis não possuem poderes incríveis, são apenas pessoas extraordinárias que vestem fantasias.

Se seu interesse é contar histórias mais épicas, você também pode criar façanhas que permitam habilidades poderosas, mas que em contrapartida exijam a aposta de pontos de destino. Obstáculos estáticos podem ser vencidos com o gasto de apenas um ponto de destino, mas outros personagens superpoderosos podem aceitar, cancelar ou aumentar a ação ao gastar seus próprios pontos de destino e narrando sua oposição. Isso continua até que um dos lados não esteja mais disposto a gastar pontos de destino. Se a ação terminar em um empate, os pontos são dados ao Narrador, mas se um personagem claramente vence, o perdedor recebe todos os pontos de destino. Note que isso pode resultar em reviravoltas dramáticas no sistema de pontos de destino, já que um jogador receberá uma mão cheia de pontos de destino ao fim de um conflito.
 
---
 
Sérgio é um herói com a façanha Entortar Barras, Erguer Portões, que permite que ele destrua obstáculos físicos ao apostar um ponto de destino. Se nenhum personagem superpoderoso se opuser, a ação é bem-sucedida sem a necessidade de uma rolagem. No entanto, se Maxine usar seus poderes nórdicos para eletrocutá-lo através das barras de metal que ele está tentando torcer, ela pode gastar um ponto de destino para cancelar a ação dele, ou gastar dois pontos para tentar aplicar o seu poder (Tempestade de Raios) enquanto cancela a façanha dele. Se ela gastar dois pontos de destino, Sérgio terá a chance de gastar novamente para cancelar ou aumentar, ou ele poderia aceitar a ação dela e receber os dois pontos de destino que ela investiu.
 
---
 
### Vilões e Capangas

O Fate já possui regras ótimas para a [criação de PdNs](../../fate-basico/criando-a-oposicao/). Para histórias de super-heróis, o Narrador pode denominá-los lacaios, vilões e supervilões para dar uma noção melhor do perigo que cada um deles representa.

Preste atenção nas sugestões no Fate Sistema Básico para grupos de lacaios e tratá-los como obstáculos. Às vezes a melhor forma de mostrar um herói derrotando um grupo de capangas de baixo nível é em uma única rolagem ou com o uso de um superpoder.

Os vilões devem ser mais ou menos como os heróis, provavelmente possuindo um ou dois superpoderes. Você provavelmente vai querer um grupo de vilões que possa enfrentar de igual para igual os heróis por tempo o suficiente para tornar a disputa interessante, mas provavelmente não serão a verdadeira ameaça que os heróis estão combatendo.

Supervilões, no entanto, podem ser personagens completos, possuindo seu próprio conceito, origem, superpoderes e extras próprios. Não poupe esforços na história de origem do supervilão; é ela que transmite o “sabor” especial do personagem aos jogadores. Um cultista maligno pode ter sua origem como **_Perdi o Único Homem a Quem Pude Amar_** enquanto um supersoldado demoníaco pode possuir a origem **_Condenado por Forças Infernais a Vagar Pela Terra_**.

Heróis também podem possuir seus próprios ajudantes (veja [Companheiros e Aliados](../companheiros-e-aliados/)) à medida que a história avança, comprando-os como extras assim de alcançarem algum marco.

---

O cenário de campanha **_Wild Blue_** , presente no **_Fate Worlds_** , traz algumas ideias incríveis para a criação de poderes como façanhas com custos narrativos, como viagem no tempo sem controle total, ou telepatia que sempre descobre informações indesejadas. Leia o livro para descobrir ainda mais uma forma de criar poderes em Fate! 

De forma similar, superpoderes podem ser chamados de _magia_. Confira o capítulo anterior para conhecer algumas ideias sobre como fazer algo mais envolvente e complexo com os seus poderes, construindo-o como um sistema de magia. Muitas outras maneiras para elaborar poderes estão disponíveis nos cenários criados exclusivamente para a edição brasileira. Procure por essas indicações!

---
 

------

- [« Veículos](../veiculos/)
- [O Paradoxo do Horror »](../paradoxo-do-horror/)
