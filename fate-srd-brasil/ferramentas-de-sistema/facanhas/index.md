---
title: Capítulo 4 - Façanhas
layout: default
---

# Capítulo 4 - Façanhas

## Novas Classes de Façanhas

As façanhas, da forma como são apresentadas no Fate Sistema Básico, criam diversas formas de personalizar seu personagem, adicionando ajustes mecânicos interessantes ao jogo. Extraímos mais de cada perícia ao criar façanhas, e esse processo pode ser bem divertido. Se desejar mexer um pouco mais em suas façanhas, esta seção foi feita para você.

### Façanhas Flexíveis

Esta é a opção mais fácil de implementar, pois não passa de uma pequena mudança na sua forma de pensar em façanhas. No Fate Sistema Básico, façanhas estão intimamente ligadas às perícias. E se você quiser que suas façanhas sejam livres dessa ligação, ou ligadas a múltiplas perícias, ou que sejam ligadas a algo completamente diferente como um aspecto, aparato ou então uma barra de estresse? 

Alguns exemplos:

**Escudo Aliado:** Você pode invocar **Escudeira Anã** quando um aliado sofrer um ataque próximo a você. Quando o fizer, redirecione o ataque a si mesmo. Sua defesa é Regular (+1) contra esse ataque.

**Fúria Descontrolada:** Quando sofrer uma consequência física, você pode invocar essa consequência de graça em seu próximo ataque. Se você sofrer múltiplas consequências físicas, você recebe uma invocação grátis de cada.

**Bugigangas Úteis:** Seus bolsos estão repletos de pequenos objetos úteis. Sempre que precisar de algo, você o possui, contanto que não seja algo _muito_ fora do comum (como um mapa para encontrar o tesouro) ou grande demais para caber em seu bolso, algibeira ou mochila. Quando você diz que possui algo, o Narrador deveria aprovar com maior facilidade. 

Isso na verdade não se trata de uma alteração na mecânica do jogo, apenas uma mudança na aparência da façanha. Qualquer um dos três exemplos acima poderia estar relacionado a uma perícia — Provocar, Lutar ou Recursos, por exemplo — mas não ter que pensar a qual perícia ligar sua façanha lhe dá mais liberdade criativa em seu funcionamento, indo além dos bônus de +2 e trocas de perícia.

### Façanhas de Aspectos

Para façanhas conectadas a aspectos, pode ser que você encare seus efeitos como invocações gratuitas em situações raras. Outras façanhas de aspectos podem exigir uma invocação, como Escudo Aliado (acima), mas fornecer algo extra ou particularmente incomum quando o aspecto é invocado. Tais efeitos devem ser mais potentes do que as invocações padrão. Você pode até mesmo criar uma façanha que esteja conectada a ações específicas de forçar — mas tenha cuidado para não neutralizar a desvantagem com o benefício.

**Investida do Touro:** Como você é ***Forte Como um Touro***, uma vez por cena, como uma ação simples, pode se mover duas zonas em linha reta e realizar um ataque físico.

**Arruaceiro de Aço:** Quando o seu aspecto ___Não Consigo Manter a Boca Fechada___ é forçado para torná-lo alvo de um ataque, você pode limpar imediatamente quaisquer consequências suaves que possua, em lugar de receber um ponto de destino.

### Gatilho de Efeito


Quando usa esta mecânica, você cria façanhas que são ativadas por condições narrativas específicas, exigem uma rolagem de perícia e possuem um efeito específico como resultado. Façanhas assim são ótimas para encorajar os jogadores a fazer coisas que você espera, já que serão recompensados por isso.

__Um Amigo em Todo Lugar:__ Sempre que entrar em um local povoado, você pode declarar que já o visitou antes e realizar uma rolagem de Contatos contra uma oposição Razoável (+2). Se bem-sucedido, você possui um amigo que lhe deve um favor – nada muito valioso ou ameaçador. Se bem-sucedido com estilo, seu amigo fará qualquer coisa que estiver a seu alcance.

__Não Estou Para Brincadeiras:__ Quando você deixa claro o quão perigoso é, faça uma rolagem de Provocar contra a Vontade do alvo. Se bem-sucedido, o alvo não lhe atacará ou se aproxima de você de boa vontade, a não ser que você tome ação contra ele primeiro. Se bem-sucedido com estilo, qualquer um que possuir Vontade abaixo do nível do alvo também responderá dessa forma.

__Passo do Tornado:__ Quando você assume a posição marcial do tornado, role Atletismo contra uma oposição Razoável (+2). Se bem-sucedido, você pode correr em superfícies verticais e saltar distâncias improváveis sem a necessidade de uma rolagem, até o fim de seu próximo turno. Se bem-sucedido com estilo, você ganha esses benefícios até o fim da cena.

Você provavelmente notou que nenhuma dessas façanhas mostra o que acontece quando empata ou falha; isso é intencional. Estes gatilhos de efeito tendem a ser poderosos, então seus problemas devem equilibrá-los. Um empate deve ser semelhante a um sucesso, mas com algum custo. Em caso de falha, sinta-se livre para aplicar as reviravoltas apropriadas.

### Façanhas Amplas

Se você está à procura de maior variedade em suas façanhas do que um simples +2 ou semelhante, considere a ideia de ampliar a façanha permitindo que ela forneça +1 em duas ou três coisas. Poderiam ser três ações diferentes com a mesma perícia, ou se ramificar por várias perícias relacio- nadas. Se permitir façanhas amplas assim, tome cuidado com a sobreposição em façanhas combinadas: você não quer duas façanhas amplas causando o efeito total de três +2 pelo preço de apenas duas façanhas.

### Façanhas Combinadas

Se quiser oferecer façanhas particularmente poderosas, considere construir o benefício de múltiplas façanhas juntas para produzir um único grande efeito. Por exemplo, você pode criar uma façanha que produza um efeito monstruoso de 4 tensões — combinando duas façanhas com o custo de duas recargas. (Talvez reconheça esse método como o utilizado na construção de poderes em The Dresden Files RPG). Entretanto, esse tipo de benefício concentrado pode acabar com o equilíbrio do jogo rapidamente. Considere limitar o acesso a essas “superfaçanhas”, seja sua quantidade – como “todos podem ter apenas uma façanha dupla” — ou durante por seleção e permissão — “apenas estas façanhas estão disponíveis para lobisomens”.

------

- [« Esferas de Perícias](../esferas-de-pericias/)
- [Custo de Façanhas »](../custo-de-facanhas/)
