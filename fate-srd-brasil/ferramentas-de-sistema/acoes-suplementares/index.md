---
title: Ações Suplementares
layout: default
---

## Ações Suplementares

Ações suplementares não aparecem em nenhum lugar no Fate Sistema Básico, apesar de terem aparecido em versões mais antigas do Fate. Fizemos isso por dois motivos. Primeiro, sentimos que é melhor usar bônus do que penalidades, sendo que ações suplementares impõem uma penalidade. Segundo, ações suplementares existem para tornar as coisas mais “realistas”, mas não tornam as coisas necessariamente mais divertidas ou a história mais empolgante. Elas dificultam fazer coisas legais, ao invés de facilitar.

Isso não significa, porém, que o conceito não tenha valor. Se quiser usar ações suplementares, poderá usá-las como são mostradas nas versões anteriores do Fate — realizar uma ação secundária que irá lhe distrair de sua ação principal impõe uma penalidade de -1 na ação principal. A maioria dessas ações são ações gratuitas no Fate Sistema Básico. Deixamos que você determine quais tipos de ações gratuitas passariam a ser suplementares.

Se quiser algo um pouco mais no estilo do Fate Sistema Básico, que não imponha uma penalidade, tente o seguinte.

---

### Ação Suplementar

Ao realizar alguma ação menor em conjunto com sua ação principal — mover-se uma zona, pegar uma arma, procurar cobertura ou algo mais que o Narrador determine como ação suplementar — você cria um impulso, algo como ___Distraído___, ___Visão Obstruída___ ou ___Mira Instável___, que permanecerá até o início de seu próximo turno. Você só pode executar uma ação desse tipo, então criará apenas um impulso por vez. Qualquer um que esteja agindo contra você ou se defendendo contra um ataque seu pode usar esse impulso — que então desaparece como um impulso normal. Além disso, o Narrador pode forçá-lo contra você uma vez de graça — o que significa que não precisa lhe oferecer um ponto de destino por isso, mas você ainda precisará pagar um ponto para resistir —, também fazendo com que o impulso desapareça em seguida.


------

- [« Armas e Armaduras Alternativas](../armas-e-armaduras-alternativas/)
- [Magia »](../magia/)
