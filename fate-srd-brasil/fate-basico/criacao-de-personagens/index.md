---
title: 3 - Criação de Personagens
layout: default
---

# 3 - Criação de Personagens

## Criar Personagens É Jogar

Você já está jogando *Fate* no momento que sentar para criar o jogo e os personagens. Esse estilo de criação de personagens envolve três coisas para garantir que isso aconteça.

Primeiro, **a criação do personagem conta parte de sua história**, como em qualquer outra sessão de jogo. Personagens que possuem histórias próprias e conexões com os outros ganham muito mais vida. Isso estabelece por onde passaram, o que fizeram e porque continuam a lutar contra as questões e enfrentam, juntos ou como oponentes. Você estará entrando em uma história em andamento, mas as coisas mais interessantes ainda não aconteceram!

Em segundo lugar, ela **organiza o cenário para a próxima parte da história**. Cada arco de uma história é uma preparação para o próximo, de forma a criar um ﬂuxo natural de um para o outro. A história dos personagens precisa definir o primeiro arco da história.

Por último, **a criação de personagens em *Fate* é colaborativa**. Assim como a criação do cenário, a criação de personagem é melhor como uma atividade em grupo. Ao fazer tudo isso em conjunto se consegue uma boa comunicação entre o Narrador e os jogadores e esse processo possui várias formas de estabelecer conexões entre os personagens e o cenário.

Se combinada com a criação do cenário, a criação de personagens pode durar uma sessão inteira – isso permite a todos aprenderem sobre os personagens e sobre o mundo de jogo. Você e os outros jogadores conversarão sobre seus personagens, dando sugestões uns aos outros, discutindo suas conexões e estabelecendo mais detalhes do cenário.

É interessante fazer anotações de todo esse processo. Você pode usar as fichas no final do livro ou baixá-las em [**Solarentretenimento.com.br**]().

Comece determinando o **Conceito** e a  **Dificuldade** de seu personagem. Então crie uma história para ele, que é um processo de três fases. Uma vez que tenha a história em mente, escolha as perícias do personagem e suas façanhas. A partir daí é só jogar!

------

### Ao Criar O Personagem:

-  **Aspectos:** crie o conceito e a dificuldade.
-  **Nome:** nomeie o personagem.
-  **Fase Um:** descreva sua primeira aventura.
-  **Fase Dois e três:** descreva como seu caminho cruza o de outros personagens.
-  **Aspectos:** escreva um aspecto para cada uma dessas três experiências.
-  **Perícias:** escolha e gradue suas perícias.
-  **Façanhas:** escolha ou crie de três a cinco façanhas.
-  **Recarga:** determina quantos pontos de destino você tem no início do jogo.
-  **Estresse e Consequências:** determinam quanto dano o seu personagem pode receber.

------

- [« Criando Personagens](../criando-personagens/)
- [A Ideia do Personagem »](../ideia-personagem/)
