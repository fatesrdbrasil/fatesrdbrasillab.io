---
title: Os Seis Vizires
layout: default
---


## Os Seis Vizires

### Notas de Criação

O sistema dos Seis Vizires assume uma sistema de magia diferenciada no qual cada um dos Vizires possui suas próprias propriedades e tendências, mas não se manifestam como seres de vontade própria. De modo geral, a magia não possui um custo associado, embora existam elementos sociais envolvidos. Isso é equilibrado por sua raridade relativa (embora esteja totalmente disponível aos PJs) e o fato de ser potente, mas não totalmente flexível. É também um sistema fortemente estruturado, com efeitos mágicos estritamente criados dentro do tema de cada Vizir (expressos através de perícias).

A fonte da magia são os Seis Vizires. O que eles realmente são é uma pergunta interessante e a resposta a isso pode render uma campanha. Neste cenário, eles são seis constelações que servem à Imperatriz e concedem poder àqueles que possuem afinidade com elas.

Do ponto de vista mecânico, este sistema se baseia fortemente em perícias, já que a expressão da magia está ligada
a elas. Há outros elementos — aspectos agem como portal de acesso a poder e os efeitos mágicos em si funcionam de forma similar às façanhas —, mas isto é basicamente um modelo para perícias aprimoradas.

### Descrição

O povo da Planície Sem Fim não se curva a ninguém exceto aos céus. Seu povo se espalha por toda a sorte de lugares, tanto que alguns dizem que esta verdade é a única coisa que ainda têm em comum. E talvez estejam certos — das torres abobadadas das Cidades Fluviais, através das Nações Nômades das Terras Hípicas e às vilas ambulantes das selvas do sul, o
Povo das Estrelas oferece suas orações e maldições a esses astros.

Com frequência, essas orações são voltadas aos Seis Vizires, as constelações que dançam em volta da corte da Imperatriz, cujo lugar no céu é sempre o mesmo. Cada uma delas carrega responsabilidades divinas e cada uma carrega pedidos honrosos à Imperatriz.

Embora todos concordem a respeito das constelações que compõem os Vizires e seus títulos, existem diversas opiniões em outros aspectos. De acordo com a região, eles são nomeados e representados de formas diferentes. Dependendo de onde você estiver, o Gigante pode ser descrito como um grande homem de pedra, uma donzela cujo machado carrega a fúria do inverno ou até mesmo um elefante. Os contos dos Vizires — sendo até mesmo estee nome pode variar — dizem muito sobre cada povo.

De tempos em tempos, alguém é abençoado por um dos Vizires e recebe uma marca cujo formato remete à constelação apropriada. A natureza dessa benção varia. Para uma família de Achinst, a primeira filha de uma família em particular sempre nasce com a marca do Gigante. Um mosteiro ocidental é dirigido por um escolhido do Soldado e dizem que esse manto é passado para aquele que o derrotar em combate. Histórias não faltam, mas não há como saber a verdade absoluta.

O que se sabe é que cada um dos abençoados ganha poder de acordo com o domínio do Vizir em questão. O temperamento do escolhido normalmente parece ser compatível com o Vizir que o escolhe, mas é incerto se isto é uma causa ou efeito da seleção.

### **Versão Resumida**

Se quiser começar logo sem ler tudo, faça o seguinte:

- Substitua a perícia Condução por Cavalgar.
- Reduza a Recarga em 1.
- Escolha um Vizir pelo qual você foi marcado (Observador, Gigante,    Sombra, Soldado, Conselheiro ou Aldeão) e crie um aspecto    **_Marcado pelo(a) [Vizir]_** (como em **_Marcado pelo Gigante_** ).
- Descreva onde fica a marca física no corpo de seu personagem.
- Quando usar a perícia associada à marca, seu esforço será mágico,     como atos sobre-humanos. Isso não se traduz em um bônus, mas    significa apenas um resultado geral mais impressionante, dependendo da situação.
  -     **+ Observador:** Investigar, Conhecimento, Notar
  -     **+ Gigante:** Atletismo, Vigor, Vontade
  -     **+ Sombra:** Roubo, Enganar, Furtividade
  -     **+ Soldado:** Lutar, Cavalgar, Atirar
  -     **+ Conselheiro:** Provocar, Comunicação, Recursos
  -     **+ Aldeão:** Contato, Ofícios, Empatia

### Mecânica

#### Perícias

Pressupõe-se um cenário fantástico, embora não necessariamente o padrão europeu. Como pode imaginar, é algo mais no espírito russo — em tudo — do que europeu. Em todo caso, isso requer que a perícia Condução seja substituída por Cavalgar, que funciona basicamente da mesma forma, mas com outro tipo de veículo.

#### A Marca Do Vizir

O seu personagem foi marcado por um Vizir, reduza sua recarga em 1. Personagens marcados por um Vizir precisam de um aspecto que reflita isso, como ___Escolhido pelo Conselheiro___ ou ___Marcado pelo Aldeão___. O nome exato do aspecto fica a cargo do jogadore, se o jogador possuir uma relação complicada com o Vizir, esta é uma ótima forma de representá-la. O significado exato do aspecto depende da marca, mas todas as marcas têm algumas coisas em comum. Primeiramente, o personagem recebe uma marca física em alguma parte do corpo, no formato da constelação do Vizir.

A forma exata e local variam — podem ser cicatrizes, marcas de nascença, uma tatuagem brilhante ou qualquer outra coisa —, mas o formato é consideravelmente consistente. Desenhos decorativos e marcas temporárias também são populares entre não-marcados em algumas regiões, mas um verdadeiro portador pode reconhecer outra marca genuína com um único olhar.

O marcado sempre sabe onde fica sua constelação, mesmo que seja dia ou se ela estiver além do horizonte. Por si só, não é muito útil para navegação, mas combinado com um pouco de conhecimento, pode permitir um estranho senso de direção.

Existem também elementos sociais em ser marcado, embora eles variem de um lugar para outro. Normalmente, ser marcado é algo bom, exceto quando a marca é de um Vizir indesejado em determinada região.

### As Marcas

Resumimos cada Vizir logo baixo. Algumas informações são autoexplicativas, como alguns termos e formas como os Vizires são representados. Outros elementos têm mais impacto nas regras. 

Cada marca possui uma virtude e um vício, que são relevantes ao uso da marca como aspecto. Por exemplo, o Gigante possui a virtude Força e seu vício é a Fúria. O aspecto Marcado pelo Gigante pode ser usado como se fosse o aspecto Força ou Fúria.

Cada marca também possui três “domínios” — as perícias ligadas a esse Vizir em particular. São eles que formam estrutura para as bênçãos — leia-se Façanhas — que os Vizires fornecem. Os domínios não possuem nenhum efeito mecânico além de esboçar a forma do domínio do Vizir — além de prover alguma orientação para jogos que usem listas de perícias modificadas.

As próprias bênçãos são elementos autossuficientes de regras. Personagens escolhem duas bênçãos de seu Vizir. Essas não custam pontos adicionais de recarga, mas lembre-se que o personagem já gastou um de recarga para poder ser um escolhido.

#### O Observador

+ **Virtude:** Atenção
+ **Vício:** Inatividade
+ **Também Conhecido Como:** O Auditor, O Inquisidor, O Sábio, O Espião ou O Vigia.
+ **Representado Como:** Uma figura sem sexo definido vestindo um manto, um magistrado, uma bibliotecária, uma coruja ou um olho. 
+ **Domínios:** Investigar, Conhecimento, Notar.

O Observador assiste e informa à Imperatriz. Ele vê tudo, e fornece a outros o conhecimento e percepção necessários para agir adequadamente. Ele mesmo raramente atua de forma direta. Em algumas histórias, o motivo é porque ele é um agente da lei — um investigador — que resolve um mistério para que as autoridades competentes possam agir. Em outros, ele está preso a um desejo de ser neutro ou por saber dos danos que poderia causar com suas próprias ações.

##### Bençãos

###### _Investigação_

**As Peças do Quebra-Cabeça:** Ao parar por alguns minutos para estudar um item em particular e sua posição, você pode reconstruir, razoavelmente, a cadeia de eventos que o levaram até ali. Essa reconstrução será precisa, mas não revelará nada além dos detalhes necessários. Por exemplo, pode revelar que foi carregado à mão em dado momento, mas não revela por quem.

**O Cofre do Olhar:** Você pode olhar para uma cena e recordá-la nos mínimos detalhes. Na prática, isso permite a você perguntar ao Narrador sobre essas memórias mesmo depois de muito tempo do fato ocorrido e fazer rolagens de Investigar com calma. Isso _inclui qualquer_ coisa que você poderia perguntar se estivesse naquele local e momento, como o conteúdo
de algum recipiente. Se a resposta para a questão exigir uma rolagem — como arrombar uma fechadura para ver o conteúdo de um baú — você pode realizar normalmente, como se ainda estivesse lá.

É possível armazenar mais de uma memória na sua mente, mas o custo disso é um ponto de destino por cena já memorizada.

###### _Conhecimento_

**A Benção das Mil Línguas:** Você pode aprender qualquer língua rapidamente. Se tiver um tutor, consegue em apenas um dia. Se apenas houver a oportunidade de ouvir e ler, uma semana. Se o material for realmente escasso, pode levar um mês.

**O Observador Vê Todos os Caminhos:** Você pode não saber tudo, mas sempre sabe como descobrir. Quando busca por uma informação consideravelmente específica, você pode dar ao Narrador um ponto de destino para descobrir o local mais próximo onde pode encontrá-la, independente do quão obscura ou perdida seja a informação. Em suma, você nunca fica sem saída quando quiser saber de algo. 

Não há garantia do quão fácil será obter a informação, mas é para isso que serve a aventura.

###### _Notar_


**O Observador Olha em Todas as Direções:** Você nunca é surpreendido. Mesmo que apenas por um momento, você sempre está prevenido para o inesperado.

**As Estrelas Iluminam a Noite:** Contanto que haja uma fonte mínima de luz, você pode ver como se fosse dia. Nos raros casos de escuridão completa, você pode enxergar tão bem como se possuísse uma fonte de luz.

#### O Gigante

+ **Virtude:** Força
+ **Vício:** Fúria
+ **Também Conhecido Como:** O Terremoto, O Trabalhador, O Pilar, O Titã.
+ **Representado Como:** Uma estátua de pedra, uma donzela gélida, um ogro, um elefante ou um touro.
+ **Domínios:** Atletismo, Vigor e Vontade.

O Gigante representa mãos fortes prontas para o trabalho, mas também representa a força descomunal. Na maioria das vezes isso significa força física, mas vai mais além disso. É dito que o Gigante é quem move os céus a mando da Imperatriz. Nos contos, o Gigante frequentemente é apresentado como bem-intencionado e poderoso, mas nem sempre tem controle do poder em suas mãos. Ele muitas vezes serve um papel secundário com outro Vizir — frequentemente o Conselheiro ou o Observador —, agindo a serviço de consciências maiores.

##### Bençãos

###### Atletismo

**Minhas Mãos Movem o Mundo:** Através de uma combinação de velocidade, destreza e ritmo excepcional, você sempre tem um caminho à sua frente. Em um ambiente estático, você é capaz de realizar manobras acrobáticas ousadas para chegar a quase todo lugar que possa ser acessado fisicamente. Em um ambiente mais fluido é impossível segurá-lo ou cercá-lo, já que você sempre encontra uma brecha.

**Passo do Gigante:** Você corre rápido como um cavalo, possui um salto vertical igual a sua própria altura e pode correr por um dia e uma noite sem precisar de descanso — porém, precisará dormir e comer bastante depois disso.

###### Vigor

**Apetite do Gigante:** Você pode comer _qualquer coisa_ sem sofrer danos. Não apenas alimentos — se puder mastigar e engolir, ou beber, você poderá ingerir com segurança e até mesmo se alimentar daquilo. Venenos, decomposição, cacos de vidro e outras inconveniências são ignoradas. Como um bônus, sabores são bem distintos e memoráveis para você, o que permite truques repugnantes como comparar o sabor de amostras de sangue para saber se vêm da mesma fonte, assim como truques mais úteis, como identificar um veneno familiar.

**Nada Pode Deter o Gigante:** Se estiver amarrado ou acorrentado, você consegue quebrar as amarras, contanto que sejam naturais ou fabricadas. Nenhuma porta ou trava pode aguentar mais que um golpe seu. Barreiras sem abertura demoram mais, mas usando apenas punhos, pés e o que puder carregar, você equivale a uma equipe de demolidores completa.

###### Vontade

**A Mente É a Maior Montanha:** No que se refere a perícias sociais, você não existe. Você não pode ser influenciado, amigado, intimidado ou con- vencido de qualquer outra forma. Sua fala não revela nada sobre você ou a veracidade de suas palavras. Para fins das habilidades do Aldeão, seu nível em Enganar é maior que a Empatia do Aldeão.

**Nunca Derrotado:** Você ganha uma caixa de consequência física de -8, que é recuperada da mesma forma que a consequência de -2.

#### A Sombra

+ **Virtude:** Sigilo
+ **Vício:** Ganância
+ **Também Conhecida Como:** O Assassino, O Espião, O Captor, O Ladrão, O Trapaceiro.
+ **Representada Como:** Uma figura encapuzada de qualquer sexo, o vento noturno, uma sombra humanoide, uma cobra, um rato ou um corvo.
+ **Domínios:** Roubo, Enganar e Furtividade

Dependendo da hora e local, a Sombra pode ser tanto um ladrão trapaceiro quanto uma ameaça sinistra e ambas as visões são um pouco verdadeiras. Agindo como a mão invisível da Imperatriz, a Sombra é responsável pelas coisas necessárias que não deveriam ser ditas. Tratar dessas coisas é sempre incerto e indesejável, até o dia em que a necessidade surge; quando então são profundamente bem-vindas, de fato.

##### Bençãos

###### Roubo


**O Zelo com as Miudezas:** Depois de ser bem-sucedido em roubar algo pequeno o suficiente pra caber em seu bolso, o item está desaparecido até que você o revele novamente. Nenhum tipo de procura — nem mesmo uma revista completa — revelará o item furtado. Você só pode ter um item escondido dessa maneira por vez.

**A Súplica das Fechaduras:** Basta sussurrar seu nome em uma fechadura para tentar abri-la, como se estivesse usando suas ferramentas. Se bem-sucedido, a ação demora apenas um momento. Se não tiver êxito, você ainda poderá tentar novamente na forma tradicional.

###### Enganar

**Corroboração da Coincidência:** O destino favorece suas mentiras com pequenas coincidências e evidências circunstanciais que tendem a lhe dar credibilidade. Você pode usar um impulso para a cena antes de sua rolagem de Enganar, desde que possa descrever como isso lhe ajuda a parecer mais honesto. Se bem-sucedido, o impulso se transforma em um aspecto de cena.

**O Nome É uma Máscara para o Mundo:** Sempre que ouvir o nome de alguém dito pelos próprios lábios da pessoa, você pode duplicar seu rosto, voz e jeito até que o sol tenha nascido duas vezes. Você nunca pode imitar a mesma pessoa duas vezes.

###### Furtividade

**Apenas o Vento Me Vê Sair:** Ao custo de um ponto de destino, você pode sair de uma cena, passando despercebido.

**Roubando Palavras ao Vento:** Você não faz barulho quando não deseja ser ouvido. Fazer permite a você não só se mover em silêncio absoluto, mas também pode ser usado de forma seletiva, como falar de forma que apenas uma pessoa possa lhe ouvir.

---

#### O Poder Dos Nomes

Mesmo aqueles que não entendem completamente a natureza da Sombra, entendem que dar a um estranho o seu nome é um gesto de confiança. O quanto as pessoas se preocupam com isso varia de cultura para cultura, mas normalmente é algo ao menos
levado em consideração.

---

#### O Soldado

+ **Virtude:** Disciplina
+ **Vício:** Servidão
+ **Também Conhecido Como:** O Cavaleiro, A Espada e O Senhor da Guerra.
+ **Representado Como:** Um guerreiro culturalmente adequado de qualquer sexo, uma arma, um leão ou um tigre.
+ **Domínios:** Lutar, Cavalgar e Atirar.

O Soldado serve através da violência e da guerra, com virtudes de aço. O Soldado valoriza a astúcia, bravura e lealdade, mas pode ser facilmente conduzido. Histórias heroicas do Soldado falam sobre batalhas lutadas e vencidas, mas outras histórias o põem contra os heróis, pelo simples motivo de seguir ordens cegamente.

##### Bençãos

###### Lutar

**Um Exército no Fio da Minha Lâmina:** Você não recebe penalidades — mas também nenhum bônus — por estar em desvantagem numérica, não importa o quão absurda seja a diferença.

**Lanças de Madeira Verde:** Você pode treinar uma tropa — personagens sem nome em um grupo de até cerca de 100 unidades — por uma semana e melhorar suas perícias Lutar em +1. Você pode repetir isso várias vezes, melhorando qualquer unidade até um nível máximo igual ao seu nível de Lutar -2.

###### Cavalgar

**Apenas o Vento Sob Nós:** Enquanto mantiver uma montaria firme, qualquer cavalo que montar consegue cavalgar sobre água como se fosse solo firme, podendo até cavalgar no ar por algumas centenas de metros — a descida em seguida é semelhante a cavalgar por um declive suave. 

**Montamos Como Um:** Montado, você luta e age sem penalidades e nada pode derrubá-lo. A qualquer momento em que receber uma consequência física, você pode optar que o cavalo a receba em seu lugar — o cavalo possui caixas de estresse semelhantes às de seu personagem.

###### Atirar

**Ao Horizonte:** Qualquer coisa que puder ver é considerado efetivamente como se estivesse a uma zona de distância quando você atira.

**Às Estrelas:** Qualquer projétil disparado ao ar pode pousar em qualquer lugar que você conheça ou próximo a alguém cujo nome você saiba. Mensagens e pequenos itens podem ser entregues dessa forma. Isso não pode ser usado para atacar diretamente, mas se o tiro for dado com a intenção de ferir, é possível matar algum cavalo ou PdN sem importância.

#### O Conselheiro

+ **Virtude:** Liderança
+ **Vício:** Teimosia
+ **Também Conhecido Como:** Avô ou Avó.
+ **Representado Como:** Um ancião sábio de qualquer sexo, um grande cão ou uma serpente.
+ **Domínios:** Provocar, Comunicação e Recursos.

O Conselheiro é o ouvido ao qual todos falam e a voz que todos ouvem. Enquanto a Imperatriz pode governar as estrelas, o Sábio é aquele que faz o dia a dia acontecer. Nas histórias, ele atua mais como assessor ou líder do que como herói, embora às vezes o Conselheiro assuma o papel do viajante perspicaz, ensinando às comunidades que visita lições que já
deveriam saber.

##### Bençãos

###### Provocar

**Coroa Ameaçadora:** Você é muito aterrorizante para ser atacado. Até que realize um ataque físico em uma cena, personagens com Vontade abaixo de Bom (+3) simplesmente não podem lhe atacar. Aqueles com Vontade suficiente ainda vacilam em seu primeiro ataque, errando automaticamente. 

**Caminhando com as Tempestades:** O clima de uma cidade — ou local de tamanho similar — será como você desejar.

###### Comunicação

**Cada Coisa em Seu Lugar:** Você sempre sabe a dinâmica de poder de um ambiente e pode se inserir em qualquer lugar que desejar. Use com cautela — embora isto afete como as pessoas interagem com você, não equivale a autoridade real, e se colocar em uma posição muito alta — especialmente acima de pessoas acostumadas a estar acima de tudo — pode causar rea-
ções desagradáveis.

**Quem Você é de Verdade:** A cada dois minutos de conversa que tiver com alguém, este revela um de seus aspectos. No entanto, para cada dois aspectos aprendidos, você revela um seu a essa pessoa e todos que estiverem ouvindo (arredondado para baixo, então o primeiro aspecto descoberto é gratuito).

###### Recursos

**Rios de Ouro:** Dinheiro não passa de um detalhe para você. Mesmo despido e lançado em uma ilha deserta, em pouco tempo estará vivendo no luxo novamente. Se lançado na prisão, subornará os guardas em momentos. Nenhuma situação restringirá seu acesso a sua perícia Recursos.

**Guerra no Papel:** Você pode tomar medidas contra organizações por meios indiretos. Na prática, é possível lutar no mesmo nível de qualquer organização que seja menor que uma nação, sem a necessidade de recrutar aliados ou possuir sua própria organização. Sim, isso significa que você pode efetivamente “matar” uma cidade ou mesmo um exército se tiver tempo suficiente.

#### O Aldeão

+ **Virtude:** Resistência
+ **Vício:** Tacanho
+ **Também Conhecido Como:** O Peão ou o Cidadão.
+ **Representado Como:** Um fazendeiro, uma moça leiteira, um remador, um martelo, um arado, uma mula ou um macaco.
+ **Domínios:** Contatos, Ofícios e Empatia.

Enquanto o Conselheiro é um líder perspicaz, o Aldeão representa a sabedoria e virtudes dos cidadãos comuns. Suas histórias são aquelas com a pessoa que aparenta ser tola, mas que no fim triunfa por seus valores simples e convencionais. Ele é a força da comunidade.

##### Bençãos

###### Contatos

**Os Laços de Um Homem Vão Além do Horizonte:** Não há nenhum lugar onde você não conheça alguém, incluindo lugares onde nunca esteve antes. Haverá um amigo onde quer que vá.

**A Força de Um Torna-se a Força de Muitos:** Assim que começar a criar contatos, esse esforço se torna autossuficiente; você fala com pessoas, estas falam com outras e estas falam com outras. Na prática, você sempre conseguirá a resposta para suas dúvidas; é apenas questão de tempo.

###### Ofícios

**A União da Forma:** Você pode combinar materiais de formas impossíveis, dando a um os atributos do outro. Você pode fazer papel com a resistência do aço ou o aço com a leveza do papel. Isso exige que dois aspectos — um para o material e outro para o atributo adicional — sejam invocados simultaneamente na rolagem de Ofícios para criar o produto final.

**As Mãos São As Maiores Ferramentas:** Você pode produzir um trabalho manual de mestre com a mais improvisada das ferramentas. Com uma oficina adequada, você pode criar incríveis dispositivos no melhor estilo Da Vinci.

###### Empatia

**Arquitetura do Coração:** Ler os comportamentos em salão inteiro é trivial para você. Mais que isso, você pode perceber emoções-chave em um ambiente e facilmente entender como suas ações afetariam o humor geral. Se fizer uma rolagem de Empatia para ler uma sala — dificuldade Medíocre (+0) — conte quantas tensões você acumula. Ao longo da cena, você pode perguntar ao Narrador como as pessoas reagiriam para cada situação hipotética. Isso pode ser feito uma quantidade de vezes igual à quantidade de tensões conseguidas.

**O Olho Interior Enxerga a Verdade Interior:** Sem a necessidade de uma rolagem, você pode dizer se outro personagem está mentindo, contanto que o nível de Enganar do alvo seja igual ou menor que sua Empatia. Caso o alvo tenha um alto nível em Enganar, você sabe que não pode distinguir, mas nunca recebe um “falso positivo”.

### Evolução

Se um personagem for bem-sucedido em alguma grande missão que se enquadre no caráter de seu Vizir — nada pequeno, mas sim uma tarefa que pode ser o centro de toda uma campanha — ele pode pegar um segundo aspecto ligado ao seu Vizir. Ele perde outro ponto de recarga e escolhe mais duas bênçãos.

### Variações e Opções

Como base, este é um modelo de poderes bastante mítico. Aqueles marcados pelos Vizires são heróis lendários, mas há diversas opções sobre como alterar isso e criar outros efeitos.

#### Nível De Poder

A maneira mais fácil de reduzir o poder das bênçãos é por fazer com que elas exijam o uso de um ponto de destino. Se desejar aumentar seu poder, remova o custo de recarga e simplesmente permita aos jogadores escolher mais bênçãos — ou quem sabe até mesmo possuir mais de uma marca.

#### Estrutura

Seis é um número bastante arbitrário de Vizires. As mesmas bênçãos/façanhas baseadas em perícias podem ser redistribuídas de acordo com outro esquema. Talvez, por exemplo, haja dezoito equivalentes Arcanos maiores, cada um ligado a uma perícia — personagens ligados a cada um dos Arcanos recebem as bênçãos dessa perícia. Por outro lado, os Vizires poderiam ser totalmente descartados, permitindo que os jogadores simplesmente escolham uma certa quantidade de bênçãos.

#### Vizires Ativos

Se os Vizires forem realmente presentes e ativos, o tom do jogo muda completamente. Eles não apenas interagirão mais diretamente com seus escolhidos, mas também estarão envolvidos em suas próprias questões políticas. Vizires ativos podem ser uma forma poderosa de botar o jogo em movimento.

#### Outros Vizires

Talvez existam outros Vizires por aí — constelações secretas com poderes e conhecimentos não revelados ao mundo em geral. Talvez sejam inimigos ocultos ou aliados perdidos; talvez ambos.

#### Vizires Malignos

Os Vizires da forma que foram apresentados são, no geral, forças positivas, com seus elementos negativos servindo principalmente como consequências naturais de seus pontos fortes. Entretanto, cada Vizir poderia vir acompanhado de um reflexo sombrio; uma entidade que abraça sua natureza maligna e cujas poucas características positivas sirvam como extensão dessa natureza. Esses Vizires Sombrios podem ter seus próprios planos, escolhidos e, o melhor de tudo, não há garantia de que aqueles que ostentam a marca sequer tenham consciência de qualquer diferença.


------

- [« Senhores das Tempestades](../senhores-das-tempestades/)
- [A Arte Sutil »](../arte-sutil/)
