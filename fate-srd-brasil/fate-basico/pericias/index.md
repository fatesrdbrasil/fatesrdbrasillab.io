---
title: Perícias
layout: default
---

## Perícias

**Selecione as perícias e o nível de cada uma.**

Uma vez que tenha passado pelas três fases e selecionado os aspectos, é hora de escolher as perícias. Você encontrará uma descrição de cada uma das perícias em [*Perícias e Façanhas*](../pericias-e-facanhas/).

Suas perícias formam uma pirâmide, com uma em nível Ótimo (+4) – ou perícia ápice – e outras em nível menor até a mais leve em nível Regular (+1). Você possui:

-  Uma perícia Ótima (+4)
-  Duas perícias Boas (+3)
-  Três perícias Razoáveis (+2)
-  Quatro perícias Regulares (+1)

Medíocre (+0) é o nível padrão para as perícias que você não possui. Às vezes uma perícia não estará disponível se não for selecionada. Nesses casos, ela não possui nem mesmo um nível medíocre.

------

### Veteranos

### Por Que A Pirâmide de Nível?

Se você jogou *The Dresden Files RPG* já sabe que usamos uma coluna ao invés da pirâmide de nível.

Nesta versão do Fate queremos que a criação de personagem seja a mais rápida possível, então criamos esse padrão. Se preferir usar a coluna, vá em frente, você tem 20 pontos de perícia.

Não descartamos esse método completamente, ele só foi deixado para [personagens mais experientes](../evolucao-e-alteracao/#a-coluna-de-perícias).

------

------

### O Topo da Pirâmide

Por padrão, usamos Ótimo (+4) como o nível de perícia mais alto na criação. Personagens avançados podem ir além disso, mas é mais difícil do que melhorar perícias abaixo desse valor (veja mais em [*Marco Maior*](../evolucao-e-alteracao/#marco-maior)).

Se você está jogando uma partida sobre super-heróis, criaturas paradimensionais, deuses míticos ou super-humanos, sinta-se livre para definir o maior nível inicial como Excepcional (+5) ou Fantástico (+6).

O número de perícias que você recebe deve ser proporcional ao tamanho da lista de perícias. Nossa lista padrão possui 18 perícias e a pirâmide padrão permite uma graduação em 10 delas, o que indica que os personagens são habilitados em mais da metade do total de coisas que podem ser realizadas, enquanto ainda há espaço para que 6 PJs tenham perícias diferentes no ápice (escolhendo suas três perícias mais fortes). Você pode ajustar isso para jogos específicos, principalmente se for modificar a graduação máxima inicial. Apenas lembre-se que quanto maior o nível inicial, mais perícias repetidas, a não ser que você possua uma lista grande de perícias.

------

> Michel sabe que Bandu não é como os outros PJs quando se fala de perícias, então ele tenta visualizar Bandu o mais excêntrico possível, se comparado aos outros personagens. O grupo já conversou e decidiu que o poder dele usará sua perícia Conhecimentos, então ele naturalmente focará nisso. Amanda também poderia criar poderes como Extras, mas preferiu essa opção pra facilitar.
>
> Ele seleciona Conhecimentos como perícia primária, Ofícios e Comunicação – para um mentalista, Bandu se considera capaz de se comunicar bem. Michel pega Atletismo, Vontade e Investigar, pois ele acredita que Bandu precisará disso como mercenário, e um monte de outras habilidades porque seus companheiros não possuem ou porque ele poderá se virar caso o grupo se separe. Para finalizar ele escolhe Lutar, Recursos, Contatos e Percepção.

**Nota:** algumas perícias possuem alguns benefícios, como algumas que podem afetar as caixas de estresse e adicionar consequências. Se você acha que precisará delas, coloque-as no topo de sua pirâmide.

- [« As Três Fases](../tres-fases/)
- [Façanhas e Recarga »](../facanhas-e-recarga/)
