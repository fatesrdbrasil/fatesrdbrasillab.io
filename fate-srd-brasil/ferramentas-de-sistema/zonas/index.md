---
title: Zonas
layout: default
---

## Zonas

Mapear as zonas durante um conflito permite uma melhor visualização do espaço físico de uma luta, criando a oportunidade de dominar o campo de batalha para personagens com altos níveis em Atletismo e Físico. Aqui seguem algumas ideias para tornar as zonas uma parte emocionante do seu jogo:

### Movimento Entre Zonas

Em um jogo com muitas zonas, você talvez queira permitir que os personagens sacrifiquem seu turno para moverem-se um número de zonas igual a seu nível de Atletismo ou para remover um número de obstáculos físicos igual a seu nível de Vigor. Personagens rápidos e fortes querem ser rápidos e fortes, mas rolar os dados nem sempre é a melhor forma de representar isso em Fate.

### Zonas Estreitas Podem Ser Dramáticas

Enquanto as zonas dividem grandes áreas como estacionamentos e estádios — mostrando o quão difícil é atravessar um campo de futebol correndo — elas também podem ser usadas para dividir lugares pequenos de formas interessantes. Por exemplo, uma luta em um navio pode acontecer nos alojamentos, o que exigiria que os aliados atravessassem várias zonas para ajudar um amigo em apuros. Ao forçar os jogadores a escolher entre gastar ações se movendo ou oferecer ajuda a alguém distante, as zonas podem tornar um conflito corriqueiro em algo bastante dramático.

### Aspectos de Zonas Perigosos

Zonas também podem criar emoção ao restringir movimento e prover desafios que os personagens precisem superar. Por exemplo, uma zona em um campo de batalha pode possuir o aspecto ___Sob Fogo Pesado___, exigindo que os personagens realizem rolagens de Atletismo para evitarem receber dano conforme correm através do tiroteio. Algumas zonas também podem desaparecer após um número específico de turnos. Pontes em ruínas, barcos naufragando e portas fechando impulsionam os personagens a se mover rápido à medida que o campo de batalha muda ao seu redor. Além disso, dão aos jogadores a chance de forçar os PdNs para essas zonas, de forma que tenham que também tenham que lidar com as ameaças.

### Zonas Mentais e Sociais

Nem todos os conflitos acontecem no mundo físico; personagens em Fate frequentemente adentram conflitos sociais e mentais que podem ser mapeados de formas interessantes. Por exemplo, um cirurgião psíquico pode se orientar através dos sonhos de um paciente, obstruídos por aspectos de situação que devem ser superados através de uma série de rolagens de Investigação e Empatia. As zonas podem detalhar os obstáculos que impedem o paciente de acessar antigas memórias, vestígios de trauma e abuso que os PJs devem enfrentar antes de poder ajudar o paciente a superar o seu passado. Da mesma forma, um Narrador pode detalhar diversos grupos sociais diferentes em uma universidade, indicando qual grupo os PJs devem impressionar antes de ganhar acesso aos alunos mais populares. Em essência, estas zonas mentais e sociais servem para restringir
os jogadores, direcionando-os aos conflitos ao limitar seu movimento.


------

- [« Consequências](../consequencias/)
- [Recarga »](../recarga/)
