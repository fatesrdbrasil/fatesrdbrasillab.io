---
title: Começando a Jogar!
layout: default
---

## Começando a Jogar!

Esta é a base necessária para jogar uma partida de Fate. Os capítulos a seguir entrarão em mais detalhes sobre o que falamos acima e mostrarão como estruturar o seu jogo.

------

### O Que Ler a Partir Daqui

-  Você provavelmente vai querer começar pelo capítulo [*Estruturando o Jogo*](../estruturando-o-jogo/), que o ajudará a montar o jogo em si, passando em seguida para o capítulo de [*Criação de Personagens*](../criacao-de-personagens/).
-  Jogadores, vocês podem querer ler [*Ações e Resoluções*](../acoes-e-resolucoes/) e [*Construindo Campanhas*](../construindo-campanhas/). Esses capítulos ensinam o passo a passo de como fazer as coisas e desenvolver seus personagens durante o jogo.
-  Narradores podem querer se familiarizar com todo o livro, mas [*Conduzindo o Jogo*](../conduzindo-o-jogo/) e [*Cenas, Sessões e Cenário*](../cenas-sessoes-e-cenario/) é particularmente importante.

------

- [« Realizando Ações](../realizando-acoes/)
- [Estruturando o Jogo »](../estruturando-o-jogo/)
