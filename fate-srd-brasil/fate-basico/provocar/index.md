---
title: Provocar
layout: default
---

### Provocar

Provocar é a perícia para incomodar os outros e incitar respostas emocionais negativas - medo, raiva, vergonha, etc. É a perícia de "ser desagradável".

Para usar provocar, você precisa de algum tipo de justificativa. Ela pode ou vir da situação, ou porque você possui um aspecto apropriado, ou por ter criado uma vantagem com outra perícia (como Comunicação ou Enganar), ou mesmo por ter avaliado os aspectos de seu alvo (veja Empatia).

Esta perícia necessita que seu alvo possa sentir emoções – robôs e zumbis normalmente não podem ser provocados.

- `O`{: .fate_font} **Superar:** Você pode provocar alguém para que perca a razão e faça algo que você quer, intimidar para obter informações, irritar a ponto fazê-los reagir ou mesmo assustar alguém, fazendo-o fugir da cena. Isso acontecerá com mais frequência quando você encontrar PdNs menores ou quando não for importante definir os detalhes. Contra PJs ou PdNs importantes, será necessário uma competição contra a perícia Vontade do alvo.
- `C`{: .fate_font} **Criar Vantagem:** Você pode criar vantagens que representem estados emocionais momentâneos, como ***Enfurecido***, ***Chocado*** ou ***Hesitante***. Seu alvo se defende usando a perícia Vontade.
- `A`{: .fate_font} **Atacar:** Você pode realizar ataques mentais com Provocar para causar dano emocional a um oponente. Sua relação com o alvo e as circunstâncias do momento são muito importantes ao determinar se é possível ou não usar esta perícia.
- `D`{: .fate_font} **Defender:** Você precisa de Vontade para isso. Ser bom em provocar outros não o ajuda a se defender.

#### Façanhas Para Provocar

-  **Armadura do Medo:** Você pode usar Provocar para se defender de ataques realizados com a perícia Lutar, mas apenas até o momento que receber algum estresse no conﬂito. É possível deixar seu oponente hesitante em atacar, mas quando alguém os mostrar que você também é humano, a vantagem desaparece.
-  **Incitar Violência:** Ao criar uma vantagem sobre um oponente usando Provocar, você pode usar sua invocação grátis para se tornar o alvo da próxima ação relevante daquele personagem, tirando a atenção dele de outro alvo.
-  **Tá Bom Então!** Você pode usar Provocar no lugar de Empatia para aprender sobre os aspectos de seu alvo, por provocar até que ele revele algum. O lvo pode se defender usando Vontade (se o Narrador achar que o aspecto em si é vulnerável à provocação, você recebe um bônus de +2).

- [« Percepção](../percepcao/)
- [Recursos »](../recursos/)
