---
title: Senhores das Tempestades
layout: default
---


## Senhores das Tempestades

### **Notas de Criação**

Fãs de _The Dresden Files RPG_ perceberão algumas similaridades entre estas regras e as regras de Evocação de DFRPG. Não é coincidência.

Este é um sistema estruturado, em sua maior parte, com um pouco de liberdade interpretativa dentro dos limites estabelecidos. Ele depende da perícia “Tempestade” e ao menos um aspecto. A fonte da magia é bem explicita, mas seu uso é limitado. Do jeito que está, ela é equilibrada mesmo se apenas um jogador escolher jogar como um Senhor da Tempestade, mas também comporta vários num mesmo grupo. 

Por padrão, é assumido que personagens que não sejam Senhores da Tempestade usem as regras opcionais de valores de [Armaduras e Armas](../../fate-basico/mais-exemplos-de-extras/#potência-de-armas-e-armaduras) Se você busca equilibrar um Senhor da Tempestade no meio de outros personagens comuns, o Senhor da Tempestade não usa esses bônus. Se o seu jogo não usa as regras de valores para Armaduras e Armas, aumente o custo de Recarga de 1 para 2.

### **Descrição**

As Cinco Grandes Tempestades se enraivecem no coração da criação, cada uma grande o bastante para partir estrelas, mas mantidas sob controle em uma dança precisa de criação e destruição. Terremoto, Dilúvio, Geleira, Inferno e Trovão, cada uma representa as fontes ilimitadas de poder, e feiticeiros mortais encontraram uma forma de explorar essas tempestades para alimentar suas próprias ambições.

### **Versão Resumida**

Não quer ler todas as regras? Use esta versão resumida:

- Se o seu jogo usa as regras de valores de Armadura/Arma, reduza a Recarga em 1. Se não, reduza a recarga em 2.
- Escolha um tipo de Tempestade entre Terremoto, Inundação, Geleira, Inferno e Trovão e crie um aspecto **_Senhor do(a) [tipo]_** ,    por exemplo **_Senhor do Terremoto_** , **_Senhor da Avalanche_** , etc. 
- Compre a perícia Tempestade.
- Use a perícia Tempestade para atacar, defender e criar vantagem,    desde que a descrição de sua ação inclua o elemento do seu tipo    de tempestade.

### Mecânica

Personagens em busca do poder da Tempestade deverão fazer o seguinte:

- Se o seu jogo usa as regras para valores de Armadura/Arma, reduza    a Recarga em 1. Se não, reduza em 2.
- Escolha um aspecto que reflete com qual Tempestade o personagem tem sintonia: Terremoto, Dilúvio, Geleira, Inferno e Trovão.     Isso pode ser tão simples quanto “Sintonia com Terremoto”, mas     não se limita apenas a isso. Contanto que o aspecto mostre clara    mente com qual tempestade o personagem possui vínculo, o termo    em si pode variar.
- (Opcional) Comprar níveis maiores na perícia Tempestade.

### Aspectos De Tempestade

Aspectos de Tempestade serão bastante úteis quando realizar rolagens da perícia Tempestade, mas também carregam parte da sintonia com sua tempestade específica. Isso assume a forma de efeitos passivos, bem como coisas específicas pelas quais aquele aspecto pode ser forçado ou invocado.

#### Terremoto

Terremotos derrubam montanhas e fazem brotar outras novas. Para usar o poder do Terremoto é preciso uma profunda estabilidade pessoal e embora isso pode promover a força, também pode tornar um pouco mais difícil de ganhar embalo.

+ __Efeito Passivo:__ O personagem nunca perde o equilíbrio, não importa quão precária seja a situação, a menos que seja derrubado ativamente.
+ __Invocar:__ Resistir — Qualquer ação que dependa de paciência, determinação ou persistência pode se beneficiar de Terremoto.
+ __Forçar:__ Atraso — Quando uma ação rápida for necessária, Terremoto pode forçar um atraso.

#### Dilúvio

O Dilúvio não pode ser contido. Ele ataca de todas as direções com uma força esmagadora, sutileza ou com uma paciência infinita, sempre em conformidade com as necessidades da situação. Nada resiste ao Dilúvio e a única esperança é seguir com ele e esperar o melhor. Aqueles que possuem sincronia com ele compartilham de um pouco dessa flexibilidade.

+ __Efeito Passivo:__ Enquanto for possível nadar, o personagem pode se manter flutuando facilmente por quanto tempo for necessário, até mesmo dormindo na água.
+ __Invocar:__ Flexibilidade — Ao fazer algo ousado, como usar uma perícia para algo fora do comum, use isso como um bônus.
+ __Forçar:__ Desordem — A água é sútil e potente, mas também faz a maior sujeira. Forçar deixa traços de sua passagem nos momentos mais inconvenientes.

#### Geleira

Onde houver terra, a Geleira estará por perto, inevitável e inflexível, estilhaçando-se mil vezes até superar o que estiver em seu caminho. A sintonia com a Geleira concede parte dessa inevitabilidade ao Senhor da Tempestade.

+ __Efeito Passivo:__ Baixas temperaturas dentro do normal não incomodam o personagem.
+ __Invocar:__ Empurrão — Seja para abrir uma porta ou para tirar um serviçal do caminho, o personagem se beneficia quando se move adiante enquanto tira as coisas de seu caminho.
+ __Forçar:__ Esforço Demasiado — A Geleira não consegue ser tão flexível, e o Senhor da Avalanche pode se pegar dedicando muito esforço numa mesma atividade ou raciocínio.

#### Inferno

O Inferno consome. Seu apetite é interminável e não há nada que não seja combustível para suas chamas infinitas e turbulentas. 

+ __Efeito Passivo:__ Temperaturas altas dentro da normalidade não incomodam o personagem.
+ __Invocar:__ Destruir — Não combater ou ferir, destruir. O Inferno só deseja isso, nada mais, nada menos.
+ __Forçar:__ Consumir — Recursos, comida, boas opiniões e fortuna, um Senhor do Inferno possui o mau hábito de usá-los sem pensar duas vezes.

#### Trovão

Aqueles que diferenciam o Trovão dos relâmpagos mostram que não possuem real compreensão. O Trovão é a expressão súbita e poderosa da força, seja o raio que corta o céu ou o som que o apresenta. Aqueles que o invocam compartilham de sua potência.
+ __Efeito Passivo:__ Sua voz é forte. Se você consegue ver alguém bem o suficiente para identificá-lo, você pode gritar alto o suficiente para ser ouvido por ele — e todos no caminho — independente das condições.
+ __Invocar:__ Agir Decisivamente — Quando for necessária uma ação rápida devido a uma mudança de circunstâncias — e não apenas numa rodada após outra em um combate — então invoque isto por um bônus.
+ __Forçar:__ Oprimir — Às vezes sensibilidade, sutileza e precisão são necessárias. Às vezes um Senhor da Tempestade subestima isso.

### A Perícia Tempestade

A perícia Tempestade é usada para invocar o poder das tempestades para todo tipo de atividade — frequentemente violenta. A forma exata que a invocação toma depende do tipo de tempestade que está sendo chamada, mas, em geral, o Senhor da Tempestade dá forma à energia da tempestade em questão e em seguida a molda à sua vontade. Isso pode tomar forma de uma jaula de raios, uma estaca afiada de gelo, uma onda de força através da terra ou qualquer coisa que o jogador consiga imaginar.

Embora existam regras específicas e limitações sobre o que um Senhor da Tempestade pode fazer baseado no tipo de tempestade que esteja invocando, elas têm algumas características em comum.

Em cada caso, a força chamada deve ser expressa de __forma externa__ ao personagem que está invocando, de uma forma __literal__. Isso quer dizer que a perícia Tempestade __não concede__ ao seu usuário a “Força da Terra” para realizar um golpe poderoso, mas permite que acerte algo com BASTANTE força usando uma pedra. Qualquer descrição de efeito deve girar em torno de como a invocação, projeção ou manipulação grosseira da força em questão poderia alcançar o efeito descrito. 

+ `O`{: .fate_font} __Superar:__ A perícia Tempestade tende a ser um pouco grosseira em todas ações de superar exceto as mais diretas, como derrubar as coisas em seu caminho. Por outro lado, ela definitivamente é boa nisso. 
+ `C`{: .fate_font} __Criar Vantagem:__ Esta é uma ação comum dentro da perícia Tempestade, por conjurar paredes de fogo ou abrir poços no chão. As tempestades mais sólidas — Terremoto e Geleira — tendem a ser mais fortes nesse tipo de efeito, já que seus resultados tendem a ser mais duráveis. 

A maioria dos efeitos pode ser tratada como uma rolagem normal de criar vantagem, usando um aspecto na cena para refletir tal vantagem, mas há alguns casos especiais. Especificamente, a perícia Tempestades pode ser usada para criar uma barreira do elemento apropriado. Nesse caso, o conjurador escolhe duas zonas e faz uma rolagem contra uma dificuldade 0. O resultado da rolagem indica a dificuldade de superar a barreira criada.

Outros efeitos de vantagem dependem do elemento específico que foi invocado.

+ `A`{: .fate_font} __Atacar:__ Todas as Tempestades são boas nisso. Como regra básica você pode realizar um ataque dentro de sua zona sem penalidades, com -1 por zona de distância. Estes são ataques normais, mas talvez haja efeitos adicionais dependendo da Tempestade utilizada.
+ `D`{: .fate_font} __Defender:__  Os elementos também podem ser usados na defesa de ataques, aparando com armas de gelo ou erguendo uma parede de água temporária para interceptar um golpe. Os detalhes dependem do tipo de Tempestade usado.

---

### Barreiras

Barreira é um termo para alguma espécie de obstáculo entre uma zona e outra, como uma parede de gelo. Quando um perso-
nagem tenta penetrar, contornar ou destruir a barreira, o valor da barreira é a dificuldade para conseguir realizar a ação.

Em geral, uma barreira existe entre duas zonas, mas é possível que uma barreira possa ser mais longa, até mesmo rodeando uma zona por completo. A criação de uma barreira é um caso especial de criar vantagem usando a perícia Tempestade. Ela cria uma barreira igual à rolagem da perícia. Logo, se o personagem rolou Bom (+3), então o valor da barreira será 3. Um personagem que obtém uma rolagem inferior a +1 não consegue formar uma barrei- ra eficaz. Cada tipo de tempestade pode modificar a rolagem ou oferecer opções extras.

Quando um ataque é feito através de uma barreira, o defensor pode usar o valor da barreira em lugar de uma rolagem de defesa. O defensor deve decidir isso antes de rolar os dados e usar a barreira abre mão da possibilidade de um sucesso com estilo. Se o ataque incluir uma tentativa de atravessar a barreira — saltando sobre ela, por exemplo — então o atacante usa a menor perícia entre as duas envolvidas (a perícia para superar a barreira e a perícia de ataque) para realizar a tentativa (a menos, é claro, que um aspecto apropriado seja aplicado para facilitar a tentativa de superar).

Por exemplo, uma parede de gelo (Barreira Ótima +4) é lançada entre um Senhor da Tempestade e um Senhor do Vazio na zona adjacente. Se o Senhor do Vazio lançar um raio de sombras, o Senhor da Tempestade pode deixar os dados de lado e utilizar um valor efetivo de Ótimo (+4), como se fosse uma rolagem de defesa. Se o Senhor do Vazio saltar por cima da barreira e atacar usando sua espada, então ele rolará a perícia mais baixa entre Atletismo (saltar) e Lutar (atacar), e o Senhor da Tempestade ainda pode usar sua barreira Ótima (+4) para se defender.

Outro detalhe importante: Barreiras funcionam para os dois lados, então o criador não recebe nenhum benefício especial para atacar o alvo do outro lado da barreira — ambos os lados se beneficiam da barreira.

---

#### Terremoto

+ `O`{: .fate_font} **Superar:** Se o que estiver sendo superado for uma barreira física e o invocador vencer o alvo por 2, a barreira pode ser removida.
+ `C`{: .fate_font} **Criar Vantagem:** Receba +1 nas rolagens para criar barreiras usando Terremoto. Quando criar uma barreira, você pode optar por receber um -4 na rolagem — -3 com o bônus — para criar uma barreira que cerque uma zona completamente. Receba um -1 adicional se desejar fechar o topo também.
+ `A`{: .fate_font} **Atacar:** Você só pode realizar ataques em alvos que estejam no ou próximos ao chão — estilhaços ainda podem atingir quem estiver voando baixo, então qualquer coisa que estiver ao alcance das mãos pode ser atin- gido. Sofra -1 em seu ataque para tentar atacar todos os alvos que estejam em sua zona (exceto você). Por -2 você pode atacar todos os alvos em sua zona e em uma zona adjacente. Você pode expandir esse efeito indefinidamente, desde que esteja disposto a continuar recebendo -2 para cada zona.
+ `D`{: .fate_font} **Defender:** A terra demora a responder e isso reflete em -1 em todas as ações de defesa.

#### Dilúvio

+ `O`{: .fate_font} **Superar:** Receba +1 em qualquer tentativa de superar uma barreira física.
+ `C`{: .fate_font} **Criar Vantagem:** Qualquer barreira criada com água diminui em 1 por rodada a não ser que o conjurador se concentre nela, recebendo -1 em todas as ações subsequentes enquanto estiver mantendo a barreira.
+ `A`{: .fate_font} **Atacar:** O dano de seus ataques ignora qualquer armadura. Você pode receber -2 por atacar todos os alvos em uma zona (exceto você).
+ `D`{: .fate_font} **Defender:** Sem regras especiais.

#### Geleira

+ `O`{: .fate_font} **Superar:** Se o que precisa ser superado for uma barreira física e o conjurador superar a dificuldade por 2, ele consegue remover completamente a barreira.
+ `C`{: .fate_font} **Criar Vantagem:** Receba +1 nas rolagens para criar barreira usando Geleira. Ao criar uma barreira, você pode criar barreiras contínuas extras. Cada seção adicional de barreira — uma seção equivale a uma barreira entre duas zonas quaisquer — reduz seu valor em 1. Assim, se você conseguir um +6 na jogada e deseja criar uma barreira com 3 seções, ela terá um valor total de 4 (6-2, pois lembre-se que a primeira é grátis).
+ `A`{: .fate_font} **Atacar:** Você pode optar por causar metade do dano (arredondado para cima) para congelar o alvo no lugar. Isso cria uma barreira ao movimento do alvo, com uma dificuldade para superar igual ao dano causado.
+ `D`{: .fate_font} **Defender:** Se bem-sucedido com estilo numa jogada de defesa, você pode desistir do impulso para melhorar o valor de qualquer uma de suas barreiras em 1.

#### Inferno

+ `O`{: .fate_font} **Superar:** Se superar uma barreira física, reduza-a em 1.
+ `C`{: .fate_font} **Criar Vantagem:** Qualquer barreira criada com Inferno diminui seu valor em 2 por rodada a não ser que o conjurador se concentre nela, recebendo -1 em qualquer ação subsequente enquanto mantiver a barreira. Qualquer um que falhe em superar uma barreira de inferno tem a opção de forçar sua passagem por ela, recebendo dano igual ao número de tensões adicionais necessárias para uma rolagem bem-sucedida de superar.
+ `A`{: .fate_font} **Atacar:** Você pode sofrer -1 no ataque para atingir todos em uma zona (exceto você).
+ `D`{: .fate_font} **Defender:** Nenhuma regra especial.

#### Trovão

+ `O`{: .fate_font} **Superar:** Nenhuma regra especial.
+ `C`{: .fate_font} **Criar Vantagem:** Qualquer barreira criada com Trovão desaparece após 1 rodada a não ser que o conjurador se concentre nela, recebendo -1 em todas as ações subsequentes enquanto mantiver a barreira.
+ `A`{: .fate_font} **Atacar:** Trovão possui duas formas de ataque:
  + _Raios Concatenados:_ Arcos de raios vão de um alvo a outro com precisão. Para cada -1 que receber, você pode incluir um alvo adicional no ataque. A penalidade por distância é determinada pelo alvo mais distante, sendo -1 por zona a partir da zona inicial.
  + _Relâmpago:_ Quando atingir um único alvo, o som do trovão se espalha quando o relâmpago atinge. Se conseguir um impulso em seu ataque, você gera um impulso extra chamado Atordoado.
+ `D`{: .fate_font} **Defender:** Nenhuma regra especial.

### Variações e Opções

#### Compreensão Profunda

Talvez cada uma das tempestades esteja ligada a uma Calmaria correspondente — Montanha, Neve, Mar, Labareda e Vento. Aqueles que dominarem uma Tempestade podem, com o tempo, ganhar outro aspecto — ao custo de 1 de recarga — para refletir tal Calmaria, o que permitirá que ele internalize os pontos fortes da Tempestade para aprimorar-se em uma variedade de formas, assim como gerar mais efeitos sutis.

#### Múltiplas Tempestades

Nada proíbe um conjurador de possuir sintonia com várias Tempestades; o custo será apenas reduzir em 1 a recarga para cada aspecto escolhido.

#### Rituais

É possível possuir a perícia Tempestades mas não possuir sintonia com uma tempestade específica. É assim que funcionam os conjuradores menores. A vantagem disso é poder utilizar qualquer Tempestade que desejar. A desvantagem é levar alguns minutos para fazer o que um verdadeiro Senhor da Tempestade pode fazer em uma única rodada.

#### Invocações

As Tempestades não são espaços vazios. Seres nativos das Tempestades nadam confortavelmente dentro de cada uma delas e podem ser invocados para servir àqueles que sabem como chamá-los. Veja o Sistema de [Invocação](../invocadores-da-tempestade/)  para uma explicação detalhada.

#### Senhores Da Luz E Da Sombra

Aqueles que estudam tais coisas sugerem que há uma conexão entre as grandes Tempestades. Nascidas de quase pura energia (Trovão), elas se fundem (Inferno) e se tornam uma massa (Dilúvio) antes de solidificarem (Geleira) em algo concreto (Terremoto). Também poderia ser o inverso, onde a matéria começa bruta e ascende em direção a se tornar energia. Seja como for, a teoria é que existem uma sexta e sétima Tempestades, fechando o círculo das grandes Tempestades. Energia e Luz de um lado, Permanência e Trevas no outro. Há aqueles que alegam controlar tais forças de forma similar às tempestades, mas são raros e boa parte de seus esforços foram gastos em conflitos entre os dois lados em busca de descobrir qual é o princípio e qual é o final.

#### Senhores Do Vazio

Com toda sua fúria destrutiva, Tempestades são parte da realidade, pois tanto criam como destroem. Em um nível cósmico, elas se opõem ao Vácuo, o nada que busca consumir tudo. Senhores da Luz e das Trevas tendem a caracterizar o seu oposto dessa maneira e não há como dizer quem está certo. O Vazio é uma força de destruição e embora não seja maligno em natureza, aqueles que o habitam são; é o lar de demônios e monstros que gostariam de nada mais que consumir as Tempestades e, com elas, nosso mundo. Veja a sessão sobre os [Senhores do Vazio](../senhores-do-vazio/) para mais ideias.

------

- [« Sistemas](../sistemas/)
- [Os Seis Vizires »](../seis-vizires/)
