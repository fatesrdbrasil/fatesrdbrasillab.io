---
title: 5 - Perícias e Façanhas
layout: default
---

# 5 - Perícias e Façanhas

## O Que É Uma Perícia?

Uma **perícia** é uma palavra que descreve a competência de alguém em algo – como Atletismo, Lutar, Enganar – que o seu personagem adquiriu por talento inato, treinamento ou anos de tentativa e erro. As perícias são a base de tudo que o seu personagem sabe fazer no jogo que envolva desafios (e dados).

As perícias são medidas através da escala de adjetivos. Quanto mais alto, melhor seu personagem é na perícia. De forma geral, a sua lista de perícias serve como uma visão geral do potencial de seu personagem – no que você é excelente, no que é bom e no que não é tão bom.

Definimos as perícias de duas maneiras em *Fate* – em termos de *ação em jogo* que você pode executar com elas e *contexto* no qual pode usá-las. Existem apenas algumas ações básicas de jogo, mas o número de contextos é infinito.

### Ações Básicas

Cobriremos isso com mais detalhes em *Ações e Resoluções*, mas aqui está uma referência para que você não precise ir até lá agora.

- `O`{: .fate_font} **Superar:** Com essa ação, você enfrenta um desafio, tarefa envolvente ou obstáculo relacionados à sua perícia.
- `C`{: .fate_font} **Criar Vantagem:** Criar vantagens é uma forma de descobrir e criar aspectos e ganhar invocações gratuitas deles, seja  quando está descobrindo algo já existente sobre um oponente ou criando uma situação que lhe traz um benefício.
- `A`{: .fate_font} **Atacar:** Você tenta ferir alguém em um conﬂito. Esse dano pode ser físico, mental, emocional ou social.
- `D`{: .fate_font} **Defender:** Você tenta evitar que alguém lhe cause dano, passe por você ou crie vantagem para ser usada contra você.

Algumas perícias também possuem um efeito especial, como conceder algumas caixas de estresse extras. Veja [Vigor](../vigor/) e [Vontade](../vontade/) na lista de perícias abaixo para ver mais exemplos.

Mesmo que haja apenas quatro ações que podem ser usadas em conjunto com as perícias, a perícia em questão fornece o contexto necessário para a ação. Por exemplo, tanto Roubo quanto Ofícios permitem que você crie vantagem, mas em contextos bastante diferentes – Roubo permite fazer isso se você estiver prestes a entrar em um local e Ofícios permite fazer isso quando você está avaliando algum objeto. As diferentes perícias permitem a você diferenciar as habilidades de cada PJ, permitindo que cada um contribua de forma única ao jogo.

## O Que É Uma Façanha?

Uma **façanha** é um traço especial do seu personagem que muda a forma como uma perícia funciona. Uma façanha é algo especial, uma forma privilegiada de um personagem usar uma perícia de maneira única, o que é bastante comum em vários cenários – treinamento especial, talentos excepcionais, a marca do destino, alterações genéticas, poderes natos e uma miríade de outras razões podem explicar o porquê de algumas pessoas serem melhores em suas perícias do que outros.

Diferentes das perícias, que são coisas que qualquer um poderia conseguir fazer na campanha, façanhas são únicas e personalizadas. Por essa razão, as próximas páginas falarão sobre como criar suas façanhas, mas há também uma lista de façanhas de exemplo para cada perícia padrão.

Adicionar façanhas a seu jogo fornece uma gama de personagens diferentes, mesmo que possuam as mesmas perícias.

> Tanto Esopo quanto Fräkeline possuem a perícia Luta, mas Fräkeline também possui **Mestre em Combate**, que a torna mais apta a Criar Vantagem com essa perícia. Isso diferencia bastante os dois personagens – Fräkeline possui uma capacidade única de analisar e entender as fraquezas de seus inimigos de uma forma que Esopo não consegue.
>
> Podemos imaginar Fräk começando uma briga para testar os movimentos e ataques do inimigo, analisando cuidadosamente os limites de seu oponente antes de lançar um golpe decisivo, enquanto Esopo se contenta em apenas entrar quebrando tudo.

Você também pode usar isso para separar um certo grupo de habilidades como pertencentes a um grupo de pessoas, se for algo que o cenário requer.

Por exemplo, em uma ambientação contemporânea, pode ser que possuir apenas uma perícia não seja suficiente para tornar alguém um médico profissional (a não ser, claro, que seja um jogo sobre médicos). No entanto, como uma façanha para outra perícia mais geral (como Conhecimentos), é possível que um personagem seja "o médico" se é o que o jogador deseja.

### Façanhas e Recarga

Adquirir uma nova façanha reduzirá sua recarga inicial em um ponto para cada façanha acima de três.

- [« A Economia de Pontos de Destino](../economia-de-pontos-de-destino/)
- [Criando Façanhas »](../criando-facanhas/)
