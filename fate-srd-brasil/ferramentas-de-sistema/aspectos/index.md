---
title: Capítulo 2 - Aspectos
layout: default
---

# Capítulo 2 - Aspectos

## Aspectos

Jogar uma partida de Fate é bem simples. Você precisa: Aspectos são o que fazem Fate _rodar_. São o método mais claro e interessante para descrever quem é seu personagem e formam a base para o manuseio dos pontos de destino. Invocar um aspecto lhe dá um certo controle sobre o seu destino, uma forma de abrandar os caprichos dos dados. Aspectos forçados são os melhores amigos do Narrador no que diz respeito a complementar a história e criar situações interessantes, injetando drama nas cenas ou simplesmente complicando os planos dos jogadores um pouco mais.

### Invocar por um efeito

O Fate Sistema Básico fala um pouco sobre este conceito, explicando que “invocar por um efeito” não passa de uma forma extravagante de forçar. Isso é verdade, mas talvez não seja o significado completo desse termo.

Quando invoca por um efeito, você gasta um ponto de destino – ou usa uma invocação grátis – para criar um efeito mecânico específico, algo diferente do que um aspecto comum é capaz de fazer. Ao criar um aspecto, observe-o e decida se é preciso ou não que algum efeito atrelado a ele. Talvez o seu mago possa invocar **_Sintonia Com a Terra_** para evitar cair ou perder o equilíbrio ou talvez o seu detetive psíquico use **_Espionagem Mental_** para ler os pensamentos superficiais de alguém. 

Mecanicamente, o efeito de um aspecto deve valer o ponto de destino gasto – o equivalente a duas tensões de potência, assim como qualquer outro efeito de invocar um aspecto. O efeito de um aspecto deve realizar algo, como nos exemplos acima, em vez de fornecer apenas um bônus. Uma invocação normal de um aspecto já concede um bônus, então você não precisa de um efeito especial que faça a mesma coisa. Um efeito de aspecto é um pouco semelhante a ter uma façanha extra de exceção às regras que sempre precisa ser paga, tanto do ponto de vista do que o efeito consegue causar quanto da complexidade acrescentada ao personagem.

--- 

#### QUANTOS EFEITOS?


Falando em complexidade de personagem, Narradores precisam decidir quantos desses efeitos cada personagem do jogador possuirá. A forma mais simples de usar esta regra é permitir que cada personagem adicione um efeito especial ao seu conceito, já que esse é provavelmente o aspecto de maior peso e que mais define o personagem. Você pode fornecer mais efeitos de aspectos aos PJs, mas fazer isso seria como permitir muitas façanhas – nem todos serão usados e a quantidade de opções pode deixar o jogo travado. 

PdNs podem possuir efeitos de aspectos também, mas é melhor dá-los apenas aos PdNs principais e talvez a algum PdN de suporte muito importante. 

Em ambos os casos é melhor limitar o número de efeitos de as-
pectos por personagem a um ou dois no máximo.

---

Em termos de custo, não há problema em permitir que os PJs tenham um ou dois desses efeitos de graça. Eles possuem o mesmo poder de uma invocação normal e são mais situacionais, fazendo com que não sejam usados com tanta frequência. Narradores, se for passar disso vocês estarão em seu direito de pedir aos jogadores que gastem sua recarga para adquirir efeitos adicionais.

**Aspecto: _Sempre Armado_**

**Efeito:**  Gaste um ponto de destino para revelar uma arma pequena e ocultável – como uma faca ou pistola pequena – guardada secretamente em alguma parte de seu corpo, mesmo se tiver sido desarmado recentemente.

**Aspecto: _Ninja do Clã da Serpente_**

**Efeito:** Gaste um ponto de destino para desaparecer repentinamente, mesmo se houver pessoas olhando para você. Isso lhe dá motivo para realizar uma rolagem de Furtividade na tentativa de se esconder. 

**Aspecto: _Explorador Élfico de Olhos Aguçados_**

**Efeito:** Gaste um ponto de destino para conseguir ver a grandes distâncias – até dois quilômetros de distância – claramente e em grande detalhe, mesmo à noite.

### Invocação Proporcional

Ao invocar um aspecto, ele lhe concede um bônus de +2 ou uma nova rolagem dos dados. Isso é suficiente na maioria das vezes, mas não cria vantagens mecânicas por invocar um aspecto que se aplique particularmente bem a uma determinada situação, assim como não dissuade os jogadores de invocar aspectos que praticamente não se aplicam à situação. Nestes casos, outra opção seria a invocação proporcional.

Invocações proporcionais dividem aspectos em três categorias: invocações questionáveis, relevantes e perfeitas.

**Invocações questionáveis** mal se aplicam à situação do momento, como invocar **_Forte Como um Touro_** em um concurso de bebidas ou usar **_Lixo Empilhado_** para amenizar sua queda da janela do segundo andar. Se uma invocação for questionável, você recebe apenas a oportunidade de rolar novamente. Isso significa que nunca será possível melhorar sua rolagem além do resultado nos dados, mas será possível usar esta invocação para tentar melhorar uma rolagem desastrosa. Significa também que invocações questionáveis múltiplas normalmente não serão tão úteis.

Ao fazer uma **invocação relevante** , você está invocando algo que claramente se aplica à situação atual sem a necessidade de muitas justificativas. Talvez esteja usando **_Gatilho Mais Rápido do Oeste_** em um duelo ou esteja escondido atrás de um Muro de Concreto em busca de proteção. Isso concede o mesmo que uma invocação normal — um +
ou rolar novamente.

Uma **invocação perfeita** é aquela que faz todos dizerem “Incrível!” ou rir e concordar de forma entusiasmada quando você a anuncia. Ela se encaixa perfeitamente à situação, a escolha certa para o momento. Há pouca coisa que pode motivar o seu personagem de forma tão intensa quanto invocar **_Udru Khai Matou Minha Família_** quando está tentando pegá-lo enquanto ele foge. Quando faz uma invocação perfeita, você é _automaticamente bem-sucedido_ em sua ação, sem a necessidade de rolagem. Se invocar o aspecto após a rolagem, apenas ignore a rolagem. Se for preciso saber quantas tensões conseguiu, assuma que obteve uma. Isso significa que você pode invocar um segundo aspecto para ser bemsucedido com estilo, se fizer muita diferença.

A única exceção é o ataque. Quando você faz uma invocação perfeita de um aspecto em um ataque, não é preciso rolar os dados. Em vez disso, seu ataque é definido como seu nível de perícia +3. Assim, se estiver atacando com Lutar Bom (+3) e fizer uma invocação perfeita, seu esforço terá sido Fantástico (+6), valor que seu adversário terá de superar. Se você já tiver rolado os dados, a invocação perfeita irá _melhorar_ sua jogada, adicionando +1 a ela.

### Destruindo Aspectos de Situação

Alguns aspectos de situação sugerem destrutibilidade ou uso limitado, como __*Pilha de Botijões*__ ou __*Colunas de Suporte Apodrecidas*__. Eles podem ter um efeito narrativo, mas não necessariamente um efeito mecânico. Se você gostaria que tais aspectos possuíssem uma diferença mecânica de aspectos como ___Escuro Como o Breu___ ou ___Cobertura Por Toda Parte___, pode permitir que os PJs finalizem tais aspectos de situação.

Quando um jogador destrói um aspecto de situação, ele declara sua intenção e explica como ao usar esse aspecto ninguém mais poderá usá-lo novamente. Se puder fazer isso de forma que agrade a todos, ele poderá invocar esse aspecto uma vez gratuitamente.

Uma vez que tenha invocado o aspecto, ele desaparece e a situação muda para a pior – ou, ao menos, se torna mais perigosa. Destruir um aspecto de situação cria outro aspecto de situação que representa como o antigo aspecto foi destruído e como isso piorou as coisas. Esse último ponto é a chave — é preciso destruir coisas para complicar outras. Não seria justo destruir o aspecto ___Pilha de Botijões___ e substituí-lo por ___Marcas Chamuscadas___. Substitua-o por algo grande, incrível e destruidor, como ___Todo o Prédio Está em Chamas!___ ou ___O Celeiro Está Desmoronando!___. O novo aspecto de situação sempre deve deixar as coisas mais tensas para todos, e deve ser uma ameaça iminente.


------

- [« A Regra de Bronze](../regra-de-bronze/)
- [Aspectos de Gênero »](../aspectos-de-genero/)
