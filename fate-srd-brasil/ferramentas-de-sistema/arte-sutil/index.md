---
title: A Arte Sutil
layout: default
---

## A Arte Sutil

### Notas de Criação

Mecanicamente falando, este sistema tem como base a ação criar vantagem. Isto é, ele simplesmente expande onde, quando e que tipo de vantagens pode ser criadas. Como é um sistema mais discreto do que os outros apresentados, ele não responde a algumas das questões principais. Isso torna o sistema mais adequado para jogos que não se focam em magia. Na verdade, ele funciona melhor em jogos onde há a dúvida de se a magia é real ou não, embora em outros jogos também possa representar uma forma de magia “menor”.

### Descrição

Quando imaginamos sociedades mágicas, a primeira coisa que vem à mente são locais de encontro antigos e ordens secretas. Esses lugares certamente existem, mas não possuem mais tanta magia — apagada pelo próprio sucesso dos locais. Considere que os benefícios da magia são efêmeros, enquanto os benefícios da colaboração, conspiração e parcerias são concretos. Para aquelas pessoas importantes, bem-sucedidas, que se encontram em salas enfumaçadas de madeira escura e carpetes altos, a magia é apenas um item decorativo. Eles não precisam mais acreditar e, na maioria dos casos, já não o fazem há muito tempo.

Há exceções. Muitas das organizações bem estabelecidas começaram repletas de crenças e intenções firmes, mas essa crença desapareceu sob o peso do próprio sucesso desses grupos. É fácil acreditar que seus amuletos de prosperidade estão ajudando nas suas escolhas de investimento quando você não tem a menor ideia do que está fazendo, mas à medida que é bem-sucedido — e aprende — então é natural começar a atribuir seu sucesso ao seu talento e escolhas inteligentes.

Como resultado, a maioria dos grupos de praticantes de magia são mais informais. Redes de amigos, amigos de amigos ou estranhos com conexões tênues que partilham de um interesse em comum. Muitas reuniões giram em torno de bebidas, encontros e noites no sofá de estranhos em apartmentos baratos.

A realidade nua e crua é que a magia cria suas raízes mais fortes nos desenraizados. Pessoas inteligentes e capazes, mas sem direção ou propósito, se apegam à magia pela solução rápida que ela representa. É uma arma desconhecida e, para alguns, a melhor à disposição.

#### Magia E Realidade

O importante é que não há prova “real” de que essa magia funciona. A introdução desses aspectos não altera a realidade de nenhum modo real ou replicável. Eles certamente alteram um pouco a balança, mas só podem obter resultados no limite do possível e razoável. Para um observador externo, essa “magia” se parece muito com viés de confirmação. Se você amaldiçoar alguém e algo ruim acontecer a essa pessoa, é fácil assumir crédito por isso, mas um cínico diria que coisas ruins acontecem com pessoas o tempo todo.

Isso pode ser difícil e, como resultado, os que acreditam tendem a se unir. Eles formam subculturas onde podem discutir o que fazem, trocar dicas e reforçar sua crença de que isso é algo que importa. De muitas maneiras, esses grupos são mais importantes do que a magia em si, mas eles não são exatamente o que se espera.

### **Versão Resumida**

Não quer ler tudo? Faça o seguinte:

- Compre uma perícia chamada “Magia”.
- Tire meia hora em um quarto escuro com o nome de alguém, um    boneco de vodu ou apetrechos similares e faça uma rolagem de     Magia para criar vantagem contra o alvo do feitiço.
- Coloque um aspecto apropriado a uma maldição ou bênção no alvo.     Ele dura três dias, ou sete se você for bem-sucedido com estilo.

### Mecânica

Este sistema adiciona uma perícia: Magia. Sua descrição se encontra abaixo.

#### Perícia: Magia

Magia é a perícia para lançar bênçãos ou maldições sobre uma pessoa ou lugar. Embora a perícia em si seja genérica, suas manifestações específicas não são. Um praticante deve possuir um conjunto de regras e diretrizes que segue para usar magia. Essas regras podem ser baseadas no mundo real, ser totalmente inventadas ou qualquer coisa entre os dois, mas devem ser consistentes e sempre demandar tempo, esforço e um ritual. Há outras limitações descritas abaixo.

+ `O`{: .fate_font} **Superar:** Há poucos obstáculos úteis que a magia possa superar, embora muitos praticantes pensem o contrário. É um erro comum achar que a perícia Magia pode ser usada para “detectar” trabalhos mágicos, mas isso seria tão confiável quanto tentar adivinhar.

Um uso concreto para a ação superar é superar o ceticismo de outros. A perícia magia também representa quão bem você “vende” a ideia da magia, ou ao menos o que acredita. Isso funciona como um uso bem específico da perícia Enganar, mesmo que o personagem não sinta que está enganando alguém.

+ `C`{: .fate_font} **Criar Vantagem:** A principal atividade com a perícia Magia é criar vantagem. Assumindo um único alvo — uma pessoa ou coisa de talvez até o tamanho de uma casa —, aproximadamente meia hora e os aparatos necessários para o ritual, a rolagem é realizada contra uma dificuldade Regular (+1). Se bem-sucedido, o alvo recebe o aspecto de bênção ou maldição (veja os detalhes abaixo) por três dias e três noites. Outras modificações possíveis seriam:
  - Se o alvo não estiver presente, então a dificuldade aumenta entre     +1 e +3. +3 se é dito apenas o nome do alvo, +1 se um laço simbólico do alvo estiver presente — seu sangue, um item precioso para     ele, etc. — e se não for claramente algum desses casos, então seria     apropriado um +2.
  - Se o alvo é grande — um grupo pequeno com menos de doze membros ou um lugar grande como um prédio ou parque — a dificuldade aumenta em +3. Esse é o tamanho máximo de um algo que um     feitiço pode afetar, embora a maioria dos praticantes desconheça isso     e todos os anos horas de magia são desperdiçadas visando partidos     políticos, times de futebol e _hipsters_.
  - Alguns feitiços possuem um alvo secundário, como um feitiço que     faça com que seu chefe fique enfurecido com alguém. A ausência desse segundo alvo também reflete na dificuldade — +0 se estiver presente, +3 se souber apenas o nome, como acima. A única exceção é que se o alvo secundário aceitar algum tipo de símbolo da magia — uma poção, alguma bugiganga — então ele estará efetivamente “presente”. Tais artefatos devem ser usados em no máximo três dias.
  - Um sucesso com estilo estende a duração para uma semana.
  - Nenhum alvo pode sofrer mais de um feitiço por vez. O feitiço mais    recente substitui o mais antigo.
  - Algumas bênçãos e maldições possuem seus próprios modificadores    adicionais.
  - Na prática, um feitiço em uma área cria um aspecto de cena que    pode ser usado normalmente por qualquer um no local.
+ `A`{: .fate_font} **Atacar:** Não existem ataques mágicos.
+ `D`{: .fate_font} **Defender:** Não existe defesa mágica.

#### Feitiços

Aspectos colocados num alvo são geralmente chamados de bençãos ou maldições, dependendo do efeito desejado, mas coletivamente todos são considerados feitiços. A lista não é aberta — há um grupo fixo de feitiços e o conhecimento destes é a moeda da comunidade mágica. Feitiços são complicados a ponto de ser muito difícil memorizá-los e ainda executá-los com exatidão; é por isso que são mantidos em livros, bancos de dados e outros tipos de arquivos. Afanar o livro de feitiços de outro mago pode ser informativo, mas também pode ser tão útil quanto roubar suas anotações sobre química orgânica — mesmo que não estejam propositalmente complicadas, podem ser bem difíceis de entender. Além disso, claro, não há como distinguir uma magia real de uma falsa.

Para maior clareza, o alvo do feitiço é a pessoa, lugar ou coisa que recebe o efeito do feitiço. Às vezes um feitiço também terá um sujeito: uma pessoa, lugar ou coisa que será o foco do efeito do feitiço sobre o alvo. Por exemplo, um feitiço de amor para fazer Jake se apaixonar por Andy seria lançado sobre Jake (o alvo), com foco em Andy (o sujeito).

**Aborrecimento:** O alvo incomoda os outros. Se o feitiço possuir sujeito, então o alvo do feitiço ficará mais facilmente aborrecido com esse sujeito.

**Carisma:** Tratando de amor, isso melhora a presença e comportamento geral do alvo. Às vezes este feitiço é ridicularizado — especialmente aqueles que precisariam desse tipo de feitiço —, mas é frequentemente usado em silêncio.

**Clareza:** Popular entre aqueles que se consideram magos sofisticados. Para muitos este feitiço é seu café da manhã, aguçando seus pensamentos e sentidos. É também um “contrafeitiço” popular, usado para remover maldições.

**Imperícia:** Sabe aqueles dias em que você deixa cair um copo, derrama o café no colo e rasga a camisa em um trinco? Isto faz esse tipo de dia acontecer. 

**Confusão:** As pessoas tendem a interpretar mal o alvo — ou, se for um local, perder-se facilmente.

**Amor:** Um dos mais conhecidos, mas também um dos mais controversos, especialmente quando usado com um sujeito. Sem um sujeito, simplesmente faz com que o alvo seja mais amigável com o mundo, mas com um sujeito, este inclina-se em direção ao alvo. Muitas pessoas veem isso como desagradável na melhor das hipóteses, e assédio na pior. É um tema delicado e um grande número de feiticeiros contorna isso ao explicitamente lançar feitiços falsos.

**Saúde:** O equivalente mágico aos antiácidos com vitamina C e zinco.

**Sorte:** Este é o feitiço mais comum em circulação, podendo assumir a forma de boa ou má sorte.

**Obscuridade:** O alvo é facilmente ignorado — pelo sujeito, se houver. Se isto é uma bênção ou maldição, depende do seu ponto de vista.

**Prosperidade:** Outra bênção popular. Questões financeiras favoráveis surgem pelo caminho do alvo. É raro isso se transformar em uma grande colheita, mas pode se mostrar como uma extensão no pagamento de um empréstimo ou uma cerveja grátis.

**Ira:** Pequenas coisas incomodam o alvo mais do que o normal, como se tivesse acordado com o pé esquerdo. Se o seu feitiço tiver um sujeito, então o alvo do feitiço é mais facilmente enfurecido por ele.

**Segurança:** Mantém o alvo — ou a área — mais seguro do que estaria. Estes não são todos os feitiços disponíveis, mas fornecem alguma inspiração no tom necessário para outras ideias.

#### Façanhas Mágicas


**De Cor:** Você pode escolher três feitiços que conhece bem a ponto de não precisar consultar suas anotações para conjurá-los.

**Mau Olhado:** Você pode tentar colocar Má Sorte em um alvo com nada mais do que um gesto simples. Isso dura apenas um dia.

**Decorador de Interiores:** Você pode até chamar de feng shui na fatura, mas é tudo decoração. Se puser um feitiço em um local, você pode organizar os móveis e a decoração para tal. Se o fizer, o efeito dura por uma estação inteira — ou até que alguém reorganize os móveis.

### Variações e Opções

#### Mas Funciona?

É inteiramente possível que este “sistema” seja uma mentira. A magia não faz nada e tudo não passa de vontades realizadas e viés de confirmação. Um Narrador poderia até mesmo colocar isso em jogo, silenciosamente deixando de representar os aspectos que os jogadores aparentemente criaram. Isso é, de modo geral, uma péssima ideia. Isso sufoca a ideia do jogador e paralisa seu conceito — a não ser que ele também goste da ideia de tudo ser uma farsa. 

Se quiser enfatizar essa ideia — mesmo que não queira adotá-la totalmente — então as coisas tornam a magia impossível de ser comprovada são boas oportunidades para forçar.

#### Movimentando As Coisas

Também é possível que tudo seja um sistema mais abertamente mágico. Nesse caso você pode introduzir efeitos únicos, improváveis ou estranhos. Isso exigirá expandir a lista de feitiços para incluir coisas mais concretas como “alimentos apodrecem quando você os toca”, o que também faz com que a magia seja mais concretamente perceptível através da perícia magia. Neste caso, a duração dos efeitos deve ser estendida para o período de um mês lunar.

#### Maldições De Combate

Assumindo um estilo mais evidente de magia, uma variante permitiria “conjurações de combate” de bênção e maldições. É algo bem diferente da imagem tradicional que temos de magos conjurando raios, mas se adapta bem a cenários com baixo nível de magia. Neste caso, o feitiço pode ser conjurando em qualquer alvo que possa ser visto e, de modo geral, isso permite efeitos interessantes para criar vantagem.

Por padrão, devem ser efeitos invisíveis, mas ainda assim podem gerar más escolhas, armas que erram e assim por diante. No entanto, se o Narrador considerar apropriado, então pode ser permitido um pouco mais de cor para tornar tudo ainda mais mágico — como um feiticeiro do fogo criando vantagens flamejantes.

#### Duelos Entre Feiticeiros

Se maldições de combates forem possíveis, então também existirão duelos de feiticeiros. Um duelo como esse ocorre quando dois feiticeiros se encontram e travam olhares, usando Magia em lugar de Lutar e infligindo dano mental até que um ou outro abandone o conflito. Para um observador xterno, tudo o que acontece é uma troca de olhares seguida de um dos dois
sucumbindo — possivelmente morto, de acordo com a decisão do vencedor. Entre ambos os feiticeiros, a batalha pode tomar qualquer forma.

Às vezes, seres de poder maior podem ser trazidos para um duelo por magia mortal. Isso exige alguma preparação do praticante, como a criação de um objeto de foco. Esses conflitos ainda ocorrem principalmente no éter, mas podem envolver uma troca de energias mais chamativa ou outro efeito. Nesses casos, se o poder maior for derrotado, o resultado raramente é fatal, mas o ser pode receber consequências.

------

- [« Os Seis Vizires](../seis-vizires/)
- [Invocadores da Tempestade »](../invocadores-da-tempestade/)
