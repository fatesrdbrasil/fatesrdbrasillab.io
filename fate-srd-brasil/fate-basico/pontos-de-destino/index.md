---
title: Pontos de Destino
layout: default
---

## Pontos de Destino

Você usa marcadores para representar quantos pontos de destino possui durante o jogo. Pontos de destino são um dos recursos mais importantes em Fate – são a medida de quão inﬂuente é possível ser na história de seu personagem.

É possível gastar pontos de destino e **invocar** um aspecto, acrescentar um detalhe à história ou ativar certas manobras. Você ganha pontos de destino ao aceitar que um dos seus aspectos seja **forçado**.

Um aviso: não use marcadores comestíveis, especialmente se a comida ainda não chegou.

### Invocar Um Aspecto

Sempre que for realizar um teste de perícia e estiver em uma situação onde um aspecto pode ser aplicado para lhe ajudar, é permitido gastar pontos de destino para invocá-lo a seu favor, podendo alterar o resultado dos dados. **Isso lhe permitirá rolar os dados novamente ou adicionar +2 ao resultado da rolagem, o que for melhor** (normalmente +2 é uma boa escolha se você rolou -2 ou mais, mas às vezes você pode querer arriscar rolar novamente para tentar conseguir +4). Isso é feito após rolar os dados se o resultado final não for satisfatório.

**Você também precisa explicar ou justificar como o aspecto lhe ajuda a melhorar o resultado para receber o bônus** - às vezes será bem claro, mas em alguns momentos um pouco de criatividade narrativa pode ser necessária.

Você pode gastar mais de um ponto de destino por rolagem, ganhando novamente uma nova rolagem ou outro +2, contanto que tenha pontos para invocar os diferentes aspectos.

> Fräkeline está numa rede paralela de comércio, tentando fazer com que um negociador de itens valiosos descreva seus protocolos de segurança contra hackers. Ela se passa por uma nobre de um planeta rico. O comerciante tem uma oposição passiva de +3 (Bom), e a perícia Enganar de Fräk é Razoável (+2).
>
> Maira lança os dados e obtém um 0. Isso a deixa com um resultado Bom, o que não é suficiente para conseguir a informação.
>
> Ela olha na ficha de personagem, e diz para Amanda: “Sabe, Se É Valioso, Poderia Ser Meu... e eu sei o que é realmente valioso nessa galáxia e o que não é. Vou tentar impressionar ele falando bem dos itens raros e valiosos que ele tem na coleção”.
>
> Amanda gosta e concorda com a ideia. Maira entrega um ponto de destino para invocar o aspecto e escolhe adicionar +2 ao seu lance de dados. Isso eleva seu resultado para Ótimo (+4), o que excede a oposição. O comerciante, devidamente impressionado, começa a se gabar de sua segurança, e Fräkeline presta atenção em todos os detalhes.

### Acrescentar um Detalhe à História

Às vezes você quer adicionar um detalhe que funcione melhor para seu personagem, mas não vê uma forma certa para representar isso com os aspectos. Por exemplo, você pode usar isso para narrar uma coincidência conveniente, como ter os objetos necessários para uma determinada tarefa (“claro que eu trouxe isso comigo!”), aparecendo em um momento dramático apropriado, ou sugerindo que você e o PdN que conheceu casualmente possuem coisas em comum.

É preciso justifcar os detalhes da história criando relações entre eles seus aspectos. Narradores, vocês têm o direito de vetar qualquer sugestão que pareça fora de contexto ou sugerir aos jogadores que revejam o conceito, especialmente se o resto do grupo não concordar com a ideia.

> Bandu, O Oculto e seus companheiros estão sendo perseguidos por um grupo de policiais na Cidade Flutuante de Sa'ha'vash. Os jogadores declaram que montarão em uma das motos voadoras para tentar escapar.
>
> Michel olha para a fcha e diz “Ei, eu tenho o aspecto Funciono Sob Pressão. Posso dizer que, como estamos acuados, eu consigo apontar o melhor caminho para escapar?”
>
> Amanda acha uma boa ideia. Michel entrega o ponto de destino e descreve Bandu gritando comandos no ouvido de Fräk, conforme eles voam pela cidade com tiros por todo lado.
>
> Michel faz com que Bandu se vire e diga: “Eu sempre sei as melhores rotas. Minha mãe era uma nave."

### Forçar

Às vezes (na verdade, com frequência) você se encontrará em uma situação na qual um aspecto pode complicar a vida de seu personagem e criar um drama inesperado. Quando isso acontecer o Narrador irá sugerir uma complicação que pode surgir. Isso é o que chamamos de **forçar**.

Em certos momentos forçar significa que seu personagem falha automaticamente em algum objetivo ou suas escolhas são restritas ou simplesmente que as consequências são inevitáveis. Você pode negociar os detalhes para criar algo que seja mais apropriado e dramático naquele momento.

Assim que a complicação for aceita, você recebe um ponto de destino em compensação. Se desejar, poderá pagar um ponto de destino para evitar a complicação, mas não recomendamos que os jogadores façam isso sempre – você provavelmente precisará desse ponto mais à frente, fora que aceitar complicações trará drama (e, consequentemente, diversão) à sua história.

Jogadores podem pedir para forçar um aspecto quando quiserem uma complicação em uma decisão que acabaram de fazer, desde que tenha alguma relação com seus aspectos. Narradores, vocês podem chamar uma dificuldade quando fizerem o mundo responder aos personagens com uma complicação ou alguma outra forma dramática.

Todos à mesa são livres para sugerir momentos apropriados para forçar um aspecto sobre qualquer personagem (inclusive seus próprios). Narradores, vocês possuem a palavra final no que é ou não um forçamento válido. Manifeste-se se perceber que um forçamento aconteceu naturalmente como resultado de uma jogada sem ter ocorrido a premiação de pontos de destino.

> Esopo tem o aspecto ***Não Entendo Absolutamente Nada*** e está participando de um baile na Estação Espacial Charon Prime, com os amigos, a convite de um conhecido na nobreza.
>
> Amanda diz aos jogadores, “Conforme vocês observam as comidas e bebidas estranhas por todo lado, uma donzela se aproxima.” Ela se vira para Esopo e pergunta sobre o motivo de estarem ali.
>
> Léo diz, “Hã... bem, vou falar que eu sou um convidado, e tentar descobrir porque ela quer saber sobre mim”.
>
> Amanda segura um ponto de destino e diz “Será que algo pode dar errado, já que Esopo não entende as coisas como uma pessoa normal?” Léo ri e responde. “Sim, com certeza Esopo vai começar a comer algo nojento na frente dela, cutucando ela enquanto faz comentários bem incorretos. Aceito o ponto de destino.”
>
> Amanda e Léo jogam um pouco para descobrir como Esopo acaba ofendendo a moça, e Amanda descreve soldados se aproximando. Um deles diz: “É melhor você ter modos ao se dirigir a Raiduqueza de Nova Titânia.”
>
> Léo leva a mão à testa e Amanda abre um sorriso maligno.

- [« Realizando Ações](../realizando-acoes/)
- [Comecando a jogar »](../comecando-a-jogar/)
