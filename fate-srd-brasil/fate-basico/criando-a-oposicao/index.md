---
title: Criando a Oposição
layout: default
---

## Criando a Oposição

Um dos trabalhos mais importantes do Narrador é criar os PdNs que se oporão aos PJs e tentarão impedi-los de cumprir com seus objetivos durante os cenários. A história só acontece quando os PJ se deparam com seus adversários e decidem o que fazer com relação a isso – até onde estão dispostos a ir, que preço estão dispostos a pagar e como mudam de acordo com a experiência.

Como Narrador, você deve buscar um balanço com os PdNs opositores – você quer que os jogadores experimentem a tensão e incerteza, mas não que sua derrota já esteja predeterminada. Você quer que eles batalhem para conseguir as coisas, mas não quer que desistam.

Veja como.

### Leve Apenas o Que Precisa Para Sobreviver

Primeiramente, lembre-se que PdNs não precisam de fichas completas como os PJs. Muitas vezes essas informações extras não importam pois os PdNs não estarão no centro das atenções. É melhor focar em escrever exatamente o que você precisa para o encontro com o PdN com os PJs e então preencher os campos necessários à medida que forem necessários (assim como os PJs também podem) se esse PdN se tornar importante na campanha.

### Os Tipos de PdNs

PdNs aparecem de três formas diferentes: **PdNs sem importância, PdNs de suporte** e **PdNs principais**.

#### PdNs Sem Importância

A maioria dos PdNs em sua campanha são sem importância – pessoas que são tão insignificantes para a história que suas interações com os PJs muitas vezes não requerem sequer que tenham um nome. O lojista aleatório que eles encontram na rua, o bibliotecário, os garçons em um bar, os guardas no portão. Sua aparição na história é temporária e fugaz – os PJs provavelmente os encontrarão uma única vez e nunca mais os verão. De fato, na maioria das vezes, você os criará apenas como reﬂexo enquanto descreve o ambiente. “A praça é bela ao meio-dia e está repleta de pessoas andando. Há um porta voz gritando as notícias locais”.

Por si só, PdNs sem importância geralmente não são feitos para gerarem muito desafio para os PJs. Você os usa como rolagens de baixa dificuldade, principalmente para mostrar o quão competentes são os PJs. Em conﬂitos, servem como distração ou atraso, forçando os PJs a trabalharem um pouco mais para conseguir o que desejam. Muitas histórias de ação e aventura apresentam vilões com um exército de capangas. Os PdNs sem importância são os capangas.

Para um PdN sem importância, são necessárias apenas duas ou três perícias baseadas no seu papel na cena. O segurança pode ter Lutar e Atirar, enquanto o secretário possui apenas Conhecimentos. Eles nunca recebem mais do que um ou dois aspectos, pois não são importantes o suficiente. Eles possuem apenas uma ou duas caixas de estresse (se tiverem alguma), mas absorver tanto ataques físicos quanto mentais. Em outras palavras, não são adversários à altura dos PJs.

Eles aparecem em três níveis: **Regular**, **Razoável** e **Bom**.

##### Regular

-  **Competência:** Recrutas, brutamontes, soldados de patente baixa e assim por diante. Se houver dúvida, um PdN sem importância deve ser Regular.
-  **Propósito:** A maioria serve apenas para mostrar o quão incríveis são os PJs.
-  **Aspectos:** Um ou dois.
-  **Perícias:** Uma ou duas em nível Regular (+1).
-  **Estresse:** Sem caixas de estresse – uma tensão de dano é suficiente para derrubá-los.

##### Razoável

-  **Competência:** Profissionais treinados como soldados e guardas de elite ou outros cujo papel na cena reﬂita sua experiência, como um nobre de língua afiada ou um ladrão talentoso.
-  **Propósito:** Drenar alguns dos recursos dos personagens (um ou dois pontos de destino, caixas de estresse, talvez uma consequência suave).
-  **Aspectos:** Um ou dois.
-  **Perícias:** Uma Razoável (+2) e uma ou duas Regulares (+1).
-  **Estresse:** Uma caixa de estresse – um ataque de duas tensões é o bastante para derrubá-los.

##### Bom

-  **Competência:** Oposição pesada, especialmente quando numerosa.
-  **Propósito:** Drenar os recursos dos personagens – como o acima, mas com mais peso. Apresente-os em maior quantidade antes de um encontro mais importante.
-  **Aspectos:** Um ou dois.
-  **Perícias:** Uma Boa (+3), Uma Razoável (+2) e uma ou duas Regulares (+1).
-  **Estresse:** duas caixas de estresse – três tensões de dano para tirá-los do de conﬂito.

##### Bandos

Sempre que possível, PdNs sem importância formarão grupos ou **bandos**. Isso não apenas aumenta suas chances de sobrevivência, como também facilita o vida do Narrador. Para todos os fins, um bando pode ser tratado como uma única unidade - ao invés de rolar os dados individualmente para cada um dos três capangas, role apenas uma vez para o bando inteiro.

Veja a seção [*Trabalho em Equipe*](../trabalho-em-equipe/) no capítulo anterior para ver como bandos podem concentrar seus esforços para ser mais eficientes.

##### Dano e Excedentes

Quando um bando recebe dano, tensões que excedam o suficiente para retirar de ação um PdN são aplicadas ao próximo PdN do bando, um por vez. Dessa maneira, é inteiramente possível que um PJ derrube um grupo de quatro ou cinco PdNs (ou mais!) em uma única rodada.

Quando um bando recebe estresse suficiente para reduzir o grupo a um único PdN, tente fazer aquele PdN se unir a outro bando na cena, se fizer sentido (se não, faça-o fugir. PdNs sem importância são bons nisso).

> Esopo e Fräkeline são atacados por meia dúzia de bandidos apenas por terem passado no beco errado.
>
> Esses criminosos são considerados PdNs sem importância, com as perícias Percepção e Lutar em nível Razoável (+2).
>
> Normalmente o nível Bom (+3) que Fräk possui em Percepção lhe permite agir primeiro, mas Amanda acha que os gângsteres estão em vantagem para cercar os PJs, permitindo que ajam primeiro na rodada. Em um grupo de seis, o nível de Percepção Razoável sobe para Fantástico (+6).
>
> Amanda os divide em dois bandos de três: um para Esopo e outro para Fräkeline. Ambos atacam com níveis Bom (+3) (Luta com nível Regular (+1) e +2 pelos ajudantes), mas nenhum dos grupos acerta o ataque.
>
> Fräkeline é a próxima. Maira diz, “Num piscar de olhos, o chicote de Fräk estala na cara desses otários!” Ela consegue um resultado Ótimo (+4) em sua jogada de Luta. O primeiro capanga defende com resultado Bom (+3) (+0 no dado, perícia Média +1 e +2 pela ajuda dos comparsas), então Fräk causa apenas uma tensão de dano nos capangas – o suficiente para tirar um deles do conﬂito. Ainda existem dois no bando, mas eles receberão apenas +1 pelo único capanga restante ao atacar novamente.
>
> No turno de Léo, Esopo causa duas tensões contra o bando que está encarando, o suficiente para acabar com dois deles e reduzir o grupo a um único PdN.

##### PdNs Sem Importância Como Obstáculos

Uma maneira ainda mais fácil de lidar com PdNs é trata-los como obstáculos: crie uma dificuldade para o PJ realizar uma ação de superar para se livrar de um PdN e resolva a questão em apenas uma jogada. Você não precisa escrever nada sobre ele, apenas determinar uma dificuldade, como mostrado em [*Ações e Resoluções*](../acoes-e-resolucoes/) e assumir que o PJ consegue passar por ele se bem-sucedido no teste.

Se a situação é um pouco mais complicada que isso, torne-a um desafio. Esse truque é útil para quando você quer que um grupo de PdNs sem importância atue mais como uma característica da cena do que como indivíduos.

> Bandu quer convencer um grupo de cientistas que continuar suas pesquisas sobre a Dimensão Vazia condenará todos à perdição e pode até destruir o universo. Amanda não quer que ele convença cada pesquisador individualmente, então tratará isso como um desafio.
>
> Os passos do desafio são: determinar os simpatizantes (Conhecimentos), convencê-los a se voltar contra os outros (Enganar) e submetê-los através de um discurso das desgraças por vir (Provocar). Ela escolhe uma dificuldade passiva Ótima (+4) para o desafio.

##### PdN Primeiro, Nome Depois

PdNs sem importância não precisam *permanecer* anônimos. Se os PJs decidirem conhecer melhor o taverneiro, o pastor, o chefe de segurança ou qualquer outra pessoa, vá em frente e torne-o uma pessoa real – mas isso não significa que você precisa torná-lo mecanicamente complexo. Se *quiser*, claro, eleve-o a um PdN de suporte. Caso contrário, porém, dar um nome e uma motivação àquele cortesão não quer dizer que ele não será derrotado com um simples golpe.

------

### Bartender (Regular)

**Aspectos:** ***Meu bar é meu templo pessoal***

**Perícias:** Contatos Regular (+1)

------

------

### Capanga Treinado (Razoável)

**Aspectos:** ***Malandro das Ruas, Criminoso Violento***

**Perícias:** Lutar Razoável (+2), Atletismo e Vigor Regular (+1)

------

------

### Pesquisador Dimensional (Bom)

**Aspectos:** ***Comportamento Arrogante, Devoto do Desconhecido***

**Perícias:** Conhecimentos Bom (+3), Enganar Razoável (+2), Vontade e Empatia Regulares (+1)

------

#### PdNs de Suporte

PdNs de suporte têm nome e são mais detalhados que PdNs sem importância, tendo papel de apoio em seus cenários. Eles frequentemente possuem alguma característica única que os distingue dos outros por sua relação com um PJ ou PdN, uma competência ou habilidade única ou simplesmente por aparecerem no jogo com frequência. Muitas histórias de aventura apresentam algum personagem "tenente" que é o braço direito do vilão principal; esse é um PdN de suporte em termos de jogo. As faces importantes que você apresenta nos locais do jogo são PdNs de suporte, bem como quaisquer personagens nomeados em algum aspecto dos PJs.

PdNs de suporte são uma ótima fonte de dramas interpessoais, pois em geral são as pessoas com quem os PJs se relacionam, como amigos, companheiros, família, contatos e adversários notáveis. Embora possam nunca se tornar centrais na questão principal do cenário, são uma parte importante da jornada por sua ajuda, por criar problemas ou por fazer parte de um enredo secundário.

PdNs de suporte são criados como PdNs sem importância, com a diferença de receberem mais alguns elementos de personagem. Esses incluem o conceito, uma dificuldade, um ou mais aspectos adicionais, uma façanha e uma ou duas linhas de estresse com duas caixas cada. Devem também possuir algumas perícias (digamos quatro ou cinco). Se possuírem perícias que lhe dão direito a caixas extras de estresse, adicione-as também. Eles possuem uma consequência suave e, se quiser que sejam especialmente difíceis, uma consequência moderada.

As perícias de um PdN de apoio devem seguir uma distribuição em coluna. Como você vai definir quatro ou cinco perícias, trate apenas como uma coluna. Se o seu PdN possui uma perícia de nível Ótimo, preencha com uma perícia em cada nível positivo abaixo disso – uma Boa, uma Razoável e uma Regular.

-  **Nível de Perícia:** A perícia mais elevada de um PdN de suporte pode superar o melhor nível de perícia de um PJ em um ou dois, mas apenas se seu papel no jogo for criar oposição pesada – PdNs de suporte aliados aos PJs devem semelhantes de perícia. Outra forma de lidar com isso em histórias de aventura é fazer o “tenente” melhor em combate que o vilão principal, contrastando a força bruta à genialidade do vilão.
-  **Concessões:** PdNs de suporte não devem lutar até o último suspiro, se for possível. Ao invés disso eles abandonam o conﬂito, especialmente no início da história e principalmente se a concessão for algo do tipo “eles fogem”. Rendições desse tipo servem a vários propósitos. Primeiramente, implicam encontros futuros mais significativos com aqueles PdNs. Como a rendição é acompanhada de recompensa em pontos de destino, sua ameaça se torna maior para o próximo conﬂito.

Além do mais, é praticamente garantida a satisfação dos jogadores quando o PdN ressurgir. “Então, Esopo, nos encontramos novamente. Mas dessa vez não facilitarei as coisas para você”.

Finalmente, é importante mostrar aos jogadores que, quando as coisas não estiverem indo bem, abandonar um conﬂito é a melhor resolução. Um PJ que concede aqui e ali aumenta os riscos e introduz novas complicações, deixando o enredo cada vez mais envolvente e dramático.

------

### Topi Gegenschein, O Explorador

**Aspectos:** ***Criminoso Aposentado, Velho Demais Pra Isso, Esopo É Quase Meu Filho***

**Perícias:** Atirar Ótimo (+4), Lutar Bom (+3), Vontade Razoável (+2) e Atletismo Regular (+1)

**Façanha:** **Experiente em Campo de Batalha.** Pode usar Lutar para criar vantagem em situações táticas de grande escala.

------

------

### Sauri, a Invisível, Ladra Profissional

**Aspectos:** ***Vingança Acima de Tudo, Aceito Qualquer Serviço***

**Perícias:** Roubo Excepcional (+5), Furtividade Ótimo (+4), Conhecimentos Bom (+3), Lutar Razoável (+2), Vigor Regular (+1) (três caixas de estresse)

**Façanha:** **Infiltrado.** +2 em Furtividade quando em ambiente urbano.

------

------

### Shagas, o Testilhão

**Aspectos:** ***Minha Vida é Brigar e Comer, Minha Confiança Está na Minha Arma***

**Perícias:** Lutar Fantástico (+6), Vigor Excepcional (+5) (quatro caixas de estresse, 1 consequência suave extra para conﬂitos físicos), Atletismo Ótimo (+4)

**Façanha:** Não possui

------

#### PdNs Principais

PdNs principais são os PdNs que chegam mais próximos de um PJ. Eles possuem uma ficha de personagem completa, como os PJs, com cinco aspectos e a mesma quantidade de perícias e façanhas. Eles são os personagens mais importantes na vida dos PJs, pois representam as forças fundamentais da oposição ou aliados cruciais. Por seus diversos aspectos, eles também oferecem o maior leque de opções para invocar e forçar aspectos. Seus vilões principais em um cenário ou arco devem ser sempre PdNs principais, assim como quaisquer PdNs de maior importância nas histórias.

Por possuírem as mesmas características de um PJ, os PdNs principais exigirão mais de seu tempo e atenção que outros personagens. Sua criação depende do tempo disponível – se quiser, é possível passar por todo o processo normal de criação de personagem e elaborar sua história através de
fases, deixando algumas lacunas na sua interação com outros personagens para mais tarde.

Você também pode criar as coisas à medida que o jogo avança, criando uma ficha parcial com alguns aspectos que já tem em mente, as perícias mais essenciais e quaisquer façanhas desejadas. A partir desse ponto, preencha a ficha durante o jogo. Isso se assemelha à criação dos PdNs de suporte, exceto que mais detalhes podem ser acrescentados durante o jogo.

Se necessário, PdNs principais lutarão até o fim, dando muito trabalho os PJs.

Em relação aos níveis de perícia, os PdNs principais farão isso de duas formas – exatamente como os PJs, que evoluem com o progresso da campanha, ou superiores aos PJs, mas permanecendo estáticos enquanto os PJs se fortalecem o suficiente para superá-los. No primeiro caso, basta dar a eles perícias como as atuais dos PJs. No segundo caso, dê-lhes perícias o bastante para que fiquem ao menos dois níveis acima do limite inicial máximo no jogo.

Assim, se o maior nível de perícia dos PJs é Ótimo (+4), seu PdN principal mais poderoso deve possuir algumas colunas em nível Fantástico (+6) ou uma pirâmide com Fantástico como ápice.

Da mesma forma, um PdN particularmente significativo deve possuir mais de cinco aspectos para realçar sua importância na história.

------

### Zoidai Vasa,Rainha do Crime da Nebulosa Cadente

**Aspectos:** 

- ***Rainha do Crime da Nebulosa Cadente***

- ***Sigo os Passos de Minha Falecida Mestra***

- ***Confio Apenas Em Criminosos*** 

- ***O Tempo Está Ao Meu Lado***

- ***Fräkeline Será Minha Sucessora***

- ***Bourosoros, Meu Eterno Cruzador Espacial*** 

- ***Meu Império É O Único***


**Perícias:**

- Enganar e Lutar Fantásticos (+6)

- Atirar e Roubo Excepcionais (+5)

- Recursos e Vontade Ótimos (+4)

- Contato e Percepção Bons (+3)

- Ofícios e Furtividade Razoáveis (+2)

- Conhecimentos e Vigor Regulares (+1)


**Estresse:**

3 caixas físicas e 4 mentais

**Façanhas:**

- **Só Um Gênio Reconhece Outro:** Use Enganar ao invés de Empatia para criar vantagem em situações sociais.

- **Mestra do Engano:** +2 em Enganar para criar vantagem em conﬂito físico.

- **Contragolpe:** Se bem-sucedido com estilo em uma ação de defesa usando Lutar, você pode escolher inﬂigir 2 de danos em lugar de receber um impulso.


------

## Interpretando A Oposição

Aqui seguem algumas dicas de como usar os personagens da oposição em suas partidas.

### Equilíbrio

Lembre-se de buscar um ponto de equilíbrio entre dizimar os PJs e deixá-los caminhar sobre os inimigos (a menos que esta seja uma horda de capangas de segunda categoria). É importante ter em mente não apenas o nível das perícias dos PdNs na cena, mas também seu número e importância.

Criar uma oposição equilibrada é mais uma arte que uma ciência, mas veja algumas estratégias que funcionam bem.

-  Não supere os PJs em número a não ser que os PdNs possuam perícias em níveis mais baixos.
-  Se forem se agrupar contra um oponente maior, certifique-se que esse oponente tenha sua maior perícia dois níveis acima da melhor perícia que qualquer PJ possa usar em conﬂito.
-  Limite-se a um PdN principal por cena, a menos que seja um conﬂito decisivo para o fechamento de um arco. Lembre-se, PdNs de suporte podem possuir perícias tão elevadas quanto for preciso.
-  A maior parte da oposição que os PJs encontram em uma sessão devem ser PdNs sem importância, com um ou dois PdNs de suporte e PdNs principais ao longo do caminho.
-  PdNs sem importância e de suporte significam conﬂitos mais curtos pois desistem ou perdem rapidamente; PdNs principais implicam conﬂitos mais longos.

------

Claro que, se desejar, você também pode promover um de seus PdNs de suporte para um principal usando esse método. Isso é ótimo para quando um PdN de suporte se torna gradativamente ou repentinamente mais importante na história – normalmente por causa dos PJs – mesmo que seus planos para ele fosse outros

------

### Criando Vantagem para PdNs

É fácil cair na mesmice de usar a oposição como meio direto de atrapalhar as coisas para os PJs, atraindo-os para uma série de cenas de conﬂitos até que alguém seja derrotado.

No entanto, tenha em mente que PdNs podem criar vantagens assim como os PJs. Sinta-se livre para usar a oposição para criar cenas que não necessariamente focam em impedir os PJs de alcançarem seus objetivos, mas em buscar informações sobre os mesmos e conseguir invocações grátis.

Deixe os vilões e os PJs sentarem para tomarem um chá e então faça rolagens de Empatia. Ou no lugar de uma cena de luta em um beco escuro, deixe os PdNs se exporem, avaliarem as habilidades dos PJs e então fugirem.

Da mesma forma, tenha em mente que os PdNs têm a vantagem da casa em conﬂitos se os PJs vão até eles. Então, ao elaborar os aspectos de situação, você pode permitir ao PdN algumas invocações grátis se for razoável que ele tenha tido tempo para criar aqueles aspectos. Mas use esse truque de boa fé – dois ou três aspectos provavelmente já forçam os limites.

### Alterar Locais de Conﬂito

Sua oposição se tornará mais interessante se eles tentarem chegar aos PJs em diversos locais de conﬂito ao invés de simplesmente tomar o caminho mais direto. Lembre-se que há várias maneiras de chegar até alguém e que os conﬂitos mentais são tão significativos quanto os físicos como forma de atingi-los. Se a oposição possuir um leque de perícias diferente dos PJs, realce seus pontos fortes e escolha uma estratégia de conﬂito que lhes dê a melhor vantagem.

Por exemplo, alguém perseguindo Esopo provavelmente não quer enfrentá-lo fisicamente, porque Luta e Atletismo são suas maiores habilidades. No entanto, ele não está tão bem preparado contra tentativas de Enganar ou para lidar com ataques mentais mágicos. Bandu, por outro lado, se sente mais ameaçado pelos grandalhões, do tipo brutamontes, que podem atacálo antes que ele possa realizar seus truques psíquicos.

- [« Durante o Jogo](../durante-o-jogo/)
- [Cenas, Sessões e Cenário »](../cenas-sessoes-e-cenario/)

