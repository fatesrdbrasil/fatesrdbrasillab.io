---
title: Realizando Ações
layout: default
---

## Realizando Ações

Algumas das coisas que vocês, jogadores, podem fazer em uma partida de Fate, precisarão que os dados sejam rolados para checar se seu personagem é bem-sucedido ou não. **Você sempre lançará os dados quando outro personagem se opuser a você ou quando houver um obstáculo significativo em seu caminho**. Do contrário, apenas diga o que seu personagem está querendo fazer e assuma que aconteceu.

### Rolando os Dados

Quando você precisar rolar os dados, pegue quatro dados Fate e lance-os. Ao verificar a rolagem, leia cada + como +1, cada 0 como 0 e cada - como -1. Você conseguirá um resultado entre -4 e +4, mas normalmente -2 e +2.

Aqui alguns exemplos de totais de rolagem:

+ `--++`{: .fate_font} = +0
+ `0-++`{: .fate_font} = +1
+ `-+++`{: .fate_font} = +3
+ `--00`{: .fate_font} = -2

Note, porém, que o resultado nos dados não é o seu total final. Se o seu personagem possui uma perícia que seja apropriada para a ação, você adiciona o nível dela à rolagem.

Assim, sempre que lançar os dados, como saber o que significa o resultado? Que bom que perguntou.

------

As razões mais comuns para usar os dados em Fate são:

+ `O`{: .fate_font} [**Superar.**](../as-quatro-acoes/#superar) Ao Superar um obstáculo
+ `C`{: .fate_font} [**Criar Vantagem.**](../as-quatro-acoes/#criar-vantagem)  Ao criar ou liberar uma vantagem na forma de um aspecto 
+ `A`{: .fate_font} [**Atacar.**](../as-quatro-acoes/#atacar)  Ao atacar alguém em um conflito 
+ `D`{: .fate_font} [**Defender.**](../as-quatro-acoes/#defender)  Ao se defender em um conflito

------

### A Escala

Em Fate, usamos a **escala** de adjetivos e números para classificar os resultados dos dados, perícias e o resultado de uma rolagem.

Aqui está ela:

| **Nível** | **Adjetivo** |
|----------:|--------------|
|        +8 | Lendário     |
|        +7 | Épico        |
|        +6 | Fantástico   |
|        +5 | Excepcional  |
|        +4 | Ótimo        |
|        +3 | Bom          |
|        +2 | Razoável     |
|        +1 | Regular      |
|        +0 | Medíocre     |
|        -1 | Ruim         |
|        -2 | Terrível     |

Não importa o lado da escala que será utilizada – algumas pessoas lembram-se das palavras mais facilmente, outras preferem números e ainda alguns gostam de ambos. Então é possível dizer, “Consegui um resultado Ótimo” ou “consegui um +4”, o que significa a mesma coisa. O que importa é você ter certeza de que todos entenderam qual foi o resultado.

Os resultados podem ir acima ou abaixo da tabela apresentada. Encorajamos você a criar seus próprios termos para os resultados acima e abaixo da escala, como “Ridiculamente Incrível!” e “Ferrou!”.

### Interpretando Resultados

Ao rolar os dados, seu objetivo é conseguir o resultado mais alto que puder ou, ao menos, maior que o de seu oponente. A oposição virá de duas formas: **oposição ativa**, de alguém que está indo contra você ou **oposição passiva** de um obstáculo com um valor fixo na escala para que você o supere (Narradores podem decidir que seus PdNs possuem um valor de oposição passiva quando você não quiser rolar os dados por eles).

De forma geral, se você superar a oposição de acordo com a escala, você terá sucesso em sua ação. Um empate cria algum resultado interessante, mas não exatamente o que seu personagem desejava. Se vencer por uma grande diferença, algo a mais acontece (como um golpe mais efetivo em um combate).

Se a oposição não for superada, então você não terá sucesso em sua ação, obterá um sucesso parcial ou algo pior acontecerá para complicar a situação. Como antes, algumas ações possuem resultados especiais quando você falha em uma rolagem.

Ao superar uma oposição, a diferença entre seu resultado final e sua oposição é o que chamamos de tensão. Quando o resultado de sua rolagem for igual à oposição, você teve zero de **tensão**. Se alcançou um a mais que a oposição, então você teve uma tensão. Se a diferença for dois, então são duas tensões. Mais à frente neste livro falaremos sobre os diferentes resultados quando conseguir tensões em uma rolagem, e como isso o beneficia.

> Esopo se depara com uma armadilha eletrônica mortal, ativada acidentalmente durante uma exploração “de rotina” no Mortuário de Da'oth. Dezenas de lasers começam a se mover pelas paredes de um corredor e ele precisa passar por eles para chegar ao outro lado.
>
> Amanda, a narradora da sessão, diz “Essa é uma oposição passiva, pois é um obstáculo no seu caminho. Ela está se opondo com uma dificuldade Ótima (+4). Os seguidores de Da'oth realmente não queriam que ninguém roubasse o túmulo.”
>
> Léo coça a cabeça, dizendo: “Com meu Atletismo Bom (+3), vou tentar pular e rolar entre os lasers pra chegar do outro lado.”
>
> Ele pega os dados e os rola, conseguindo `-+++`{: .fate_font}, um resultado total de +2. Isso eleva seu resultado de Razoável (+2) para Excepcional (+5), o que é o bastante para superar a oposição e conseguir uma tensão.
>
> Amanda diz: “Com alguns saltos e acrobacias, você consegue chegar ao outro lado com pouco mais do que um arranhão. O sistema continua funcionando, o que significa que você vai ter que lidar com ele mais tarde para poder sair.”
>
> Léo responde, “apenas uma quarta-feira como outra qualquer", e Esopo continua sua jornada pela catacumba.

- [« A Ficha de Personagem](../ficha-de-personagem/)
- [Pontos de Destino »](../pontos-de-destino/)
