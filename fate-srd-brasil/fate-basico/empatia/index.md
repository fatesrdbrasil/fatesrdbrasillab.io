---
title: Empatia
layout: default
---

### Empatia

Empatia envolve saber e reconhecer alterações de personalidade ou sentimento em uma pessoa. É basicamente Percepção, mas voltada a questões emocionais.

- `O`{: .fate_font} **Superar:** Você não usa Empatia para se livrar de obstáculos diretamente – o que normalmente acontece é obter algum tipo de informação com ela e então agir com outra perícia. Em alguns casos, no entanto, você pode usar Empatia assim como se usa Percepção, para ver se consegue perceber uma mudança na atitude ou intenção de alguém.
- `C`{: .fate_font} **Criar Vantagem:** Use Empatia para ler o estado emocional de uma pessoa e ter uma visão geral de quem ela é, presumindo que você tenha algum tipo de contato pessoal com ela. Isso normalmente será feito na tentativa de acessar os aspectos da ficha de outros personagens, mas às vezes você também conseguirá criar aspectos, especialmente em PdNs. Se o alvo possuir alguma razão para notar sua tentativa de leitura, ele poderá se defender usando Enganar ou Comunicação.

Também é possível usar Empatia para descobrir as circunstâncias que permitirão realizar ataques mentais em alguém, detectando seus pontos fracos.

- `A`{: .fate_font} **Atacar:**Empatia não pode ser usada em ataques.
- `D`{: .fate_font} **Defender:**Essa é a perícia ideal para defender-se de manobras de Enganar, pois permite ver através de mentiras e detecte as intenções verdadeiras de alguém. Você também pode usá-la para defender-se de vantagens sociais que estejam sendo criadas e usadas contra você.

**Especial:** Empatia é a principal perícia a ser usada para ajudar outros a se recuperarem de consequências de natureza mental.

#### Façanhas Para Empatia

-  **Detector de Mentiras:** +2 em todas rolagens de Empatia realizadas para discernir ou descobrir mentiras, independente se direcionadas a você ou a outra pessoa.
-  **Faro para Problemas:** Você pode usar Empatia ao invés de Percepção para determinar o seu turno em um conﬂito, contanto que tenha a chance de observar ou conversar com os envolvidos por alguns minutos antes.
-  **Psicólogo:** Uma vez por sessão você pode reduzir um nível de consequência de alguém (severa para moderada, moderada para suave, suave para nenhuma) se for bem-sucedido em uma rolagem de Empatia com uma dificuldade Razoável (+2) para uma consequência suave, Boa (+3) para uma consequência moderada ou Ótima (+4) para severa. Você precisa conversar com a pessoa que está tratando por ao menos meia hora para que ela possa receber os benefícios dessa façanha e também não é possível usá-la em si mesmo (normalmente, essa rolagem é usada apenas para iniciar o processo de melhora, ao invés de alterar o nível da consequência).

- [« Contatos](../contatos/)
- [Enganar »](../enganar/)
