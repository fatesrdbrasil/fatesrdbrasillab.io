---
title: Capítulo 8 - Magia
layout: default
---


# Capítulo 8 - Magia

## Introdução

Quando o seu personagem realiza uma ação em jogo, espera-se que ela faça sentido para você. Você pode imaginá-la com certa clareza e possui o senso instintivo sobre como as coisas funcionam para que possa se divertir sem esquentar muito a cabeça. Considere o número de elementos envolvidos ao dar um soco em alguém: suas mãos estão livres? Você consegue
movê-las? Está perto o suficiente? Já preparou os punhos? 

Não paramos nem falamos sobre esses passos em jogo porque está subtendido que eles fazem parte da ação de dar um soco. Essa clareza diminui à medida que avançamos para áreas fora de nossa experiência, mas de modo geral é possível compreender a cadeia de ações que levam a um resultado.

Na magia isso não existe. Não temos a mesma base de experiência para usar de base quando começamos a lançar raios e trovões. Portanto, tentamos encontrar regras e lógica que tornem a magia mais familiar para nós, o que é um paradoxo. A magia é, em sua natureza, algo fictício e os escritores e criadores estão mais interessados em como ela os ajuda a contar histórias do que nas regras internas.

Jogos, por outro lado, precisam de regras. A consistência das regras cria a funcionalidade — sem lógica e coerência, é pura loucura.

A boa notícia é que há um ponto simples no qual você pode se escorar. Embora seja verdade que a magia seja uma conveniência entre autores, aqueles que a usam sem pensar no que fazem acabam produzindo uma fantasia simplista e sem graça. Dar regras à magia não serve apenas para tornar o jogo melhor, mas também para tornar a ficção melhor. Se você consegue encontrar o ponto onde essas duas prioridades se sobrepõem, então estará no caminho de criar um grande sistema de magia.

---

Qualquer ação suficientemente confusa é indistinguível de magia.

---

### O Básico


O primeiro fator diz respeito à natureza da magia. Magia neutra é uma força, como a eletricidade ou gravidade, que funciona semelhante a uma ferramenta, enquanto uma força diferenciada ou responde, ou tende a certos resultados. O exemplo mais comum disso seria uma magia que tende à luz e às trevas e que talvez funcione de formas diferentes em cada extremo do espectro. Neste caso a magia não é necessariamente uma força inteligente, mas possui tendências. Por exemplo, o fogo tende a queimar, terra tende a ser estável. Magia opiniosa deriva de alguém. Talvez de um deus ou anjo, talvez de alguma monstruosidade horrível fora do espaço e do tempo. Quem quer que sejam, possuem intenções próprias e a magia é uma de suas ferramenta na busca de seus objetivos. Há muito espaço para nuances — a magia pode ser neutra em seu uso, mas a fonte pode ser opiniosa. Por outro lado, se a magia realmente invoca ou canaliza esses seres, então a manifestação real da magia pode ser moldada por seus desejos.

### O Que é Magia?

Então, o que é magia e como ela funciona?

Não há uma resposta simples para isso e, apesar dessa ser a ideia, isso acaba sendo bastante frustrante. Poderíamos dizer que magia é uma forma de fazer coisas que de outra forma seria impossível, ou um meio alternativo de fazer coisas já possíveis, mas não é o bastante. Você poderia fazer como a Arthur C. Clarke e tratá-la como um tipo diferente de ciência. Talvez considerá-la como um sistema de preços, riscos e recompensas. Você pode considerá-la algo que vem de outra pessoa — alguém horrível ou incrível, dependendo do caso.

Você ainda deixaria lacunas, mas para facilitar a aplicação vamos organizar alguns tópicos chaves e reduzi-la a cinco fatores.

+ __Tom:__ A magia é uma força neutra, diferenciada ou possui opiniões próprias?
+ __Custo:__ A magia exige um preço, um risco ou nenhum dos dois?
+ __Limites:__ A magia segue regras rígidas? É flexível e livre? Quais são seus limites?
+ __Disponibilidade:__ A magia é universalmente disponível a ponto de qualquer um no cenário poder usá-la? É rara o bastante para apenas algumas pessoas a possuírem, possivelmente incluindo todos os PJs? É rara o bastante para apenas um ou dois PJs terem acesso, talvez?
+ __Fonte:__ De onde vem a magia?

#### Tom

O primeiro fator diz respeito à natureza da magia. _Magia neutra_ é uma força, como a eletricidade ou gravidade, que funciona semelhante a uma ferramenta, enquanto uma _força diferenciada_ ou responde, ou tende a certos resultados. O exemplo mais comum disso seria uma magia que tende à luz e às trevas e que talvez funcione de formas diferentes em cada extremo do espectro. Neste caso a magia não é necessariamente uma força inteligente, mas possui tendências. Por exemplo, o fogo tende a queimar, terra tende a ser estável. _Magia opiniosa_ deriva de alguém. Talvez de um deus ou anjo, talvez de alguma monstruosidade horrível fora do espaço e do tempo. Quem quer que sejam, possuem intenções próprias e a magia é uma de suas ferramenta na busca de seus objetivos. Há muito espaço para nuances — a magia pode ser neutra em seu uso, mas a fonte pode ser opiniosa. Por outro lado, se a magia realmente invoca ou canaliza esses seres, então a manifestação real da magia pode ser moldada por seus desejos.

#### Custo

O segundo fator trata dos custos no uso da magia. Para alguns, é essencial que a magia tenha um custo, que haja uma negociação em troca do poder. Esse custo pode ser literal ou simbólico, mas quando existe então fica subentendido que o poder tem um preço. Contraste isso com a magia que tem algum _risco_ associado a ela. Tal como um preço, isso coloca um limitador natural no uso da magia, mas com diferentes conjuntos de prioridades, especialmente se a magia for algo corriqueiro. O risco pode ser óbvio — como magias que possuem risco de explodir na sua cara — ou pode ser sutil — uma toxicidade cumulativa —, mas com que faça com que cada uso de magia seja considerado com cautela. Além disso, custos funcionam bem com magias diferenciadas e opiniosas, enquanto riscos são melhores com magia neutra ou opiniosa — onde o risco é “chamar a atenção dos seres com opiniões próprias”.

Não ter custo é uma questão curiosa e que não deve ser aplicada de forma muito literal. Geralmente há _algum_ custo, mesmo se for um chapéu pontudo e o custo da oportunidade de estudar magia ao invés de obter aquela pós-graduação. Estes são custos mundanos. É por isso que esta abordagem funciona melhor com magias neutras altamente regulamentadas. Ela se enquadra bem na vertente de “magia como ciência” ou em listas concretas de magias e efeitos — ou com regras para coisas como implantes cibernéticos, que são basicamente sistemas de magia com outra aparência. Seja qual for o caso, se não há custo nem ou risco, normalmente há algum outro fator limitador, mesmo que a magia seja bastante onipresente – como limites nos tipos de magia que uma pessoa possa usar.

#### Limites

O terceiro fator é um pouco enganoso, pois também está ligado à tolerância em sua mesa de jogo. Sistemas de magia rigorosos com listas de magia e efeitos pré-definidos atraem a alguns jogadores, enquanto outros preferem sistemas mais soltos e interpretativos. Há muitas possibilidades entre uma coisa e outra, como sistemas mais livres, mas limitados por algo como elementos ou esferas.

Seja qual for sua preferência, isso lhe ajuda a pensar sobre o que a magia não pode fazer. É mecanicamente mais fácil fazer um sistema onde a magia pode fazer _tudo_ — criar uma perícia mágica e então deixar os jogadores rolagem para tudo que descrever magicamente — mas a tendência é que isso se torne entediante. Limites formam boa parte do que torna magia algo mágico e, em retorno, são boa parte de como ela pode ser implementada no jogo.

#### Disponibilidade

O quarto fator lhe diz algo sobre o cenário, claro, mas também responde a uma pergunta importante sobre o equilíbrio e o foco do jogo. Um sistema de magia que esteja disponível a todos os personagens pode ser bem diferente de outro onde apenas um dos jogadores possa acessá-la. Se apenas um personagem possuir acesso à magia, então é importante que essa magia não seja tão poderosa ao ponto desse personagem ofuscar os outros, roubando a atenção, mas também não tão fraca que o jogador se sinta um idiota por ter feito essa escolha. Se, por outro lado, todos possuírem magia, há muito mais terreno para explorar. Quando todos podem fazer coisas incríveis, o equilíbrio deixa de uma dor de cabeça tão grande.

#### Fonte

O último fator é o mais e também o menos importante — não importa muito _qual_ a resposta, o que importa é que você _tenha uma_. Quanto melhor você entender a origem da magia, mais fácil será para entender o que ela pode fazer e, o mais importante, o que ela não pode. Não é obrigatório dividir essas explicações com seus jogadores e na verdade esta é uma questão em que aconselhamos discrição. Não é porque não pode confiar essa informação aos jogadores, mas porque seu sistema de magia vai parecer menos impactante quando tudo for explicado. Um pouco de mistério é essencial para a atmosfera mágica.

Observe que nenhum desses fatores pergunta “O que a magia faz?”, já que a resposta a isso é outra pergunta: O que ela _precisa fazer_? Esperamos que você tenha alguma ideia a respeito disso, porque se não souber, nada vai funcionar. “Porque eu preciso ter um sistema de magia” não é uma boa resposta.

### Magia e Fate

O objetivo destas regras fornecer as ferramentas para traduzir suas explicações e imaginação em uma estrutura que possa ser compartilhada. Isso pressupõe que você possua algo que _deseja_ compartilhar.

Fate é um jogo de representação. Ou seja, se você tem uma ideia na cabeça, ele te dá as ferramentas necessárias para expressá-la em jogo. Precisa que os personagens sejam capazes de realizar algo? Certifique-se que possuam uma perícia para isso. Há algum truque que deseja que realizem? Crie uma façanha. Gostaria de reforçar um elemento temático? Coloque um aspecto.

Essas mesmas ferramentas estão disponíveis para quando você quiser colocar magia em seu jogo. Porém, como para o restante do jogo, não há apenas uma ferramenta certa para isso. Dependendo de como a magia funcionaria em seu jogo, mecânicas diferentes podem ser adequadas para apresentá-la.

Os sistemas de magia que seguem possuem duas finalidades. Primeiro, cada um deles é um sistema de magia funcional que pode ser colocado em seu jogo ou modificado para servir às suas necessidades. Isso é importante, mas é quase secundário para o outro fim. Cada um desses sistemas são, também, um exemplo de como aplicar as mecânicas para criar certos efeitos.

Assim terminamos esse vai e volta. Se você conhece bem as regras do Fate, então será fácil tomar decisões para algo simples como um soco e apenas um pouco mais complicado criar seu próprio sistema de combate. Ao chegar no final desta seção, o objetivo é que você se sinta igualmente confortável em pegar conceito de magia que você vem imaginando e traduzi-la em mecânicas com a mesma facilidade que toma decisões corriqueiras.

#### Perícias Mágicas

Perícias são um caminho fácil para lidar com magia, qualquer que seja a perícia usada. A questão principal é se usará perícias existentes ou se será necessário criar uma nova perícia mágica. Cada uma dessas abordagens possui seus pontos fortes específicos e vale a pena considerá-los quando estiver desenvolvendo um sistema. Se pretende usar perícias existentes, então acabará com o desafio de cobrir todas as perícias. É possível, claro, optar por tornar apenas uma perícia mágica, mas é preciso ter cuidado para não criar perícias poderosas demais dessa forma.

Criar uma perícia nova pode resolver vários problemas, especialmente por poder criar várias perícias, se desejar diferenciar as diferentes áreas da magia. Há também um custo sutil nisso, já que comprar essa perícia significa negligenciar alguma perícia “real”.

Como não há resposta certa, quando em dúvida, crie uma nova perícia. Modificar uma perícia existente é mais trabalhoso e é algo que só deve ser feito quando você tiver uma visão realmente clara de sua funcionalidade.

#### Aspectos Mágicos


Aspectos possuem dois papéis importantes na maioria dos sistemas de magia — como porta de entrada e como uma expressão. Como porta de entrada, quase todo sistema de magia vai exigir que o personagem possua ao menos um aspecto que reflita sua tradição mágica ou fonte de poder. Embora haja exceções — como em mundos onde a “magia” é apenas uma tecnologia com caráter especial — saber fazer magia normalmente é importante o suficiente para o personagem se considerar usar um aspecto.

Aspectos também são uma ótima maneira de representar efeitos mágicos. A forma mais simples e fácil de incorporas isso é fazer um sistema de magia onde ela simplesmente amplia as possibilidades de criação de aspectos através de vantagens ou impulsos.

#### Façanhas Mágicas

Façanhas podem certamente servir como base para um sistema de magia, especialmente se as façanhas simplesmente fizerem coisas claras e explícitas. No entanto, esse modelos na maioria das vezes é melhor para um sistema de __poderes__. A diferença é pontual, mas poderes cabem melhor a monstros e super-heróis. Ainda assim, façanhas podem ser um jeito interessante de dar um toque especial no sistema de magia, mas os custos devem ser considerados com cuidado. Frequentemente, um sistema de magia tem um custo de recarga intrínseco, o que torna escolher façanhas algo perigoso. Ou os custos devem ser ajustados, ou as façanhas devem realmente valer a pena.

#### Extras Mágicos


Extras, do jeito que são escritos, são basicamente seu próprio sistema de magia. Um sistema de magia em si talvez forneça explicações e justificativas extras específicos, mas o sistema por si só é robusto e fácil de ser usado para qualquer tipo de efeito.


------

- [« Ações Suplementares](../acoes-suplementares/)
- [Sistemas »](../sistemas/)
