---
title: Companheiros e Aliados
layout: default
---

## Companheiros e Aliados


Jogadores podem escolher tanto companheiros quanto aliados na forma de extras, mas diferenciar um garoto prodígio de um esquadrão de agentes profissionais pode ser complicado. Seguem algumas formas de distinguir companheiros, que trabalham em colaboração com os PJs, de aliados treinados que podem ser chamados em situações específicas.

### Permissões e Custos


Embora tanto companheiros quanto aliados sejam extras (veja [Extras](../../fate-basico/extras), no Fate Sistema Básico), companheiros tendem vir de um relacionamento pessoal e aliados surgem de organizações e autorizações conseguidas com recursos. Por exemplo, Sherlock Holmes pode selecionar o aspecto ___Meu Bom Amigo Dr. Watson___, para adicionar Watson como seu companheiro, e ter o aspecto Contatos Incomuns em Baker Street para representar as crianças que usa como espiãs e mensageiras. Quanto ao custo, companheiros costumam impor custos sociais e de relacionamento com os personagens — pedindo que contribuam a causas importantes ou pedindo ajuda em momentos difíceis — enquanto aliados exigem algum pagamento direto, favores ou outros recursos. 

### Estresse e Consequências

Um ótimo recurso para diferenciar companheiros e aliados é usar caixas de estresse e consequências em conflitos. Companheiros são como personagens de suporte — forneça-lhes uma linha de estresse limitada com uma consequência suave (talvez uma moderada) e uma extrema, que podem mudar a planilha do personagem em um momento dramático. 

Aliados, por outro lado, são como capangas sem muita importância, que possuem uma caixa de estresse para cada membro do grupo. Infligir estresse em aliados os retira do conflito completamente ao invés de deixá-los com consequências que durarão várias cenas. Essa distinção torna mais difícil de derrotar uma gangue de aliados com um único ataque, mas faz com que companheiros sejam mais flexíveis e resistentes em diversas situações sociais e físicas, podendo utilizar suas consequências para defender os PJs.

### Aliados Permanentes ou Temporários

Companheiros quase sempre são uma parte permanente da planilha do personagem, mas aliados podem ser temporários, dependendo das necessidades do PJ. Por exemplo, um oficial da inteligência pode ter o aspecto Enviado Pelos Federais para possuir um grupo de agentes federais de plantão, ao custo de um ponto de recarga. É igualmente plausível que tal personagem possa abrir mão desses aliados permanentes para criar uma vantagem para a próxima cena, usando Contatos ou Recursos. Claro, aliados permanentes devem ser mais poderosos e com mais caixas de estresse, aspectos e façanhas adicionais do que aliados temporários, que são pouco melhores que um multidão genérica.


------

- [« Escala](../escala/)
- [Riqueza »](../riqueza/)
