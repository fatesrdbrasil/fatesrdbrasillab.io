---
title: 10 - Criando Campanhas
layout: default
---

# 10 - Criando Campanhas

## O Que É Um Arco?

Quando você senta para jogar *Fate*, pode ser que jogue apenas uma sessão. Essa é uma forma válida de jogar, mas vamos assumir que você queira jogar um pouco mais. O que precisa, nesse caso, é de um **arco**.

Um arco é uma história completa, com seus próprios assuntos, situações, antagonistas, inocentes e fim, contada ao longo de algumas sessões (em geral, algo em torno de duas a cinco). Você não precisa ter tudo planejado (e provavelmente não deveria, dado que histórias meticulosamente planejadas nunca sobrevivem ao contato com jogadores), mas é preciso ter uma ideia de onde as coisas começam e terminam, e o que pode acontecer no meio do caminho.

Uma analogia ficcional: um arco é como um livro. Ele conta sua história própria e termina quando ela se conclui; você cria alguma forma de desfecho e segue adiante. Às vezes você decide começar outra história, em outras seu livro é só o primeiro de uma série. É aí que temos uma campanha.

## O Que É Uma Campanha?

Quando temos múltiplos arcos conectados contados em sequência, e há uma história ou tema maior que atravessa todos eles, chamamos de **campanha**. Campanhas são longas, levando meses ou até anos para serem completadas (se isso chegar a acontecer).

Claro que isso não precisa ser tão assustador como parece. Sim, uma campanha é longa, extensa e bastante complexa. Você não precisa, no entanto, criar toda a história de uma única vez. Semelhante ao que acontece em um arco, você pode ter uma ideia vaga sobre como começar e finalizar a história (e isso será de bastante ajuda), mas você realmente precisa planejar um arco de cada vez.

Os jogadores são propensos a agitar um pouco as coisas e alterar o curso do que você planejou caso crie mais de um arco por vez, o que pode ser inútil e frustrante. Planejar o segundo arco de uma campanha baseado nos eventos do primeiro, no resultado final e no que os jogadores fizeram, porém, pode ser muito gratificante.

- [« O cenário em jogo](../o-cenario-em-jogo/)
- [Construindo um Arco »](../construindo-um-arco/)

