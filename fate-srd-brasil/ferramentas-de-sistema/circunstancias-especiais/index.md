---
title: Capítulo 6 - Circunstâncias Especiais
layout: default
---

# Capítulo 6 - Circunstâncias Especiais

## Perseguições

Em uma história de aventura, você eventualmente terá uma grande cena de perseguição. Isso é algo comum em aventuras e o Fate Sistema Básico visa esse gênero. O que você quer é um jeito de tornar a perseguição empolgante. Não é divertido — e perde-se boa parte da emoção — quando uma única rolagem determina quem escapa. Há algumas formas diferentes de abordar esse tipo de ação.

Usando apenas as regras padrão, podemos simular uma perseguição como um desafio (Fate Sistema Básico, página 137). Para uma perseguição básica na qual você não quer gastar muito tempo, as regras básicas são perfeitamente aplicáveis. Adicione alguns obstáculos nas rolagens dos jogadores e deixe-os superar os mesmos. Se forem bem-sucedidos em rolagens suficientes, conseguem escapar ou, se estiver perseguindo, conseguem alcançar seu alvo. É simples, mas não tão interessante.

Se houver mais oposição ativa, as regras de disputa (Fate Sistema Básico, página 141) são a próxima opção e podem funcionar bem. Elas permitem alguma tensão e, para fazer de uma forma na qual cada jogador não precise fazer rolagens individuais, fazemos a disputa como se fosse entre dois times.. O primeiro time a alcançar três vitórias vence. Se for
o grupo em fuga, conseguem escapar; se for o grupo perseguidor, alcançam o alvo, trazendo as devidas consequências. Ser pego frequentemente desencadeia um conflito.

As regras de disputas funcionam bem, mas três vitórias são razoavelmente fáceis de conquistar se o grupo de personagens for razoavelmente grande, fora que às vezes você pode querer um pouco mais de drama em suas cenas de perseguição. Eis um método alternativo, chamado de barra de perseguição. É uma mistura das regras de conflito e disputa.

Para começar, crie uma barra de estresse para perseguição. Esse será o seu cronômetro para a cena. O lado em fuga está tentando esvaziar as caixas de estresse, enquanto o lado perseguidor está tentando preenchê-las. O tamanho da caixa de estresse determina quão longa será a cena e onde você inicia nessa barra determina a dificuldade da fuga.

A primeira decisão a ser tomada é quando tempo durará a perseguição. Se procura por uma cena de tempo mediano, uma barra de estresse com 10 caixas pode servir. Se quiser menos de 10, talvez seja melhor realizar uma disputa normalmente. Se deseja que seja mais longa e envolvente, adicione mais estresse. Uma cena de perseguição com 14 caixas de estresse pode se tornar o maior evento da sessão e uma com 18 ou 20 pode ser o foco de toda uma sessão de jogo. Você provavelmente não deseja ir muito além disso ou arriscará prolongar sua cena de perseguição a ponto de entediar os jogadores.

Definir quantas caixas de estresse iniciarão marcadas determinará o quão próximo o perseguidor está de alcançar o grupo em fuga. Recomendamos começar com o estresse no meio (em 5 de 10 caixas, por exemplo). Você pode tornar as coisas mais difíceis para quem está em fuga colocado o estresse mais próximo do final da perseguição, por exemplo na sétima das 10 caixas. Da mesma forma, você pode facilitar as coisas para quem está escapando por iniciar a perseguição com poucas caixas marcadas. É bom evitar isso a não ser que os PJs sejam os perseguidores. Se a perseguição for menos complicada, use um desafio ou disputa ao invés da linha de estresse de perseguição.
    
Depois de criar a linha de estresse, determine quem age primeiro. Você pode fazer isso julgando a situação ou avaliando na planilha de personagem, de cada um dos lados, quem possui o maior nível na perícia relevante. Cada um dos lados terá seu turno, então quem inicia possui alguma vantagem, mas nada além disso.

Cada lado, no seu turno, realiza uma rolagem de perícia na tentativa de aumentar ou diminuir a linha de estresse. Isso é uma ação de superar e pode ser contra oposição passiva ou, mais comumente, por oposição ativa do outro lado da perseguição. Essas ações podem significar praticamente qualquer coisa e é mais emocionante se elas forem variadas e criativas. Rolagens de Condução para manobrar um veículo em uma perseguição podem ser descritas como os personagens realizando manobras para se livrar de obstáculos ou cortar o trânsito, por exemplo, enquanto Atletismo para perseguições a pé poderia representar os personagens escalando telhados e realizando manobras arriscadas nos ares. Várias outras perícias podem entrar em jogo permitindo diferentes tipos de ações. Você pode usar Enganar para fintar seu oponente, Lutar para nocautear alguém, Notar para perceber perigos e evitá-los enquanto seu oponente não conse- gue, Vigor para empurrar obstáculos no caminho, lançando-os na direção do oponente. Se um jogador tiver uma ideia para uma boa ação em qualquer perícia, ela deveria ser permitida.

Quando realizar a rolagem, o resultado determina o que acontece com a barra de estresse.

- Se falhar, seu oponente pode escolher criar um impulso que funcione contra você ou mover a linha de estresse uma caixa a seu favor.
- Se empatar, você pode escolher mover uma caixa de estresse a seu  favor, mas se o fizer, seu oponente ganha +1 em sua próxima jogada.
- Se bem-sucedido, você move uma caixa de estresse a seu favor.
- Se bem-sucedido com estilo, você move duas caixas de estresse a seu     favor ou então apenas uma caixa e ganha um impulso que você pode     usar contra seu oponente em sua próxima jogada.

Cada um dos lados terá seu turno para realizar as manobras e rolagens de perícias. Certifique-se que cada um dos personagens dos jogadores tenha a chance de contribuir na tentativa de escapar. Às vezes a rolagem será muito boa ou muito ruim, mas tudo bem. Mantenha a tensão com boas descrições, falando sobre os detalhes das manobras e seus resultados. Quando um lado ou outro for eliminado ou preencher a linha de estresse, a perseguição chega ao fim. Ou alguém foi capturado, ou alguém escapou.


------

- [« Nível de Poder](../nivel-de-poder/)
- [Conflito Social »](../conflito-social/)
