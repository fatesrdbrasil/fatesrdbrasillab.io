I"�<h1 id="5---perícias-e-façanhas">5 - Perícias e Façanhas</h1>

<h2 id="o-que-é-uma-perícia">O Que É Uma Perícia?</h2>

<p>Uma <strong>perícia</strong> é uma palavra que descreve a competência de alguém em algo – como Atletismo, Lutar, Enganar – que o seu personagem adquiriu por talento inato, treinamento ou anos de tentativa e erro. As perícias são a base de tudo que o seu personagem sabe fazer no jogo que envolva desafios (e dados).</p>

<p>As perícias são medidas através da escala de adjetivos. Quanto mais alto, melhor seu personagem é na perícia. De forma geral, a sua lista de perícias serve como uma visão geral do potencial de seu personagem – no que você é excelente, no que é bom e no que não é tão bom.</p>

<p>Definimos as perícias de duas maneiras em <em>Fate</em> – em termos de <em>ação em jogo</em> que você pode executar com elas e <em>contexto</em> no qual pode usá-las. Existem apenas algumas ações básicas de jogo, mas o número de contextos é infinito.</p>

<h3 id="ações-básicas">Ações Básicas</h3>

<p>Cobriremos isso com mais detalhes em <em>Ações e Resoluções</em>, mas aqui está uma referência para que você não precise ir até lá agora.</p>

<ul>
  <li><code class="language-plaintext fate_font highlighter-rouge">O</code> <strong>Superar:</strong> Com essa ação, você enfrenta um desafio, tarefa envolvente ou obstáculo relacionados à sua perícia.</li>
  <li><code class="language-plaintext fate_font highlighter-rouge">C</code> <strong>Criar Vantagem:</strong> Criar vantagens é uma forma de descobrir e criar aspectos e ganhar invocações gratuitas deles, seja  quando está descobrindo algo já existente sobre um oponente ou criando uma situação que lhe traz um benefício.</li>
  <li><code class="language-plaintext fate_font highlighter-rouge">A</code> <strong>Atacar:</strong> Você tenta ferir alguém em um conﬂito. Esse dano pode ser físico, mental, emocional ou social.</li>
  <li><code class="language-plaintext fate_font highlighter-rouge">D</code> <strong>Defender:</strong> Você tenta evitar que alguém lhe cause dano, passe por você ou crie vantagem para ser usada contra você.</li>
</ul>

<p>Algumas perícias também possuem um efeito especial, como conceder algumas caixas de estresse extras. Veja <a href="../vigor/">Vigor</a> e <a href="../vontade/">Vontade</a> na lista de perícias abaixo para ver mais exemplos.</p>

<p>Mesmo que haja apenas quatro ações que podem ser usadas em conjunto com as perícias, a perícia em questão fornece o contexto necessário para a ação. Por exemplo, tanto Roubo quanto Ofícios permitem que você crie vantagem, mas em contextos bastante diferentes – Roubo permite fazer isso se você estiver prestes a entrar em um local e Ofícios permite fazer isso quando você está avaliando algum objeto. As diferentes perícias permitem a você diferenciar as habilidades de cada PJ, permitindo que cada um contribua de forma única ao jogo.</p>

<h2 id="o-que-é-uma-façanha">O Que É Uma Façanha?</h2>

<p>Uma <strong>façanha</strong> é um traço especial do seu personagem que muda a forma como uma perícia funciona. Uma façanha é algo especial, uma forma privilegiada de um personagem usar uma perícia de maneira única, o que é bastante comum em vários cenários – treinamento especial, talentos excepcionais, a marca do destino, alterações genéticas, poderes natos e uma miríade de outras razões podem explicar o porquê de algumas pessoas serem melhores em suas perícias do que outros.</p>

<p>Diferentes das perícias, que são coisas que qualquer um poderia conseguir fazer na campanha, façanhas são únicas e personalizadas. Por essa razão, as próximas páginas falarão sobre como criar suas façanhas, mas há também uma lista de façanhas de exemplo para cada perícia padrão.</p>

<p>Adicionar façanhas a seu jogo fornece uma gama de personagens diferentes, mesmo que possuam as mesmas perícias.</p>

<blockquote>
  <p>Tanto Esopo quanto Fräkeline possuem a perícia Luta, mas Fräkeline também possui <strong>Mestre em Combate</strong>, que a torna mais apta a Criar Vantagem com essa perícia. Isso diferencia bastante os dois personagens – Fräkeline possui uma capacidade única de analisar e entender as fraquezas de seus inimigos de uma forma que Esopo não consegue.</p>

  <p>Podemos imaginar Fräk começando uma briga para testar os movimentos e ataques do inimigo, analisando cuidadosamente os limites de seu oponente antes de lançar um golpe decisivo, enquanto Esopo se contenta em apenas entrar quebrando tudo.</p>
</blockquote>

<p>Você também pode usar isso para separar um certo grupo de habilidades como pertencentes a um grupo de pessoas, se for algo que o cenário requer.</p>

<p>Por exemplo, em uma ambientação contemporânea, pode ser que possuir apenas uma perícia não seja suficiente para tornar alguém um médico profissional (a não ser, claro, que seja um jogo sobre médicos). No entanto, como uma façanha para outra perícia mais geral (como Conhecimentos), é possível que um personagem seja “o médico” se é o que o jogador deseja.</p>

<h3 id="façanhas-e-recarga">Façanhas e Recarga</h3>

<p>Adquirir uma nova façanha reduzirá sua recarga inicial em um ponto para cada façanha acima de três.</p>

<ul>
  <li><a href="../economia-de-pontos-de-destino/">« A Economia de Pontos de Destino</a></li>
  <li><a href="../criando-facanhas/">Criando Façanhas »</a></li>
</ul>
:ET