---
title: Fate Condensado - Começando
layout: default
---
# Começando

## Defina Seu Cenário

Qualquer jogo de Fate começa com a definição de seu cenário de jogo. Isto pode ser um conceito que o narrador está trazendo à mesa, um cenário popular com o qual os jogadores estejam familiarizados, ou um exercício de construção de mundo colaborativo envolvendo todos à mesa. A discussão sobre detalhes do cenário pode ser rápida e objetiva, pode envolver uma sessão completa e detalhada com todo o grupo – ou qualquer meio-termo entre estas.

Sua escolha de cenário forma a base do consenso da mesa sobre o que é verdade, e o que é aceitável em jogo e em conceitos de personagem. Se seu cenário não possui pessoas voando nele, então um jogador que decida fazer um personagem voador não deve ser aprovado. Se seu mundo envolve organizações sombrias e conspirações profundas, os jogadores podem esperar histórias livres de conflitos morais pré-estabelecidos e livre de ridículos palhaços assassinos. Vocês que decidem!

## Crie Seus Personagens

### Quem É Você?

Depois de decidir sobre o cenário, é hora dos jogadores criarem seus personagens — também chamados PJs (personagens dos jogadores). Cada jogador assume o papel de um dos heróis de sua história, controlando todas as suas ações. O jogador constrói o personagem que quer ver no mundo. Tenha em mente que os personagens de Fate são competentes, dramáticos e dispostos a se envolver com as aventuras à frente.

Seu PJ é composto de vários elementos:

>  - **Aspectos:** frases descrevendo quem seu herói é;
>  - **Perícias:** áreas em que seu herói tem um certo domínio;
>  - **Façanhas:** coisas notáveis que seu herói faz;
>  - **Estresse:** capacidade do seu herói de manter a calma e seguir em frente;
>  - **Consequências:** os ferimentos, físicos e mentais, que seu herói pode suportar;
>  - **Recarga:** uma medida do poder de afetar a narrativa de seu herói;
>  - **Toques finais:** os detalhes pessoais do seu herói.

### Aspectos 

Os aspectos são frases curtas que descrevem quem seu personagem é ou o que é importante para ele. Eles podem estar relacionados à qualidades físicas, mentais, história, crenças, treinamentos, relacionamentos ou até equipamentos particularmente importantes.

A primeira coisa a saber sobre os aspectos é que eles **são verdades dentro do jogo** (veja [aqui](../aspectos-pontos-destino/#aspectos-são-sempre-verdades) para uma discussão mais aprofundada). Em outras palavras, como você define seu personagem é uma verdade real na história que estiver contando. Se você escrever que o seu personagem é um _Atirador de Elite Presciente_, ele _será_ um atirador de elite presciente. Você definiu que ele pode ver o futuro e é especialista em atirar com um rifle. 

Você também poderá usar aspectos no jogo para alterar a narrativa. Eles lhe permitem melhorar suas rolagens de dados e definir fatos sobre o mundo. Por último, aspectos podem dar **pontos de destino** para você caso criem uma complicação ao seu personagem — então, foque em aspectos versáteis, que sejam de dois gumes, que possam ser usados contra você ou ao seu favor.

Para mais informações sobre aspectos e o que o torna bom, leia mais em Aspectos e Pontos de Destino [aqui](../aspectos-pontos-destino/).

**Para começar, você dará ao seu personagem cinco aspectos:**  um conceito, uma dificuldade, um relacionamento e dois aspectos livres. Comece pelo conceito e continue a partir dele.

#### Conceito

O seu aspecto Conceito é uma descrição geral do seu personagem que cobre os pontos principais. É a primeira coisa que você diria ao descrever o personagem para um amigo. 

#### Dificuldade

A seguir é a dificuldade do seu personagem – algo que torna a vida dele mais complicada. Pode ser uma fraqueza pessoal, laços familiares ou outros compromissos. Escolha algo que você gostará de interpretar durante o jogo!

#### Relacionamento

Seu relacionamento descreve uma conexão com outro PJ. Eles já podem ser conhecidos ou acabaram de se conhecer.

Um bom aspecto de relacionamento deve introduzir ou indicar um conflito ou pelo menos um desequilíbrio para gerar atritos nessa relação. Isto não significa que o relacionamento é abertamente antagônico, apenas que não é um mar de rosas. 

Se você preferir, você pode esperar para escrever o aspecto de relacionamento até que todos estejam mais ou menos próximos de concluir seus respectivos personagens.

#### Aspectos Livres

Você pode criar da forma que quiser seus dois últimos aspectos. Não há restrições além da obrigação de estarem condizentes com o cenário e a proposta do jogo. Escolha qualquer coisa que você acredite que fará seu personagem ser mais interessante, mais divertido de jogar ou mais conectado com o mundo em que ele vive.

### Perícias

Enquanto aspectos definem quem o seu personagem é, **perícias** mostram o que ele pode fazer. Cada perícia descreve uma ampla atividade que seu personagem pode ter aprendido através de estudo, prática ou simplesmente por talento nato. Um personagem com roubo é capaz, até certo ponto, de todo tipo de crime relacionado com a fina arte do roubo — verificar falhas de segurança de um local, burlar sistemas de segurança, bater carteiras e arrombar fechaduras.

Cada perícia tem um **nível**. Quanto maior o nível, melhor o personagem é naquela perícia. De forma geral, as perícias do seu personagem mostrarão em quais ações ele é habilidoso, quais ele vai conseguir se virar e quais não são o seu forte.

Você escolherá o nível das perícias do seu personagem, distribuídas em uma pirâmide com a perícia de maior nível em Ótimo (+4), da seguinte maneira:

  - Uma perícia em nível Ótimo (+4);
  - Duas perícias em nível Bom (+3);
  - Três perícias em nível Razoável (+2);
  - Quatro perícias em nível Regular (+1);
  - Todas as outras perícias em Medíocre (+0).

#### A Escala de Adjetivos

Em ***Fate Condensado*** – e Fate em geral – todos os valores são organizados em uma escala de adjetivos: 

| NÍVEL | ADJETIVOS    |
| ----- | ---------    |
| \+8   | Lendário     |
| \+7   | Épico        |
| \+6   | Fantástico   |
| \+5   | Excepcional  |
| \+4   | Ótimo        |
| \+3   | Bom          |
| \+2   | Razoável     |
| \+1   | Regular      |
| \+0   | Medíocre     |
| \-1   | Ruim         |
| \-2   | Terrível     |
| \-3   | Catastrófico |
| \-4   | Horripilante |

#### Lista De Perícias

Descrições para as perícias são encontradas abaixo.

  - Atirar
  - Atletismo
  - Comunicação
  - Condução
  - Conhecimentos
  - Contatos
  - Empatia
  - Enganar
  - Furtividade
  - Investigar
  - Lutar
  - Ofícios
  - Percepção
  - Provocar
  - Recursos
  - Roubo
  - Saberes
  - Vigor
  - Vontade

**Atirar:** Habilidade com todas as formas de combate à distância, sejam armas de fogo, facas de arremesso ou arco e flecha. Façanhas de Atirar permitem ataques de precisão, saques rápidos ou sempre ter uma arma à disposição.

**Atletismo:** Uma medida do potencial físico. Façanhas de Atletismo se concentram na movimentação (como correr, saltar, *parkour*) e em desviar de ataques.

**Comunicação:** Habilidade de criar conexões com outras pessoas e trabalhar em equipe. Enquanto a perícia Provocar está relacionada com a manipulação, a Comunicação possibilita interações sinceras, de confiança e boa vontade. Façanhas de Comunicação permitem influenciar multidões, melhorar relacionamentos ou fazer contatos.

**Condução:** Controlar veículos nas circunstâncias mais difíceis, fazer manobras radicais, ou simplesmente obter o máximo do seu veículo. Façanhas de Condução podem ser manobras exclusivas, um veículo especial de sua propriedade ou a capacidade de usar Condução no lugar de Perícias como Roubo ou Conhecimento sob certas circunstâncias.

**Conhecimentos:** Conhecimento de senso comum e educação formal, incluindo história, ciências e medicina. Façanhas de Conhecimentos geralmente se referem a áreas especializadas do conhecimento e habilidades médicas.

**Contatos:** Conhecimento das pessoas certas e conexões que podem ajudá-lo. Façanhas de Contatos fornecem aliados dispostos a ajudar e uma rede de informações em qualquer lugar do mundo.

**Empatia:** Capacidade de julgar precisamente o ânimo e as intenções de alguém. Façanhas de Empatia podem ser sobre avaliar o humor de uma multidão, identificar mentiras ou ajudar outras pessoas a se recuperarem de consequências mentais.

**Enganar:** Habilidade de mentir e trapacear de forma convincente e serena. Façanhas de Enganar podem aprimorar a capacidade de contar um tipo específico de mentira ou ajudar a criar identidades falsas.

**Furtividade:** Ficar oculto ou passar despercebido, e escapar quando precisar se esconder. Façanhas de Furtividade permitem que você desapareça à plena vista, se misture na multidão ou avance despercebido pelas sombras.

**Investigar:** Análise deliberada e cuidadosa, e resolução de mistérios. Esta perícia é utilizada para reunir pistas ou reconstruir uma cena de crime. Façanhas de Investigar ajudam a formar deduções brilhantes ou reunir informações rapidamente.

**Lutar:** Maestria em combate corpo a corpo, seja com armas ou punhos. Façanhas de Lutar incluem armas exclusivas e técnicas especiais.

**Ofícios:** Capacidade de criar ou destruir maquinários, construir engenhocas e realizar proezas inventivas ao estilo MacGyver. Façanhas de Ofícios permitem que você tenha gambiarras à disposição, dão bônus para construir ou quebrar coisas, e justificam o uso de Ofícios no lugar de perícias como Roubo ou Conhecimento sob certas circunstâncias.

**Percepção:** Habilidade de captar detalhes rapidamente, identificar problemas antes que aconteçam, e ser perceptivo, de maneira geral. Ela contrasta com Investigar, que é usada para uma observação lenta e deliberada. Façanhas de Percepção aguçam seus sentidos, melhoram seu tempo de reação ou dificultam que você seja surpreendido.

**Provocar:** Capacidade de incitar as pessoas a agirem da maneira que você deseja. Não é uma interação positiva, mas sim manipulativa e rude. Façanhas de Provocar permitem incitar oponentes a agir de forma imprudente, atrair agressões para si ou assustar inimigos (desde de que eles possam sentir medo).

**Recursos:** Acesso a coisas materiais, não apenas dinheiro ou propriedades. Pode refletir sua capacidade de pegar algo emprestado de amigos ou recorrer ao arsenal de uma organização. Façanhas de Recursos permitem que você use Recursos no lugar de Comunicação ou Contatos, ou permitem invocações gratuitas extras quando você paga por algo de  elhor qualidade.

**Roubo:** Conhecimento e capacidade de burlar sistemas de segurança, bater carteiras e cometer crimes em geral. Façanhas de Roubo dão bônus aos vários estágios de um crime, desde o planejamento até a execução e fuga.

**Saberes:** Conhecimento arcano, fora do escopo da perícia Conhecimento, incluindo tópicos sobrenaturais de qualquer tipo. É aqui que as coisas estranhas acontecem. Façanhas de Saberes incluem aplicações práticas de saberes arcanos como a conjuração de magias. Alguns cenários podem remover a perícia Saberes, substituí-la por outra ou combiná-la com a perícia Conhecimento.

**Vigor:** Força bruta e resistência física. Façanhas de Vigor permitem que você execute proezas de força sobre-humana, como usar seu peso a seu favor durante uma luta, e mitigue consequências físicas. Além disso, um nível alto em Vigor concede mais caixas de estresse físico ou caixas de consequências físicas ([clique para maiores detalhes](#estresse-e-consequências)).

**Vontade:** Resistência mental. Capacidade de resistir uma tentação ou de permanecer lúcido durante um evento traumático. Façanhas de vontade permitem que você ignore consequências mentais, resista à agonia mental provocada por poderes estranhos e mantenha-se firme ante a inimigos que tentem provocá-lo. Além disso, um nível alto de Vontade  concede mais caixas de estresse mental ou caixas de consequências mentais ([clique para maiores detalhes](#estresse-e-consequências)).

#### Listas Alternativas de Perícias.

A lista acima é um exemplo de como perícias podem funcionar em Fate e não tem a intenção de ser uma lista “oficial” que deve ser usada toda vez que você jogar. Você pode usá-la ou não, a escolha é sua. Qualquer perícia pode cobrir diferentes conjuntos de atividades, dependendo do cenário. Personalização é a chave\! Ajuste sua lista à história que você quer jogar.

Tendo isso em mente enquanto você constrói seu jogo em Fate, a primeira coisa a se pensar é se você manterá ou não a mesma lista de perícias. É possível que a granularidade da lista de exemplo padrão que nós demos acima não seja do seu gosto, então você poderá trabalhar com ela combinando, mudando ou dividindo algumas perícias da lista.

Aqui estão algumas coisas para você pensar:

  - A lista padrão tem 19 perícias e os personagens distribuirão valores acima de Medíocre (+0) em 10 delas. Se você mudar o número de perícias, você pode querer mudar como os níveis serão alocados.
  - Nossa lista padrão tem como foco responder a pergunta _“O que você pode fazer?”_, mas sua lista não precisa seguir essa linha. Você pode querer uma lista focada em _“Em que você acredita?”_ ou _“Como você faz as coisas?”_ (como nas abordagens de ***Fate Acelerado***), funções de uma equipe de enganadores e ladrões e por aí vai.
  - Níveis de perícias em Fate são estruturados para dar suporte a nichos de personagens. É por isso que, por padrão, os personagens começam com perícias em forma de pirâmide. Certifique-se de que a proteção destes nichos seja possível em qualquer lista que você criar.
  - A melhor perícia inicial deveria ter nível por volta de Ótimo (+4). Você pode alterar isso para baixo ou para cima se achar necessário, mas certifique-se de observar o que isso implica para os níveis de dificuldades e oposições que os PJs enfrentarão.

> Felipe decide que ele quer fazer um jogo de Fate sobre exploração espacial com uma lista menor de perícias focadas mais em ações. Ele decide por uma lista com 9 perícias: Lutar, Saber, Mover, Perceber, Pilotar, Esgueirar, Falar, Improvisar e Impor. Ele também gostou da ideia de ter uma forma de diamante para os níveis de perícias ao invés de uma pirâmide, então ele faz com que os personagens definam os valores de perícias iniciais da seguinte forma: 1 em Ótimo (+4), 2 em Bom (+3), 3 em Razoável (+2), 2 em Regular (+1) e 1 em Medíocre (+0). Seus PJs terão muitas perícias e competências centrais em comum devido ao meio largo do seu diamante, enquanto poderão desfrutar de uma proteção de nicho no topo da “ponta” do diamante.

Se você está considerando criar sua própria lista de perícias para seu jogo e procura por ideias para inspirar sua imaginação, veja mais [aqui](../regras-opcionais/#mudando-a-lista-de-perícias).

### Recarga

A recarga é o número mínimo de pontos de destino (mais detalhes [aqui](../aspectos-pontos-destino/#ganhando-pontos-de-destino)) com os quais seu personagem começa cada sessão de jogo. Os personagens iniciam com recarga de 3.

Em cada sessão, você começa com um total de pontos de destino no mínimo igual à sua recarga. Certifique-se de registrar com quantos pontos de destino você termina cada sessão de jogo – se você tiver mais pontos de destino do que seu valor de recarga, você começará a próxima sessão com os pontos de destino que você tinha ao final desta sessão.

> Por exemplo, Carlos recebeu muitos pontos de destino durante a sessão de hoje terminando com 5 pontos de destino. Seu valor de recarga é 2, então Carlos iniciará a próxima sessão com 5 pontos de destino. Entretanto, Edson terminou a mesma sessão com apenas 1 ponto de destino. Como seu valor de recarga é 3, ele iniciará a próxima sessão com 3 pontos de destino, e não com apenas aquele que havia sobrado. 

### Façanhas 

Enquanto todos os personagens tem acesso a todas as perícias – mesmo se na maioria delas forem Medíocres (+0) – seu personagem tem algumas **façanhas** únicas. Façanhas são técnicas, truques ou equipamentos legais que tornam seu personagem único e interessante. Enquanto perícias representam as competências gerais do personagem, façanhas são áreas de excelência mais específicas; a maioria delas dá a você bônus em circunstâncias determinadas ou permite que você faça algo que outros personagens simplesmente não podem fazer.

Seu personagem começa com 3 caixas de façanhas gratuitas. Você não precisa defini-las logo de cara, podendo preenchê-las à medida que joga. Você pode comprar mais façanhas gastando seu valor de recarga em 1 por façanha adquirida, não podendo ficar com valor inferior a 1 em recarga.

#### Escrevendo Façanhas

Você escreve suas próprias façanhas quando estiver criando um personagem. De forma geral, há dois tipos de façanhas.

**Façanhas que concedem bônus:** O primeiro tipo de façanha **concede +2 de bônus** quando usar uma perícia definida dentro de certos parâmetros, normalmente limitado a um tipo específico de ação (mais detalhes [aqui](../agindo-rolando-dados/#ações)) e um tipo de circunstância narrativa.

Escreva esse tipo de façanha da seguinte forma:

> Porque eu **\[descreva como você é incrível ou tem algum equipamento bacana\]**, eu ganho +2 quando eu uso **\[escolha uma perícia\]** para **\[escolha uma ação: superar, criar vantagem, atacar, defender\]** quando **\[descreva uma circunstância\]**.

**Exemplo de façanha que concede bônus:** Porque eu sou um **atirador treinado militarmente**, eu **ganho +2** quando eu uso **Atirar** para **Atacar** quando **tiver um alvo _Na Mira_**.

**Façanhas que mudam regras:** O segundo tipo de façanha **muda as regras do jogo**. Essa é uma categoria ampla que inclui, mas não é limitada aos seguintes:

  - **Trocar quais perícias são usadas em uma determinada situação**. Por    exemplo, um pesquisador pode usar Conhecimentos para realizar um    ritual enquanto qualquer outro personagem usaria Saberes;
  - **Usar uma ação com uma perícia que normalmente não serve para aquilo**.     Por exemplo, permitir que um personagem use Furtividade para atacar     um oponente pelas costas saltando das sombras (o que normalmente     seria feito com Lutar);
  - **Dar a um personagem um tipo diferente de bônus para perícia que seja     aproximadamente igual a +2**. Por exemplo, quando um orador competente     cria uma vantagem usando Comunicação, ele ganha uma invocação     gratuita adicional.
  - **Permitir que um personagem declare um pequeno fato que é sempre    verdadeiro**. Por exemplo, um sobrevivencialista sempre tem itens de     sobrevivência como fósforos consigo, mesmo em situações que isso não     seria provável. Esse tipo de façanha estabelece que você não precisa     invocar aspectos para declarar detalhes  narrativos (mais detalhes [aqui](../aspectos-pontos-destino/#invocações-para-declarar-detalhes-narrativos)) para     o fato em questão;
  - **Permitir que um personagem crie uma exceção específica nas regras**.    Por exemplo, um personagem pode ter mais duas caixas de estresse ou    mais um campo para consequência suave.

Escreva esse tipo de façanha da seguinte forma:

> Porque eu **\[descreva como você é incrível ou tem um equipamento bacana\]**, eu posso **\[descreva sua proeza incrível\]**, mas somente **\[descreva uma circunstância ou limitação\]**.

**Exemplo de façanha que muda as regras:** Porque eu **não acredito em magia**, eu posso **ignorar os efeitos de uma habilidade sobrenatural**, mas somente **uma vez por sessão**.

### Estresse e Consequências

Estresse e consequências são como seu personagem suporta as adversidades físicas e mentais de suas aventuras. Personagens têm pelo menos 3 caixas de um ponto para estresse físico e pelo menos 3 caixas de um ponto para estresse mental. Eles também ganham uma caixa para cada consequência: suave, moderada e severa.

Seu nível em Vigor afeta seu total de caixas de estresse físico. Vontade faz o mesmo com estresse mental. Use a seguinte tabela:

| VIGOR/VONTADE                 | ESTRESSE FÍSICO/MENTAL |
| ----------------------------- | ---------------------- |
| Medíocre (+0)                 | `3`{: .fate_font}                      |
| Regular (+1) ou Razoável (+2) | `3`{: .fate_font} `1`{: .fate_font}                      |
| Bom (+3) ou Ótimo (+4)        | `3`{: .fate_font} `3`{: .fate_font}              |
| Excepcional (+5) ou maior     | `3`{: .fate_font} `3`{: .fate_font} e uma segunda caixa para consequência suave especificamente para causas físicas ou mentais                      |

Você aprenderá como o estresse e as consequências funcionam durante o jogo em _“Recebendo Dano” (maiores detalhes [aqui](../desafios-disputas-conflitos/#recebendo-dano))_.

#### Peraí, não é isso o que eu me lembro\!

Em ***Fate Condensado***, estamos usando somente caixas de estresse de um ponto. Tanto o ***Fate Básico*** quanto o ***Fate Acelerado*** usam uma série de caixas com valores progressivos (uma caixa de 1 ponto, uma de 2 pontos, etc). Você pode usar a forma que preferir. Para esta versão, decidimos ficar com caixas de 1 ponto para tornar o jogo mais simples. Com o outro método, as pessoas acabavam se confundindo com mais facilidade.

Há alguns outros pontos sobre esse estilo de caixa de estresse que você deve ter em mente.

  - Como você verá [adiante](../desafios-disputas-conflitos/#estresse), com caixas de 1 ponto você poderá     marcar quantas quiser quando for atacado (já o sistema progressivo     do ***Fate Básico*** tem a regra que "você só pode marcar uma caixa por     golpe").
  - Esse estilo segue a linha do ***Fate Básico*** de separar as barras de     estresse Físico e Mental, em vez de usar uma única barra como no     ***Fate Acelerado***. Se você preferir uma barra unificada, adicione mais     3 caixas para compensar e use o _maior_ entre Vigor e Vontade para     aumentá-la como indicado acima.
  - Três pontos de absorção de estresse em uma barra não é muito\! Se os     personagens se sentirem frágeis durante o jogo, considere adicionar     1 ou 2 caixas à quantidade padrão. Isso dependerá da rapidez em que     as consequências acontecem (no estilo antigo, uma barra com     `2`{: .fate_font} absorve 2 a 3 estresses, `3`{: .fate_font} absorve 3 a 6 e     `4`{: .fate_font} absorve 4 a 10).

### Toques Finais

Dê um nome e uma descrição ao seu personagem e discuta a história dele com os outros jogadores. Se você ainda não escreveu um aspecto de relacionamento, faça isso agora.


------


- [« Introdução](../introducao/)
- [Agindo e Rolando os Dados »](../agindo-rolando-dados/)
