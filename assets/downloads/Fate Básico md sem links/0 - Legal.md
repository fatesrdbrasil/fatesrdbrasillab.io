# Fate Sistema Básico SRD

## Documento de Referência de Fate Sistema Básico

© 2018 Coletivo Solar e Fábio Silva

Versão 1.0

Este é o Documento de Referência do Sistema (*System Reference Document*) de Fate Acelerado para uso com licença Creative Commons. A seguinte atribuição deve ser fornecida em seu texto, onde você indica seus créditos (*copyright*), em tamanho equivalente ao seu texto de créditos (*copyright*):

Esta obra é baseada em Fate Acelerado, encontrado em www.solarentretenimento.com.br, traduzido, criado e editado por Alain Valchera, Fábio Silva, Fernando del Angeles, Gabriel Faedrich, Luís Henrique Fonseca e Matheus Funfas e licenciado para uso sob licença Creative Commons Atribuição 4.0 Internacional (http://creativecommons.org/licenses/by/4.0/). Documento de Referência do Sistema produzido por Fábio Emílio Costa, Fábio Silva e Jaime Rangel de S. Junior.