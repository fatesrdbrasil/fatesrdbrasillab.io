---
title: Perguntas Narrativas
layout: default
---

## Perguntas Narrativas

Agora que há um problema realmente significativo, podemos encorpar a situação um pouco e descobrir qual é a verdadeira questão que o cenário quer resolver – em outras palavras, quais são as questões pertinentes no cerne do problema?

Eis o que faremos: criar uma série de perguntas que o seu cenário responderá. Chamamos isso de **perguntas narrativas**, porque a história emerge naturalmente no processo de resposta.

Quanto mais perguntas você tiver, mais longo será o cenário. Uma a três perguntas provavelmente serão resolvidas em uma sessão. Quatro a oito levará umas duas ou três sessões. Mais que oito ou nove e você talvez possua perguntas até mesmo para o *próximo* cenário, mas isso não é de forma alguma um problema.

Recomendamos fazer tais perguntas com respostas limitadas a si/não, com um formato “Será que (personagem) pode/vai conseguir (objetivo)?”. Você não precisa seguir exatamente esse formato e pode expandir o formato da pergunta de várias formas, o que mostraremos em breve.

Cada problema criado possuirá uma pergunta bem óbvia: “Os PJs podem resolver o problema?”. Você precisa saber disso em algum momento, mas não precisa pular diretamente para essa parte – afinal, se trata do fim do cenário. Coloque outras questões antes dessa que agreguem complexidade ao cenário para então chegar a pergunta final. Tente descobrir o que torna o problema difícil de resolver.

Para criar essas perguntas, você provavelmente terá que reﬂetir sobre o problema que criou, ao menos um pouco, e descobrir detalhes como “quem, o que, como, onde, quando, por que”. Esse é o objetivo do processo.

> **Uma Conspiração Galáctica: Problema e Perguntas**
> 
> Fräkeline possui ***Se É Valioso, Poderia Ser Meu*** e Bandu tem ***O Império Quer Minha Cabeça***, o que sugere que o Império pode usar Fräk como isca para chegar até Bandu. Uma espiã disfarçada conta a Bandu sobre uma possível relação entre sua origem e uma relíquia do Museu que o Império deseja. Ao mesmo tempo, um contrato com um bom pagamento chega até Fräk, solicitando o roubo de um tesouro no Museu.
> 
> Duas perguntas óbvias sobre a história vêm à mente: *Será que Fräkeline consegue o tesouro? Será que Bandu é tentado pela informação?* Mas Amanda quer deixar essas respostas para o final, então pensa em outras questões.
> 
> Primeiramente, ela não sabe se eles concordam plenamente com a situação, então ela começa com: *Fräkeline vai aceitar o trabalho? Será que Bandu correrá atrás dessa pista?*
> 
> Em seguida ela precisa imaginar porque eles não vão direto ao problema. Ela decide que Fräk possui um rival anônimo na busca pelo tesouro do Museu (vamos chamá-lo de “O Cetro de Coronilla”) e um empregador misterioso que prometeu outra boa quantia atraente de créditos se o serviço for completado.
> 
> Bandu, por sua vez, precisa descobrir como chegará à informação que deseja sem se tornar alvo do Império, e provavelmente tentará descobrir precisamente quem está atrás dessa relíquia.
> Isso, então, gera mais três perguntas: *Será que Fräkeline pode encontrar o seu concorrente antes que ele a encontre? Poderá Bandu encontrar um aliado no Museu para defendê-lo? Poderá Bandu descobrir quem está armando para ele, sem sofrer maiores consequências?*
> 
> Então, como ela quer alguma tensão entre os dois, ela acrescenta mais uma pergunta referente à relação entre os personagens: Será que Fräkeline dará as costas a Bandu por causa de seus próprios interesses?

Note que cada uma dessas questões tem o potencial para moldar significativamente o enredo do cenário. Logo de cara, se Bandu decidir ignorar o aviso, você terá uma situação bastante diferente da que aconteceria se ele investigasse. Se a busca de Bandu for uma armadilha do Império, sua captura pode se tornar uma questão principal. Se Fräk decidir ajudar Bandu ao invés de perseguir o Cetro, então eles terão um outro problema para lidar, diferente do que foi pensado para Fräkeline.

Note também que algumas das perguntas possuem algo a mais que modifica a condição básica de “Pode X conseguir Y?”. O motivo pelo qual você pode preferir abordar o assunto dessa forma é o mesmo pelo qual você pode querer evitar rolar os dados às vezes – um resultado de apenas sucesso ou falha nem sempre é interessante, especialmente no caso das falhas.

Veja uma das questões para Fräkeline: “Será que Fräk pode encontrar o seu concorrente *antes que ele a encontre?*” Sem a parte realçada, a questão pode ficar bem entediante – se ela falhar na descoberta, essa pergunta poderá se tornar praticamente inútil na trama, e parte do jogo perde o foco. Isso não é bom.

Da maneira que expressamos, no entanto, está claro que teremos algo acontecendo se ela falhar – talvez ela não saiba quem é o seu rival, mas seu rival sabe quem *ela* é. Aconteça o que acontecer com o Cetro, o rival pode aparecer para caçá-la em um cenário futuro. Podemos também guardar a identidade do rival como algo que possa ser revelado a Fräkeline eventualmente, mas ainda podemos manter alguns conﬂitos e disputas no caminho enquanto um avalia as habilidades do outro.

Há também algum espaço para estender o material de um cenário para outro futuro. Talvez a identidade do oponente de Fräk não seja revelada nessa sessão – tudo bem, pois isso é um detalhe que sempre poderá aparecer mais à frente.

------

Se você acabar com muitas perguntas histórias (mais de oito, por exemplo), tenha em mente que você não precisa necessariamente responder a todas em um único cenário – você pode trazer à tona as questões que ficarem sem resposta como abertura para os cenários seguintes. Na verdade, é exatamente assim que você [cria bons arcos](../construindo-um-arco/) – você possui diversas perguntas e leva dois ou três cenários para responder a todas elas.

------

- [« Crie Problemas](../crie-problemas/)
- [Estabelecendo a Oposição »](../estabelecendo-a-oposicao/)

