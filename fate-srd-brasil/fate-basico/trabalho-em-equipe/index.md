---
title: Trabalho em Equipe
layout: default
---

## Trabalho Em Equipe

Personagens podem ajudar uns aos outros em ações. Há duas formas de fazer isso em *Fate* - combinando perícias, para quando todos estiverem fazendo o mesmo tipo de esforço na ação (como usar Vigor em conjunto para derrubar uma parede em ruínas) e acumulando vantagens, para quando o grupo faz os preparativos para que uma única pessoa tenha sucesso (como causar múltiplas distrações para que alguém use Furtividade para entrar numa fortaleza).

Quando combinar perícias, avalie quem possui o maior valor entre os participantes. Qualquer outro participante que possuir a mesma perícia em um nível ao menos Regular (+1) adiciona +1 na jogada do personagem com maior nível e apenas esse personagem faz a rolagem. Logo, se três pessoas estiverem lhe ajudando e você possui o maior nível na perícia, você receberá um bônus de +3 na sua rolagem.

Se você falhar em uma rolagem de perícia combinada, todos os participantes compartilham os custos – qualquer complicação que afetar um personagem afetará todos eles ou todos recebem consequências. Como alternativa, você pode impor um custo que afete todos os personagens igualmente.

> Para fugir dos guardiões, o grupo decide que é mais fácil apenas combinar perícias.
>
> Dos três PJs, Fräkeline possui o maior valor em Atletismo (+4). Esopo possui um nível Bom (+3) e Bandu, Regular (+1), então cada um deles oferece um bônus de +1. Com ajuda de seus companheiros, Fräkeline rolará com um nível Fantástico (+6).
>
> Os guardiões do templo possuem apenas Regular (+1) em Atletismo, mas há cinco deles, então eles rolarão com Excepcional (+5) para essa disputa.

Quando você acumula vantagens, cada pessoa faz uma ação de criar vantagem normalmente e concede qualquer invocação gratuita que conseguiu a um único personagem. Lembre-se que múltiplas invocações de um mesmo aspecto podem ser cumulativas.

> Bandu e Fräkeline querem ajudar Esopo a acertar a nave de Rakir, que está fugindo, com um tiro de canhão orbital.
>
> Tanto Fräkeline quanto Bandu rolam para criar vantagem em seus turnos, resultando em três invocações gratuitas de ***Tiros de Distração*** com os canhões secundários, com o sucesso na rolagem de Bandu e o sucesso com estilo de Fräk, que adiciona mais duas invocações grátis.
>
> No turno de Esopo, ele usa toda essa vantagem para um ataque Fantástico (+6).

- [« Conflitos](../conflitos/)
- [Conduzindo o jogo »](../conduzindo-o-jogo/)

