I"�<h2 id="custos-de-façanhas">Custos de Façanhas</h2>

<p>Em Fate Sistema Básico você recebe três façanhas gratuitas, recarga inicial 3 e pode comprar até duas façanhas adicionais ao custo de uma recarga cada. Essa é a forma padrão de lidar com façanhas, mas isso não quer dizer que seja a única forma. Cada um desses componentes — número inicial de façanhas, recarga inicial e custo por façanha — é uma característica que aumentada ou diminuída, tornando as façanhas mais ou menos comuns. As alterações feitas determinarão, em parte, seu tipo de jogo, assim como terão um impacto nos Marcos.</p>

<p>Mais façanhas significa PJs mais poderosos. Um PJ com mais façanhas pode conseguir grandes bônus nas rolagens, usar suas perícias mais elevadas com regularidade e quebrar as regras com frequência. Isso leva a partidas mais do tipo <em>pulp</em>, fora da realidade e até mesmo fantásticas. Se é isso que você deseja, ótimo! Considere, também, que fornecer mais façanhas aos PJs, além de conceder benefícios e exceções às regras, também torna os personagens mais complexos. Se os seus jogadores não estão acostumados a essa complexidade, isso pode reduzir o ritmo de jogo e tornar as coisas <em>menos</em> empolgantes em vez de mais.</p>

<p>Por outro lado, fornecer aos PJs poucas façanhas pode tornar os personagens mais simples e fáceis de usar durante o jogo, mas os torna menos competentes. Um PJ com apenas uma façanha, por exemplo, possui apenas uma carta na manga, um único movimento especial. Isso pode significar um jogo mais denso ou com personagens claramente definidos e de nicho fixo, mas pode desapontar jogadores que querem ser bons em muitas coisas. Os PJs ainda serão competentes, mas não <em>tão</em> competentes.</p>

<h3 id="ajustando-as-façanhas-iniciais">Ajustando as Façanhas Iniciais</h3>

<p>Esta é uma forma fácil de conceder mais ou menos façanhas aos PJs. Reduzir a zero significa que um jogador começa sem nenhuma façanha e tem que pagar o preço para cada façanha que queira adquirir. Adquirir façanhas pode parecer caro, mas também as torna individualmente mais importantes para os PJs. Ajustá-las da outra forma, fornecendo aos PJs mais façanhas iniciais, dá aos jogadores muitas escolhas divertidas para fazer logo de começo sem nenhum custo. Isso também aumenta o nível básico de poder de cada PJ, tornando mais provável que se tornem muito competentes em uma grande variedade de tarefas, ou incrivelmente bons em uma ou duas. Além disso, causa a impressão de que façanhas são mais baratas, o que encoraja os jogadores a adquirir ainda mais.</p>

<p>Ao ajustar as façanhas iniciais, fique de olho no que os seus jogadores desejam. Todos os jogadores aceitaram o fato dos personagens serem menos poderosos ou mais complexos? Nem todos fazem questão de ganhar sempre ou gostam de tomar dúzias de decisões antes do jogo começar.</p>

<h3 id="ajuste-na-recarga-inicial">Ajuste na Recarga Inicial</h3>

<p>Destas três opções, ajustar a recarga é provavelmente a que possui maior impacto ao jogar, porque afeta diretamente a economia de pontos de destino que é central ao jogo. Conceder mais pontos de destino a cada sessão significa que os jogadores provavelmente resistirão contra tentativas de forçar e invocarão aspectos com mais frequência. Os jogadores ganham mais controle sobre a história, o que pode ser uma coisa boa, mas também um pouco mais difícil para criar desafios à altura.</p>

<p>Reduzir a recarga tem o efeito oposto – os jogadores possuem menos controle sobre a história, pois têm menos pontos de destino em mãos. Precisam aceitar forçar aspectos e não invocarão aspectos com frequência. Isso também significa que confiarão mais em invocações grátis de vantagens criadas, o que pode prolongar as cenas de luta mais complexas.</p>

<h3 id="ajustando-o-custo-das-façanhas">Ajustando o Custo das Façanhas</h3>

<p>Façanhas custando um ponto de recarga é uma das possibilidades, mas não a única. Você pode ajustar esse valor levemente ao aumentar ou reduzir o custo de recarga que um jogador deve pagar por uma façanha. Talvez façanhas custem dois pontos de recarga ou talvez um único ponto de recarga compre duas façanhas.</p>

<p>Há outras formas de lidar com os custos de façanhas. Talvez o jogador tenha que abrir mão de um ponto de perícia para comprar uma façanha, baixando uma perícia Bom (+3) para Razoável (+2) para assim poder comprar a façanha que deseja. Talvez seja preciso abrir mão de um aspecto ou dedicar um aspecto para conseguir a façanha. Conectar um aspecto a uma façanha pode criar um vínculo entre o jogador e a nova façanha e estabe- lece um bom limite máximo no número de façanhas por jogador.</p>

<p>Outra forma é tornar as façanhas gratuitas ou com baixo custo, mas incluir o custo dentro de cada façanha. Talvez cada façanha custe um ponto de destino para ativar, cause recebimento de estresse, custe uma ação para ativar ou ceda um impulso ao inimigo. Você pode incluir quantos custos desejar no uso de uma façanha.</p>

<p>Tenha em mente que façanhas possuem seus custos e fornecem benefícios porque custam um ponto <em>permanente</em> de recarga do personagem, reduzindo seus pontos de destino. Esse ponto de destino não poderá ser convertido em um bônus de +2 ou algum outro benefício durante o jogo. Então, se você planeja alterar o custo de uma façanha, pense em como ajustar os benefícios obtidos para equilibrar com o que foi “perdido” no custo.</p>

<h3 id="equilíbrio-entre-opções">Equilíbrio Entre Opções</h3>

<p>A maioria das soluções exige o ajuste de múltiplos detalhes para encontrar o ponto ideal para cada grupo em particular. Isso é completamente normal. Este é o seu jogo e cabe a você e a seu grupo determinar o que funciona bem para vocês.</p>

<hr />

<ul>
  <li><a href="../facanhas/">« Façanhas</a></li>
  <li><a href="../o-grande-jogo/">O Grande Jogo »</a></li>
</ul>
:ET