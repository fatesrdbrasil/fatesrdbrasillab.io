---
title: Construindo Um Arco
layout: default
---

## Construindo Um Arco

A maneira mais fácil de construir um arco é não construir um – [anteriormente](../estabelecendo-a-oposicao/) sugerimos que se você possui muitas perguntas narrativas em seu cenário, talvez possa reservar algumas delas para o próximo. Então, no próximo cenário, adicione algumas novas perguntas às que ainda estão sem resposta. Misture tudo isso e você terá material para três ou quatro cenários sem precisar de muito trabalho adicional. Além disso, será possível incorporar mudanças pensadas de acordo com os aspectos dos personagens, ao invés de criar um plano que será interrompido.

Dito isto, sabemos que alguns Narradores preferem ter um senso de estrutura que os deixem seguros em campanhas longas. Ao construir arcos, recomendamos usar o mesmo método para construir cenários apresentado no capítulo anterior, mas alterando o alcance das perguntas sobre a história. Em vez de focar nos problemas simples e imediatos que os PJs possam resolver, pense em problemas gerais, onde os PJs terão de lidar com coisas pequenas para terem a chance de resolverem os grandes.

O melhor lugar para procurar por problemas para sua campanha é nos detalhes sobre os lugares e organizações que você já imaginou durante a criação do cenário. Se você ainda não criou algum local ou grupo em particular, agora pode ser uma boa hora para isso, para que assim você tenha material para um arco.

> Amanda decide que quer fazer um arco maior para cada PJ.
> 
> Para Bandu, seu ***O Império Quer a Minha Cabeça*** torna tudo mais fácil – ela decide que talvez o Império tenha planos mais sinistros para ele do que matá-lo, talvez envolvendo o estudo de seus poderes mentais para fins nefastos.
> 
> Ela precisa se concentrar nas perguntas sobre a história que são mais gerais e pensar um pouco sobre como resolvê-las. Após pensar um pouco, ela escolhe:
> 
> -  Será que Bandu se livrar da perseguição do Império? (Isso permite que ela faça cenários individuais com Bandu tentando apagar sua identidade ou algo assim)
> -  Será que Bandu irá se aliar aos seus inimigos para atacar o Império? (Isso permite que ela crie cenários individuais sobre cada um de seus rivais)
> -  Bandu conseguirá fazer um acordo com seus perseguidores?
> -  Qual é o plano final do Império? Será que serão bem-sucedidos? (resposta que termina o arco)

Em seguida, faça o mesmo processo para descobrir quais são os PdNs que oferecerão a oposição, tendo em mente que sua inﬂuência é mais abrangente em um arco do que em um cenário.

- [« Criando Campanhas](../criando-campanhas/)
- [Construindo Campanhas »](../construindo-campanhas/)

