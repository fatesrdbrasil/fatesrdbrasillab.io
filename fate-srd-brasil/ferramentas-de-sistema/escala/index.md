---
title: Escala
layout: default
---

## Escala

Se você espera que os temas e questões de seu jogo causem conflitos entre entidades de diferentes tamanhos e escalas — dragão contra cavaleiro, nave pequena contra estação espacial ou pequenas empresas contra megacorporações, por exemplo — considere estas regras. Caso contrário, é melhor pular esta sessão.

Primeiro, defina a **escala** desejada; três ou quatro níveis devem bastar. O que essa escala representa dependerá da natureza da campanha e das histórias que deseja contar, mas sempre reflete coisas coisas progressivamente maiores ou mais poderosas no cenário e ela frequentemente aparecerá nos conflitos.

Quando duas entidades importantes do cenário entram em conflito, a diferença entre seus níveis entra em jogo. Para cada nível de diferença entre eles, aplique um ou ambos os efeitos a seguir no maior dos dois: +1 em uma rolagem de ataque _ou_ +1 em uma rolagem de defesa Causar +2 tensões de dano em um ataque bem-sucedido ou reduzir um dano recebido em 2
A aplicação desses efeitos depende do que fizer sentido no contexto.

Claro, se o conflito for entre duas entidades de mesmo porte ou nível de escala, então nenhum dos efeitos se aplicam. Eles só entram em jogo quando os níveis são _desiguais_.

**Escala como Extra:** A maioria das coisas em seu cenário terá um nível de escala baseado no que são de acordo com o senso comum — todas as gangues são Locais, por exemplo — mas você pode dar aos jogadores a chance de alterar a escala de seus personagens, posses ou recursos, na forma de um extra que possam receber, se isso se enquadrar no seu conceito de personagem.

---

O jogo de Eric é sobre guerra entre sociedades secretas de tamanho e influência variadas. Sua escala possui níveis chama-
dos: Local (limitado a uma única cidade), Regional (várias cidades) e Generalizado (todas as cidades).

Se a Gangue do Machado (nível: Local) inicia um ataque ousado contra a Associação Beneficente dos Viajantes Celestiais (escala: Regional), a Gangue do Machado passará por apuros, em termos de recursos e de pessoal, contra a Associação que é melhor financiada. Faz sentido conceder à Associação um bônus de +1 em uma rolagem de defesa de Lutar.

---


------

- [« Outros Dados](../outros-dados/)
- [Companheiros e Aliados »](../companheiros-e-aliados/)
