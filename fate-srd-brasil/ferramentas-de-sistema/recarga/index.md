---
title: Recarga
layout: default
---

## Recarga

Se quiser trabalhar melhor o desenvolvimento dos personagens no seu jogo, você pode permitir que eles dediquem uma cena à ativação de sua recarga, adicionando algum drama entre cenas envolventes de ação. Cenas de recarga podem ser:

**Cenas de Flashback:** Sempre que for apropriado, um jogador pode declarar que um evento específico ativou uma cena de flashback. Com a ajuda do grupo, o jogador narra o flashback sobre o passado do personagem, revelando um detalhe surpreendente ou interessante que tenha conexão com o presente, renovando o foco do personagem ou colocando-o de volta nos eixos.

**Cenas de Recuperação:** A critério do Narrador, os de personagens voltam às suas vidas normais para descanso e repouso, no intuito de perseguir seus próprios interesses ou retomar as energias. São cenas fantásticas para o Narrador adicionar novos PdNs à história.

**Cenas de Reflexão:** Um jogador declara uma cena de reflexão enquanto os outros personagens fazem perguntas que revelam informações importantes e interessantes sobre a história do personagem refletindo. Isto é perfeito para histórias cheias de ação, onde o tempo real fora da aventura não é realista.

Essas cenas podem trazer pontos de destinos adicionais aos PJs no meio da sessão, deixando as coisas empolgantes, ou repor completamente a recarga do início da sessão. Sinta-se livre para usar apenas um tipo ou permitir que seus jogadores usem todas.


------

- [« Zonas](../zonas/)
- [Modificando O Cenário: Grandes Mudanças »](../modificando-cenario-grande-mudancas/)
