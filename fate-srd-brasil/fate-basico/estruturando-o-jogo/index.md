---
title: 2 - Estruturando o Jogo
layout: default
---

# 2 - Estruturando o Jogo

## O Que É Necessário Para Uma Boa Partida de Fate?

É possível usar *Fate* para contar histórias de todos os tipos, com uma grande variedade de temas. Não há uma ambientação padrão aqui; você e seu grupo inventarão o que quiserem. As melhores partidas de Fate, no entanto, possuem certas ideias em comum, as quais pensamos ser os melhores casos para os quais o jogo foi desenvolvido.

Independente se seu interesse é fantasia, ficção científica, super-heróis ou suspense policial, Fate funciona melhor quando utilizado para contar histórias sobre pessoas *proativas*, *competentes* e *dramáticas*.

### Proatividade

Personagens em uma partida de Fate devem ser proativos. Eles possuem uma variedade de habilidades utilizáveis na resolução ativa de problemas e não se sentem intimidados ou relutantes em usá-las. Não devem ficar sentados esperando por uma solução para determinado problema – eles saem e gastam suas energias, assumindo riscos e superando obstáculos para atingir seus objetivos.

Isso não significa que eles não têm um plano ou estratégia ou que são descuidados. Apenas significa que mesmo o mais paciente dentre eles irá, uma hora ou outra, se erguer e tomar ação, seguindo por algum caminho.

Toda partida de Fate deve oferecer aos personagens oportunidades claras de serem proativos na solução dos problemas, além de diversas formas de resolvê-los. Um jogo sobre bibliotecários gastando todo o seu tempo lendo tomos e aprendendo coisas não tem a ver com *Fate*. Um jogo sobre bibliotecários *usando um conhecimento esquecido para salvar o mundo*, sim.

### Competência

Personagens em uma partida de *Fate* são bons no que fazem. Não são tolos que fazem papel de ridículos quando tentam fazer as coisas – são altamente habilidosos, talentosos ou treinados individualmente para serem capazes de realizar mudanças significativas no mundo em que vivem. São as pessoas certas para o trabalho e se envolvem em situações de crise pois têm boas chances de conseguirem resolvê-la.

Isso não significa que serão sempre bem-sucedidos ou que suas ações não possam ter consequências indesejadas. Isso quer apenas dizer que ao falharem, não será por erros estúpidos ou porque não estavam preparados para assumirem os riscos.

Qualquer partida de *Fate* deve tratar os personagens como pessoas competentes, dignas dos riscos e desafios lançados a eles. Um jogo sobre um grupo de lixeiros que é forçado a combater supervilões que constantemente os humilham não entra no espírito de *Fate*. Um jogo sobre *lixeiros que se tornam um incrível esquadrão anti-villões*, é.

### Drama

Personagens em uma partida de *Fate* levam vidas dramáticas. Os riscos que correm são mais altos, tanto no sentido das coisas com as quais terão que lidar em seu mundo, quanto das coisas que ocorrem em suas cabeças. Assim como nós, eles têm problemas pessoais e lutam contra seus próprios transtornos e, embora as circunstâncias externas de suas vidas possam ser muito mais grandiosas que as nossas, ainda podemos compreendê-los e simpatizar com eles.

Isso não significa que eles gastam todo o tempo lamentando misérias e dores ou que tudo em suas vidas está sempre beirando o caos. Quer apenas dizer que suas vidas exigem que façam escolhas difíceis e arquem com as consequências — em outras palavras eles são, no fundo, *humanos*.

Qualquer partida de Fate deve possibilitar e criar oportunidades para o drama ao redor e entre os personagens, além de dar a chance de formar uma ligação humana com eles. Um jogo sobre aventureiros que lutam estupidamente contra um número de bandidos cada vez maior não é *Fate*. Porém, um jogo sobre *aventureiros que tentam levar uma vida normal apesar de estarem destinados a lutar contra o mal definitivo* é *Fate*.

------

### Ao Estruturar Uma Partida:

  -  **Ambientação:** decida qual a aparência do mundo que cerca os protagonistas.
  -  **Dimensione:** decida quão épica ou pessoal sua história será.
  -  **Assunto:** decida quais dramas e pressões cairão sobre os protagonistas para fazê-los agir.
  -  **PdNs:** decida quais são as pessoas e locais importantes.
  -  **Perícias e Façanhas:** decida que tipos de coisas os personagens são capazes de realizar.
  -  **Criação de Personagem:** crie os protagonistas.

------

- [« Começando a Jogar](../comecando-a-jogar/)
- [Estruturando Seu Jogo »](../estruturando-seu-jogo/)
