I"`<h2 id="criação-rápida-de-personagem">Criação Rápida De Personagem</h2>

<p>Se você deseja criar um personagem sem tantos detalhes e começar a jogar logo, é possível deixar a maioria dos campos em branco e preencher durante a partida. Aqui está o mínimo que precisa para começar a jogar:</p>

<ul>
  <li>Conceito;</li>
  <li>Melhor perícia;</li>
  <li>Nome.</li>
</ul>

<p>Você pode começar com um conceito mais vago e afinar os detalhes do aspecto depois. <strong><em>Cara Com Uma Espada</em></strong> é um conceito razoável se for seguir este método, mas depois você pode descobrir algo sobre o seu personagem que redefina isso.</p>

<p>Quando isso acontecer, reescreva o aspecto para reﬂetir essa nova ideia. Você deve saber qual é a sua melhor perícia para começar – isso dará uma boa ideia do que seu personagem sabe fazer. Se você tiver mais ideias sobre quais perícias selecionar, como em quais perícias seu personagem é bom e em quais ele é fraco, pode anotá-las (lembrando que, normalmente, perícias abaixo de Regular (+1) não são anotadas, mas faça uma anotação na sua ficha sobre elas se for necessário).</p>

<p>Seu personagem também precisa de um nome! Talvez apenas o primeiro nome ou um apelido seja o suficiente no momento (há também o truque de criar um nome falso, apenas para revelar mais tarde que estava disfarçado ou tem amnésia e então anotar seu nome verdadeiro).</p>

<h3 id="começando-a-jogar">Começando a Jogar</h3>

<p>Com este método, você começa com 3 de recarga, logo possui 3 pontos de destino iniciais.</p>

<p>Após o final da primeira sessão, se ainda possuir o mesmo personagem, é bom separar um tempo para preencher o restante dos aspectos, perícias e façanhas.</p>

<h3 id="preenchendo-aspectos-durante-o-jogo">Preenchendo Aspectos Durante o Jogo</h3>

<p>A não ser que você tenha uma ideia imediata para a seu aspecto de dificuldade, pode deixá-lo para mais tarde. Com os outros três aspectos, já que pulou as três fases, é possível criar qualquer aspecto que pareça interessante no momento. Normalmente você faz isso quando seu personagem está em uma situação e precisa usar algo ou você quer causar uma reviravolta na situação (para melhor ou pior).</p>

<p>Assim como o aspecto principal, não se sinta frustrado se não acertá-lo como queria de imediato. Após o fim da sessão, separe um tempo para analisar e ajustar os aspectos que criou com pressa.</p>

<h3 id="preenchendo-as-perícias-durante-o-jogo">Preenchendo as Perícias Durante o Jogo</h3>

<p>A qualquer momento, se for usar uma perícia que não esteja na sua ficha de personagem, uma destas duas possibilidades pode acontecer: você assume que ela tem um nível Medíocre (+0) ou você pode escrevê-la em um dos campos de perícia vazios e rolar assumindo aquele nível. Essa opção só existe se houver algum espaço vazio nas perícias.</p>

<p>Se você decidiu rolar uma perícia como Medíocre (+0) ao invés de adotá-la em sua ficha, é possível anotá-la com uma graduação maior em outro momento. Por exemplo, talvez você precise fazer uma rolagem de Conhecimento considerando ela como Medíocre (+0). Depois, quando precisa rolar ela novamente, você a escreve como Razoável (+2).</p>

<p>Da mesma forma, você pode querer incluir uma perícia em sua ficha depois de conseguir uma boa rolagem com ela em nível Medíocre.</p>

<p>Como algumas perícias possuem benefícios secundários, como ajustar suas caixas de estresse e suas consequências, você pode receber tais benefícios assim que assumir o novo nível em determinada perícia. Até então, não há benefícios por assumir que elas são de nível Medíocre (+0).</p>

<h3 id="preenchendo-façanhas-durante-o-jogo">Preenchendo Façanhas Durante o Jogo</h3>

<p>Você ganha três façanhas grátis, as quais podem ser preenchidas durante o jogo. Você pode preencher suas façanhas a qualquer momento, contanto que pague um ponto de destino para cada uma delas. Isso acontece porque sua recarga determina com quantos pontos de destino você inicia, então por selecionar uma façanha, você deveria passar a ter menos pontos.</p>

<p>Se você não possuir pontos de destino para cobrir esse custo, mas teve uma ideia interessante que gostaria de usar no momento, faça-o. Porém, seu personagem ainda <em>não possui</em> aquela façanha até que poder pagar por ela.</p>

<p>Também será preciso ajustar sua recarga na próxima sessão para cada façanha adicional que quiser ter.</p>

<ul>
  <li><a href="../tudo-pronto/">« Tudo pronto!</a></li>
  <li><a href="../aspectos-e-pontos-de-destino/">Aspectos e Pontos de Destino »</a></li>
</ul>
:ET