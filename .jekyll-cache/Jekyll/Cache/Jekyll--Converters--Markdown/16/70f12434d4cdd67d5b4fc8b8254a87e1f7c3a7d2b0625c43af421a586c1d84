I"�<h1 id="genre-aspects">Genre Aspects</h1>

<p>There are times you might want to reinforce something about your game’s genre.
Creating issues that underscore the genre’s themes is a good way, and you can
get a lot of mileage out of encouraging your players to create characters tied
to the genre. Sometimes you want something mechanically unique, though.</p>

<p>When you use genre aspects, you’re making a change to the way aspects work in
order to reinforce your chosen genre. Sometimes you’ll apply these changes to
all aspects in the game, while other times you’ll want to restrict these
changes to a specific subset of aspects, or even a single specific aspect. The
ways in which you can change aspects are too varied to simply list here, so
instead we’ll provide a few examples.</p>

<h2 id="low-powered-aspects">Low-Powered Aspects</h2>

<p>This is a sweeping change to all aspects in the game, and it’s good for
emulating genres where the PCs are weaker than the forces arrayed against
them, such as in horror or noir. The change is simple: when you invoke an
aspect, you can reroll the dice you rolled or you can get a +2 to the roll,
but you cannot choose the +2 option more than once per roll if you invoke
multiple aspects. This means that an aspect only increases by a small number
the _maximum _effort a PC can exert. This is going to cause failure to be more
common, which means that failure needs to always be interesting, and it needs
to move the action forward just as much as success does.</p>

<p>If you want to allow the PCs to get multiple aspect bonuses in certain
situations, consider tying such things to a stunt, such as:</p>

<p><strong>Keen Senses:</strong> When you invoke aspects on a Notice roll, you can invoke any number of aspects, provided you have the fate points to cover the cost.</p>

<h2 id="quest-aspects">Quest Aspects</h2>

<p>This is a small change, but one that can reinforce the PCs’ shared goals. It
works well in fantasy settings, or any other setting where the PCs act as a
group toward some sort of large goal, such as slaying the dragon or rescuing
the village from bandits.</p>

<p>Whenever the PCs accept a quest, the group works together to create a good
aspect representing that quest. For example, if the PCs are trying to save the
village from a bandit king, the group might create the quest aspect
<span class="aspect">Martin Half-Heart Must Be Stopped!</span>. Any PC in the party
can invoke this aspect, and it can be compelled as if it were on each PC’s
character sheet, earning everyone a fate point.</p>

<p>If the PCs resolve a quest aspect, it’s a milestone. The scope of the
milestone depends on the difficulty and length of the quest. <a href="../../fate-core/advancement-change">Learn more about
milestones</a>.</p>

<ul>
  <li><a href="/fate-srd/fate-system-toolkit/aspects">« Aspects</a></li>
  <li><a href="/fate-srd/fate-system-toolkit/gear-aspects">Gear Aspects »</a></li>
</ul>

:ET