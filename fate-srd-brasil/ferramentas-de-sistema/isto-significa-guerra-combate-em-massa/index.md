---
title: "Isto Significa Guerra: Combate Em Massa"
layout: default
---


## Isto Significa Guerra: Combate Em Massa

---
 
#### Você Precisará De:

- Cartões para anotações que usará como ficha de unidades e para monitorar as zonas.
- Marcadores ou miniaturas para representar as unidades e líderes.
- Amigos, dados Fate e tudo o que precisaria para jogar uma partida de Fate SiS tema BáSico.

---
 
 
Esta modificação pode ser inserida em qualquer partida de Fate ou também serve como um minijogo enquanto se espera a pizza. Ela traz ferramentas simples para jogar e resolver conflitos em grande escala. Estas regras não são compatíveis com as regras de equipes apresentadas acima, já que abrangem um grupo maior do que um punhado pessoas.
 
### Treinamento Básico

Combatentes são **unidades** em um **campo** de batalha formado por **zonas**. São três conceitos abstratos, maleáveis o bastante para se encaixar em seus jogos e conflitos específicos.

As unidades são criadas como personagens, com perícias, aspectos e consequências, mas sem caixas de estresse ou façanhas. Uma unidade pode consistir em alguns navios de guerra, uma dezena de biplanos ou mil orcs urrantes, mas todos agem como um em batalha.

Represente cada zona do campo de batalha com um cartão de anotações, ou **cartão de zona**. Represente cada unidade com um marcador ou miniatura e coloque-o no cartão de zona para indicar sua posição atual no campo de batalha.

Uma unidade pode ter um **líder** ligado a ela, na forma de um PJ, PdN de suporte ou PdN principal. Líderes tornam suas unidades mais eficientes e podem se envolver pessoalmente com outros líderes no campo de batalha.
 
### Ações das Unidades
 
Quando ativada, uma unidade pode se mover uma zona gratuitamente, contanto que não haja nenhum obstáculo na zona de destino, como uma unidade inimiga ou um terreno obstruído. Ela também pode realizar uma ação — superar, atacar ou criar vantagem, como detalhado abaixo. 

Se a unidade possuir um líder, este poderá desistir de sua ação para conceder uma _segunda_ ação à unidade. Um jogador também pode gastar pontos de destino para conceder uma segunda ação a uma unidade sem líder.

Independentemente, nenhuma unidade pode realizar a mesma ação duas vezes no mesmo turno e atacar sempre finaliza o turno do jogador.

Se você possui uma unidade com um líder, ela pode se mover uma zona e atacar, criar vantagem e atacar e assim por diante, mas não pode atacar ou criar vantagem duas vezes seguidas, ou atacar e então criar vantagem ou se mover.

#### **Criar Vantagem**

Pode ser na forma de reconhecimento de terreno, intimidar outra unidade, usar o ambiente ou qualquer coisa que faça sentido no contexto. Aqui seguem algumas formas específicas de usar esta ação para tornar a sua batalha mais dinâmica.

**Emboscada:** Uma unidade pode usar Furtividade para colocar um aspecto de situação, como **_É Uma Armadilha!_** em jogo. Isso não pode ser tentado se a unidade possui um inimigo em sua zona.

**Intimidação:** Uma unidade pode usar Provocar para colocar um aspecto de situação em jogo, como **_Tropas Enfurecidas_** , **_O Poder da Frota Imperial_** ou **_Hesitante_**.

**Supressão:** Use Atirar para pôr o aspecto de situação **_Paralisado_** em uma unidade inimiga. Uma unidade com esse aspecto não pode se mover para outra zona a não ser que seja bem-sucedida em uma rolagem de superar contra a jogada de Atirar do atacante. O aspecto desaparece se o defensor for bem-sucedido ou se o atacante não usar uma ação para manter o aspecto turno após turno.

**Patrulha:** Uma unidade pode usar Notar para pôr um novo aspecto de zona em jogo em uma zona adjacente que ainda não possua uma unidade. A dificuldade é Razoável (+2), com +2 para cada aspecto que a zona já possua. Por exemplo, se a zona já possuir o aspecto **_Mata Fechada_** , a dificuldade para impor um segundo aspecto deve ser Ótima (+4).

**_Cerco:_** Uma unidade pode colocar o aspecto Cercado em jogo se possuir mais aliados do que inimigos em uma zona. Cada unidade aliada em uma zona recebe +1 nos próprios ataques enquanto esse aspecto estiver em jogo.


####  Superar

Ao usar uma perícia para se mover para uma zona com um obstáculo ou uma unidade inimiga:

Se a zona possuir um aspecto que dificulta o movimento, como _**Floresta Densa**_ ou _**Campo de Asteroides**_, a dificuldade é igual a duas vezes o número desses aspectos. Por exemplo, a dificuldade para entrar em um _**Terreno
Pedregoso**_ com um _**Rio Agitado**_ é igual a Ótimo (+4).

Se a zona possuir uma ou mais unidades inimigas, uma delas pode se opor ativamente a essa tentativa com uma ação de defesa, normalmente usando Atletismo, Condução ou Pilotagem. Cada unidade adicional em uma zona que seja aliada ao defensor fornece +1 à rolagem do defensor. De qualquer forma, use as resoluções padrões de superar para resolver a ação. 

**Ao usar uma perícia para se mover uma ou duas zonas adicionais livres de obstáculos:**

Em caso de empate ou sucesso, mova-se uma zona (com um custo menor, em caso de empate). Em caso de sucesso com estilo, a unidade pode abrir mão do impulso para se mover uma zona adicional.

 
#### Atacar

Se a batalha usar tanto Atirar como Lutar, ataques contra inimigos na mesma zona usam Lutar e ataques contra inimigos em zonas adjacentes usam Atirar. Se sua batalha usar apenas atirar — como em combates aéreos — então todos os ataques serão feitos com Atirar, independente da distância. Atacar um inimigo a duas zonas de distância concede +2 na rolagem de
defesa do defensor.

Provocar não pode ser usada para atacar, apenas para criar vantagem. Veja _Intimidação_ na página anterior.

Dependendo do local da batalha, você pode querer ajustar o funcionamento da defesa. Por exemplo, em uma guerra medieval talvez a única defesa contra Atirar seja Vontade — você não desvia de flechas, você mantém sua posição e e procura manter sua sanidade mental também. Isso pode retirar um pouco da funcionalidade de Atletismo, mas se aplica a todas as unidades por igual, logo ninguém fica em muita desvantagem.
 
### Qualidade da Unidade

 A qualidade de uma unidade — Regular, Razoável ou Boa — determina quantas perícias, aspectos e consequências ela possui.

+ **Regular:** Recrutas. Uma perícia Regular (+1). Um aspecto. Nenhuma consequência — um único golpe a retira do conflito.
+ **Razoável:** Soldados. Uma perícia Razoável (+2), duas Regulares (+1). Dois aspectos. Uma consequência moderada.
+ **Boa:** Soldados de elite. Uma perícia Boa (+3), duas Razoáveis (+2) e três Regulares (+1). Três aspectos. Uma consequência suave e uma moderada.

---

#### PERÍCIAS DE UNIDADES

Eis uma lista de perícias, retiradas do Fate Sistema BáSico, que as unidades podem
possuir.

- Atirar
- Atletismo
- Condução/Pilotar
- Furtividade
- Lutar
- Notar
- Provocar
- Vontade

Nem todas essas perícias serão apropriadas para todas as unidades em todo tipo de
conflito, é claro. Numa batalha entre naves espaciais, Lutar e Condução não serão muito
úteis, enquanto em uma batalha subterrânea entre anões e mortos-vivos a perícia Pilotar
provavelmente não será utilizada. Use o bom senso

---

### Aspectos de Unidade

 
O primeiro aspecto de uma unidade é o seu nome, que funciona como o seu conceito: _**Esquadrão Rebelde , Granadeiros Anões , 27ª Unidade de Infantaria Pesada ,**_ etc. 

Se a unidade é de categoria Razoável ou Boa, defina os aspectos secundários como preferir.
 
### Construindo Unidades

 
Cada jogador recebe um “kit de batalha” com pontos de construção para o combate — quanto maior o número, maior o número de unidades e maior a batalha. Gaste esses pontos para criar unidades ou para comprar pontos de destino adicionais que poderão ser gastos durante o combate. Cinco pontos de construção bastam para uma batalha pequena com unidades de baixa qualidade, enquanto 20 seria consideravelmente épico. 10-12 é um bom meio termo. Pontos de construção remanescentes podem ser gastos durante a batalha, mas quaisquer pontos que sobrarem após a batalha serão perdidos, assim como quaisquer pontos de destino comprados com pontos de construção.

Escreva os detalhes de cada unidade em seu próprio cartão de anotações. Se ela for derrotada, vire o cartão — mas deixe-o guardado, para que possa usar em uma batalha futura.

---

#### CUSTO DE UNIDADES

 
+ Regular: 1 ponto de construção
+ Razoável: 2 pontos de construção
+ Bom: 3 pontos de construção
+ Ponto de Destino: 3 pontos de construção
 
---
 
### Zonas

Uma zona pode ser uma única colina, uma campina de centenas de metros ou um setor espacial. As especificações dependem do seu jogo e da escala do conflito.

#### Número de Zonas

Como regra geral, o campo de batalha deve ter um número de zonas igual ao número de jogadores mais um. Isso inclui o Narrador, então um jogo com um Narrador e três jogadores deve possuir cinco zonas. Se parecer pouco espaço para o número de unidades envolvidas, adicione mais lguns cartões.

#### Acrescentando Aspectos de Zona

Por um ponto de destino, um jogador pode escrever um aspecto em um cartão de zona vazio que tenha sido colocado no campo de batalha, mas antes que a batalha comece. Ponha o cartão de zona de volta ao campo de batalha com a face do aspecto virada para baixo. Quando uma unidade se mover para aquela zona ou fizer uma patrulha por lá, vire o cartão e revele o aspecto.

Se um jogador colocar um novo aspecto em uma zona por criar vantagem durante o jogo, escreva no cartão para que todos vejam.

#### Criando o Campo de Batalha

Cada jogador tem seu turno para colocar cartões de zona, começando com aquele que tiver mais pontos de destino restantes em seu pacote. Cada cartão de zona deve estar adjacente a outro cartão já existente. Tente evitar um campo muito linear — muitas entradas e saídas em muitas das zonas tornará a batalha mais interessante e intensa.
 
### Líderes

 
Qualquer PJ ou PdN de suporte ou principal pode ser um líder. Use uma miniatura ou outro tipo de marcador para representar cada líder — algo que possa ser colocado junto ao cartão da unidade bem como direta- mente no campo de batalha, caso sua unidade seja derrotada ou quando ele agir independentemente. Derrotar uma unidade não derrota seu líder, apenas um líder pode atacar e derrotar diretamente outro líder.

Designar um líder a uma unidade, ou _removê-lo de uma_, não exige uma ação, mas um líder não pode fazer ambos no mesmo turno. 

Um líder designado a uma unidade pode realizar a sua ação junto com sua unidade. Ele pode conceder essa ação a sua unidade, para que ela realize duas ações, ou pode fazer outra coisa, como combater outro líder ou remover uma consequência da unidade.

Um líder designado a uma unidade lhe concede alguns benefícios.
 
- Todas as perícias da unidade que sejam inferiores à Vontade do líder recebem +1 de bônus enquanto o líder estiver posicionado. Se o seu jogo possuir outra perícia mais adequada para isso, use-a.
- O líder pode invocar seus próprios aspectos em prol da unidade.
- O líder pode usar Vontade para remover uma consequência, com as    dificuldades padrão mostradas no Fate Sistema Básico. Isso conta    como a ação do líder naquele turno.
- O líder pode usar sua ação para colocar um impulso em sua unidade,    como **_Avante!_**. Isso não exige uma rolagem a não ser que a unidade    possua uma consequência, o que nesse caso exige uma rolagem de    Vontade com uma dificuldade igual a duas vezes o número de consequências que a unidade possui. 

Um líder independente precisa ser ativado para poder realizar as coisas,
assim como uma unidade.

### Sequência de Jogo

1. Escolha um de seus líderes e role sua Vontade. Os resultados mais    altos agem primeiro, seguindo a ordem decrescente. Em caso de    empate, quem possuir Vontade mais alta age primeiro. Se o empate    permanecer, então o jogador com mais unidades começa.
2. Quando for o seu turno, escolha e ative uma de suas unidades ou     líderes independentes. Se escolher uma unidade que possua um    líder, este pode realizar sua ação normalmente. Se escolher um líder    independente, ele não poderá afetar unidades, mas poderá interagir    com outros líderes (em geral de forma violenta). Cada unidade e    cada líder de um dos lados deve agir antes de qualquer unidade ou    líder do mesmo lado agir novamente.
3. Quando todos os jogadores não aliados perdem suas unidades ou    concedem, a batalha termina.

### Vitória

Todos que façam parte do lado vitorioso recebem um ponto de destino. Cada jogador que derrotou um líder inimigo — em campo de batalha, persuadindo-o a conceder ou trocar de lado; o que for — recebe um ponto de destino para cada líder que tenham derrotado.

------

- [« Equipe](../equipes/)
- [Duelos Aventurescos »](../duelos-aventurescos/)
