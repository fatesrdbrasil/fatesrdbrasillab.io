---
title: A Ideia de Personagem
layout: default
---

## A Ideia de Personagem

**Crie os aspectos de Conceito e Dificuldade do personagem.**

A criação do personagem começa pelo seu *conceito*. É possível buscar inspiração em algum personagem de série ou filme ou pode se basear em algo específico que queira ser capaz de fazer (como quebrar tábuas com a cabeça, transformar-se em lobo, explodir coisas, etc.). Da mesma forma como foi feito com as questões da ambientação anteriormente, você pegará suas ideias e as transformará em dois aspectos centrais do seu personagem – **conceito** e **dificuldade**.

Os personagens devem ser excepcionais e interessantes. Eles podem ter sucesso fácil em situações simples, diferente do que eles realmente encontrarão em seu caminho. Será necessário pensar em um motivo para seu personagem continuar se envolvendo nessas situações perigosas. Sem isso, o Narrador não tem nenhuma obrigação de tentar fazer a história girar em torno de seu protagonista, pois ele vai estar bastante ocupado com os outros personagens que lhe deram um motivo para participar.

Como o conceito e a dificuldade estão interligados, eles estão sempre juntos. Você provavelmente terá uma ideia melhor se pensar em ambos como um grande passo ao invés de ideias separadas. Apenas depois de ter resolvido isso (e um nome, claro!) que será possível prosseguir com o restante do processo de criação do personagem.

Dito isso, não se preocupe – se sua ideia de personagem evoluir mais tarde, isso é bom! Você sempre poderá voltar e alterar algumas de suas decisões.

### Conceito

Seu **conceito** é uma frase que resume o seu personagem – quem ele é e o que faz. É um aspecto, porém um dos mais importantes que seu personagem possui.

------

### Continue Construindo Seu Cenário

Conforme criar coisas para os personagens, será necessário criar coisas para o mundo à volta deles também. Você vai acabar falando sobre PdNs, organizações, lugares e coisas assim. Isso é ótimo!

Você também pode criar conceitos de personagem que agreguem algo fundamental ao mundo do jogo, como quando alguém diz “quero ser um feiticeiro”, sendo que ninguém falou sobre magia ainda. Quando isso acontecer, discuta com o grupo se isso combina com o cenário e faça os ajustes necessários.

------

Pense nesse aspecto como uma profissão, sua função na vida ou sua vocação – no que você é bom, mas também é um dever com o qual você tem que lidar, sendo constantemente causador de dificuldades. Ou seja, ele é bom em algumas horas e ruim em outras. Há algumas formas diferentes de interpretar isso:

-  Pode ser literalmente uma ideia como “seu emprego”: ***Detetive Chefe***, ***Cavaleiro Imperial***, ***Capanga Iniciante***.
-  Você pode incluir um adjetivo ou descrição que defina melhor a ideia: ***Detetive Chefe Relutante***, ***Regente Desprezível de Ondrin***, ***Capanga Iniciante Ambicioso***.
-  Você também pode misturar mais de uma profissão ou função que a maior parte das pessoas poderia estranhar: ***Feiticeiro Investigador Particular***, ***Cavaleiro Cantor da Távola Redonda***, ***Contador Matador de Monstros***.
-  Você pode descrever uma relação importante com sua família ou uma organização com a qual esteja profundamente envolvido (especialmente se a família ou organização for inﬂuente ou bem conhecida): ***A Ovelha Negra da Família Thompson***, ***Capanga Iniciante do Sindicato***, ***O Bode Expiatório do Clã do Dedo em Ondrin***.

------

### Se Você Travou Nos Aspectos

Há uma regra de ouro para criar aspectos para um personagem: *você sempre pode alterá-los posteriormente*. Se houver dificuldades ao criar um aspecto, escreva a ideia usando quantas palavras forem necessárias, para passar a ideia para o papel. Se alguma outra ideia aparecer depois de ter anotado, ótimo! Se não, talvez alguém na mesa possa lhe ajudar a ter alguma ideia de aspecto. Se você permanecer travado, deixe-o de lado – você terá tempo de sobra durante o jogo para redefini-lo.

Se realmente for preciso, não há problema em deixar algum deles em branco. Dê uma olhada em [Criação Rápida de Personagem](../criacao-rapida-de-personagem/) para mais informações.

------

------

### Ajustes, Ajustes por Todos Os Lados

O Sistema *Fate* que se encontra neste livro não é tudo o que o Fate pode oferecer, mas apenas um ponto de partida – um conjunto de regras padrão que está pronto para usar.

À medida que o sistema se tornar mais familiar, o grupo pode querer mudar certas coisas para adaptá-lo melhor ao seu estilo de jogo. *Não há problema nenhum nisso*. Estas regras padrão não são mandamentos. Nós esperamos que você as altere. Na verdade, mostraremos ao longo do livro locais onde ajustes podem ser bem-vindos. O próximo livro, [Fate RPG - Ferramentas do Sistema](../../ferramentas-de-sistema/), é sobre como modelar o *Fate* às suas necessidades. Siga em frente e altere os detalhes. Não nos importamos.

------

Essas não são as únicas formas de trabalhar o seu conceito, mas já é um bom começo. Mas não se estresse com isso – o pior que pode acontecer é detalhá-los demais. Você ainda criará quatro outros aspectos além desses, mas não precisa fazer todos agora.

Alguns conceitos podem cobrir mais de um personagem, portanto é importante você ter algo que diferencie um personagem do outro. Se o conceito precisar ser semelhante para *todos os personagens*, como quando o Narrador prepara uma história apenas para espadachins, é crucial que as dificuldades sejam diferentes.

> Léo e Maira tinham definido seus personagens como “homem-lagosta” e “garota com partes robóticas”, enquanto Michel quer "um cara que não usa armas". Essas, porém, são apenas ideias iniciais. Agora é hora de transformar isso em conceitos.
>
> Léo se agarra a sua ideia de ligar o seu conceito à aparência. Algo que seja engraçado, que traga problemas e cenas divertidas. Ele escreve ***Mercenário Lagosta***. Seu personagem, além de ter uma cara incomum, irá passar por inúmeros vexames.
>
> Maira, por outro lado, não sabe muito bem para onde ir com “garota com partes robóticas”. Ela não está interessada nesse lance de organização, então tenta pensar em alguns adjetivos. Eventualmente surge a ideia de “***Famosa Ladra Robótica***”, o que a faz pensar em uma personagem que é conhecida por fazer coisas erradas.
>
> A ideia de Michel sobre “um cara que não usa armas” seria um aspecto muito simples. Ele pensa sobre o que foi criado até gora: há um culto espacial com interesses secretos, clã e impérios violentos. Como ele quer fazer um personagem que use mais inteligência do que armas, ele pergunta: “ei, posso ter poderes mentais? ” O grupo conversa um pouco sobre o que isso pode representar, e decidem que alguém com superpoderes é algo bem plausível nesse cenário e não ofuscará os outros personagens. Após isso, ele escreve “***Mentalista Andarilho***”.

### Dificuldade

Além do conceito, todo personagem possui algum tipo de aspecto problemático, algum tipo de **dificuldade** que faz parte de sua vida e história. Se o seu conceito é o que o seu personagem é, a dificuldade é a resposta para uma pergunta simples: o que complica sua vida?

As dificuldades trazem caos e situações interessantes para a vida personagem e divididas em dois tipos: problemas pessoais e relações complicadas.

-  **Problemas pessoais reﬂetem seu lado negro ou impulsos difíceis de serem contidos.** É algo que seu personagem pode se sentir tentado a fazer ou pode fazer inconscientemente no pior momento possível. Exemplos: ***Não Consigo Controlar as Emoções***, ***Adoro Um Rostinho Bonito***, ***As Garrafas Me Chamam.***
-  **Relações complicadas são pessoas ou organizações que complicam a sua vida.** Pode ser um grupo de pessoas que lhe odeia e queira levá-lo à ruína, o pessoal do seu trabalho que não facilita as coisas para você ou seus parentes e amigos que aparecem no meio do fogo cruzado. Exemplos: ***Homem de Família***, ***Dívida Com a Máfia***, ***A Tríade da Cicatriz Me Quer Morto***.

Sua dificuldade não deve ser fácil de superar. Se fosse, seu personagem já o teria feito e isso não seria interessante, mas ela também não deve paralisar o personagem completamente. Se a dificuldade interferir constantemente no dia a dia, muito tempo será perdido lidando com isso. Você não deveria ter de lidar com sua dificuldade a cada curva – a não ser que seja esse o tema de uma aventura (e mesmo assim, isso é apenas uma única aventura).

Dificuldades também não devem ser ligadas diretamente ao conceito – se você tem ***Detetive Dedicado***, dizer que sua dificuldade é ***O Submundo do Crime Me Odeia*** é uma dificuldade fraca, pois é possível assumir isso a partir do seu conceito (claro que você poderia trocar isso por algo mais pessoal como ***Dom Giovani Me Odeia***, para que funcione melhor).

Antes de seguir em frente, fale com o Narrador sobre a dificuldade de seu personagem. Tenha certeza de que ambos concordam com a escolha. Talvez vocês queiram encontrar uma forma dessa dificuldade poder ser invocada ou forçada – para garantir que ambos a vejam da mesma forma ou para gerar novas ideias. É importante que ao final dessa conversa o Narrador saiba o que você espera da sua complicação.

> Léo quer reforçar o aspecto animalesco de seu personagem e pensa em “***Não Entendo Absolutamente Nada***”. Ele imagina um personagem que vive se envolvendo em problemas e acumulando inimigos.
>
> Maira gosta da ideia de sua personagem ser sua própria inimiga, então vai investir em algum problema pessoal. Ela teve a ideia de interpretar alguém que não consegue deixar de pensar “***Se É Valioso, Poderia Ser Meu***”, então escreve isso.
>
> Depois de ver que os outros travam lutas pessoais, Michel quer agregar um pouco mais ao cenário por ter algum tipo de problema de relações. Ele quer algo que tenha ligação com seu conceito, algo que não possa lutar abertamente contra – ele quer ver intrigas na história. Assim, escreve "***O Império Quer Minha Cabeça***" (o que nomeia um grupo de pessoas no cenário com o qual o personagem de Michel tem relação).

------

### A Parte “Boa” de Uma Dificuldade

Já que sua dificuldade é um aspecto, ela também deveria poder se invocada, não é mesmo? É fácil de esquecer como complicações podem ajudar seu personagem depois de focarmos tanto nas dificuldades que trazem.

Resumidamente, sua experiência com sua dificuldade lhe fortalece nesse quesito. Lidar com problemas pessoais deixa você vulnerável a ser tentado e seduzido, mas também pode dar uma certa força interior pois você sabe o tipo de pessoa que quer ser. Relações complicadas podem trazer dificuldades, mas as pessoas aprendem muito a partir das dificuldades que precisam enfrentar. Aprendem especialmente como contornar muitas das questões menores de suas dificuldades atuais.

O ***Não Entendo Absolutamente Nada*** de Léo pode ser usado a favor do grupo. Talvez ele possa arranjar problemas com isso para distrair as pessoas enquanto Maira se esgueira nas sombras.

A tentação de Maira, ***Se É Valioso, Poderia Ser Meu***, pode ter dado a ela um bom conhecimento do valor de certas coisas (ela também pode estar bem familiarizada com prisões, o que pode ser bem útil no caso de uma fuga).

O ***Império Quer Minha Cabeça*** que Michel pode vir a calhar quando ele tiver que enfrentá-los, já que ele os conhece bem – ele sabe bem quais são suas táticas. Ele também pode usar esse aspecto para obter ajuda de outros rivais do império.

------

------

### Escolhendo Aspectos

Boa parte da criação dos personagens se foca em criar aspectos – uns chamados de conceitos, outros de dificuldades, mas todos funcionam basicamente do mesmo jeito. Os aspectos são uma das partes mais importantes de seu personagem pois definem quem ele é e criam meios de conseguir pontos de destino, usados para receber bônus e vantagens. Se houver tempo, leia todo o capítulo dedicado a aspectos antes de começar o processo de criação de personagens. Caso contrário, aqui vão algumas dicas.

Aspectos que não ajudarem a contar uma boa história (dando um sucesso quando você precisar e colocando-o em perigo e no meio da ação quando a história precisa) não funcionam. Aspectos que levam o personagem a conﬂitos - e o ajudam quando isso acontecer - são os melhores e mais úteis.

Aspectos precisam ser tanto úteis quanto perigosos – permitindo moldar a história e ganhar muitos pontos de destino – e nunca devem ser entediantes. Os melhores aspectos sugerem ambas as formas de usá-los e também formas de complicar situações. Aspectos que não podem ser usados dessa forma tendem a ser maçantes.

O ponto é: se você quer ampliar o poder de seu aspecto, maximize seu campo de atuação. 

Ao inventar um novo aspecto, é possível que as ideias não venham à mente. Se sentir que está sem ideias boas, há uma grande seção focada em diversos métodos para elaborar aspectos interessantes em [*Aspectos e Pontos de Destino*](../aspectos-e-pontos-de-destino).

Se seu personagem não tiver tantas conexões com os outros personagens, converse com o grupo sobre quais aspectos poderiam ligar seu personagem aos deles. Esse é o propósito das [Fases Dois e Três](../tres-fases/) - mas não significa que você não possa fazer isso em outro momento se preferir.

Se você não conseguir quebrar esse bloqueio de forma alguma, não force - deixe o espaço em branco. Sempre será possível voltar e preencher o aspecto mais tarde ou desenvolvê-lo durante o jogo - como nas [Regras de Criação Rápida de Personagens](../criacao-rapida-de-personagem).

De qualquer forma, é melhor deixar um aspecto em branco do que escolher um que seja pouco inspirador ou que não desperte a vontade de usá-lo. Se você escolher aspectos que não te atraem, eles acabarão como obstáculos no caminho da sua diversão.

------

### Nome

Se ainda não fez, agora é a hora de dar um nome a seu personagem!

> Léo procura um autor de fábulas com animais e encontra “Esopo”, o contador de histórias grego. Ele gosta da ideia e acha que o nome se aplica bem ao conceito de “um homem-lagosta”, e se diverte dizendo que assim ele “sempre terá a moral”.
>
> Maira chama sua personagem de “Fräkeline”. Ela quer usar o apelido “Fräk”, por causa de um seriado que ela assiste.
>
> O nome do personagem de Michel fica como “Bandu”, só porque lhe parece engraçado. Ele para um pouco e pensa, antes de adicionar... “O Oculto”, porque ele vê Bandu como um cara misterioso, que seria chamado de “Bandu, O Oculto” em público.

- [« Criação de Personagem](../criacao-de-personagens/)
- [As Três Fases »](../tres-fases/)
