# Documentações importantes para o Fate SRD Brasil

+ Copiar `docs/pre-commit` para `.git/hooks/pre-commit` para mover automaticamente _assets_ novos do repositório de artigos para o principal do _site_

+ git subtree pull --prefix _posts https://gitlab.com/fatesrdbrasil/artigos-fatesrdbrasil.git main < buscar novos artigos
+ git subtree push --prefix _posts https://gitlab.com/fatesrdbrasil/artigos-fatesrdbrasil.git main < caso seja necessário atualizar o repositório de artigos remotamente


