I"<h1 id="the-character-sheet">The Character Sheet</h1>

<p>Players, your character sheet contains everything you need to know about your
<abbr title="Player Character">PC</abbr>—abilities, personality, significant background elements, and any other
resources that character has to use in the game. Here’s an example of a Fate
character sheet to illustrate all the components.</p>

<p><strong>Looking for a character sheet?</strong> <a href="http://www.evilhat.com/home/fate-core-downloads/">Download them from here</a>.</p>

<h2 id="aspects">Aspects</h2>

<p><a href="../../fate-core/types-aspects">Aspects</a> are phrases that describe
some significant detail about a character. They are the reasons why your
character matters, why someone is interested in seeing your character in the
game. Aspects can cover a wide range of elements, such as personality or
descriptive traits, beliefs, relationships, issues and problems, or anything
else that helps us invest in the character as a person, rather than just a
collection of stats.</p>

<p>Aspects come into play in conjunction with fate points. When an aspect
benefits you, you can spend fate points to <a href="../../fate-core
/invoking-compelling-aspects" title="Invoking &amp; Compelling Aspects">invoke</a> that aspect for
a bonus. When your aspects complicate your character’s life, you gain fate
points back—this is called accepting a <a href="../../fate-core
/invoking-compelling-aspects">compel</a>.</p>

<blockquote>
  <p>Lily’s character, Cynere, has the aspect <span class="aspect">Tempted by Shiny
Things</span> on her sheet, which describes her general tendency to overvalue
material goods and make bad decisions when gems and coin are involved. This
adds an interesting, fun element to the character that gets her into a great
deal of trouble, bringing a lot of personality to the game.</p>
</blockquote>

<p>Aspects can describe things that are beneficial or detrimental—in fact, the
best aspects are both.</p>

<p>And aspects don’t just belong to characters; the environment your characters
are in can have aspects attached to it as well.</p>

<h2 id="skills">Skills</h2>

<p><a href="../../fate-core/skills" title="Skills">Skills</a> are what you use during
the game to do complicated or interesting actions with the dice. Each
character has a number of skills that represent his or her basic capabilities,
including things like perceptiveness, physical prowess, professional training,
education, and other measures of ability.</p>

<p>At the beginning of the game, the player characters have skills rated in steps
from Average (+1) to Great (+4). Higher is better, meaning that the character
is more capable or succeeds more often when using that skill.</p>

<p>If for some reason you need to make a roll using a skill your character
doesn’t have, you can always roll it at Mediocre (+0). There are a couple
exceptions to this, like magic skills that most people don’t have at all.
Learn about <a href="../../fate-core/skills-
stunts">skills in greater detail</a>.</p>

<blockquote>
  <p>Zird the Arcane has the Lore skill at Great (+4), which makes him ideally
suited to knowing a convenient, obscure fact and doing research. He does not
have the Stealth skill, however, so when the game calls upon him to sneak up
on someone (and Amanda will make sure it will), he’ll have to roll that at
Mediocre (+0). Bad news for him.</p>
</blockquote>

<h2 id="stunts">Stunts</h2>

<p><a href="../../fate-core/skills-stunts" title="Stunts">Stunts</a> are special tricks
that your character knows that allow you to get an extra benefit out of a
skill or alter some other game rule to work in your favor. Stunts are like
special moves in a video game, letting you do something unique or distinctive
compared to other characters. Two characters can have the same rating in a
skill, but their stunts might give them vastly different benefits.</p>

<blockquote>
  <p>Landon has a stunt called Another Round? It gives him a bonus to get
information from someone with his Rapport skill, provided that he is drinking
with his target in a tavern.</p>
</blockquote>

<h2 id="stress">Stress</h2>

<p><a href="../../fate-core/stress-consequences" title="Stress &amp; Consequences">Stress</a> is one of the two options you have to avoid losing a conflict—it represents
temporary fatigue, getting winded, superficial injuries, and so on. You have a
number of stress levels you can burn off to help keep you in a fight, and they
reset at the end of a conflict, once you’ve had a moment to rest and catch
your breath.</p>

<h2 id="consequences">Consequences</h2>

<p><a href="../../fate-core/stress-consequences">Consequences</a> are the other
option you have to stay in a conflict, but they have a more lasting impact.
Every time you take a consequence, it puts a new aspect on your sheet
describing your injuries. Unlike stress, you have to take time to recover from
a consequence, and it’s stuck on your character sheet in the meantime, which
leaves your character vulnerable to complications or others wishing to take
advantage of your new weakness.</p>

<h2 id="refresh">Refresh</h2>

<p><a href="../../fate-core/stunts-refresh">Refresh</a> is the number of fate
points you get at the start of every game session to spend for your character.
Your total resets to this number unless you had more fate points at the end of
the last session.</p>

<ul>
  <li><a href="/fate-srd/fate-core/players-gamemasters">« Players &amp; Gamemasters</a></li>
  <li><a href="/fate-srd/fate-core/taking-action-dice-ladder">Taking Action, Dice, &amp; the Ladder »</a></li>
</ul>

:ET