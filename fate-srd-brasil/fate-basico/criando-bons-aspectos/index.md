---
title: Criando Bons Aspectos
layout: default
---

## Criando Bons Aspectos

Já que os aspectos são tão importantes para o jogo, é importante criar os melhores aspectos possíveis. Como reconhecer, então, um aspecto bem criado?

Os melhores aspectos têm **sentido duplo, dizendo mais de uma coisa em uma frase simples.**

### Sentido Duplo

Do ponto de vista dos jogadores, bons aspectos são claramente benéficos aos personagens enquanto também criam oportunidades para complicar suas vidas ou são usados contra eles.

Um aspecto com sentido duplo aparecerá no jogo mais vezes do que apenas um positivo ou negativo. É possível usá-los frequentemente para fazer coisas incríveis, além ser possível aceitar mais forçamentos para ganhar mais pontos de destino.

Tente o seguinte: liste duas formas de invocar o aspecto e duas formas de outra pessoa invocá-lo ou forçá-lo contra você. Se os exemplos vierem facilmente à cabeça, ótimo! Se não, adicione algum contexto para fazer o aspecto funcionar ou deixe a ideia de lado e crie outro aspecto.

> Vamos dar uma olhada em um aspecto como ***Gênio da Informática***. Os benefícios de possuir esse aspecto são bem óbvios – sempre que fizer algo com tecnologia ou *hacking* você pode invocá-lo. Criá-lo não pareceu oferecer uma gama de oportunidades para forçá-lo contra você. Então, vamos pensar em uma forma de melhorá-lo um pouco.
>
> E se alterarmos esse aspecto para ***Nerd dos Nerds***? Assim ele ainda deixa óbvio a sua vantagem em trabalhar com tecnologia, mas também adiciona um ponto negativo – você parece estranho para as pessoas. Isso indica que você poderia aceitar forçamentos para estragar situações sociais ou que alguém poderia invocar o aspecto quando você for distraído por algum equipamento.

Narradores devem levar o mesmo em consideração para aspectos de jogo ou situação. Qualquer característica da cena que for enfatizada deve ser algo que os PJs ou seus inimigos possam usar de forma dramática. Seus aspectos de jogo trazem problemas, mas também deveriam apresentar formas de os PJs tirarem alguma vantagem disso.

### Diga Mais de Uma Coisa

Como vimos, aspectos podem descrever diversas coisas sobre um personagem: traços da personalidade, histórico, relacionamentos, problemas, posses e por aí vai. Os melhores aspectos pegam um pouco de cada uma dessas categorias, pois isso significa que haverá várias formas de usá-los durante o jogo.

> Vamos dar uma olhada em um aspecto que um soldado poderia ter: ***Tenho Que Provar Meu Valor.*** Você pode invocá-lo sempre que estiver tentando fazer algo para impressionar os outros ou demonstrar sua competência. Alguém talvez queira forçá-lo para colocá-lo em uma briga que deveria ser evitada ou aceitar uma situação sofrida para manter sua reputação. Então sabemos que esse aspecto possui dois sentidos, até agora tudo bem.
>
> Isso funcionará bem por um tempo, mas uma hora ou outra esse aspecto vai perdendo o valor. Ele diz apenas uma coisa sobre o personagem. A não ser que esteja provando seu valor, esse aspecto não serve para nada.
>
> Agora tente ligar esse aspecto a alguma organização: ***A Legião Exige Que Eu Prove Meu Valor***. Suas opções se tornaram mais amplas agora. Não apenas você tem tudo o que tinha anteriormente, mas também que a Legião pode exigir de você e colocá-lo em enrascadas por fazer coisas pelas quais você será culpado, ou então enviar PdNs de nível superior que dificultarão sua vida. Você também pode invocar o aspecto quando lida com a Legião ou com qualquer um que seja afetado pela Legião. Realmente há mais coisas que podem ser exploradas assim.

Narradores não precisam se preocupar muito com seus aspectos de situação, pois eles só precisam durar uma cena. É muito mais importante que aspectos de jogo e personagem sugiram vários contextos de uso.

### Ideia Clara

Pelo fato dos aspectos serem frases, eles podem possuir todas as ambiguidades da linguagem. Um aspecto não será muito utilizado se ninguém entender o que ele significa.

Isso não quer dizer que você tem que ser poético ou expressivo. ***Apenas Um Garoto Do Interior*** não é tão cativante quanto ***Pastor do Campo Bucólico***. Se for esse tipo de tom que você espera do seu jogo, sinta-se livre para incluir suas peculiaridades linguísticas.

No entanto, não deixe que isso atrapalhe a clareza da ideia. Evite metáforas e ideias indiretas, sendo que você pode ir direto ao ponto. Dessa forma outras pessoas não terão que ficar parando para perguntar, durante a partida, se determinado aspecto se aplica ou iniciando discussões sobre o seu significado.

> Vamos dar uma olhada em ***Memórias, Desejos e Arrependimentos.*** É uma frase que traz muitas emoções. O aspecto sugere algum tipo de melancolia sobre o passado. Como aspecto, porém, não fica claro *para que serve*. Como ele pode te ajudar? Sem uma ideia concreta sobre a que o aspecto se refere, invocá-lo ou forçá-lo é praticamente impossível.
>
> Suponha que estamos conversando sobre ele e você especifica que tem em mente que seu personagem é assombrado por suas lembranças da última grande guerra. Você matou pessoas que não queria matar, viu coisas que não queria ver e basicamente perdeu toda a esperança de um dia poder levar uma vida normal novamente.
>
> Eu achei isso fantástico e dou a sugestão que isso possa se chamar ***Cicatrizes da Guerra***. Menos poético, talvez, mas se refere diretamente a tudo o que acabou de falar e dá uma ideia melhor sobre que tipos de pessoas do seu passado que podem ser trazidas para dentro do jogo.


Se estiver na dúvida se o seu aspecto é muito vago, pergunte aos outros jogadores o que acham que ele significa.

## Se Empacar

Agora você sabe o que torna um aspecto bom, mas isso não diminui em nada o número de possibilidades à sua disposição — ainda há uma quantidade virtualmente infinita de temas e assuntos para escolher.

Se houver dificuldade em escolher, aqui vão algumas dicas para facilitar as coisas.

### Às Vezes é Melhor Não Escolher

Se você não conseguir pensar num aspecto que realmente te agrade, é melhor deixar aquele espaço em branco ou apenas rascunhar suas ideias nas margens. Às vezes é muito mais fácil esperar seu personagem entrar em cena antes de elaborar um aspecto.

Então, quando estiver em dúvida, deixe-o em branco. Talvez você tenha uma ideia geral do aspecto, mas não saiba como descrever ou talvez não tenha ideia alguma. Não se preocupe. Haverá chances de criar algo interessante durante o jogo.

O mesmo funciona para o caso de você ter várias ideias para um aspecto, mas elas não se batem e você não sabe qual escolher. Escreva todas elas nas margens e veja qual delas parece funcionar melhor durante o jogo. Então preencha o aspecto que mais lhe parece favorável.

### Sempre Pergunte o Que é Importante e Por quê

Dissemos acima que os aspectos são o porquê de algo ser importante no jogo e porque nós deveríamos nos importar. Esse é o seu guia inicial para a escolha de bons aspectos. Quando em dúvida, sempre pergunte: o que realmente importa aqui e por quê?

Os eventos descritos nas fases deveriam ajudar a imaginar o que o aspecto
deveria ser. Não tente resumir os eventos da fase ou algo assim – lembre-se, o importante é informar algo importante sobre o seu personagem. Novamente, pergunte-se o que é realmente importante naquela fase:

-  Qual foi o resultado? Isso é importante?
-  O personagem desenvolveu alguma relação ou conexão importante durante essa fase?
-  Esse aspecto o ajudou a estabelecer traços importantes de personalidade ou crença?
-  A fase deu alguma reputação ao personagem?
-  A fase criou um problema para o personagem no mundo do jogo?

Assuma que cada questão termina com "por bem ou mal" - afinal, essas características, relacionamentos e reputações não serão necessariamente positivas. Criar uma relação com um arqui-inimigo é tão útil quanto uma com seu melhor amigo.

Se houver mais de uma opção, fale com o Narrador e os outros jogadores para ver o que acham mais interessante. Lembre-se, vocês devem ajudar uns aos outros – o jogo funciona melhor se todos quiserem o melhor para o grupo.

> Durante a **fase três** de Fräkeline, Maira definiu que ela aparece na história de Esopo para avisá-lo de um inimigo em comum. Ela tenta fazer essa situação tomar a forma de um aspecto, mas não tem a mínima ideia de por onde começar. Dando uma olhada nas perguntas acima, vemos uma variedade de possibilidades – ela definitivamente quer algum tipo de relação com Esopo, e talvez os rivais de Esopo não gostem nem um pouco dela também.
>
> Maira pergunta ao restante do grupo e, após alguma conversa todos, parecem bem entusiasmados com a ideia de Fräk ter uma ligação com Esopo em um aspecto – ela decide que Fräk toma a proteção de Esopo como uma missão pessoal. Ela decide que ***Esopo Precisa Ser Protegido***, porque é específico o bastante para ser invocado ou forçado, mas deixa uma brecha para ser melhor desenvolvido durante o jogo.

### Variando

Você não quer que todos os aspectos descrevam as mesmas coisas. Cinco relações significam que você não poderá usar seu aspecto a não ser que ao menos um deles apareça em jogo, mas cinco traços de personalidade significam que você não possui conexão com o mundo do jogo. Se você sentir dificuldades em escolher os aspectos, talvez seja útil ver o que seus outros aspectos descrevem para ajudá-lo a pensar em algo para a fase atual.

> Léo finaliza seus aspectos com ***Mercenário Lagosta*** e ***Não Entendo Absolutamente Nada*** como seu conceito e dificuldade. Até agora, é um personagem bem direto – do tipo que sempre pode encontrar confusão.
>
> Léo segue para a fase um e explica que Esopo é um tipo de boêmio urbano que cresceu praticamente sozinho – seu planeta era povoado por lagostas gigantes e ele decidiu fugir antes de ser devorado. Ele eventualmente se envolveu em uma briga e perdeu sua nave pessoal.
>
> Amanda pergunta a ele o que é realmente importante para essa fase e Léo pondera um pouco. Os dois primeiros aspectos de Esopo têm uma conotação bastante pessoal – ele não possui muitas relações ainda. Então Léo se concentra nisso e decide ter uma ligação pessoal com alguém da criminalidade que pode lhe ajudar a recuperar sua nave. Ele nomeia o personagem como “O Explorador”, criando o aspecto “***O Explorador Sabe Meus Segredos***” e Amanda ganha um novo PdN para usar.

### Deixe Seus Amigos Decidirem

Falamos anteriormente sobre o fato das coisas ﬂuírem melhor quando todos se ajudam – colaboração é o coração do jogo e provavelmente diremos mais algumas vezes até o final do livro.

Sempre existe a opção, especialmente com os aspectos, de simplesmente pedir que o Narrador e os outros jogadores deem ideias. Sugira eventos de uma fase e faça as mesmas perguntas que eles o fizerem. O que é importante para eles? O que os empolga? Eles têm sugestões para tornar os eventos da fase mais dramáticos ou intensos? Que aspecto
acham que seria mais interessante ou apropriado?

Você tem a decisão final sobre seu personagem, então não veja isso como passar o controle para outra pessoa. É como perguntar aos fãs o que gostariam de ver na história e usar suas sugestões como faísca inicial para sua própria ideia. Se todos colaborarem um pouco com os personagens de outros, o jogo será bastante beneficiado desse senso de investimento mútuo.

- [« Para que servem os Aspectos](../para-que-servem-os-aspectos/)
- [Invocando »](../invocando/)
