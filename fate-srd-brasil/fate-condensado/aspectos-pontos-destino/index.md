---
title: Fate Condensado - Aspectos E Pontos De Destino
layout: default
---
# Aspectos E Pontos De Destino

Um ***aspecto*** é uma palavra ou frase que descreve algo de especial sobre uma pessoa, lugar, coisa, situação ou grupo. Quase tudo que você possa pensar pode ter aspectos. Uma pessoa pode ter uma reputação como *O Maior Atirador do Deserto* (veja abaixo para saber mais sobre esses tipos de aspectos). Uma sala pode estar *Em Chamas* após você derrubar um lampião. Após um encontro com um monstro, você pode estar *Aterrorizado*. Aspectos permitem que você mude a história de acordo com as tendências, perícias ou problemas do seu personagem.

## Aspectos são sempre verdades

Você pode invocar aspectos para um ganhar um bônus numa rolagem (mais detalhes [aqui](#invocações)) e forçá-los para criar uma complicação (mais detalhes [aqui](#forçando)). Mas mesmo quando estas mecânicas não estão sendo acionadas, aspectos ainda afetam a narrativa. Quando você tem aquela monstruosidade distorcida *Presa em uma prensa hidráulica*, isso é uma *verdade*. Ela não pode fazer muita coisa presa ali, e ela não sairá tão facilmente.

Em essência, “aspectos são sempre verdades” significa que aspectos **permitem ou impedem que certas coisas aconteçam na história** (eles também podem afetar a dificuldade: veja [aqui](../sendo-narrador/#definindo-dificuldade-e-oposição) para mais detalhes. Se a monstruosidade mencionada anteriormente está *presa*, o narrador (e todos no jogo) devem respeitar isso. A criatura perdeu a permissão de se mover até que algo aconteça e remova esse aspecto, seja com um sucesso em superar (que talvez exija um aspecto justificável como *força sobre-humana*) ou alguém tolamente revertendo a prensa. Do mesmo modo, se você tem *Pernas melhoradas ciberneticamente*, você argumentativamente ganhou permissão para saltar muros em um único pulo, sem a necessidade de ter que rolar para isso.

Isso não quer dizer que você pode criar qualquer aspecto que quiser e forçar a barra para utilizá-lo. Aspectos fornecem muito poder para mudar a história, sim, mas com poder vem a responsabilidade de jogar dentro dos limites da história. Aspectos devem se alinhar com o senso crítico de todos na mesa sobre o que é realmente possível. **Se um Aspecto não passar por esse crivo, ele precisa ser reformulado.**

Claro, você *pode* usar uma ação de Criar Vantagem para colocar o aspecto *Desmembrado* naquele super-soldado cheio de fungos, mas isso com certeza esbarra na ação Atacar, e além disso, dá muito mais trabalho arrancar o braço dele (embora esse Aspecto funcione melhor como uma Consequência - veja a página a seguir). Você *pode* dizer que é o *Melhor atirador do mundo*, mas você precisará comprovar isso com suas perícias. E por mais que você queira ser *A prova de balas*, removendo a permissão de alguém usar armas de fogo leves para te atacar, isso provavelmente não irá colar, a menos que o jogo que você está jogando envolva usar aspectos como super poderes.

## Que tipo de aspectos existem?

Há uma incontável variedade de aspectos (mais detalhes [aqui](#outros-tipos-de-aspectos) para outros tipos), mas não importa como sejam chamados, todos funcionam basicamente da mesma maneira. A principal diferença é por quanto tempo eles ficam em jogo antes de sumirem.

### Aspectos de personagem

Esses Aspectos estão na sua ficha de personagem, como seu Conceito e Dificuldade. Eles descrevem traços de personalidade, detalhes importantes sobre seu passado, relacionamentos que você tem com outros personagens, itens ou títulos importantes que você possui, problemas com os quais você tem que lidar, objetivos que você está tentando alcançar, ou reputações ou obrigações das quais você tem que lidar. Esses Aspectos mudam principalmente durante os Marcos (mais detalhes [aqui](../avanco/#marcos)).

**Exemplos:** *Líder de um Grupo de Sobreviventes; Atenção aos Detalhes; Devo Proteger meu Irmão.*

### Aspectos de Situação

Esses Aspectos descrevem o ambiente ou o cenário onde a ação está acontecendo. Um Aspecto de Situação em geral desaparece no final da cena da qual faz parte, ou quando alguém realiza alguma ação que possa mudá-lo ou eliminá-lo. Essencialmente, eles duram apenas enquanto a situação que eles representam exista.

**Exemplos:** *Em Chamas; Luz Solar Brilhante; Multidão Furiosa; Caído no Chão; Perseguido pela Polícia.*

### Consequências

Esses Aspectos representam ferimentos e outros traumas duradouros sofridos ao absorver um dano, geralmente por meio de ataques (mais detalhes [aqui](../desafios-disputas-conflitos/#consequências)).

**Exemplos:** *Tornozelo Torcido; Concussão; Insegurança Debilitante.*

### Impulsos

O ***Impulso*** é um tipo especial de aspecto, que representa uma situação extremamente temporária ou pontual. Você não pode forçar um Impulso ou gastar Pontos de Destino para invocá-lo. Você pode invocá-lo apenas uma vez gratuitamente, após isso ele desaparece. Um Impulso não utilizado desaparece tão logo a vantagem que ele representa desapareça, o que normalmente dura alguns segundos ou a duração de uma única ação. Eles nunca duram além do final da cena, e você pode adiar a nomeação de um até que você o use. Se você estiver no controle de um Impulso, você pode passá-lo para um aliado se isso fizer algum sentido.

**Exemplo:** *Na mira, Distraído, Desequilibrado*

## O que posso fazer com os Aspectos?

### Ganhando Pontos de Destino

Uma forma de ganhar Pontos de Destino é permitindo que os seus Aspectos de Personagem sejam ***Forçados*** (mais detalhes [aqui](#forçando)) contra você para complicar a situação ou tornar a sua vida mais difícil. Você também pode obter um ponto de destino se alguém usar seu aspecto contra você em uma invocação hostil (mais detalhes [aqui](#invocações-hostis)) ou quando você conceder (mais detalhes [aqui](../desafios-disputas-conflitos/#concedendo)).

Lembre-se: a cada sessão, você também começa com Pontos de Destino no mínimo iguais a sua ***Recarga***. Se você foi Forçado em seus Aspectos mais do que os Invocou na sessão anterior, você começará a próxima sessão com mais pontos. Consulte a sessão sobre recarga, [aqui](../comecando/#recarga) para detalhes.

### Invocações

Para desbloquear o verdadeiro poder dos aspectos e fazer que eles te ajudem, você precisará gastar ***pontos de destino*** para ***invocá-los*** durante as rolagens de dados (mais detalhes [aqui](#aspectos-são-sempre-verdades)). Controle seus pontos de destino com moedas, marcadores, fichas de poker ou qualquer outra coisa. 

Você também pode invocar aspectos gratuitamente *caso* você tenha invocações gratuitas que você, ou um aliado, tenha criado com a ação de criar vantagem (mais detalhes [aqui](../agindo-rolando-dados/#criar-vantagem)).

#### O truque das reticências

Se quiser uma maneira fácil de garantir que você tenha espaço para incorporar aspectos em uma rolagem, tente narrar suas ações de modo a incluir reticências no final delas (“...”) e então termine a ação invocando os aspectos desejados, como abaixo:

> Ricardo diz, “Então eu tento decifrar as runas e...” (rola os dados, odeia o resultado) “e ...*Se Eu Não Estive Lá, Eu Li a Respeito*...” (gasta um ponto de destino) “...então eu facilmente começo a divagar sobre sua origem".

#### Invocações Hostis

Na maioria das vezes, um aspecto invocado será de personagem ou de situação. Às vezes você invocará aspectos de adversários (ou aspectos de situação ligados aos mesmos) contra eles. Isso é chamado de ***invocação hostil e funciona*** da mesma forma que invocar qualquer aspecto: pague um ponto de destino e receba +2 na sua rolagem ou role novamente os dados. Existe uma pequena diferença: **quando você faz uma invocação hostil, você passa o Ponto de Destino para a oposição**. Porém eles não podem usar o ponto de destino até que a cena esteja encerrada. Esse pagamento *só ocorre* quando há o gasto de um Ponto de Destino para invocações hostis. Invocações gratuitas não geram um pagamento.

#### Invocações para Declarar Detalhes Narrativos

Você pode adicionar um detalhe importante ou improvável à história baseado em um Aspecto em Jogo. Não gaste pontos de destino quando “aspectos são sempre verdades” (mais detalhes [aqui](#aspectos-são-sempre-verdades)) se aplica. Pague apenas quando você quiser *exagerá-los*, ou quando a mesa quiser e não houver um Aspecto mais relevante.

### Forçando

Aspectos podem ser ***forçados*** para complicar a situação e com isso ganhar Pontos de Destino. Para Forçar um Aspecto, o Narrador ou um jogador oferece um Ponto de Destino ao jogador cujo personagem tem um aspecto sendo forçado e diz a ele porque aquele Aspecto está tornando as coisas mais difíceis ou complicadas. Se ele recusar a ter seu aspecto forçado, é ele quem tem que pagar um Ponto de Destino e explicar como o personagem evita a complicação. Sim, isso significa que se você não tem nenhum Ponto de Destino, você não pode evitar que seu aspecto seja forçado\!

**Qualquer Aspecto pode ser Forçado**, seja um Aspecto de Personagem, aspecto de Situação, ou Consequência, mas apenas se isso afetar o personagem que tem o aspecto forçado.

**Qualquer um pode propor forçar um aspecto**. O jogador que propõe forçar aspecto precisa gastar um dos seus Pontos de Destino. O Narrador então assume controle sobre o forçar aspecto contra o alvo afetado. O Narrador não perde Pontos de Destino ao propor forçar aspecto, ele tem uma reserva limitada de Pontos de Destino para Invocar Aspectos, mas ele pode Forçar quantos Aspectos quiser.

**Forçar pode ser retroativo**. Se um jogador perceber que se colocou em uma complicação relacionada a um dos seus Aspectos, ou em Aspectos de Situação que o afete devido à sua interpretação, ele pode perguntar ao Narrador se isso conta como **forçar aspecto contra si mesmo**. Se o grupo concordar, o Narrador passa ao jogador um Ponto de Destino. 

**Está tudo bem em reconhecer que uma proposta de forçar aspecto não faz muito sentido e decidir desistir de fazê-lo**. Se o grupo concordar que uma proposta de forçar aspecto não é apropriada, ela deve ser abandonada sem que o personagem que a propôs tenha que pagar um Ponto de Destino.

#### Forçar aspectos gera complicações, não impedimentos.

Ao propor forçar um aspecto, certifique-se de que a complicação é um curso de ação ou uma mudança importante nas circunstâncias, não uma negação das opções.

*“Você tem areia nos olhos, então você atira na criatura e erra”*, não é forçar aspecto, pois ele apenas nega ações ao invés de complicar as coisas.

*“Sabe, maldita sorte, acho que a areia no seus olhos significa que você realmente não consegue ver nada. Você atira a esmo contra o Shoggoth, perfurando alguns barris que agora estão jorrando gasolina em direção à fogueira”*. Esse é um forçar aspecto muito melhor. Ele muda a cena, eleva a tensão e dá algo novo para os jogadores pensarem.

Para mais ideias sobre o que pode ou não funcionar como um forçar aspecto, veja a discussão sobre as formas de forçar no ***Fate Básico*** começando na página 65 desse livro, ou online [aqui](/fate-srd-brasil/fate-basico/invocando/#formas-de-forçar).

#### Eventos e Decisões

Em geral, existem duas formas de forçar: ***Eventos*** e ***Decisões***.

Forçar um aspecto baseado em um evento é algo que acontece a um personagem por causa de uma força externa. Essa força externa se relaciona ao Aspecto de alguma forma, resultando em uma complicação infortúnia.

Forçar um aspecto em uma decisão é algo interno, onde as fraquezas ou valores contraditórios do personagem interferem em seu julgamento. O Aspecto guia o personagem a tomar uma decisão em particular e a consequência dessa decisão gera uma complicação para ele.

De qualquer modo, a chave é que o resultado seja uma complicação\! Sem complicação, não há como forçar um aspecto.

#### Invocações Hostis ou Forçar Aspectos?

Não confunda invocações hostis e forçar aspectos\! Embora sejam semelhantes, ambas são maneiras de colocar o personagem em um problema imediato em troca de um ponto destino, elas funcionam de formas diferentes.

Forçar cria uma *mudança narrativa*. A decisão de Forçar um Aspecto não é algo que acontece dentro do universo de jogo, em vez disso, é o Narrador ou Jogador propondo uma mudança na história. Esse efeito pode ser amplo, mas o alvo obtém o Ponto de Destino imediatamente se aceitar ter seu aspecto forçado e pode escolher recusá-lo.

Uma Invocação Hostil é um *efeito mecânico*. O Alvo não tem a oportunidade de recusar a invocação, mas como em qualquer invocação, você precisará explicar como a invocação desse aspecto faz sentido. E embora o jogador obtenha um Ponto de Destino, ele não pode ser usado na cena atual. No entanto, o resultado final é mais restrito: um bônus de +2 na rolagem ou rolar novamente os dados uma vez.

Forçar permite que você, como jogador ou narrador, mude *o que se trata em uma cena*. Elas mudam o rumo da narrativa. Usá-la contra um oponente é uma proposta arriscada: ele pode recusar ou pode atingir seu objetivo apesar da complicação, graças ao novíssimo Ponto de Destino que você acabou de lhe dar.

Invocações Hostis o ajudam a lidar com o momento atual. Além dos seus próprios Aspectos, você tem os aspectos do seu oponente para invocar, dando a você mais opções e tornando as cenas mais dinâmicas e conectadas.

### Como posso adicionar ou remover Aspectos?

Você pode criar ou descobrir um Aspecto de Situação usando a ação de Criar Vantagem (mais detalhes [aqui](../agindo-rolando-dados/#criar-vantagem)). Você também pode criar Impulsos dessa forma, ou como resultado de um empate ou sucesso com estilo em ações de Superar um obstáculo, Atacar ou Defender.

Você pode remover um Aspecto desde que você possa pensar em uma maneira do seu personagem fazer isso, abrir o extintor de incêndio contra *Chamas ardentes*, usar manobras evasivas para escapar do guarda que está *Na sua cola* te perseguindo. Dependendo da situação, isso pode exigir uma Ação de Superar (mais detalhes [aqui](../agindo-rolando-dados/#superar)); nesse caso, os oponentes poderiam usar uma ação de defender para tentar manter o Aspecto, se eles puderem descrever como o farão.

Entretanto, se não houver nenhuma limitação narrativa para impedir a remoção do Aspecto, você pode simplesmente fazê-lo. Se você estiver *Amarrado dos pés à cabeça* e um amigo cortar essas amarras, o aspecto desaparece. Se nada estiver te impedindo, não há necessidade de realizar rolagens.

### Outros tipos de Aspectos

Cobrimos os tipos de Aspectos padrões como visto [aqui](#que-tipo-de-aspectos-existem). Os tipos a seguir são opcionais, mas podem agregar valor ao seu jogo. Até certo ponto, esses Aspectos são variantes dos Aspectos de Personagem (se você expandir sua noção do que conta como um personagem) e Aspectos de Situação (se você mudar sua noção de quanto tempo eles duram).

**Aspectos de Organização:** às vezes você terá que lidar com uma organização inteira que opera sob uma série de princípios. Considere dar à organização Aspectos que qualquer membro possa acessar como se fossem seus próprios.

**Aspectos de Cenas:** Às vezes, uma trama específica pode introduzir temas que aparecem de tempos em tempos na história. Considere definir isso como um Aspecto que estará disponível para todos os personagens da história até que essa parte da história seja concluída.

**Aspectos de Cenário:** Como um Aspecto de Cena, o Cenário de sua campanha pode ter temas recorrentes. Ao contrário de um Aspectos de Cena, esses Aspectos não desaparecem.

**Aspectos de Zona:** você pode incluir Aspectos de Situação a um determinado lugar do mapa representado por uma zona (mais detalhes [aqui](../desafios-disputas-conflitos/#zonas)). Isso pode adicionar um dinamismo extra às interações do grupo com o mapa.  O Narrador pode encorajar isso disponibilizando uma Invocação Gratuita “disponível para pegar” em um Aspecto de Zona no início de uma cena, atraindo os personagens (jogadores ou não) a tirar proveito desse Aspecto como parte de sua estratégia inicial.

------


- [« Agindo e Rolando os Dados](../agindo-rolando-dados/)
- [Desafios, Disputas e Conflitos »](../desafios-disputas-conflitos/)
