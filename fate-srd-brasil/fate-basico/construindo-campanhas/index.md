---
title: Construindo Campanhas
layout: default
---

## Construindo Campanhas

Mais uma vez, a maneira mais fácil de fazer isso é não se preocupar – apenas deixe que seus cenários e arcos criem uma história para a campanha. Seres humanos são máquinas de criar padrões e é bastante provável que você consiga captar naturalmente qual deve ser o enredo de sua campanha analisando as perguntas ainda não respondidas dos arcos e cenários.

No entanto, se você gosta de planejar um pouco mais, o conselho é o mesmo que para os arcos, exceto que você estará ampliando um pouco mais as coisas. Escolha *uma* pergunta narrativa para responder, uma que será o ponto culminante depois dos PJs passarem por diversos cenários e arcos. Faça então algumas anotações sobre os passos necessários para que seja possível responder a essa pergunta; eles servirão como material para a elaboração de arcos e cenários.

Os melhores aspectos para elaborar um problema no nível campanha são as questões presentes ou iminentes de seu cenário, dado seu alcance.

> Amanda sabe que sua campanha vai depender dos personagens resolverem o aspecto ***A Bruxaria Retornará!***. Então umas de suas perguntas é “Será que os PJs poderão evitar o retorno da bruxaria espacial?"
> 
> Ela sabe que para isso eles primeiro precisarão descobrir os planos dos Bruxos de Moondor. Também precisarão se certificar que nenhum de seus inimigos pessoais ou o Clã do Dedo interfiram na missão de deter a bruxaria. Isso lhe dá uma boa ideia de quais arcos construirão a campanha.

- [« Construindo um Arco](../construindo-um-arco/)
- [Evolução e Alteração »](../evolucao-e-alteracao/)

