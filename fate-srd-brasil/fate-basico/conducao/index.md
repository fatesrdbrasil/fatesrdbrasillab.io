---
title: Condução
layout: default
---

### Condução

A perícia Condução trata da operação de veículos e coisas velozes.

O funcionamento da perícia Condução em seu jogo dependerá muito de quantas ações você pretende realizar dentro de um veículo ou outro meio de transporte e do tipo de tecnologia disponível em seu cenário.

Por exemplo, em um cenário futurista de alta tecnologia, com naves espaciais e tudo o mais (como em *Caroneiros de Asteroide*), talvez você possa mudar Condução para Pilotagem (devido às espaçonaves). Em um cenário de guerra, Operação (para tanques e veículos pesados) fará mais sentido. Cenários de baixa tecnologia, onde o transporte pode ser feito com animais, podem combinar mais com Cavalgar.

------

### Veículos Diferentes, Perícias Diferentes

Não crie muitas categorias a não ser que faça uma diferença notável no seu jogo. Recomendamos considerar a opção de possuir perícias modificadas por façanhas (veja [*Criando Façanhas*](../criando-facanhas/)).

------

- `O`{: .fate_font} **Superar:** Condução é equivalente a Atletismo quando é utilizada em um veículo –para se movimentar para quando estiver em circunstâncias difíceis, como em terreno acidentado, repleto de buracos ou realizar manobras com o veículo. Obviamente, Condução é muito usada em disputas, especialmente perseguições e corridas.
- `C`{: .fate_font} **Criar Vantagem:** Uma Você pode usar Condução para determinar a melhor maneira de chegar em algum lugar com um veículo. Uma boa rolagem pode permitir que você aprenda detalhes da rota que são expressos em aspectos ou então declarar que sabe um ***Atalho Conveniente*** ou algo similar.
- Você também pode ler a descrição da perícia Atletismo e transferi-la para um veículo. Usar a ação Criar Vantagem em Condução, muitas vezes, se trata de obter um bom posicionamento, fazer uma manobra ousada (como um ***Giro Aéreo***) ou colocar o seu oponente em uma posição ruim.
- `A`{: .fate_font} **Atacar:** Condução normalmente não é usado para realizar ataques (apesar de façanhas poderem alterar isso). Se quiser colidir o carro contra algo, você pode usar Condução como ataque, mas levará as mesmas tensões que causar no conﬂito.
- `D`{: .fate_font} **Defender:** Evitar dano a um veículo em um conﬂito físico é uma das formas mais comuns de usar Condução. Você também pode usá-la para defender-se de vantagens criadas contra você ou deter ações de outros veículos tentando ultrapassá-lo.

#### Façanhas Para Condução

-  **Duro na Queda:** +2 em Condução sempre que estiver perseguindo outro veículo.
-  **Pé na Tábua:** Você pode fazer seu veículo atingir uma velocidade maior do que é possível. Sempre que estiver em uma disputa onde a velocidade é o principal fator (como em uma perseguição ou corrida) e empatar com seu adversário em uma rolagem de Condução, é considerado sucesso.
-  **Preparar Para o Impacto!:** Você ignora duas tensões de dano ao colidir com outro veículo. Portanto, se a colisão e causar quatro tensões de dano, você recebe apenas duas.

- [« Comunicação](../comunicacao/)
- [Conhecimentos »](../conhecimentos/)
