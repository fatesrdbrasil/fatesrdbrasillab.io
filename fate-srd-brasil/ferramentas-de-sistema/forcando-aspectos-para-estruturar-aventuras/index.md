---
title: Forçando Aspectos Para Estruturar Aventuras
layout: default
---

## Forçando Aspectos Para Estruturar Aventuras


Se você não consegue pensar em uma ideia legal para uma aventura, basta ir mais a fundo nos aspectos dos PJs! Aqui daremos algumas dicas para iniciar uma aventura rapidamente e também fornecer alguns pontos de destino extras logo de início.

Primeiro, dê uma olhada nos aspectos dos PJs e veja se tem alguma ideia de imediato. Se perceber que alguns deles oferecem um gancho interessante para uma aventura, em especial algo que esteja ligado ao mundo de jogo ou a alguma organização, diga ao jogador que você deseja usá-lo como base para uma aventura e ofereça-lhe um ponto de destino. A maioria dos jogadores aceitarão forçar sem muita negociação, mas se o jogador não estiver interessado, não o force a pagar um ponto de destino por recusar – simplesmente veja a possibilidade de forçar outro aspecto.

Assim que conseguir a base da sua aventura, apresente-a à mesa. Dê sugestões de outros aspectos e solicite sugestões dos jogadores. Converse com eles sobre o que aparecerá na aventura e dê pontos de destino por boas sugestões. Essas sugestões não precisam estar ligadas a aspectos, mas é melhor se estiverem. Afinal, se múltiplos aspectos estiverem ligados à história, você poderá continuar a forçar esses aspectos mais tarde!

Se você aceitar sugestões que não sejam baseadas em aspectos existentes, transforme essas sugestões em aspectos. Elas podem ser aspectos de situação ou aspectos espalhados por toda a aventura.

Depois de conseguir um bom ponto de partida, comece a jogar! Agora você tem uma situação inicial do interesse dos jogadores , que por sua vez possuem alguns pontos de destino extras que podem investir para realizar coisas incríveis logo no início!


------

- [« Opções Para Criação de Personagens](../opcoes-criacao-personagens/)
- [Eventos de Aspectos »](../eventos-de-aspectos/)
