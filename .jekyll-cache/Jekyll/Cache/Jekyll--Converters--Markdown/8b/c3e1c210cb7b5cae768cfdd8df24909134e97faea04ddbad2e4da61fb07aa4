I" <h2 id="outras-soluções">Outras Soluções</h2>

<p>Às vezes a lista de perícias tradicional simplesmente não é o que você precisa. Talvez você queira uma lista completamente nova ou talvez precise de uma abordagem diferente. A boa notícia é que mesmo se fizer algo drasticamente diferente, as orientações das perícias existentes podem continuar a ser de grande ajuda, já que a ação por baixo delas ainda podem funcionar da mesma forma que a perícia original. Independente se a perícia se chama Lutar, Kung Fu ou Durão, a mecânica básica de socar alguém permanecerá praticamente a mesma.</p>

<h3 id="nova-lista-de-perícias">Nova Lista de Perícias</h3>

<p>Se for necessário criar uma nova lista de perícias no mesmo estilo da existente — apenas com perícias novas — então o caminho vai depender muito do motivo da mudança, algo que ninguém saberá melhor que você. As orientações sobre como adicionar e remover perícias são a sua melhor referência para isso.</p>

<h3 id="profissões">Profissões</h3>

<p>Enquanto perícias normais representam o que você faz, as profissões refletem quem o personagem é, deixando suas habilidades subentendidas. Se por exemplo as profissões forem Lutador, Erudito, Lenhador, Ladrão, Artesão e Diplomata, então poderíamos rolar Lutador em situações onde normalmente usaria Lutar ou Atirar, Ladrão onde normalmente rolaria Furtividade ou Roubo, e assim por diante. Listas de profissões em geral são mais curtas e abertas que a lista de perícias, o que pode tornar a pirâmide bem pequena. É possível usar uma lista de profissões fixas ou usá-las de forma geral, como trataremos abaixo.</p>

<h3 id="abordagens">Abordagens</h3>

<p>Se a lista padrão de perícias representa o que você sabe fazer e as profissões representam o que você é, as abordagens refletem como faz as coisas. Isto é, ao invés de possuir mais perícias, um personagem pode ter níveis de Poderoso, Estiloso, Esperto ou Determinado que pode rolar dependendo da situação. Em uma luta de espadas você agride ferozmente a defesa de seu oponente (Poderoso), balança em um lustre (Estiloso), finta e manobra em busca de uma abertura (Esperto) ou é paciente até que seu adversário comenta um erro (Determinado)?</p>

<p>Fãs do Fate Acelerado reconhecerão esse método como o usado pelo sistema, através das abordagens Cuidadoso, Inteligente, Estiloso, Poderoso, Ágil e Sorrateiro.</p>

<h3 id="perícias-livres">Perícias Livres</h3>

<p>Então, e se não houvesse lista de perícia e os jogadores pudessem simplesmente dizer as perícias que possuem? Pode parecer meio caótico a princípio, mas você notaria padrões familiares, com nomes novos e interessantes. Tal abordagem é funcional, com apenas uma ressalva – todos precisam saber o que a perícia representa. Caso contrário é possível que um jogador acabe com uma super-perícia que é útil em diversas situações, deixando o restante do grupo de lado.</p>

<p>A forma mais fácil de evitar isso é ter certeza que todos entendem a diferença entre uma perícia, uma profissão e uma abordagem. A maior parte dos problemas surgirá se você tiver um jogador escolhendo perícias enquanto outro escolhe profissões e abordagens, já que profissões e abordagens são mais abrangentes quando aplicadas em jogo, diferente das perícias. Abordagens e profissões podem ser um pouco mais amplas, mas ainda devem ser tratadas com cautela.</p>

<p>Além de estimular a criatividade dos jogadores, esta abordagem permite um truque especial — usar aspectos como perícias.</p>

<h3 id="perícias-e-aspectos">Perícias e Aspectos</h3>

<p>Se estiver usando perícias livres, uma de suas perícias poderia compartilhar o nome com um aspecto. Se você é um <strong><em>Cavaleiro do Cálice</em></strong> e também possui <strong><em>Cavaleiro do Cálice Bom (+3)</em></strong>, isso cria um certo ar de completude. Isso não é nem um pouco obrigatório, mas às vezes parece ser a melhor escolha. Também é possível usar aspectos para deixar de lado as perícias, mas isso é um pouco mais complicado e falaremos sobre mais adiante.</p>

<hr />

<ul>
  <li><a href="../usando-como-foram-escritas/">« Usando como foram escritas</a></li>
  <li><a href="../mudancas-estruturais/">Mudanças Estruturais »</a></li>
</ul>
:ET