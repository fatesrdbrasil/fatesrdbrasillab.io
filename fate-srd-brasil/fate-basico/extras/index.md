---
title: 11 -  Extras
layout: default
---

# 11 -  Extras


## O Que São Extras?

Um **Extra** em *Fate* é um termo bastante amplo. Vamos utilizá-lo para descrever tudo que tecnicamente faz parte do personagem ou é controlado por ele, mas que precisa de regras especiais. Se a sua aventura em *Fate* fosse um filme, os extras seriam os efeitos especiais.

Alguns exemplos incluem:

- Poderes mágicos e sobrenaturais;
- Ferramentas e equipamentos especiais, como armas e armaduras encantadas em um jogo de fantasia ou super tecnologia em um jogo de ficção;
- Veículos que os personagens possuem;
- Organizações ou locais sobre as quais os personagens possuam uma grande inﬂuência.

As ferramentas apresentadas aqui permitirão a você criar extras que se enquadrem perfeitamente em seu jogo.

Consideramos os extras como uma extensão da ficha de personagem, portanto quem controla o personagem que possui algum extra, controla também esses benefícios. Na maioria das vezes serão jogadores, mas PdNs também podem possuir extras controlados pelo Narrador.

É preciso pagar um **custo** ou receber **permissão** para adquirir um extra.

## A Regra de Bronze ou Fate Fractal

Antes de continuar, lembre-se do seguinte:

**Em *Fate*, tudo pode ser tratado como se fosse um personagem. Qualquer coisa pode possuir aspectos, perícias, façanhas, barra de estresse e consequências se for preciso.**

Chamamos isso de Regra de Bronze, mas você também pode conhecê-la como *Fate* Fractal caso costume ler sobre o assunto na internet. Já vimos alguns exemplos desta regra neste livro; damos aspectos ao jogo durante sua criação, colocamos aspectos de situação no ambiente assim como em personagens, e o Narrador pode permitir que riscos ambientais ataquem como se tivessem perícias.

Neste capítulo, estenderemos as possibilidades ainda mais.

- [« Evolução do Mundo](../evolucao-do-mundo/)
- [Criando um Extra »](../criando-um-extra/)

