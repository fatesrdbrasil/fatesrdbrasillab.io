---
title: Opções Para Criação de Personagem
layout: default
---

## Opções Para Criação de Personagem

O sistema de criação de personagens no Fate Sistema Básico apresenta um estilo específico de elaboração de personagens, um que lhe dá bastante liberdade, mas também provê estrutura e guia os jogadores a criarem um grupo coerente de indivíduos. Talvez você queira algo um pouco menos aberto ou algo um pouco mais focado.

### Profissões e Raças

Qualquer um que já tenha jogado RPGs de fantasia está ao menos vagamente familiarizado com esta ideia. Para aqueles que não conhecerem, uma **profissão** é um conjunto de perícias e habilidades que partem do conceito de um personagem em particular, frequentemente ligado ao que você _faz_ no mundo de jogo. Uma **raça** descreve o que você _é_ — de onde
veio, quem é seu povo e quais são seus talentos naturais.

---

#### A PALAVRA “RAÇA”

O termo “raça” é impreciso e problemático neste contexto. Nós não estamos usando essa palavra como as pessoas a usam hoje em dia; o que queremos dizer é algo mais próximo de uma espécie. Ao criar uma raça em Fate, o que está fazendo na verdade é criar regras para os tipos de seres além dos humanos, não uma nacionalidade ou etnia.

Então porque usamos a palavra “raça”? Porque é comum nos RPGs. As pessoas sabem o que significa porque diversos jogos no passado já a usavam, o que causa um entendimento imediato sobre o que estamos falando. É o melhor termo para isso? Não. Mas não temos outra palavra com o mesmo impacto na história deste hobby, então usaremos esta, cientes de que é um termo problemático.

---

Em Fate, uma profissão é representada por um conjunto de perícias distribuídas na pirâmide, com valores específicos. Alguns espaços são deixados em branco e outros são definidos. Ao escolher uma profissão, você recebe a pirâmide relacionada a ela. Preencha o que permanecer me branco com o que desejar. Uma profissão também inclui alguns aspec- tos; escolha ao menos um desses ou crie novos baseados no tema representado pelos aspectos fornecidos. Não pegue mais do que dois aspectos de sua profissão. Por último, uma profissão possui uma lista de façanhas disponíveis. Essas são exclusivas; se você faz parte daquela profissão, poderá escolhê-las. Se não for da profissão, então essas façanhas estão fora de cogitação.

Uma raça em Fate vem com um certo número de aspectos; escolha ao menos um, mas não mais que dois. Assim como na profissão, você pode criar seus próprios aspectos se desejar. Sua raça também lhe concede uma perícia racial com alguns detalhes diferenciados (veja os exemplos abaixo). Coloque sua perícia racial em qualquer dos espaços vazios que possuir na pirâmide para determinar o diferencial de seu personagem em seu meio; a descrição explica como você pode usar a sua perícia racial. Você pode diferenciá-la ainda mais modificando-a como uma façanha.

---

### Profissão: Guerreiro

#### Aspectos

___Mercenário Experiente, Cavaleiro do Reino, Mestre de Armas, Defensor Robusto, Veterano de Guerra.___

#### Perícias

+ Ótimo (+4): Lutar
+ Bom (+3): Atletismo ou Vigor e outra perícia à escolha
+ Razoável (+2): Atirar ou Provocar e duas outras
+ Regular (+1): Quaisquer quatro perícias

#### Façanhas


__Escudeiro:__ Sempre que receber uma consequência suave você pode optar por destruir sua armadura ou escudo, contanto que esteja usando o item. Uma vez que sua armadura ou escudo tenham sido destruídos, você precisará realizar os devidos reparos ou adquirir novos.

__Força Bruta:__ Você pode gastar um ponto de destino para superar um obstáculo automaticamente, mesmo que já tenha rolado os dados, contanto que esteja tentando algo que use a força bruta.

__Caçador de Recompensas:__ Sempre que entrar em alguma região, você pode procurar por um trabalho. Após mais ou menos uma hora, você encontra um trabalho que paga bem, mas ou é perigoso, ou não tão perigoso, mas não paga muito bem. Se gastar um ponto de destino, o trabalho não é muito perigoso e ainda paga bem.

__Defensor dos Fracos:__ Quando alguém é atacado fisicamente na mesma zona que você, gaste um ponto de destino para redirecionar esse ataque para você mesmo. Você se defende desse ataque com +1.

---

---

### Profissão: Ladrão

#### Aspectos

**_Especialista em Aquisições, Vigarista Astuto, Membro da Guilda dos Ladrões, Faca Sorrateira, Invasor(a) de Domicílios._**

#### Perícias

+ **Ótimo (+4):** Enganar ou Furtividade
+ **Bom (+3):** Atletismo ou Provocar e outra perícia à escolha
+ **Razoável (+2):** Lutar ou Atirar e duas outras
+ **Regular (+1):** Quaisquer quatro perícias

#### Façanhas

**Golpe Traiçoeiro:** Quando puder surpreender o seu inimigo ou atacar de um lugar escondido, você pode realizar ataques com Furtividade. Se bem-sucedido, você cria um impulso. Se bem-sucedido com estilo, você poderá invocar esse impulso duas vezes gratuitamente.

**Identidade Secreta:** Você possui uma identidade alternativa que pode assumir. Descreva sua identidade, escolha um aspecto de conceito e dificuldade para sua identidade alternativa, assim como a melhor perícia. Você pode assumir essa identidade com alguma preparação e o gasto de um ponto de destino. Enquanto estiver nessa identidade, o conceito e dificuldade substituirão o seu e você pode usar Enganar em lugar de sua perícia de nível mais alto. Você perde esses
benefícios assim que o disfarce cair, e talvez tenha que gastar algum tempo criando uma nova identidade.

**Criminoso Nato:** Sempre que entrar em uma região pela primeira vez, você pode gastar um ponto de destino para declarar que os criminosos locais o conhecem. Escolha uma das seguintes opções: Eles possuem algum trabalho promissor; você e seus companheiros terão acesso a quartos e comida gratuitos por algumas semanas; o ajudarão com algo _imediatamente_ , mas você fica devendo um favor.

**Nem Um Pouco Ameaçador:** Escolha Enganar ou Furtividade quando usar esta façanha. Quando você cria vantagem com a perícia escolhida para causar a impressão de não ser ameaçador ou o mais discreto possível, os inimigos procurarão outro alvo enquanto esse aspecto existir. Assim que você realizar um ataque bem-sucedido, o aspecto some.

---

---


### Raça: Elfo

#### Aspectos

___A Sabedoria de Séculos, “Conheço Esta Floresta”, A Grande Jornada, Magia no Sangue, Perfeição em Tudo.___

#### Perícia Racial: Elfo

Você pode usar a perícia Elfo para reconhecer fauna e flora úteis, conhecer caminhos pelas florestas ou notar perigos escondidos. Além disso, escolha uma das seguintes vantagens (você pode pegar mais de uma pelo custo de uma façanha ou recarga para cada). 

__Alta Magia Élfica:__ Você pode usar a perícia Elfo para conjurar magias ligadas à natureza, mesmo que não possua nenhuma habilidade mágica.

__Perfeição Em Batalha:__ Escolha Atirar ou Lutar. Quando estiver usando armamentos típicos de seu povo, você pode usar a perícia Elfo no lugar da perícia escolhida.

---

---

### Raça: Orc

#### Aspectos

___Sangue e Glória, Todos Temem a Horda, “A Dor é Para os Fracos”, Os Espíritos me Guiam, Guerreiro Dos Sete Clãs.___

#### Perícia Racial: Orc

Você pode usar esta perícia para resistir a dor, clamar aos espíritos por auxílio ou realizar feitos de força bruta. Adicionalmente, escolha uma das seguintes vantagens (você pode pegar mais de uma ao custo de uma façanha ou recarga para cada).

__Fúria Sanguinária:__ Ao usar a perícia Orc para criar vantagem representando uma fúria de batalha, você recebe uma invocação extra para o aspecto caso seja bem-sucedido ou bem-sucedido com estilo.

__Pele Resistente:__ Você pode usar Orc em lugar de Vigor para determinar seu estresse e consequências físicos, e também ganha uma consequência física suave adicional.

---

### A História de Origem

A história de origem é um método para dar um início rápido à criação de personagens já em jogo, usando trechos narrativos para cada jogador. Antes de começar a jogar uma história de origem, um personagem precisa de duas coisas: um conceito e uma perícia no nível mais alto. A maioria dos jogadores terá ao menos uma ideia geral do que quer ser, mas pode ter dúvidas nos detalhes. O conceito e a perícia mais alta definem o personagem de forma ampla, criando um ponto de partida para uma história de origem.

Jogar a história de origem com um personagem é como jogar uma partida normal, com a diferença que você tem o objetivo de definir quem o personagem é. Comece no meio da narrativa — ao começar com algo acontecendo, você dá ao jogador a oportunidade de fazer escolhas sobre o seu personagem.

Cada personagem inicia sua história de origem com um ponto de destino.

### **Escolhendo Perícias**

Durante a história, peça várias rolagens de perícia. Foque na perícia mais alta do personagem até certo ponto, mas também faça rolagens de outras perícias. Sempre que o jogador realizar uma rolagem de perícia e não quer ter o nível Medíocre (+0) nela, ele pode colocar essa perícia em um dos espaços disponíveis. Feito isso, ela passa a fazer parte do personagem.um pouco mais difícil para criar desafios à altura.

### **Escolhendo Aspectos**

Coloque o jogador em uma grande variedade de situações, expondo-o a dificuldades diferentes. Quando um jogador encontrar algum problema, como, por exemplo, quando precisar de um bônus de +2 ou rolar novamente, sugira um aspecto. Se ele aceitar a sua sugestão ou criar seu próprio aspecto, deixe-o invocá-lo uma vez de graça e conceda um ponto de destino!

### **Escolhendo Façanhas**

Você pode oferecer façanhas ao jogador da mesma forma que ofereceu os aspectos — apresentar algo que permita se safar de uma situação complicada ou permitir que consiga realizar algo que precisa ser feito. Assim como no Fate Sistema Básico, o personagem recebe três façanhas gratuitas e pode pegar façanhas adicionais ao reduzir a recarga – a menos que isso tenha sido alterado, claro. Se for uma façanha com uso limitado ou que custa um ponto de destino, deixe-o usar uma vez gratuitamente.

### **Envolvendo Outros Jogadores**
A história de origem do personagem do jogador é uma atividade comunitária! Outros jogadores podem aparecer como PdNs — você pode sugerir isso a eles, se precisar que alguém interprete um personagem em particular. Eles podem até mesmo aparecer com seus próprios personagens, independente se já tiverem jogado sua própria história ou não.

Outros jogadores podem sugerir aspectos, mas apenas se esses aspectos definirem um relacionamento com seus próprios personagens. Se dois jogadores definirem um relacionamento durante uma história de origem, ambos recebem um aspecto e um ponto de destino — que o outro jogador poderá usar em sua própria história.

### Finalizando um Histórico

Prossiga com a história de origem até uma conclusão lógica, mas tente não prolongá-la além de quinze ou vinte minutos antes de passar para a próxima. A ideia é jogar a história de cada um dos jogadores em uma única sessão

------

- [« O Grande Jogo](../o-grande-jogo/)
- [Forçando Aspectos Para Estruturar Aventuras »](../forcando-aspectos-para-estruturar-aventuras/)
