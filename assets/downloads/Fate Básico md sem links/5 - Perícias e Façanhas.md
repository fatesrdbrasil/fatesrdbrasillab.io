# 5 - Perícias e Façanhas

## O Que É Uma Perícia?

Uma **perícia** é uma palavra que descreve a competência de alguém em algo – como Atletismo, Lutar, Enganar – que o seu personagem adquiriu por talento inato, treinamento ou anos de tentativa e erro. As perícias são a base de tudo que o seu personagem sabe fazer no jogo que envolva desafios (e dados).

As perícias são medidas através da escala de adjetivos. Quanto mais alto, melhor seu personagem é na perícia. De forma geral, a sua lista de perícias serve como uma visão geral do potencial de seu personagem – no que você é excelente, no que é bom e no que não é tão bom.

Definimos as perícias de duas maneiras em *Fate* – em termos de *ação em jogo* que você pode executar com elas e *contexto* no qual pode usá-las. Existem apenas algumas ações básicas de jogo, mas o número de contextos é infinito.

### Ações Básicas

Cobriremos isso com mais detalhes em *Ações e Resoluções*, mas aqui está uma referência para que você não precise ir até lá agora.

- **Superar:** Com essa ação, você enfrenta um desafio, tarefa envolvente ou obstáculo relacionados à sua perícia.
- **Criar Vantagem:** Criar vantagens é uma forma de descobrir e criar aspectos e ganhar invocações gratuitas deles, seja  quando está descobrindo algo já existente sobre um oponente ou criando uma situação que lhe traz um benefício.
- **Atacar:** Você tenta ferir alguém em um conﬂito. Esse dano pode ser físico, mental, emocional ou social.
- **Defender:** Você tenta evitar que alguém lhe cause dano, passe por você ou crie vantagem para ser usada contra você.

Algumas perícias também possuem um efeito especial, como conceder algumas caixas de estresse extras. Veja [Vigor]() e [Vontade]() na lista de perícias abaixo para ver mais exemplos.

Mesmo que haja apenas quatro ações que podem ser usadas em conjunto com as perícias, a perícia em questão fornece o contexto necessário para a ação. Por exemplo, tanto Roubo quanto Ofícios permitem que você crie vantagem, mas em contextos bastante diferentes – Roubo permite fazer isso se você estiver prestes a entrar em um local e Ofícios permite fazer isso quando você está avaliando algum objeto. As diferentes perícias permitem a você diferenciar as habilidades de cada PJ, permitindo que cada um contribua de forma única ao jogo.

## O Que É Uma Façanha?

Uma **façanha** é um traço especial do seu personagem que muda a forma como uma perícia funciona. Uma façanha é algo especial, uma forma privilegiada de um personagem usar uma perícia de maneira única, o que é bastante comum em vários cenários – treinamento especial, talentos excepcionais, a marca do destino, alterações genéticas, poderes natos e uma miríade de outras razões podem explicar o porquê de algumas pessoas serem melhores em suas perícias do que outros.

Diferentes das perícias, que são coisas que qualquer um poderia conseguir fazer na campanha, façanhas são únicas e personalizadas. Por essa razão, as próximas páginas falarão sobre como criar suas façanhas, mas há também uma lista de façanhas de exemplo para cada perícia padrão.

Adicionar façanhas a seu jogo fornece uma gama de personagens diferentes, mesmo que possuam as mesmas perícias.

> Tanto Esopo quanto Fräkeline possuem a perícia Luta, mas Fräkeline também possui **Mestre em Combate**, que a torna mais apta a Criar Vantagem com essa perícia. Isso diferencia bastante os dois personagens – Fräkeline possui uma capacidade única de analisar e entender as fraquezas de seus inimigos de uma forma que Esopo não consegue.
>
> Podemos imaginar Fräk começando uma briga para testar os movimentos e ataques do inimigo, analisando cuidadosamente os limites de seu oponente antes de lançar um golpe decisivo, enquanto Esopo se contenta em apenas entrar quebrando tudo.

Você também pode usar isso para separar um certo grupo de habilidades como pertencentes a um grupo de pessoas, se for algo que o cenário requer.

Por exemplo, em uma ambientação contemporânea, pode ser que possuir apenas uma perícia não seja suficiente para tornar alguém um médico profissional (a não ser, claro, que seja um jogo sobre médicos). No entanto, como uma façanha para outra perícia mais geral (como Conhecimentos), é possível que um personagem seja "o médico" se é o que o jogador deseja.

### Façanhas e Recarga

Adquirir uma nova façanha reduzirá sua recarga inicial em um ponto para cada façanha acima de três.

## **Criando Façanhas**

Em *Fate*, permitimos que os jogadores criem as façanhas durante a criação do personagem ou deixem-nas em aberto para fazer isso durante o jogo. Há alguns exemplos de façanhas listados abaixo, junto às perícias. Não é uma lista definitiva; na verdade, elas estão lá para mostrar a você como criar as suas próprias façanhas (embora você possa simplesmente escolher alguma já presente no livro, se assim desejar).

Também temos uma lista das coisas que uma façanha pode ser capaz de *realizar*, para ajudá-lo quando for criá-las em jogo. Em caso de dúvida, dê uma olhada na lista de façanhas para buscar inspiração, assim como nas façanhas dos personagens de exemplo.

------

Narradores que queiram reforçar um conjunto em particular de perícias como sendo importantes ou únicas para o jogo podem optar por criar uma lista de façanhas, facilitando a busca por referências durante a criação do personagem. Isso normalmente é feito como parte da criação dos extras; veja a sessão *[Extras]()* na página XX para mais detalhes.

------

### Adicionando Uma Nova Ação a Uma Perícia

A opção mais básica de uma façanha é permitir a uma perícia realizar algo que ela normalmente não pode fazer. Ela adiciona uma nova ação para a perícia para uso em situações específicas, àqueles que possuírem a tal façanha. A nova ação pode estar disponível para outra perícia (permitindo que uma perícia seja trocada por outra em certas circunstâncias) ou alguma que não está disponível para nenhuma delas.

Aqui estão algumas façanhas com esse efeito:

-  **Golpe Traiçoeiro.** Você pode usar Furtividade para realizar ataques físicos, desde que o alvo não tenha notado sua presença.
- **Sede De Vitória.** Você pode usar Provocar para entrar em conﬂitos nos quais normalmente precisaria usar Vigor sempre que sua capacidade de desconcentrar o seu oponente com sua presença seja viável.
-  **Você Nunca Está a Salvo.** Você pode usar Roubo para realizar ataques mentais e criar vantagem sobre um alvo, intimidando e fazendo-o sentir-se inseguro.

------

Só porque você possui uma façanha não significa que deve usá-la sempre que for relevante. Usar uma façanha sempre será uma escolha e você pode não usar se não achar apropriado ou simplesmente não quiser.

Por exemplo, você poderia ter uma façanha que lhe permitisse entrar em uma luta usando Atletismo no lugar de Lutar quando está lidando com arcos e outras armas de projéteis. Quando for atacar à distância você pode escolher usar Lutar ou Atletismo. Fica a seu critério.

------

### Adicionando Bônus a Uma Ação

Outra forma de usar as façanhas é agregando um bônus automático a uma perícia em uma situação específica, deixando o personagem se especializar em algo. A circunstância deve mais limitada do que o permitido normalmente pela ação e deve se aplicar a apenas uma ou duas ações. 

O bônus normal é +2. No entanto, se desejar, você também pode expressar esse bônus como duas tensões a mais de efeito após ser bem-sucedido na rolagem, se isso fizer mais sentido. Lembre-se, grandes alterações em sua rolagem permitem a você ser mais eficaz.

Você também pode usar isso para estabelecer qualquer efeito como benefício adicional por ser bem-sucedido em uma rolagem. Ou seja, isso pode ser uma oposição passiva Razoável (+2) para um oponente, o equivalente a -2 em dano, uma consequência suave ou uma vantagem que precisa de oposição Razoável (+2) para remover.

Aqui, alguns exemplos sobre como adicionar bônus a uma ação:

-  **Especialista Arcano.** Ganhe um bônus de +2 na ação de criar vantagem quando usar Conhecimentos, quando estiver em uma situação sobrenatural ou oculta.
-  **Chumbo Grosso.** Você realmente gosta de descarregar balas. Em qualquer momento que usar uma arma automática e for bem-sucedido em um ataque usando Atirar, você cria automaticamente uma oposição de +2 contra movimentos na zona atual até o próximo turno, por causa das balas voado no ar (normalmente você precisa de uma ação separada para criar esse tipo de oposição, mas não quando usa uma façanha).
-  **Filho da Corte.** Ganha um bônus e +2 no uso da ação criar vantagem usando a perícia Comunicação quanto realizar alguma função aristocrata, como em um baile real.

### Criar Uma Exceção à Regra

Por fim, uma façanha pode permitir a uma perícia, em circunstâncias restritas, uma exceção a qualquer outra regra do jogo que não se encaixe perfeitamente na categoria de determinada ação. O capítulo [*Desafios, Disputas e Conﬂitos*]() está repleto de regrinhas que cobrem certas circunstâncias nas quais uma perícia pode ser usada e o que acontece. As façanhas podem quebrar isso, permitindo ao seu personagem expandir as possibilidades.

O único limite para esse tipo de façanha é que ela nunca pode alterar as regras básicas dos aspectos em termos de invocar, forçar e na economia de pontos de destino. Isso permanece inalterado.

Aqui estão alguns exemplos de façanhas que criam exceções às regras:

-  **Ritualista** Use Conhecimentos em lugar de outra perícia durante um desafio, permitindo a você usá-la duas vezes no mesmo desafio.
-  **Atar.** Quando você usa Ofícios para criar uma vantagem ***Atado*** (ou similar) sobre alguém, você sempre pode criar uma oposição ativa contra quaisquer rolagens de superar para escapar da imobilização que você criou (também usando Ofícios), mesmo se não estiver por lá. Normalmente, se você não está lá, o personagem escapa com uma rolagem contra uma oposição passiva, o que torna escapar muito mais fácil.
-  **Contra-Ataque.** Se você for bem-sucedido com estilo em uma rolagem de Lutar para se defender, pode optar por inﬂigir 2 caixas de estresse ao invés de receber um impulso.

### Balanceando a Utilidade de Façanhas

Se você olhar a maioria das façanhas de exemplo, vai perceber que as situações em que pode usá-las são bem limitadas quando comparadas com as perícias que elas afetam. Esse é o ponto certo que você procura para suas próprias façanhas - limitadas o suficiente para que sejam especiais quando usadas, mas não tanto que não consigam aparecer em jogo.

Se a façanha *toma lugar* de todas ações básicas da perícia, então não está limitada o suficiente. Você não quer que uma façanha substitua a perícia que ela modifica.

As duas principais formas de manter uma façanha limitada é fazer com que ela cubra apenas uma ação específica ou um par de ações (apenas ao criar vantagem ou apenas em rolagens de ataque ou defesa) ou por limitar a situação em que você pode usá-la (apenas entre os nobres, apenas quando lida com o sobrenatural, etc.).

Para ter os melhores resultados, use ambos - restrinja a façanha a uma ação específica, que só pode ser usada dentro de uma situação específica durante o jogo. Se você achar que a situação é muito limitada, volte um pouco e pense em como a perícia poderia ser usada em jogo. Se a façanha for útil para um desses usos, você provavelmente está no caminho certo. Se não, pode ser necessário ajustar um pouco a façanha para garantir que ela apareça.

Também é possível restringir uma façanha por fazê-la útil apenas em um determinado período do jogo, como uma vez por conﬂito, uma vez por cena ou uma vez por sessão.

------

Jogadores que criarem façanhas que forneçam um bônus a uma ação devem pensar em situações que não ocorrem com tanta frequência em jogo. Por exemplo, no caso do Especialista Arcano acima seria impróprio se em seu jogo não aparecerem muitas situações sobrenaturais, e Filho da Corte seria inútil se sua campanha não há muitos detalhes sobre a nobreza. Se você acha que não vai usar uma façanha ao menos duas vezes por sessão, procure alterá-la.

Narradores são responsáveis por ajudar os jogadores a ver quando suas façanhas podem ser úteis – veja as condições que eles criaram como coisas que eles desejam ver em suas sessões.

------

> Léo está pensando em uma façanha para Esopo chamada “**Minha Arma Detona**”. Ele quer adicionar duas tensões quando for bem-sucedido em um ataque usando Atirar, sempre que usar sua “incrível arma lançadora de cascavéis”, como ele diz.
>
> Amanda pensa um pouco. Ele preenche todos os requisitos, mas há um problema – nem Amanda nem Léo conseguem imaginar muitas situações onde Esopo não vá querer utilizar sua arma. Basicamente, ele poderá usar essa façanha sempre que usar a perícia Atirar. Ela decide que a façanha fornece muita vantagem e pede para que ele modifique.
>
> Léo pensa um pouco e diz “Então, que tal se ela for melhor quando uso ela contra outros seres do mar?”
>
> Amanda pergunta, “Mas vocês vão encontrar tantos camarões e tubarões assim? Acho que o ponto principal fosse vocês encontrando raças bem diferentes e visitando lugares interessantes.”
>
> Léo concorda que isso não apareceria com tanta frequência e pensa mais um pouco.
>
> Então surge uma ideia. “Que tal isto – e se, quando alguém usa sua caixa de estresse de 2 pontos para absorver um dos meus ataques de Atirar, eu poder fazer com que usem sua consequência suave no lugar?”
>
> Amanda gosta, porque pode aparecer em vários conﬂitos que Esopo participar, mas é algo que não fornece vantagem demasiada. Ela pede para que isso se limite e um uso por conﬂito, para finalizar. Na ficha de Esopo, Léo escreve:
>
> -  **Minha Arma Detona:** Uma vez por conﬂito, você pode forçar um oponente a receber uma consequência suave, em lugar de 2 caixas de estresse em um ataque bem-sucedido usando Atirar com sua arma lançadora de cascavéis.

------

### Façanhas Movidas a Pontos de Destino

Outra forma de restringir o uso de façanhas é o seu custo em pontos de destino. Isso é uma boa opção se a façanha em si for bastante poderosa ou se você não encontrar uma forma de usá-la menos em jogo.

Nosso conselho para determinar se a façanha é muito poderosa é checar se ela vai além dos limites que sugerimos acima (como adicionar uma nova ação a uma perícia e um bônus), ou se afeta significativamente o conﬂito. Especificamente, quase qualquer façanha que permita causar estresse adicional em um conﬂito deve custar um ponto de destino para ser usada.

------

### Façanhas Interligadas

Se você quer entrar em detalhes sobre um tipo de talento ou treinamento em particular, você pode criar façanhas interligadas: um grupo de façanhas relacionadas entre si, ligadas umas às outras de alguma forma.

Isso permite que você crie coisas como estilos de luta, clãs ou escolas específicas em seu mundo de jogo, e representa os benefícios de pertencer a um deles. Isso também o ajuda a especificar que tipos de competências especializadas estão disponíveis, se você quiser que seu jogo tenha "classes de personagens" distintas - pode haver um grupo de façanhas interligadas para o "Ás dos Céus", bem como para o "Ladrão Sorrateiro".

Criar um grupo de façanhas interligadas é fácil. Monte uma façanha original que serve como pré-requisito para todas as outras do grupo, permitindo que você pegue essas outras façanhas melhores em seguida. Depois, você precisa criar uma porção de façanhas que sejam interligadas a primeira de alguma forma, melhorando os efeitos iniciais ou ampliando para outros tipos de efeitos.

### Agrupando os Efeitos

Talvez a maneira mais simples de lidar com uma façanha relacionada seja fazer a façanha original mais eficiente na mesma situação:

-  Se a façanha adicionar uma ação, limite-a e dê à nova ação um bônus. Siga as mesmas regras para adicionar bônus – as circunstâncias em que ele se aplica devem ser mais restritas do que na ação base.
-  Se a façanha acrescer um bônus à ação, inclua um bônus adicional de +2 para a mesma ação, *ou* um efeito de duas tensões àquela ação.
-  Se a façanha criar uma exceção à regra, faça uma exceção ainda maior (isso pode ser difícil, dependendo da exceção original. Não se preocupe, você tem outras opções).

Tenha em mente que uma façanha melhorada substitui efetivamente a original. Pense nela como uma única super-façanha que custa dois espaços (e dois pontos de recarga) por ser mais poderosa que outras façanhas. Aqui estão algumas possibilidades:

-  **Mestre Em Combate Avançado.** (requer Mestre em Combate, p. XX) quando você está em uma luta contra alguém armado, recebe um bônus de +2 ao criar uma vantagem usando a façanha Mestre Em Combate.
-  **Descendente da Corte.** (requer Filho da Corte, p. XX) Quando usar Superar em conjunto com a façanha Filho da Corte, você pode adicionalmente criar um aspecto de situação que descreva como a atitude geral de todos fica a seu favor. Se alguém quiser tentar se livrar desse aspecto ele deve ser bem-sucedido contra uma oposição Razoável (+2).
-  **Ritualista Experiente.** (requer Ritualista, p. XX) Você recebe um bônus de +2 quando usa a perícia Conhecimentos em lugar de outra qualquer durante um desafio. Isso permite a você usar Conhecimentos duas vezes no mesmo desafio.

### Ramificando Efeitos

Ramificar efeitos é criar uma nova façanha relacionada à original em tema ou alvo, mas fornecendo um efeito totalmente novo. Se você pensar nos efeitos cumulativos como uma linha vertical de uma façanha ou perícia, imagine os efeitos ramificados como expansões laterais dessa linha.

Se sua façanha original adicionava uma ação a uma perícia, uma façanha ramificada pode adicionar uma ação diferente àquela perícia, conceder um bônus a uma ação diferente que a perícia já tenha ou criar uma exceção às regras, etc. O efeito mecânico não está relacionado à façanha original, mas dá seu próprio toque complementar à mistura.

Isso permite criar alguns caminhos diferentes para fazer algo incrível com apenas uma façanha. Você pode usar isso para destacar diferentes elementos de certa perícia e ajudar os personagens a se diferenciarem uns dos outros, mesmo que possuam níveis altos nas mesmas perícias, por possuírem façanhas conectadas diferentes.

Um exemplo de como isso funciona: vamos dar uma olhada na perícia Enganar. Ao ler a descrição da perícia, há vários caminhos que podemos usar para criar façanhas: mentir, prestidigitação, disfarce, criar histórias duvidosas ou conﬂitos sociais.

Vamos fazer nossa primeira façanha parecida com isso:

-  **Enrolador.** Você receber um bônus de +2 quando usar uma ação de superar com a perícia Enganar, desde que você não tenha que falar mais que algumas frases com a pessoa para despistá-la.

Aqui estão algumas opções interessantes para criar façanhas ramificadas:

-  **Disfarce Rápido** (requer Enrolador). Você é capaz de montar um disfarce convincente num piscar de olhos usando itens que o cercam. Faça uma rolagem de Enganar para criar um disfarce sem precisar de preparação em quase qualquer situação**.**
-  **Saída de Mestre** (requer Enrolador). Você consegue inventar uma história como ninguém, mesmo que não tenha preparado nada previamente. Toda vez que tentar superar em público usando a perícia Enganar, adicione automaticamente um aspecto de situação que represente a sua história falsa, e ganhe uma invocação grátis dela.
-  **Ei, o Que Foi Aquilo?** (requer Enrolador). Ganhe um bônus de +2 sempre que usar Enganar para distrair momentaneamente alguém, desde que falar faça parte da distração.

Cada uma dessas façanhas se relaciona tematicamente a usos rápidos e espontâneos de Enganar, mas cada uma tem seu toque especial.

## Lista De Perícias

Aqui está uma lista básica de perícias que serve como exemplo para suas partidas de *Fate*, seguidas de exemplos de façanhas para cada uma delas. São elas que usamos para todos os exemplos nesse livro e devem servir de base para ajustar suas próprias listas, adicionando e restringindo perícias para melhor se enquadrar em seu cenário. Para saber mais sobre como criar suas próprias perícias, veja o capítulo [*Extras*]().

Cada descrição de perícia contém uma lista de ações de jogo que se aplicam à perícia descrita. Essa lista não é completa – veja as instruções do que fazer em casos especiais na [página XX]().

------

### Criando Cenários a partir de Perícias

As perícias são uma das mecânicas primárias que reforçam a ambientação do cenário que você está criando para o seu jogo. As perícias fornecidas nesta lista são propositalmente genéricas para que possam ser usadas em vários tipos de cenário, assim como as façanhas por não estarem atreladas a cenários específicos.

Ao criar seu próprio cenário para usar com *Fate*, você também deve criar sua própria lista de perícias. A lista oferecida aqui é um bom ponto de partida, mas criar perícias específicas para o seu mundo pode ajudar a dar mais profundidade ao usar uma mecânica personalizada para reforçar a história. Façanhas também devem reﬂetir os tipos de habilidades disponíveis em seu jogo.

------

------

### Perícias e Equipamentos

Algumas perícias, como Atirar e Ofícios, pressupõem uso de equipamento específico. Presumimos que se você possui a perícia, também possui o equipamento para usá-la e que a eficácia do equipamento está inclusa no resultado da perícia. Se você deseja tornar equipamento algo especial, vale a pena dar uma olhada no capítulo [*Extras*]().

------

### Lista de Perícias

| **Perícia** | **Superar** | **C. Vantagem** | **Atacar** | **Defender** |
|-------------------|----------------------|------------------------------|-------------------|------------------------|
| Atletismo | X | X |  | X |
| Atirar | X | X | X |  |
| Comunicação | X | X |  | X |
| Condução | X | X |  | X |
| Conhecimentos | X | X |  | X |
| Contatos | X | X |  | X |
| Empatia | X | X |  | X |
| Enganar | X | X |  | X |
| Furtividade | X | X |  | X |
| Investigar | X | X |  |  |
| Lutar | X | X | X | X |
| Ofícios | X | X |  |  |
| Percepção | X | X |  | X |
| Provocar | X | X | X |  |
| Recursos | X | X |  |  |
| Roubo | X | X |  |  |
| Vigor | X | X |  |  |
| Vontade | X | X |  | X |

### Atletismo

A perícia Atletismo representa o nível geral de aptidão física do seu personagem, independentemente se isso deriva de treinamento, dons naturais ou alguma característica específica do cenário (como magia ou alteração genética). É o quão bem você move seu corpo. Assim sendo, é uma escolha popular para personagens que se envolvam em ação.

Atletismo é bem presente em todos os gêneros que combinam com *Fate* — seria apenas desnecessário em jogos que focam exclusivamente em interação social, onde não há conﬂitos físicos.

- **Superar:** movimentos físicos – saltar, correr, escalar, nadar, etc. Você p Atletismo permite superar qualquer obstáculo que exige ode usar a ação de superar com Atletismo para se mover entre as zonas em um conﬂito se houver um aspecto de situação ou outro obstáculo em seu caminho. Você também rola Atletismo para perseguir ou correr em disputas ou desafios que dependam de atividades semelhantes.
- **Criar Vantagem:** está indo para a parte mais alta do terreno, correndo mais rápido Ao criar uma vantagem usando Atletismo, você que seu oponente ou executando manobras acrobáticas deslumbrantes a fim de confundir seus adversários.
- **Atacar:** Atletismo não é utilizado como perícia de combate.
- **Defender:** conﬂitos contra ataques à curta e longa distância. Atletismo é uma perícia genérica na rolagem de defesa em Também é possível usar esta defesa contra personagens que estejam tentando passar por você, houver como intervir fisicamente em tal tentativa.

#### Façanhas para Atletismo

-  **Corredor Audaz:**Durante o conﬂito, você se move duas zonas sem precisar rolar os dados ao invés de apenas uma, desde que não haja aspectos de situação restringindo o seu movimento.
-  **Parkour Hardcore:** +2 quando quiser superar usando Atletismo em uma perseguição em telhados ou outro ambiente precário.
-  **Reação Atordoante:** Quando for bem-sucedido com estilo em uma defesa contra ataque de um oponente, você automaticamente contra -ataca com algum tipo de soco potente ou contundente. Você poderá colocar o aspecto ***Atordoado*** no seu oponente e ganhar uma invocação gratuita dele, ao invés de apenas um impulso.

### Atirar

Atirar é a perícia usada para armas à distância em um conﬂito ou mesmo em situações em que seu alvo não possa resistir ativamente (seja um tiro ao alvo ou a parede de um celeiro).

Assim como Lutar, se seu cenário requerer que haja a distinção entre tipos de armas à distância, é possível desmembrar esta perícia em outras como Arcos, Armas de Fogo, Armas de Feixe, etc. Não vá muito longe com isso, a não ser que seja fundamental para seu jogo.

- **Superar:** A menos que, por alguma razão, você precise demonstrar sua habilidade em Atirar em uma situação em que não esteja em um conﬂito, você provavelmente não usará essa perícia para se livrar de algum obstáculo. Obviamente, disputas envolvendo armas de fogo são bastante comuns em algumas aventuras de ficção, logo recomendamos que você fique de olho nas oportunidades de acrescentá-las no jogo se possuir um personagem que seja especialista nisso.
- **Criar Vantagem:** Em um conﬂito físico, Atirar pode ser usado para executar uma variedade de movimentos, como truques de tiro, manter alguém sob fogo pesado e algo parecido. Em partidascinematográficas, você pode ser capaz de desarmar um oponente com um tiro – bem parecido com o que vemos nos filmes. Você também pode justificar criar aspectos baseados em seu conhecimento em armas (como colocar o aspecto ***Propenso a Travar*** na arma do adversário).
- **Ataque:** Esta perícia permite realizar ataques físicos. Você pode realizar esses ataques de uma a duas zonas de distância, diferente de Lutar (algumas vezes a distância mudará de acordo com a arma).
- **Defesa:** Atirar não pode ser usado para defesa direta - Atletismo serviria para isso. Você pode usá-la para dar cobertura - o que pode servir como defesa para seus aliados ou servir de oposição ao movimento de alguém - embora isso pudesse ser facilmente representado ao criar uma vantagem (***Fogo de Cobertura***, ou ***Chuva de Balas***, por exemplo).

------

Talvez ache impróprio que Atletismo possa ser usado como defesa contra armas de fogo ou outras armas de alta tecnologia que existam em seu cenário. Na verdade não há outra perícia apropriada para se defender contra isso. Se optar por manter as coisas assim, armas se tornarão bastante perigosas. Você também pode escolher outra perícia para esses casos

------

#### Façanhas para Atirar

-  **Tiro Específico:** Durante um ataque usando Atirar, gaste um ponto de destino e declare uma condição específica que você deseja inﬂigir no alvo, como ***Tiro na Mão***. Se bem-sucedido, você coloca isso como um aspecto de situação além do estresse causado pelo acerto.

-  **Rápido No Gatilho:** Você pode usar Atirar no lugar de Percepção para determinar sua ordem no turno de um conﬂito físico onde atirar rapidamente possa ser relevante.

-  **Precisão Misteriosa:** Uma vez por conﬂito, adicione uma invocação grátis de uma vantagem criada que represente o tempo gasto apontando ou alinhando o tiro (como ***Na Mira***).

### Comunicação

A perícia Comunicação é a responsável por fazer conexões positivas com pessoas e incitar emoções positivas. É a perícia para que os outros gostem e confiem em você.

- **Superar:** Use Comunicação para cativar ou inspirar pessoas a fazer o que você quer ou estabelecer uma boa conexão com elas. Cative o guarda para que ele o deixe passar, ganhe a confiança de alguém ou torne-se o centro das atenções na taverna local. Fazer isso com PdNs menos importantes é apenas uma questão de realizar uma ação de superar, mas você pode precisar entrar em uma disputa para conquistar a confiança um PdN mais importante ou PJ.
- **Criar Vantagem:** Use Comunicação para estabelecer um humor positivo em um alvo, cena ou para fazer com que alguém passe a confiar legitimamente em você. Você pode levar alguém na conversa para que ela tenha ***Confiança Elevada*** ou animar uma multidão a ponto de deixá-la ***Eufórica de Alegria***, ou simplesmente deixar alguém ***Falante*** ou ***Colaborativo***.
- **Ataque:** Comunicação não é usada para causar dano, logo esta perícia não é utilizada para realizar ataques.
- **Defesa:** Comunicação pode ser usada como defesa contra qualquer perícia usada com a intenção de ferir sua reputação, azedar um clima bom que tenha criado ou fazê-lo ficar mal na frente dos outros. A perícia não serve, no entanto, como defesa contra-ataques mentais. Isso exige a perícia Vontade.

#### Façanhas para Comunicação

- **Boa Impressão:** Você pode, duas vezes por sessão, elevar um impulso obtido com Comunicação a um aspecto de situação com uma invocação grátis.
- **Demagogo:** +2 em Comunicação quando fizer um discurso inspirador em frente a uma multidão (se houverem PdNs importantes ou PJs na cena, é possível atingir a todos simultaneamente com uma rolagem ao invés de dividir as tensões).
- **Popular:** Se estiver em uma região onde você seja popular e bem-visto, é possível usar Comunicação no lugar de Contatos. Você pode estabelecer sua popularidade gastando um ponto de destino para declarar os detalhes da história ou devido a uma explicação prévia.

### Condução

A perícia Condução trata da operação de veículos e coisas velozes.

O funcionamento da perícia Condução em seu jogo dependerá muito de quantas ações você pretende realizar dentro de um veículo ou outro meio de transporte e do tipo de tecnologia disponível em seu cenário.

Por exemplo, em um cenário futurista de alta tecnologia, com naves espaciais e tudo o mais (como em *Caroneiros de Asteroide*), talvez você possa mudar Condução para Pilotagem (devido às espaçonaves). Em um cenário de guerra, Operação (para tanques e veículos pesados) fará mais sentido. Cenários de baixa tecnologia, onde o transporte pode ser feito com animais, podem combinar mais com Cavalgar.

------

### Veículos Diferentes, Perícias Diferentes

Não crie muitas categorias a não ser que faça uma diferença notável no seu jogo. Recomendamos considerar a opção de possuir perícias modificadas por façanhas (veja [*Criando Façanhas*]() na página XX).

------

- **Superar:** Condução é equivalente a Atletismo quando é utilizada em um veículo –para se movimentar para quando estiver em circunstâncias difíceis, como em terreno acidentado, repleto de buracos ou realizar manobras com o veículo. Obviamente, Condução é muito usada em disputas, especialmente perseguições e corridas.

- **Criar Vantagem:** Uma Você pode usar Condução para determinar a melhor maneira de chegar em algum lugar com um veículo. Uma boa rolagem pode permitir que você aprenda detalhes da rota que são expressos em aspectos ou então declarar que sabe um ***Atalho Conveniente*** ou algo similar.
- Você também pode ler a descrição da perícia Atletismo e transferi-la para um veículo. Usar a ação Criar Vantagem em Condução, muitas vezes, se trata de obter um bom posicionamento, fazer uma manobra ousada (como um ***Giro Aéreo***) ou colocar o seu oponente em uma posição ruim.
- **Atacar:** Condução normalmente não é usado para realizar ataques (apesar de façanhas poderem alterar isso). Se quiser colidir o carro contra algo, você pode usar Condução como ataque, mas levará as mesmas tensões que causar no conﬂito.
- **Defender:** Evitar dano a um veículo em um conﬂito físico é uma das formas mais comuns de usar Condução. Você também pode usá-la para defender-se de vantagens criadas contra você ou deter ações de outros veículos tentando ultrapassá-lo.

#### Façanhas Para Condução

-  **Duro na Queda:** +2 em Condução sempre que estiver perseguindo outro veículo.
-  **Pé na Tábua:** Você pode fazer seu veículo atingir uma velocidade maior do que é possível. Sempre que estiver em uma disputa onde a velocidade é o principal fator (como em uma perseguição ou corrida) e empatar com seu adversário em uma rolagem de Condução, é considerado sucesso.
-  **Preparar Para o Impacto!:** Você ignora duas tensões de dano ao colidir com outro veículo. Portanto, se a colisão e causar quatro tensões de dano, você recebe apenas duas.

### Conhecimentos

A perícia Conhecimentos trata de conhecimento e educação. Assim como outras perícias, ela recebeu esse nome porque se enquadra bem na proposta dos nossos exemplos – outros jogos podem chamá-la de Erudição, Conhecimento Acadêmico ou algo similar.

Se seu jogo por algum motivo precisar de campos de conhecimento separados, é possível dividir a perícia em diversos segmentos que seguem o mesmo modelo do original. Por exemplo, você pode ter uma perícia Conhecimentos específica para o sobrenatural e conhecimento arcano, e uma perícia de Conhecimento Acadêmico ligada à educação tradicional.

- **Superar:** Você pode usar Conhecimentos para se livrar de obstáculos que necessitem da aplicação do conhecimento de seu personagem na tentativa atingir um objetivo. Por exemplo, você talvez faça uma rolagem de Conhecimentos pra decifrar alguma língua ancestral nas ruínas de uma tumba, presumindo que seu personagem já tenha pesquisado sobre isso em algum momento. Sinceramente, você pode usar Conhecimentos sempre que precisar saber se o seu personagem pode responder a alguma pergunta complicada, sempre que houver alguma tensão envolvida com não saber a resposta.
- **Criar Vantagem:** Conhecimentos cria uma variedade de oportunidades ﬂexíveis para criar vantagem, contanto que possa pesquisar sobre a questão. A perícia frequentemente será utilizada para conseguir detalhes da história, alguma informação obscura descoberta ou já conhecida, mas se a informação lhe der vantagem numa cena futura, ela pode se tornar um aspecto. Da mesma forma, você pode usar Conhecimentos para criar vantagens baseadas em qualquer assunto que seu personagem possa ter estudado, o que é um jeito divertido de adicionar detalhes ao cenário.
- **Atacar:** Esta perícia não é usada em conﬂitos (em nossos exemplos, os poderes mentais que Bandu utiliza são baseados na perícia Conhecimentos, a única exceção – ele pode usar Conhecimentos para realizar ataques e defesas mentais. Veja o capítulo Extras para mais detalhes sobre as formas de usar poderes e magias).
- **Defender:** A perícia também não pode ser usada para defender.

#### Façanhas Para Conhecimentos

-  **Já Li Sobre Isso:** Você já leu centenas – se não milhares – de livros sobre inúmeros assuntos. Você pode gastar um ponto de destino para usar Conhecimentos no lugar de *qualquer outra perícia* em uma rolagem, desde que consiga explicar seu conhecimento sobre a ação que quer realizar.
-  **Escudo da Razão:** Você pode usar Conhecimentos como defesa contra tentativas de Provocar, contanto que consiga justificar sua habilidade de superar o medo através de pensamento racional e razão.
-  **Especialista:** Escolha um campo de especialização, como alquimia, criminologia ou zoologia. Você recebe +2 nas rolagens de Conhecimentos que estejam relacionadas ao seu campo de especialização.

### Contatos

Contatos é a perícia de conhecer e criar conexões com pessoas. Ela pressupõe que você seja proficiente no uso de quaisquer meios de formar redes de contatos possíveis no cenário.

- **Superar:** Use Contatos para superar qualquer obstáculo que impeça de encontrar alguém. Seja pela velha busca nas ruas, questionando sua rede de contatos ou procurando arquivos em bancos de dados, você é capaz de encontrar pessoas ou obter acesso a elas.

- **Criar Vantagem:** Contatos permite saber quem é a pessoa certa para o que quer que você precise ou decidir que você já conhece tal pessoa. É provável que você use esta perícia para criar detalhes da história, representados através de aspectos ("Ei, galera, meu contato me informou que Joe Steel é ***O Melhor Mecânico de Toda Região***, vamos falar com ele.").
- Você também pode criar vantagem para saber qual a opinião nas ruas sobre um indivíduo, objeto ou local específicos, baseado no que os seus contatos lhe dizem. Esses aspectos lidam mais com a reputação do que com os fatos, como ***Conhecido Por Sua Vilania*** ou ***Vigarista Notório***. Não há certeza se a pessoa faz jus à reputação que tem, embora isso não torne o aspecto inválido – as pessoas frequentemente ganham reputações enganosas que complicam suas vidas. Esta perícia também pode ser usada para criar aspectos que representem informações plantadas ou coletadas por você através de sua rede de contatos.
- **Atacar:** Esta perícia não é usada para atacar; é difícil machucar alguém apenas por conhecer pessoas.
- **Defender:** Contatos pode ser usada para se defender de pessoas que tentam criar vantagens sociais contra você, contanto que sua rede de contatos possa interferir na situação. Você também pode utilizá-la para evitar que alguém use Enganar ou Contatos para “sumir do mapa” ou para interferir no uso de Investigação em tentativas de rastrear seu personagem.

#### Façanhas Para Contatos

-  **Ouvido Atento:** Sempre que alguém iniciar um conﬂito contra você em uma área onde haja uma rede de contatos sua, você usa a perícia Contatos no lugar de Percepção para determinar a iniciativa, pois foi avisado a tempo.
-  **Língua Afiada:** +2 quando usar ação criar vantagem para espalhar rumores sobre alguém**.**
-  **Peso da Reputação:** Você pode usar Contatos ao invés de Provocar para criar vantagens baseadas no medo gerado pela reputação sinistra associada a você e àqueles que o acompanham. Deve existir um aspecto apropriado que esteja ligado a essa façanha.

### Empatia

Empatia envolve saber e reconhecer alterações de personalidade ou sentimento em uma pessoa. É basicamente Percepção, mas voltada a questões emocionais.

- **Superar:** Você não usa Empatia para se livrar de obstáculos diretamente – o que normalmente acontece é obter algum tipo de informação com ela e então agir com outra perícia. Em alguns casos, no entanto, você pode usar Empatia assim como se usa Percepção, para ver se consegue perceber uma mudança na atitude ou intenção de alguém.

- **Criar Vantagem:** Use Empatia para ler o estado emocional de uma pessoa e ter uma visão geral de quem ela é, presumindo que você tenha algum tipo de contato pessoal com ela. Isso normalmente será feito na tentativa de acessar os aspectos da ficha de outros personagens, mas às vezes você também conseguirá criar aspectos, especialmente em PdNs. Se o alvo possuir alguma razão para notar sua tentativa de leitura, ele poderá se defender usando Enganar ou Comunicação.
- Também é possível usar Empatia para descobrir as circunstâncias que permitirão realizar ataques mentais em alguém, detectando seus pontos fracos.
- **Atacar:**Empatia não pode ser usada em ataques.
- **Defender:**Essa é a perícia ideal para defender-se de manobras de Enganar, pois permite ver através de mentiras e detecte as intenções verdadeiras de alguém. Você também pode usá-la para defender-se de vantagens sociais que estejam sendo criadas e usadas contra você.

**Especial:** Empatia é a principal perícia a ser usada para ajudar outros a se recuperarem de consequências de natureza mental.

#### Façanhas Para Empatia

-  **Detector de Mentiras:** +2 em todas rolagens de Empatia realizadas para discernir ou descobrir mentiras, independente se direcionadas a você ou a outra pessoa.
-  **Faro para Problemas:** Você pode usar Empatia ao invés de Percepção para determinar o seu turno em um conﬂito, contanto que tenha a chance de observar ou conversar com os envolvidos por alguns minutos antes.
-  **Psicólogo:** Uma vez por sessão você pode reduzir um nível de consequência de alguém (severa para moderada, moderada para suave, suave para nenhuma) se for bem-sucedido em uma rolagem de Empatia com uma dificuldade Razoável (+2) para uma consequência suave, Boa (+3) para uma consequência moderada ou Ótima (+4) para severa. Você precisa conversar com a pessoa que está tratando por ao menos meia hora para que ela possa receber os benefícios dessa façanha e também não é possível usá-la em si mesmo (normalmente, essa rolagem é usada apenas para iniciar o processo de melhora, ao invés de alterar o nível da consequência).

### Enganar

Enganar é a perícia para mentir e ludibriar as pessoas.

- **Superar:** Use Enganar para blefar e se livrar de alguém, para fazer alguém acreditar em uma mentira ou para conseguir algo de alguém por fazê-lo acreditar em uma de suas mentiras. Para PdNs genéricos só é necessária uma rolagem para superar, mas para PJs e PdNs importantes é necessário uma disputa contra Empatia. O vencedor da disputa pode justificar a vitória por adicionar um aspecto de situação em seu alvo, se acreditar na sua mentira puder ser útil em alguma cena futura.
Enganar é a perícia que você usa para se disfarçar ou descobrir o disfarce de alguém. Você precisa ter tempo e material necessários para conseguir o efeito desejado (nota: isso funcionou bem no cenário de *Caroneiros de Asteroide*, mas em outros jogos pode ser que Enganar não seja adequada e seja necessária uma façanha).
- Você pode usar enganar para realizar pequenos truques com as mãos e desorientações.
- **Criar Vantagem:** Use Enganar para criar distrações momentâneas, histórias inventadas ou impressões falsas. É possível fintar em uma luta de espadas, fazendo o oponente ficar ***Desbalanceado***. O truque “o que é aquilo!” pode criar uma ***Chance de Escapar***. Você poderia inventar uma ***História Falsa do Nobre Rico*** quando participar de um baile real, ou enganar alguém para fazê-lo revelar um de seus aspectos ou outra informação.
- **Atacar:** Enganar é uma perícia que cria uma grande variedade de oportunidades vantajosas, mas não causa dano diretamente.
- **Defender:** Você pode usar Enganar para espalhar informações falsas e despistar um uso de Investigação contra você ou então para se defender de tentativas de usar Empatia para descobrir suas intenções verdadeiras.

#### Façanhas Para Enganar

-  **Mentiras Sobre Mentiras:** +2 para criar uma vantagem de Enganar sobre alguém que já tenha acreditado em uma de suas mentiras anteriormente nessa sessão.
-  **Jogos Mentais:** Você pode usar Enganar no lugar de Provocar para realizar ataques mentais, desde que consiga inventar uma mentira bem elaborada como parte do ataque.
-  **Um Indivíduo, Muitas Faces:** Ao conhecer alguém novo, você pode gastar um ponto de destino para declarar que já conhecia essa pessoa antes, mas sob determinado disfarce. Crie um aspecto de situação para representar sua história enganosa e você pode usar Enganar no lugar de Comunicação sempre que interagir com essa pessoa.

------

### Perícias Sociais e Outros Personagens

Muitas das perícias sociais possuem ações que alteram o estado emocional de outros personagens ou os fazem aceitar algum fato da história (ao acreditar em suas mentiras, por exemplo).

Um sucesso ao usar uma perícia social *não* dá o direito de forçar outro personagem a agir de forma contrária a sua natureza, ou como a pessoa que o controla o vê. Se outro PJ for afetado por uma de suas perícias, o jogador sugere como seu personagem responde. Ele não pode anular sua vitória, mas pode escolher como ela funcionará.

Então, você pode ser bem-sucedido em Provocar o personagem com gritos e intimidações na intenção de amedrontá-lo e fazê-lo hesitar, criando uma vantagem, mas se o outro jogador não imaginar o seu personagem agindo dessa forma, vocês devem discutir alguma alternativa – talvez você o irrite tanto que ele fique desequilibrado, ou você o constrange ao fazê-lo passar vexame em público.
Não importam os detalhes, desde que você ganhe sua vantagem. Use-a como uma oportunidade de criar uma história com outra pessoa, ao invés de excluí-la.

------

### Furtividade

A perícia Furtividade dificulta que outros o percebam, tanto quando se esconde como quando tenta se deslocar sem ser visto. Ela combina muito bem com a perícia Roubo.

- **Superar:** Você pode usar Furtividade para superar qualquer situação que dependa de você passar despercebido. Esgueirar-se entre sentinelas e seguranças, esconder-se de um perseguidor, apagar evidências de que você esteve em um lugar e outras situações desse tipo são resolvidas com Furtividade.
- **Criar Vantagem:** Você vai usar Furtividade para criar aspectos em você mesmo, se posicionando perfeitamente para um ataque ou emboscada em um conﬂito. Dessa forma, você pode estar ***Bem Escondido*** quando os guardas passarem por você e tirar vantagem disso ou ***Difícil de Encontrar*** quando estiver lutando no escuro.
- **Atacar:** Furtividade não é utilizada em ataques.
- **Defender:** de Percepção para encontrá-lo Você pode usar esta perícia para impedir tentativas , assim como para tentar apagar seus rastros e atrapalhar a rolagem da perícia Investigar de um perseguidor.

#### Façanhas Para Furtividade

-  **Mais Um Na Multidão:** +2 em Furtividade para se camuﬂar em uma multidão. O significado de “multidão” depende do ambiente – uma estação de metrô exige mais pessoas do que um bar pequeno.
-  **Invisibilidade Ninja:**  Uma vez por sessão, você pode desaparecer de vista ao custo de um ponto de destino, usando uma fonte de fumaça (pastilhas, bombas, etc.) ou outra técnica misteriosa. Isso coloca o impulso ***Desaparecido***. Enquanto estiver assim, ninguém pode atacar ou criar vantagem sobre você até que sejam bem-sucedidos em uma ação de superar com Percepção para saber onde você está (basicamente significa que eles abrirão mão de uma ação na tentativa). Esse aspecto desaparece assim que invocado, ou se alguém for bem-sucedido na rolagem de superar.
-  **Alvo Deslizante:** Contanto que esteja na escuridão ou nas sombras, você pode usar Furtividade para se defender contra ataques de Atirar de adversários que estejam ao menos a uma zona de distância.

### Investigar

Investigar é a perícia usada para encontrar coisas. Ela anda lado a lado com Percepção - enquanto Percepção está ligada a estar atento ao que está acontecendo ao seu redor e a observação física, Investigar gira em torno do esforço concentrado e análise profunda.

- **Superar:** Os obstáculos de Investigar são informações difíceis de descobrir por algum motivo. Analisar uma cena de crime em busca de pistas, buscar em um ambiente desorganizado por algo que precisa, até mesmo se debruçar sobre um livro antigo e mofado tentando encontrar a passagem que esclarece tudo.
Correr contra o tempo para coletar uma evidência antes que os policiais apareçam ou um desastre ocorra é uma forma clássica do uso de Investigar em um desafio.
-  **Criar Vantagem:** Investigar é provavelmente uma das perícias mais versáteis que podem ser utilizadas com criar uma vantagem. Se estiver disposto a dedicar algum tempo, poderá encontrar qualquer informação sobre qualquer um, descobrir qualquer detalhe sobre um lugar ou objeto ou criar aspectos sobre quase qualquer coisa no mundo de jogo que o seu personagem poderia razoavelmente trazer à tona.
Se isso soa grandioso demais, considere o seguinte como algumas das possibilidades para o uso de Investigar: espionagem de conversas, procurar pistar em uma cena de crime, examinar registros, verificar a veracidade sobre uma informação, conduzir uma vigilância e pesquisar uma história falsa.
- **Atacar:** Esta perícia não é usada em ataques.
- **Defender:** Ela também não é usada como defesa.

#### Façanhas Para Investigar

-  **Atenção aos Detalhes:** Você pode usar Investigar em lugar de Empatia para se defender contra o uso da perícia Enganar. O que os outros aprendem através de reações e intuição, você aprendeu através da observação minuciosa de micro expressões.
-  **Bisbilhoteiro:** Em uma rolagem bem-sucedida de Investigar usando a ação de criar vantagem na tentativa de espionar uma conversa, você pode descobrir ou criar um aspecto adicional (porém sem receber uma invocação grátis).
-  **O Poder Da Dedução:** Uma vez por cena você pode gastar um ponto de destino (e alguns minutos de observação) para realizar uma rolagem especial de Investigar, representando suas habilidades de dedução. Para *cada* tensão que conseguir nessa rolagem você descobre ou cria um aspecto ou na cena, ou no alvo observado, mas pode invocar apenas um deles gratuitamente.

### Lutar

A perícia Lutar cobre todas as formas de combate corpo a corpo (em outras palavras, entre indivíduos que estejam na mesma zona), tanto desarmado quanto armado. Para armas de longo alcance, veja Atirar.

- **Superar:** Você pode Em geral você não usa Lutar f ora de um conﬂito, portanto ela não é muito usada para se livrar de obstáculos. Você pode usá-la para demonstrar seu poder marcial ou para participar em algum tipo de competição esportiva.
- **Criar Vantagem:** Você provavelmente usará Lutar para criar a maior parte das vantagens em um conﬂito físico. Qualquer manobra especial pode ser representada em vantagens: um golpe atordoador, um "golpe sujo", desarmar, entre outros. Você pode até usar Lutar para analisar o estilo de outro lutador, detectando fraquezas que possam ser exploradas em sua técnica.
- **Atacar:** Autoexplicativo. Você pode realizar ataques físicos com Lutar. Lembre-se que serve apenas para ataques corpo a corpo, então você deve estar na mesma zona que o seu oponente.
- **Defender:** Use Lutar para se defender de ataques ou vantagens criadas contra você com a perícia Lutar, assim como de qualquer ação que possa ser interrompida através da violência. Não é possível usar esta perícia para se defender de ataques de Atirar, a menos que cenário seja fantasioso o bastante para permitir agarrar projéteis no ar ou usar espadas laser para deﬂetir tiros de energia.

#### Façanhas Para Lutar

-  **Mão Pesada:** Quando é bem-sucedido com estilo em um ataque usando Lutar e escolher reduzir o resultado para obter um impulso, você ganha um aspecto de situação com uma invocação grátis ao invés do impulso.
-  **Arma Reserva:** Sempre que alguém estiver prestes a adicionar o aspecto de situação ***Desarmado*** em você ou similar, gaste um ponto de destino para declarar que você possui uma segunda arma. Ao invés de ganhar o aspecto de situação, seu oponente ganha um impulso, representando o momento de distração durante a troca de arma.
-  **Golpe Matador:** Uma vez por cena, ao causar uma consequência em um oponente, você pode gastar um ponto de destino para aumentar a severidade dela (então suave se torna moderada, moderada se torna severa). Se seu oponente receberia uma consequência severa, ele recebe a consequência severa *mais* uma segunda consequência ou tem que sair da luta.

------

### A(s) Arte(s) da Luta

É esperado que a maioria das partidas jogadas em *Fate* tenha uma boa quantidade de ação e conﬂitos físicos. É um ponto muito importante, como na perícia Ofícios, já que as perícias de combate escolhidas dizem muito do estilo do jogo.

Nos exemplos temos Lutar e Atirar como perícias separadas, o que nos dá uma divisão básica sem sermos muito detalhistas. É fácil perceber que isso sugere que um combate usando armas e um combate desarmado são praticamente a mesma coisa – não há vantagem inerente de um para outro. É bastante comum criar uma divisão entre luta armada e desarmada — em categorias como Punhos e Armas, por exemplo.

Você poderia especializar isso ainda mais se quisesse que cada tipo de arma tenha sua própria perícia (espadas, lanças, machados, armas de plasma, carabinas, etc.), mas é bom reiterar mais uma vez que você não se empolgue muito nisso a não ser que seja realmente importante para seu cenário. O uso de especializações em armas também pode ser encontrado na parte [*Extras*]() (p. XX).

------

### Ofícios

Ofícios é a perícia para trabalhar com máquinas e aparelhos, para o bem ou para o mal.

A perícia é chamada Ofícios porque é o que usamos nos exemplos, mas ela pode varias muito dependendo do cenário e da tecnologia disponível. Em um cenário moderno ou de ficção-científica, pode ser que chamar de Engenharia ou Mecânica seja melhor, por exemplo.

- **Superar:** Ofícios é usado para construir, quebrar ou consertar equipamentos, desde que você tenha tempo e as ferramentas necessárias. Muitas vezes, ações usando a perícia Ofícios acontecem como parte de uma situação mais complexa, tornando-a uma perícia popular para desafios. Por exemplo, se estiver tentando consertar uma porta quebrada, falhar ou conseguir não muda muita coisa; é melhor considerar como sucesso e seguir em frente. Agora, se estiver tentando consertar o carro enquanto um grupo de lobisomens está no seu encalço...
- **Criar Vantagem:** Você pode usar Ofícios para criar aspectos que representem características úteis de uma máquina, colocando nela pontos fortes que você pode usar a seu favor (***Blindada***, ***Construção Robusta***) ou uma vulnerabilidade que possa explorar (***Falha no Sinal***, ***Feito às Pressas***).
Criar vantagens usando Ofícios pode ser também uma forma de sabotar e interferir em objetos mecânicos que estejam presentes na cena. Por exemplo, você pode criar uma ***Polia Improvisada*** para ajudá-lo a chegar na plataforma superior ou jogar algo na balista que está atirando em você para que ela tenha os ***Mecanismos Emperrados*** e atrapalhe seu funcionamento.
- **Ataque:** Você provavelmente não vai usar Ofícios para atacar em um conflito, a menos que o conﬂito seja especificamente sobre usar máquinas, como em um cerco armado. Narradores e jogadores devem conversar sobre essa possibilidade se alguém estiver realmente interessado em usar essa perícia. Normalmente, armas construídas provavelmente precisarão de outras perícias na hora de atacar – quem fabrica uma espada ainda precisa da perícia Lutar para usá-la!
- **Defesa:** Como nos ataques, Ofícios não é usada para defender, a menos que você a use para controlar algum tipo de mecanismo de defesa.

#### Façanhas Para Ofícios

-  **Sempre Criando Coisas Úteis:** Você não precisa gastar um ponto de destino para declarar que possui as ferramentas apropriadas para um trabalho em particular usando Ofícios, mesmo em situações extremas (como quando está numa prisão e longe do seu equipamento). Esse tipo de oposição deixa de existir.
-  **Melhor Que Novo:** Sempre que obtiver um sucesso com estilo numa ação de superar para consertar um dispositivo, você pode criar imediatamente um novo aspecto de situação (com uma invocação grátis) que reﬂita as melhorias realizadas, ao invés de apenas um impulso.
-  **Precisão Cirúrgica:** Quando usa Ofícios em um conﬂito envolvendo algum tipo de mecanismo, você pode filtrar alvos indesejados em ataques de zona inteira sem ter que dividir o dano (normalmente é necessário dividir sua rolagem entre os alvos).

------

Se construir coisas e criar itens é uma parte importante do seu jogo, veja o capítulo *Extras* (p. XX) para uma discussão sobre mais usos da perícia Ofícios.

------

------

### Tantos Ofícios...

Se for importante para o seu jogo lidar com *tipos diferentes de tecnologia*, é possível ter vários tipos desta perícia na sua lista. Um jogo futurista, por exemplo, pode ter Engenharia, Cibernética e Biotecnologia, todas com as mesmas ações disponíveis para o seu tipo de tecnologia. Em tais jogos, nenhum personagem pode ter conhecimento em todos os campos sem adquirir diversas perícias individuais.

Se pretende fazer isso, certifique-se de ter um bom motivo – se a *única* coisa que essa divisão causar for nomes diferentes para os mesmos efeitos, você deve mantê-las em uma só, de forma genérica, e usar façanhas para representar as especialidades.

------

### Percepção

A perícia Percepção trata justamente disso - perceber as coisas. Ela anda lado a lado com Investigar, representando a percepção geral do personagem, sua habilidade em notar detalhes rapidamente e outros poderes de observação. Normalmente, ao usar Percepção, o resultado será mais rápido que Investigar, portanto os detalhes são mais superficiais, mas você também não precisa de tanto esforço para encontrá-los.

- **Superar:** Percepção quase não é usada para superar obstáculos, mas é usada como uma reação: perceber algo em uma cena, ouvir algum som fraco, notar a arma escondida na cintura do adversário.
Perceba que isso não é uma desculpa para o Narrador pedir rolagens de Percepção a torto e a direito para ver quão observadores os jogadores são; isso é entediante. Ao invés disso, peça testes de Percepção quando ser bem sucedido resultaria em algo interessante e falhar, também.
- **Criar Vantagem:** Você usa Percepção para criar aspectos baseados na observação direta – procurar em uma sala por aspectos que se destacam, encontrar uma rota de fuga em um prédio abandonado, notar alguém em meio a uma multidão, etc. Ao observar pessoas, Percepção pode mostrar o que está acontecendo com elas *externamente*; para notar mudanças internas, veja Empatia. Você também pode usar Percepção para declarar que seu personagem encontra algo que poderá ser utilizado a seu favor em alguma situação, como uma ***Rota de Fuga Conveniente*** quando está tentando fugir de um prédio ou uma ***Fraqueza Sutil*** na linha de defesa inimiga. Por exemplo, se você estiver em uma briga de bar, poderá realizar uma rolagem de Percepção para dizer que há uma poça no chão, bem próxima aos pés de seu oponente e que poderia fazê-lo escorregar.
- **Atacar:**Esta perícia não é utilizada em ataques.
- **Defender:** Você pode usar Percepção para se defender do uso de Furtividade contra você ou para descobrir se está sendo observado.

#### Façanhas Para Percepção

-  **Perigo Iminente:** Você possui uma capacidade sobrenatural para detectar o perigo. Sua perícia Percepção funciona normalmente para condições como camuﬂagem total, escuridão ou outros empecilhos sensoriais em situações onde alguém ou algo tem a intenção de prejudicar você.
-  **Leitura Corporal:** Você pode usar Percepção no lugar de Empatia para aprender sobre os aspectos de alguém através da observação.
-  **Reação Rápida:** Você pode usar Percepção no lugar de Atirar para realizar tiros rápidos que não envolvam uma grande necessidade de precisão na pontaria. No entanto, por estar realizando uma ação instintiva, você não tem a capacidade de identificar seu alvo quando usar essa façanha. Então, por exemplo, você pode ser capaz de atirar em alguém que está se movendo por entre os arbustos com essa façanha, mas você não será capaz de dizer se é um amigo ou inimigo antes de puxar o gatilho. Escolha com cuidado!

### Provocar

Provocar é a perícia para incomodar os outros e incitar respostas emocionais negativas - medo, raiva, vergonha, etc. É a perícia de "ser desagradável".

Para usar provocar, você precisa de algum tipo de justificativa. Ela pode ou vir da situação, ou porque você possui um aspecto apropriado, ou por ter criado uma vantagem com outra perícia (como Comunicação ou Enganar), ou mesmo por ter avaliado os aspectos de seu alvo (veja Empatia).

Esta perícia necessita que seu alvo possa sentir emoções – robôs e zumbis normalmente não podem ser provocados.

- **Superar:** Você pode provocar alguém para que perca a razão e faça algo que você quer, intimidar para obter informações, irritar a ponto fazê-los reagir ou mesmo assustar alguém, fazendo-o fugir da cena. Isso acontecerá com mais frequência quando você encontrar PdNs menores ou quando não for importante definir os detalhes. Contra PJs ou PdNs importantes, será necessário uma competição contra a perícia Vontade do alvo.
- **Criar Vantagem:** Você pode criar vantagens que representem estados emocionais momentâneos, como ***Enfurecido***, ***Chocado*** ou ***Hesitante***. Seu alvo se defende usando a perícia Vontade.
- **Atacar:** Você pode realizar ataques mentais com Provocar para causar dano emocional a um oponente. Sua relação com o alvo e as circunstâncias do momento são muito importantes ao determinar se é possível ou não usar esta perícia.
- **Defender:** Você precisa de Vontade para isso. Ser bom em provocar outros não o ajuda a se defender.

#### Façanhas Para Provocar

-  **Armadura do Medo:** Você pode usar Provocar para se defender de ataques realizados com a perícia Lutar, mas apenas até o momento que receber algum estresse no conﬂito. É possível deixar seu oponente hesitante em atacar, mas quando alguém os mostrar que você também é humano, a vantagem desaparece.
-  **Incitar Violência:** Ao criar uma vantagem sobre um oponente usando Provocar, você pode usar sua invocação grátis para se tornar o alvo da próxima ação relevante daquele personagem, tirando a atenção dele de outro alvo.
-  **Tá Bom Então!** Você pode usar Provocar no lugar de Empatia para aprender sobre os aspectos de seu alvo, por provocar até que ele revele algum. O lvo pode se defender usando Vontade (se o Narrador achar que o aspecto em si é vulnerável à provocação, você recebe um bônus de +2).

### Recursos

Recursos descreve o nível geral de bens materiais do seu personagem no mundo do jogo, assim como a habilidade de utilizá-los de alguma forma. Isso não precisa significar necessariamente dinheiro na mão, considerando as várias formas de representar riqueza em um dado cenário – em um jogo medieval, pode significar terras, vassalos ou ouro; em um cenário moderno, pode representar seu crédito.

Essa perícia está na lista básica para criar uma forma básica e simples de lidar com riquezas de forma abstrata sem entrar em detalhes contábeis. Algumas pessoas podem achar estranho haver uma perícia estática para algo que costumamos ver como uma fonte limitada de recursos. Se isso lhe incomodar, veja o [quadro na]() página XX para conhecer formas de limitar esses recursos.

- **Superar:** Você pode usar Recursos para se livrar de uma situação onde o dinheiro possa ajudar, como pagar subornos ou adquirir itens raros e caros. Desafios ou disputas podem envolver leilões ou licitações.
- **Criar Vantagem:** Você pode usar Recursos para facilitar as coisas e tornar as pessoas mais amigáveis seja através do suborno (***Uma Mão Lava a Outra...***) ou simplesmente pagando drinques para as pessoas (***No Vinho Repousa a Verdade***). Você também pode usar Recursos para declarar que você possui algo útil em mãos ou que pode consegui-la facilmente, o que pode lhe dar um aspecto que represente tal objeto.
- **Atacar:** Esta perícia não é utilizada em ataques.
- **Defender:** Esta perícia não é utilizada como defesa.

#### Façanhas Para Recursos

-  **O Dinheiro Fala Mais Alto:** Você pode usar Recursos no lugar de Comunicação em qualquer situação na qual dinheiro possa ajudar.
-  **Investidor Experiente:** Você recebe uma invocação gratuita adicional quando criar vantagens com Recursos, contanto representem o retorno de um investimento feito em outra sessão (ou seja, não é possível declarar que já tinha feito, mas se você fizer durante o jogo, você recebe retornos maiores).
-  **Fundos de Investimento:** Duas vezes por sessão, você pode receber um impulso que represente algum tipo de sorte inesperada ou entrada de dinheiro.

------

### Limitando Os Recursos

Caso alguém estiver usando a perícia Recursos exageradamente ou se você simplesmente quer representar o retorno cada vez menor causado pelo gasto constante de suas reservas, considere usar um destes sistemas:

-  Quando um personagem obtiver sucesso em uma rolagem de Recursos, mas não com estilo, dê a ele um aspecto de situação que reﬂita sua perda temporária de recursos, como ***Carteira Vazia*** ou ***Precisando de Dinheiro***. Se isso acontecer novamente simplesmente renomeie o aspecto para algo pior – ***Precisando de Dinheiro*** poderia se tornar ***Falido***, e ***Falido*** poderia se tornar ***Devendo aos Credores***. O aspecto não é uma consequência, mas pode ser um alvo de ações de forçar contra jogadores que não param de gastar. O aspecto pode ser removido se o personagem parar de gastar ou no final da sessão.
-  Sempre que o personagem for bem-sucedido em uma rolagem de Recursos, diminua o nível da perícia em um até o final da sessão. Se ele for bem-sucedido em uma rolagem com um nível Medíocre (+0), não poderá realizar mais rolagens de Recursos naquela sessão.

Se quiser ir além, é possível transformar finanças numa categoria de conﬂito e dar aos personagens caixas de estresse de riqueza, com caixas extras para quem possui Recursos em níveis altos, mas não recomendamos fazer isso a não ser que riqueza material seja realmente algo muito importante no seu jogo.

------

### Roubo

A perícia Roubo cobre a aptidão de seu personagem em roubar coisas e entrar em locais proibidos.

Em cenários de alta tecnologia, esta perícia também inclui proficiência no uso de equipamentos relacionados, permitindo ao personagem hackear sistemas, desarmar alarmes e coisas semelhantes.

- **Superar:** Como mostrado acima, Roubo permite a você superar qualquer obstáculo relacionado a roubo ou infiltração. Contornar fechaduras e armadilhas, bater carteiras, furtar objetos, encobrir rastros e outras atividades semelhantes entram nesta categoria.
- **Criar Vantagem:** Você pode avaliar um determinado local usando Roubo para determinar o quão difícil será entrar e com qual tipo de segurança você está lidando, assim como para descobrir vulnerabilidades que possa explorar. Você também pode examinar o trabalho de outros ladrões para saber como eles realizaram determinadas ações e criar ou descobrir aspectos relacionados a qualquer prova ou pista que possam ter deixado para trás.
- **Ataque:** Esta perícia não é utilizada em ataques.
- **Defender:** O mesmo se aplica aqui. Na verdade ela não é uma perícia de conﬂito, então não há muitas formas de usá-la neste sentido.

#### Façanhas Para Roubo

-  **Sempre Há Uma Saída:** +2 para Roubo quando você tenta criar uma vantagem ao tentar escapar de algum lugar.
-  **Especialista em Segurança:** Não é preciso estar presente para oferecer oposição ativa a alguém que está tentando superar artifícios de segurança criados ou montados por você (normalmente, um personagem rola contra uma oposição passiva nessas situações).
-  **Gíria da Ruas:** Você pode usar Roubo no lugar de Contatos sempre que lidar especificamente com ladrões e marginais.

### Vigor

A perícia Vigor é similar a Atletismo, representando as aptidões físicas naturais do personagem, como força bruta e resistência. No nosso exemplo de jogo, dividimos essa perícia para representar dois tipos diferentes de personagens fortes – o ágil (representado por Atletismo) e o forte (representado por Vigor).

Pode ser que no seu jogo não seja necessário separar as duas coisas – embora você ainda possa permitir que os jogadores façam essa distinção usando aspectos e façanhas.

- **Superar:** Você pode usar Vigor para se livrar de empecilhos que requeiram força bruta – na maioria das vezes para superar um aspecto de situação em uma zona – ou outros obstáculos físicos, como barras de prisão ou portões fechados. Claro, Vigor é a perícia clássica para disputas físicas, como queda de braço, assim como para maratonas ou outros desafios baseados em resistência física.
- **Criar Vantagem:** Vigor oferece uma grande variedade de vantagens em um conﬂito físico, normalmente ligadas a agarrar e imobilizar oponentes, deixando-os ***Derrubados*** ou ***Imobilizados***. Você também pode usar como uma forma de descobrir deficiências físicas em seu alvo – agarrar o mercenário pode mostrar que ele possui uma ***Perna Manca***.
- **Atacar:** Vigor não é usado diretamente para causar dano – veja a perícia Lutar para isso.
- **Defender:** Embora normalmente não se utilize Vigor para se defender de ataques, é possível usar a perícia para oferecer oposição ativa ao movimento de alguém, desde que o espaço seja pequeno o bastante para que seja plausível usar seu corpo como obstáculo. Você também pode colocar algo pesado no caminho e segurar o objeto para impedir alguém de passar.

**Especial:** Esta perícia concede caixas extras de estresse físico ou espaços para consequências. Regular (+1) ou Razoável (+2) libera a caixa de 3 pontos de estresse. Bom (+3) e Ótimo (+4) libera a quarta caixa. Excepcional (+5) ou mais concede, além das caixas, uma consequência suave adicional. Todas essas vantagens são apenas para danos físicos.

#### Façanhas Para Vigor

-  **Lutador:** +2 nas rolagens da perícia Vigor feitas para criar vantagem sobre um inimigo por agarrá-lo ou uma ação similar.
-  **Saco de Pancadas:** Você pode usar Vigor para se defender contra ataques da perícia Lutar feitos com os punhos e pernas ou instrumentos de contusão, porém você sempre recebe uma tensão de estresse físico em caso de empate.
-  **Resistente Como Aço:** Uma vez por sessão, ao custo de um ponto de destino, você pode reduzir a severidade de uma consequência física moderada para uma consequência suave (se o campo da consequência suave estiver livre) ou apagar uma consequência suave.

### Vontade

Esta perícia representa o nível geral da fortitude mental de seu personagem, da mesma forma que Vigor representa a fortitude física.

- **Superar:** Você pode usar Vontade para encarar obstáculos que exijam esforço mental. Enigmas e quebra-cabeças estão dentro desta categoria, assim como qualquer tarefa mental que exija concentração, como decifrar um código. Use Vontade quando vencer um desafio mental for apenas uma questão de tempo e Conhecimentos se for preciso mais do que esforço mental para superar o desafio. Muitos dos obstáculos que você enfrentará usando Vontade podem fazer parte de desafios maiores, para reﬂetir o esforço envolvido.
Disputas de Vontade podem reﬂetir jogos mais desafiadores, como xadrez ou uma bateria de provas difíceis. Em cenários onde a magia ou o psiquismo são habilidades comuns, disputas de Vontade ocorrerão bastante.
- **Criar Vantagem:** Você pode usar Vontade para colocar aspectos em você mesmo, representando um estado de concentração e foco profundos.
- **Atacar:** Vontade não é usada para atacar. Dito isso, em cenários onde há habilidades psíquicas, entrar em conﬂitos desse gênero pode ser algo que você faça usando essa perícia. Esse tipo de coisa pode ser adicionado à Vontade através de uma façanha ou extra.
- **Defender:** Vontade é a perícia principal para se defender contra ataques mentais de Provocar, representando seu controle sobre suas emoções.

**Especial:** A perícia Vontade concede caixas adicionais de estresse e consequência mentais. Regular (+1) ou Razoável (+2) libera a terceira caixa de estresse. Bom (+3) e Ótimo (+4) liberam a quarta caixa de estresse. Excepcional (+5) ou mais concede, além das caixas, um espaço adicional para consequência suave. Todos esses espaços só poderão ser usados para danos mentais.

#### Façanhas Para Vontade

-  **Determinado:** Use Vontade no lugar de Vigor em qualquer rolagem de superar para feitos físicos.
-  **Duro na Queda:** Você pode escolher ignorar uma consequência suave ou moderada pela duração da cena. Ela não poderá ser forçada ou invocada por seus inimigos. Ao fim da cena ela voltará pior, no entanto; se era uma consequência suave, se torna moderada e se era moderada, se tornará severa.
-  **Indomável:** +2 para se defender contra ataques de Provocar ligados especificamente à intimidação ou medo.

