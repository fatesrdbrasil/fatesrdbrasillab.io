---
title: Vigor
layout: default
---

### Vigor

A perícia Vigor é similar a Atletismo, representando as aptidões físicas naturais do personagem, como força bruta e resistência. No nosso exemplo de jogo, dividimos essa perícia para representar dois tipos diferentes de personagens fortes – o ágil (representado por Atletismo) e o forte (representado por Vigor).

Pode ser que no seu jogo não seja necessário separar as duas coisas – embora você ainda possa permitir que os jogadores façam essa distinção usando aspectos e façanhas.

- `O`{: .fate_font} **Superar:** Você pode usar Vigor para se livrar de empecilhos que requeiram força bruta – na maioria das vezes para superar um aspecto de situação em uma zona – ou outros obstáculos físicos, como barras de prisão ou portões fechados. Claro, Vigor é a perícia clássica para disputas físicas, como queda de braço, assim como para maratonas ou outros desafios baseados em resistência física.
- `C`{: .fate_font} **Criar Vantagem:** Vigor oferece uma grande variedade de vantagens em um conﬂito físico, normalmente ligadas a agarrar e imobilizar oponentes, deixando-os ***Derrubados*** ou ***Imobilizados***. Você também pode usar como uma forma de descobrir deficiências físicas em seu alvo – agarrar o mercenário pode mostrar que ele possui uma ***Perna Manca***.
- `A`{: .fate_font} **Atacar:** Vigor não é usado diretamente para causar dano – veja a perícia Lutar para isso.
- `D`{: .fate_font} **Defender:** Embora normalmente não se utilize Vigor para se defender de ataques, é possível usar a perícia para oferecer oposição ativa ao movimento de alguém, desde que o espaço seja pequeno o bastante para que seja plausível usar seu corpo como obstáculo. Você também pode colocar algo pesado no caminho e segurar o objeto para impedir alguém de passar.

**Especial:** Esta perícia concede caixas extras de estresse físico ou espaços para consequências. Regular (+1) ou Razoável (+2) libera a caixa de 3 pontos de estresse. Bom (+3) e Ótimo (+4) libera a quarta caixa. Excepcional (+5) ou mais concede, além das caixas, uma consequência suave adicional. Todas essas vantagens são apenas para danos físicos.

#### Façanhas Para Vigor

-  **Lutador:** +2 nas rolagens da perícia Vigor feitas para criar vantagem sobre um inimigo por agarrá-lo ou uma ação similar.
-  **Saco de Pancadas:** Você pode usar Vigor para se defender contra ataques da perícia Lutar feitos com os punhos e pernas ou instrumentos de contusão, porém você sempre recebe uma tensão de estresse físico em caso de empate.
-  **Resistente Como Aço:** Uma vez por sessão, ao custo de um ponto de destino, você pode reduzir a severidade de uma consequência física moderada para uma consequência suave (se o campo da consequência suave estiver livre) ou apagar uma consequência suave.

- [« Roubo](../roubo/)
- [Vontade »](../vontade/)

