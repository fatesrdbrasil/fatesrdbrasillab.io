---
title: Invocando Aspectos
layout: default
---

## Invocando Aspectos

A forma mais comum de usar aspectos em *Fate* é **invocá-los**. Se você está em uma situação em que seu personagem pode se beneficiar de um aspecto de alguma forma, então ele pode ser invocado.

Para invocar um aspecto, explique porque ele é relevante, gaste um ponto de destino e escolha um dos seguintes benefícios:

+  Receber um bônus de +2 depois de ter lançado os dados em uma rolagem de perícia.
+  Lançar novamente os dados.
+  Fornecer o bônus de +2 à jogada de outro personagem, se encontrar uma forma de fazer o seu aspecto útil a ele.
+  Adicionar +2 à qualquer fonte de oposição passiva, se encontrar uma forma de explicar como o aspecto dificulta a situação para a oposição. Você também pode usar isso para criar uma dificuldade de nível Razoável (+2), se não houver nenhuma.

Não importa quando você invoca o aspecto, mas é melhor esperar até rolar os dados para ver qual o benefício que você poderia usar. Você *pode* invocar diversos aspectos em um único lance de dados, mas você *não pode* invocar o mesmo aspecto várias vezes na mesma rolagem. Se rolar novamente não ajudou muito, você pode invocar outro aspecto (e gastar mais um ponto de destino) para uma segunda rolagem ou por +2.

O grupo deve concordar com a relevância do aspecto que você invocar; Narradores, a palavra final é de vocês. O uso de um aspecto tem que fazer sentido ou você deve encontrar uma forma criativa de incorporar o aspecto de maneira coerente.

Os detalhes de como isso será feito cabem a você. Às vezes faz tanto sentido invocar um aspecto que você pode apenas erguer um ponto de destino e falar o nome do aspecto. Talvez, porém, seja necessário descrever melhor a ação de seu personagem para que todos entendam melhor o que está realizando (é por isso que recomendamos que o grupo esteja em sintonia sobre o significado dos aspectos de cada um – fica mais fácil justificar usá-los durante o jogo).

------

### Rolar Novamente ou +2

Rolar novamente os dados é mais arriscado que simplesmente pegar o bônus de +2, mas tem um potencial grande de benefício. Nós recomendamos que você use isso apenas em jogadas como -3 ou -4, para maximizar as chances de benefício na nova rolagem.

------

> Esopo está tentando vencer uma disputa contra um rival em um clube noturno, e a perícia que ele está usando é Provocar, porque ele descreve a cena como se ambos estivessem “se desafiando a fazer coisas impossíveis”.
>
> Léo lança os dados, e consegue um péssimo resultado. “Quero invocar ***Não Entendo Absolutamente Nada***”, ele diz. Amanda lança um olhar incrédulo e pergunta “E como é que isso funciona?”
>
> Léo diz, “Bem, pensei em entender errado o que ele está dizendo, e fazer algum tipo de comentário de duplo sentido sobre algo que eu faria com a família dele, para tentar fazer todos no bar rirem dele. Eu acho que humilhações acidentais são precisamente minha área”.
>
> Amanda diz, "Certo, gostei disso".
>
> Léo gasta o ponto de destino.

Mais exemplos de aspectos estão espalhados ao longo do livro – como eles estão intimamente ligados ao funcionamento do *Fate* isso resulta em vários exemplos. Dê uma olhada nas páginas XX, XX e XX.

Se o aspecto que você invocar estiver na ficha de outro personagem, incluindo aspectos de situação que estejam ligados a ele, o ponto de destino gasto deve ser entregue ao jogador desse personagem. O outro jogador, porém, não poderá usar esse ponto de destino até que a cena termine.

### Invocações Grátis

Não é preciso pagar um ponto de destino sempre que invocar um aspecto – às vezes isso será gratuito.

Quando for bem-sucedido em criar vantagem, você ganha uma invocação de aspecto grátis. Se for bem-sucedido com estilo, você ganha duas. Algumas das outras ações também podem dar impulsos gratuitos.

O jogador também pode receber uma invocação grátis de quaisquer consequências causadas por ele em um conﬂito.

Invocações grátis como invocações normais, exceto em dois quesitos: não há gasto de pontos e é possível combiná-las com uma invocação normal para obter um bônus melhor. É possível, portanto, ganhar uma invocação grátis e gastar um ponto de destino no mesmo aspecto para receber um bônus de +4 ao invés de +2, rolar novamente duas vezes ou adicionar +4 à jogada de um aliado ou aumentar uma dificuldade passiva em +4. Você pode ainda dividir os benefícios, lançando os dados novamente e recebendo +2. Também é possível fazer mais de uma invocação grátis por vez.

Após a invocação gratuita, é possível invocar o aspecto mais vezes com o gasto de pontos de destino, se o aspecto ainda estiver disponível.

------

### O Truque das Reticências

Se você quer encontrar uma forma de incorporar um aspecto a uma rolagem, tente narrar a sua ação com reticências no final (“...”), e então termine a frase com o aspecto que você quer usar. Assim:

Maira diz, “Certo, então eu intimido os bandidos com a minha cara de má...” (lança os dados e odeia o resultado) “... e eles não se assustam no primeiro momento, mas logo percebem que eu sou a ***Famosa Ladra Robótica*** de quem com certeza já ouviram falar” (e gasta o ponto de destino).

Michel diz: “Estou tentando decifrar os códigos antes que o templo antigo desabe e...” (lança os dados e odeia o resultado) “... como ***Funciono Sob Pressão***” (gasta o ponto de destino) “..., consigo encontrar o que procuro”.

------

> Fräkeline foi bem-sucedida em um ataque e causou a consequência ***Corte Profundo***. Se no próximo turno atacar seu alvo novamente, poderá invocar esse aspecto sem custo por ter sido quem o causou e receber +2 ou rolar novamente.

É possível repassar a invocação gratuita para outro personagem, se quiser. Isso permite trabalhar em equipe com seus companheiros. É bastante útil em um combate se quiser ajudar alguém a dar um golpe especial – todos criam alguma vantagem e repassam suas invocações grátis para a mesma pessoa, que pode usar todas de uma vez para formar um bônus imenso.

------

### Para Veteranos

Em outros jogos *Fate*, invocações grátis eram chamadas de “*tagging*”. Bem, achamos esse termo um pouco forçado. Você pode continuar chamando assim, se quiser – o importante é que todos na mesa entendam as regras.

------

## Forçando Aspectos

A outra forma de usar aspectos no jogo é **forçando**. Se em determinada situação um aspecto pode deixar as coisas mais dramáticas ou complicadas para o seu personagem, alguém pode forçar esse aspecto. O aspecto pode ser do seu personagem, da cena, local, jogo ou qualquer outro disponível. Vamos começar falando sobre os aspectos para depois falar dos outros.

Para forçar um aspecto, explique por que ele é relevante e, em seguida, faça uma sugestão de complicação. Vocês podem negociar os detalhes da complicação até chegar a um consenso razoável. O alvo do aspecto forçado tem então duas opções:

-  Aceitar a complicação e receber o ponto de destino
-  Pagar um ponto de destino para que a complicação não ocorra

A complicação ocorre independentemente dos esforços feitos contra ela – uma vez que tenha aceitado e recebido o ponto de destino, você não poderá usar suas perícias ou qualquer outra coisa para aliviar a situação. Será necessário lidar com o novo rumo que a história tomará devido à essa complicação.

Se você decidir evitar que a situação ocorra, então você e o grupo descrevem como isso acontece. Às vezes é apenas uma questão de concordar que o evento nunca aconteceu ou então narrar o seu personagem realizando alguma ação preventiva. Contanto que o grupo concorde, qualquer solução que dê sentido à situação pode ser utilizada.

O Narrador tem a palavra final, como sempre – não apenas sobre o resultado final de um aspecto forçado no jogo, mas também se forçar aquele aspecto é sequer válido. Use os mesmo critérios que aplicaria às invocações – o fato de que determinada complicação surja daquele aspecto deve fazer sentido imediato ou requerer uma explicação simples.

Por último (mas muito importante): **se um jogador quer forçar o aspecto de outro personagem, custará um ponto de destino para propor a complicação**. O Narrador sempre força de graça e um jogador sempre pode forçar o seu próprio personagem de graça.

------

### Para Veteranos

Em outros jogos *Fate*, você talvez tenha visto os jogadores forçando como “invocando por efeito”. Achamos que seria mais simples chamar de “forçar”, independente de quem forçou.

------

### Formas de Forçar

Existem duas categorias do resultado de um forçamento no jogo: **eventos** e **decisões**. Aqui estão algumas ferramentas para ajudá-lo a imaginar como um forçamento deve acontecer e a prevenir bloqueios criativos.

#### Eventos

Forçar um aspecto baseado em um evento acontece com o personagem **independente de sua vontade**, quando o mundo ao seu redor responde a certo aspecto de uma forma específica e cria uma complicação. Mais ou menos assim:

-  Você possui o aspecto \_\_\_ e está na seguinte situação \_\_\_, então faz sentido que, infelizmente, \_\_\_ aconteça a você. Hoje não é o seu dia.

Aqui uns exemplos:

> Fräkeline possui ***Famosa Ladra Robótica*** e participa secretamente em competições de jogos online. Infelizmente um admirador a reconhece em um evento e começa a causar alarde, chamando a atenção de todos para ela.
>
> Esopo tem ***O Explorador Sabe Meus Segredos***, e se vê obrigado a cumprir algumas ordens do mesmo para manter os detalhes dessas histórias sórdidas longe dos curiosos.
>
> Bandu tem ***O Império Quer a Minha Cabeça***, e, por isso, faz sentido que um mercador reconheça sua face num anúncio de procura-se ou algum caçador de recompensa apareça de tempos em tempos para tentar capturá-lo.

Como verá em forçamentos baseados em decisões, o que realmente interessa é a complicação. Sem isso, você não tem um ponto de apoio – o fato de os PJs terem, continuamente, problemas e coisas dramáticas acontecendo com eles (é exatamente isso o que faz deles PJs).

Para Narradores, forçamentos à base de eventos são sua chance de fazer a festa. É esperado que o Narrador controle mundo ao redor dos PJs, então fazer esse mundo reagir de forma inesperada é parte de sua função.

Forçamentos à base de eventos são excelentes para os jogadores. Você ganha uma recompensa simplesmente por *estar lá*. Talvez seja difícil justificar um forçamento sobre si mesmo, já que requer o controle sobre um elemento do jogo que normalmente não é sua responsabilidade. Sinta-se livre para propor formas de forçar um evento, mas lembre-se que o Narrador tem a palavra final sobre o controle do mundo de jogo e talvez ele não use suas ideias se ele já estiver com algo em mente.

#### Decisões

Uma decisão é um tipo de forçamento interno do personagem. É quando um aspecto é forçado por causa de uma decisão que o personagem fez, por isso o nome. É mais ou menos assim:

-  Você tem o aspecto \_\_\_ e está na seguinte situação \_\_\_, então faz sentido que você decida fazer \_\_\_. Isso dará errado quando \_\_\_ acontecer.

Alguns exemplos:

> Esopo tem ***Não Entendo Absolutamente Nada*** e está conversando com uma dignitária em um baile real, então faz sentido que ele vá se comportar de maneira inadequada. Isso acaba mal quando ela informa que é a princesa do planeta e ofendê-la é considerado um crime.
>
> Fräkeline possui ***Se É Valioso, Poderia Ser Meu*** e, durante uma missão no Museu do Consenso Universal, faz sentido que ela queira levar uma relíquia antiga para si. O problema aparece quando ela descobre que o cofre de segurança lançou nano-robôs em seu sangue, e ela precisa pagar a dívida com os Credores Universais se quiser livrar dessa complicação.
>
> Bandu tem ***Me Ofendo por Pouco***, que representa seu comportamento de não levar desaforo pra casa e se ofender com facilidade. Assim, faz sentido que os modos arrogantes da comunidade de cientistas o irritem, deixando que ele mesmo se humilhe e seja expulso da estação de pesquisa.

O impacto dramático nesse tipo de forçamento está, portanto, na decisão que o personagem toma, na maioria das vezes – é assim que as coisas dão errado. Antes que algo acabe mal, a primeira frase pode anteceder uma rolagem de perícia ou apenas uma questão de interpretação. A complicação que a decisão cria é o que torna aquilo um ato de forçar.

A parte da decisão deve ficar bem evidente e ser algo que o jogador estava pensando em fazer. O mesmo vale para jogadores que tentem forçar aspectos de PdNs ou de outros PJs – tenha certeza de que todos já concordam no que faria sentido que aquele PdN ou personagem faça antes de sugerir forçar
um aspecto.

Essa é uma ótima forma dos jogadores obterem pontos de destino. Ao propor um ato de forçar baseado em decisão para o seu personagem, você basicamente está pedindo para que algo de errado aconteça com o que você está prestes a fazer. Você nem precisa ter uma complicação em mente – um sinal ao Narrador deve ser suficiente para colocar as coisas em movimento. O Narrador deve apoiar a iniciativa, contanto que a sugestão não seja fraca (ou seja, deve haver uma complicação bem interessante). Se a sugestão for fraca, converse com o grupo para elaborar algo mais interessante.

------

Os Narradores devem se lembrar que um jogador é responsável por tudo que seu personagem dele faz e diz. Forçamentos deste tipo podem ser sugeridos, mas se o jogador não sentir que esse é o tipo de decisão que seu personagem tomaria, ele não deve ser coagido a pagar um ponto de destino para escapar. Seria melhor negociar até encontrar algo que o jogador concorda fazer e uma complicação que deriva dessa nova decisão. Se não conseguirem concordar em algo, deixem de lado.

------

------

Quando tentar forçar baseado em uma decisão e ninguém concordar em como a decisão deve acontecer, não deve ser necessário pagar pontos de destino para evitá-la – deixe de lado também. Ir contra um ato de forçar baseado em decisão apenas significa que a parte "o que dá errado" não acontece.

------

### Forçando Retroativamente

Às vezes você notará durante o jogo que forçou um aspecto sem receber um ponto de destino por isso. Você encarnou o seu aspecto e acabou entrando em confusão por isso ou narrou coisas malucas e dramáticas acontecendo ao seu personagem graças a algum aspecto dele.

Qualquer jogador que fizer isso pode pedir e receber um ponto de destino retroativamente, como o aspecto tivesse sido forçado. Narradores têm a palavra final. Deve ser fácil identificar quando algo assim ocorrer - dê uma olhada nas explicações anteriores sobre aspectos forçados e veja se consegue resumir o que aconteceu no jogo de acordo com as orientações dadas. Se conseguir, dê o ponto de destino.

### Forçando Aspectos de Situação

Assim como qualquer outro tipo de aspecto (inclusive aspectos de jogo) você pode forçar aspectos de situação. Como aspectos de situação não têm ligação com os personagens, você frequentemente estará forçando aspectos relacionados a eventos ao invés de baseados em decisões. O personagem ou personagens afetados ganham um ponto de destino.

Alguns exemplos:

> O armazém está ***Em Chamas*** e os PJs estão presos dentro dele, portanto faz sentido que, infelizmente, o vilão que perseguiam consiga escapar.
> 
> Fräkeline procura uma peça numa fábrica de androides abandonada ***Em Escombros***, então faz sentido que, com o barulho e a demora, a guarda da cidade apareça antes que ela encontre o que deseja, o que a deixa com muita coisa a explicar.
>
> Na antiga biblioteca onde Bandu está existem ***Camadas de Poeira*** por todo lado, então faz sentido que o caçador de recompensas que o persegue fique sabendo que ele esteve ali.

- [« Criando Bons Aspectos](../criando-bons-aspectos/)
- [Usando Aspectos na Interpretação »](../usando-aspectos-na-interpretacao/)
