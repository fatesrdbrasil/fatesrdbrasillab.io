---
title: Roubo
layout: default
---

### Roubo

A perícia Roubo cobre a aptidão de seu personagem em roubar coisas e entrar em locais proibidos.

Em cenários de alta tecnologia, esta perícia também inclui proficiência no uso de equipamentos relacionados, permitindo ao personagem hackear sistemas, desarmar alarmes e coisas semelhantes.

- `O`{: .fate_font} **Superar:** Como mostrado acima, Roubo permite a você superar qualquer obstáculo relacionado a roubo ou infiltração. Contornar fechaduras e armadilhas, bater carteiras, furtar objetos, encobrir rastros e outras atividades semelhantes entram nesta categoria.
- `C`{: .fate_font} **Criar Vantagem:** Você pode avaliar um determinado local usando Roubo para determinar o quão difícil será entrar e com qual tipo de segurança você está lidando, assim como para descobrir vulnerabilidades que possa explorar. Você também pode examinar o trabalho de outros ladrões para saber como eles realizaram determinadas ações e criar ou descobrir aspectos relacionados a qualquer prova ou pista que possam ter deixado para trás.
- `A`{: .fate_font} **Ataque:** Esta perícia não é utilizada em ataques.
- `D`{: .fate_font} **Defender:** O mesmo se aplica aqui. Na verdade ela não é uma perícia de conﬂito, então não há muitas formas de usá-la neste sentido.

#### Façanhas Para Roubo

-  **Sempre Há Uma Saída:** +2 para Roubo quando você tenta criar uma vantagem ao tentar escapar de algum lugar.
-  **Especialista em Segurança:** Não é preciso estar presente para oferecer oposição ativa a alguém que está tentando superar artifícios de segurança criados ou montados por você (normalmente, um personagem rola contra uma oposição passiva nessas situações).
-  **Gíria da Ruas:** Você pode usar Roubo no lugar de Contatos sempre que lidar especificamente com ladrões e marginais.

- [« Recursos](../recursos/)
- [Vigor »](../vigor/)
