I".<h1 id="crafts">Crafts</h1>

<p>Crafts is the skill of working with machinery, for good or ill.</p>

<p>The default skill is called Crafts because it’s what we use in the examples,
but this skill might vary a great deal depending on the setting and what kind
of technology is available. In a modern or sci-fi setting, this might be
Engineering or Mechanics instead.</p>

<p><span class="fate_font">O</span><strong>Overcome</strong>: Crafts allows you to build, break, or
fix machinery, presuming you have the time and tools you need. Often, actions
with Crafts happen as one component of a more complex situation, making it a
popular skill for challenges. For example, if you’re just fixing a broken
door, neither success nor failure is interesting; you should just succeed and
move on. Now, if you’re trying to get your car to start while a pack of
werewolves is hunting you…</p>

<p><span class="fate_font">C</span><strong>Create an Advantage</strong>: You can use Crafts to
create aspects representing features of a piece of machinery, pointing out
useful features or strengths you can use to your advantage ([span:aspect
]Armor-Plated&lt;/span&gt;, <span class="aspect">Rugged Construction</span>) or a
vulnerability for you to exploit (<span class="aspect">Flaw in the Cross-Beam</span>,_
_<span class="aspect">Hasty Work</span>).</p>

<p>Creating Crafts advantages can also take the form of quick and dirty sabotage
or jury-rigging on mechanical objects in the scene. For example, you might
create a Makeshift Pulley to help you get to the platform above you, or throw
something into the ballista that’s firing on you to give it a Jammed Pivoting
Joint and make it harder to hit you.</p>

<p><span class="fate_font">A</span><strong>Attack</strong>: You probably won’t use Crafts to attack in
a conflict, unless the conflict is specifically about using machinery, like
with siege weaponry. GMs and players, talk over the likelihood of this
happening in your game if you have someone who is really interested in taking
this skill. Usually, weapons you craft are likely to be used with other skills
to attack—a guy who makes a sword still needs Fight to wield it well!</p>

<p><span class="fate_font">D</span><strong>Defend</strong>: As with attacking, Crafts doesn’t defend,
unless you’re somehow using it as the skill to control a piece of machinery
that you block with.</p>

<h3 id="so-many-crafts">So Many Crafts</h3>

<p>If working with different types of tech is important to your game, you might
have several of these skills in your list. So, a futuristic game might have
Engineering, Cybernetics, and Biotechnology, all basically with the same moves
available for their respective type of tech. In such a game, an individual
character can’t be proficient at all of them without expending a lot of skill
ranks.</p>

<p>If you’re going to do this, make sure that you have a reason for it besides
pedantry—if the only thing that splitting the skills gets you is the same
effects with different names, you should keep it more generalized and use
stunts to handle the specialties.</p>

<h2 id="crafts-stunts">Crafts Stunts</h2>

<ul>
  <li><strong>Always Making Useful Things</strong>. You don’t ever have to spend a fate point to declare that you have the proper tools for a particular job using Crafts, even in extreme situations (like being imprisoned and separated from all your stuff). This source of opposition is just off the table.</li>
  <li><strong>Better than New!</strong> Whenever you succeed with style on an overcome action to repair a piece of machinery, you can immediately give it a new situation aspect (with a free invoke) reflecting the improvements you’ve made, instead of just a boost.</li>
  <li><strong>Surgical Strikes</strong>. When using Crafts in a conflict involving machinery, you can filter out unwanted targets from whole-zone attacks without having to divide up your shifts (<a href="../../fate-core/challenges">normally, you’d need to divide your roll between your targets</a>).</li>
</ul>

<p>If building constructs and creating items is a big part of your game, check
out <a href="../../fate-core/extras">Extras</a> for a discussion of what might
result from the use of Crafts.</p>

<ul>
  <li><a href="/fate-srd/fate-core/contacts">« Contacts</a></li>
  <li><a href="/fate-srd/fate-core/deceive">Deceive »</a></li>
</ul>

:ET