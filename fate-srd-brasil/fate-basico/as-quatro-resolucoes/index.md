---
title: As Quatro Resoluções
layout: default
---

## As Quatro Resoluções

**Ao rolar os dados você pode falhar, empatar, obter sucesso ou obter um sucesso com estilo.**

De modo geral, toda rolagem que você fizer em um jogo de *Fate* terá uma dessas resoluções. Os detalhes podem variar um pouco dependendo de qual ação você fizer, mas todas as ações em jogo se encaixam neste modelo.

### Falha

**Se a sua rolagem for inferior à de sua oposição, você falhou.**

Isso pode significar várias coisas: você não consegue o que queria, você consegue a um grande custo ou sofre alguma consequência mecânica negativa. Às vezes pode significar mais do que uma dessas opções. Cabe ao Narrador determinar o custo apropriado (veja o [quadro abaixo](#custo-severo-vs-custo-suave)).

### Empate

**Se sua rolagem for igual à de seu oponente, você empata.**

Isso significa que você consegue o que deseja, mas com certo custo ou consegue uma versão inferior do que queria.

### Sucesso

**Se a sua rolagem for bem-sucedida com 1 ou 2 tensões, você teve sucesso.**

Isso significa que você consegue o que queria sem custo algum.

### Sucesso com Estilo

**Se a sua rolagem exceder a de sua oposição por 3 ou mais tensões, você é bem-sucedido com estilo.**

Isso significa que você consegue o que quer, e também um benefício adicional.

------

### Narradores

### Custo Severo Vs. Custo Suave

Ao pensar em custo, pense tanto na história quanto na mecânica do jogo para ajudar a determinar o custo mais apropriado.

Um custo severo pode tornar uma situação pior de alguma forma, criando um novo problema ou piorando um existente. Crie outra fonte de oposição na cena atual ou na próxima (como um novo PdN ou um novo obstáculo para contornar), peça ao jogador que anote uma consequência do menor nível disponível, ou dê uma vantagem a um oponente com uma invocação grátis.

Um custo suave pode adicionar detalhes problemáticos ou ruins à história para os PJs, mas não necessariamente perigosos. Você também pode perguntar ao jogador sobre a possibilidade de receber estresse ou fornecer um impulso a algum oponente.

Não há problema se um custo suave for apenas um detalhe narrativo, mostrando como o PJ conseguiu por muito pouco. Damos mais conselhos sobre como lidar com custos em [*Conduzindo o Jogo*](../durante-o-jogo/#sucesso-a-um-custo).

------

- [« Ações e Resoluções](../acoes-e-resolucoes/)
- [As Quatro Ações »](../as-quatro-acoes/)
