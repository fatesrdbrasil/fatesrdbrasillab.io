---
title: Condições
layout: default
---

## Condições


Consequências são uma ótima forma de lidar com ferimentos, cicatrizes emocionais e outras condições persistentes na história de seu jogo. Elas não são para todos, no entanto. Algumas pessoas têm dificuldade em criar boas consequências no improviso, enquanto outros querem algo mais definido e concreto. Outros só querem alguma coisa diferente. 

__Condições__ são como consequências, exceto pelo fato de que são predefinidas, desta forma:

Há três tipos de condições: momentânea, estável e duradoura. Uma __condição momentânea__ desaparece quando você consegue uma chance para respirar e se acalmar. No exemplo acima, Irritado e Amedrontado são condições momentâneas. Uma __condição estável__ permanece até que um evento específico ocorra. Se estiver Exausto, permanecerá assim até que consiga dormir. Se
estiver Faminto, permanecerá assim até que coma algo. Ferido e Lesionado são __condições duradouras__. Elas permanecerão por uma sessão inteira e exigem que alguém realize uma jogada de superar contra uma oposição passiva de ao menos Ótimo (+4) antes de começar a se recuperar dela. As __condições duradouras__ possuem duas caixas ao seu lado e você marca ambas quando receber a condição. Quando iniciar a recuperação, apague uma das caixas. Apague a segunda (e se recupere totalmente) após mais uma sessão completa de jogo. Você só pode receber uma condição duradoura se ambas as caixas estiverem
livres.

Você sofre uma determinada condição quando o Narrador disser, normalmente como resultado de uma situação narrativa, mas também pode usá-las para absorver estresse. Ao receber estresse, você pode reduzir o estresse em 1 se marcar a condição momentânea. É possível marcar quantas condições desejar para um único golpe.

Uma vez que esteja sofrendo uma condição, ela se torna um aspecto na planilha de seu personagem, como qualquer outro. Desta forma, as condições são bem semelhantes às consequências, pois você pode invocá-las e elas podem ser invocadas ou forçadas contra você. Assim como uma consequência, quando você recebe uma condição, alguém pode invocá-la gratuitamente
uma vez contra seu personagem.


| Momentâneas                                              | Estáveis                                                 | Duradouras                                                                                                        |
|----------------------------------------------------------|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| Irritado                                                 | Exausto                                                  | Ferido                                                                                                            |
| Amedrontado                                              | Faminto                                                  | Lesionado                                                                                                         |
| Passam quando tiver uma chance de respirar e acalmar-se. | Ficam marcadas até um evento específico (dormir, comer). | É necessária uma ação de recuperação para remover. A segunda caixa só pode ser marcada se a primeira não estiver. |

Se você pretende usar condições em seu jogo, algo que pode fazer para reforçar o tema ou o estilo de seu jogo é criar suas próprias condições. Você não precisa seguir o mesmo padrão de duas caixas momentâneas, duas estáveis e duas duradouras, como apresentado aqui, mas você deve manter o mesmo número total de tensões que pode ser absorvido, 14. Tenha certeza também de seguir as regras para diferentes tipos de condições: condições momentâneas desaparecem rápido, condições estáveis exigem um motivo narrativo e condições duradouras exigem tratamento. As condições acima são genéricas o suficiente para abranger uma grande variedade de cenários, mas modelar as condições para a sua ambientação pode ser uma forma útil de fazer os personagens se sentirem parte daquele mundo. 

Como exemplo, aqui estão algumas condições do cenário Fight Fire de Jason Morningstar (encontrado no Fate Worlds: Worlds on Fire):

| Momentâneas                                              | Estáveis                                                 | Duradouras                                                                                                        |
|----------------------------------------------------------|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| Sem Fôlego                                               | Desidratado                                              | Ferido                                                                                                            |
| Em Pânico                                                |                                                          | Lesionado                                                                                                         |
| Contundido                                               |                                                          |                                                                                                                   |
| Desorientado                                             |                                                          |                                                                                                                   |
| Passam quando tiver uma chance de respirar e acalmar-se. | Ficam marcadas até um evento específico (dormir, comer). | É necessária uma ação de recuperação para remover. A segunda caixa só pode ser marcada se a primeira não estiver. |


------

- [« Aspectos de Equipamento](../aspectos-de-equipamento/)
- [Perícias »](../pericias/)
