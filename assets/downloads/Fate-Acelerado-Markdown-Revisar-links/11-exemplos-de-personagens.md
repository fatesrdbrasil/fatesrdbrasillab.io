# Exemplos de Personagens

Aqui estão quatro personagens que podem ser usados como estão ou como inspiração para criar seus próprios. No exemplo eles têm apenas uma façanha, mas você pode criar mais duas para cada um sem diminuir a Recarga.

## Dandarion, O Mais Épico

Dandarion é um bárbaro destemido e boca suja que vaga o mundo em busca de entretenimento barato e fugaz. Criado por ursos polares de dentes-de-sabre, na região das Geleiras Flamejantes, em um ponto cardeal de um Reino sem norte, Dandarion tornou-se uma lenda em todo continente por seus feitos heroicos e pouco usuais. Apesar de ter espírito selvagem, o bárbaro possui uma incômoda inclinação por ajudar os outros e punir badernistas que passam dos limites.

------

### Dandarion

Conceito: ***O Bárbaro Mais Épico das Geleiras Flamejantes***
Dificuldade: ***Só Entendo As Coisas Literalmente***
**Outros Aspectos:** ***Conquistador Em Todos os Sentidos, Ajudo os Outros Porque Sim, Pela Força É Sempre Mais Fácil***

**Abordagens**

**Ágil:** Razoável (+2)
**Cuidadoso:** Regular (+1)
**Esperto:** Medíocre (+0)
**Estiloso:** Razoável (+2)
**Poderoso:** Bom (+3)
**Sorrateiro:** Regular (+1)

**Façanhas**

**Solucionador de Problemas:** Como eu sou Solucionador de Problemas, recebo +2 para Superar ou Criar Vantagem contra oposições passivas que precisem de cuidado e planejamento para serem resolvidas.

**Estresse \[ \]\[ \]\[ \]**

**Consequências**

Suave (2):
Moderada (4):
Severa (6):

**Recarga: 3**

------

## Lola Calavera

Após ter perdido sua vida e sua família em uma festa (muito) mal sucedida promovida pelo Rei de Coronilla, Lola retornou dos mortos como um fantasma para assombrar a família real e pôr um fim na tirania da metrópole sobre seu povoado. Como ex-dançarina de flamenco, ela mantém seus trajes característicos e seu comportamento sedutor, utilizando-se de beleza, inteligência e poderes sobrenaturais para atormentar os que atravessam seu caminho.

------

### Lola

**Conceito:** ***Animada Dançarina Fantasma do Castelo Real***
**Dificuldade:** ***Perseguida pelos Padres de Coronilla***
**Outros Aspectos:** ***Eu Ponho Medo nos Quadris dos Reis, Pepe Me Ajuda a Balançar o Esqueleto, Todo Dia É Dia de Los Muertos***

**Abordagens**

**Ágil:** Razoável (+2)
**Cuidadoso:** Regular (+1)
**Esperto:** Regular (+1)
**Estiloso:** Bom (+3)
**Poderoso:** Medíocre (+0)
**Sorrateiro:** Razoável (+2)

**Façanhas**

**Aparição Espectral:** Como eu sou Aparição Espectral, uma vez por cena posso desaparecer e ressurgir instantaneamente em outra zona próxima, ignorando bloqueios no caminho.

**Estresse \[ \]\[ \]\[ \]**

**Consequências**

Suave (2):
Moderada (4):
Severa (6):

**Recarga: 3**

------

## Francis Maestlin, O Caçador

Francis Maestlin é um dos membros mais ativos do Esquadrão Argos, a equipe de caça e escolta da expedição de colonização do planeta Tauri, que **orbita** a estrela Mérope. Ele é calmo e paciente, com um preparo físico invejável e um profundo respeito pela vida selvagem. Apesar de seu fascínio por todas as formas de vida e seu amor por filhotes, a mira precisa e a velocidade de reação de Francis lhe renderam o título de “O Caçador”.

------

### Francis Maestlin

**Conceito:** ***Soldado de Caça e Escolta do Esquadrão Argos***
**Dificuldade:** ***Minha Fama e Eu Somos Completamente Diferentes***
**Outros Aspectos: *****Estude Primeiro Atire Depois*****,***** Minha Família São Spinoza e A Força Expedicionária*****,***** Respeito à Vida***

**Abordagens**

**Ágil:** Bom (+3)
**Cuidadoso:** Razoável (+2)
**Esperto:** Regular (+1)
**Estiloso:** Medíocre (+0)
**Poderoso:** Razoável (+2)
**Sorrateiro:** Regular (+1)

**Façanhas**

**Precisão Impossível:** Quando uso Precisão Impossível, ganho +2 em ataques Cuidadosos contra alvos em movimento.

**Estresse \[ \]\[ \]\[ \]**

**Consequências**

Suave (2):
Moderada (4):
Severa (6):

**Recarga: 3**

------

## Exia

Única pilota do robô Apoteose, Exia atua como vanguarda na defesa da Terra contra a invasão dos Gargantuas que ameaçam a humanidade. Impulsiva, honrada e extremamente teimosa, Exia entrou para a equipe de defesa do planeta quando ainda era uma criança, acompanhando de perto a evolução da tecnologia terráquea e o despontar de pilotos lendários, que hoje lhe servem de inspiração.

------

### Exia

**Conceito:** ***Orgulhosa Pilota de Zelotes na Defesa Global***
**Dificuldade:** ***Lidar Com Pessoas Não É Pra Mim***
**Outros Aspectos:** ***Combater Monstros Gigantes É O Que Eu Faço Melhor, Ordens Superiores São Apenas Guias, Miura Tem Minha Confiança***

**Abordagens**

**Ágil**: Razoável (+2)
**Cuidadoso:** Medíocre (+0)
**Esperto:** Regular (+1)
**Estiloso:** Razoável (+2)
**Poderoso:** Regular (+1)
**Sorrateiro:** Bom (+3)

**Façanhas**

**Contato com Zelote Apoteose:** Como tenho Contato com Zelote Apoteose, uma vez por sessão posso convocá-lo para combater ameaças gigantes.

**Estresse \[ \]\[ \]\[ \]**

**Consequências**

Suave (2):
Moderada (4):
Severa (6):

**Recarga: 3**

------