---
title: Equipe
layout: default
---

## Equipe

 
E se você quiser jogar com uma equipe de personagens recrutados para invadir as praias da Normandia no Dia D? Seria possível usar o Fate para jogar com uma equipe de Fuzileiros Espaciais? 

Sim, senhor. É possível.

--- 

Estas regras, assim como tudo no Fate , são flexíveis o suficiente para funcionar em vários contextos, abrangendo unidades militares modernas, tropas da Segunda Guerra Mundial, exércitos em um cenário de fantasia e robôs futuristas.

---

### Criando Equipes

 
Primeiramente, crie os personagens normalmente (veja o Fate Sistema Básico p. 25). Ao elaborar seus personagens, nomeie a equipe e diga que papel ela possui na força a que pertencem. Seriam um bando de ex-criminosos tornados soldados ou uma força profissional altamente treinada e equipada? 

Independente da sua decisão, transforme a descrição em dois aspectos, um conceito do equipe e uma dificuldade do equipe, que sempre será o calo no pé do seu time. Qualquer jogador que pertencer à unidade pode invocar esses aspectos, bem como forçá-los.
 
---
 
Lara, Antônio e Michele decidiram que serão uma equipe improvisada, formada por um grupo de sobreviventes lutando contra os ataques de forças alienígenas que quase destruíram Nova Orleans. Após criar os personagens individualmente, eles nomeiam a equipe como Defensores da Ala Nove, escolhendo o conceito _**Cães de Guerra**_ para representar sua agressividade e a dificuldade _**Mais Problemas Que Soluções**_ para mostrar que os alienígenas estão na frente.
 
---
 
### Perícias de Equipe

Diferente das perícias individuais, perícias de equipe podem ser usadas para afetar todo o campo de guerra. Por exemplo, seu soldado pode liderar uma investida contra as linhas inimigas ou chamar reforços para enfraque cer uma defesa implacável.

Após criar os aspectos da unidade, forneça a ela:

- Uma perícia de equipe Razoável (+2)
- Duas perícias de equipe Regulares (+1)
- Uma Façanha de Equipe

---

Se deseja enfatizar a equipe em lugar dos personagens individuais, reduza as façanhas e recarga iniciais dos personagens de três para dois e limite a maior perícia a Bom (+3) ao invés de Ótimo (+4).

---

#### Operações

A perícia Operações mede a habilidade da unidade em trabalhar em conjunto no campo de batalha, eliminando unidades inimigas e firmando-se em posições estratégicas.

**Superar:** Operações permite superar obstáculos em equipe, como quando você aplica fogo de cobertura para alcançar um soldado ferido ou trabalha em conjunto para escalar uma muralha.

**Criar Vantagem:** Ao criar vantagem com Operações, sua equipe está plantando armadilhas ( **_Emboscada!_** ) ou investindo diretamente contra barricadas inimigas ( **_Capangas em Pânico_** ).

**Atacar:** Operações permite que sua equipe lance ataques coordenados contra alvos e deve ser rolado em lugar das perícias individuais Lutar e Atirar enquanto o grupo continuar agindo como equipe.

**Defender:** Role Operações para defender quando a equipe tentar se retirar de uma zona de combate ou em outras tentativas de evitar ataques inimigos em grupo.

##### Exemplos De Façanhas Para Operações

**Difícil de Cercar:** Ganhe +2 em qualquer rolagem de superar para recuar de uma zona de combate.

**Ataque Relâmpago:** Sua equipe é rápida, leve e mortal. Receba +2 nas rolagens de Operações quando seu ataque procura pegar o inimigo desprevenido.

 
#### Equipamento
 
A perícia Equipamento representa os recursos que sua equipe tem disponíveis para alcançar seus objetivos.

**Superar:** Assim como a perícia Recursos, a perícia Equipamento pode ser usada para superar uma situação que exija algumas ferramentas adicionais. O grupo pode chamar caminhões para transportá-los através de um terreno acidentado ou mesmo convocar um bombardeio.
 
**Criar Vantagem:** Sua equipe pode usar Equipamento para conseguir uma arma muito poderosa em uma missão específica ( _**Lança-Chamas!**_) ou tentar conseguir recursos essenciais para a navegação (_**Mapas Topográficos**_).
 
 
**Atacar/Defender:** A perícia Equipamento não é usada em ataques ou defesas.
 
 
##### Exemplos De Façanhas Para Equipamentos

**Equipamento de Alta Tecnologia:** Você pode usar Equipamento em lugar de Operações em uma situação onde possuir uma tecnologia superior pode fazer a diferença.

**Estoque Completo:** Você recebe +2 em todas as rolagens de Equipamento realizadas para criar vantagem quando acessar seus suprimentos preexistentes.
 
#### Reconhecimento
 
**Superar:** Reconhecimento normalmente não é usado para superar obstáculos, mas pode ser usado de forma semelhante a Notar, para dar à equipe a chance de evitar emboscadas e armadilhas.

**Criar Vantagem:** Sua equipe pode usar Reconhecimento durante a batalha para adentrar a névoa da guerra, conseguindo informações sobre lugares além de sua posição atual.

**Atacar/Defender:** Reconhecimento não é usada para defesas ou ataques.
 
##### Exemplos De Façanhas Para Reconhecimento

**Decifradores de Códigos:** Enquanto monitora comunicações inimigas, você pode rolar Reconhecimento ao criar vantagem para descobrir ou criar um aspecto adicional (no entanto, isso não lhe concede uma invocação grátis extra).

**Contraprogramação:** você usa Reconhecimento ao invés de Operações para preparar uma armadilha que utilize o sistema de comunicação do inimigo contra eles mesmos.

### Rolando Perícias de Equipe

Para rolar uma perícia de equipe (ou uma façanha de equipe), um jogador deve abrir mão de sua ação pessoal para reunir o grupo. A natureza da ação que reunirá todos depende da situação — pelotões militares em geral seguem ordens —, mas geral o jogador precisará da ajuda da maioria dos outros jogadores.
 
Se ele for bem-sucedido em coordenar a equipe, a dificuldade da tarefa cai em 1 para cada membro que sacrificar sua próxima ação para esse fim, conforme o grupo volta sua atenção para a realização da tarefa. Além de reduzir a dificuldade, o sucesso ou falha da rolagem afeta toda a unidade, já que as perícias da equipe podem remodelar o campo de batalha e ganhar o dia — ou fazer com que o grupo sofra junto. Dessa forma, estresse infligido contra equipe como um todo é infligido em cada membro igualmente.

---

Ao invés de realizar uma rolagem para tentar romper uma barreira alienígena sozinha, Lara decide que os Defensores da Ala Nove devem derrubá-la juntos. A dificuldade para completar essa tarefa cai de Fantástica (+6) para Ótima (+4). Se bem sucedida, toda a unidade se beneficiará do rompimento da barreira inimiga sem precisar rolar uma segunda vez. Se ela falhar, toda a unidade receberá estresse do contra-ataque alienígena.

---

### Combate Entre Equipes

Para realizar um ótimo combate entre equipes, utilize rolagens de Operações para se mover diretamente para o centro da ação. Ao invés de começar no início da luta — quando os conflitos são entediantes —, permita que a equipe elabore um plano de ataque e role Operações como uma ação de superar com uma dificuldade apropriada ao alvo e veja no que dá. Se forem bem-sucedidos, os jogadores devem narrar uma resolução boa para cada tensão acima do número alvo. Se falharem, o Narrador descreverá uma resolução negativa para cada tensão abaixo do número alvo. Seja como for, pule direto para a ação e repita isso quando as coisas começarem a esfriar no meio da luta.

---

Após romper a barreira alienígena, Lara está certa de que seu personagem pode chegar perto o suficiente da rainha alienígena para matá-la antes que o restante dos alienígenas se reagrupe. Ela reúne o batalhão para investir contra as linhas inimigas, rolando sua perícia Operações Razoável (+2) contra uma dificuldade Ótima (+4) determinada pelo Narrador. Ela consegue +2 em sua rolagem, juntamente de seu Operações (+2), e invoca o aspecto Cães de Guerra para receber outros +2; um total Fantástico (+6). Para sua primeira tensão, ela narra que foram bem-sucedidos em invadir as linhas inimigas e, para o segundo, afirma que a sua personagem chegou perto o suficiente para matar a rainha.

---

------

- [« Monstros](../monstros/)
- [Isto Significa Guerra: Combate Em Massa »](../isto-significa-guerra-combate-em-massa/)
