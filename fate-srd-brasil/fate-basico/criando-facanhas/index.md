---
title: Criando Façanhas
layout: default
---

## Criando Façanhas

Em *Fate*, permitimos que os jogadores criem as façanhas durante a criação do personagem ou deixem-nas em aberto para fazer isso durante o jogo. Há alguns exemplos de façanhas listados abaixo, junto às perícias. Não é uma lista definitiva; na verdade, elas estão lá para mostrar a você como criar as suas próprias façanhas (embora você possa simplesmente escolher alguma já presente no livro, se assim desejar).

Também temos uma lista das coisas que uma façanha pode ser capaz de *realizar*, para ajudá-lo quando for criá-las em jogo. Em caso de dúvida, dê uma olhada na lista de façanhas para buscar inspiração, assim como nas façanhas dos personagens de exemplo.

------

Narradores que queiram reforçar um conjunto em particular de perícias como sendo importantes ou únicas para o jogo podem optar por criar uma lista de façanhas, facilitando a busca por referências durante a criação do personagem. Isso normalmente é feito como parte da criação dos extras; veja a sessão *[Extras](../extras/)* para mais detalhes.

------

### Adicionando Uma Nova Ação a Uma Perícia

A opção mais básica de uma façanha é permitir a uma perícia realizar algo que ela normalmente não pode fazer. Ela adiciona uma nova ação para a perícia para uso em situações específicas, àqueles que possuírem a tal façanha. A nova ação pode estar disponível para outra perícia (permitindo que uma perícia seja trocada por outra em certas circunstâncias) ou alguma que não está disponível para nenhuma delas.

Aqui estão algumas façanhas com esse efeito:

-  **Golpe Traiçoeiro.** Você pode usar Furtividade para realizar ataques físicos, desde que o alvo não tenha notado sua presença.
- **Sede De Vitória.** Você pode usar Provocar para entrar em conﬂitos nos quais normalmente precisaria usar Vigor sempre que sua capacidade de desconcentrar o seu oponente com sua presença seja viável.
-  **Você Nunca Está a Salvo.** Você pode usar Roubo para realizar ataques mentais e criar vantagem sobre um alvo, intimidando e fazendo-o sentir-se inseguro.

------

Só porque você possui uma façanha não significa que deve usá-la sempre que for relevante. Usar uma façanha sempre será uma escolha e você pode não usar se não achar apropriado ou simplesmente não quiser.

Por exemplo, você poderia ter uma façanha que lhe permitisse entrar em uma luta usando Atletismo no lugar de Lutar quando está lidando com arcos e outras armas de projéteis. Quando for atacar à distância você pode escolher usar Lutar ou Atletismo. Fica a seu critério.

------

### Adicionando Bônus a Uma Ação

Outra forma de usar as façanhas é agregando um bônus automático a uma perícia em uma situação específica, deixando o personagem se especializar em algo. A circunstância deve mais limitada do que o permitido normalmente pela ação e deve se aplicar a apenas uma ou duas ações. 

O bônus normal é +2. No entanto, se desejar, você também pode expressar esse bônus como duas tensões a mais de efeito após ser bem-sucedido na rolagem, se isso fizer mais sentido. Lembre-se, grandes alterações em sua rolagem permitem a você ser mais eficaz.

Você também pode usar isso para estabelecer qualquer efeito como benefício adicional por ser bem-sucedido em uma rolagem. Ou seja, isso pode ser uma oposição passiva Razoável (+2) para um oponente, o equivalente a -2 em dano, uma consequência suave ou uma vantagem que precisa de oposição Razoável (+2) para remover.

Aqui, alguns exemplos sobre como adicionar bônus a uma ação:

-  **Especialista Arcano.** Ganhe um bônus de +2 na ação de criar vantagem quando usar Conhecimentos, quando estiver em uma situação sobrenatural ou oculta.
-  **Chumbo Grosso.** Você realmente gosta de descarregar balas. Em qualquer momento que usar uma arma automática e for bem-sucedido em um ataque usando Atirar, você cria automaticamente uma oposição de +2 contra movimentos na zona atual até o próximo turno, por causa das balas voado no ar (normalmente você precisa de uma ação separada para criar esse tipo de oposição, mas não quando usa uma façanha).
-  **Filho da Corte.** Ganha um bônus e +2 no uso da ação criar vantagem usando a perícia Comunicação quanto realizar alguma função aristocrata, como em um baile real.
- **Mestre em Combate.** +2 ao Lutar para Criar Vantagens contra um oponente, contanto que ele possua um estilo de luta ou fraqueza que possa ser explorado.

### Criar Uma Exceção à Regra

Por fim, uma façanha pode permitir a uma perícia, em circunstâncias restritas, uma exceção a qualquer outra regra do jogo que não se encaixe perfeitamente na categoria de determinada ação. O capítulo [*Desafios, Disputas e Conﬂitos*](../desafios-disputas-e-conflitos/) está repleto de regrinhas que cobrem certas circunstâncias nas quais uma perícia pode ser usada e o que acontece. As façanhas podem quebrar isso, permitindo ao seu personagem expandir as possibilidades.

O único limite para esse tipo de façanha é que ela nunca pode alterar as regras básicas dos aspectos em termos de invocar, forçar e na economia de pontos de destino. Isso permanece inalterado.

Aqui estão alguns exemplos de façanhas que criam exceções às regras:

-  **Ritualista** Use Conhecimentos em lugar de outra perícia durante um desafio, permitindo a você usá-la duas vezes no mesmo desafio.
-  **Atar.** Quando você usa Ofícios para criar uma vantagem ***Atado*** (ou similar) sobre alguém, você sempre pode criar uma oposição ativa contra quaisquer rolagens de superar para escapar da imobilização que você criou (também usando Ofícios), mesmo se não estiver por lá. Normalmente, se você não está lá, o personagem escapa com uma rolagem contra uma oposição passiva, o que torna escapar muito mais fácil.
-  **Contra-Ataque.** Se você for bem-sucedido com estilo em uma rolagem de Lutar para se defender, pode optar por inﬂigir 2 caixas de estresse ao invés de receber um impulso.

### Balanceando a Utilidade de Façanhas

Se você olhar a maioria das façanhas de exemplo, vai perceber que as situações em que pode usá-las são bem limitadas quando comparadas com as perícias que elas afetam. Esse é o ponto certo que você procura para suas próprias façanhas - limitadas o suficiente para que sejam especiais quando usadas, mas não tanto que não consigam aparecer em jogo.

Se a façanha *toma lugar* de todas ações básicas da perícia, então não está limitada o suficiente. Você não quer que uma façanha substitua a perícia que ela modifica.

As duas principais formas de manter uma façanha limitada é fazer com que ela cubra apenas uma ação específica ou um par de ações (apenas ao criar vantagem ou apenas em rolagens de ataque ou defesa) ou por limitar a situação em que você pode usá-la (apenas entre os nobres, apenas quando lida com o sobrenatural, etc.).

Para ter os melhores resultados, use ambos - restrinja a façanha a uma ação específica, que só pode ser usada dentro de uma situação específica durante o jogo. Se você achar que a situação é muito limitada, volte um pouco e pense em como a perícia poderia ser usada em jogo. Se a façanha for útil para um desses usos, você provavelmente está no caminho certo. Se não, pode ser necessário ajustar um pouco a façanha para garantir que ela apareça.

Também é possível restringir uma façanha por fazê-la útil apenas em um determinado período do jogo, como uma vez por conﬂito, uma vez por cena ou uma vez por sessão.

------

Jogadores que criarem façanhas que forneçam um bônus a uma ação devem pensar em situações que não ocorrem com tanta frequência em jogo. Por exemplo, no caso do Especialista Arcano acima seria impróprio se em seu jogo não aparecerem muitas situações sobrenaturais, e Filho da Corte seria inútil se sua campanha não há muitos detalhes sobre a nobreza. Se você acha que não vai usar uma façanha ao menos duas vezes por sessão, procure alterá-la.

Narradores são responsáveis por ajudar os jogadores a ver quando suas façanhas podem ser úteis – veja as condições que eles criaram como coisas que eles desejam ver em suas sessões.

------

> Léo está pensando em uma façanha para Esopo chamada “**Minha Arma Detona**”. Ele quer adicionar duas tensões quando for bem-sucedido em um ataque usando Atirar, sempre que usar sua “incrível arma lançadora de cascavéis”, como ele diz.
>
> Amanda pensa um pouco. Ele preenche todos os requisitos, mas há um problema – nem Amanda nem Léo conseguem imaginar muitas situações onde Esopo não vá querer utilizar sua arma. Basicamente, ele poderá usar essa façanha sempre que usar a perícia Atirar. Ela decide que a façanha fornece muita vantagem e pede para que ele modifique.
>
> Léo pensa um pouco e diz “Então, que tal se ela for melhor quando uso ela contra outros seres do mar?”
>
> Amanda pergunta, “Mas vocês vão encontrar tantos camarões e tubarões assim? Acho que o ponto principal fosse vocês encontrando raças bem diferentes e visitando lugares interessantes.”
>
> Léo concorda que isso não apareceria com tanta frequência e pensa mais um pouco.
>
> Então surge uma ideia. “Que tal isto – e se, quando alguém usa sua caixa de estresse de 2 pontos para absorver um dos meus ataques de Atirar, eu poder fazer com que usem sua consequência suave no lugar?”
>
> Amanda gosta, porque pode aparecer em vários conﬂitos que Esopo participar, mas é algo que não fornece vantagem demasiada. Ela pede para que isso se limite e um uso por conﬂito, para finalizar. Na ficha de Esopo, Léo escreve:
>
> -  **Minha Arma Detona:** Uma vez por conﬂito, você pode forçar um oponente a receber uma consequência suave, em lugar de 2 caixas de estresse em um ataque bem-sucedido usando Atirar com sua arma lançadora de cascavéis.

------

### Façanhas Movidas a Pontos de Destino

Outra forma de restringir o uso de façanhas é o seu custo em pontos de destino. Isso é uma boa opção se a façanha em si for bastante poderosa ou se você não encontrar uma forma de usá-la menos em jogo.

Nosso conselho para determinar se a façanha é muito poderosa é checar se ela vai além dos limites que sugerimos acima (como adicionar uma nova ação a uma perícia e um bônus), ou se afeta significativamente o conﬂito. Especificamente, quase qualquer façanha que permita causar estresse adicional em um conﬂito deve custar um ponto de destino para ser usada.

------

### Façanhas Interligadas

Se você quer entrar em detalhes sobre um tipo de talento ou treinamento em particular, você pode criar façanhas interligadas: um grupo de façanhas relacionadas entre si, ligadas umas às outras de alguma forma.

Isso permite que você crie coisas como estilos de luta, clãs ou escolas específicas em seu mundo de jogo, e representa os benefícios de pertencer a um deles. Isso também o ajuda a especificar que tipos de competências especializadas estão disponíveis, se você quiser que seu jogo tenha "classes de personagens" distintas - pode haver um grupo de façanhas interligadas para o "Ás dos Céus", bem como para o "Ladrão Sorrateiro".

Criar um grupo de façanhas interligadas é fácil. Monte uma façanha original que serve como pré-requisito para todas as outras do grupo, permitindo que você pegue essas outras façanhas melhores em seguida. Depois, você precisa criar uma porção de façanhas que sejam interligadas a primeira de alguma forma, melhorando os efeitos iniciais ou ampliando para outros tipos de efeitos.

### Agrupando os Efeitos

Talvez a maneira mais simples de lidar com uma façanha relacionada seja fazer a façanha original mais eficiente na mesma situação:

-  Se a façanha adicionar uma ação, limite-a e dê à nova ação um bônus. Siga as mesmas regras para adicionar bônus – as circunstâncias em que ele se aplica devem ser mais restritas do que na ação base.
-  Se a façanha acrescer um bônus à ação, inclua um bônus adicional de +2 para a mesma ação, *ou* um efeito de duas tensões àquela ação.
-  Se a façanha criar uma exceção à regra, faça uma exceção ainda maior (isso pode ser difícil, dependendo da exceção original. Não se preocupe, você tem outras opções).

Tenha em mente que uma façanha melhorada substitui efetivamente a original. Pense nela como uma única super-façanha que custa dois espaços (e dois pontos de recarga) por ser mais poderosa que outras façanhas. Aqui estão algumas possibilidades:

-  **Mestre Em Combate Avançado.** (requer [Mestre em Combate](#adicionando-bônus-a-uma-ação)) quando você está em uma luta contra alguém armado, recebe um bônus de +2 ao criar uma vantagem usando a façanha Mestre Em Combate.
-  **Descendente da Corte.** (requer [Filho da Corte](#adicionando-bônus-a-uma-ação)) Quando usar Superar em conjunto com a façanha Filho da Corte, você pode adicionalmente criar um aspecto de situação que descreva como a atitude geral de todos fica a seu favor. Se alguém quiser tentar se livrar desse aspecto ele deve ser bem-sucedido contra uma oposição Razoável (+2).
-  **Ritualista Experiente.** (requer [Ritualista](#criar-uma-exceção-à-regra)) Você recebe um bônus de +2 quando usa a perícia Conhecimentos em lugar de outra qualquer durante um desafio. Isso permite a você usar Conhecimentos duas vezes no mesmo desafio.

### Ramificando Efeitos

Ramificar efeitos é criar uma nova façanha relacionada à original em tema ou alvo, mas fornecendo um efeito totalmente novo. Se você pensar nos efeitos cumulativos como uma linha vertical de uma façanha ou perícia, imagine os efeitos ramificados como expansões laterais dessa linha.

Se sua façanha original adicionava uma ação a uma perícia, uma façanha ramificada pode adicionar uma ação diferente àquela perícia, conceder um bônus a uma ação diferente que a perícia já tenha ou criar uma exceção às regras, etc. O efeito mecânico não está relacionado à façanha original, mas dá seu próprio toque complementar à mistura.

Isso permite criar alguns caminhos diferentes para fazer algo incrível com apenas uma façanha. Você pode usar isso para destacar diferentes elementos de certa perícia e ajudar os personagens a se diferenciarem uns dos outros, mesmo que possuam níveis altos nas mesmas perícias, por possuírem façanhas conectadas diferentes.

Um exemplo de como isso funciona: vamos dar uma olhada na perícia Enganar. Ao ler a descrição da perícia, há vários caminhos que podemos usar para criar façanhas: mentir, prestidigitação, disfarce, criar histórias duvidosas ou conﬂitos sociais.

Vamos fazer nossa primeira façanha parecida com isso:

-  **Enrolador.** Você receber um bônus de +2 quando usar uma ação de superar com a perícia Enganar, desde que você não tenha que falar mais que algumas frases com a pessoa para despistá-la.

Aqui estão algumas opções interessantes para criar façanhas ramificadas:

-  **Disfarce Rápido** (requer Enrolador). Você é capaz de montar um disfarce convincente num piscar de olhos usando itens que o cercam. Faça uma rolagem de Enganar para criar um disfarce sem precisar de preparação em quase qualquer situação**.**
-  **Saída de Mestre** (requer Enrolador). Você consegue inventar uma história como ninguém, mesmo que não tenha preparado nada previamente. Toda vez que tentar superar em público usando a perícia Enganar, adicione automaticamente um aspecto de situação que represente a sua história falsa, e ganhe uma invocação grátis dela.
-  **Ei, o Que Foi Aquilo?** (requer Enrolador). Ganhe um bônus de +2 sempre que usar Enganar para distrair momentaneamente alguém, desde que falar faça parte da distração.

Cada uma dessas façanhas se relaciona tematicamente a usos rápidos e espontâneos de Enganar, mas cada uma tem seu toque especial.

- [« Perícias e Façanhas](../pericias-e-facanhas/)
- [Lista de Perícias »](../lista-de-pericias/)
