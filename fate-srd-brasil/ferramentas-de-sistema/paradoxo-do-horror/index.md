---
title: O Paradoxo do Horror
layout: default
---

## O Paradoxo do Horror

 
A princípio, o Fate não serve tão bem para jogos de horror. Está bem ali, na introdução do Fate Sistema Básico “... funciona melhor em qualquer tema que os personagens sejam proativos, capazes e possuam vidas dramáticas”. O horror faz o seu trabalho sombrio ao colocar os personagens em circunstâncias mortais e inescapáveis além de seu controle. No horror, os personagens são frequentemente forçados a reagir, em lugar de agir. Apesar de suas capacidades, os personagens enfrentam ameaças que são superiores a eles; sua competência não é o bastante para vencer. A derrota parece ser inevitável e o sucesso vem a um alto custo. Ao invés de focar na vitória, os personagens focam mais em sobreviver os próximos minutos e escapar vivos.

Como conciliar essas duas realidades? Como pegar um sistema criado para personagens proativos e capazes e transformá-lo num sistema que sirva às exigências dodo horror?

Esse é o paradoxo do horror em Fate. Eis a nossa solução.
 
### Os Elementos do Horror
 
No fundo, o que importa no horror é a resposta emocional visceral que os jogadores têm durante o jogo. O sistema é completamente capaz de fazer isso, se tiver sua ajuda. Para os propósitos do Ferramentas do Sistema, horror é uma combinação de uma atmosfera opressiva, circunstâncias impossíveis e desespero absoluto.

#### Atmosfera Opressiva

__Force à Vontade:__ Mesmo que forçar um aspecto não seja uma ferramenta para _forçar resultados_, ainda assim é uma ótima ferramenta para _fazer coisas darem errado_. Logo, faça isso abundantemente. Coloque aspecto nas cenas, na história, na campanha — force-os para fazer com que tudo dê errado para todos. Simplesmente por colocar ___A Morte Vem Para Todos___ em uma história e forçar ele na pior hora (para os jogadores) tornará as coisas muito piores, trazendo muita tensão. Sim, os jogadores afetados sairão com alguns pontos de destino — que precisarão para sobreviver —, mas também sentirão a pressão do momento — além de se perguntarem quando mais aspectos serão forçados. Machuque-os. Deixe-os preocupados

__Todo Caminho é Obscuro:__ Aspectos não são a única forma de evocar uma atmosfera. Isso também pode ser feito com obstáculos e zonas. Nunca é fácil fugir do horror, então o caminho para a salvação deve estar repleto de empecilhos (veja mais em obstáculos, abaixo). Além disso, quando estiver desenhando os mapas de zonas, deixe-os mais claustrofóbicos que o normal: mais zonas que cubram menos espaço. Embora as distâncias físicas não sejam muito diferentes, é simplesmente mais difícil fugir de lugares perigosos em jogos de horror — em uma casa normal, a porta da frente pode estar a apenas uma zona de distância. Em uma casa assombrada? Tente mais ou menos cinco ou dez... e _nunca_ deixe-os correr sem interrupções.
 
#### Circunstâncias Impossíveis

O fato de os personagens serem capazes não significa que as coisas devem ser fáceis para eles.

Qualquer obstáculo cuja dificuldade esteja dois níveis acima da perícia usada para superá-lo é capaz de requerer uma invocação de aspecto. Com as melhores perícias dos jogadores girando em torno de Bom ou Ótimo, isso significa que os níveis iniciais de dificuldade devem começar na mesma faixa e ultrapassá-la em seguida. Não pegue leve nisso. Pegue pesado mesmo!

Dificuldades altas trazem chances maiores de falha. Ao invés de permitir que isso paralise os jogadores e deixe-os apáticos, faça bastante uso do “sucesso a um custo” como uma alternativa — e torne os custos realmente pesados. O preço deve ser desconfortável. Ao pagarem o que é devido, os jogadores sentirão uma fisgada do horror. Cada passo em frente derrama um pouco mais de sangue. É como morrer aos poucos. 

Em lutas contra inimigos impossivelmente difíceis, os jogadores estarão mais inclinados a conceder, para tentar ganhar algum controle da situação. Não há razão para não aceitar isso, mas você deve sempre preferir concessões que realmente machucam, de verdade. É aqui que pagarão o preço e sentirão ainda mais a mordida do horror.

Tudo isso conduz a uma sensação de desespero.

Depois disso...

#### **Desespero Absoluto**

O desespero surge das escolhas difíceis feitas sob pressão e dos recursos, que são escassos e estão em risco constante. Os recursos limitados de um PJ incluem pontos de destino, estresse e consequências. Limite-os sem torná-los ausentes. Considere:

+ **Recarga baixa:** Se os jogadores possuírem apenas alguns pontos de destino, esses pontos serão preciosos. Eles precisarão gastá-los se quiserem ter sucesso. A tensão nessas escolhas aumenta a ansiedade.
+ ** Estresse mínimo:** Não forneça mais do que uma caixa ou duas, se é que vai fornecer alguma. No horror, personagens não devem possuir muita resistência antes de começar a apanhar e sangrar. Além disso, se você usar caixas de estresse mental ou de sanidade, você estará a apenas alguns passos dos traumas duradouros, do terror e da loucura.
+ **Consequências fracas:** Considere fazer com que as consequências absorvam menos danos. O padrão é -2/-4/-6 para suave, moderada e severa, o que é bem resistente. Para o horror, considere cortar esses valores à metade ou tente algo como -1/-2/-4. Não será necessário muito mais do que um golpe para que os personagens realmente se machuquem, tornando qualquer conflito ainda mais perigoso.

Alinhe seu modelo de sistema com as escolhas difíceis que os personagens precisam realizar sob pressão. Isso eliminará vitórias fáceis e sem consequências. Afinal de contas, os PJs por si só são recursos limitados: não podem estar em dois lugares ao mesmo tempo e a pressão aparece quando o tempo é curto. O tempo é o recurso limitado que você como Narrador pode controlar. No horror, nunca deve haver tempo o bastante.

Portanto, dê aos jogadores muita coisa para fazer, mas não dê tempo o bastante para fazê-las. As pessoas e coisas com as quais os personagens se preocupam também são recursos limitados — e para o Narrador, muitas vezes são coisas mais fáceis de ameaçar do que o próprio personagem. Salve seu marido ou seu filho: mas não ambos — você é apenas um e há duas bombas em extremos opostos da casa.

Seus jogadores o odiarão por isso e, se passaram por tudo isso porque queriam horror, então o amarão por isso também, entre os gritos de pavor.

------

- [« Supers](../supers/)
