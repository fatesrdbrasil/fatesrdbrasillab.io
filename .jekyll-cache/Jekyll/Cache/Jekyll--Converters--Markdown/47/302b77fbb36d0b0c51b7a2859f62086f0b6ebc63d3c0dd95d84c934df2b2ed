I"J2<h1 id="exemplos-de-personagens">Exemplos de Personagens</h1>

<p>Aqui estão quatro personagens que podem ser usados como estão ou como inspiração para criar seus próprios. No exemplo eles têm apenas uma façanha, mas você pode criar mais duas para cada um sem diminuir a Recarga.</p>

<h2 id="dandarion-o-mais-épico">Dandarion, O Mais Épico</h2>

<p>Dandarion é um bárbaro destemido e boca suja que vaga o mundo em busca de entretenimento barato e fugaz. Criado por ursos polares de dentes-de-sabre, na região das Geleiras Flamejantes, em um ponto cardeal de um Reino sem norte, Dandarion tornou-se uma lenda em todo continente por seus feitos heroicos e pouco usuais. Apesar de ter espírito selvagem, o bárbaro possui uma incômoda inclinação por ajudar os outros e punir badernistas que passam dos limites.</p>

<hr />

<h3 id="dandarion">Dandarion</h3>

<h4 id="aspectos"><strong>Aspectos</strong></h4>

<table>
  <thead>
    <tr>
      <th style="text-align: right"><strong><em>Tipo</em></strong></th>
      <th><strong><em>Aspecto</em></strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: right"><strong>Conceito</strong></td>
      <td><strong><em>O Bárbaro Mais Épico das Geleiras Flamejantes</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Dificuldade</strong></td>
      <td><strong><em>Só Entendo As Coisas Literalmente</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Outros:</strong></td>
      <td><strong><em>Conquistador Em Todos os Sentidos</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"> </td>
      <td><strong><em>Ajudo os Outros Porque Sim</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"> </td>
      <td><strong><em>Pela Força É Sempre Mais Fácil</em></strong></td>
    </tr>
  </tbody>
</table>

<h4 id="abordagens"><strong>Abordagens</strong></h4>

<table>
  <thead>
    <tr>
      <th style="text-align: right"><strong>Abordagens</strong>*</th>
      <th><strong><em>Nível</em></strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: right"><strong>Ágil:</strong></td>
      <td>Razoável (+2)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Cuidadoso:</strong></td>
      <td>Regular (+1)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Esperto:</strong></td>
      <td>Medíocre (+0)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Estiloso:</strong></td>
      <td>Razoável (+2)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Poderoso:</strong></td>
      <td>Bom (+3)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Sorrateiro:</strong></td>
      <td>Regular (+1)</td>
    </tr>
  </tbody>
</table>

<h4 id="façanhas"><strong>Façanhas</strong></h4>

<ul>
  <li>
    <p><strong>Solucionador de Problemas:</strong> Como eu sou <em>Solucionador de Problemas</em>, recebo +2 para <em>Superar ou Criar Vantagem</em> contra oposições passivas que precisem de cuidado e planejamento para serem resolvidas.</p>
  </li>
  <li>
    <p><strong>Estresse: <code class="language-plaintext fate_font highlighter-rouge">3</code></strong></p>
  </li>
  <li>
    <p><strong>Consequências</strong></p>
    <ul>
      <li>Suave (2):</li>
      <li>Moderada (4):</li>
      <li>Severa (6):</li>
    </ul>
  </li>
</ul>

<p><strong>Recarga: 3</strong></p>

<hr />

<h2 id="lola-calavera">Lola Calavera</h2>

<p>Após ter perdido sua vida e sua família em uma festa (muito) mal sucedida promovida pelo Rei de Coronilla, Lola retornou dos mortos como um fantasma para assombrar a família real e pôr um fim na tirania da metrópole sobre seu povoado. Como ex-dançarina de flamenco, ela mantém seus trajes característicos e seu comportamento sedutor, utilizando-se de beleza, inteligência e poderes sobrenaturais para atormentar os que atravessam seu caminho.</p>

<h3 id="lola">Lola</h3>

<h4 id="aspectos-1"><strong>Aspectos</strong></h4>

<table>
  <thead>
    <tr>
      <th style="text-align: right"><strong><em>Tipo</em></strong></th>
      <th><strong><em>Aspecto</em></strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: right"><strong>Conceito</strong></td>
      <td><strong><em>Animada Dançarina Fantasma do Castelo Real</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Dificuldade</strong></td>
      <td><strong><em>Perseguida pelos Padres de Coronilla</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Outros:</strong></td>
      <td><strong><em>Eu Ponho Medo nos Quadris dos Reis</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"> </td>
      <td><strong><em>Pepe Me Ajuda a Balançar o Esqueleto</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"> </td>
      <td><strong><em>Todo Dia É</em> Dia de Los Muertos</strong></td>
    </tr>
  </tbody>
</table>

<h4 id="abordagens-1"><strong>Abordagens</strong></h4>

<table>
  <thead>
    <tr>
      <th style="text-align: right"><strong>Abordagens</strong>*</th>
      <th><strong><em>Nível</em></strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: right"><strong>Ágil:</strong></td>
      <td>Razoável (+2)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Cuidadoso:</strong></td>
      <td>Regular (+1)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Esperto:</strong></td>
      <td>Regular (+1)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Estiloso:</strong></td>
      <td>Bom (+3)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Poderoso:</strong></td>
      <td>Medíocre (+0)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Sorrateiro:</strong></td>
      <td>Razoável (+2)</td>
    </tr>
  </tbody>
</table>

<h4 id="façanhas-1"><strong>Façanhas</strong></h4>

<ul>
  <li>
    <p><strong>Aparição Espectral:</strong> Como eu sou <em>Aparição Espectral</em>, <em>uma vez por cena</em> posso desaparecer e ressurgir instantaneamente em outra zona próxima, ignorando bloqueios no caminho.</p>
  </li>
  <li>
    <p><strong>Estresse: <code class="language-plaintext fate_font highlighter-rouge">3</code></strong></p>
  </li>
  <li>
    <p><strong>Consequências</strong></p>
    <ul>
      <li>Suave (2):</li>
      <li>Moderada (4):</li>
      <li>Severa (6):</li>
    </ul>
  </li>
</ul>

<p><strong>Recarga: 3</strong></p>

<hr />

<h2 id="francis-maestlin-o-caçador">Francis Maestlin, O Caçador</h2>

<p>Francis Maestlin é um dos membros mais ativos do Esquadrão Argos, a equipe de caça e escolta da expedição de colonização do planeta Tauri, que orbita a estrela Mérope. Ele é calmo e paciente, com um preparo físico invejável e um profundo respeito pela vida selvagem. Apesar de seu fascínio por todas as formas de vida e seu amor por filhotes, a mira precisa e a velocidade de reação de Francis lhe renderam o título de “O Caçador”.</p>

<hr />

<h3 id="francis-maestlin">Francis Maestlin</h3>

<h4 id="aspectos-2"><strong>Aspectos</strong></h4>

<table>
  <thead>
    <tr>
      <th style="text-align: right"><strong><em>Tipo</em></strong></th>
      <th><strong><em>Aspecto</em></strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: right"><strong>Conceito</strong></td>
      <td><strong><em>Soldado de Caça e Escolta do Esquadrão Argos</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Dificuldade</strong></td>
      <td><strong><em>Minha Fama e Eu Somos Completamente Diferentes</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Outros:</strong></td>
      <td><strong><em>Estude Primeiro Atire Depois</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"> </td>
      <td><strong><em>Minha Família São Spinoza e A Força Expedicionária</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"> </td>
      <td><strong>*Respeito à Vida</strong></td>
    </tr>
  </tbody>
</table>

<h4 id="abordagens-2"><strong>Abordagens</strong></h4>

<table>
  <thead>
    <tr>
      <th style="text-align: right"><strong>Abordagens</strong>*</th>
      <th><strong><em>Nível</em></strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: right"><strong>Ágil:</strong></td>
      <td>Bom (+3)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Cuidadoso:</strong></td>
      <td>Razoável (+2)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Esperto:</strong></td>
      <td>Regular (+1)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Estiloso:</strong></td>
      <td>Medíocre (+0)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Poderoso:</strong></td>
      <td>Razoável (+2)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Sorrateiro:</strong></td>
      <td>Regular (+1)</td>
    </tr>
  </tbody>
</table>

<h4 id="façanhas-2"><strong>Façanhas</strong></h4>

<ul>
  <li>
    <p><strong>Precisão Impossível:</strong> Quando uso <em>Precisão Impossível</em>, ganho +2 em <em>ataques Cuidadosos</em> contra alvos em movimento.</p>
  </li>
  <li>
    <p><strong>Estresse: <code class="language-plaintext fate_font highlighter-rouge">3</code></strong></p>
  </li>
  <li>
    <p><strong>Consequências</strong></p>
    <ul>
      <li>Suave (2):</li>
      <li>Moderada (4):</li>
      <li>Severa (6):</li>
    </ul>
  </li>
</ul>

<p><strong>Recarga: 3</strong></p>

<hr />

<h2 id="exia">Exia</h2>

<p>Única pilota do robô Apoteose, Exia atua como vanguarda na defesa da Terra contra a invasão dos Gargantuas que ameaçam a humanidade. Impulsiva, honrada e extremamente teimosa, Exia entrou para a equipe de defesa do planeta quando ainda era uma criança, acompanhando de perto a evolução da tecnologia terráquea e o despontar de pilotos lendários, que hoje lhe servem de inspiração.</p>

<hr />

<h3 id="exia-1">Exia</h3>

<h4 id="aspectos-3"><strong>Aspectos</strong></h4>

<table>
  <thead>
    <tr>
      <th style="text-align: right"><strong><em>Tipo</em></strong></th>
      <th><strong><em>Aspecto</em></strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: right"><strong>Conceito</strong></td>
      <td><strong><em>Orgulhosa Pilota de Zelotes na Defesa Global</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Dificuldade</strong></td>
      <td><strong><em>Lidar Com Pessoas Não É Pra Mim</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Outros:</strong></td>
      <td><strong><em>Combater Monstros Gigantes É O Que Eu Faço Melhor</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"> </td>
      <td><strong><em>Ordens Superiores São Apenas Guias</em></strong></td>
    </tr>
    <tr>
      <td style="text-align: right"> </td>
      <td><strong>*Miura Tem Minha Confiança</strong></td>
    </tr>
  </tbody>
</table>

<h4 id="abordagens-3"><strong>Abordagens</strong></h4>

<table>
  <thead>
    <tr>
      <th style="text-align: right"><strong>Abordagens</strong>*</th>
      <th><strong><em>Nível</em></strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: right"><strong>Ágil:</strong></td>
      <td>Razoável (+2)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Cuidadoso:</strong></td>
      <td>Medíocre (+0)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Esperto:</strong></td>
      <td>Regular (+1)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Estiloso:</strong></td>
      <td>Razoável (+2)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Poderoso:</strong></td>
      <td>Regular (+1)</td>
    </tr>
    <tr>
      <td style="text-align: right"><strong>Sorrateiro:</strong></td>
      <td>Bom (+3)</td>
    </tr>
  </tbody>
</table>

<h4 id="façanhas-3"><strong>Façanhas</strong></h4>

<ul>
  <li>
    <p><strong>Contato com Zelote Apoteose:</strong> Como tenho <em>Contato com Zelote Apoteose</em>, <em>uma vez por sessão</em> posso convocá-lo para combater ameaças gigantes.</p>
  </li>
  <li>
    <p><strong>Estresse: <code class="language-plaintext fate_font highlighter-rouge">3</code></strong></p>
  </li>
  <li>
    <p><strong>Consequências</strong></p>
    <ul>
      <li>Suave (2):</li>
      <li>Moderada (4):</li>
      <li>Severa (6):</li>
    </ul>
  </li>
</ul>

<p><strong>Recarga: 3</strong></p>

<ul>
  <li><a href="../narrando-o-jogo/">« Aprendendo Com Suas Ações: Evolução do Personagem</a></li>
</ul>
:ET