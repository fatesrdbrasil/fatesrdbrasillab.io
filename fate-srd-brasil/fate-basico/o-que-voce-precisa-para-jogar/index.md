---
title: O Que Você Precisa Para Jogar
layout: default
---

## O Que Você Precisa Para Jogar

Jogar uma partida de *Fate* é bem simples. Você precisa:

- **De três a cinco pessoas**. Uma delas será o Narrador do Jogo (ou apenas Narrador) e o restante serão os **jogadores**. Explicaremos esses termos logo à frente.
-  Uma **ficha de personagem** por jogador e algumas folhas de papel em branco para anotações. Falaremos sobre o que é a ficha de personagem logo abaixo (Narradores, quaisquer personagens importantes que você interprete também podem ter uma ficha).
- **Dados Fate**, ao menos quatro, mas preferencialmente quatro por participante. Dados Fate (ou Fudge) são de um tipo especial de dado de seis lados que são marcados em dois lados com o símbolo de adição (+), em dois lados com o símbolo de subtração (-), e dois lados em branco (0). Você pode conseguir esses dados em lojas especializadas.
- O **Baralho Fate** é uma alternativa aos dados Fate. Ele consiste em um conjunto de cartas que simulam as jogadas dos dados e foi desenvolvido para funcionar de forma semelhante aos dados.
- Marcadores para representar os **pontos de destino**. Fichas de pôquer, contas de vidro ou qualquer coisa similar funciona. Serão necessários ao menos trinta ou mais desses só para garantir que terá o bastante para qualquer jogo. Você pode usar riscos feitos com lápis em sua ficha de personagem em vez de marcadores, mas marcadores físicos são mais divertidos.
-  Fichas pautadas ou cartões em branco para aspectos. São opcionais, mas bastante úteis para marcar **aspectos** durante o jogo. Falaremos sobre os aspectos em breve.

------

Se você não deseja usar os dados Fudge, ou não possui qualquer conjunto de dados normais de seis lados pode substituí-los. Se for usar dados normais, leia 5 e 6 como `+`{: .fate_font}, 1 e 2 como `-`{: .fate_font} e 3 e 4 como `0`{: .fate_font}.

------

- [« O Básico](../basico/)
- [Jogadores e Narrador do Jogo »](../jogadores-narrador-de-jogo/)
