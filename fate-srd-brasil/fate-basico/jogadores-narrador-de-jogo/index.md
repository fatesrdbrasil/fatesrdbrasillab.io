---
title: Jogadores e Narrador do Jogo
layout: default
---

## Jogadores e Narrador do Jogo

Em uma partida de *Fate*, você pode ser um **Jogador** ou **Narrador do Jogo**.

Se você for um jogador, a primeira coisa a fazer é assumir a responsabilidade de interpretar um dos protagonistas do jogo, os quais chamamos de **personagens do jogador** (ou PJ, para abreviar). Você toma decisões por seu personagem e descreve para todos na mesa o que ele diz e faz. Você também será o responsável pela parte mecânica de seu personagem – rolando os dados quando apropriado, escolhendo quais habilidades usar em determinada situação e gastando os pontos de destino.

Se você é o Narrador do Jogo seu trabalho é assumir a responsabilidade do mundo do qual os PJs fazem parte. O Narrador toma decisões e lança os dados por todos os personagens no mundo de jogo que não possuírem um jogador para interpretar – nós os chamamos de **personagens do narrador** (ou PdN). Você descreve os ambientes e lugares onde os PJs irão durante o jogo e cria os cenários e situações com os quais eles interagem. Você também atuará como juiz de regras, determinando as consequências das decisões dos PJs e seu impacto sobre a história e o enredo.

Tanto os jogadores quanto o narrador do jogo possuem uma responsabilidade secundária: **colaborar com a diversão de todos**. Fate funciona melhor como um esforço colaborativo, onde todos compartilham ideias e buscam por oportunidades de tornar esses momentos o mais divertidos possível.

- [« O que você precisa para jogar](../o-que-voce-precisa-para-jogar/)
- [A Ficha de Personagem »](../ficha-de-personagem/)
