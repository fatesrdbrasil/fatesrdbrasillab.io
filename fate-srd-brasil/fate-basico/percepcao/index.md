---
title: Percepção
layout: default
---

### Percepção

A perícia Percepção trata justamente disso - perceber as coisas. Ela anda lado a lado com Investigar, representando a percepção geral do personagem, sua habilidade em notar detalhes rapidamente e outros poderes de observação. Normalmente, ao usar Percepção, o resultado será mais rápido que Investigar, portanto os detalhes são mais superficiais, mas você também não precisa de tanto esforço para encontrá-los.

- `O`{: .fate_font} **Superar:** Percepção quase não é usada para superar obstáculos, mas é usada como uma reação: perceber algo em uma cena, ouvir algum som fraco, notar a arma escondida na cintura do adversário.
Perceba que isso não é uma desculpa para o Narrador pedir rolagens de Percepção a torto e a direito para ver quão observadores os jogadores são; isso é entediante. Ao invés disso, peça testes de Percepção quando ser bem sucedido resultaria em algo interessante e falhar, também.
- `C`{: .fate_font} **Criar Vantagem:** Você usa Percepção para criar aspectos baseados na observação direta – procurar em uma sala por aspectos que se destacam, encontrar uma rota de fuga em um prédio abandonado, notar alguém em meio a uma multidão, etc. Ao observar pessoas, Percepção pode mostrar o que está acontecendo com elas *externamente*; para notar mudanças internas, veja Empatia. Você também pode usar Percepção para declarar que seu personagem encontra algo que poderá ser utilizado a seu favor em alguma situação, como uma ***Rota de Fuga Conveniente*** quando está tentando fugir de um prédio ou uma ***Fraqueza Sutil*** na linha de defesa inimiga. Por exemplo, se você estiver em uma briga de bar, poderá realizar uma rolagem de Percepção para dizer que há uma poça no chão, bem próxima aos pés de seu oponente e que poderia fazê-lo escorregar.
- `A`{: .fate_font} **Atacar:**Esta perícia não é utilizada em ataques.
- `D`{: .fate_font} **Defender:** Você pode usar Percepção para se defender do uso de Furtividade contra você ou para descobrir se está sendo observado.

#### Façanhas Para Percepção

-  **Perigo Iminente:** Você possui uma capacidade sobrenatural para detectar o perigo. Sua perícia Percepção funciona normalmente para condições como camuﬂagem total, escuridão ou outros empecilhos sensoriais em situações onde alguém ou algo tem a intenção de prejudicar você.
-  **Leitura Corporal:** Você pode usar Percepção no lugar de Empatia para aprender sobre os aspectos de alguém através da observação.
-  **Reação Rápida:** Você pode usar Percepção no lugar de Atirar para realizar tiros rápidos que não envolvam uma grande necessidade de precisão na pontaria. No entanto, por estar realizando uma ação instintiva, você não tem a capacidade de identificar seu alvo quando usar essa façanha. Então, por exemplo, você pode ser capaz de atirar em alguém que está se movendo por entre os arbustos com essa façanha, mas você não será capaz de dizer se é um amigo ou inimigo antes de puxar o gatilho. Escolha com cuidado!

- [« Ofícios](../oficios/)
- [Provocar »](../provocar/)
