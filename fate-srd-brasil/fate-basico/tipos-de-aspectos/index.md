---
title: Tipos De Aspectos
layout: default
---

## Tipos De Aspectos

Todo jogo de *Fate* tem diferentes tipos de aspectos: aspectos de personagem, de situação, consequências e impulsos. O que os diferencia é, principalmente, ao quê estão ligados e o quanto duram.

### Aspectos de Jogo

Aspectos de jogo são detalhes fixos no jogo, por isso o nome. Mesmo que possam mudar com o tempo, nunca desaparecem. Se você passou pela criação de jogo, já definiu esses aspectos - as questões atuais ou iminentes a serem resolvidas. Eles descrevem os problemas ou ameaças que existem no mundo que servirão de base para a história de seu jogo.

Todos podem invocar, forçar ou obter vantagem sobre um aspecto de jogo a qualquer hora; eles sempre estarão lá, presentes para qualquer um.

### Aspectos de Personagem

Aspectos de personagem também são permanentes, mas de alcance menor, e ligados a um PJ ou PdN. Podem descrever uma infinidade de coisas que definem um personagem, como:

+  Traços significativos da personalidade e crenças (***Não Resisto a Um Rostinho Bonito***, ***Nunca Deixo Um Amigo Para Trás***, ***Um Tsytaviano Bom é Um Tsytaviano Morto***).
+  O histórico ou profissão do personagem (***Educado Na Academia Das Lâminas***, ***Nascido No Espaço***, ***Ladrão de Rua Cibernético***).
+  Uma posse interessante ou característica notável (***Espada Herdada De Meu Pai***, ***Aparência Impecável***, ***Olhos Aguçados***).
+  Ligação com pessoas e organizações (***Na Liga dos Punhos Rápidos***, ***Favor Do Rei***, ***Membro Da Companhia Dos Lordes***).
+  Problemas, metas ou questões com as quais o personagem deve lidar (***Prêmio Por Minha Cabeça***, ***O Rei Deve Morrer***, ***Medo de Altura***).
+  Títulos, reputação ou obrigações que o personagem tenha (***Membro Importante Da Guilda Dos Mercadores***, ***Vigarista Eloquente***, ***Guiado Pela Honra De Vingar Meu Irmão***).

Você pode invocar ou forçar um aspecto de seu personagem sempre que ele for relevante. Narradores podem propor forçar o aspecto de qualquer personagem. Os jogadores podem sugerir forçar os aspectos de outros indivíduos, mas o narrador tem a palavra final sobre o que é ou não uma sugestão válida.

### Aspectos de Situação

Um aspecto de situação é temporário, feito para durar por apenas uma cena ou até que não faça mais sentido (mas não mais que uma sessão, geralmente). Aspectos de situação podem ser ligados ao ambiente onde a cena ocorre - afetando todos os presentes - mas também podem ser colocados em personagens específicos ao criar vantagem.

Aspectos de situação descrevem características marcantes da situação em que os personagens se encontram. Isso inclui:

+  Características físicas do ambiente (***Mata Fechada***, ***Neve Densa***, ***Planeta Com Baixa Gravidade***).
+  Posicionamento (***Ponto de Atirador***, ***Nas Árvores***, ***Na Retaguarda***).
+  Obstáculo imediato (***Celeiro Em Chamas***, ***Fechadura Trancada***, ***Fenda Para o Abismo***).
+  Detalhes contextuais presentes (***Cidadãos Desapontados***, ***Câmeras De Segurança***, ***Máquinas Barulhentas***).
+  Mudanças repentinas nas condições dos personagens (***Areia Nos Olhos***, ***Desarmado***, ***Encurralado***, ***Coberto De Gosma***).

Quem poderá usar os aspectos de situação varia de acordo com o contexto — às vezes será claro, mas em outras será preciso justificar como está usando aquele aspecto para que faça sentido na cena. O Narradores tem a palavra final sobre a validade da invocação ou forçamento de tais aspectos.

Às vezes esses aspectos se tornam obstáculos que os personagens precisam superar. Outras vezes justificam sua oposição à ação de alguém.

### Consequências

Uma consequência é mais permanente que um aspecto de situação, mas não tanto como um aspecto de personagem. Elas são um tipo especial de aspecto que você opta por receber durante conﬂitos para não precisar sair dele e descrevem ferimentos ou problemas mais duradouros (***Ombro Deslocado***, ***Nariz Sangrando***, ***Carma Social***).

Consequências permanecem por algum tempo, de algumas cenas a algumas sessões ou cenários, dependendo de quão severa ela for. Por causa de seu teor negativo, é provável que ela seja forçada contra você com frequência depois de adquirida. Fora isso, qualquer um que tenha motivos para se beneficiar da consequência poderá invocá-la ou criar vantagem sobre ela.

### Impulsos

Impulsos são um tipo de aspecto totalmente diferente. Você consegue um impulso quando tenta criar vantagem, mas não é tão bem-sucedido ou como um bônus para quando tenta algo e é muito bem sucedido. Impulsos podem ser invocados gratuitamente, mas desaparecem em seguida.

Se quiser, pode permitir que outro personagem invoque o seu impulso, se isso for relevante e puder ajudá-lo de alguma forma.

- [« Aspectos e Pontos de Destino](../aspectos-e-pontos-de-destino/)
- [Para que servem os Aspectos »](../para-que-servem-os-aspectos/)
