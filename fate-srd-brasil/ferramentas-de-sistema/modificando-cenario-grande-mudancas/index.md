---
title: "Modificando O Cenário: Grandes mudanças"
layout: default
---

## Modificando O Cenário: Grandes Mudanças

Embora os personagens em Fate sejam sempre protagonistas, às vezes os jogadores decidem que querem modificar o cenário, limpando as ruas da cidade do crime ou assegurando o reforço de uma fortificação para evitar ataques futuros.

### Descubra os Problemas Antes de Resolvê-los

Primeiramente os jogadores devem reunir as informações necessárias. Talvez os personagens decidam analisar sua própria experiência acumulada — “Não é O’Banner o principal distribuidor de drogas da cidade?” — ou talvez precisem fazer rolagens de Contatos, Investigar ou Conhecimento para descobrir antigas alianças e conhecimento perdido. De qualquer forma, o grupo deve construir uma série de aspectos do cenário que representem o problema existente.

Para limpar as ruas, um grupo poderia listar:

+ ___Traficantes de Drogas___
+ ___Interceptadores e Transportadores___
+ ___Financiadores de Drogas___
+ ___Policiais Corruptos___
+ ___O Rei Do Crime, Marty O’Banner___

### Uma Parte de Cada Vez

Para cada aspecto do cenário que o grupo desejar mudar, precisarão rolar contra uma dificuldade apropriada para resolver o problema. Por exemplo, prender todos os ___Traficantes de Drogas___ pode exigir que os personagens realizem rolagens de Recursos para mobilizar a força policial ou Vigor para capturar pessoalmente os bandidos. Ao mesmo tempo que os personagens podem invocar seus próprios aspectos, o Narrador pode pagar pontos de destino para invocar os aspectos de cenário existentes — como ___Policiais Corruptos___ – para complicar o avanço dos jogadores. 

Para cada rolagem, use o resultado para jogar os personagens no meio de uma situação — como um episódio de uma série de TV — que reflita o resultado da rolagem. Em uma rolagem ruim, os Traficantes de Drogas se esconderam bem e será preciso uma busca profunda para encontrá-los. Com uma rolagem bem-sucedida, o grupo encontra os bandidos, mas precisa fazê-los falar. Seja como for, se os personagens conseguirem resolver a situação, mude o aspecto de cenário para algo positivo, trocando ___Traficantes de Drogas___ por ___Algumas Ruas Mais Limpas.___

Uma vez que comecem progredir, deixe o grupo invocar livremente aspectos de cenários resolvidos anteriormente, quando aplicável, já que o trabalho pode se tornar mais fácil conforme concluem as tarefas da lista.

### Precisamos de Uma Montagem Cinematográfica!

Se não estiver interessado em interpretar as mudanças do dia a dia que ajudam a melhorar um sistema de justiça criminal ou para a fortificação de um castelo, você pode fazer uma montagem com cenas sobre o pro- gresso dos personagens. Determine um limite de tempo — incluindo um número limitado de ações — que represente o tempo e recursos disponíveis aos personagens para resolver a situação antes de retomar a história principal.

Para cada rolagem, os personagens tentam resolver um dos aspectos usando a perícia apropriada. Personagens podem trabalhar juntos ou podem se dividir para tentar resolver vários problemas ao mesmo tempo. Narre as mudanças e o sucesso do grupo como uma montagem cinematográfica, pausando apenas o tempo suficiente para cada rolagem a fim de saber se o grupo foi bem-sucedido ou falhou em melhorar a situação.


------

- [« Recarga](../recarga/)
- [Outros Dados »](../outros-dados/)
