---
title: Atletismo
layout: default
---

### Atletismo

A perícia Atletismo representa o nível geral de aptidão física do seu personagem, independentemente se isso deriva de treinamento, dons naturais ou alguma característica específica do cenário (como magia ou alteração genética). É o quão bem você move seu corpo. Assim sendo, é uma escolha popular para personagens que se envolvam em ação.

Atletismo é bem presente em todos os gêneros que combinam com *Fate* — seria apenas desnecessário em jogos que focam exclusivamente em interação social, onde não há conﬂitos físicos.

- `O`{: .fate_font} **Superar:** movimentos físicos – saltar, correr, escalar, nadar, etc. Você p Atletismo permite superar qualquer obstáculo que exige ode usar a ação de superar com Atletismo para se mover entre as zonas em um conﬂito se houver um aspecto de situação ou outro obstáculo em seu caminho. Você também rola Atletismo para perseguir ou correr em disputas ou desafios que dependam de atividades semelhantes.
- `C`{: .fate_font} **Criar Vantagem:** está indo para a parte mais alta do terreno, correndo mais rápido Ao criar uma vantagem usando Atletismo, você que seu oponente ou executando manobras acrobáticas deslumbrantes a fim de confundir seus adversários.
- `A`{: .fate_font} **Atacar:** Atletismo não é utilizado como perícia de combate.
- `D`{: .fate_font} **Defender:** conﬂitos contra ataques à curta e longa distância. Atletismo é uma perícia genérica na rolagem de defesa em Também é possível usar esta defesa contra personagens que estejam tentando passar por você, houver como intervir fisicamente em tal tentativa.

#### Façanhas para Atletismo

-  **Corredor Audaz:**Durante o conﬂito, você se move duas zonas sem precisar rolar os dados ao invés de apenas uma, desde que não haja aspectos de situação restringindo o seu movimento.
-  **Parkour Hardcore:** +2 quando quiser superar usando Atletismo em uma perseguição em telhados ou outro ambiente precário.
-  **Reação Atordoante:** Quando for bem-sucedido com estilo em uma defesa contra ataque de um oponente, você automaticamente contra -ataca com algum tipo de soco potente ou contundente. Você poderá colocar o aspecto ***Atordoado*** no seu oponente e ganhar uma invocação gratuita dele, ao invés de apenas um impulso.

- [« Lista de Perícias](../lista-de-pericias/)
- [Atirar »](../atirar/)
