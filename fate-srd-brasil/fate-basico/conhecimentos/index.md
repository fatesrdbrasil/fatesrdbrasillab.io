---
title: Conhecimentos
layout: default
---

### Conhecimentos

A perícia Conhecimentos trata de conhecimento e educação. Assim como outras perícias, ela recebeu esse nome porque se enquadra bem na proposta dos nossos exemplos – outros jogos podem chamá-la de Erudição, Conhecimento Acadêmico ou algo similar.

Se seu jogo por algum motivo precisar de campos de conhecimento separados, é possível dividir a perícia em diversos segmentos que seguem o mesmo modelo do original. Por exemplo, você pode ter uma perícia Conhecimentos específica para o sobrenatural e conhecimento arcano, e uma perícia de Conhecimento Acadêmico ligada à educação tradicional.

- `O`{: .fate_font} **Superar:** Você pode usar Conhecimentos para se livrar de obstáculos que necessitem da aplicação do conhecimento de seu personagem na tentativa atingir um objetivo. Por exemplo, você talvez faça uma rolagem de Conhecimentos pra decifrar alguma língua ancestral nas ruínas de uma tumba, presumindo que seu personagem já tenha pesquisado sobre isso em algum momento. Sinceramente, você pode usar Conhecimentos sempre que precisar saber se o seu personagem pode responder a alguma pergunta complicada, sempre que houver alguma tensão envolvida com não saber a resposta.
- `C`{: .fate_font} **Criar Vantagem:** Conhecimentos cria uma variedade de oportunidades ﬂexíveis para criar vantagem, contanto que possa pesquisar sobre a questão. A perícia frequentemente será utilizada para conseguir detalhes da história, alguma informação obscura descoberta ou já conhecida, mas se a informação lhe der vantagem numa cena futura, ela pode se tornar um aspecto. Da mesma forma, você pode usar Conhecimentos para criar vantagens baseadas em qualquer assunto que seu personagem possa ter estudado, o que é um jeito divertido de adicionar detalhes ao cenário.
- `A`{: .fate_font} **Atacar:** Esta perícia não é usada em conﬂitos (em nossos exemplos, os poderes mentais que Bandu utiliza são baseados na perícia Conhecimentos, a única exceção – ele pode usar Conhecimentos para realizar ataques e defesas mentais. Veja o capítulo Extras para mais detalhes sobre as formas de usar poderes e magias).
- `D`{: .fate_font} **Defender:** A perícia também não pode ser usada para defender.

#### Façanhas Para Conhecimentos

-  **Já Li Sobre Isso:** Você já leu centenas – se não milhares – de livros sobre inúmeros assuntos. Você pode gastar um ponto de destino para usar Conhecimentos no lugar de *qualquer outra perícia* em uma rolagem, desde que consiga explicar seu conhecimento sobre a ação que quer realizar.
-  **Escudo da Razão:** Você pode usar Conhecimentos como defesa contra tentativas de Provocar, contanto que consiga justificar sua habilidade de superar o medo através de pensamento racional e razão.
-  **Especialista:** Escolha um campo de especialização, como alquimia, criminologia ou zoologia. Você recebe +2 nas rolagens de Conhecimentos que estejam relacionadas ao seu campo de especialização.

- [« Condução](../conducao/)
- [Contatos »](../contatos/)
