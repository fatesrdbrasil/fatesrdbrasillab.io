---
title: Invocadores da Tempestade
layout: default
---

## Invocadores da Tempestade

### Notas de Criação

Esta é uma expansão do sistema das “Cinco Tempestades” descrita na sessão [Senhores da Tempestade][]. É um sistema completo que pode ser facilmente transformado em um jogo próprio, mas é igualmente fácil de se usar em conjunto com os outros sistemas de magia das Cinco Tempestades para formar um sistema mais completo.

### Descrição

Cada uma das cinco Tempestades — Terremoto, Dilúvio, Geleira, Inferno e Trovão — no centro da realidade é o lar de uma miríade de seres que têm sua morada nesses ambientes mortais. Para os leigos, eles são elementais — seres compostos dos elementos das Tempestades, mas dotados de inteligência e vontade próprias. Apesar de serem os mais numerosos dentro das tempestades, são apenas o começo.

Os elementais compõem a base do que parece um ecossistema dentro das Tempestades. Os mais numerosos são crias quase inteligentes, pequenas criaturas de natureza elemental. Cada grupo acima é mais poderoso, mas menor em número, até um limite atingido onde as criaturas começam a assumir forma e natureza distintas. Estas muitas vezes se assemelham a versões fantásticas de criaturas mundanas, enquanto algumas mais poderosas lembram humanos. Há príncipes e rainhas entre esses seres e dizem que os mais poderosos entre eles estão par a par com os próprios deuses.

Com a perícia e ferramentas adequadas, um Conjurador pode invocar esses elementos para realizar seus pedidos, com um perigo que corresponde à potência da criatura invocada. No entanto, os maiores seres das Tempestades estão fora do alcance de qualquer mero Conjurador.

Invocadores são diferentes dos Conjuradores pois possuem um pacto com uma das grandes forças das Tempestades, podendo usufruir desse acordo, influenciando seres ainda maiores das tempestades. No entanto, esses pactos têm seu preço.

### Versão Resumida

Não quer ler todas as regras? Use este resumo:

- Compre uma Perícia de Conjuração.
- Faça alguns rituais para invocar um elemental de Água, Fogo, Gelo,    Relâmpago ou Terra com uma dificuldade de Regular (+1) a Ótimo    (+4). Essa é a dificuldade que precisa ser vencida na rolagem de perícia, além de também representar o nível de habilidade do ser invocado. A invocação dura uma semana e só é possível manter um de    cada vez.
- Os elementais vêm em quatro tamanhos:
  - **Crias** (Regular [+1], 0 estresse, nenhuma consequência), pequenos orbes do tamanho de um punho. Não são tão espertos, mas são rápidos, sorrateiros e seguem ordens simples.
  - **Lacaios** (Razoável [+2], 0 estresse, 1 consequência suave), do tamanho de cães, mais fortes do que aparentam e capazes de transportar cargas pesadas ou realizar trabalhos simples.
  - **Serviçais** (Bom [+3], 2 de estresse, 2 consequências suaves), de tamanho e formato aproximado a humanos, entendem ordens mais complexas e são soldados eficientes.
  - **Assessores** (Ótimo [+4], 2 de estresse, 1 consequência suave e 1 moderada) são seres poderosos carregam consigo um aspecto da Tempestade, então são criaturas visualmente bastante incríveis.
- Se você tiver um pacto com um ser maior para se tornar mais poderoso, então:
  - Reduza sua Recarga em 1.
  - Adicione o aspecto ___Pacto com o Príncipe da [escolha a Tempestade]___.
  - Mude sua perícia de Conjuração para uma perícia de Invocação.
  - Com uma rolagem bem-sucedida, você agora pode invocar um elemental Excepcional (4 de estresse, 2 de armadura, -2/-
4/-6 de consequências), com uma descrição à sua escolha. Você ainda pode realizar conjurações normais com o elemento com tem o pacto, mas por que o faria?

### Mecânica


Este sistema adiciona duas perícias e alguns aspectos que refletem a magia de invocação. Também adiciona alguns aspectos específicos que refletem os pactos com as forças das Tempestades.

#### **Perícia:** Conjuração

Conjuração é a arte de invocar elementais das Cinco Tempestades. É um processo lento e exige a criação de um círculo de invocação e o uso de sacrifícios apropriados para ganhar o serviço de uma dessas criaturas. Qualquer um pode escolher a perícia Conjuração sem nenhum custo de recarga.

+ `O`{: .fate_font} **Superar:** Conjuração também pode ser usada como uma perícia Conhecimento quando tratar de criaturas da Cinco Tempestades. Também pode ser usada para renovar o vínculo de uma criatura elemental já invocada sem o tempo e esforço gastos na invocação inicial. Isso simplesmente requer uma rolagem de superar contra o nível da criatura — veja o resumo dos elementais nas próximas páginas.
  + Falha: A criatura é imediatamente libertada e irá fugir ou lutar, dependendo da situação e como foi tratada.
  + Empate: O vínculo não é renovado e é encerrado normalmente.
  + Sucesso: Renova o vínculo por uma semana.
  + Sucesso Com Estilo: Renova o vínculo por um mês.
+ `C`{: .fate_font} **Criar Vantagem:** Invocar um elemental é uma forma específica de criar vantagem. Fazer isso exige um círculo de convocação e uma quantidade e tipo de sacrifícios baseados no ser que será invocado. Para uma Cria, um punhado de material relacionado será o bastante, mas um ritual para conjurar um Assessor exigirá muito mais.

Role contra uma dificuldade baseada no tipo de criatura a ser invocada — veja na próxima página.

  + **Falha:** A criatura é invocada, mas se torna livre. Crias e Lacaios tendem a fugir — causando problemas em outros lugares — enquanto Serviçais e Assessores podem se voltar contra o Conjurador, se conseguirem uma oportunidade.
  + **Empate:** O elemental aparece, mas realizará apenas um único serviço simples que tome menos de uma noite.
  + **Sucesso:** O elemental aparece e estará a seu serviço por uma semana.
  + **Sucesso Com Estilo:** O elemental aparece e permanece vinculado por um mês.
+ `A`{: .fate_font} **Atacar:** Conjuração pode ser usada como perícia de ataque contra criaturas conjuradas, com o “dano” contribuindo para o banimento.
+ `D`{: .fate_font} **Defender:** Conjuração pode ser usada na defesa contra ataques realizados por criaturas invocadas. A defesa pode ser reforçada ao permanecer em um círculo, com um valor de +1 para um círculo desenhado às pressas a +4 para um círculo matematicamente perfeito, feito de um metal precioso e gravado com runas antigas de poder.

O nível de Conjuração do personagem é também o limite do número de elementais vinculados consigo ao mesmo tempo. Suas dificuldades somadas não podem exceder o nível de Conjuração, então um Conjurador Ótimo (+4) pode chamar quatro Crias, uma Cria e um Servo ou alguma outra combinação que totalize quatro. É bastante comum — e ruidoso — um Conjurador possuir diversos elementais invocados ao mesmo tempo.

#### Elementais

##### Resumo Dos Elementais

| __Elemental__ | __Nível__   | __Estresse__ | __Consequências__ |
|:-------------:|:-----------:|:------------:|:-----------------:|
| Cria          | Regular     | 0            | Nenhuma           |
| Lacaio        | Razoável    | 0            | -2                |
| Serviçal      | Bom         | 2            | -2/-2             |
| Assessor      | Ótimo       | 3            | -2/-4             |
| Nomeado       | Excepcional | 4            | -2/-4/-6          |

##### Bônus Elementais

| __ELEMENTO (TEMPESTADE)__ | __CRIA__ | __LACAIO, SERVIÇAL OU ASSESSOR__ | __NOMEADO__                               |
|:-------------------------:|:--------:|:--------------------------------:|:-----------------------------------------:|
| Água (Dilúvio)            | Nenhum   | Consequência suave adicional     | Consequência suave adicional, Armadura: 1 |
| Fogo (Inferno)            | Nenhum   | Arma:1                           | Arma:2                                    |
| Gelo (Geleira)            | Nenhum   | Armadura: 1                      | Armadura: 2                               |
| Relâmpago (Trovão)        | Nenhum   | 1 zona de alcance de ataque      | Alcance 2                                 |
| Terra (Terremoto)         | Nenhum   | +2 Estresse                      | +4 Estresse                               |

Para a maior parte das ações, a maioria dos elementais possui apenas uma única perícia: Elemental, cujo nível é igual à sua classificação; portanto uma Cria do Fogo possui a perícia padrão ‘Cria do Fogo: Regular (+1)’. Certos elementais têm outras perícias específicas, mas na ausência dessas um elemental rolará sua perícia padrão ou rolará com uma penalidade de -2 para outras ações.

Todos os elementais possuem a habilidade de fundir-se em seus elementos naturais, recebendo um bônus de +4 em Furtividade desde que alguma quantidade desse elemento esteja presente para que desapareçam dentro dele. Além disso, recebem benefícios baseados em seu elemento como descrito na tabela _Bônus Elementais_.

###### _Cria_

Crias parecem um punhado vivo de seu elemento e possuem pouco  poder e inteligência. No entanto, sua invocação é a mais simples dentre os elementais, além de serem bem adequados para tarefas simples, especialmente as que envolvem Atletismo e Furtividade — eles recebem um bônus de +4 em ambos. São quase inúteis em combate, visto que não possuem caixas de estresse e não podem receber consequências. 

A diferença entre elementos é altamente estética entre as Crias, mas suas variações podem ser bem exóticas. Crias são os elementais mais fáceis de se encontrar na natureza e muitos dos que viveram por muito tempo fora das tempestades acabaram ”se adaptando”, adotando características da fauna e flora locais. Essas crias “nativas” podem ser vinculadas a um conjurador como quaisquer outras.

###### _Lacaio_

Lacaios possuem um corpo elemental do tamanho de um cão e não são mais espertos do que as Crias — muitas vezes são até menos — mas são consideravelmente mais fortes e pacientes. Eles recebem um bônus de +2 em Resistência e em qualquer rolagem de Vigor relacionada ao transporte de cargas. Eles são bem adequados para a realização de tarefas chatas e demoradas, mas não são grandes lutadores, pois não possuem caixas de estresse e podem apenas receber uma consequência suave de -2.

###### _Serviçal_

Serviçais são do tamanho de um humano e por vezes sua forma é levemente similar a um humanoide, com alguns braços e pernas, embora raramente com algo que lembre uma cabeça. Embora não sejam gênios, são inteligentes e capazes de seguir instruções complicadas ou lutar por seus mestres. Servos possuem 2 caixas de estresse e podem receber duas consequências de -2.

###### _Assessor_

Assessores são como a maioria das pessoas imagina um elemental: algum elemento da Tempestade com vida. São maiores que uma pessoa e parecem ser um turbilhão vivo feito de sua Tempestade original. Também são hábeis e inteligentes em combate. Assessores possuem 3 caixas de estresse e podem receber uma consequência de -2 e outra de -4.

### Pactos Elementais

Um Conjurador pode fazer um pacto com um dos poderes da Tempestade. Fazer isso concede poder muito maior sobre os elementais daquele domínio, mas isso vem a um grande custo. O Conjurador é agora um Invocador, limitado a apenas aquele elemento, e agora também responde a um ser de grande poder e motivações questionáveis. Apesar do preço, devido ao poder que deriva desse pacto, nunca há falta de indivíduos que o busquem, mesmo que nem todos sobrevivam ao processo. Tenha cuidado com os Príncipes das Cinco Tempestades.

Mecanicamente, o pacto toma a forma de um aspecto que reflete a negociação. Ele pode ser invocado para ajudar nas invocações e conjurações — bem como para nomeá-las, em certas ocasiões — e pode ser forçado de qualquer forma que sirva ao interesse do outro lado do pacto. Isso pode variar, de estipulações aparentemente aleatórias — como precisar carregar um determinado acessório — a proibições — o Príncipe do Magma odeia banhos! — ou direitos de visita. O jogador pode optar por anular um pacto —perdendo os benefícios e sendo atacado imediata mente por qualquer conjuração que tenha no momento —, mas fazer isso cria um inimigo — e apenas como referência, ___A Inimizade do Príncipe do Trovão___ é um ótimo aspecto para substituir o antigo!

O jogador e o Narrador devem trabalhar nos detalhes do outro lado da negociação. A ideia base é que ela seja feita com o Príncipe ou a Rainha de uma Tempestade, com títulos como “Príncipe do Magma”, “Dama das Geleiras” ou “Condessa do Céus Partidos”, mas as opções são realmente infinitas.

O pacto reduz a recarga do personagem em um. É possível fazer mais de um pacto, mas fazer isso garante que daquele ponto em diante você será o ponto de impacto das nas batalhas entre esses dois Reinos de Tempestade.

#### **Perícia: Invocação**

A perícia Invocação substitui a perícia Conjuração quando um personagem faz um pacto. Ela possui o mesmo nível e funciona de forma semelhante à perícia Conjuração, com as seguintes mudanças:

- O personagem pode apenas invocar criaturas da Tempestade com a    qual tiver um pacto.
- Elementais invocados agora não têm custo.
- Invocações que falharem sempre resultam na fuga da criatura.
- A perícia do personagem é tratada como +4 acima ao determinar     quantos elementais podem ser controlados ao mesmo tempo.
- O personagem agora pode invocar e se vincular a uma criatura elemental nomeada baseada no patrono de seu pacto. A natureza dessa     criatura é parte da identificação do patrono. Criaturas nomeadas são     tipos específicos de criaturas fantásticas — pássaros de fogo, tatus de     relâmpago ou qualquer coisa que o jogador e o Narrador concordarem que seja legal.
- Esses elementais nomeados são de nível Excepcional (+5), possuem    4 caixas de estresse e consequências de -2/-4/-6, assim como seus    bônus elemental — veja a tabela na página anterior.

### Elementais em Combate

Controlar um elemental além do personagem não é muito trabalhoso, mas um Invocador ou Conjurador talentoso pode se aventurar com vários elementais ao seu lado e lidar com todos eles em um combate pode ser bastante complicado de manejar. Por essa razão, um Conjurador pode usar uma variação da regra de Trabalho em Equipe, da seguinte forma:

- O ataque e defesa básicos são determinados pelo elemental mais     poderoso sob o controle do personagem. Ele recebe +1 por estar sob     o controle do personagem.
- Criar vantagem e superar dificuldades ainda usam a perícia do personagem, mas com +1 para cada elemental que não seja uma Cria     e que esteja na luta, se o elemental puder ajudar naquela situação.
- Elementais funcionam como um reserva de consequências para o     jogador. A qualquer momento em que receba dano, ele poderá perder     elementais como se houvesse recebido consequências, de acordo com     os seguintes valores:
  - Lacaio: -2
  - Serviçal: -4
  - Assessor: -6
  - Nomeado: Qualquer valor único

### Variações e Opções

#### Poder Emprestado


Um Invocador com um pacto pode comprar a perícia Tempestades como se fosse um Senhor da Tempestade e tomar parte do poder de seu patrono emprestado. No entanto, há limites para este uso. O invocador não pode usar a perícia Tempestades enquanto uma criatura nomeada estiver invocada e ao usar a perícia ele renuncia o benefício de +4 no número de criaturas invocadas possíveis. O lado positivo é que ele pode sacrificar qualquer elemental que não seja uma Cria para receber +1 em sua rolagem de Tempestades, embora este bônus não seja cumulativo. Esta variação não custa uma recarga adicional.

#### Mestres Das Crias

É possível que alguns conjuradores renunciem os aspectos mais amplos da conjuração em favor de se especializar em Crias, tanto por sua utilidade como para os torneios secretos de Crias que se tornaram tão populares.

Nesse caso, substitua a perícia de Conjuração por uma de Treinamento de Crias, para saber quantas crias um personagem pode controlar, mas isso não permite conjurá-las. Em vez disso, ele poderá domar crias “selvagens”, aquelas que ficaram presas no mundo mortal por tempo suficiente para adotar a forma de alguma criatura. Um Mestre das Crias encontra e captura essas Crias e então as domestica, tornando-as Crias de Batalha que serão treinadas para lutar.

Em um exemplo usando a Regra de Bronze, Crias de Batalha são criaturas Regulares (+1) sem caixas de estresse e consequências ao menos no que diz respeito à sua interação com o mundo, mas entre si são diferenciadas minuciosamente. Ou seja, cada Cria pode ter um conjunto completo de perícias, poderes e habilidade úteis em batalha contra outras crias e, dentro dessas batalhas, essas diferenças são bem importantes. Entretanto, para um observador externo, a mais grandiosa e a mais simples das Crias de Batalha são praticamente a mesma coisa.

##### Treinando Sua Cria De Batalha

Crias de batalha começam com 2 caixas de estresse, uma consequência suave e 4 perícias em Medíocre (+0): Força, Velocidade, Perícia e Resistência (sim, existe uma perícia chamada Perícia. Lide com isso). Essas são, respectivamente, usadas para atacar, superar, criar vantagem e defender durante os combates. Resistência também é a defesa contra ataques.

Crias podem evoluir ao vencer batalhas ou através de treinamento. Exatamente como o avanço ocorre depende da situação, mas cada avanço pode ser usado para:
+ **Melhorar uma perícia.** Todas as perícias podem ser melhoradas para Bom (+3). Uma perícia pode ser elevada a Ótimo (+4) e uma pode atingir Excepcional (+5).
+ **Melhorar a resistência.** Um avanço pode ser gasto para adicionar uma consequência de -2, melhorar uma de -2 para -4 ou melhorar uma de -4 para -6. O máximo de consequências para uma Cria deBatalha é -2/-4-/6.
+ **Adquirir um aprimoramento.** Algumas possibilidades seriam:
  + **Carapaça:** +1 de Armadura.
  + **Carapaça Resistente:** (Requer Carapaça e Resistência Boa): +1 de Armadura.
  + **Ataque de Sopro:** Pode atacar dois alvos de uma vez.

Há muitas outras possibilidades de aprimoramento. Em geral, trate-as como façanhas simples.

Em qualquer situação que não seja um combate com outras crias, ela ainda é considerada como uma cria de nível Regular, sem estresse, sem consequências e sem melhorias — exceto num sentido puramente estético. Isso se aplica independente de quantas melhorias ela tenha recebido.

------

- [« A Arte Sutil](../arte-sutil/)
- [Senhores do Vazio »](../senhores-do-vazio/)
