I"U<h1 id="establish-the-opposition">Establish the Opposition</h1>

<p>You might have already come up with an <abbr title="Non-Player Character">NPC</abbr> or group of NPCs who is/are
responsible for what’s going on when you made up your problem, but if you
haven’t, you need to start putting together the cast of characters who are the
key to answering your story questions. You also need to nail down their
motivations and goals, why they’re standing in opposition to the PCs’ goals,
and what they’re after.</p>

<p>At the very least, you should be able to answer the following questions for
each named <abbr title="Non-Player Character">NPC</abbr> in your scenario:</p>

<ul>
  <li>What does that <abbr title="Non-Player Character">NPC</abbr> need? How can the PCs help her get that, or how are the PCs in the way?</li>
  <li>Why can’t the <abbr title="Non-Player Character">NPC</abbr> get what she needs through legitimate means? (In other words, why is this need contributing to a problem?)</li>
  <li>Why can’t she be ignored?</li>
</ul>

<p>Wherever you can, try and consolidate NPCs so that you don’t have too many
characters to keep track of. If one of your opposition NPCs is serving only
one purpose in your scenario, consider getting rid of him and folding his role
together with another <abbr title="Non-Player Character">NPC</abbr>. This not only reduces your workload, but it also
allows you to develop each <abbr title="Non-Player Character">NPC</abbr>’s personality a bit more, making him more
multi-dimensional as you reconcile his whole set of motives.</p>

<p>For each <abbr title="Non-Player Character">NPC</abbr> that you have, decide whether you need to make them a supporting
or main. Stat them up according to the guidelines given in <a href="../../fate-core/running-game">Running the Game
</a>.</p>

<blockquote>
  <p>An Arcane Conspiracy: Opposition</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Amanda looks over the story questions and thinks of NPCs she’ll need in
order to answer them. She makes a list of the obvious suspects.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <ul>
    <li>Cynere’s mysterious employer (not appearing)</li>
  </ul>
</blockquote>

<blockquote>
  <ul>
    <li>The chief arbiter for the Collegia Arcana (supporting)</li>
  </ul>
</blockquote>

<blockquote>
  <ul>
    <li>Cynere’s competitor for the Jewel (supporting)</li>
  </ul>
</blockquote>

<blockquote>
  <ul>
    <li>A barrister who isn’t part of the conspiracy (supporting)</li>
  </ul>
</blockquote>

<blockquote>
  <ul>
    <li>A corrupt barrister, and the one that Zird’s rivals want to set him up
with (supporting)</li>
  </ul>
</blockquote>

<blockquote>
  <ul>
    <li>The Collegia wizard who engineered the conspiracy to bring Zird down
(main)</li>
  </ul>
</blockquote>

<blockquote>

</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>That’s six NPCs, four supporting, one main, and one that isn’t going to be
in the scenario—she really doesn’t want to drop any details on who’s hiring
Cynere yet. She also doesn’t really want to keep track of five NPCs, so she
starts looking for opportunities to consolidate.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>One pairing that immediately strikes her is making Cynere’s competitor and
the neutral barrister into the same person, whom she names Anna. Anna might
not be involved in this conspiracy, but clearly, there’s a more complicated
motive at work. What’s going on with her? Amanda ultimately decides that
Anna’s motives are beneficent; she’s secreting the Jewel away to keep it out
of the hands of more corrupt elements in the Collegia’s infrastructure. She
doesn’t know anything about Cynere and will mistake her for an agent of those
corrupt elements until they clear the air.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Then she decides that the chief arbiter and the architect of the conspiracy
are the same—he didn’t trust anyone else to stick the final nail in Zird’s
coffin, so he made sure he’d be appointed arbiter over the trial. Amanda likes
this because his political power makes him a formidable opponent to
investigate and gives him a powerful lackey in the form of the corrupt
barrister. But why does he have it in for Zird in the first place?</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>She further decides that his motives aren’t personal, but he’s getting ready
to do some stuff that will rock the foundations of the Collegia, and he knows
that as a misfit in that organization, Zird is one of the most likely
candidates to resist him. So it’s basically a preemptive strike.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>As for the corrupt barrister, the first thing that comes to mind is a
pathetic, sniveling toady who is totally in the arbiter’s pocket. But she
wants to add a measure of depth to him, so she also decides that the arbiter
has blackmail material on him, which helps to ensure his loyalty. She doesn’t
know what that info is yet, but she’s hoping that nosy PCs will help her
figure it out through a story detail later.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>She names the arbiter Lanthus, and the corrupt barrister Pight. Now she has
her NPCs, and she goes about making their sheets.</p>
</blockquote>

<h3 id="advantages-can-save-you-work">Advantages Can Save You Work</h3>

<p>When you’re establishing your NPCs for your scenario, you don’t have to have
everything set in stone when you get to the table—whatever you don’t know, you
can always establish by letting the advantages the players create become the
NPCs’ aspects.</p>

<ul>
  <li><a href="/fate-srd/fate-core/ask-story-questions">« Ask Story Questions</a></li>
  <li><a href="/fate-srd/fate-core/set-first-scene">Set Up The First Scene »</a></li>
</ul>

:ET