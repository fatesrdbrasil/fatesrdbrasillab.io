---
title: Fate Condensado
layout: default
---
# Fate Condensado em Português

## ***Fate Condensado*** SRD - Documento de Referência de ***Fate Condensado***

© 2020 Evil Hat Productions, Ink Head Publishing. Versão 1.0

O projeto da tradução foi concluído em 19/05/2021 e está liberado para criação de SRDs na língua portuguesa. O texto abaixo é obrigatório para ser inserido no seu site ou blog antes da leitura do SRD.
     
> Esta obra é baseada em Fate Condensado, encontrado em https://www.facebook.com/inkheadpublishing, traduzido pela comunidade e fans, desenvolvido e editado por Estevan Fernandes Queiroz e licenciado para uso sob licença Creative Commons Atribuição 4.0 Internacional (http://creativecommons.org/licenses/by/4.0/). Versão original: Fate Condensed© Evil Hat Productions, LLC. Documento de Referência do Sistema produzido por Estevan Fernandes Queiroz.
>
> Fate Condensed ©2020 Evil Hat Productions, LLC. Fate™ is a trademark of Evil Hat Productions, LLC. The Fate Core font is © Evil Hat Productions, LLC and is used with permission. The Four Actions icons were designed by Jeremy Keller. 
>
> This work is based on Fate Condensed (found at http://www.faterpg.com/), a product of Evil Hat Productions, LLC, developed, authored, and edited by PK Sullivan, Lara Turner, Leonard Balsera, Fred Hicks, Richard Bellingham, Robert Hanz, Ryan Macklin, and Sophie Lagacé, and licensed for our use under the Creative Commons Attribution 3.0 Unported license (http://creativecommons.org/licenses/by/3.0/).


## Índice

+ [Início](#fate-condensado-em-portugues)
  + [Objetivo e História do Projeto](#objetivo-e-história-do-projeto)
  + [Agradecimentos](#agradecimentos)
+ [Introdução](introducao/)
  + [O que eu preciso para jogar?](introducao/#o-que-eu-preciso-para-jogar)
	+ [Para Veteranos: mudanças do Fate Básico](introducao/#para-veteranos-mudanças-do-fate-básico)
+ [Começando](comecando/)
  + [Defina Seu Cenário](comecando/#defina-seu-cenário)
  + [Crie Seus Personagens](comecando/#crie-seus-personagens)
  + [Quem É Você?](comecando/#quem-é-você)
  + [Aspectos](comecando/#aspectos)
	+ [Conceito](comecando/#conceito)
	+ [Dificuldade](comecando/#dificuldade)
	+ [Relacionamento](comecando/#relacionamento)
	+ [Aspectos Livres](comecando/#aspectos-livres)
  + [Perícias](comecando/#perícias)
	+ [A Escala de Adjetivos](comecando/#a-escala-de-adjetivos)
	+ [Lista De Perícias](comecando/#lista-de-perícias)
	+ [Listas Alternativas de Perícias](comecando/#listas-alternativas-de-perícias)
  + [Recarga](comecando/#recarga)
  + [Façanhas](comecando/#façanhas)
	+ [Escrevendo Façanhas](comecando/#escrevendo-façanhas)
  + [Estresse e Consequências](comecando/#estresse-e-consequências)
	+ [Peraí, não é isso o que eu me lembro!](comecando/#peraí-não-é-isso-o-que-eu-me-lembro)
  + [Toques Finais](comecando/#toques-finais)
+ [Agindo e Rolando os Dados](agindo-rolando-dados/)
  + [Dificuldade e Oposição](agindo-rolando-dados/#dificuldade-e-oposição)
  + [Modificando os Dados](agindo-rolando-dados/#modificando-os-dados)
	+ [Invocando aspectos](agindo-rolando-dados/#invocando-aspectos)
	+ [Usando Façanhas](agindo-rolando-dados/#usando-façanhas)
  + [Resoluções](agindo-rolando-dados/#resoluções)
	+ [Falha](agindo-rolando-dados/#falha)
		+ [Falha simples](agindo-rolando-dados/#falha-simples)
		+ [Sucesso a um custo maior](agindo-rolando-dados/#sucesso-a-um-custo-maior)
		+ [Receber um Ataque](agindo-rolando-dados/#receber-um-ataque)
	+ [Empate](agindo-rolando-dados/#empate)
		+ [Sucesso a um custo menor](agindo-rolando-dados/#sucesso-a-um-custo-menor)
		+ [Sucesso Parcial](agindo-rolando-dados/#sucesso-parcial)
	+ [Sucesso](agindo-rolando-dados/#sucesso)
	+ [Sucesso com estilo](agindo-rolando-dados/#sucesso-com-estilo)
  + [Ações](agindo-rolando-dados/#ações)
	+ [Superar](agindo-rolando-dados/#superar)
	+ [Criar Vantagem](agindo-rolando-dados/#criar-vantagem)
	+ [Atacar](agindo-rolando-dados/#atacar)
	+ [Defender](agindo-rolando-dados/#defender)
		+ [Quais perícias podem ser usadas para Atacar e Defender?](agindo-rolando-dados/#quais-perícias-podem-ser-usadas-para-atacar-e-defender)
+ [Aspectos E Pontos De Destino](aspectos-pontos-destino/)
  + [Aspectos são sempre verdades](aspectos-pontos-destino/#aspectos-são-sempre-verdades)
  + [Que tipo de aspectos existem?](aspectos-pontos-destino/#que-tipo-de-aspectos-existem)
	+ [Aspectos de personagem](aspectos-pontos-destino/#aspectos-de-personagem)
	+ [Aspectos de Situação](aspectos-pontos-destino/#aspectos-de-situação)
	+ [Consequências](aspectos-pontos-destino/#consequências)
	+ [Impulsos](aspectos-pontos-destino/#impulsos)
  + [O que posso fazer com os Aspectos?](aspectos-pontos-destino/#o-que-posso-fazer-com-os-aspectos)
	+ [Ganhando Pontos de Destino](aspectos-pontos-destino/#ganhando-pontos-de-destino)
	+ [Invocações](aspectos-pontos-destino/#invocações)
		+ [O truque das reticências](aspectos-pontos-destino/#o-truque-das-reticências)
		+ [Invocações Hostis](aspectos-pontos-destino/#invocações-hostis)
		+ [Invocações para Declarar Detalhes Narrativos](aspectos-pontos-destino/#invocações-para-declarar-detalhes-narrativos)
	+ [Forçando](aspectos-pontos-destino/#forçando)
		+ [Forçar aspectos gera complicações, não impedimentos](aspectos-pontos-destino/#forçar-aspectos-gera-complicações-não-impedimentos)
		+ [Eventos e Decisões](aspectos-pontos-destino/#eventos-e-decisões)
		+ [Invocações Hostis ou Forçar Aspectos?](aspectos-pontos-destino/#invocações-hostis-ou-forçar-aspectos)
	+ [Como posso adicionar ou remover Aspectos?](aspectos-pontos-destino/#como-posso-adicionar-ou-remover-aspectos)
	+ [Outros tipos de Aspectos](aspectos-pontos-destino/#outros-tipos-de-aspectos)
+ [Desafios, Disputas e Conflitos](desafios-disputas-conflitos/)
  + [Definindo as Cenas](desafios-disputas-conflitos/#definindo-as-cenas)
  + [Zonas](desafios-disputas-conflitos/#zonas)
	+ [Aspectos de Situação](desafios-disputas-conflitos/#aspectos-de-situação)
		+ [Invocações gratuitas nos aspectos de cena?](desafios-disputas-conflitos/#invocações-gratuitas-nos-aspectos-de-cena)
		+ [Aspectos de Zonas](desafios-disputas-conflitos/#aspectos-de-zonas)
	+ [Ordem de turnos](desafios-disputas-conflitos/#ordens-de-turnos)
  + [Trabalho em Equipe](desafios-disputas-conflitos/#trabalho-em-equipe)
  + [Desafios](desafios-disputas-conflitos/#desafios)
  + [Disputas](desafios-disputas-conflitos/#disputas)
	+ [Criando Vantagens Durante uma Disputa](desafios-disputas-conflitos/#criando-vantagens-durante-uma-disputa)
  + [Conflitos](desafios-disputas-conflitos/#conflitos)
	+ [Recebendo dano](desafios-disputas-conflitos/#recebendo-dano)
		+ [Estresse](desafios-disputas-conflitos/#estresse)
		+ [Consequências](desafios-disputas-conflitos/#consequências)
		+ [Sendo Tirado de Ação](desafios-disputas-conflitos/#sendo-tirado-de-ação)
		+ [Concedendo](desafios-disputas-conflitos/#concedendo)
	+ [Terminando um Conflito](desafios-disputas-conflitos/#terminando-um-conflito)
	+ [Recuperando-se de Conflitos](desafios-disputas-conflitos/#recuperando-se-de-conflitos)
+ [Avanço](avanco/)
  + [Marcos](avanco/#marcos)
  + [Progressos](avanco/#progressos)
	+ [Aprimorando os níveis das perícias](avanco/#aprimorando-os-níveis-das-perícias)
  + [Sessões e Arcos](avanco/#sessões-e-arcos)
+ [Sendo o Narrador](sendo-narrador/)
  + [Definindo Dificuldade e Oposição](sendo-narrador/#definindo-dificuldade-e-oposição)
  + [Personagens do Narrador (PdN)](sendo-narrador/#personagens-do-narrador-pdn)
	+ [PdN Principais](sendo-narrador/#pdn-principais)
	+ [PdN Menores](sendo-narrador/#pdn-menores)
	+ [Monstros, Grandes Males e Outras Ameaças](sendo-narrador/#monstros-grandes-males-e-outras-ameaças)
  + [Seus pontos de destino](sendo-narrador/#seus-pontos-de-destino)
  + [Ferramentas de Segurança](sendo-narrador/#ferramentas-de-segurança)
+ [Regras opcionais](regras-opcionais/)
  + [Condições](regras-opcionais/#condições)
	+ [Separando ainda mais as Condições](regras-opcionais/#separando-ainda-mais-as-condições)
	+ [Outras Versões de Condições](regras-opcionais/#outras-versões-de-condições)
  + [Mudando a lista de perícias](regras-opcionais/#mudando-a-lista-de-perícias)
	+ [Alternativas para a pirâmide](regras-opcionais/#alternativas-para-a-pirâmide)
  + [Criação de Personagem Durante o Jogo](regras-opcionais/#criação-de-personagem-durante-o-jogo)
  + [Contagem Regressiva](regras-opcionais/#contagem-regressiva)
  + [Consequências Extremas](regras-opcionais/#consequências-extremas)
  + [Disputas mais rápidas](regras-opcionais/#disputas-mais-rápidas)
  + [Defesa Total](regras-opcionais/#defesa-total)
  + [Obstáculos](regras-opcionais/#obstáculos)
	+ [Perigos](regras-opcionais/#perigos)
	+ [Bloqueios](regras-opcionais/#bloqueios)
	+ [Distrações](regras-opcionais/#distrações)
	+ [Exemplos de Obstáculos](regras-opcionais/#exemplos-de-obstáculos)
		+ [Perigos](regras-opcionais/#perigos-1)
		+ [Bloqueios](regras-opcionais/#bloqueios-1)
		+ [Distrações](regras-opcionais/#distrações-1)
  + [Escala](regras-opcionais/#escala)
	+ [Aspectos e Escala](regras-opcionais/#aspectos-e-escala)
		+ [A Escala se aplica ao criar uma vantagem sobrenatural?](regras-opcionais/#a-escala-se-aplica-ao-criar-uma-vantagem-sobrenatural)
  + [Tensões de Tempo](regras-opcionais/#tensões-de-tempo)
  + [Maneiras de quebrar as regras para o Grande Mal](regras-opcionais/#maneiras-de-quebrar-as-regras-para-o-grande-mal)
	+ [Imunidade por Desafio ou Disputa](regras-opcionais/#imunidade-por-desafio-ou-disputa)
	+ [Armadura descartável de lacaios](regras-opcionais/#armadura-descartável-de-lacaios)
	+ [Revelar a Forma Verdadeira](regras-opcionais/#revelar-a-forma-verdadeira)
	+ [Aumentando a Escala](regras-opcionais/#aumentando-a-escala)
	+ [Bônus Solo](regras-opcionais/#bônus-solo)
	+ [A ameaça é um mapa (ou uma colméia de personagens)](regras-opcionais/#a-ameaça-é-um-mapa-ou-uma-colméia-de-personagens)
  + [Maneiras de Lidar com Múltiplos Alvos](regras-opcionais/#maneiras-de-lidar-com-múltiplos-alvos)
  + [Potência de Armas e Armaduras](regras-opcionais/#potência-de-armas-e-armaduras)
+ [Que versão é essa?](versao/)
  + [O que veio antes](versao/#o-que-veio-antes)
  + [Licenciamento](versao/#licenciamento)

## Objetivo e História do Projeto:

Esse é um projeto coletivo, sem fins lucrativos, feito por fãs da Comunidade Movimento Fate Brasil no Facebook e patrocinado pela Ink Head Publishing com autorização da editora Pluma Press, atual detentora dos direitos de licenciamento de Fate no Brasil. O objetivo é criar um material com participação voluntária de fans do sistema e posteriormente distribuído de forma gratuita através de link para download. As regras traduzidas em PT-BR serão atualizadas também no SRD oficial da comunidade Brasileira: [*https://fatesrdbrasil.gitlab.io/fate-srd/*](https://fatesrdbrasil.gitlab.io/fate-srd/). Você é livre para criar um SRD no seu site ou blog mantendo os dizeres obrigatórios no início desse projeto.

Agradecemos a oportunidade ao Fábio Silva da Editora Pluma Press e também à Editora Solar pelos termos e base de tradução original do Fate Core 2ed. 

![](/assets/img/10000000000002D90000013BEE887F57E1D2E94B.jpg)    
![](/assets/img/1000000000000190000001908F7FE41D9CB0B914.jpg)      
![](/assets/img/100000000000026E000000EA0BD4EC4CB69926A9.jpg)   

Assim como o texto do Fate Condensed original em língua inglesa, esse projeto trabalhará com uma atribuição do Creative Commons: Creative Commons Attribution 4.0 International (CC BY 4.0), que permite a utilização e replicação apenas do texto e não dos elementos gráficos, indicando os devidos créditos aos criadores. 

![](/assets/img/10000201000002D900000090AFB8FCE7FA978D52.png)

## Agradecimentos 

### Etapa 1 - Tradução:
 + Pedro Gustavo
 + Cláudio “Cleedee” Torcato
 + Fábio Emilio Costa
 + Estevan Queiroz
 + Lu Cicerone Cavalheiro
 + Daniel Cruz e Silva ”RyuDadas”
 + Filipe Dalmatti Lima
 + Thiago Almeida
 + Luciano Campos
 + Adeyvison Siqueira
 + Pedro Acacito. 

### Etapa 2 Revisão e Localização:

+ Lu Cicerone Cavalheiro
+ Luiz Borges Gomide
+ Wesley Ramos
+ Estevan Queiroz.

### Etapa 3 Refinamento Tradução e Portugues:

+ Lu Cicerone Cavalheiro
+ Lucas Peixoto
+ Wesley Ramos
+ Estevan Queiroz.

------

- [Introdução »](introducao/)
