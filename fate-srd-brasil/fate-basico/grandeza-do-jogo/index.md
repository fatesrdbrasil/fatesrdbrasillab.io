---
title: A Grandeza do Jogo
layout: default
---

## A Grandeza do Jogo

**Decida o quão épicas ou pessoais serão suas histórias.**

O cenário pode ser limitado ou vasto, mas onde quer que suas histórias se passem, determine a grandeza de seu jogo.

Em um jogo de pequena escala, os personagens lidam com os problemas em uma cidade ou região, sem fazer grandes viagens e os problemas são locais. Um cenário vasto envolve lidar com problemas mundiais, que afetam uma civilização ou até mesmo uma galáxia, se se o estilo do seu jogo suporta esse tipo de coisa (às vezes um jogo de pequena escala se transformará em uma campanha vasta, como você talvez já tenha visto ocorrer em séries de livros ou televisão).

> Amanda gosta da ideia de “uma garota com partes robóticas e um homem-lagosta”, e acha que isso cairá bem com aventuras grandes, com viagens pela galáxia, problemas com guildas espaciais e impérios malignos.

- [« Adaptando O Cenário Ao Fate](../adaptando-cenario-fate/)
- [Questões de Ambientação »](../questoes-de-ambientacao/)
