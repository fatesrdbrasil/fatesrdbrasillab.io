I"�8<p>Fate System Toolkit</p>

<h1 id="the-subtle-art">The Subtle Art</h1>

<h2 id="design-notes">Design Notes</h2>

<p>Mechanically speaking, this system is an elaboration of creating an advantage.
That is, it simply expands the scope of when, where, and what kind of
advantages can be created. Because it’s much more low-key than other magic
types, it doesn’t answer a number of the big questions. This makes it a magic
system better suited to a game that is not itself primarily magical. In fact,
it’s best suited to games where there is some question of whether magic is
real or not, though in other games it may be a way to represent a “lesser”
path of magic.</p>

<h2 id="description">Description</h2>

<p>When you imagine magical societies, the first thing to spring to mind may be
ancient lodges and secret orders. Certainly those exist, but they don’t have
much magic left in them—their own success tamped it out. Consider that the
benefits of magic are ephemeral, while the benefits of collaboration,
conspiracy, and partnership are concrete. For the kind of important,
successful folks who meet in smoky rooms with dark woods and deep carpets, the
magic is just window dressing. They don’t need to believe anymore, and in most
cases, they haven’t needed to for a long time.</p>

<p>There are exceptions. Most of these established organizations started out full
of belief and strong intentions, but that belief faded under the weight of
their own success. It’s easy to believe that your prosperity charms are
helping your stock picks when you have no idea what you’re doing, but as you
succeed—and learn—then the tendency is to start attributing your successes to
your talent and smart picks. That means, as you get better, magic helps you
less.</p>

<p>As a result, most practicing magical groups are much more informal. Networks
of friends, friends of friends, or loosely connected strangers sharing a
common interest. Meetings mostly revolve around drinking, hook-ups, and couch
surfing in cheap rented apartments.</p>

<p>The simple reality is that magic has its strongest roots in the rootless.
People who are smart and capable, but lack direction or any real purpose,
gravitate to magic for the quick fix solution it represents. It’s an
underground, and for some, it’s the best weapon available.</p>

<h2 id="magic-and-reality">Magic and Reality</h2>

<p>What’s important is that there is no “real world” proof that this magic works.
The introduction of these aspects does not change reality in any real,
repeatable way. Certainly, they tilt the scale a bit, but they do so within
the realm of reasonable outcomes. To an outside observer, this “magic” looks a
lot like confirmation bias. If you put a curse on someone and something bad
happens to them, then it’s easy to take credit for it, but a cynic will note
that bad things happen to people all the time.</p>

<p>This can be rough, and as a result, believers tend to cluster together. They
form subcultures where they can nerd out about what they’ve done, swap tips,
and generally reinforce their belief that this is something that matters. In
many ways, these groups are more important than the magic itself, but they are
not what one might expect.</p>

<h2 id="the-30-second-version">The 30-Second Version</h2>

<p>Don’t want to read it all? Just do this:</p>

<ul>
  <li>Buy a skill called “Magic.”</li>
  <li>Take a half hour in a dark room with someone’s name, a voodoo doll, or similar accouterments, and make a Magic roll to create an advantage against the target of the spell.</li>
  <li>Put an aspect appropriate to a curse or blessing on the target. It lasts for three days, seven if you succeed with style.</li>
</ul>

<h2 id="mechanics">Mechanics</h2>

<p>This system adds one skill: Magic. Its description follows.</p>

<h2 id="skill-magic">Skill: Magic</h2>

<p>Magic is the skill of placing blessings and curses upon a person or place.
While the skill itself is generic, its specific manifestations are not. A
practitioner must have a set of rules and trappings that they follow to use
magic. Those rules may be based on a real-world practice, be totally made up,
or anywhere in between, but they must be consistent and they always demand
time, effort, and ritual. There are other limitations listed below.</p>

<p><span class="fate_font">O</span> <strong>Overcome</strong>: There are few useful obstacles that
magic may overcome, though many practitioners think otherwise. It’s a common
misperception that the Magic skill can be used to “detect” magical workings,
but this is really no more reliable than guessing.</p>

<p>The one concrete use for overcome is to overcome the skepticism of others. The
Magic skill also represents how well you “sell” the idea of magic, or at least
your belief. This works like a very specific sort of use of the Deceive
skill—even if the character doesn’t feel he’s deceiving anyone.</p>

<p><span class="fate_font">C</span> <strong>Create an Advantage</strong>: The primary activity with
the Magic skill is creating an advantage. Assuming a single target—a person,
or a thing perhaps as large as a house—about a half hour’s time, and the
appropriate ritual trappings, the roll is made against a difficulty of
Average. So long as the character gets a success, then the target gains the
aspect of the blessing or curse—see below for details—for three days and three
nights. Further modifications follow:</p>

<ul>
  <li>If the target is not present, then the difficulty is increased from between +1 to +3. +3 if the target is merely named, +1 if a powerful symbolic tie to the target is present—their blood, a treasured possession—and if it’s not clearly either, then a +2 is appropriate.</li>
  <li>If the target is large—a small group of less than a dozen or a large place like an office building or park—difficulty increases by +3. That’s the maximum size that a spell can actually work at, though most practitioners are unaware of this, and every year, hours of magic get wasted targeting the GOP, the Dallas Cowboys, and hipsters.</li>
  <li>Some spells have a secondary target, such as a spell that makes your boss mad at someone. The absence of that secondary target similarly impacts the difficulty—+0 if present, +3 if you only have a name, as above. The one qualifier is that if the secondary target can be made to accept some token of the spell—a potion, a trinket—then they are effectively “present”. Such tokens must be used within three days.</li>
  <li>Success with style extends the duration to a week.</li>
  <li>No target can be the subject of more than one spell at a time. The newest spell replaces the existing ones.</li>
  <li>Some blessings and curses have their own additional modifiers.</li>
  <li>A spell on an area effectively creates a scene aspect that can be used normally by anyone in the location.</li>
</ul>

<p><span class="fate_font">A</span> <strong>Attack</strong>: There is no such thing as a magical
attack.</p>

<p><span class="fate_font">D</span> <strong>Defend</strong>: There is no such thing as a magical
defense.</p>

<h3 id="spells">Spells</h3>

<p>Aspects put on a target are generally referred to as blessings or curses,
depending on their intended effect, but collectively, they are all considered
spells. These are not an open-ended list—there are a fixed set of spells, and
the knowledge of them is the currency of the magical community. Spells are
complicated enough that they are very difficult to commit to memory and still
get exactly right, so they are kept in notebooks, databases, and other
archives. Poaching another magician’s spellbook can be informative, but it can
also be about as useful as their organic chemistry notes—even if they haven’t
actively obscured them, they can be very idiosyncratic to understand. And, of
course, there’s no real way to distinguish between a spell that’s a dud and a
real one.</p>

<p>For clarity, the <strong>target **of a spell is the person, place or thing it’s
being cast on. Sometimes a spell will also have a **subject</strong>, a person, place
or thing which will be the focus of the spell’s effect on the target. For
example, a love spell to make Jake fall for Andy would be cast on Jake (the
target) focused on Andy (the subject).</p>

<p><strong>Annoyance</strong>: The target rubs people the wrong way. If the spell has a subject, then the target of the spell is more easily annoyed by that subject.</p>

<p><strong>Charisma</strong>: While related to love, this turns it on its head by improving the target’s general presence and demeanor. It’s sometimes a subject of ridicule—specifically, ridiculing those who would need such a spell—but it sees a lot of quiet use.</p>

<p><strong>Clarity</strong>: Popular among those who fancy themselves sophisticated magi, for many this spell is their morning cup of coffee, sharpening their thoughts and senses. It’s also a popular “counterspell,” used to remove curses.</p>

<p><strong>Clumsiness</strong>: You know those days where you dropped a glass, spilled your coffee in your lap, and ripped your shirt on a latch? This makes for that kind of day.</p>

<p><strong>Confusion</strong>: People tend to misunderstand the target—or get easily lost if it’s a place.</p>

<p><strong>Love</strong>: One of the most well known but also most contentious spells, especially when used with a subject. Without a subject, it simply makes the target more friendly towards the world, but with a subject, it inclines the subject toward the target. A lot of people view this as skeezy at best, and date rape at worst. It’s a touchy topic, and a number of magicians get around this by explicitly casting dud spells.</p>

<p><strong>Health</strong>: The magical equivalent of fizzy tablets with vitamin C and zinc.</p>

<p><strong>Luck</strong>: This is the most common spell in circulation, and it can take the form of good or bad luck.</p>

<p><strong>Obscurity</strong>: The target is easily overlooked—by the subject, if appropriate. Whether this is a blessing or a curse depends a lot on your perspective.</p>

<p><strong>Prosperity</strong>: Another popular blessing, financial things fall the target’s way. It’s rare that this turns into a large windfall, but it can show up as a loan extension or a free beer.</p>

<p><strong>Rage</strong>: Small things annoy the target more than usual, as if they’d woken up on the wrong side of the bed. If the spell has a subject, then the target of the spell is more easily enraged by that subject.</p>

<p><strong>Safety</strong>: Keeps the target—or area—safer than it would be.</p>

<p>These are not all the spells available, but they should provide some insight
into the tone of any additions.</p>

<h3 id="magic-stunts">Magic Stunts</h3>

<p><strong>By Rote</strong>: You may pick three spells that you know well enough that you don’t need to consult your notes to cast.</p>

<p><strong>Evil Eye</strong>: You can attempt to put <span class="aspect">Bad Luck</span> on a target with nothing more than an obvious gesture. This lasts only a day.</p>

<p><strong>Interior Decorator</strong>: You may call it <em>feng shui</em> on your invoices, but it’s all just decor. If you put a spell on a place, you may arrange the furniture and decorations just so. If you do, the effect lasts for up to a season—or until someone rearranges the furnishings.</p>

<h2 id="variations-and-options">Variations and Options</h2>

<h3 id="but-does-it-work">But Does It Work?</h3>

<p>It is entirely possible that this “system” is a lie. Magic doesn’t actually do
anything, and it’s all wish fulfillment and confirmation bias. A GM could even
pull this on a player, by quietly not accounting for the aspects they appear
to create. That is, by and large, a terrible idea. It really hoses the player
and undercuts their concept—unless they also like the idea of it being fake.</p>

<p>If you want to emphasize this idea—even if you don’t want to embrace it
fully—then things that make the magic impossible to prove make great compels.</p>

<h3 id="jazzing-it-up">Jazzing It Up</h3>

<p>It’s also possible for this to be a more overtly magical system. In that case
you can introduce odd, improbable, or super weird effects. This will require
expanding the spell list to include more concrete things like “food rots when
you touch it,” and it also makes spells something that can be concretely
perceived by the magic skill. In this case, the duration of effects should be
extended to a lunar month.</p>

<h3 id="combat-curses">Combat Curses</h3>

<p>Assuming a more overt style of magic, a variant on this would allow for
“combat casting” of blessings and curses. This is very different from the
traditional image of the mage casting lightning bolts, but it suits lower
magic settings very well. In this case, spells can be cast on any target in
sight, and by and large, this allows for colorful create an advantage effects.</p>

<p>By default, these must be invisible effects, but can still cause bad choices
to be made, weapons to miss and so on. However, if the GM deems it
appropriate, then certain color may be allowed to make these overtly
magical—such as a fire priest creating fiery advantages.</p>

<h4 id="wizards-duels">Wizards’ Duels</h4>

<p>If combat curses are supported, then so are wizards’ duels. A wizards’ duel
occurs when two wizards meet and choose to lock eyes, entering battle, which
uses Magic in lieu of Fight and inflicts mental damage until one or the other
is taken out. To an external observer, all that happens is they lock eyes,
then one collapses—possibly dead, depending on the decision of the victor. To
the two wizards, the battle can take any form.</p>

<p>Sometimes, beings of greater power can be drawn into a duel by mortal magic.
This requires some preparation on the part of the human practitioner, such as
the creation of a focus object. These clashes still occur mostly in the ether,
but they might involve a flashier exchange of energies or other effect. In
such a case, if the greater power loses, the result is rarely fatal, but the
being may carry a consequence forward.</p>

<ul>
  <li><a href="/fate-srd/fate-system-toolkit/six-viziers">« The Six Viziers</a></li>
  <li><a href="/fate-srd/fate-system-toolkit/voidcallers">Voidcallers »</a></li>
</ul>

:ET