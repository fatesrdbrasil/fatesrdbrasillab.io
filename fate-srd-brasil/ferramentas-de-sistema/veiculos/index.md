---
title: Veículos
layout: default
---


## Veículos

Em um jogo que gira em torno de atividades pessoais, veículos ocupam um lugar peculiar. Eles estendem as capacidades dos personagens como ferramentas e armas, mas são algo separado do personagem, como aliados e recursos. É algo que fácil de ser esquecido, mas que pode ser essencial, dependendo das prioridades do jogador ou da campanha. Ao descrever veículos, esta seção geralmente assume que são carros ou caminhões, mas muitas dessas ideias podem ser facilmente usadas para cavalos, carruagens, espaçonaves e assim por diante.

### Veículos Incidentais

Em muitos jogos, os veículos são apenas incidentais no jogo. Isto é, aparecem de acordo com a necessidade, mas caso contrário não recebem muita atenção. Nesse caso, veículos são frequentemente usados apenas para permitir rolagens da perícia Condução. Quando os vilões estão fugindo em um carro e você entra em um para persegui-lo, toda a situação vai depender apenas da perícia.

Se houver a necessidade de diferenciar os veículos dentro deste contexto, isso normalmente deve ser feito através de aspectos. Um veículo normalmente terá entre um e três aspectos, cujos detalhes dependerão bastante do interesse de seus jogadores em carros. Aspectos como ___Grande___, ___Rápido___, ___De Trilha___ ou ___Lata Velha___ são totalmente válidos, bem como ___Motor V8___, ___Cambagem Negativa___, ___Transmissão de Cinco Marchas___, ___Injeção Eletrônica___.

Para a maioria dos jogos isso é o bastante, mas em jogos onde dirigir é crucial há uma boa chance desses veículos acabarem se tornando mais ou menos descartáveis.

### Veículos Pessoais

Normalmente um veículo pessoal será representado por um aspecto, mas detalhes além disso dependem muito do jogo. Um jogo mais realista talvez possua apenas um veículo pessoal, como um carro esporte de detetive, mas supercarros modificados cheios de aparatos podem ser mais adequados para outros tipos de jogos.

A regra básica para veículos precisa ser um pouco mais complexa do que a regra para veículos incidentais e, para carros mais complexos, extras se tornam mais apropriados.

### Reparos

Um item representado por um aspecto normalmente não pode ser destruído, porém, apesar disso pode ser um pouco difícil de acreditar em um veículo indestrutível. Como regra geral, permita que um veículo pessoal sofra dano normalmente, mas diga que o dano foi reparado sem problemas entre as sessões. 

A não ser, claro, que o jogador queira lidar com o dano. Possuir um carro que precise de reparo é ótimo como enfoque de cena e como motivador de ação de vez em quando — talvez ele precise de peças específicas. Se um jogador opta por lidar com o carro como avariado, então a primeira vez na sessão em que ele mencionar os reparos necessários - como puxar conversa enquanto trabalha no motor — isso na verdade é forçar o aspecto, o que concede um ponto de destino ao jogador.

### Veículo de Grupo

Uma ideia frequentemente usada na ficção e em jogos eletrônicos é um veículo comunitário, como algum tipo de nave, van, carro ou algo assim, mas que serve como transporte em grupo e muitas vezes como uma base de operações móvel.

Uma forma fácil de fazer isso é tornar o veículo em questão um aspecto que todos no grupo possuam — ou ao menos todos que tenham algum vínculo com o veículo. Fazer isso torna evidente a importância do veículo no jogo.

Também é possível seguir uma abordagem mais sutil, onde cada um dos personagens possui um aspecto que reflete a sua relação com o veículo — uma capitã de uma espaçonave e seu engenheiro podem possuir perspectivas bem diferentes sobre a função de sua nave.

Seja qual for o caso, um veículo de grupo pode possuir aspectos como um veículo comum, mas é mais flexível do ponto de vista do que esses aspectos deveriam ser. Cada aspecto de jogador que seja relacionado ao veículo permite ao jogador incluir um aspecto no veículo. Isso torna a construção mais orgânica e cria níveis de investimento diferentes de acordo com o interesse de cada personagem.

### Regras Rápidas para Veículos

#### **Diferenças Entre Veículos**

Eis uma sequência hierárquica simplificada de velocidade:

- A pé
- Bicicleta/Cavalo
- Carro/Motocicleta
- Helicóptero
- Avião

Quando uma perseguição envolve uma diferença de velocidade entre veículos, o mais rápido recebe um número de invocações grátis dos aspectos do veículo igual à diferença os níveis de velocidade. A diferença pode ser atenuada pelas circunstâncias — alguém a pé ou de bicicleta pode ultrapassar um carro em um congestionamento e um carro pode ajudá-lo a alcançar um avião antes da decolagem —, mas isso deve bastar para cobrir a maioria dos casos específicos.

#### Furtando Carros

Uma tentativa de furtar ou obter um carro de outra forma deve ser tratada como uma ação de superar, com a perícia apropriada — normalmente Roubo ou Recursos — contra uma dificuldade baseada na situação, levando em conta tanto a segurança como a disponibilidade. Em caso de sucesso, os aspectos criados são os aspectos do veículo furtado. Isso assume que o personagem está pegando o que há disponível — tentar roubar um caro específico seria uma ação de superar contra os detalhes específicos dessa situação.

#### Carros Personalizados

Personalizar carros é uma aplicação da perícia Ofícios que requer uma oficina e ferramentas apropriadas. A dificuldade base na rolagem de superar é 0, com +2 para cada aspecto do veículo. Em caso de sucesso, um aspecto pode ser adicionado ao veículo, ou removido, se for aceitável. O número máximo de aspectos de que um veículo pode ter é 5.

#### Dano em Veículos

Os veículos não possuem estresse, mas podem receber consequências — normalmente ao transformar uma rolagem malsucedida de Condução em um sucesso usando a regra opcional de Esforço Extra (veja página 49). Um veículo médio (3 aspectos ou menos) pode receber uma consequência suave. Um veículo excepcional (4 ou 5 aspectos) pode receber uma consequência suave e uma moderada. Um veículo com um aspecto como ___Blindado___ ou ___Categoria Militar___ talvez possa receber até mesmo uma consequência severa.

#### Façanhas Com Veículos

___Ladrão de Carros:___ Quando for roubar um carro, use Condução em lugar de Roubo.
 

------

- [« Duelos Aventurescos](../duelos-aventurescos/)
- [Supers »](../supers/)
