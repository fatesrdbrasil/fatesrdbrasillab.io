I"�<h3 id="condução">Condução</h3>

<p>A perícia Condução trata da operação de veículos e coisas velozes.</p>

<p>O funcionamento da perícia Condução em seu jogo dependerá muito de quantas ações você pretende realizar dentro de um veículo ou outro meio de transporte e do tipo de tecnologia disponível em seu cenário.</p>

<p>Por exemplo, em um cenário futurista de alta tecnologia, com naves espaciais e tudo o mais (como em <em>Caroneiros de Asteroide</em>), talvez você possa mudar Condução para Pilotagem (devido às espaçonaves). Em um cenário de guerra, Operação (para tanques e veículos pesados) fará mais sentido. Cenários de baixa tecnologia, onde o transporte pode ser feito com animais, podem combinar mais com Cavalgar.</p>

<hr />

<h3 id="veículos-diferentes-perícias-diferentes">Veículos Diferentes, Perícias Diferentes</h3>

<p>Não crie muitas categorias a não ser que faça uma diferença notável no seu jogo. Recomendamos considerar a opção de possuir perícias modificadas por façanhas (veja <a href="../criando-facanhas/"><em>Criando Façanhas</em></a>).</p>

<hr />

<ul>
  <li><code class="language-plaintext fate_font highlighter-rouge">O</code> <strong>Superar:</strong> Condução é equivalente a Atletismo quando é utilizada em um veículo –para se movimentar para quando estiver em circunstâncias difíceis, como em terreno acidentado, repleto de buracos ou realizar manobras com o veículo. Obviamente, Condução é muito usada em disputas, especialmente perseguições e corridas.</li>
  <li><code class="language-plaintext fate_font highlighter-rouge">C</code> <strong>Criar Vantagem:</strong> Uma Você pode usar Condução para determinar a melhor maneira de chegar em algum lugar com um veículo. Uma boa rolagem pode permitir que você aprenda detalhes da rota que são expressos em aspectos ou então declarar que sabe um <strong><em>Atalho Conveniente</em></strong> ou algo similar.</li>
  <li>Você também pode ler a descrição da perícia Atletismo e transferi-la para um veículo. Usar a ação Criar Vantagem em Condução, muitas vezes, se trata de obter um bom posicionamento, fazer uma manobra ousada (como um <strong><em>Giro Aéreo</em></strong>) ou colocar o seu oponente em uma posição ruim.</li>
  <li><code class="language-plaintext fate_font highlighter-rouge">A</code> <strong>Atacar:</strong> Condução normalmente não é usado para realizar ataques (apesar de façanhas poderem alterar isso). Se quiser colidir o carro contra algo, você pode usar Condução como ataque, mas levará as mesmas tensões que causar no conﬂito.</li>
  <li><code class="language-plaintext fate_font highlighter-rouge">D</code> <strong>Defender:</strong> Evitar dano a um veículo em um conﬂito físico é uma das formas mais comuns de usar Condução. Você também pode usá-la para defender-se de vantagens criadas contra você ou deter ações de outros veículos tentando ultrapassá-lo.</li>
</ul>

<h4 id="façanhas-para-condução">Façanhas Para Condução</h4>

<ul>
  <li><strong>Duro na Queda:</strong> +2 em Condução sempre que estiver perseguindo outro veículo.</li>
  <li><strong>Pé na Tábua:</strong> Você pode fazer seu veículo atingir uma velocidade maior do que é possível. Sempre que estiver em uma disputa onde a velocidade é o principal fator (como em uma perseguição ou corrida) e empatar com seu adversário em uma rolagem de Condução, é considerado sucesso.</li>
  <li>
    <p><strong>Preparar Para o Impacto!:</strong> Você ignora duas tensões de dano ao colidir com outro veículo. Portanto, se a colisão e causar quatro tensões de dano, você recebe apenas duas.</p>
  </li>
  <li><a href="../comunicacao/">« Comunicação</a></li>
  <li><a href="../conhecimentos/">Conhecimentos »</a></li>
</ul>
:ET