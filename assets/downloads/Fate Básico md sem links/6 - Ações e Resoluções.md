# 6 - Ações e Resoluções

## É Hora da Ação!

**Você rola os dados quando houver algum empecilho interessante entre você e seu objetivo.** Se não houver nenhuma oposição interessante, você simplesmente consegue realizar o que desejava.

Como falamos nos capítulos anteriores, personagens em partidas de *Fate* resolvem seus problemas de forma proativa. Jogadores devem realizar muitas ações durante o jogo – invadir o covil do vilão, pilotar uma espaçonave por um campo minado, reunir pessoas para um protesto ou organizar sua rede de informantes para saber tudo o que se passa nas ruas.

Sempre que agir, há uma boa chance de algo ou alguém se meter em seu caminho. A história não seria nada interessante se o vilão aparecesse repentinamente e se entregasse – ele certamente possuirá medidas de segurança memoráveis para manter você distante de seus planos. As minas podem ser instáveis e prestes a explodir ao seu redor. Ou os manifestantes estão com medo da polícia. Alguém pode estar subornando os informantes para que fiquem de bico calado.

É aí que entram os dados.

-  Escolha a perícia que seja apropriada para a ação.
-  Role quatro dados *Fate*.
-  Junte os dados com símbolos iguais. Um [+] é +1, um [-] é -1 e um [ ] é 0.
-  Adicione o seu nível de perícia à rolagem. O total é o seu resultado de acordo com a escala.
-  Se você invocar um aspecto, adicione +2 ao seu resultado ou lance os dados novamente.

>
> Fräkeline procura rápida uma forma de passar pelos guardas de Nova Quantum. Amanda diz que ela precisa realizar uma ação de superar com oposição passiva (contra uma dificuldade), já que são PdNs simples e não valem um grande conﬂito.
>
> Maira olha a lista de perícias de Fräk e escolhe Recursos, esperando ter dinheiro suficiente para satisfazê-los. Ela possui Recursos em nível Regular (+1), então adicionará um ao resultado que conseguir nos dados. Ela lança os dados e consegue: [ ][-][+][+]
>
> Seu total é +2 (+1 nos dados e +1 de sua perícia Média), o que corresponde a Razoável (+2) na escala.

### Oposição

Como dissemos em *O Básico*, ao rolar os dados comparamos o resultado à oposição. A oposição pode ser ativa, o que significa que outra pessoa está rolando os dados contra você, ou passiva, significando que você rolará contra um nível de dificuldade de acordo com a escala que representa a inﬂuência do ambiente na situação. O Narrador deve decidir qual é a fonte de oposição mais aceitável.

> Amanda decide rolar uma oposição ativa contra Maira, representando os guardas. Ela decide que a perícia mais adequada para isso é Vontade – eles estão tentando resistir à tentação do suborno.
>
Os guardas são PdNs sem importância que não possuem nenhuma razão aparente para possuírem um valor alto em Vontade, então ela fornece a eles o nível Medíocre (+0). Ela rola os dados e consegue: [ ][+][+][+]
>
> ... para o incrível resultado de +3!
>
> Isso fornece um resultado Bom (+3), vencendo o resultado de Maira por um.

------

### Narradores

### Ativa ou Passiva?

Se um PJ ou um PdN importante puder interferir consideravelmente em alguma ação, então você terá a oportunidade de rolar uma oposição ativa. *Isso não consta como uma ação para o personagem opositor; é apenas uma consequência básica da resolução das ações*. Em outras palavras, um jogador não precisa fazer nada especial para ganhar o direito de se opor a uma ação contra ele, contanto que o personagem esteja presente e possa interferir. Se houver alguma dúvida, possuir um aspecto de situação apropriado pode ajudar a justificar porque um personagem consegue se opor a alguém.

Se não houver personagens no caminho, então dê uma olhada nos aspectos de situação da cena atual para ver se algum deles justifica algum tipo de obstáculo ou avaliar as circunstâncias (como *terreno acidentado*, uma *fechadura complexa, tempo se esgotando, uma situação complicada*, etc.). Se soar interessante, escolha uma oposição passiva por selecionar uma dificuldade na escala.

Algumas vezes você irá lidar com casos mais complicados, como quando algo inanimado parece poder oferecer oposição ativa (como uma arma automatizada) ou um PdN que não tem como oferecer resistência direta (como quando ele não sabe o que os PJs estão fazendo). Siga os seus instintos – use o tipo de oposição que se encaixa à circunstância ou que torna a cena mais interessante.

------

------

### Narradores

### O Quão Difíceis Devem Ser as Rolagens de Perícias?**

Você não deve se preocupar com a dificuldade da rolagem em casos de oposição ativa – é só usar o nível de perícia do PdN e rolar os dados como os jogadores fariam, deixando o destino resolver a situação. Temos algumas instruções sobre níveis de perícias de PdNs em [*Conduzindo o Jogo*]() na página XX.

No caso de oposição passiva, você precisa decidir qual o nível na escala que o jogador precisa alcançar em sua jogada. É mais uma arte que uma ciência, mas temos algumas ideias que podem lhe ajudar.

Qualquer coisa com dois ou três níveis a mais que a perícia do jogador – perícia Razoável (+2) e oposição Ótima (+4), por exemplo – significa que o jogador provavelmente irá falhar ou precisará invocar aspectos para ser bem-sucedido.

Qualquer coisa com dois ou mais níveis abaixo da perícia do PC – perícia Razoável (+2) e oposição Medíocre (+0), por exemplo – significa que o jogador provavelmente não precisará invocar aspectos e possui uma boa chance de ser bem-sucedido com estilo.

Entre esses valores, há uma boa chance de empatar ou ser bem-sucedido e a mesma chance dele precisar ou não invocar aspectos para tal.

Portanto, dificuldades baixas são melhores quando você quer dar aos PJs uma chance de mostrarem o quanto são incríveis, dificuldades no nível de suas perícias são a melhor forma de prover tensão mas sem exigir demais e altas dificuldades são a melhor forma de enfatizar o quão difíceis ou anormais são as circunstâncias e fazê-los parar para pensar um pouco.

Por último, algumas dicas simples:

Regular leva esse nome por uma razão – se a oposição não possui nada de especial, então a dificuldade não precisa passar de +1.

Se houver ao menos uma razão para a oposição ser mais especial, mas está difícil decidir o nível adequado de dificuldade, escolha Razoável (+2). É algo entre os níveis de perícias dos PJs, então será um desafio interessante para perícias abaixo de Ótimo (+4), além do que é bom dar aos PJs a chance de exibirem suas habilidades mais altas também.

------

## As Quatro Resoluções

**Ao rolar os dados você pode falhar, empatar, obter sucesso ou obter um sucesso com estilo.**

De modo geral, toda rolagem que você fizer em um jogo de *Fate* terá uma dessas resoluções. Os detalhes podem variar um pouco dependendo de qual ação você fizer, mas todas as ações em jogo se encaixam neste modelo.

### Falha

**Se a sua rolagem for inferior à de sua oposição, você falhou.**

Isso pode significar várias coisas: você não consegue o que queria, você consegue a um grande custo ou sofre alguma consequência mecânica negativa. Às vezes pode significar mais do que uma dessas opções. Cabe ao Narrador determinar o custo apropriado (veja o [quadro]() na página XX).

### Empate

**Se sua rolagem for igual à de seu oponente, você empata.**

Isso significa que você consegue o que deseja, mas com certo custo ou consegue uma versão inferior do que queria.

### Sucesso

**Se a sua rolagem for bem-sucedida com 1 ou 2 tensões, você teve sucesso.**

Isso significa que você consegue o que queria sem custo algum.

### Sucesso com Estilo

**Se a sua rolagem exceder a de sua oposição por 3 ou mais tensões, você é bem-sucedido com estilo.**

Isso significa que você consegue o que quer, e também um benefício adicional.

------

### Narradores

### Custo Severo Vs. Custo Suave

Ao pensar em custo, pense tanto na história quanto na mecânica do jogo para ajudar a determinar o custo mais apropriado.

Um custo severo pode tornar uma situação pior de alguma forma, criando um novo problema ou piorando um existente. Crie outra fonte de oposição na cena atual ou na próxima (como um novo PdN ou um novo obstáculo para contornar), peça ao jogador que anote uma consequência do menor nível disponível, ou dê uma vantagem a um oponente com uma invocação grátis.

Um custo suave pode adicionar detalhes problemáticos ou ruins à história para os PJs, mas não necessariamente perigosos. Você também pode perguntar ao jogador sobre a possibilidade de receber estresse ou fornecer um impulso a algum oponente.

Não há problema se um custo suave for apenas um detalhe narrativo, mostrando como o PJ conseguiu por muito pouco. Damos mais conselhos sobre como lidar com custos na página XX em [*Conduzindo o Jogo*]().

------

## As Quatro Ações

**Quando você faz uma rolagem de perícia, você escolhe uma das quatro ações: superar, criar vantagem, atacar ou defender.**

Existem quatro tipos de ação que você pode realizar em *Fate*. Quando você faz uma rolagem de perícia, você precisa decidir qual deles você tentará. A descrição de cada perícia diz a você quais são as ações apropriadas àquela perícia e em quais circunstâncias normalmente, a ação que você precisa escolher ficará bem óbvia de acordo com a descrição da perícia, seguindo seus instintos e a situação de jogo, mas às vezes você precisará conversar com o grupo para encontrar a mais apropriada.

As quatro ações são: **superar**, **criar vantagem**, **atacar** e **defender**.

### Superar

**Use a ação superar para alcançar objetivos variados de acordo com a perícia usada.**

Cada perícia engloba um grupo específico de esforços variados, situações nas quais aquela perícia se torna ideal. Um personagem com Roubo tenta arrombar uma janela, um personagem com Empatia tenta acalmar uma multidão e um personagem com Ofícios tenta consertar o eixo quebrado de uma carroça após uma perseguição desesperada.

Quando o seu personagem estiver em uma dessas situações e houver algo entre ele e seu objetivo, você usa a ação superar para lidar com isso. Pense nesta ação como o “faz tudo” de toda perícia – se o que deseja realizar não se enquadra em nenhuma outra categoria, provavelmente é uma ação de superar.

A oposição que precisa ser superada pode ser passiva ou ativa, dependendo da situação.

-  **Quando falhar em uma ação de superar,** há duas opções. Você pode simplesmente falhar, o que significa que não alcança seu objetivo, ou consegue o que deseja a um custo severo.
-  **Quando empatar em uma ação de superar,** você consegue o seu objetivo ou consegue o que deseja, mas a um custo menor.
-  **Quando for bem-sucedido em uma ação de superar,**  você consegue o que deseja sem nenhum custo.
-  **Quando for bem-sucedido com estilo em uma ação de superar,** você consegue um impulso além de alcançar o que desejava.

------

Ocasionalmente você pode se deparar com situações que sugiram benefícios ou penalidades diferentes das apresentadas no livro. Tudo bem se for preciso voltar às descrições básicas das 4 resoluções e tentar criar algo que faça sentido.

Por exemplo, em uma ação de superar é dito que você consegue um impulso quando é bem-sucedido com estilo, mas se essa ação de superar estiver encerrando uma cena ou você não consegue pensar em um impulso legal, poderia escolher adicionar algum detalhe à história ao invés de receber o impulso.

------

> Esopo espreita pelos armamentos da fortaleza de Mareesa, A Imperatriz, tentando sabotar os canhões anti-gravidade. Se ele for bem-sucedido, o exército rebelde que o contratou tem uma chance muito maior de vitória, permitindo que suas naves se aproximem.
>
> Amanda diz “Certo, então você chega ao controles do canhão e começa a desligar o equipamento, mas começa a ouvir passos ecoando na sala anterior – parece que o próximo guarda da patrulha chegou um pouco mais cedo”.
>
> “Droga”, diz Léo. “Fui encontrar o único guarda disciplinado da armada. Preciso desativar isto e dar o fora – o General Ephon me alertou que se me encontrassem, ele negaria minha existência”.
>
> Amanda dá de ombros e diz “Quer acelerar o trabalho? Você vai enfrentar uma oposição passiva aqui – tempo curto e lidar com máquinas complexas, eu acho que a dificuldade é de nível Ótimo (+4)”.
>
> Esopo tem a perícia Ofícios em nível Regular (+1). Léo resmunga e diz, “Bandu é quem devia estar cuidando disto.” Ele lança os dados e consegue +2, para um resultado Bom (+3). O que não é suficiente.
>
> Léo pega um ponto de destino e diz “Bem, como sempre digo... ***Bater Sempre Funciona***”, se referindo a um de seus aspectos. Amanda ri e acena com a cabeça, e com a invocação, ele consegue um resultado Excepcional (+5). Isso é bom para um sucesso, mas não o suficiente para um sucesso com estilo, então Esopo consegue seu objetivo sem nenhum custo.
>
> Ele descreve como consegue sabotar o equipamento simplesmente por arrancar o máximo de partes que conseguir, antes de mergulhar num canto escondido quando os guardas se aproximam...

------

### Criar Vantagem

**Use a ação de criar vantagem para criar aspectos de situação que trazem benefícios ou para receber os benefícios de qualquer aspecto a qual tenha acesso.**

A ação de criar vantagem abrange uma grande variedade de esforços no sentido de usar suas habilidades para tirar vantagem (daí o nome) do ambiente ou da situação.

Às vezes isso significa fazer algo para mudar drasticamente as circunstâncias ao seu redor (como jogar areia nos olhos do adversário ou pôr fogo em algo), mas também pode significar que você consegue novas informações que possam ajudá-lo (como conseguir ler a fraqueza de um monstro através de pesquisas) ou tirar vantagem de algo que estudou previamente (como a predisposição de um oponente para um temperamento ruim).

Ao criar vantagem, você deve especificar se está criando um novo aspecto de situação ou se está tirando vantagem de um aspecto existente. No primeiro caso, você vai colocar esse novo aspecto de situação em um personagem ou no ambiente?

A oposição pode ser ativa ou passiva, dependendo das circunstâncias. Se o alvo é outro personagem, sua rolagem sempre contará como uma ação de defesa. Se você estiver usando a ação criar vantagem para criar um aspecto...

-  **Quando falhar,** o aspecto não é criado, ou é, mas outra pessoa recebe a invocação gratuita – ou seja, você consegue, mas outra pessoa ganha vantagem disso. Pode ser o seu oponente em um conﬂito ou qualquer personagem que possa se beneficiar em seu detrimento. Pode ser preciso reformular o aspecto de forma que o outro personagem receba os benefícios – trabalhe nisso com o beneficiado da forma que fizer mais sentido.
-  **Quando empatar,** você consegue um impulso no lugar do aspecto de situação que estava tentando criar. Isso significa que você pode precisar renomear o aspecto para reﬂetir sua natureza temporária (***Terreno Acidentado*** pode virar ***Pedras no Caminho***).
-  **Quando for bem-sucedido,** você cria o aspecto de situação com uma invocação grátis.
-  **Quando for bem-sucedido com estilo,** você consegue criar o aspecto de situação e duas invocações grátis no lugar de apenas uma.

> Nas profundezas dos Esgotos de Kuna Korden, Fräkeline está em uma posição complicada, tendo que lutar com alguns membros do Povo Toupeira.
>
> As primeiras rodadas não foram muito bem e ela recebeu alguns golpes certeiros. Maira diz “Amanda, você disse que há latões, vigas de ferro e outras coisas espalhadas, certo?”
>
> Amanda diz "Sim, há" e Maira pergunta “Posso chutar algo na direção deles para dificultar um pouco as coisas? Imagino que por serem grandes e cegos, não são tão ágeis quanto eu”.
>
> Amanda diz “Por mim tudo bem. Isso é uma ação de criar vantagem usando Atletismo. Um dos touperídeos vai tentar uma ação de defesa, pois está perto o suficiente”.
>
> Fräkeline possui Atletismo Ótimo (+4). Maira lança os dados e consegue +1, atingindo um resultado Excepcional (+5). O homem-toupeira mais próximo faz a rolagem para se defender e consegue um resultado Razoável (+2). Fräk é bem-sucedida com estilo! Maira acrescenta o aspecto ***Entulhos no Caminho*** à cena e pode invocar esse aspecto duas vezes de graça.
>
> Amanda descreve a dificuldade dos homens-toupeira ao tentar se locomover e agora Fräk tem um pouco de vantagem sobre eles...

Se estiver criando vantagem em um aspecto existente...

-  **Quando falhar,** você concede uma invocação grátis a alguém. Pode ser seu oponente em um conﬂito ou outro personagem que possa se beneficiar.
-  **Quando empatar ou for bem-sucedido,** você consegue invocar o aspecto gratuitamente.

-  **Quando for bem-sucedido com estilo,** você consegue duas invocações grátis do aspecto.

> Bandu foi contratado pelo raiduque de Rafatalakan para observar um traficante de especiarias que opera no mercado galáctico da Nebulosa Cadente.
>
> Michel diz, “Vou usar Comunicação para criar vantagem, pra fazer ele se abrir para mim. Não sei exatamente o que estou procurando em termos de aspectos – apenas alguma observação valiosa que eu possa usar depois ou informar a Fräk.” Ele possui a façanha **Mentiroso Amigável** então pode fazer isso sem usar a perícia Enganar, escondendo suas reais intenções.
>
> Amanda diz, “Tudo bem pra mim. Ele é um comerciante, então seu nível de Enganar é bastante alto. Vou considerar isso uma oposição passiva, pois ele não desconfia de você. Tente bater uma dificuldade Ótima (+4)”.
>
> Michel rola os dados. Sua perícia Comunicação é Boa (+3) e ele consegue +1 nos dados, o que lhe concede um empate.
>
> Amanda observa suas anotações, sorri e diz “Certo, você nota o seguinte. Esse traficante é obviamente uma pessoa bastante social, se entrosa com outros mercadores e clientes em potencial por onde passa. Isso muitas vezes toma a forma de um ﬂerte, um ar sugestivo de paquera sempre que ele fala com rapazes – ele parece não conseguir evitar”.
>
> Ela mostra um dos cartões de anotações com o aspecto ***Gamado Em Rapazes Bonitos*** escrito nele, para indicar que esse aspecto do mercador agora é público. Michel percebe que pode invocar esse aspecto uma vez, gratuitamente. “Rapazes bonitos, é?” Michel diz. “Ele me achou bonito?”.
>
> Amanda sorri. “Bem, ele achou você amigável...”
>
> Michel balança a cabeça. “As coisas que faço pelos negócios...”

### Atacar

**Use a ação de atacar para ferir alguém em um conﬂito ou tirá-lo da cena.**

Atacar é a mais direta das quatro ações – quando quiser ferir alguém em um conﬂito, use um ataque. Um ataque não é necessariamente físico por natureza; algumas perícias permitem a você machucar alguém mentalmente.

Na maioria das vezes seu alvo oferece uma oposição ativa contra o seu ataque. Oposição passiva em um ataque significa que você pegou seu alvo desprevenido ou de alguma outra forma incapaz de reagir com eficiência, ou por ser um PdN sem importância e não valer a pena se preocupar com os dados.

Além disso, sendo passiva ou não, a oposição sempre conta como uma ação de defesa, então você pode dar uma olhada nessas duas ações, já que estão intimamente ligadas.

-  **Quando falhar em um ataque,** você não causa nenhum dano em seu alvo (também significa que o seu alvo é bem-sucedido em sua ação de defesa, o que pode gerar outros efeitos).
-  **Quando empatar em uma ação de ataque,** você não causa dano algum, mas consegue um impulso.
-  **Quando for bem-sucedido em um ataque,** você inﬂige **dano** em seu alvo igual ao número de tensões que conseguir. Isso força o alvo a tentar gastar caixas de estresse ou receber consequências; se isso não for possível, seu alvo tem que sair do conﬂito.
-  **Quando for bem-sucedido com estilo,** prossiga como num sucesso normal, mas você também tem a opção de reduzir o valor do dano em um e receber um impulso.

> Fräkeline está travando um combate com Damien, um dos famosos Mareesianos, a guarda de elite da Imperatriz Galáctica. Com seu jeito particular de lutar, Fräk tenta atacá-lo com seu chicote de energia.
>
> A perícia Lutar de Fräkeline é Boa (+3). Damien é Ótimo (+4) em Lutar. Maira rola os dados e consegue +2 para um ataque Excepcional (+5).
>
> Amanda rola dos dados por Damien e obtém -1, rolagem de nível Bom (+3). Maira vence por dois, inﬂigindo 2 tensões.
>
> Ela acha, porém, que isso não é bom o suficiente. “Também vou invocar ***Famosa Ladra Robótica***” ela diz, “porque sou conhecida por não deixar nada mal resolvido”.
>
> Maira paga o ponto de destino, tornando o seu resultado final Épico (+7). Ela consegue 4 tensões, obtendo assim um sucesso com estilo, provocando um grande golpe. Ela escolhe inﬂigir as 4 tensões em dano, mas também poderia ter inﬂigido apenas 3 e escolhido um impulso.
>
> Agora Damien precisa sofrer esse estresse ou receber consequências para se manter na luta!

### Defender

**Use a ação de defender para evitar ataques ou evitar que alguém crie uma vantagem sobre você.**

Sempre que for atacado em um conﬂito ou quando tentarem criar uma vantagem sobre você, haverá a chance de se defender. Como no ataque, isso não significa necessariamente se defender de ataques físicos – algumas perícias permitem se defender de ataques mentais.

Como a rolagem de defesa é uma reação, sua oposição será quase sempre ativa. Se você rolar defesa contra uma oposição passiva, será porque o ambiente é hostil de alguma forma (como num incêndio), ou o PdN não é tão importante para o narrador se importar com os dados.

-  **Quando falhar na sua defesa,** você sofre as consequências do que quer que esteja tentando evitar. Você recebe o dano ou as características da vantagem criada sobre você.
-  **Quando empatar na sua defesa,** você concede um impulso ao seu oponente.
-  **Quando for bem-sucedido na sua defesa,** você consegue evitar o ataque ou a tentativa de criar vantagem sobre você.
-  **Quando for bem-sucedido com estilo,** encare como um sucesso normal, mas você também ganha um impulso, o que pode permitir que você vire o jogo.

------

### Posso Me Defender de Ações de Superar?

Tecnicamente, não. A ação de defesa foi criada para que você evite receber estresse, consequências ou aspectos de situação – basicamente, para proteger você de todo tipo de coisa ruim que possa ser representada pela mecânica do jogo.

Porém, é possível rolar uma oposição ativa se você puder atrapalhar *qualquer* ação, como mostram as [regras na página XX](). Logo, se alguém realizar uma ação de superar que possa falhar porque você está no caminho de alguma forma, diga “Ei! Não é bem assim!”, pegue os dados e role como oposição. Você não ganha nenhum benefício extra como quando usa a ação defender, mas você também não precisa se preocupar com as desvantagens de falhar.

------

> Bandu, O Oculto está discutindo com outro estudioso, Raithua Kaluki, no Museu do Consenso Universal a respeito da origem de uma relíquia em exposição.
>
> Raithua não quer apenas que Bandu seja desacreditado, mas também que ele perca a autoconfiança forçando-o a dar um passo em falso e duvidar de si mesmo. O grupo concorda que, como Kaluki é bastante reconhecido no meio, ele pode tentar afetá-lo dessa forma, então o conﬂito inicia.
>
> Assim que Bandu termina o seu argumento inicial, Amanda descreve como Kaluki usa um ataque de Provocar, encontrando brechas nas teorias de Bandu e forçando-o a se reavaliar. Kaluki tem nível Bom (+3) em Provocar.
>
> Bandu defende com Vontade, que ele possui em um nível Razoável (+2). Amada rola os dados por Kaluki e consegue +1, para um total Ótimo (+4). Michel lança os dados por Bandu e consegue +2 alcançando um resultado Ótimo (+4). Bandu não precisa se preocupar em receber qualquer tipo de dano, mas ele fornece um impulso a Kaluki, o qual Amanda decide ser ***Lapso Momentâneo.***

------

### Sem Efeitos Cumulativos!

Você notará que a ação defender possui resultados que espelham algumas resoluções das ações ataque e criar vantagem. Por exemplo, é dito que quando você empatar em uma jogada de defesa, você fornece ao seu oponente um impulso. Em Atacar, explicamos que quando você empatar, você recebe um impulso.

Isso não significa que o atacante recebe dois impulsos – é o mesmo resultado de dois pontos de vista diferentes. Nós apenas escrevemos assim para que os resultados fossem consistentes quando você lê as regras, independente de que maneira a coisa foi resolvida.

------
