# Fate Condensed em Português

O projeto da tradução foi concluído em 19/05/2021 e está liberado para criação de SRDs na língua portuguesa. O texto abaixo é obrigatório para ser inserido no seu site ou blog antes da leitura do SRD.

> ***Fate Condensado*** SRD - Documento de Referência de ***Fate Condensado***
> 
> © 2020 Evil Hat Productions, Ink Head Publishing. Versão 1.0
>
> Esta obra é baseada em ***Fate Condensado***, encontrado em [*https://www.facebook.com/inkheadpublishing*](https://www.facebook.com/inkheadpublishing), traduzido pela comunidade e fans, desenvolvido e editado por Estevan Fernandes Queiroz e licenciado para uso sob licença Creative Commons Atribuição 4.0 Internacional ([*http://creativecommons.org/licenses/by/4.0/*](http://creativecommons.org/licenses/by/4.0/)). Versão original: Fate Condensed© Evil Hat Productions, LLC. Documento de Referência do Sistema produzido por (insira nome dos responsáveis aqui).
>
> Fate Condensed ©2020 Evil Hat Productions, LLC. Fate™ is a trademark of Evil Hat Productions, LLC. The Fate Core font is © Evil Hat Productions, LLC and is used with permission. The Four Actions icons were designed by Jeremy Keller. 
>
> This work is based on Fate Condensed (found at http://www.faterpg.com/), a product of Evil Hat Productions, LLC, developed, authored, and edited by PK Sullivan, Lara Turner, Leonard Balsera, Fred Hicks, Richard Bellingham, Robert Hanz, Ryan Macklin, and Sophie Lagacé, and licensed for our use under the Creative Commons Attribution 3.0 Unported license ([*http://creativecommons.org/licenses/by/3.0/*](http://creativecommons.org/licenses/by/3.0/)).

## Objetivo e História do Projeto:

Esse é um projeto coletivo, sem fins lucrativos, feito por fãs da Comunidade Movimento Fate Brasil no Facebook e patrocinado pela Ink Head Publishing com autorização da editora Pluma Press, atual detentora dos direitos de licenciamento de Fate no Brasil. O objetivo é criar um material com participação voluntária de fans do sistema e posteriormente
distribuído de forma gratuita através de link para download. As regras traduzidas em PT-BR serão atualizadas também no SRD oficial da comunidade Brasileira: [*https://fatesrdbrasil.gitlab.io/fate-srd/*](https://fatesrdbrasil.gitlab.io/fate-srd/). Você é livre para criar um SRD no seu site ou blog mantendo os dizeres obrigatórios no início desse projeto.

Agradecemos a oportunidade ao Fábio Silva da Editora Pluma Press e também à Editora Solar pelos termos e base de tradução original do Fate Core 2ed. 

![](Pictures/10000000000002D90000013BEE887F57E1D2E94B.jpg)    
![](Pictures/1000000000000190000001908F7FE41D9CB0B914.jpg)      
![](Pictures/100000000000026E000000EA0BD4EC4CB69926A9.jpg)   

Assim como o texto do Fate Condensed original em língua inglesa, esse projeto trabalhará com uma atribuição do Creative Commons: Creative Commons Attribution 4.0 International (CC BY 4.0), que permite a utilização e replicação apenas do texto e não dos elementos gráficos, indicando os devidos créditos aos criadores. 

![](Pictures/10000201000002D900000090AFB8FCE7FA978D52.png)

## Agradecimentos 

### Etapa 1 - Tradução:
 + Pedro Gustavo
 + Cláudio “Cleedee” Torcato
 + Fábio Emilio Costa
 + Estevan Queiroz
 + Lu Cicerone Cavalheiro
 + Daniel Cruz e Silva ”RyuDadas”
 + Filipe Dalmatti Lima
 + Thiago Almeida
 + Luciano Campos
 + Adeyvison Siqueira
 + Pedro Acacito. 

### Etapa 2 Revisão e Localização:

+ Lu Cicerone Cavalheiro
+ Luiz Borges Gomide
+ Wesley Ramos
+ Estevan Queiroz.

### Etapa 3 Refinamento Tradução e Portugues:

+ Lu Cicerone Cavalheiro
+ Lucas Peixoto
+ Wesley Ramos
+ Estevan Queiroz.

# Introdução

Este é o ***Fate Condensado***, uma versão do Sistema ***Fate Básico*** mais compacta possível. Um sistema completo de jogo de interpretação de papéis; enquanto outros livros podem aprimorar seu uso, você não precisará de nenhum outro para jogar. Dito isso, vamos ao que você precisa!

## O que eu preciso para jogar?

Para jogar ***Fate Condensado*** você precisará de dois a seis amigos, um de vocês atuando como Narrador, alguns dados, alguns marcadores, material para escrita, papel e algo para escrever notas curtas (por exemplo, pequenas notas adesivas).

***Fate Condensado*** usa Dados Fate™ quando os personagens decidem agir. Os Dados Fate são dados de seis lados com dois lados em branco com valor igual a \[0\], dois lados com \[+\], e dois lados com \[-\]. Um conjunto de quatro dados já é o suficiente, mas um conjunto por jogador é o ideal. Outras alternativas existem, como usar dados comuns de seis lados
(1-2 = \[-\], 3-4 = \[0\], 5-6 = \[+\]), ou o Baralho do Destino, que usa cartas ao invés de dados. Usaremos a palavra rolar e variantes durante o texto em nome da simplicidade.

> ### Para Veteranos: mudanças do ***Fate Básico***
> 
> Condensar a essência de um sistema de mais de 300 páginas em menos de 70 páginas de texto leva a algumas mudanças. No momento em que isto é escrito, faz oito anos desde que o Sistema ***Fate Básico*** foi criado, então uma pequena iteração do design deve ser esperada. Em particular, destacamos o seguinte:
>
>  - Mudamos para caixas de estresse de um ponto para ajudar a reduzir confusões ([veja aqui](#estresse-e-consequências));
>  - Iniciativa estilo Balsera (também conhecida como "ordem de ação eletiva" ou "iniciativa pipoca") é o padrão ao invés de usar perícias para determinar a ordem de turnos. ([veja aqui](#ordem-de-turnos)).
> - A evolução do personagem funciona de um modo um pouco diferente: eliminamos os marcos significativos e ajustamos os maiores (renomeado para progresso) para compensar ([veja aqui](#avanço)).
>  - Removemos a noção de oposição ativa como algo separado da ação defender ([veja aqui](#defender)). Isso teve alguns pequenos efeitos em cascata, especialmente no resultado de empate da ação superar ([veja aqui](#superar)).
> - A ação de criar vantagem foi ajustada para oferecer maior clareza e dinamismo na hora de descobrir a existência de aspectos desconhecidos ([veja aqui](#criar-vantagem)).
> - Defesa total é apresentada como regra opcional e foi alterada um pouco para acomodar o escopo expandido da ação defender ([veja aqui](#defesa-total)). Outras alternativas às regras padrão são apresentadas também a partir da [página xx](#regras-opcionais).

# Começando

## Defina Seu Cenário

Qualquer jogo de Fate começa com a definição de seu cenário de jogo. Isto pode ser um conceito que o narrador está trazendo à mesa, um cenário popular com o qual os jogadores estejam familiarizados, ou um exercício de construção de mundo colaborativo envolvendo todos à mesa. A discussão sobre detalhes do cenário pode ser rápida e objetiva, pode
envolver uma sessão completa e detalhada com todo o grupo – ou qualquer meio-termo entre estas.

Sua escolha de cenário forma a base do consenso da mesa sobre o que é verdade, e o que é aceitável em jogo e em conceitos de personagem. Se seu cenário não possui pessoas voando nele, então um jogador que decida fazer um personagem voador não deve ser aprovado. Se seu mundo envolve organizações sombrias e conspirações profundas, os jogadores podem esperar histórias livres de conflitos morais pré-estabelecidos e livre de ridículos palhaços assassinos. Vocês que decidem!

## Crie Seus Personagens

### Quem É Você?

Depois de decidir sobre o cenário, é hora dos jogadores criarem seus personagens — também chamados PJs (personagens dos jogadores). Cada jogador assume o papel de um dos heróis de sua história, controlando todas as suas ações. O jogador constrói o personagem que quer ver no mundo. Tenha em mente que os personagens de Fate são competentes, dramáticos e dispostos a se envolver com as aventuras à frente.

Seu PJ é composto de vários elementos:

>  - **Aspectos:** frases descrevendo quem seu herói é;
>  - **Perícias:** áreas em que seu herói tem um certo domínio;
>  - **Façanhas:** coisas notáveis que seu herói faz;
>  - **Estresse:** capacidade do seu herói de manter a calma e seguir em frente;
>  - **Consequências:** os ferimentos, físicos e mentais, que seu herói pode suportar;
>  - **Recarga:** uma medida do poder de afetar a narrativa de seu herói;
>  - **Toques finais:** os detalhes pessoais do seu herói.

### Aspectos 

Os aspectos são frases curtas que descrevem quem seu personagem é ou o que é importante para ele. Eles podem estar relacionados à qualidades físicas, mentais, história, crenças, treinamentos, relacionamentos ou até equipamentos particularmente importantes.

A primeira coisa a saber sobre os aspectos é que eles **são verdades dentro do jogo** (veja a [página xx](#aspectos-são-sempre-verdades) para uma discussão mais aprofundada). Em outras palavras, como você define seu personagem é uma verdade real na história que estiver contando. Se você escrever que o seu personagem é um _Atirador de Elite Presciente_, ele _será_ um atirador de elite presciente. Você definiu que ele pode ver o futuro e é especialista em atirar com um rifle. 

Você também poderá usar aspectos no jogo para alterar a narrativa. Eles lhe permitem melhorar suas rolagens de dados e definir fatos sobre o mundo. Por último, aspectos podem dar **pontos de destino** para você caso criem uma complicação ao seu personagem — então, foque em aspectos versáteis, que sejam de dois gumes, que possam ser usados contra você ou ao seu favor.

Para mais informações sobre aspectos e o que o torna bom, leia mais em Aspectos e Pontos de Destino ([página XX](#aspectos-e-pontos-de-destino)).

**Para começar, você dará ao seu personagem cinco aspectos:**  um conceito, uma dificuldade, um relacionamento e dois aspectos livres. Comece pelo conceito e continue a partir dele.

#### Conceito

O seu aspecto Conceito é uma descrição geral do seu personagem que cobre os pontos principais. É a primeira coisa que você diria ao descrever o personagem para um amigo. 

#### Dificuldade

A seguir é a dificuldade do seu personagem – algo que torna a vida dele mais complicada. Pode ser uma fraqueza pessoal, laços familiares ou outros compromissos. Escolha algo que você gostará de interpretar durante o jogo!

#### Relacionamento

Seu relacionamento descreve uma conexão com outro PJ. Eles já podem ser conhecidos ou acabaram de se conhecer.

Um bom aspecto de relacionamento deve introduzir ou indicar um conflito ou pelo menos um desequilíbrio para gerar atritos nessa relação. Isto não significa que o relacionamento é abertamente antagônico, apenas que não é um mar de rosas. 

Se você preferir, você pode esperar para escrever o aspecto de relacionamento até que todos estejam mais ou menos próximos de concluir seus respectivos personagens.

#### Aspectos Livres

Você pode criar da forma que quiser seus dois últimos aspectos. Não há restrições além da obrigação de estarem condizentes com o cenário e a proposta do jogo. Escolha qualquer coisa que você acredite que fará seu personagem ser mais interessante, mais divertido de jogar ou mais conectado com o mundo em que ele vive.

### Perícias

Enquanto aspectos definem quem o seu personagem é, **perícias** mostram o que ele pode fazer. Cada perícia descreve uma ampla atividade que seu personagem pode ter aprendido através de estudo, prática ou simplesmente por talento nato. Um personagem com roubo é capaz, até certo ponto, de todo tipo de crime relacionado com a fina arte do roubo — verificar falhas de segurança de um local, burlar sistemas de segurança, bater carteiras e arrombar fechaduras.

Cada perícia tem um **nível**. Quanto maior o nível, melhor o personagem é naquela perícia. De forma geral, as perícias do seu personagem mostrarão em quais ações ele é habilidoso, quais ele vai conseguir se virar e quais não são o seu forte.

Você escolherá o nível das perícias do seu personagem, distribuídas em uma pirâmide com a perícia de maior nível em Ótimo (+4), da seguinte maneira:

  - Uma perícia em nível Ótimo (+4);
  - Duas perícias em nível Bom (+3);
  - Três perícias em nível Razoável (+2);
  - Quatro perícias em nível Regular (+1);
  - Todas as outras perícias em Medíocre (+0).

#### A ESCALA DE ADJETIVOS

Em ***Fate Condensado*** – e Fate em geral – todos os valores são organizados
em uma escala de adjetivos: 

| NÍVEL | ADJETIVOS    |
| ----- | ---------    |
| \+8   | Lendário     |
| \+7   | Épico        |
| \+6   | Fantástico   |
| \+5   | Excepcional  |
| \+4   | Ótimo        |
| \+3   | Bom          |
| \+2   | Razoável     |
| \+1   | Regular      |
| \+0   | Medíocre     |
| \-1   | Ruim         |
| \-2   | Terrível     |
| \-3   | Catastrófico |
| \-4   | Horripilante |

#### Lista De Perícias

Descrições para as perícias são encontradas abaixo.

  - Atirar
  - Atletismo
  - Comunicação
  - Condução
  - Conhecimentos
  - Contatos
  - Empatia
  - Enganar
  - Furtividade
  - Investigar
  - Lutar
  - Ofícios
  - Percepção
  - Provocar
  - Recursos
  - Roubo
  - Saberes
  - Vigor
  - Vontade

**Atirar:** Habilidade com todas as formas de combate à distância, sejam armas de fogo, facas de arremesso ou arco e flecha. Façanhas de Atirar permitem ataques de precisão, saques rápidos ou sempre ter uma arma à disposição.

**Atletismo:** Uma medida do potencial físico. Façanhas de Atletismo se concentram na movimentação (como correr, saltar, *parkour*) e em desviar de ataques.

**Comunicação:** Habilidade de criar conexões com outras pessoas e trabalhar em equipe. Enquanto a perícia Provocar está relacionada com a manipulação, a Comunicação possibilita interações sinceras, de confiança e boa vontade. Façanhas de Comunicação permitem influenciar multidões, melhorar relacionamentos ou fazer contatos.

**Condução:** Controlar veículos nas circunstâncias mais difíceis, fazer manobras radicais, ou simplesmente obter o máximo do seu veículo. Façanhas de Condução podem ser manobras exclusivas, um veículo especial de sua propriedade ou a capacidade de usar Condução no lugar de Perícias como Roubo ou Conhecimento sob certas circunstâncias.

**Conhecimentos:** Conhecimento de senso comum e educação formal, incluindo história, ciências e medicina. Façanhas de Conhecimentos geralmente se referem a áreas especializadas do conhecimento e habilidades médicas.

**Contatos:** Conhecimento das pessoas certas e conexões que podem ajudá-lo. Façanhas de Contatos fornecem aliados dispostos a ajudar e uma rede de informações em qualquer lugar do mundo.

**Empatia:** Capacidade de julgar precisamente o ânimo e as intenções de alguém. Façanhas de Empatia podem ser sobre avaliar o humor de uma multidão, identificar mentiras ou ajudar outras pessoas a se recuperarem de consequências mentais.

**Enganar:** Habilidade de mentir e trapacear de forma convincente e serena. Façanhas de Enganar podem aprimorar a capacidade de contar um tipo específico de mentira ou ajudar a criar identidades falsas.

**Furtividade:** Ficar oculto ou passar despercebido, e escapar quando precisar se esconder. Façanhas de Furtividade permitem que você desapareça à plena vista, se misture na multidão ou avance despercebido pelas sombras.

**Investigar:** Análise deliberada e cuidadosa, e resolução de mistérios. Esta perícia é utilizada para reunir pistas ou reconstruir uma cena de crime. Façanhas de Investigar ajudam a formar deduções brilhantes ou reunir informações rapidamente.

**Lutar:** Maestria em combate corpo a corpo, seja com armas ou punhos. Façanhas de Lutar incluem armas exclusivas e técnicas especiais.

**Ofícios:** Capacidade de criar ou destruir maquinários, construir engenhocas e realizar proezas inventivas ao estilo MacGyver. Façanhas de Ofícios permitem que você tenha gambiarras à disposição, dão bônus para construir ou quebrar coisas, e justificam o uso de Ofícios no lugar de perícias como Roubo ou Conhecimento sob certas circunstâncias.

**Percepção:** Habilidade de captar detalhes rapidamente, identificar problemas antes que aconteçam, e ser perceptivo, de maneira geral. Ela contrasta com Investigar, que é usada para uma observação lenta e deliberada. Façanhas de Percepção aguçam seus sentidos, melhoram seu tempo de reação ou dificultam que você seja surpreendido.

**Provocar:** Capacidade de incitar as pessoas a agirem da maneira que você deseja. Não é uma interação positiva, mas sim manipulativa e rude. Façanhas de Provocar permitem incitar oponentes a agir de forma imprudente, atrair agressões para si ou assustar inimigos (desde de que eles possam sentir medo).

**Recursos:** Acesso a coisas materiais, não apenas dinheiro ou propriedades. Pode refletir sua capacidade de pegar algo emprestado de amigos ou recorrer ao arsenal de uma organização. Façanhas de Recursos permitem que você use Recursos no lugar de Comunicação ou Contatos, ou permitem invocações gratuitas extras quando você paga por algo de  elhor qualidade.

**Roubo:** Conhecimento e capacidade de burlar sistemas de segurança, bater carteiras e cometer crimes em geral. Façanhas de Roubo dão bônus aos vários estágios de um crime, desde o planejamento até a execução e fuga.

**Saberes:** Conhecimento arcano, fora do escopo da perícia Conhecimento, incluindo tópicos sobrenaturais de qualquer tipo. É aqui que as coisas estranhas acontecem. Façanhas de Saberes incluem aplicações práticas de saberes arcanos como a conjuração de magias. Alguns cenários podem remover a perícia Saberes, substituí-la por outra ou combiná-la com a perícia Conhecimento.

**Vigor:** Força bruta e resistência física. Façanhas de Vigor permitem que você execute proezas de força sobre-humana, como usar seu peso a seu favor durante uma luta, e mitigue consequências físicas. Além disso, um nível alto em Vigor concede mais caixas de estresse físico ou caixas de consequências físicas ([página XX](#estresse-e-consequências)).

**Vontade:** Resistência mental. Capacidade de resistir uma tentação ou de permanecer lúcido durante um evento traumático. Façanhas de vontade permitem que você ignore consequências mentais, resista à agonia mental provocada por poderes estranhos e mantenha-se firme ante a inimigos que tentem provocá-lo. Além disso, um nível alto de Vontade  concede mais caixas de estresse mental ou caixas de consequências mentais ([página XX](#estresse-e-consequências)).

#### Listas Alternativas de Perícias.

A lista acima é um exemplo de como perícias podem funcionar em Fate e não tem a intenção de ser uma lista “oficial” que deve ser usada toda vez que você jogar. Você pode usá-la ou não, a escolha é sua. Qualquer perícia pode cobrir diferentes conjuntos de atividades, dependendo do cenário. Personalização é a chave\! Ajuste sua lista à história que você quer jogar.

Tendo isso em mente enquanto você constrói seu jogo em Fate, a primeira coisa a se pensar é se você manterá ou não a mesma lista de perícias. É possível que a granularidade da lista de exemplo padrão que nós demos acima não seja do seu gosto, então você poderá trabalhar com ela combinando, mudando ou dividindo algumas perícias da lista.

Aqui estão algumas coisas para você pensar:

  - A lista padrão tem 19 perícias e os personagens distribuirão valores acima de Medíocre (+0) em 10 delas. Se você mudar o número de perícias, você pode querer mudar como os níveis serão alocados.
  - Nossa lista padrão tem como foco responder a pergunta _“O que você pode fazer?”_, mas sua lista não precisa seguir essa linha. Você pode querer uma lista focada em _“Em que você acredita?”_ ou _“Como você faz as coisas?”_ (como nas abordagens de ***Fate Acelerado***), funções de uma equipe de enganadores e ladrões e por aí vai.
  - Níveis de perícias em Fate são estruturados para dar suporte a nichos de personagens. É por isso que, por padrão, os personagens começam com perícias em forma de pirâmide. Certifique-se de que a proteção destes nichos seja possível em qualquer lista que você criar.
  - A melhor perícia inicial deveria ter nível por volta de Ótimo (+4). Você pode alterar isso para baixo ou para cima se achar necessário, mas certifique-se de observar o que isso implica para os níveis de dificuldades e oposições que os PJs enfrentarão.

> Felipe decide que ele quer fazer um jogo de Fate sobre exploração espacial com uma lista menor de perícias focadas mais em ações. Ele decide por uma lista com 9 perícias: Lutar, Saber, Mover, Perceber, Pilotar, Esgueirar, Falar, Improvisar e Impor. Ele também gostou da ideia de ter uma forma de diamante para os níveis de perícias ao invés de uma pirâmide, então ele faz com que os personagens definam os valores de perícias iniciais da seguinte forma: 1 em Ótimo (+4), 2 em Bom (+3), 3 em Razoável (+2), 2 em Regular (+1) e 1 em Medíocre (+0). Seus PJs terão muitas perícias e competências centrais em comum devido ao meio largo do seu diamante, enquanto poderão desfrutar de uma proteção de nicho no topo da “ponta” do diamante.

Se você está considerando criar sua própria lista de perícias para seu jogo e procura por ideias para inspirar sua imaginação, veja a [página XX](#mudando-a-lista-de-perícias).

### Recarga

A recarga é o número mínimo de pontos de destino ([página XX](#ganhando-pontos-de-destino)) com os quais seu personagem começa cada sessão de jogo. Os personagens iniciam com recarga de 3.

Em cada sessão, você começa com um total de pontos de destino no mínimo igual à sua recarga. Certifique-se de registrar com quantos pontos de destino você termina cada sessão de jogo – se você tiver mais pontos de destino do que seu valor de recarga, você começará a próxima sessão com os pontos de destino que você tinha ao final desta sessão.

> Por exemplo, Carlos recebeu muitos pontos de destino durante a sessão de hoje terminando com 5 pontos de destino. Seu valor de recarga é 2, então Carlos iniciará a próxima sessão com 5 pontos de destino. Entretanto, Edson terminou a mesma sessão com apenas 1 ponto de destino. Como seu valor de recarga é 3, ele iniciará a próxima sessão com 3 pontos de destino, e não com apenas aquele que havia sobrado. 

### Façanhas 

Enquanto todos os personagens tem acesso a todas as perícias – mesmo se na maioria delas forem Medíocres (+0) – seu personagem tem algumas **façanhas** únicas. Façanhas são técnicas, truques ou equipamentos legais que tornam seu personagem único e interessante. Enquanto perícias representam as competências gerais do personagem, façanhas são áreas de excelência mais específicas; a maioria delas dá a você bônus em circunstâncias determinadas ou permite que você faça algo que outros personagens simplesmente não podem fazer.

Seu personagem começa com 3 caixas de façanhas gratuitas. Você não precisa defini-las logo de cara, podendo preenchê-las à medida que joga. Você pode comprar mais façanhas gastando seu valor de recarga em 1 por façanha adquirida, não podendo ficar com valor inferior a 1 em recarga.

#### Escrevendo Façanhas

Você escreve suas próprias façanhas quando estiver criando um personagem. De forma geral, há dois tipos de façanhas.

**Façanhas que concedem bônus:** O primeiro tipo de façanha **concede +2 de bônus** quando usar uma perícia definida dentro de certos parâmetros, normalmente limitado a um tipo específico de ação ([página XX](#ações)) e um tipo de circunstância narrativa.

Escreva esse tipo de façanha da seguinte forma:

> Porque eu **\[descreva como você é incrível ou tem algum equipamento bacana\]**, eu ganho +2 quando eu uso **\[escolha uma perícia\]** para **\[escolha uma ação: superar, criar vantagem, atacar, defender\]** quando **\[descreva uma circunstância\]**.

**Exemplo de façanha que concede bônus:** Porque eu sou um **atirador treinado militarmente**, eu **ganho +2** quando eu uso **Atirar** para **Atacar** quando **tiver um alvo _Na Mira_**.

**Façanhas que mudam regras:** O segundo tipo de façanha **muda as regras do jogo**. Essa é uma categoria ampla que inclui, mas não é limitada aos seguintes:

  - **Trocar quais perícias são usadas em uma determinada situação**. Por    exemplo, um pesquisador pode usar Conhecimentos para realizar um    ritual enquanto qualquer outro personagem usaria Saberes;
  - **Usar uma ação com uma perícia que normalmente não serve para aquilo**.     Por exemplo, permitir que um personagem use Furtividade para atacar     um oponente pelas costas saltando das sombras (o que normalmente     seria feito com Lutar);
  - **Dar a um personagem um tipo diferente de bônus para perícia que seja     aproximadamente igual a +2**. Por exemplo, quando um orador competente     cria uma vantagem usando Comunicação, ele ganha uma invocação     gratuita adicional.
  - **Permitir que um personagem declare um pequeno fato que é sempre    verdadeiro**. Por exemplo, um sobrevivencialista sempre tem itens de     sobrevivência como fósforos consigo, mesmo em situações que isso não     seria provável. Esse tipo de façanha estabelece que você não precisa     invocar aspectos para declarar detalhes  narrativos ([página XX](#invocações-para-declarar-detalhes-narrativos)) para     o fato em questão;
  - **Permitir que um personagem crie uma exceção específica nas regras**.    Por exemplo, um personagem pode ter mais duas caixas de estresse ou    mais um campo para consequência suave.

Escreva esse tipo de façanha da seguinte forma:

> Porque eu **\[descreva como você é incrível ou tem um equipamento bacana\]**, eu posso **\[descreva sua proeza incrível\]**, mas somente **\[descreva uma circunstância ou limitação\]**.

**Exemplo de façanha que muda as regras:** Porque eu **não acredito em magia**, eu posso **ignorar os efeitos de uma habilidade sobrenatural**, mas somente **uma vez por sessão**.

### Estresse e Consequências

Estresse e consequências são como seu personagem suporta as adversidades físicas e mentais de suas aventuras. Personagens têm pelo menos 3 caixas de um ponto para estresse físico e pelo menos 3 caixas de um ponto para estresse mental. Eles também ganham uma caixa para cada consequência: suave, moderada e severa.

Seu nível em Vigor afeta seu total de caixas de estresse físico. Vontade faz o mesmo com estresse mental. Use a seguinte tabela:

| VIGOR/VONTADE                 | ESTRESSE FÍSICO/MENTAL |
| ----------------------------- | ---------------------- |
| Medíocre (+0)                 | [1][1][1]                      |
| Regular (+1) ou Razoável (+2) | [1][1][1] [1]                      |
| Bom (+3) ou Ótimo (+4)        | [1][1][1] [1][1][1]              |
| Excepcional (+5) ou maior     | [1][1][1] [1][1][1] e uma segunda caixa para consequência suave especificamente para causas físicas ou mentais                      |

Você aprenderá como o estresse e as consequências funcionam durante o jogo em _“Recebendo Dano” ([página XX](#recebendo-dano))_.

#### Peraí, não é isso o que eu me lembro\!

Em ***Fate Condensado***, estamos usando somente caixas de estresse de um ponto. Tanto o ***Fate Básico*** quanto o ***Fate Acelerado*** usam uma série de caixas com valores progressivos (uma caixa de 1 ponto, uma de 2 pontos, etc). Você pode usar a forma que preferir. Para esta versão, decidimos ficar com caixas de 1 ponto para tornar o jogo mais simples. Com o outro método, as pessoas acabavam se confundindo com mais facilidade.

Há alguns outros pontos sobre esse estilo de caixa de estresse que você deve ter em mente.

  - Como você verá na [página XX](#estresse), com caixas de 1 ponto você poderá     marcar quantas quiser quando for atacado (já o sistema progressivo     do ***Fate Básico*** tem a regra que "você só pode marcar uma caixa por     golpe").
  - Esse estilo segue a linha do ***Fate Básico*** de separar as barras de     estresse Físico e Mental, em vez de usar uma única barra como no     ***Fate Acelerado***. Se você preferir uma barra unificada, adicione mais     3 caixas para compensar e use o _maior_ entre Vigor e Vontade para     aumentá-la como indicado acima.
  - Três pontos de absorção de estresse em uma barra não é muito\! Se os     personagens se sentirem frágeis durante o jogo, considere adicionar     1 ou 2 caixas à quantidade padrão. Isso dependerá da rapidez em que     as consequências acontecem (no estilo antigo, uma barra com     \[1\]\[2\] absorve 2 a 3 estresses, \[1\]\[2\]\[3\] absorve 3 a 6 e     \[1\]\[2\]\[3\]\[4\] absorve 4 a 10).

### Toques Finais

Dê um nome e uma descrição ao seu personagem e discuta a história dele com os outros jogadores. Se você ainda não escreveu um aspecto de relacionamento, faça isso agora.

# Agindo e Rolando os Dados

Em uma sessão de ***Fate Condensado***, você controlará as ações do personagem que criou contribuindo para a história que vocês estão contando juntos. Em geral, o narrador descreve o mundo e as ações de personagens do narrador (PdN) enquanto que os outros jogadores narraram as ações de seus PJs individualmente.

Para agir, siga o princípio da ***ficção primeiro***: diga o que seu personagem está tentando fazer para _depois_ descobrir quais regras usará. Os aspectos do seu personagem informam o que ele pode tentar fazer e ajudam a definir o contexto para interpretar os resultados. A maioria das pessoas não podem nem tentar realizar uma cirurgia de emergência em um aliado eviscerado, mas com um aspecto estabelecendo um antecedente médico, você poderia tentar. Sem esse aspecto, você poderia no máximo conseguir alguns momentos para as últimas palavras. Em caso de dúvida, verifique com seu narrador e com a mesa.

Como saber se você foi bem sucedido? Frequentemente, seu personagem simplesmente terá sucesso, porque a ação não é difícil e ninguém está tentando impedi-lo. Mas em situações difíceis ou imprevisíveis, você precisará usar os dados para descobrir o que acontece.  

Quando um personagem quiser realizar uma ação, o grupo deverá refletir sobre estas questões:

  - O que está impedindo isso de acontecer?
  - O que poderia dar errado?
  - O que seria interessante acontecer caso desse errado?

Se ninguém tiver boas respostas para todas essas questões, a ação simplesmente acontece. Dirigir para o aeroporto não requer uma rolagem de dados. Por outro lado, dirigir apressado pela rodovia até um avião te esperando enquanto é perseguido por bestas ciberneticamente aprimoradas vindas de outro mundo é o momento perfeito para rolar os dados.

Sempre que você agir, siga estes passos:

1.  Ficção primeiro: descreva o que você está tentando fazer e, só     então, escolha a perícia e a ação adequadas;
2.  Role 4 dados;
3.  Some os símbolos dos dados: um \[+\] vale +1, \[-\] vale -1 e [0]     é 0. Isso resultará em um resultado entre -4 a 4;
4.  Some o resultado dos dados com o nível de sua perícia;
5.  Modifique os dados invocando aspectos (páginas [xx](#invocando-aspectos) e [xx](#invocações)) e usando     façanhas (página [XX](#usando-façanhas)).
6.  Declare seu resultado total, chamado de ***esforço***.

## Dificuldade e Oposição

Se a ação de um personagem enfrenta um obstáculo fixo ou tenta alterar o mundo ao invés de um personagem ou criatura, sua ação enfrenta um valor estático de **dificuldade**. Essas ações incluem arrombar fechaduras, bloquear portas e avaliar taticamente um acampamento inimigo. O narrador pode decidir que a presença de certos aspectos (do personagem, da cena, ou qualquer outra coisa) justifica mudanças na dificuldade.

Em outras ocasiões, um oponente fará **oposição** contra a ação do personagem usando a ação de defender (página [xx](#defender)). Nesses casos, o narrador também rolará os dados e seguirá as mesmas regras da seção anterior usando perícias, façanhas ou aspectos que o oponente tenha. Toda vez que você rolar para atacar um oponente ou para criar uma vantagem diretamente contra ele, o oponente rolará para defender-se. 

A oposição pode assumir formas variadas. Lutar com um cultista pela adaga ritualística tem um oponente claro. Ou você poderia ser contraposto pelo poder de um antigo ritual que deve ser superado para salvar o mundo. Arrombar o cofre do Primeiro Banco Metropolitano para acessar as caixas de depósito seguro é um desafio com risco de ser descoberto, mas cabe ao narrador decidir se você está rolando contra a *oposição* dos guardas em patrulha ou pela *dificuldade*  presentada pelo próprio cofre.

## Modificando os Dados

Você pode modificar seus dados invocando aspectos para ganhar +2 na sua rolagem ou para rolar novamente os dados. Algumas façanhas também garantem um bônus. Você também pode invocar aspectos para ajudar um aliado (página [xx](#trabalho-em-equipe)) ou para aumentar a dificuldade que um oponente enfrenta.

### Invocando aspectos

Quando você realiza uma ação, mas os dados acabam resultando em um valor baixo, você não precisa aceitar a falha (embora você possa. Isso também é divertido.). Os aspectos em jogo oferecem opções e oportunidades para você ser bem sucedido.

Quando um aspecto puder justificadamente ajudar seus esforços, descreva como ele ajuda e gaste um ponto de destino para ***invocá-lo*** (ou use uma invocação gratuita). O que é ou não é justificável está sujeito à ***regra do “Fala Sério”*** - qualquer um pode dizer *“Fala Sério\!”* para uma invocação de um aspecto. De modo simples, a regra “Fala Sério” é uma **ferramenta de calibração** que qualquer um da mesa pode usar para garantir que o jogo permaneça fiel à sua visão e conceito. Você também pode usar as ferramentas de segurança discutidas na [página xx](#ferramentas-de-segurança) de maneira semelhante.

Você tem duas opções quando sua invocação receber um “Fala Sério”. Primeiro, você pode retirar sua invocação e tentar outra coisa ou talvez até invocar um aspecto diferente. Segundo, você pode ter uma breve discussão sobre o porquê você acredita que aquele aspecto funcione. Se mesmo assim a pessoa não for convencida, retire a invocação e bola pra frente. Se ela acabar concordando com seu ponto de vista, vá em frente e invoque o aspecto normalmente. A regra do “Fala Sério” existe para ajudar na diversão de todos da mesa. Use quando algo não soe certo, não faça sentido ou não encaixe no tom da narrativa. Alguém invocando _Ótimo em Primeiras Impressões_ para arremessar um carro seria  provavelmente um “Fala sério”. Mas talvez o personagem tenha uma façanha sobrenatural que o torna incrivelmente forte, o suficiente para plausivelmente arremessar um carro, e esta seja sua manobra de abertura em uma luta contra um monstro terrível. Nesse caso, talvez _Ótimo em Primeiras Impressões_ seja plausível.

> Sinta-se à vontade para adaptar o nome dessa regra para uma gíria ou termo regional que você e seu grupo tenham mais familiaridade.

Cada vez que você invoca um aspecto, você poderá **ganhar um bônus de +2** na sua rolagem, ou rolar novamente todos os seus quatro dados ou ainda adicionar 2 na dificuldade da rolagem de alguém caso seja justificável. Você pode invocar múltiplos aspectos na mesma rolagem, mas não pode invocar o mesmo aspecto várias vezes nela. Há uma exceção: você pode gastar quantas *invocações gratuitas* quiser de um aspecto na mesma rolagem.

Frequentemente você invocará um de seus próprios aspectos. Você pode também invocar um aspecto de situação ou fazer uma invocação hostil de um aspecto de outro personagem ([página xx](#invocações-hostis)).

### Usando Façanhas

Façanhas podem dar um bônus para sua rolagem se considerar que você atende aos critérios descritos nela como circunstâncias, ações, ou perícia usada. Você poderá usar a ação de Criar Vantagem ([página xx](#criar-vantagem)) para introduzir aspectos que se alinhem a essas circunstâncias. Lembre-se das circunstâncias das quais suas façanhas podem ser usadas quando  escrever suas ações e prepare-se para o sucesso. 

Normalmente, façanhas dão um bônus de +2 em uma circunstância específica sem custo algum; você pode usá-las sempre que elas se aplicarem à situação. Algumas façanhas raras e excepcionalmente poderosas podem exigir que você pague um ponto de destino para usá-las.

## Resoluções

Sempre que rolar os dados, a diferença entre seu esforço e a dificuldade alvo ou oposição é medida em ***tensões***. Uma tensão tem o valor de 1. Existem quatro possíveis resultados:

  - Se o seu esforço é menor que a dificuldade alvo ou oposição, você     **falha**;
  - Se o seu esforço é igual ao alvo, você **empata**; 
  -  Se o seu esforço é uma ou duas tensões maiores que o alvo, você     obtém **sucesso**;
  - Se o seu esforço é três ou mais tensões maiores que o alvo, você     obtém **sucesso com estilo**.

Alguns resultados são obviamente melhores para você do que outros, mas todos devem avançar a história de maneiras interessantes. Você começou com ficção primeiro ([página xx](#agindo-e-rolando-os-dados)); certifique-se que termine com isso também para manter o foco da história e para garantir que o resultado seja interpretado de uma forma que se encaixe na ficção.

> Edson não é um exímio arrombador de cofres (embora ele tenha as ferramentas), e mesmo assim ele está dentro do quartel general de um culto sinistro no qual uma porta de metal está entre ele e o livro de rituais que ele desesperadamente precisa. Conseguirá ele arrombar o cofre?

### Falha

> _Se seu esforço for menor que a dificuldade alvo ou oposição, você falha._

Isso pode acontecer de algumas formas: falha simples, sucesso a um custo maior ou recebendo dano. 

#### Falha simples.

A primeira e mais fácil de entender é a ***falha simples***. Você não alcança seu objetivo, não realiza nenhum progresso e deixa a desejar. Garanta que isso mantenha a história andando - simplesmente falhar no arrombamento do cofre é estagnado e entediante.

> Edson puxa a trava do cofre triunfantemente, mas o cofre se mantém fechado enquanto os alarmes disparam. A falha mudou a situação e impulsionou a história adiante - agora há guardas a caminho. Edson se depara com uma nova escolha: tentar outra forma de abrir o cofre, agora que a sutileza não é mais uma opção, ou desistir e fugir?

#### Sucesso a um custo maior.

Em segundo lugar, existe o ***sucesso a um custo maior***. Você faz o que definiu que faria, mas existe um preço significativo a pagar: a situação piora ou fica mais complicada. Narrador, você pode tanto declarar que esse é o resultado quanto oferecê-lo no lugar da falha. Ambas opções são boas e úteis em diferentes situações.

> Edson falha na sua rolagem e o narrador diz: “Você ouve um clique do último tambor caindo em seu lugar. Ele ecoa com o clique do cão do revólver de um guarda que te diz para erguer as mãos para o alto”. O custo maior aqui é o confronto com um guarda que ele esperava evitar.

#### Receber um Ataque

Por último, você pode ***levar um ataque***, que deverá ser absorvido com estresse ou consequências, ou sofrer alguma desvantagem. Esse tipo de falha é mais comum ao se defender de ataques ou superar obstáculos perigosos. É diferente de uma falha simples porque somente o personagem, e não necessariamente o grupo todo, é afetado. Também é diferente de um sucesso a um custo maior, já que o sucesso não está necessariamente envolvido.

> Edson foi capaz de abrir a porta do cofre, mas enquanto ele gira a maçaneta, ele sente um golpe nas costas de sua mão. Ele não conseguiu desarmar a armadilha\! Ele escreve a consequência leve: _"envenenado"_.

Você pode misturar essas opções: uma falha nociva pode ser dura, mas apropriada para o momento. Sucesso ao custo de um ferimento é certamente uma opção.

### Empate.

> _Se seu esforço é igual a dificuldade alvo ou oposição, você empata._

Assim como na falha, empates devem mover a história adiante e nunca travar a ação. Algo interessante deve acontecer. Semelhante à falha, isso pode ser resolvido de duas formas: sucesso a um custo menor ou sucesso parcial.

#### Sucesso a um custo menor.

O primeiro é o ***sucesso a um custo menor*** - alguns pontos de estresse, detalhes da história que dificultam ou complicam, mas não impedimentos em si, ou um impulso ([página xx](#impulsos)) dado ao inimigo são custos menores.

> Todas as primeiras tentativas de Edson falham. Quando finalmente consegue abrir a porta, o dia amanheceu e escapar com a ajuda da escuridão é impossível. Ele conseguiu o que queria, mas sua situação piorou agora.

#### Sucesso Parcial

A outra forma de lidar com o empate é com um ***sucesso parcial*** - você foi bem sucedido, mas conseguiu somente parte do que queria.

> Edson pôde abrir somente uma pequena fresta do cofre - se a porta abrir mais que alguns centímetros o alarme tocará e ele não sabe como desativá-lo. Ele acaba puxando algumas páginas do ritual através da estreita abertura, mas terá que adivinhar as etapas finais.

### Sucesso

> _Se seu esforço for uma ou duas tensões maior que a dificuldade alvo ou oposição, você obtém sucesso._

Você consegue o que queria sem nenhum custo adicional.

> Aberto\! Edson pega o ritual e foge antes que os guardas o percebam.

Aplicando o princípio da “ação primeiro” ao sucesso.

A ficção _define_ como o sucesso se parece. E se o Edson não tivesse as ferramentas ou experiência necessária para arrombar o cofre? Talvez aquele sucesso estaria mais para nosso exemplo de “custo menor” descrito acima. De forma  imilar, se o Edson estava no time porque ele _construiu_ o cofre, aquele sucesso poderia se parecer mais com nosso exemplo de “com estilo”.

### Sucesso com estilo.

> _Se seu esforço for três ou mais tensões maior que a dificuldade alvo ou oposição - você obtém um sucesso com estilo._

Você consegue o que queria e ganha um pouco mais além disso.

> Edson é mais que sortudo. O cofre abre quase que instantaneamente. Ele não só pega o ritual, como tem tempo o suficiente para vasculhar outros papéis no fundo do cofre. Entre vários registros fiscais e documentos financeiros, ele encontra um mapa da antiga mansão Akeley.

## Ações

Existem quatro ações que você pode usar rolamentos, cada uma com um propósito e efeito específico na história:

  - **Superar:** supere obstáculos com suas perícias;
  - **Criar Vantagem:** crie vantagem que mude a situação em seu     benefício;
  - **Atacar:** ataque para prejudicar o inimigo;
  - **Defender:** defenda-se para sobreviver a um ataque, impedir alguém     de criar uma vantagem, ou se opor ao esforço de superar um     obstáculo.

### Superar

> _Supere obstáculos usando suas perícias_

Todo personagem encontrará desafios imprevistos no decorrer da história. A ação de ***Superar*** mostrará como eles encaram e transpõem tais obstáculos.

Um personagem bom em Atletismo pode escalar muros e correr por ruas aglomeradas de pessoas. Um detetive com Investigação elevada pode conectar pistas que outros não perceberiam. Alguém hábil em Comunicação evitará com facilidade que uma briga comece em um bar hostil;

Os resultados ao Superar são:

  - **Se você falhar**, discuta com o Narrador (ou com o jogador defensor,     caso exista) se isso é uma falha ou um sucesso a um custo maior     ([página XX](#sucesso-a-um-custo-maior))
  - **Se você empatar**, é um sucesso a um custo menor ([página XX](#sucesso-a-um-custo-menor)) - você se     encontra em uma enrascada, a sua oposição ganha um Impulso ([página     XX](#impulsos)) ou você pode sofrer algum dano. Alternativamente você falha, mas     ganha um Impulso.
  - **Se você obtém sucesso**, você consegue o que deseja, e a história     segue em frente sem problemas.
  - **Se você obtém sucesso com estilo**, é um sucesso e você também ganha    um Impulso.

> Carlos se dirigiu até o centro de pesquisa na Antártica. As edificações foram destruídas e os ocupantes desapareceram. Ele deseja procurar por pistas nos destroços. O narrador diz a ele para rolar Investigar contra uma dificuldade Razoável (+2). Carlos obtém [0][0]\[+\]\[+\] nos dados, e somando sua perícia Investigar de nível Regular (+1), resulta em um esforço Bom (+3). Um sucesso\! O narrador descreve a pista que ele encontrou: pegadas na neve, feitas por criaturas que andam sobre pernas finas e inumanas.

Ações de Superar são frequentemente usadas para determinar se um personagem pode acessar ou notar um fato ou pista em particular. Fique de olho nas opções de sucesso a um custo quando for o caso. Se perder um detalhe fizer sua história estagnar, não siga com o resultado de falha, mas sim o sucesso a um custo.

### Criar Vantagem

> _Crie um aspecto de situação ou ganhe um benefício a partir de um aspecto existente._

Você pode usar a ação ***Criar Vantagem*** para mudar o curso da história. Ao usar suas perícias para introduzir novos aspectos ou adicionar invocações em aspectos existentes, você pode virar a maré a seu favor e de seus companheiros de equipe. Você pode mudar as circunstâncias (barrando uma porta ou criando um plano), descobrir  informações adicionais (aprendendo o ponto fraco de um monstro através de pesquisa), ou se aproveitar de algo conhecido (como o gosto de um CEO por uísque).

Um aspecto criado (ou descoberto) através de criar vantagem funciona como qualquer outro: ele define as circunstâncias da narrativa e pode permitir, prevenir ou impedir ações, por exemplo: você não pode ler um feitiço se a sala estiver em _Escuridão Total_. Isso também pode ser invocado ([página xx](#invocações)) ou forçado ([página xx](#forçando). Além disso, criar vantagem garante uma ou mais ***invocações gratuitas*** do aspecto criado. Uma invocação gratuita, como o nome indica, permite que você invoque um aspecto sem gastar um ponto de destino. Você pode inclusive permitir que seus aliados usem as invocações gratuitas que você criou. 

Ao rolar para criar vantagem, especifique se você está criando um novo aspecto ou tirando vantagem de um existente. No primeiro caso, você vincula o aspecto a um aliado, oponente ou ao ambiente? Se você o vincular a um oponente, ele pode usar a ação Defender para se opor a você. Caso contrário, você frequentemente enfrentará uma dificuldade estática, mas o narrador pode decidir se algo ou alguém se opõe aos seus esforços com uma rolagem de defesa.

Os resultados ao criar um aspecto são:

  - **Se você falhar**, você não cria o aspecto (falha) ou você o cria, mas     é o inimigo que ganha uma invocação gratuita (sucesso a um custo).     Se você for bem sucedido a um custo, o aspecto final talvez deva ser     reescrito para beneficiar o inimigo. Isso ainda pode valer a pena,     já que aspectos são verdades ([página xx](#aspectos-são-sempre-verdades)).
  - **Se você empatar**, você não cria o aspecto, mas ganha um impulso     ([página xx](#impulsos)).
  - **Se você obtém sucesso**, você cria um aspecto de situação com uma     invocação gratuita nele.
  - **Se você obtém sucesso com estilo**, você cria um aspecto de situação e     ganha *duas* invocações gratuitas nele.

Sobre aspectos existentes conhecidos ou desconhecidos, os resultados são:

  - **Se você falhar**, e o aspecto for conhecido, o inimigo ganha uma     invocação gratuita. Se for desconhecido, eles podem escolher    revelá-lo para ganhar uma invocação gratuita.
  - **Se você empatar**, você ganha um impulso se o aspecto for desconhecido     e ele se mantém desconhecido. Se o aspecto for conhecido, você ganha     uma invocação gratuita.
  - **Se você obtém sucesso**, ganhe uma invocação gratuita no aspecto,    revelando-o caso for desconhecido.
  - **Se você obtém sucesso com estilo**, ganhe duas invocações gratuitas    também revelando-o caso for desconhecido.

> Edson está cara a cara com um Shoggoth, uma incansável e enorme fera carnuda. Ele sabe que a criatura é muito poderosa para atacá-la diretamente, então ele decide que é melhor distraí-la, ele anuncia: “Eu gostaria de fazer um coquetel molotov para atear fogo nessa coisa\!”. 
> 
> O narrador decide que acertar o Shoggoth é algo trivial, então essa é uma rolagem de Ofícios - com que rapidez ele pode encontrar e transformar algo inflamável? A dificuldade é definida em Bom (+3). Edson têm Ofício Mediano (+1), mas rolou \[0\]\[+\]\[+\]\[+\], garantindo um esforço de Ótimo (+4).
>
> Edson monta o molotov e o arremessa na besta. O shoggoth está agora _“Em Chamas”_ e Edson ganha uma invocação gratuita nesse aspecto. O Shoggoth está definitivamente distraído, e caso tente correr atrás dele, Edson pode usar essa invocação gratuita para ajudá-lo a escapar.

### Atacar

> _Ataque para ferir o inimigo._

A ação ***Atacar*** é como você tenta tirar um oponente de jogo, seja tentando matar um monstro repulsivo ou nocautear um guarda inocente que não sabe a verdade sobre o que está protegendo. Um ataque pode ser: descarregar uma metralhadora, golpear com um belo soco ou realizar um nefasto feitiço.

Tenha em mente se é possível ferir seu alvo ou não. Nem todo ataque é igual. Você não pode simplesmente socar um kaiju esperando machucá-lo. Determine se o seu ataque teria ao menos uma chance de ser bem sucedido antes de rolar os dados. Vários seres poderosos podem ter fraquezas específicas que precisarão ser exploradas ou alguns tipos de defesa que você terá que superar antes de começar a feri-los.

Os resultados de um ataque são:

  - **Se você falhar**, você erra. O ataque é defendido, esquivado ou     simplesmente absorvido pela armadura.
  - **Se você empatar**, talvez você acertou por um triz fazendo o defensor     recuar. De qualquer forma, você ganha um impulso ([página xx](#impulsos))
  - **Se você obtém sucesso**, você causa um golpe igual à diferença entre o     total do seu ataque contra o esforço da defesa. O defensor deverá    absorver esse golpe com estresse ou consequências ou será tirado de    ação.
  - **Se você obtém sucesso com estilo**, você golpeia exatamente como um    sucesso normal, mas você poderá reduzir a tensão da rolagem em um    para ganhar um impulso.

> Raquel encontrou por acaso um cadáver ressurreto por poderes arcanos para cumprir algum propósito sombrio. Ela decide golpeá-lo. Ela tem Lutar em Ótimo (+4), mas rolou \[-\]\[-\]\[0\]\[0\], gerando um esforço Razoável (+2).

### Defender

> _Defenda-se para sobreviver a um ataque ou para interferir na ação de um oponente._

Um monstro tenta comer seu rosto? Um inimigo tenta empurrar você para fora do caminho enquanto ele foge da sua fúria? E sobre o cultista que tenta esfaquear seus rins? ***Defender***, defender, defender...

Defender é a única ação reativa no ***Fate Condensado***. Você usa Defender para impedir que algo aconteça com você fora do seu turno, portanto você frequentemente enfrentará uma rolagem de oposição ao invés de uma dificuldade definida. Seu inimigo faz uma rolagem, e você rola imediatamente para se defender, desde que você seja o alvo da ação e/ou possa justificar sua oposição (o que geralmente faz de você o alvo). Aspectos e Façanhas podem oferecer justificativa.

Os resultados ao se defender são:

  - **Se você falhar** contra um ataque, você recebe dano, que você deverá     absorver com estresse ou consequência ([página XX](#estresse)). De qualquer modo,     seu oponente foi bem-sucedido em realizar o que desejava, conforme a    ação usada.
  - **Se você empatar**, siga as orientações para empate da ação oposta.
  - **Se você obtém sucesso**, você não sofre dano ou nega a ação da     oposição.
  - **Se você obtém sucesso com estilo**, você não sofre dano, nega a ação     da oposição e recebe um impulso por ter conseguido uma vantagem por     um momento.

> Continuando o exemplo anterior, o cadáver tenta se defender do ataque de Raquel. O Narrador rola \[-\]\[0\]\[0\]\[+\], o que não muda o nível de Atletismo Medíocre (+0) da criatura.
>
> Como o esforço de Raquel foi maior, seu ataque é bem sucedido por duas tensões, e o cadáver está um pouco mais perto de cair e nunca mais se levantar. Tivesse o cadáver rolado melhor, então a defesa teria sido bem-sucedida, e a monstruosidade desmorta teria evitado sofrer o dano.

#### Quais perícias podem ser usadas para Atacar e Defender?

A lista de perícias padrão trabalha com as premissas abaixo:

  - Lutar e Atirar podem ser usadas para realizar ataques físicos;
  - Atletismo pode ser usada para se defender de qualquer ataque físico;
  - Lutar pode ser usada para se defender contra Ataques Físicos corpo a     corpo;
  - Provocar pode ser usada para realizar ataque mental;
  - Vontade pode ser usada para se defender de ataques mentais.

Outras perícias podem receber permissão para Atacar ou Defender dentro de circunstâncias especiais, conforme determinado pelo Narrador ou por um consenso da mesa. Algumas façanhas podem conceder permissão garantida e mais ampla em certas circunstâncias em que geralmente não poderiam ser realizadas. Quando uma perícia não pode ser usada para Atacar ou Defender, mas pode ajudar com isso, prepare o caminho usando ações de Criar Vantagem com ela, e então use as Invocações gratuitas obtidas no seu próximo Ataque ou Defesa. 

# Aspectos E Pontos De Destino

Um ***aspecto*** é uma palavra ou frase que descreve algo de especial sobre uma pessoa, lugar, coisa, situação ou grupo. Quase tudo que você possa pensar pode ter aspectos. Uma pessoa pode ter uma reputação como *O Maior Atirador do Deserto* (veja abaixo para saber mais sobre esses tipos de aspectos). Uma sala pode estar *Em Chamas* após você derrubar um lampião. Após um encontro com um monstro, você pode estar *Aterrorizado*. Aspectos permitem que você mude a história de acordo com as tendências, perícias ou problemas do seu personagem.

## Aspectos são sempre verdades

Você pode invocar aspectos para um ganhar um bônus numa rolagem ([página xx](#invocações)) e forçá-los para criar uma complicação ([página xx](#forçando)). Mas mesmo quando estas mecânicas não estão sendo acionadas, aspectos ainda afetam a narrativa. Quando você tem aquela monstruosidade distorcida *Presa em uma prensa hidráulica*, isso é uma *verdade*. Ela não pode fazer muita coisa presa ali, e ela não sairá tão facilmente.

Em essência, “aspectos são sempre verdades” significa que aspectos **permitem ou impedem que certas coisas aconteçam na história** (eles também podem afetar a dificuldade: veja [página xx](#definindo-dificuldade-e-oposição). Se a monstruosidade mencionada anteriormente está *presa*, o narrador (e todos no jogo) devem respeitar isso. A criatura perdeu a permissão de se mover até que algo aconteça e remova esse aspecto, seja com um sucesso em superar (que talvez exija um aspecto justificável como *força sobre-humana*) ou alguém tolamente revertendo a prensa. Do mesmo modo, se você tem *Pernas melhoradas ciberneticamente*, você argumentativamente ganhou permissão para saltar muros em um único pulo, sem a necessidade de ter que rolar para isso.

Isso não quer dizer que você pode criar qualquer aspecto que quiser e forçar a barra para utilizá-lo. Aspectos fornecem muito poder para mudar a história, sim, mas com poder vem a responsabilidade de jogar dentro dos limites da história. Aspectos devem se alinhar com o senso crítico de todos na mesa sobre o que é realmente possível. **Se um Aspecto não passar por esse crivo, ele precisa ser reformulado.**

Claro, você *pode* usar uma ação de Criar Vantagem para colocar o aspecto *Desmembrado* naquele super-soldado cheio de fungos, mas isso com certeza esbarra na ação Atacar, e além disso, dá muito mais trabalho arrancar o braço dele (embora esse Aspecto funcione melhor como uma Consequência - veja a página a seguir). Você *pode* dizer que é o *Melhor atirador do mundo*, mas você precisará comprovar isso com suas perícias. E por mais que você queira ser *A prova de balas*, removendo a permissão de alguém usar armas de fogo leves para te atacar, isso provavelmente
não irá colar, a menos que o jogo que você está jogando envolva usar aspectos como super poderes.

## Que tipo de aspectos existem?

Há uma incontável variedade de aspectos (veja [página XX](#outros-tipos-de-aspectos) para outros tipos), mas não importa como sejam chamados, todos funcionam basicamente da mesma maneira. A principal diferença é por quanto tempo eles ficam em jogo antes de sumirem.

### Aspectos de personagem

Esses Aspectos estão na sua ficha de personagem, como seu Conceito e Dificuldade. Eles descrevem traços de personalidade, detalhes importantes sobre seu passado, relacionamentos que você tem com outros personagens, itens ou títulos importantes que você possui, problemas com os quais você tem que lidar, objetivos que você está tentando alcançar, ou reputações ou obrigações das quais você tem que lidar. Esses Aspectos mudam principalmente durante os Marcos ([página XX](#marcos)).

**Exemplos:** *Líder de um Grupo de Sobreviventes; Atenção aos Detalhes; Devo Proteger meu Irmão.*

### Aspectos de Situação

Esses Aspectos descrevem o ambiente ou o cenário onde a ação está acontecendo. Um Aspecto de Situação em geral desaparece no final da cena da qual faz parte, ou quando alguém realiza alguma ação que possa mudá-lo ou eliminá-lo. Essencialmente, eles duram apenas enquanto a situação que eles representam exista.

**Exemplos:** *Em Chamas; Luz Solar Brilhante; Multidão Furiosa; Caído no Chão; Perseguido pela Polícia.*

### Consequências

Esses Aspectos representam ferimentos e outros traumas duradouros sofridos ao absorver um dano, geralmente por meio de ataques ([página XX](#consequências-1)).

**Exemplos:** *Tornozelo Torcido; Concussão; Insegurança Debilitante.*

### Impulsos

O ***Impulso*** é um tipo especial de aspecto, que representa uma situação extremamente temporária ou pontual. Você não pode forçar um Impulso ou gastar Pontos de Destino para invocá-lo. Você pode invocá-lo apenas uma vez gratuitamente, após isso ele desaparece. Um Impulso não utilizado desaparece tão logo a vantagem que ele representa desapareça, o que normalmente dura alguns segundos ou a duração de uma única ação. Eles nunca duram além do final da cena, e você pode adiar a nomeação de um até que você o use. Se você estiver no controle de um Impulso, você pode passá-lo para um aliado se isso fizer algum sentido.

**Exemplo:** *Na mira, Distraído, Desequilibrado*

## O que posso fazer com os Aspectos?

### Ganhando Pontos de Destino

Uma forma de ganhar Pontos de Destino é permitindo que os seus Aspectos de Personagem sejam ***Forçados*** ([Página XX](#forçando)) contra você para complicar a situação ou tornar a sua vida mais difícil. Você também pode obter um ponto de destino se alguém usar seu aspecto contra você em uma invocação hostil (abaixo) ou quando você conceder ([página XX](#concedendo)).

Lembre-se: a cada sessão, você também começa com Pontos de Destino no mínimo iguais a sua ***Recarga***. Se você foi Forçado em seus Aspectos mais do que os Invocou na sessão anterior, você começará a próxima sessão com mais pontos. Consulte a [página XX](#recarga) para detalhes.

### Invocações

Para desbloquear o verdadeiro poder dos aspectos e fazer que eles te ajudem, você precisará gastar ***pontos de destino*** para ***invocá-los*** durante as rolagens de dados ([página xx](#aspectos-são-sempre-verdades)). Controle seus pontos de destino com moedas, marcadores, fichas de poker ou qualquer outra coisa. 

Você também pode invocar aspectos gratuitamente *caso* você tenha invocações gratuitas que você, ou um aliado, tenha criado com a ação de criar vantagem ([página XX](#criar-vantagem).

#### O truque das reticências

Se quiser uma maneira fácil de garantir que você tenha espaço para incorporar aspectos em uma rolagem, tente narrar suas ações de modo a incluir reticências no final delas (“...”) e então termine a ação invocando os aspectos desejados, como abaixo:

> Ricardo diz, “Então eu tento decifrar as runas e...” (rola os dados, odeia o resultado) “*e ..._Se Eu Não Estive Lá, Eu Li a Respeito_...*” (gasta um ponto de destino) “...então eu facilmente começo a divagar sobre sua origem".

#### Invocações Hostis

Na maioria das vezes, um aspecto invocado será de personagem ou de situação. Às vezes você invocará aspectos de adversários (ou aspectos de situação ligados aos mesmos) contra eles. Isso é chamado de ***invocação hostil e funciona*** da mesma forma que invocar qualquer aspecto: pague um ponto de destino e receba +2 na sua rolagem ou role novamente os dados. Existe uma pequena diferença: **quando você faz uma invocação hostil, você passa o Ponto de Destino para a oposição**. Porém eles não podem usar o ponto de destino até que a cena esteja encerrada. Esse pagamento *só ocorre* quando há o gasto de um Ponto de Destino para invocações hostis. Invocações gratuitas não geram um pagamento.

#### Invocações para Declarar Detalhes Narrativos

Você pode adicionar um detalhe importante ou improvável à história baseado em um Aspecto em Jogo. Não gaste pontos de destino quando “aspectos são sempre verdades” ([página xx](#aspectos-são-sempre-verdades)) se aplica. Pague apenas quando você quiser *exagerá-los*, ou quando a mesa quiser e não houver um Aspecto mais relevante.

### Forçando

Aspectos podem ser ***forçados*** para complicar a situação e com isso ganhar Pontos de Destino. Para Forçar um Aspecto, o Narrador ou um jogador oferece um Ponto de Destino ao jogador cujo personagem tem um aspecto sendo forçado e diz a ele porque aquele Aspecto está tornando as coisas mais difíceis ou complicadas. Se ele recusar a ter seu aspecto forçado, é ele quem tem que pagar um Ponto de Destino e explicar como o personagem evita a complicação. Sim, isso significa que se você não tem nenhum Ponto de Destino, você não pode evitar que seu aspecto seja forçado\!

**Qualquer Aspecto pode ser Forçado**, seja um Aspecto de Personagem, aspecto de Situação, ou Consequência, mas apenas se isso afetar o personagem que tem o aspecto forçado.

**Qualquer um pode propor forçar um aspecto**. O jogador que propõe forçar aspecto precisa gastar um dos seus Pontos de Destino. O Narrador então assume controle sobre o forçar aspecto contra o alvo afetado. O Narrador não perde Pontos de Destino ao propor forçar aspecto, ele tem uma reserva limitada de Pontos de Destino para Invocar Aspectos, mas ele pode Forçar quantos Aspectos quiser.

**Forçar pode ser retroativo**. Se um jogador perceber que se colocou em uma complicação relacionada a um dos seus Aspectos, ou em Aspectos de Situação que o afete devido à sua interpretação, ele pode perguntar ao Narrador se isso conta como **forçar aspecto contra si mesmo**. Se o grupo concordar, o Narrador passa ao jogador um Ponto de Destino. 

**Está tudo bem em reconhecer que uma proposta de forçar aspecto não faz muito sentido e decidir desistir de fazê-lo**. Se o grupo concordar que uma proposta de forçar aspecto não é apropriada, ela deve ser abandonada sem que o personagem que a propôs tenha que pagar um Ponto de Destino.

#### Forçar aspectos gera complicações, não impedimentos.

Ao propor forçar um aspecto, certifique-se de que a complicação é um curso de ação ou uma mudança importante nas circunstâncias, não uma negação das opções.

*“Você tem areia nos olhos, então você atira na criatura e erra”*, não é forçar aspecto, pois ele apenas nega ações ao invés de complicar as coisas.

*“Sabe, maldita sorte, acho que a areia no seus olhos significa que você realmente não consegue ver nada. Você atira a esmo contra o Shoggoth, perfurando alguns barris que agora estão jorrando gasolina em direção à fogueira”*. Esse é um forçar aspecto muito melhor. Ele muda a cena, eleva a tensão e dá algo novo para os jogadores pensarem.

Para mais ideias sobre o que pode ou não funcionar como um forçar aspecto, veja a discussão sobre as formas de forçar no ***Fate Básico*** começando na página 65 desse livro, ou online em http://fatesrdbrasil.gitlab.io/fate-srd-brasil/fate-basico/invocando/\#formas-de-for%C3%A7ar

#### Eventos e Decisões

Em geral, existem duas formas de forçar: ***Eventos*** e ***Decisões***.

Forçar um aspecto baseado em um evento é algo que acontece a um personagem por causa de uma força externa. Essa força externa se relaciona ao Aspecto de alguma forma, resultando em uma complicação infortúnia.

Forçar um aspecto em uma decisão é algo interno, onde as fraquezas ou valores contraditórios do personagem interferem em seu julgamento. O Aspecto guia o personagem a tomar uma decisão em particular e a consequência dessa decisão gera uma complicação para ele.

De qualquer modo, a chave é que o resultado seja uma complicação\! Sem complicação, não há como forçar um aspecto.

#### Invocações Hostis ou Forçar Aspectos?

Não confunda invocações hostis e forçar aspectos\! Embora sejam semelhantes, ambas são maneiras de colocar o personagem em um problema imediato em troca de um ponto destino, elas funcionam de formas diferentes.

Forçar cria uma *mudança narrativa*. A decisão de Forçar um Aspecto não é algo que acontece dentro do universo de jogo, em vez disso, é o Narrador ou Jogador propondo uma mudança na história. Esse efeito pode ser amplo, mas o alvo obtém o Ponto de Destino imediatamente se aceitar ter seu aspecto forçado e pode escolher recusá-lo.

Uma Invocação Hostil é um *efeito mecânico*. O Alvo não tem a oportunidade de recusar a invocação, mas como em qualquer invocação, você precisará explicar como a invocação desse aspecto faz sentido. E embora o jogador obtenha um Ponto de Destino, ele não pode ser usado na cena atual. No entanto, o resultado final é mais restrito: um bônus de +2 na rolagem ou rolar novamente os dados uma vez.

Forçar permite que você, como jogador ou narrador, mude *o que se trata em uma cena*. Elas mudam o rumo da narrativa. Usá-la contra um oponente é uma proposta arriscada: ele pode recusar ou pode atingir seu objetivo apesar da complicação, graças ao novíssimo Ponto de Destino que você acabou de lhe dar.

Invocações Hostis o ajudam a lidar com o momento atual. Além dos seus próprios Aspectos, você tem os aspectos do seu oponente para invocar, dando a você mais opções e tornando as cenas mais dinâmicas e conectadas.

### Como posso adicionar ou remover Aspectos?

Você pode criar ou descobrir um Aspecto de Situação usando a ação de Criar Vantagem ([página XX](#criar-vantagem)). Você também pode criar Impulsos dessa forma, ou como resultado de um empate ou sucesso com estilo em ações de Superar um obstáculo, Atacar ou Defender.

Você pode remover um Aspecto desde que você possa pensar em uma maneira do seu personagem fazer isso, abrir o extintor de incêndio contra *Chamas ardentes*, usar manobras evasivas para escapar do guarda que está *Na sua cola* te perseguindo. Dependendo da situação, isso pode exigir uma Ação de Superar (página XX); nesse caso, os oponentes poderiam usar uma ação de defender para tentar manter o Aspecto, se eles puderem descrever como o farão.

Entretanto, se não houver nenhuma limitação narrativa para impedir a remoção do Aspecto, você pode simplesmente fazê-lo. Se você estiver *Amarrado dos pés à cabeça* e um amigo cortar essas amarras, o aspecto desaparece. Se nada estiver te impedindo, não há necessidade de realizar rolagens.

### Outros tipos de Aspectos

Cobrimos os tipos de Aspectos padrões na [página XX](#que-tipo-de-aspectos-existem). Os tipos a seguir são opcionais, mas podem agregar valor ao seu jogo. Até certo ponto, esses Aspectos são variantes dos Aspectos de Personagem (se você expandir sua noção do que conta como um personagem) e Aspectos de Situação (se você mudar sua noção de quanto tempo eles duram).

**Aspectos de Organização:** às vezes você terá que lidar com uma organização inteira que opera sob uma série de princípios. Considere dar à organização Aspectos que qualquer membro possa acessar como se fossem seus próprios.

**Aspectos de Cenas:** Às vezes, uma trama específica pode introduzir temas que aparecem de tempos em tempos na história. Considere definir isso como um Aspecto que estará disponível para todos os personagens da história até que essa parte da história seja concluída.

**Aspectos de Cenário:** Como um Aspecto de Cena, o Cenário de sua campanha pode ter temas recorrentes. Ao contrário de um Aspectos de Cena, esses Aspectos não desaparecem.

**Aspectos de Zona:** você pode incluir Aspectos de Situação a um determinado lugar do mapa representado por uma zona ([página XX](#zonas)). Isso pode adicionar um dinamismo extra às interações do grupo com o mapa.  O Narrador pode encorajar isso disponibilizando uma Invocação Gratuita “disponível para pegar” em um Aspecto de Zona no início de uma cena, atraindo os personagens (jogadores ou não) a tirar proveito desse Aspecto como parte de sua estratégia inicial.

# Desafios, Disputas e Conflitos

Muitas vezes durante uma cena, você será capaz de resolver uma ação com uma única rolagem de dados: você arromba o cofre, evita a segurança ou convence o repórter a lhe dar suas anotações? Em outros casos, você enfrentará eventos prolongados que exigem mais rolagens para serem resolvidos. Para esses casos, oferecemos três ferramentas de resolução: ***Desafios, Disputas*** e ***Conflitos***. Cada uma funciona de maneira um pouco diferente, dependendo do tipo de evento e da oposição envolvida.

  - **Um Desafio é uma situação complicada ou dinâmica**. Você terá oposição     de algo ou alguém, mas não existe um “outro lado” dominante. É assim     que você poderia interpretar um pesquisador procurando por pistas em     tomos antigos, o negociador do grupo distraindo o bibliotecário e o     brutamontes impedindo que horrores nomináveis invadam a biblioteca,     tudo ao mesmo tempo.
  - **Uma Disputa é uma situação onde dois ou mais grupos buscam objetivos    mutuamente exclusivos, mas não prejudicam ativamente um ao outro.**    Disputas são perfeitas para perseguições, debates e corridas de    todos os tipos. E o fato das partes envolvidas não estarem tentando    ferir uma à outra, não significa que elas não possam eventualmente    sofrer danos\!
  - **Um Conflito é quando os personagens podem e querem ferir uns aos    outros.** Lutar na lama com um cultista enquanto facadas são    desferidas nas barrigas, perfurar com uma rajada de balas uma horda    de carniçais enquanto eles arranham sua carne com suas garras e uma    troca de farpas maledicentes com seu rival sob o olhar atento da    rainha. Todas essas cenas são conflitos.

## Definindo as Cenas

Independentemente do tipo de cena, o Narrador começa definindo as peças essenciais no seu lugar, de modo que os jogadores saibam quais recursos estão disponíveis e quais complicações estão em jogo.

### Zonas

As ***zonas*** são uma representação do espaço físico: um mapa simples dividido em algumas sessões específicas. Um conflito em uma remota casa de fazenda pode ter quatro zonas: primeiro andar, segundo andar, jardim de frente e bosque aos fundos. Duas a quatro zonas normalmente são o suficiente para lidar com a maioria dos conflitos. Cenas maiores ou mais complexas podem exigir mais zonas. Tente manter seu mapa de zonas em esboço simples, algo que caiba em um cartão de anotações ou possa ser facilmente desenhado em um quadro branco.

As zonas ajudam a guiar a história moldando o que é possível. Quem você pode atacar e para onde você pode se mover dependem da zona onde esteja.

***Qualquer um em uma zona pode interagir com todos ou qualquer coisa dentro dela.*** Isto significa que você pode bater, esfaquear ou se envolver fisicamente com pessoas ou coisas em sua zona. Precisa abrir aquele cofre de parede no quarto? Você deverá estar nessa mesma zona. Qualquer coisa fora da sua zona geralmente está fora do seu alcance; você precisará se mover para lá ou usar algo que possa estender seu alcance até lá (telecinese, uma pistola, etc).

Mover-se entre zonas é fácil, desde que não haja nada em seu caminho. **Você pode se mover para uma zona adjacente além da sua ação durante seu turno ([página xx](#ordem-de-turnos)), desde que nada esteja em seu caminho.** Se sua movimentação está obstruída, será necessário usar sua ação para se mover. Faça uma rolagem de superar para escalar uma parede, correr por um grupo de cultistas ou saltar pelos telhados. Caso falhe, você ficará em sua zona ou mova-se ao custo de algo. Você também pode usar sua ação para se mover para *qualquer lugar* no mapa, entretanto o narrador tem o direito de definir uma dificuldade alta se for um movimento épico.

Se algo não é arriscado ou interessante o suficiente para merecer uma rolagem, então não é um impedimento ao seu movimento. Por exemplo, você não precisa usar sua ação para abrir uma porta destrancada, pois isso já faz parte do movimento.

Atirar te permite atacar à distância. Ataques desse tipo podem atingir inimigos em zonas adjacentes ou até mais longe, caso as zonas não ofereçam obstáculos no caminho. Se há uma criatura revirando o quarto de cima, localizado no fim do corredor, você não pode atirar estando ao pé da escada. Preste atenção na maneira como as zonas e os aspectos de situação são definidos para decidir o que é justo no jogo ou não.

### Aspectos de Situação

Ao definir uma cena, o narrador deverá pensar em características ambientais interessantes e dinâmicas que restrinjam a ação ou forneçam oportunidades para mudar as situações ao usá-las. Três a cinco detalhes são mais que suficientes. Use essas categorias como um guia:

  - **Tom, humor ou clima** - escuridão, relâmpagos ou ventos uivantes;
  - **Restrições à movimentação** - escadas conectando as zonas, coberto de     lodo e cheio de fumaça;
  - **Coberturas e obstáculos** - veículos, pilares ou caixotes;
  - **Características perigosas** - caixas de TNT, barris de petróleo e     estranhos artefatos estalando com eletricidade;
  - **Objetos utilizáveis** - armas improvisadas, estátuas ou estantes de    livros para derrubarem ou portas para bloquearem.

Qualquer um pode invocar e forçar esses aspectos, então lembre-se de levá-los em conta quando lutar com aquele cultista pelo chão no meio do *Lodo Cáustico que Cobre Tudo*.

Mais aspectos de situação podem ser escritos à medida que a cena se desenrola. Se fizer sentido que haja *Sombras Profundas* nas alcovas das catacumbas, vá em frente e escreva isso quando um jogador perguntar se existem sombras que eles podem usar para se esconder. Outros aspectos entram em jogo porque os jogadores usam a ação criar vantagem. Coisas como *Chamas Por Todos os Lados* não acontecem simplesmente sem ação de um personagem. Bem, geralmente não.

#### Invocações gratuitas nos aspectos de cena?

Cabe ao narrador decidir se um aspecto de situação que surge da definição da cena fornecerá uma invocação gratuita aos jogadores (até mesmo aos PdN). Alguns dos aspectos de cena podem fornecer a um esperto jogador justamente a vantagem de que ele precisa naquele momento, e uma invocação gratuita pode ser um grande incentivo para levar os  jogadores a interagir com o ambiente. Invocações gratuitas também podem estar nos aspectos de cena desde o começo dela, devido a preparações feitas com antecedência.

#### Aspectos de Zonas

Conforme mencionado na (página xx)[#zonas], alguns aspectos de situação podem se aplicar a zonas específicas no mapa e não em outras. Assim adiciona-se uma textura extra, oportunidade e desafio ao mapa que talvez fariam falta.

### Ordem de turnos

Geralmente, você não precisará saber quem está agindo e precisamente quando, mas em disputas e conflitos a ordem de turnos pode se tornar importante. Essas cenas ocorrem em uma série de ***turnos***. Em um turno, cada personagem envolvido pode realizar uma ação de Superar, Criar Vantagem ou Atacar, e pode se mover uma vez (as disputas funcionam de maneira um pouco diferente, veja na [página XX](#disputas)). Como Defender é uma reação à ação de outra pessoa, personagens podem se defender quantas vezes for necessário durante os turnos de outros personagens, desde que possam justificar sua capacidade de interferir baseado no que já foi estabelecido na história.

No início de uma cena, o Narrador e os jogadores decidem quem será o primeiro a agir baseados na situação e então o jogador ativo escolhe quem vai em seguida. Os personagens do Narrador agem na ordem de turnos, assim como os Jogadores, com o Narrador decidindo quem vai em seguida após os PdN terem agido. Depois que todos jogaram seu turno, o último jogador escolhe quem vai agir em seguida no início da próxima rodada.

> Carolina e Raquel descobriram por acaso um pequeno grupo de cultistas, liderados por um acólito com uma máscara dourada, realizando algum ritual arcano. Como os cultistas estão focados em seu trabalho, o Narrador declara que os jogadores agirão primeiro nesse conflito. Os jogadores decidem que Carolina agirá primeiro: ela cria uma vantagem contra o cultista mascarado, correndo e gritando na direção deles. Agora ele está *Distraído*. Simples, mas eficaz. Para que esse Aspecto de Situação seja melhor usado, o jogador de Carolina decide que Raquel deve agir em seguida. Raquel arremessa uma adaga contra o acólito mascarado e imediatamente invoca o Aspecto *Distraído* para melhorar seu ataque. Não é o suficiente para tirar o acólito de ação com um só golpe, mas essa jogada dupla deixa o cultista cambaleando.
>
> Infelizmente, agora que todos os PJs na cena agiram, Ruth não tem escolha a não ser escolher um dos cultistas para agir em seguida. Ela escolhe o acólito mascarado. O Narrador sorri, pois sabe que assim que o acólito agir, ele pode fazer os cultistas agirem até o final da rodada, e então eles poderão decidir que o acólito mascarado inicie a próxima rodada. Os PJs podem ter acertado um bom primeiro golpe, mas agora os cultistas podem revidar.

Esse método de determinar a ordem de turnos tem vários nomes em discussões online: Ordem de Turnos Eletiva, Iniciativa Pipoca, Passar o Bastão ou “Estilo Balsera”, a última em homenagem a Leonard Balsera, um dos autores do ***Fate Básico***, que plantou a semente dessa ideia. Você pode aprender mais sobre esse método e suas estratégias (em inglês) em <https://www.deadlyfredly.com/2012/02/marvel/>

## Trabalho em Equipe

O Fate oferece três métodos para o trabalho em equipe: combinar a mesma perícia de vários personagens em uma única rolagem, empilhar invocações gratuitas criando vantagens para facilitar o êxito de um membro da equipe, e invocar Aspectos a favor de um aliado.

Ao combinar perícias, descubra quem tem o melhor nível da perícia entre todos os envolvidos. Cada participante adicional que tenha ao menos Regular (+1) nessa perícia adiciona +1 ao nível de perícia do personagem com perícia mais alta. Oferecer suporte dessa maneira usa sua ação do turno. Os apoiadores sofrem os mesmos custos e consequências da pessoa que realiza a rolagem de dados. O bônus total máximo que uma equipe pode fornecer dessa forma é igual ao nível mais alto de perícia da pessoa com maior pontuação.

De outro modo, você pode criar uma vantagem em seu turno e permitir que um aliado use as invocações gratuitas quando fizer sentido em utilizá-las. Fora do seu turno, você pode invocar um Aspecto para adicionar um bônus nas rolagens de outros personagens.

## Desafios

Muitas das dificuldades que seus personagens irão encarar durante uma cena podem ser resolvidas com apenas uma rolagem: desarmar uma bomba, encontrar o tomo da sabedoria anciã ou decifrar um código. Mas às vezes as coisas são mais fluídas, mais complicadas, e não tão simples quanto encontrar o tomo da sabedoria anciã porque o iate que você está procurando está ancorado no porto de Hong Kong enquanto uma ventania furiosa está do lado de fora e a biblioteca do barco está pegando fogo, algo que claramente não é sua culpa.

Em circunstâncias complicadas, sem oposição, você pode usar um ***desafio***: uma série de rolagens de superar que tentam lidar com um problema maior. Os desafios permitem que todo o grupo trabalhe em conjunto em uma cena e
mantém as coisas dinâmicas. 

Para definir um Desafio, o Narrador considera a situação e escolhe uma série de perícias que podem contribuir para o sucesso do grupo. Trate cada ação como uma rolagem separada de Superar. As ações de trabalho em equipe são permitidas, mas podem trazer custos e complicações, como falta de tempo ou resultados ineficazes.

Narradores, façam o possível para dar a cada personagem na cena uma oportunidade de contribuir. Foque em um número de perícias a serem usadas igual ao número de personagens envolvidos. Reduza esse número se você acha que alguns dos personagens poderão ser retirados de ação ou distraídos por outras prioridades, ou se quiser abrir espaço para trabalho em equipe. Para desafios mais complicados, construa o desafio com mais ações necessárias do que personagens disponíveis, além de ajustar as dificuldades das ações.

Após as rolagens, o narrador avalia os sucessos, falhas e custos de cada ação conforme interpreta como a cena prossegue. Pode ser que os resultados levem a outro desafio, uma disputa ou mesmo um conflito. Uma combinação de sucessos e falhas deve permitir que os personagens avancem com uma vitória parcial enquanto enfrentam novas complicações entrelaçadas.

## Disputas

Uma ***disputa*** ocorre quando dois ou mais lados estão em oposição direta, mas não estão em um conflito. Isso não significa que um lado *não queira* ferir o outro. Um exemplo de disputa pode ser um grupo tentando escapar
de uma ameaça antes que ela impeça qualquer chance de vitória.

No começo de uma disputa, todos os envolvidos declaram suas ações e o que esperam alcançar. Se mais de um PJ estiver envolvido, eles podem estar do mesmo lado ou em lados opostos, dependendo dos objetivos, por exemplo: em uma prova de atletismo, provavelmente será cada um por si. **Em uma disputa os PJs não podem ou não estão tentando ferir o inimigo.** Ameaças externas como uma erupção de um vulcão ou a ira de um deus, podem atacar qualquer um ou todos os lados. Essas ameaças também podem estar envolvidas na disputa.

Disputas ocorrem em uma série de rodadas. Cada lado realiza uma ação de superar para realizar algo visando alcançar seu objetivo em cada turno. Apenas um personagem de cada lado realiza essa rolagem em cada turno, mas seus aliados podem tanto oferecer bônus de Trabalho em Equipe ou Criar Vantagens para ajudar, (o que traz alguns riscos - veja abaixo). As ações de superar podem ter dificuldades passivas, como no caso de estarem enfrentando dificuldades ambientais distintas ou comparando entre si os resultados quando estiverem em oposição direta.

Ao final de cada rodada, compare os esforços das ações de cada lado. O lado com maior esforço marca uma vitória. Se o vencedor tiver sucesso com estilo (e ninguém mais), ele marca duas vitórias. O primeiro lado a marcar três vitórias vence a disputa (você sempre pode decidir, em vez disso, realizar disputas extendidas que exijam mais vitórias, embora não recomendamos mais de cinco).

Quando há um empate para o maior esforço, ninguém marca vitórias e uma ***reviravolta inesperada*** acontece. O Narrador introduzirá um novo Aspecto de Situação que refletirá como a cena, terreno ou situação mudou.

Em Disputas em que uma ameaça tenta ferir qualquer um dos competidores, todos de um dos lados sofrem um golpe quando a sua rolagem for menor que a rolagem de ataque da ameaça ou a dificuldade estática. Eles sofrem tensões de dano igual às tensões da falha. Assim como em um conflito, se um personagem não consegue absorver todas as tensões de um dano, ele é tirado de ação.

### Criando Vantagens durante uma Disputa

Durante uma disputa, seu lado pode tentar criar vantagens antes de realizar sua rolagem de superar. O objetivo, ou qualquer um que possa razoavelmente interferir, pode se opor com uma rolagem de defesa normalmente. Cada participante pode tentar criar uma vantagem além de rolar ou oferecer um bônus de trabalho em equipe ([página XX](#trabalho-em-equipe)). Se você falhar ao criar vantagem, você tem uma opção: ou seu lado abdica a rolagem de superar, ou você pode sofrer um sucesso a custo (preservando sua rolagem de superar ou o bônus de trabalho em equipe) dando ao outro lado uma invocação gratuita. Se você ao menos empatar, proceda normalmente com sua rolagem ou bônus de trabalho em equipe.

## Conflitos

Quando os heróis lutam de verdade, seja com as autoridades, cultistas, ou algum horror inominável, e podem vencer, você tem um ***conflito***. Em outras palavras, use conflitos quando a violência e a coerção forem meios razoáveis para os fins dos PCs.

Conflitos podem parecer os mais diretos, afinal de contas, a história dos jogos de RPGs é toda construída em cima de simuladores de combate. Mas tenha em mente uma parte importante de sua descrição: os personagens envolvidos têm a capacidade de *ferirem uns aos outros*. Se isso for unilateral (por exemplo: digamos que você tente socar uma montanha viva) não há a menor chance de você feri-la. Isso não é um conflito. Isso é uma Disputa, provavelmente onde os personagens estão tentando fugir ou encontrar meios de contra-atacar.

Conflitos podem ser físicos ou mentais. Conflitos físicos podem ser tiroteios, duelos de espada ou o atropelamento de seres extradimensionais com caminhões. Conflitos mentais incluem discussões com entes queridos, interrogatórios e ataques profanos sobre a sua mente.

A escolha do tempo é importante ao usar algumas formas de Trabalho em Equipe ([página XX](#trabalho-em-equipe)). Você pode invocar um Aspecto em nome de seu aliado para melhorar seu resultado a qualquer momento. Você pode ajudar um aliado *antes do turno dele começar* criando uma vantagem ou dando um bônus de +1 em sua ação. Se ele agiu antes de você neste turno, você não pode criar uma vantagem para ajudá-lo, mas você pode usar seu turno (pulando esse turno) para oferecer a ele um bônus de +1 de Trabalho em Equipe.

### Recebendo dano

Quando um ataque é bem sucedido, o defensor deve absorver o dano, que é igual a diferença entre o esforço do Atacante e o esforço do Defensor.

Você pode absorver tensões de Dano marcando caixas de Estresse e sofrendo Consequências. Se você não puder ou não desejar absorver todas as tensões, você é ***tirado de ação*** ([página XX](#sendo-tirado-de-ação): você é removido de cena e o atacante é quem decide o que acontecerá.

> Uma série de decisões lamentáveis colocaram Carlos em um porão úmido, confrontando um carniçal que deseja muito devorá-lo. O Carniçal ataca golpeando com suas garras afiadas; esse Ataque usa sua perícia Lutar de nível Razoável (+2). O Narrador rola \[0\]\[0\]\[+\]\[+\], aumentando o esforço para Ótimo (+4). Carlos tenta pular fora do caminho com seu nível Bom (+3) em Atletismo, mas rola \[0\]\[0\]\[0\]\[-\] reduzindo seu esforço para Razoável (+2). Como o esforço de ataque do carniçal foi dois passos maior que o esforço de defesa de Carlos, Carlos deve absorver duas tensões. Ele marca as duas primeiras de suas três caixas de estresse físico: o combate já está se mostrando perigoso.

#### Estresse

Em poucas palavras, o ***Estresse*** é uma “armadura narrativa”. É um recurso usado para manter seu personagem ativo e em condições de lutar quando seus inimigos o atingem. Quando você marca caixas de Estresse para absorver um golpe, é como dizer “essa foi por pouco” ou “caramba, essa me deixou sem ar, mas estou bem”. Entretanto, é um recurso limitado: a maioria dos personagens tem apenas três caixas de estresse físico e três de estresse mental, embora personagens com valores mais altos de Vontade e Vigor tenham mais.

Você encontrará duas ***Barras de Estresse*** na sua ficha de personagem, uma para o dano físico e outra para dano mental. Quando você recebe um golpe, você pode marcar caixas vazias de estresse do tipo apropriado para absorver o dano e permanecer na luta. Cada caixa de estresse que você marca absorve uma tensão. Você pode marcar várias caixas de estresse, se necessário.

Essas caixas são binárias: ou estão vazias e podem ser usadas ou estão marcadas e não podem ser usadas. Mas tudo bem, você limpará as barras de estresse assim que a cena terminar (desde que os monstros não o comam primeiro).

#### Consequências

***Consequências*** são novos Aspectos que você escreve em sua ficha de personagem quando seu personagem recebe um golpe, representando o dano real e os ferimentos que seu personagem sofre.

Quando você sofre uma consequência para absorver um golpe, escreva um aspecto em uma caixa de consequência vazia que descreva o dano ocorrido ao seu personagem. Use a gravidade das consequências como um guia: se você foi mordido por uma Cria Estelar, uma consequência suave seria *Mordida feia*, enquanto uma consequência moderada seria *Mordida que não para de sangrar*, e uma consequência severa seria *Perna Aleijada*.

Enquanto o estresse transforma o golpe em um quase acerto, sofrer uma consequência significa que você foi atingido com força. Por que você sofreria uma consequência? Porque às vezes o estresse não é o bastante. Lembre-se: você tem que absorver toda a tensão do golpe para permanecer no conflito. Você só tem algumas caixas de estresse. A boa notícia é que as consequências podem absorver golpes realmente grandes.

Cada personagem começa com três caixas de consequências: uma suave, uma moderada e uma severa. Sofrer uma consequência suave absorve duas tensões, uma moderada absorve quatro tensões e uma severa absolve seis tensões.

Portanto, se você sofrer um grande golpe de cinco tensões, você poderá absorver tudo usando apenas uma caixa de estresse e uma caixa de consequência moderada. Isso é muito mais eficiente do que gastar cinco das suas caixas de estresse.

A desvantagem das consequências é que elas são aspectos, e aspectos sempre são verdades ([página xx](#aspectos-são-sempre-verdades)). Então, se você possui a consequência *Barriga Baleada*, isso significa que você não poderá fazer coisas que uma pessoa baleada na barriga não faria (como correr rápido). Se as coisas ficarem realmente complicadas devido a isso, você também pode até mesmo receber um forçar aspecto em sua consequência. E assim como os aspectos que você cria quando usa a ação Criar Vantagem, o personagem que criou a consequência, isto é, quem te atingiu, recebe uma invocação gratuita nessa consequência. Ai que dor\!

> Carlos ainda está lutando contra o carniçal. A criatura o ataca com suas garras, dessa vez conseguindo \[0\]\[0\]\[+\]\[+\], adicionando o seu Lutar Razoável (+2), e invoca seu aspecto *Fome por carne* para um adicional de +2, resultando em um golpe devastador de nível Fantástico (+6). A rolagem \[-\]\[-\]\[0\]\[0\] de Carlos, adicionado ao seu nível Bom (+3) em Atletismo, resulta apenas em uma defesa Regular (+1); por isso, ele precisa absorver cinco tensões. Ele opta por sofrer uma consequência moderada. Seu jogador e o narrador decidem que o carniçal fez uma *Ferida aberta no peito* de Carlos. Essa consequência absorve quatro das tensões, deixando uma, do qual Carlos absorve com sua última caixa de estresse restante.

#### Sendo Tirado de Ação

Se você não consegue absorver todas as tensões de um dano com estresse e consequências, você é ***tirado de ação***.

Ser tirado de ação é ruim. Quem tirou você de ação decide o que acontece. Em situações perigosas e em confrontos com inimigos poderosos, isso pode significar que você morreu, mas essa não é a única possibilidade. O resultado deve estar de acordo com o escopo e a escala do conflito em questão: você não morrerá de vergonha se perder uma discussão, mas mudanças em sua ficha de personagem (e mais) são possíveis. O resultado também deve caber dentro dos limites estipulados pelo grupo: se o grupo entende que os personagens nunca devem ser mortos sem o consentimento do jogador, isso é perfeitamente válido.

Mas mesmo quando a morte está em jogo (é melhor deixar isso claro antes de uma rolagem de dados), o Narrador deve se lembrar que isso geralmente é um resultado chato. Um PJ que foi tirado de ação pode ter sido sequestrado, se perdido, colocado em perigo, ser forçado a sofrer consequências... a lista segue. A morte de um personagem significa que alguém terá que fazer um novo personagem e trazê-lo para a história, mas um destino pior que a morte é limitado apenas pela sua imaginação.

Siga a ficção ao descrever como alguém, ou algo, é retirado de ação. Um cultista foi retirado de ação por uma rajada de tiros de metralhadora? Um borrifo de gotas vermelhas enche o ar antes de caírem molhando o chão. Você foi arremessado do caminhão quando ele cruzava o viaduto da Rua 26? Você desaparece saltando pela borda e é esquecido enquanto o conflito continua ao longo da Represa Ryan? Quando estiver discutindo os termos sobre o que ocorre com um personagem ao ser tirado de ação, lembre-se da possibilidade da morte, mas muitas vezes é mais interessante enganar a morte.

> O carniçal acerta com muita sorte, dando um ataque Lendário (+8) contra  a defesa Ruim (-1) de Carlos. Nesse ponto do conflito, todas as caixas de stress de Carlos estão cheias, assim como sua caixa de consequência moderada. Mesmo que ele tome uma consequência leve e uma consequência severa de uma só vez, absorvendo 8 tensões, não seria o suficiente. Como resultado, Carlos foi tirado de ação. O carniçal decide o seu destino. O Narrador poderia em seu direito decidir que o carniçal matasse Carlos ali mesmo... mas ser morto não é o resultado mais interessante.
> 
> Em vez disso, o Narrador declara que Carlos sobreviveu, foi nocauteado e arrastado para o covil da criatura, com as consequências intactas. Carlos acordará perdido e fragilizado no meio da escuridão das catacumbas abaixo da cidade. Como ele foi retirado de ação, Carlos não tem escolha além de aceitar os termos colocados a ele. 

#### Concedendo

Então, como você evita uma morte horrível, ou algo pior? Você pode interromper qualquer ação em um conflito para ***conceder***, desde que os dados não tenham sido rolados ainda. Apenas se renda. Diga a todos que você desiste e que você não consegue seguir adiante. O seu personagem é derrotado e é retirado de ação, mas **você ganha um Ponto de Destino** e mais um ponto extra para cada consequência sofrida nesse conflito.

Além disso, conceder significa que *você* declara os termos de sua derrota e como será tirado de ação. Você pode escapar dos monstros e viver para lutar outro dia. Entretanto, ainda é uma derrota. Você terá que dar ao seu oponente algo que ele queira. Você não pode conceder e descrever como heroicamente salvou o dia: isso não é mais uma possibilidade.

Conceder é uma ferramenta poderosa. Você pode conceder para fugir com um plano de ação pronto para a próxima batalha, uma pista de para onde ir ou alguma vantagem futura. Você simplesmente não pode vencer *essa* luta.

Você deve conceder antes que seu oponente role os dados. Você não pode esperar os resultados dos dados e conceder quando parecer óbvio que você não vai vencer: isso é ser um mau perdedor.

Espera-se alguma negociação aqui. Procure uma solução que funcione para todos na mesa. Se a oposição não concordar com os termos da sua concessão, eles podem pressionar por uma reformulação desses termos ou pedir que você sacrifique algo diferente ou algo extra. Como conceder ainda é uma derrota para você, isso significa que a oposição deve ganhar pelo menos parte do que ela busca.

Quanto mais significativo for o custo que você paga, maior será o benefício que o seu lado deverá receber como parte da concessão: se a derrota certa estiver sobre todo grupo, um membro que conceda pode optar por realizar uma última resistência heróica (e fatal), e isso poderia significar que todos os demais sejam poupados\!

### Terminando um Conflito

Um conflito chega ao fim quando todos de um lado concedem ou são retirados de ação. No final de um conflito, qualquer jogador que concedeu receberá seus pontos de destino pela concessão ([página xx](#concedendo)). O narrador também paga os devidos pontos de destino aos jogadores por invocações hostis ([página xx](#invocações-hostis)) que aconteceram durante o conflito.

### Recuperando-se de Conflitos

Ao fim de cada cena, cada personagem limpa suas caixas de estresse. As consequências levam mais tempo e esforço para serem removidas.

Para iniciar o processo de recuperação, a pessoa que está tratando você precisará ter sucesso em uma ação de superar com uma perícia apropriada. Machucados físicos normalmente são tratados por conhecimentos médicos via Conhecimentos, enquanto consequências mentais são tratadas com Empatia. Essa ação de superar enfrenta dificuldade igual a gravidade da consequência: Razoável (+2) para consequência suave, Ótima (+4) para moderada e Fantástica (+6) para severa. Estas dificuldades aumentam em dois quando você tenta tratar a si mesmo (é mais fácil ter outra pessoa fazendo isso).

Se você for bem-sucedido nesta rolagem, reescreva a consequência para indicar que ela está sendo curada. Um Braço Quebrado pode ser reescrito como Braço Engessado, por exemplo.

O Sucesso aqui é somente o primeiro obstáculo - leva tempo para recuperar de uma consequência.

  - Consequências **leves** demoram uma cena inteira após o tratamento para     serem removidas.
  - Consequências **moderadas** duram mais tempo, levando uma sessão inteira    após o tratamento para serem removidas.
  - Consequências **severas** são removidas somente quando você atinge um    progresso ([página xx](#progressos)) após o tratamento.

# Avanço

À medida que seus personagens avançam no enredo, eles crescem e mudam. No final de cada sessão, você ganhará um ***marco***, que permite modificar os elementos da ficha do seu personagem. Conforme você conclui cada arco de história, você ganha um ***progresso*** que permite adicionar novos elementos à sua ficha de personagem. (Saiba mais sobre sessões e arcos na [página xx](#sessões-e-arcos).)

## Marcos

Os Marcos acontecem ao fim de uma sessão, parte do caminho ao lidar com um arco de história. Eles são focados em ajustar seu personagem lateralmente ao invés de avançá-lo. Você pode não querer usar um marco, está tudo bem. Nem sempre faz sentido mudar seu personagem. A oportunidade está aí, caso precise.

Com um marco você pode fazer uma das opções abaixo:

  - Trocar os níveis entre duas perícias quaisquer ou substituir uma    perícia de nível Regular (+1) por uma que tenha nível Medíocre    (+0).
  - Reescrever uma façanha.
  - Comprar uma nova façanha ao custo de 1 recarga. (Lembre-se: você não    pode ficar com recarga abaixo de 1).
  - Reescrever qualquer um dos seus aspectos, exceto seu conceito.

## Progressos

Progressos são mais significativos, permitindo que seu personagem realmente cresça em poder. Um progresso permite que você escolha uma opção da lista de marcos e, além disso, você pode escolher todos os itens a seguir:

  - Reescrever o conceito do seu personagem, caso queira.
  - Se você tiver quaisquer consequências moderadas ou severas que ainda     não estejam em recuperação, você pode começar o processo de     recuperação e renomeá-las. As consequências que já estavam em     processo de recuperação podem ser removidas.
  - Aumentar uma perícia em um nível, mesmo de Medíocre (+0) para     Regular(+1).

Se o narrador sentir que uma grande parte da trama foi concluída e é o momentos dos personagens se “empoderarem”, ele pode também oferecer uma ou ambas das opções a seguir:

  - Ganhar um ponto de recarga que pode ser gasto imediatamente para    adquirir uma nova façanha, se desejar.
  - Aumentar uma segunda perícia em um nível.

### Aprimorando os níveis das perícias.

Ao aprimorar o nível de uma perícia, você deve manter uma estrutura em “coluna”. Cada andar não pode ter mais perícias que o andar abaixo. Isso significa que você precisará melhorar algumas perícias Medíocres (+0) primeiro ou você pode guardar alguns pontos ao invés de gastá-los imediatamente, permitindo vários aprimoramentos de uma só vez.

> Raquel quer aprimorar seu nível de Saberes de Regular (+1) para Razoável (+2), mas isso significa que ela teria quatro perícias no Razoável (+2) e apenas três no Regular (+1)... assim não funciona. Felizmente, ela guardou um segundo ponto de perícia de um progresso anterior, então ela também aumenta sua Empatia de Medíocre (+0) para Regular (+1). Agora ela tem: um Ótimo (+4), dois Bons (+3), quatro Razoáveis (+2) e quatro Regulares (+1).
> 
> A Pirâmide - Inválido - Válido - Também Válido
>
>  [TODO]

## Sessões e Arcos

Existem algumas suposições em jogo aqui, quando falamos sobre sessões e arcos. Gostaríamos de esclarecer essas suposições para que você possa fazer ajustes baseado em como seu jogo se difere delas.

Uma ***sessão*** é uma única sessão de jogo composta por várias cenas e algumas horas de jogo. Pense nisso como um único episódio de uma série de TV. Provavelmente durará em torno de 3 a 4 horas.

Um ***arco*** é uma série de sessões que geralmente contém elementos da trama levados de uma sessão para outra. Esses elementos da trama não precisam ser concluídos dentro de um arco, mas geralmente há desenvolvimentos e mudanças significativas que ocorrem ao longo dele. Pense nisso como um terço ou meia temporada de uma série de TV. Provavelmente é composto por cerca de quatro sessões de jogo.

Se seu jogo pender para fora desses “prováveis” intervalos, você pode querer mudar a forma como algumas partes do avanço funcionam. Se seus arcos duram por mais de quatro a seis sessões de jogo, você pode permitir que consequências Severas sejam recuperadas depois de 4 sessões se passem em vez de esperar até o fim do arco. Se você quiser que o avanço aconteça mais lentamente, permita melhorias como pontos de perícia e ganho de recarga com menos frequência. Se seu grupo tende a agendar sessões relativamente curtas, você pode não atingir um marco no final de todas as sessões. Tempere a gosto, molde o jogo à sua maneira\!

# Sendo o Narrador

Como narrador, você é o diretor das sessões de jogo. Note que você não é o chefe. ***Fate Condensado*** é colaborativo e os jogadores têm opinião com o que acontece com seus personagens. Seu trabalho é manter o jogo em movimento agindo desse modo:

  - **Dirija as cenas:** Uma sessão é composta por cenas. Decida onde a     cena começa, quem está nela e o que está acontecendo. Decida quando     todas as coisas interessantes já aconteceram e quando a cena chega     ao fim. Ignore as coisas desnecessárias; da mesma forma que você não     rola os dados se o resultado de uma ação não for interessante, não     crie uma cena caso nada excitante, dramático, útil ou divertido     aconteça nela.
  - **Julgue as regras:** Quando surgir alguma dúvida sobre como aplicar     as regras, você pode discutir com os jogadores e tentar chegar a um     consenso aceitável, mas você tem a palavra final como narrador.
  - **Defina a Dificuldade:** Decida quando as rolagens serão    necessárias e defina suas dificuldades.
  - **Determine os custos da falha:** Quando um personagem falha em sua    rolagem, decida qual será o custo do sucesso a um custo. Você    certamente pode aceitar sugestões dos jogadores - eles podem até    dizer o que gostariam que acontecesse ao seus personagens, mas é    você que tem a palavra final.
  - **Jogue com os PNJs:** Cada jogador controla o seu próprio    personagem, mas você controla todo o resto do jogo, desde cultistas    a monstros e até o próprio Grande Mal.
  - **Dê aos PJs oportunidades de ação:** Se os jogadores não sabem o    que fazer a seguir, é seu trabalho dar-lhes um empurrãozinho. Nunca    deixe as coisas ficarem muito paradas por indecisão ou falta de    informação, faça algo para agitar as coisas. Em caso de dúvida,    pense nas táticas e objetivos do Grande Mal para criar um leve    incômodo para os heróis.
  - **Faça com que todos tenham seu momento de brilho:** Seu objetivo    não é derrotar os jogadores, mas desafiá-los. Certifique-se de que    cada PJ tenha a chance de ser a estrela de vez em quando. Force    Aspectos e crie desafios sob medida para as diferentes perícias e    fraquezas dos personagens.
  - **Complique a vida dos PJ:** além de jogar monstros nos personagens,     você será a fonte primária de forçar aspectos. Os jogadores podem    forçar aspectos de si mesmos ou de outros personagens, é claro, mas    você deve garantir que todos tenham a oportunidade de experienciar    as repercussões negativas de seus aspectos.
  - **Construir sobre as escolhas dos jogadores:** observe as ações que    os PJ tomaram durante a partida e pense como o mundo muda e responde    a elas. Faça que sintam o mundo como algo vivo apresentando essas    consequências - boas ou ruins - de suas ações no jogo.

## Definindo Dificuldade e Oposição

Às vezes, a ação de um PJ enfrentará ***oposição*** por meio de uma rolagem de defesa de outro personagem na cena. Nesse caso, o personagem oponente rola os dados e adiciona seus níveis de perícia relevantes, assim como um PJ. Se o personagem oponente tiver aspectos relevantes, eles podem ser invocados; o narrador pode invocar os aspectos dos PdN usando os pontos de destino de sua reserva ([página XX](#seus-pontos-de-destino)).

Mas, se não houver oposição, você deve decidir a ***dificuldade*** da ação:

  - **Dificuldades baixas**, abaixo do valor da perícia relevante do PJ, são    melhores quando você quer dar a eles uma chance de se exibirem.
  - **Dificuldades moderadas**, próximas do valor da perícia relevante do    PJ, são melhores quando você quer gerar tensão, mas não    sobrecarregá-los.
  - **Dificuldades altas**, muito maiores do que o nível da perícia    relevante do PJ, são melhores quando você quer enfatizar quão    ameaçador ou incomum são as circunstâncias e fazê-los utilizar todo    o seu potencial, ou colocá-los em uma posição onde sofrerão as    consequências de uma falha.

Da mesma forma, use a escala de adjetivos ([página XX](#a-escala-de-adjetivos)) de valores para ajudá-lo a escolher uma dificuldade apropriada. É algo excepcionalmente difícil? Então escolha Excepcional (+5)\! Aqui estão algumas regras básicas para você começar:

Se a tarefa não for muito difícil, torne-a Medíocre (+0), ou apenas diga ao jogador que ele teve sucesso sem uma rolagem, desde que não haja muita pressão de tempo ou o personagem tem algum aspecto que sugere que ele seria bom nisso.

Se você puder pensar em pelo menos um motivo do porquê a tarefa é difícil, escolha Razoável (+2); para cada fator extra trabalhando contra eles, adicione outro +2 à dificuldade.

Ao pensar sobre esses fatores, consulte quais aspectos estão em jogo. Quando algo é importante o suficiente para se tornar um aspecto, ele deveria ganhar alguma atenção aqui. Uma vez que aspectos são verdades ([página xx](#aspectos-são-sempre-verdades)), eles poderão influenciar o quanto algo deve ser fácil ou difícil de ser realizado. Isso não significa que os aspectos são os únicos fatores a serem considerados\! Escuridão é escuridão independentemente de você decidir ou não torná-la um aspecto de cena. 

Se a tarefa é incrivelmente difícil, vá o mais alto quanto você achar que faz sentido. O PJ precisará gastar alguns pontos de destino e obter muita ajuda para ser bem sucedido, mas tudo bem.

Para uma visão mais ampla do que você pode fazer para criar oposições e adversários variados e interessantes para seus jogadores, verifique o Kit de Adversários de Fate, disponível para venda em PDF ou as regras essenciais de forma gratuita no sistema de referência de regras (em inglês) em <https://fate-srd.com/>

## Personagens do Narrador (PdN)

Os PdN incluem espectadores, elenco de apoio, aliados, inimigos, monstros e praticamente qualquer coisa que possa complicar ou se opor aos esforços dos PJ. Você provavelmente criará outros personagens para que os PJ interajam com eles.

### PdN Principais

Se alguém é particularmente importante para a história, você pode criá-lo como se fosse um PJ. Isso é apropriado para alguém com quem os PJ irão lidar muito, como um aliado, um rival, o representante de um grupo poderoso ou um Grande Mal.

Um PdN importante não segue necessariamente os mesmos limites que um PJ iniciante. Se o PdN se tornar uma ameaça recorrente como um chefe, dê a ele uma perícia com nível máximo (veja Definindo Dificuldade e Oposição, [página xx](#definindo-dificuldade-e-oposição)), mais façanhas e tudo que seja necessário para torná-lo perigoso.

### PdN Menores

Os PdN que não são personagens significativos ou recorrentes não precisam ser tão bem definidos como PdN principais. Para um PdN menor, defina apenas o que for absolutamente necessário. A maioria dos PdN menores terão um único aspecto, que é exatamente o que eles são: _cão de guarda, burocrata obstrutivo, cultista enfurecido_, etc. Se  necessário, dê-lhes um ou dois aspectos que refletem algo interessante sobre eles ou uma fraqueza. Eles também podem ter uma façanha.

Dê a eles uma ou duas perícias que descrevam no que eles são bons. Você pode usar perícias da lista de perícias ou inventar algo mais específico, como Razoável (+2) em Arranjar Brigas de Bar ou Ótimo (+4) em Morder Pessoas.

Dê a eles de zero a três caixas de estresse, quanto mais eles tem, mais ameaçador eles serão. Geralmente eles não têm caixas de consequências, se eles forem atingidos com mais tensões do que podem absorver com seu estresse, eles simplesmente são tirados de ação. Os PdN secundários não são feitos para durar.

### Monstros, Grandes Males e Outras Ameaças

Assim como PdN menores, os monstros e outras ameaças (como uma tempestade, um fogo que se alastra ou um esquadrão de lacaios blindados) são escritos como personagens, mas geralmente são mais simples que um PJ. Você só precisa definir o que é absolutamente necessário. Diferente de PdN menores, essas ameaças podem ser definidas de qualquer forma. Quebre as regras ([página XX](#maneiras-de-quebrar-as-regras-para-o-grande-mal)), dê a eles qualquer combinação de aspectos, perícias, façanhas, estresses e consequências necessárias para torná-los perigosos e pense sobre que tipo de dificuldades eles apresentarão aos PJs quando determinar suas pontuações.

## Seus pontos de destino

No início de cada cena, comece com uma reserva de pontos de destino igual ao número de PJs. Se a cena inclui um PdN principal ou um monstro que concedeu ([página XX](#concedendo)) um conflito anterior ou recebeu invocações hostis ([página XX](#invocações-hostis)) em uma cena anterior, esses pontos de destino são adicionados à sua reserva. Se um aspecto foi forçado contra você na cena anterior, não dando a oportunidade de gastar esse ponto de destino ganho, você pode adicioná-lo à sua reserva também.

> Carlos, Raquel, Carolina e Edson se dirigem para o confronto final contra Alice VillaBoas que anteriormente havia escapado dos heróis após conceder em um conflito depois de ter sofrido uma consequência moderada. Isso significa que o narrador tem quatro pontos de destino pelos quatro jogadores e dois a mais que Alice está trazendo.

Como narrador, você pode gastar os pontos de destino dessa reserva para invocar aspectos, recusar forçar aspectos que os jogadores ofereçam aos NPCs e utilizar qualquer façanha que exija um ponto, tudo exatamente como os jogadores fazem.

**Entretanto, você _não precisa_ gastar ponto de destinos para forçar aspectos.** Você tem um estoque infinito de pontos de destino para esse propósito. 

## Ferramentas de Segurança

Narradores (e na verdade, os jogadores também) têm a responsabilidade de garantir que todos na mesa se sintam seguros no jogo e no espaço que estão jogando. Uma maneira do narrador apoiar isto é oferecendo uma estrutura em que qualquer um da mesa possa expressar preocupação ou objeção. Quando isso acontecer, deve ter prioridade e deve ser resolvido. Aqui estão algumas ferramentas que podem ajudar a tornar esse processo mais acessível para os jogadores na mesa e mais fácil de ser aplicado quando necessário.

  - **Carta X:** a carta X é uma ferramenta opcional (criada por John     Stavropoulos) que permite que qualquer pessoa em seu jogo (incluindo     você) mude qualquer conteúdo que faça alguém se sentir     desconfortável durante o jogo. Você pode aprender mais sobre a     carta X em  *<http://tinyurl.com/x-card-rpg>*
  - **Caixas de Ferramentas de Mudança de Roteiro para RPG:** para algo com     um pouco mais de nuances e granularidade, busque por Script Change     de Brie Beau Sheldon, que oferece opções para pausar, retroceder,     avançar e muito mais, lembrando a familiar metáfora de um tocador de    mídia. Veja mais sobre o Script Change em *<http://tinyurl.com/nphed7m>*.

Ferramentas como essas também podem ser usadas como a regra do “Fala Sério” ([página XX](#invocando-aspectos)) para calibração. Elas oferecem uma maneira para os jogadores debaterem confortavelmente sobre o que eles procuram no jogo. Dê a essas ferramentas o respeito e o suporte que elas merecem\!

# Regras opcionais

Essas são algumas regras opcionais ou alternativas que você pode decidir usar em seus jogos.

## Condições

As ***condições*** são um substituto para as consequências e as substituem por completo. Condições servem a dois propósitos: elas aliviam a pressão sobre os jogadores e Narrador para definir rapidamente o aspecto da consequência recebida, e te dão a oportunidade para moldar a natureza do seu jogo ao pré-definir as formas como os ferimentos duradouros recaem sobre os personagens.

A versão do ***Fate Condensado*** para as condições pega cada nível de consequência e a separa em duas condições com a metade do valor.

|                            |                              |
| -------------------------- | ---------------------------- |
| \[1\] Arranhado (Leve)     | \[​1\] Assustado (Leve)      |
| \[2\] Machucado (Moderado) | \[2\] Abalado (Moderado)     |
| \[3\] Ferido (Severo)      | \[3\] Desmoralizado (Severo) |

Estes correspondem a estados físicos e mentais, mas só porque você sofreu um ataque físico não significa que você não possa marcar também uma condição mental, e vice-versa, desde que faça sentido. Os ataques são traumáticos\!

As condições são recuperadas da mesma forma que as consequências, baseadas em sua gravidade. Caso você ganhe uma consequência suave adicional, em seu lugar você recebe duas caixas a mais, sendo em _Arranhado_ ou _Assustado_, conforme apropriado.

### Separando ainda mais as Condições

Caso você prefira manter as condições físicas e mentais separadas, dobre o número de caixas de cada uma. Dito isso, existe um limite: se duas caixas no total foram marcadas em qualquer uma das condições em uma linha, nenhuma outra caixa poderá ser marcada nessa linha. Portanto, se você tiver uma caixa (de duas) marcada em _Arranhado_ e nenhuma em
_Assustado_, e depois marcar a segunda caixa de _Arranhado_ ou a primeira de _Assustado_, você não poderá marcar mais nenhuma caixa naquela linha.

Se você ganhar um espaço de consequência suave (de um valor alto de Vigor, Vontade ou por uma Façanha), em vez disso, adicione mais duas caixas em Arranhado ou Assustado, conforme o apropriado. Essas caixas adicionadas aumentam o limite de corte para aquela linha, um por um.

### Outras Versões de Condições

Vários jogos publicados baseados em Fate usam condições em vez de consequências. Sinta-se livre para adotar outra implementação ao invés dessa se for melhor para você. Cada uma atinge praticamente o mesmo propósito para o jogo: reduzir a pressão de descobrir os aspectos de consequência de improviso e orientar a natureza do jogo ao limitar os tipos de danos duradouros que os personagens podem sofrer.

## Mudando a lista de perícias

Conforme mencionado na [página xx](#lista-de-perícias), a ***lista de perícias*** é a primeira coisa a se considerar um ajuste ao criar seu próprio jogo utilizando Fate. Nossa configuração padrão apresenta uma lista de 19 perícias organizadas em uma pirâmide de 10 espaços. Essa lista também é estruturada em torno da tradicional ideia de capacidades em vários campos de ação, em essência abordando a questão “o que você pode fazer?” Outras listas de perícias não são necessariamente do mesmo tamanho, nem organizadas da mesma maneira, tampouco abordam a mesma questão. Dito isto, aqui estão algumas listas curtas de perícias para serem consideradas, emprestadas e modificadas:

  - **Ações:** Resistir, Lutar, Saber, Mover, Observar, Pilotar,     Esgueirar, Falar, Consertar.
  - **Abordagens:** Cuidadoso, Esperto, Estiloso, Poderoso, Ágil,    Sorrateiro.
  - **Aptidões:** Atletismo, Combate, Liderança, Conhecimento,    Subterfúgio.
  - **Atributos:** Força, Destreza, Constituição, Inteligência e    Carisma.
  - **Relacionamentos:** Liderança, Parceria, Suporte, Solo.
  - **Papéis:** Piloto, Lutador, Hacker, Construtor, Atravessador,    Ladrão, Mentor.
  - **Temas:** Ar, Fogo, Metal, Mente, Pedra, Vazio, Água, Vento,    Madeira.
  - **Valores:** Dever, Glória, Justiça, Amor, Poder, Segurança,    Verdade, Vingança.

Se você quiser uma lista mais longa, tente começar pela lista padrão, adicionando, combinando e removendo perícias dela à medida que achar necessário, até encontrar o que você está procurando. Você pode, em vez disso, misturar duas ou mais listas descritas acima de algum modo.

**Avanço:** quanto menor é o número de perícias em sua lista em relação à lista padrão, menor é a frequência com que você irá recompensar pontos de perícias devido aos avanços. Permita talvez, apenas durante a evolução dos personagens ([página xx](#avanço)), ou restringi-las de outro modo.

### Alternativas para a pirâmide:

  - **Diamante:** um meio largo (cerca de um terço das perícias) que     afunila no topo e na base. Por exemplo, 1 em +0, 2 em +1, 3 em +2, 2     em +3 e 1 em +4.
  - **Coluna:** um número aproximado de perícias em cada nível. Se sua     lista for curta o suficiente, isso pode ser uma linha, com uma     perícia por nível.
  - **Livre + Limite:** Dê aos jogadores pontos de perícia o suficiente     para fazerem uma pirâmide (ou outro formato), mas não os obrigue.    Eles podem comprar o que quiserem, desde que fiquem abaixo do    limite.

**Abrangência:** certifique-se de considerar quantas perícias você espera que sejam usadas do total. A lista padrão tem uma taxa de uso de 53% (10 de 19). Quanto maior for essa porcentagem, maior a chance de haver mais jogadores com competências repetidas. Preserve a proteção do nicho de personagens.

**Combinação:** Você pode querer ter duas listas com os jogadores somando uma perícia de cada para realizar sua rolagem. Tenha sempre em mente os limites mínimos e máximos possíveis da combinação dentro da faixa de 0 ao limite estabelecido. Você pode também ter valores de +0 a +2 em cada lista, ou -1 a +1 em uma e +1 a +3 na outra, etc.

## Criação de Personagem Durante o Jogo

Se um jogador se sente confortável em tomar decisões criativas e rápidas de improviso, ele pode gostar de criar personagens *à medida que joga* ao invés de antes do jogo. Isso imita a maneira que os personagens se revelam e se desenvolvem em outras mídias. Não é para todos, mas para grupos que se identificam com esse método, pode ser realmente agradável para todos.

Com esse método, os personagens começam apenas com um nome, um conceito e sua maior perícia, e só\! Conforme o jogo progride e eles são desafiados a usar alguma perícia sem nível, eles podem escolher um espaço vazio e revelar uma nova perícia de imediato. Da mesma forma, os aspectos e façanhas podem ser preenchidos quando as circunstâncias as pedirem, bem no momento em que um ponto de destino é gasto ou um bônus é reivindicado.

## Contagem Regressiva

Uma contagem regressiva adiciona urgência a um adversário ou situação: lide com isso agora ou coisas ficarão piores. Quer você esteja falando de uma bomba-relógio, um ritual quase concluído, um ônibus se equilibrando na beira de uma ponte suspensa ou um soldado com um rádio prestes a chamar reforços, as contagens regressivas forçam os PJs a agir rapidamente ou enfrentar um resultado pior.

Contagens regressivas possuem três componentes: uma trilha de contagem regressiva, um ou mais gatilhos e um resultado.

A ***trilha da contagem*** se parece muito com uma trilha de estresse: é uma linha de caixas que você marca da esquerda para a direita. Toda vez que você marcar uma caixa, a contagem regressiva fica mais próxima do fim. Quanto mais curta for a trilha, mais rápido a catástrofe se aproxima. 

O ***gatilho*** é um evento que marca uma caixa na trilha de contagem regressiva. Ele pode ser tão simples quanto a passagem de minuto, hora, dia ou rodada ou tão específico como “o vilão recebe uma consequência ou é tirado de ação”.

Quando você marca a última caixa, a contagem regressiva termina e o ***resultado*** acontece, seja ele qual for. Os narradores, se desejarem, podem revelar a existência de uma trilha de contagem regressiva para os jogadores sem dizer a eles o que ela representa a princípio, como um tipo de presságio para aumentar a sensação de tensão na história.

Uma contagem regressiva pode ter mais de um gatilho se você quiser; talvez a contagem regressiva avance em um ritmo previsível até que aconteça algo que a acelere. Você também poderia dar um gatilho diferente para cada caixa na trilha de contagem regressiva se quiser que uma série de eventos específicos desencadeiem o resultado.

## Consequências Extremas

As consequências extremas introduzem uma quarta severidade opcional de consequências em seu jogo: alguma coisa que muda permanentemente e irrevogavelmente um personagem.

Tomar uma consequência extrema reduz o estresse recebido em 8. Quando tomado, você deve **substituir** um dos aspectos existentes do seu personagem (que não seja o conceito, o qual está fora de questão) por um aspecto que represente a mudança profunda no personagem resultante do ferimento que ele tomou.

Por padrão, não há opção de recuperação de uma consequência extrema. Ela se tornou parte do personagem agora. No seu próximo progresso, você poderá renomeá-la para que reflita como você se adaptou a ela, mas você não poderá voltar ao aspecto original.

Entre progressos, um personagem só pode usar essa opção uma vez.

## Disputas mais rápidas

Alguns grupos podem sentir que as disputas envolvem muitas tentativas de criar vantagens por rodada. Para esses grupos, tente o seguinte método: em cada rodada de uma disputa, cada participante pode escolher apenas uma das três opções:

  - Rolar superar para seu lado ([página XX](#superar))
  - Rolar para criar vantagem, mas sem bônus de trabalho em equipe ([página XX](#trabalho-em-equipe))
  - Fornecer seu bônus de trabalho em equipe para a rolagem de superar    do seu lado ou à tentativa de outro jogador em criar vantagens. Não    role os dados.

## Defesa Total

Às vezes, um jogador (ou o narrador) pode declarar que seu personagem somente se defenderá até o próximo turno ao invés de realizar uma ação nesse turno. Isso se chama defesa total.

Ao declarar defesa total, você deve deixar claro o foco de seus esforços. Por padrão, você está se defendendo (de ataques e esforços para criar vantagens sobre você), mas você pode desejar especificar alguém que está protegendo, ou uma defesa contra um grupo específico de agressores, ou ainda contra um esforço ou resultado particular que você queira se opor.

**Enquanto estiver em defesa total, você ganha +2 para todas as defesas relacionadas ao foco declarado.**

Se nada ocorrer e você não rolou para se defender, no próximo turno você ganha um impulso ([página XX](#impulsos)) já que ganhou a oportunidade de se preparar para sua próxima ação. Isso compensa “perder sua vez” porque você focou seus esforços em defender-se contra algo que não aconteceu.

## Obstáculos

A característica que define os inimigos é que eles podem ser atacados e retirados de jogo. Em contraste, a característica que define os **obstáculos** é que eles não podem. Os obstáculos tornam as cenas comprovadamente mais difíceis para os PJs, mas os personagens não podem simplesmente lutar contra eles. Os obstáculos devem ser contornados, resistidos ou tornados irrelevantes.

Embora a maioria dos obstáculos sejam características do ambiente, alguns podem ser personagens que não podem ser tirados de jogo usando métodos convencionais. O dragão pode ser um chefe, mas pode também ser um obstáculo perigoso. A estátua animada que impede você de chegar ao mago maligno pode ser uma ameaça, mas também pode ser um bloqueio ou uma distração. Tudo dependerá da função do adversário na cena e como os PJs deverão lidar com isso.

Os obstáculos não aparecem em todas as cenas. Eles servem para acentuar os inimigos na cena, para torná-los mais ameaçadores ou memoráveis, mas o uso excessivo de obstáculos pode ser frustrante para os PJs, particularmente aqueles focados em combate. Você pode usá-los para dar aos PJs menos combativos algo para fazer durante uma luta.

Existem três tipos de obstáculos: perigos, bloqueios e distrações.

### Perigos

Se um obstáculo pode atacar os PJs, ele é um ***perigo***. Jatos de fogo, pedras rolantes ou um atirador de elite longe o suficiente para ser diretamente resolvido: todos são perigos. Cada perigo tem um nome, um nível de perícia e uma Potência de Arma ([página XX](#potência-de-armas-e-armaduras)) de 1 a 4.

O nome do perigo é tanto sua perícia quanto um aspecto, isto é, o nome define o que o perigo pode fazer e seu valor de perícia define o quão bom ele é fazendo isso, mas o nome pode também ser invocado ou forçado como qualquer outro aspecto.

De modo geral, o nível da perícia de um perigo deve ser pelo menos tão alta quanto a maior perícia dos PJs, se não um pouco maior. Um perigo com um nível de perícia *e* Potência de Arma muito altos, provavelmente tirará de cena um ou dois PJs. Você também pode criar um perigo com um nível menor de perícia, mas uma alta Potência de Arma, criando algo que não acerta frequentemente, mas quando acerta, acerta com força. Invertendo isso, se cria um perigo que acerta frequentemente, mas não causa muito dano.

Um perigo age na iniciativa, exatamente como os PJs e seus inimigos fazem. Se suas regras exigem que todos rolem iniciativa, os perigos irão rolar com seu valor de perícia. Em seu turno, a cada rodada, um perigo age como implícito em seu nome e rola com seu nível de perícia. Se ele atacar e acertar com um empate ou melhor, adicione sua Potência de Arma às tensões. Perigos podem atacar ou criar vantagens; eles não podem ser atacados e não superam obstáculos.

Se um jogador quiser superar ou criar vantagem contra um perigo, ele enfrentará uma oposição passiva igual ao seu nível de perigo.

### Bloqueios

Enquanto os perigos existem para ferir os PJs, os ***bloqueios*** os impedem de fazer coisas que eles queiram fazer. Os bloqueios podem causar estresse, embora nem sempre o façam. As principais diferenças entre bloqueios e perigos é que os bloqueios não realizam ações e são mais difíceis de serem removidos. Os bloqueios oferecem oposição passiva em certas circunstâncias e podem ameaçar ou causar dano se não lhes for dada atenção.

Assim como os perigos, bloqueios tem um nome e um nível de perícia, e seu nome é tanto uma perícia quanto um aspecto. Diferente dos perigos, o nível de perícia do bloqueio não deveria ser muito maior que um nível a mais que a perícia mais alta dos PJs, caso contrário, as coisas podem se tornar frustrantes rapidamente. Um bloqueio pode ter uma Potência de Arma tão alto quanto 4, mas não é necessário que tenha uma.

Os bloqueios somente entram em jogo em circunstâncias específicas. Um _tanque de ácido_ só importa quando alguém tenta atravessá-lo ou é arremessado dentro dele. Uma _cerca de arame_ só afeta quem tenta passar por ela. A _estátua animada_ só impede a entrada em uma sala específica.

Os bloqueios não atacam e não possuem um turno nas rodadas de ação. Em vez disso, sempre que um bloqueio interferir na ação de alguém, ele deverá rolar contra o valor do bloqueio como dificuldade base. Se o bloqueio não pode causar danos, ele simplesmente impede que o PJ realize a ação desejada. Se ele pode causar danos e o PJ falha na rolagem de superar, ele toma um golpe igual à quantidade de tensões do teste falho.

Os personagens podem tentar forçar alguém contra um bloqueio como um ataque. Se você fizer isso, você rolará para atacar normalmente, mas adicionará uma Potência de Arma igual à metade da Potência de Arma do bloqueio (arredondado para baixo, mínimo de 1).

Finalmente, alguns bloqueios podem ser usados como cobertura ou armadura. Isso é situacional e para alguns bloqueios isso simplesmente não faz sentido. Você provavelmente não pode se esconder atrás de um _tanque de ácido_, mas uma _cerca de arame_ é uma proteção efetiva contra um taco de beisebol, provavelmente evitando o ataque por completo.

Quando alguém está usando um bloqueio como cobertura, decida se isso mitiga ou nega o ataque. Se nega, o ataque simplesmente não é possível. Se mitiga, o defensor adiciona uma Potência de Armadura igual à metade da perícia do bloqueio (arredondado pra baixo, mínimo 1).

Use os bloqueios com moderação. Os bloqueios podem dificultar a realização de certas ações pelos PJs, então poderá ser frustrante se os usar demais, mas também podem levar os jogadores a pensarem criativamente. Eles podem ver uma oportunidade de usar os bloqueios a seu favor. Se eles descobrirem como, permita que o façam\!

Às vezes, os jogadores querem apenas remover os bloqueios de uma vez. Para fazer isso, faça uma rolagem de superar contra uma dificuldade fixa igual ao nível do bloqueio +2.

### Distrações

Se os perigos atacam diretamente os PJs e os bloqueios impedem que eles realizem certas ações, as ***distrações*** forçam os PJs a decidirem quais são suas prioridades. Dos obstáculos, as distrações costumam ser as menos definidas mecanicamente. Elas também não tornam necessariamente a cena mecanicamente mais difícil, em vez disso, elas apresentam aos PJs decisões difíceis. Aqui estão as parte da distração:

  - O **nome** de uma distração é uma representação breve e incisiva do que     ela é. Pode ser um aspecto, se você quiser ou precise que seja.
  - A **escolha** de uma distração é uma pergunta simples que codifica a     decisão que oferece aos PJ.
  - A **repercussão** da distração é o que acontece aos PJs se eles não     lidam com ela. Algumas distrações podem ter múltiplas repercussões,     incluindo aquelas que surgem ao lidar com a distração *com sucesso*.
  - A **oposição** da distração é uma oposição passiva contra as rolagens     dos PJs que visam lidar com ela. Nem toda distração precisa oferecer     uma oposição.

Se você teme que os PJs lidem facilmente com uma luta que você tem reservada, adicionar uma ou duas distrações pode forçá-los a decidir se é mais importante derrotar os bandidos ou lidar com as distrações.

Lidar com uma distração deve sempre ter um benefício claro ou, caso seja ignorada, não lidar com uma distração deveria sempre ter uma consequência clara.

### Exemplos de Obstáculos

- #### Perigos
  - _Torre de metralhadora_ Ótimo (+4), Arma:3
  - _Sniper Distante_ Excepcional (+5), Arma:4
- #### Bloqueios
  - _Cerca de Arame_ Razoável (+2), dificuldade Ótima (+4) para ser    removida.
  - _Tanque de Ácido_ Bom (+3), Arma:4, dificuldade Excepcional (+5) para    ser removida.
- #### Distrações
  - _O Ônibus Cheio de Civis_ 
    - **Escolha:** o ônibus cairá da ponte?
    - **Oposição:** Bom (+3)
    - **Repercussão (deixá-los):** Todos os civis do ônibus morrem.
    - **Repercussão (salvá-los):** O vilão consegue fugir\!
  - _A Gema Brilhante_
    - **Escolha:** você consegue tirar a gema do pedestal?
    - **Repercussão (deixar a gema):** Você não obtém a gema (de valor inestimável).
    - **Repercussão (pegar a gema):** Você ativa as armadilhas dentro do templo.

## Escala

**Escala** é um subsistema opcional que você pode usar para representar seres sobrenaturais que atuam em um nível além das capacidades gerais da maioria dos personagens em seu jogo. Normalmente você não precisa se preocupar com o impacto da escala em seu jogo. No entanto, pode haver momentos onde será desejável apresentar aos personagens uma ameaça maior que eles normalmente enfrentam, ou uma oportunidade para os personagens lutarem fora de sua categoria de peso normal.

Por exemplo: se você desejar mudar a lista para algo mais adequado ao seu cenário, apresentaremos aqui cinco níveis potenciais de escala: Mundano, Sobrenatural, Extraplanar, Lendário e Divino.

  - **Mundano** representa personagens sem acesso a poderes sobrenaturais ou    tecnologias que os impulsionariam além das capacidades humanas.
  - **Sobrenatural** representa personagens que têm acesso a poderes    sobrenaturais ou tecnologias que vão além da capacidade humana, mas    que permanecem humanos na essência.
  - **Extraplanar** representa personagens incomuns ou únicos cujos poderes    os distinguem das preocupações normais da humanidade.
  - **Lendário** representa espíritos poderosos, entidades e seres    alienígenas para os quais a humanidade é mais uma curiosidade que    uma ameaça.
  - **Divino** representa as forças mais poderosas do universo: arcanjos,    deuses, fadas rainhas, planetas vivos e assim por diante.

Ao aplicar escala para dois indivíduos ou forças opostas, compare os níveis de cada lado e determine quem é maior e por quantos níveis. Eles obtêm *um* dos seguintes benefícios em qualquer ação rolada contra os de escala menor:

  - \+1 por nível de diferença em sua ação *antes* da rolagem;
  - \+2 por nível de diferença para o resultado *após* a rolagem, *somente     se* ela for bem sucedida;
  - 1 invocação gratuita adicional por nível de diferença para os    resultados de uma ação de criar vantagem bem-sucedida.

A aplicação frequente e rígida das regras de escala podem colocar os PJs em uma clara desvantagem. Compense proporcionando generosas oportunidades a esses jogadores de subverterem as desvantagens da escala de maneiras inteligentes. Opções viáveis incluem pesquisar pelos pontos fracos do alvo, mudar para um local onde a escala não se aplique ou alterar os objetivos para que seu oponente não possa aproveitar da sua vantagem de escala.

### Aspectos e Escala

Aspectos de situação ativos às vezes representam um efeito sobrenatural. Nesses casos, o narrador pode determinar que invocar aquele aspecto concede o benefício adicional da sua escala.

Além disso, um aspecto criado sobrenaturalmente pode conceder escala para algumas ações quando invocado. Ele também pode fornecer escala mesmo sem uma invocação, como no caso de um véu mágico ou um traje de camuflagem de alta tecnologia; você não precisa invocar *Velado* para ganhar escala sobrenatural ao se esgueirar.

#### A Escala se aplica ao criar uma vantagem sobrenatural?

Se você está criando uma vantagem e *não há oposição*, ao invés de rolar dados, você simplesmente ganha o aspecto com uma invocação gratuita. Esse aspecto garante a escala conforme descrito anteriormente.

Se você estiver criando vantagem *em alguém para prejudicá-lo*, como lançar *Emaranhado por vinhas animadas* em seu oponente, você pode ganhar escala em seu esforço para criar vantagem.

Se você está criando uma vantagem por meios sobrenaturais e o *grupo adversário pode impedir diretamente seu esforço por meio de interferência física ou sobrenatural*, sua escala pode ser aplicada contra a rolagem de defesa deles.

*Caso contrário*, role para criar vantagem sem escala (provavelmente contra uma dificuldade definida), mas o uso posterior daquele aspecto concede escala quando apropriado.

## Tensões de Tempo

Ao determinar quanto tempo leva para os personagens fazerem algo, você pode querer usar uma abordagem mais sistemática para decidir os impactos de sucesso, de fracasso e das opções “a um custo”. Quão mais lento ou mais rápido? Use estas orientações e deixe que as tensões decidam.

Primeiro, decida quanto tempo a tarefa leva para ser feita com um simples sucesso. Use uma quantidade aproximada mais uma unidade de tempo: “alguns dias”, “meio minuto”, “várias semanas” e assim por diante. Quantidades aproximadas podem ser: metade, cerca de um, alguns ou vários de uma determinada unidade de tempo.

Em seguida, observe por quantas tensões a rolagem excede ou erra o valor alvo. Cada tensão vale um passo na quantidade de onde quer que seja o seu ponto de partida.

Portanto, se o seu ponto de partida é “algumas horas”, então um ponto de tensão mais rápido pula a quantidade para “cerca de uma hora”, duas tensões para “meia hora”. Ir mais rápido do que “metade” reduz a unidade para um nível menor (de horas para minutos, etc) e a quantidade para “vários”, então três pontos de tensão mais rápido faria com que o tempo fosse “vários minutos”. 

No caso de mais lento, é o mesmo processo na direção oposta: um ponto de tensão mais lento são “várias horas”, dois são “meio dia” e três são “cerca de um dia”.

## Maneiras de quebrar as regras para o Grande Mal

Entre combinar perícias e criar vantagens para trabalho em equipe ([página xx](#trabalho-em-equipe)), um grupo de PJs pode realmente dominar por completo um único oponente. Isso funciona se você deseja respeitar a vantagem dos números, mas não é tão bom se você deseja apresentar um “Grande Mal” que equivale ao grupo todo.

Mas lembre-se: para monstros e outras grandes ameaças é aceitável quebrar as regras ([página XX](#monstros-grandes-males-e-outras-ameaças)) então faça isso procurando maneiras de neutralizar a vantagem numérica do grupo enquanto dá a eles alguma chance. Aqui estão algumas sugestões de como você poderia fazer isso. Você pode usar uma ou mais dessas em combinação para chefes finais especialmente difíceis ou assustadores.

### Imunidade por Desafio ou Disputa

Ambos os métodos tem como objetivo prolongar o confronto final conduzindo o grupo a uma atividade “contra o relógio” antes que eles possam efetivamente encarar o Grande Mal diretamente.

Com a ***imunidade por desafios***, o seu Grande Mal não pode ser afetado diretamente (fisicamente, mentalmente ou ambos) até que o grupo vença um desafio (por exemplo, desativando a fonte de seu poder, descobrindo qual é sua fraqueza, etc). O Grande Mal, enquanto isso, pode agir livremente e poderá atacá-los durante seus esforços, se opor às ações de superar ou criar vantagens com rolagens de defesa, remover as invocações gratuitas dos PJs com seu próprio superar ou preparar-se para o eventual progresso dos jogadores criando vantagens próprias.

Com a ***imunidade por disputas***, o grupo deverá vencer uma disputa para poder atacar diretamente o Grande Mal; e o Grande Mal pode atacá-los enquanto eles tentam. Se o Grande Mal vence a disputa, ele consegue realizar seus planos e sair ileso.

### Armadura descartável de lacaios

Cercar-se de lacaios é um modo de tentar equilibrar o lado do Grande Mal contra o dos PJs, mas só funcionará até o momento em que os jogadores decidam ir atrás do Grande Mal diretamente e ignorem aqueles lacaios irritantes por um tempo.

Mas com a ***armadura descartável de lacaios*** em jogo, um Grande Mal sempre poderá ter sucesso a um custo em suas rolagens de defesa contra ataques, forçando um lacaio a ficar no caminho desse ataque. Esse lacaio não rola para se defender uma vez que simplesmente recebe o golpe que teria acertado o Grande Mal de qualquer modo. Isso força os PJs a desgastarem o exército do Grande Mal antes do confronto final.

Lembre-se: lacaios não precisam ser lacaios literais. Por exemplo, você pode escrever um ou mais “geradores de escudos”, cada um com uma trilha de estresse e talvez uma perícia para criar vantagens defensivas para blindar o Grande Mal\!

### Revelar a Forma Verdadeira

Ok, o grupo jogou tudo que eles tinham no Grande Mal, e - *incrível\!* - eles simplesmente o derrotaram\! Há apenas um problema: isso o libera de sua jaula de carne para revelar sua forma verdadeira\!

Com a ***revelação da sua forma verdadeira***, o seu Grande Mal não é apenas um personagem, são ao menos dois personagens que deverão ser derrotados sequencialmente, cada um revelando novas capacidades, façanhas, níveis maiores de perícias, novas caixas de estresse e consequências e até mesmo novos “quebra-regras”.

Se você quiser suavizar isso, mantenha as consequências que o Grande Mal já tinha sofrido entre as formas, dispensando as suaves e diminuindo as médias e graves em um passo cada.

### Aumentando a Escala

Você poderia ***aumentar a escala*** para permitir que seu Grande Mal opere em uma escala maior que a dos PJs usando a regra de escala da [página XX](#escala). Você pode fazer isso mesmo que a escala não seja normalmente usada em sua campanha, essas regras só precisam ser aplicadas quando o Grande Mal entra em campo\!

### Bônus Solo

Os jogadores podem desfrutar de um bônus de trabalho em equipe, claro, mas por quê não dar ao seu Grande Mal um ***bônus solo*** complementar quando é o único a encarar os heróis?

Você poderia implementar esse bônus solo de algumas maneiras. Você pode usar mais de uma delas, mas tenha cuidado ao combiná-las, pois elas escalam rapidamente.

  - O Grande Mal recebe um **bônus para rolagens de perícias** igual ao     máximo do potencial de bônus de trabalho em equipe ([página XX](#trabalho-em-equipe) do     número de PJs agindo contra o Grande Mal menos um (então +2 contra     um grupo de três, etc). Assim como acontece com os PJs, este bônus     não pode ser maior do que o dobro da perícia do Grande Mal (ou     talvez você quebre essa regra *também*).
  - O Grande Mal poderá **reduzir o estresse** recebido de ataques bem     sucedidos pelo número de PJs oponentes dividido por dois,     arredondado para cima. Caso você ache que isso tornará a luta muito     demorada, então os golpes reduzidos desta forma não podem ser     reduzidos para menos que 1.
  - O Grande Mal tem **invocações amplificadas**: ao *pagar* pela invocação de     um aspecto, seu bônus é igual ao número de PJs que ele enfrenta.     Isso não acontece com invocações gratuitas, mas fará com que cada    ponto de destino gasto seja aterrorizante.
  - O Grande Mal pode **suprimir invocações**: quando enfrentando 2 ou mais     inimigos, as invocações da oposição fornecem apenas um bônus +1, ou    somente permitirão re-rolagens quando utilizadas diretamente contra    o Grande Mau. Opcionalmente, o Grande Mal também poderá remover a    capacidade dos PJs de acumular invocações gratuitas.

### A ameaça é um mapa (ou uma colméia de personagens)

Em Fate qualquer coisa pode ser um personagem, então por quê não um mapa? Quando ***a ameaça é um mapa***, seu Grande Mal tem zonas ([página xx](#zonas)) que deverão ser navegadas para alcançar a vitória.

Conforme você detalha o mapa do Grande Mal, cada zona poderá ter suas próprias perícias, aspectos e caixas de estresse. Algumas zonas poderão conter desafios simples que precisarão ser superados para se aproximarem da criatura. Cada zona poderá realizar uma ação como um personagem independente contra os PJs que estejam ocupando aquela zona ou, no caso da zona representar um membro do corpo ou similar, ser capaz de atacar zonas adjacentes também. Se uma zona é removida de ação pelo ataque de um dos PJs, ela poderá ser ignorada e não poderá realizar ações por conta própria, mas em geral o Grande Mal não é derrotado até que os heróis alcancem seu coração e o matem de verdade.

Este método funciona particularmente bem se o seu Grande Mal é um monstro verdadeiramente gigantesco, mas não precisa se limitar a essa situação. Você pode usar a ideia de tratar a ameaça como uma coleção de personagens interconectados, sem exigir que os PJs realmente entrem ou naveguem pelo chefão como um mapa literal. Usado dessa forma, você tem um híbrido entre um mapa e uma armadura de lacaios descartável ([página xx](#armadura-descartável-de-lacaios)): uma ***colmeia de personagens***, de certo modo. Algumas partes do Grande Mal devem ser derrotadas antes que os jogadores possam atingir onde ele é realmente vulnerável, e em troca essas partes podem realizar suas próprias ações.

Quer você se engaje totalmente na ideia do mapa ou simplesmente construa o Grande Mal como uma colméia, com certeza vai acabar com uma luta mais dinâmica, onde o Grande Mal age com mais frequência, e os jogadores devem descobrir um plano de ataque que elimine a ameaça, peça por peça, antes que eles possam finalmente abatê-la.

## Maneiras de Lidar com Múltiplos Alvos

Inevitavelmente alguém em sua mesa desejará afetar vários alvos de uma vez. Se for permitido, aqui estão alguns métodos que você pode usar.

Se você deseja ser seletivo com seus alvos, você pode ***dividir seu esforço***. Role sua perícia e se o resultado final for positivo você pode dividir esse total como quiser entre os alvos, dos quais podem cada um se defender contra o esforço que você designou a eles. Você deve atribuir pelo menos um ponto de esforço para um alvo ou você não o atingiu de forma alguma.

> Sofia enfrenta um trio de capangas e quer atacar todos os três em uma sequência de estocadas com seu florete. Graças a uma invocação e uma boa rolagem, o esforço do Lutar chegou a Épico (+7). Ela atribuiu um Bom (+3) para o primeiro que parece ser o veterano e Razoável (+2) para cada um dos outros dois, em um total de sete. Então cada um rola para se defender.

Em algumas circunstâncias especiais, como em uma explosão ou similar, você pode querer realizar um ***ataque de zona*** contra todo mundo em uma zona, seja amigo ou inimigo. Aqui, você não divide seu esforço, cada alvo precisará se defender contra sua rolagem total. As circunstâncias e o método devem ser adequados para se fazer isso, geralmente o narrador exigirá que você invoque um aspecto ou use uma façanha para obter permissão.

Se você deseja criar uma vantagem afetando uma zona inteira ou grupo, ***mire na cena*** ao invés disso: coloque um aspecto único na zona ou na própria cena ao invés de colocar aspectos separados em cada um dos alvos. Isso tem a vantagem adicional de reduzir a contabilidade de aspectos no geral. Se alguém insistir em criar um aspecto separado em cada alvo, ele deve se restringir ao método de divisão de esforço.

Em qualquer um dos métodos, todos os alvos devem ocupar a mesma zona. O narrador pode permitir exceções ocasionais baseado no método e/ou às circunstâncias.

Somente um tipo de ação deve ser usado, como atacar vários alvos de uma vez, resolver dois problemas de uma vez com superar ou influenciar a mente de alguns PdNs importantes para criar uma vantagem. O narrador pode permitir os dois tipos de ações diferentes em circunstâncias especiais, mas essas ações devem fazer sentido para a perícia usada por
ambas.

## Potência de Armas e Armaduras

Quer explorar um pouco mais da vibe de equipamentos de combate que outros jogos tem? Considere usar Potências de Armas e Armaduras. Resumindo, ser atingido por uma arma irá feri-lo mais e ter uma armadura prevenirá que isso aconteça (você poderia simular isso com façanhas, mas usar as caixas de façanha pode não parecer adequado para você).

Uma Potência de ***Arma*** é adicionada à tensão de um ataque bem sucedido. Se você tem Arma:2, significa que qualquer golpe inflige 2 tensões a mais que o normal. Isso conta para empates: você inflige estresse em um empate *em vez* de receber um impulso.

Uma Potência de ***Armadura*** reduz a tensão de um golpe bem sucedido. Assim, Armadura:2 faz com que qualquer golpe cause 2 tensões a menos que o usual. Se você acertar, mas a armadura do alvo reduz a tensão do ataque para 0 ou menos, você obtém um impulso para usar contra seu alvo, porém não causa dano algum.

Escolha cuidadosamente o nível desses valores. Fique de olho em como eles poderiam causar uma consequência (ou pior) em um empate. Recomendamos uma variação de 0 a 4 no máximo.

# Que versão é essa?

Desde o final de 2012, existe apenas uma versão do Fate da Evil Hat: ***Fate Básico***, a quarta edição do sistema.

Ah, mas e o ***Fate Acelerado***, pode perguntar o atento fã de Fate? O fato é que também é Fate Core, é o mesmo sistema apenas com ***diferentes ajustes*** (ou seja, opções de configuração) para trilhas de estresse, lista de perícias, façanhas e design de PdN. Quaisquer diferenças aparentes nas *funções essenciais* do sistema são devidas a acidentes de desenvolvimento paralelo e podem ser consideradas não intencionais - com um pedido sincero de desculpas para advogados de regras que existam por aí\! Se houver um conflito entre os designs de regras, ***Fate Básico*** é a autoridade.

Essas duas perspectivas do ***Fate Básico*** se unem aqui no ***Fate Condensado***, bem literalmente, de fato. Condensado começou como um texto Acelerado, menos todos os ajustes do Acelerado, substituídos pela versão do Básico. A partir desse ponto de partida, aplicamos 8 anos de experiência de jogos da comunidade para refinar e esclarecer. Esse esforço produziu algumas pequenas diferenças como notado na [página xx](#para-veteranos-mudanças-do-fate-básico), mas quer você escolha jogar com ou sem elas, o sistema ainda é ***Fate Básico*** no final do dia. Nós sentimos que o Condensado é uma melhoria com certeza, mas não deve existir uma guerra entre edições aqui (e por favor não inicie uma). É tudo ***Fate Básico***.

## O que veio antes

Fate começou como uma modificação do sistema Fudge por volta dos anos 2000, culminada por algumas conversas acaloradas entre Fred Hicks e Rod Donoghue sobre como eles poderiam fazer para rodar outra partida de Amber. As versões que surgiram entre essa época e 2005 foram gratuitas, digitais e liberadas para a comunidade online do Fudge com uma recepção surpreendentemente entusiasmada. Elas foram de “Fate Zero” para Fate 2.0.

Então, Jim Butcher ofereceu a Fred e Rob uma chance de criar um RPG baseado em Dresden Files, desencadeando o estabelecimento da Evil Hat como uma empresa e uma nova tomada no sistema Fate, visto primeiramente no ***Espírito do Século*** (2006) e eventualmente no ***The Dresden Files RPG*** (2010). A versão de Fate encontrada em ambos (e graças ao licenciamento aberto em vários outros) foi o Fate 3.0. O esforço de extrair o sistema (feito por Leonard Balsera e Ryan Macklin) para apresentá-lo por si só, levou-o a passar por várias melhorias, da qual surgiu o ***Fate Básico***.

## Licenciamento

De uma forma ou de outra, Fate sempre teve um licenciamento aberto. Você pode encontrar detalhes do licenciamento do Fate para seus projetos (em inglês) em <http://www.faterpg.com/licensing>
