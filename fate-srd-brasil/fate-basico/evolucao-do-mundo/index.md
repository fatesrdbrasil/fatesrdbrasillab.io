---
title: Evolução do Mundo
layout: default
---

## Evolução do Mundo

Os personagens não são os únicos que mudam em resposta aos eventos do jogo. Eles deixam suas marcas por onde passam. Problemas que eram críticos no início do jogo são resolvidos ou alterados de alguma forma. Outras coisas que não eram um problema agora ressurgem com uma gravidade maior. Antigos adversários caem no esquecimento e novos surgem.

Enquanto os jogadores modificam seus personagens através dos marcos, o Narrador deve ficar atento aos aspectos elaborados durante a criação do jogo, para verificar se precisam ser modificados devido às ações dos jogadores (ou por mera falta de uso).

Seguem abaixo algumas orientações referentes a isso.

#### Para Marcos Menores

-  Você precisa adicionar novos locais ao jogo devido ao que os PJs fizeram? Se for assim, crie alguns PdNs para ajudá-lo a dar mais personalidade ao novo ambiente e adicione algumas questões a serem resolvidas nesse lugar.
-  Os PJs resolveram alguma questão local? Livre-se do aspecto ou talvez altere-o para representar como a questão foi resolvida (***Na Sombra de Um Necromante*** poderia se tornar ***Memórias da Tirania***, por exemplo).

> O grupo alcança um marco menor por ter resgatado o herdeiro do trono de Ondrin dos capangas de Zoidai Vasa, a Rainha do Crime. Foi uma pequena vitória que pode fornecer recompensas interessantes, já que agora eles têm um aliado poderoso, a Família Real de Ondrin.
> 
> Amanda pensa um pouco sobre que tipo de alterações poderiam ocorrer devido a vitória do grupo. Ela não precisa adicionar um novo local, mas acha que Zoidai pode guardar algum rancor da família, agora que o filho do duque foi resgatado. Ela decide alterar o aspecto ***Sociedade Secreta Com A Rainha do Crime*** para ***A Um Passo Da Guerra Com Zoidai*** para representar a mudança na história, assim como o desejo de guerra do Raiduque de Ondrin.

#### Para Marcos Significativos

-  Os PJs resolveram alguma questão que envolvia o mundo inteiro? Se sim, remova (ou altere) o aspecto.
-  Os PJs criaram alterações permanentes em algum local? Se sim, crie outra questão para reﬂetir isso, para melhor ou pior.

> Mais tarde, o grupo derrota e expulsa do planeta o pistoleiro Vega Nexos, tenente de Zoidai. A Rainha do Crime ainda é uma ameaça, mas seu poder foi significantemente reduzido; essa é uma grande vitória. Fräk acabou com Vega em um combate direto, e agora ele não é mais um problema; isso resolve a questão ***Todos Temem Vega Nexos***, que pairava sobre o mundo de jogo. Amanda risca a questão, ainda sem saber como substituí-la.
> 
> Eles também criaram alterações permanentes em Ondrin. A região não pertence mais a Zoidai, e embora a maioria da população seja grata por isso, alguns de seus servos continuam a criar problemas. Amanda substitui o aspecto do local, de ***Sob o Controle de Zoidai Vasa*** para ***Sorrisos Amedrontados***, para expressar a paranoia atual.

#### Para Marcos Maiores

- Os PJs realizaram alterações permanentes no mundo de jogo? Então, forneça uma nova questão para reﬂetir isso, para o bem ou mal.

> Com a derrota épica de Zoidai Vasa pelos PJs, Amanda decide que a criminalidade começará uma luta pelo poder. Ela decide criar o aspecto ***Submundo Sem Líder*** para reﬂetir isso.

Você não precisa realizar essas alterações de forma precisa ou regularmente como os jogadores – seja o mais natural possível. Em outras palavras, foque em mudar os aspectos com os quais os personagens interagiram diretamente.

Se há aspectos ainda não explorados, mantenha-os na manga caso ache que chegará sua vez. No entanto, você também pode alterá-los para torná-los mais relevantes naquele momento ou simplesmente para passar uma sensação de imersão no mundo aos PJs.

>  Zoidai Vasa não era o único perigo da região. Os Carbonívoros atacam de tempos em tempos e os governantes de Naraka e Cantor 7 estão em guerra no sistema. Amanda pretende incluir mutantes em breve, então mantém ***As Aparências Enganam*** como aspecto regional. A variedade de problemas permite que os PJs façam várias decisões interessantes.

Lembre-se também que se os PJs resolvem alguma questão iminente, outro deve surgir para tomar seu lugar. Não se preocupe com isso imediatamente – você precisa passar aos jogadores a sensação de que conseguem causar mudanças permanentes no jogo. Porém, se perceber que não há dificuldades suficientes para mover o jogo, está na hora de esquentar as coisas e colocar uma nova questão a ser resolvida, seja no mundo de jogo como um todo ou em um local específico.

### Lidando Com PdNs

Lembre-se, Narrador, que ao adicionar um novo local ao mundo de jogo, você deverá incluir também ao menos um PdN. Às vezes isso pode significar trazer uma pessoa de um lugar que não será mais usado.

Da mesma forma, quando há uma mudança significativa em uma questão local ou do mundo do jogo, é preciso avaliar se os PdNs atuais são suficientes para expressar essa mudança. Caso contrário, você precisará adicionar mais alguns ou alterar de forma significativa um PdN que já possui – adicionar aspectos ou revisar os existentes para manter o personagem relevante à questão a qual está ligado.

Na maioria das vezes será bem óbvio quando um personagem novo for necessário – quando alguém morre ou é removido permanentemente do jogo ou quando as coisas estão ficando chatas, é hora de mudar um pouco.

> Quando os heróis resgatam o herdeiro do trono, a família do Raiduque se encontra em dívida com eles. Para representar isso, Amanda muda alguns de seus aspectos para torná-los mais amigáveis.
> 
> Ela acredita também que alguém vai querer assumir o submundo. Ela acha interessante a ideia de tornar o herdeiro do trono, Hali Lachesis, um amante de Zoidai que não está feliz com a derrota e expulsão da rainha criminosa. Ele decide então se tornar o ***Rei Tirano da Nebulosa Cadente***. Amanda anota alguns aspectos criados na hora e o transforma no mais novo vilão do cenário, pegando todos de surpresa.

### PdNs Recorrentes

Há duas formas básicas de reutilizar PdNs. Você pode usá-los para *mostrar o quanto os PJs evoluíram desde que começaram* ou usá-los para *mostrar como o mundo está respondendo à sua evolução*.

Na primeira, você não altera o PdN, pois esse é o ponto – a próxima vez que os PJs o encontrarem, eles já o terão ultrapassado ou terão novas preocupações; talvez tenham evoluído rapidamente, deixando o PdN para trás. Pode ser preciso alterar a importância dele no jogo – se ele foi um PdN importante, agora ele pode ter se tornado um PdN de suporte graças à evolução dos PJs.

Na segunda opção, você permite ao PdN avançar igualmente com os PJs – adicionando novas perícias, alterando seus aspectos, criando mais uma ou duas façanhas ou qualquer outra coisa necessária para manter o equilíbrio com os PJs. O tipo de PdN pode perdurar como um inimigo que retorna em diversos arcos, ou ao menos passa uma certa noção de continuidade conforme os PJs ficam mais fortes e inﬂuentes.

> Zoidai Vasa evoluiu junto com os PJs. Ela foi uma vilã principal e Amanda quis mantê-la relevante e desafiadora até o momento de sua derrota final. Logo, toda vez que os PJs atingiam um marco, ela aplicava os mesmos efeitos a Zoidai. Ela também efetuou alguns ajustes aqui e ali (alterou aspectos, trocou algumas perícias) como uma resposta ao que os PJs fizeram.
> 
> Talar, um dos agentes chefes de Zoidai em Ondrin, também mudou desde o último confronto. Ele era um PdN importante e a luta com ele havia sido planejada como ponto alto da sessão. Os PJs, porém, passaram direto por ele, convencendo-o a deixá-los entrar, o que fez com que ele se tornasse menos relevante depois disso. Ele ficou ressentido e entrou no caminho dos PJs algumas vezes, mas não conseguiu evoluir tão rápido quanto eles. A última vez que o encontraram, ele levou a maior surra e fugiu com o rabo entre as pernas.

- [« Evolução e Alteração](../evolucao-e-alteracao/)
- [Extras »](../extras/)

