I"dU<h1 id="invoking--compelling-aspects">Invoking &amp; Compelling Aspects</h1>

<h2 id="invoking-aspects">Invoking Aspects</h2>

<p>The primary way you’re going to use aspects in a game of Fate is to invoke
them. If you’re in a situation where an aspect is beneficial to your character
somehow, you can invoke it.</p>

<p>In order to invoke an aspect, explain why the aspect is relevant, spend a
<a href="../../fate-core/fate-points" title="Fate Points">fate point</a>, and you can
choose one of these benefits:</p>

<ul>
  <li>Take a +2 on your current skill roll after you’ve rolled the dice.</li>
  <li>Reroll all your dice.</li>
  <li>Pass a +2 benefit to another character’s roll, if it’s reasonable that the aspect you’re invoking would be able to help.</li>
  <li>Add +2 to any source of passive opposition, if it’s reasonable that the aspect you’re invoking could contribute to making things more difficult. You can also use this to create passive opposition at Fair (+2) if there wasn’t going to be any.</li>
</ul>

<h3 id="the-reroll-vs-the-2">The Reroll Vs. The +2</h3>

<p>Rerolling the dice is a little riskier than just getting the +2 bonus, but has
the potential for greater benefit. We recommend you reserve this option for
when you’ve rolled a –3 or a –4 on the dice, to maximize the chance that
you’ll get a beneficial result from rerolling. <a href="../../fate-core/taking-action-dice-ladder#the-math-behind-the-
dice">The odds are better that way
</a>.</p>

<p>It doesn’t matter when you invoke the aspect, but usually it’s best to wait
until after you’ve rolled the dice to see if you’re going to need the benefit.
You can invoke multiple aspects on a single roll, but you cannot invoke the
same aspect multiple times on a single roll. So if your reroll doesn’t help
you enough, you’ll have to pick another aspect (and spend another fate point)
for a second reroll or that +2.</p>

<p>The group has to buy into the relevance of a particular aspect when you invoke
it; GMs, you’re the final arbiter on this one. The use of an aspect should
make sense, or you should be able to creatively narrate your way into ensuring
it makes sense.</p>

<p>Precisely how you do this is up to you. Sometimes, it makes so much sense to
use a particular aspect that you can just hold up the fate point and name it.
Or you might need to embellish your character’s action a little more so that
everyone understands where you’re coming from. (That’s why it is recommended
that you make sure that you’re on the same page with the group as to what each
of your aspects means—it makes it easier to justify bringing it into play.)</p>

<blockquote>
  <p>Landon is trying to win a contest of wits with a rival in a tavern, and the
skill they’re currently using is Rapport, which they’ve described as
“attempting to shame each other as politely as possible.”</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Lenny rolls badly on one of the contest exchanges, and says, “I want to
invoke <span class="aspect">The Manners of a Goat</span>.” Amanda gives him a
skeptical look and replies, “What happened to ‘as politely as possible’?”</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Lenny says, “Well, what I was thinking about doing was making some kind of
ribald but not vulgar innuendo about the guy’s parentage, in order to get the
crowd at the bar to laugh at him, perhaps despite themselves. I figure that
bawdy put-downs are precisely my cup of tea.”</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Amanda nods and says, “Okay, I’ll take that.”</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Lenny spends the fate point.</p>
</blockquote>

<p>If you want to see more examples of invoking an aspect, they’ve been scattered
them throughout the book—they’re so integral to how Fate works that they
naturally end up in many examples of play.</p>

<p>If the aspect you invoke is on someone else’s character sheet, including
situation aspects attached to them, and the invoke is to their disadvantage,
you give them the fate point you spent. (Invoking a third party’s aspect is
treated just like invoking an unattached situation aspect.) They don’t
actually get to use it until after the end of the scene, though.</p>

<h3 id="the-ellipsis-trick">The Ellipsis Trick</h3>

<p>If you want an easy way to ensure you have room to incorporate aspects into a
roll, try narrating your action with an ellipsis at the end (“…”), and then
finish the action with the aspect you want to invoke. Like this:</p>

<p>Lily says, “Okay, so I raise my sword up and…” (rolls dice, hates the
result) “…and it looks like I’m going to miss at first, but it turns out to
be a quick feint-and-slash, a classic move from the <span class="aspect">Infamous Girl
with Sword</span>” (spends the fate point).</p>

<p>Ryan says, “So I’m trying to decipher the runes in the book and…” (rolls the
dice, hates the result) “…and <span class="aspect">If I Haven’t Been There, I’ve
Read About It</span>…” (spends a fate point) “…and I easily start rambling
about their origin.”</p>

<h3 id="free-invocations">Free Invocations</h3>

<p>You don’t always have to pay a fate point to invoke an aspect—sometimes it’s
free.</p>

<p>When you succeed at <a href="../../fate-core/four-
actions" title="Creating An Advantage">creating an advantage</a>, you “stick” a free invocation onto an
aspect. If you succeed with style, you get two invocations. Some of the other
actions also give you free boosts.</p>

<p>You also get to stick a free invocation on any consequences you inflict in a
conflict.</p>

<p>Free invocations work like normal ones except in two ways: no fate points are
exchanged, and you can stack them with a normal invocation for a better bonus.
So you can use a free invocation and pay a fate point on the same aspect to
get a +4 bonus instead of a +2, two rerolls instead of one, or you can add +4
to another character’s roll or increase passive opposition by +4. Or you could
split the benefits, getting a reroll and a +2 bonus. You can also stack
multiple free invocations together.</p>

<p>After you’ve used your free invocation, if the aspect in question is still
around, you can keep invoking it by spending fate points.</p>

<blockquote>
  <p>Cynere succeeds on an attack, and causes her opponent to take the
<span class="aspect">Cut Across the Gut</span> consequence. On the next exchange, she
attacks him again, and she can invoke that for free because she put it there,
giving her a +2 or a reroll.</p>
</blockquote>

<p>If you want, you can pass your free invocation to another character. That
allows you to get some teamwork going between you and a buddy. This is really
useful in a conflict if you want to set someone up for a big blow—have
everyone create an advantage and pass their free invocations onto one person,
then that person stacks all of them up at once for a huge bonus.</p>

<h3 id="for-veterans">For Veterans</h3>

<p>In other Fate games, free invocations were called “tagging.” This was one bit
of jargon too many. You can still call it that if you want—whatever helps you
and your table understand the rule.</p>

<h3 id="hostile-invocations-excerpted-from-fateful-concepts-hacking-contests">Hostile Invocations (Excerpted from <em>Fateful Concepts: Hacking Contests</em>)</h3>

<p>Whenever you invoke another player character’s aspects against them—notably
but not only consequences—that’s a <strong>hostile invocation</strong>, and that
character’s player gets the fate point. This rule is an important part of Fate
because it’s a way to give a player fate points. But this doesn’t just count
for character aspects! If there’s an aspect effectively attached to or
controlled by a <abbr title="Player Character">PC</abbr>, like some aspect-worthy gear they’re holding or an
advantage they created, and it’s invoked against that character, that’s a
hostile invocation.</p>

<p>If that aspect isn’t invoked directly against the character, but the action
works against that character’s interests (which is generally the only reason
you could invoke someone else’s aspect), that player still gets the fate
point. This rule is key to remember when one player hostilely invokes
another’s aspect.</p>

<p>Fate points from hostile invocations can’t be spent on the situation where
they’re gained. They’re available starting on the next scene. (Otherwise, you
could just spend back and forth and draw a contest out by invoking and
counter-invoking each other.) Oh, and usually everyone can invoke
<span class="aspect">On Fire</span> even if one character deliberately created it as
an advantage, because no one actually controls that aspect. (Unless you’re
talking about magic or something else that grants control, hence “usually.”)</p>

<p>This section—<em>Hostile Invocations</em>—is an excerpt from Ryan Macklin’s <em><a href="http://www.drivethrurpg.com/product/143487
/Fateful-Concepts-Hacking-Contests?affiliate_id=144937">Fateful
Concepts: Hacking Contests</a></em> and is released under
a <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported
license</a>. If you find this useful
consider <a href="http://www.drivethrurpg.com/product/143487
/Fateful-Concepts-Hacking-Contests?affiliate_id=144937">buying the e-book</a>.</p>

<p>To use this text, include the following attribution in your work: “This work
is based on Fateful Concepts: Hacking Contests by <a href="http://ryanmacklin.com/">Ryan
Macklin</a>, licensed for our use under the <a href="http://creativecommons.org/licenses/by/3.0/">Creative
Commons Attribution 3.0 Unported
license</a>.”</p>

<h2 id="compelling-aspects">Compelling Aspects</h2>

<p>The other way you use aspects in the game is called a <strong>compel</strong>. If you’re in
a situation where having or being around a certain aspect means your
character’s life is more dramatic or complicated, someone can compel the
aspect. That aspect can be on your character, the scene, location, game, or
anywhere else that’s currently in play. We’ll start with character aspects,
and then talk about <a href="../../fate-core/invoking-
compelling-aspects" title="Compelling Situation Aspects">situation aspects</a> in a bit.</p>

<p>In order to compel an aspect, explain why the aspect is relevant, and then
make an offer as to what the complication is. You can negotiate the terms of
the complication a bit, until you reach a reasonable consensus. Whoever is
getting compelled then has two options:</p>

<ul>
  <li>Accept the complication and receive a fate point</li>
  <li>Pay a fate point to prevent the complication from happening</li>
</ul>

<p>The complication from a compel occurs regardless of anyone’s efforts—once
you’ve made a deal and taken the fate point, you can’t use your skills or
anything else to mitigate the situation. You have to deal with the new story
developments that arise from the complication.</p>

<p>If you prevent the complication from happening, then you and the group
describe how you avoid it. Sometimes it just means that you agree that the
event never happened in the first place, and sometimes it means narrating your
character doing something proactive. Whatever you need to do in order to make
it make sense works fine, as long as the group is okay with it.</p>

<p>GMs, you’re the final arbiter here, as always—not just on how the result of a
compel plays out, but on whether or not a compel is valid in the first place.
Use the same judgment you apply to an invocation—it should make instinctive
sense, or require only a small amount of explanation, that a complication
might arise from the aspect.</p>

<p>Finally, and this is very important: <strong>if a player wants to compel another
character, it costs a fate point to propose the complication.</strong> The <abbr title="Game Master">GM</abbr> can
always compel for free, and any player can propose a compel on his or her own
character for free.</p>

<h3 id="for-veterans-1">For Veterans</h3>

<p>In other Fate games, you might have seen player-driven compels referred to as
“invoking for effect.” It is clearer to just call it a compel, no matter who
initiates it.</p>

<h3 id="types-of-compels">Types of Compels</h3>

<p>There are two major categories for what a compel looks like in the game:
events and decisions. These are tools to help you figure out what a compel
should look like and help break any mental blocks.</p>

<h4 id="events">Events</h4>

<p>An event-based compel happens to the character in spite of herself, when the
world around her responds to a certain aspect in a certain way and creates a
complicating circumstance. It looks like this:</p>

<ul>
  <li>You have <strong>__ aspect and are in __</strong> situation, so it makes sense that, unfortunately, <em>__</em> would happen to you. Damn your luck.</li>
</ul>

<p>Here are a few:</p>

<blockquote>
  <p>Cynere has <span class="aspect">Infamous Girl with Sword</span> while covertly
attending a gladiatorial contest, so it makes sense that, unfortunately, an
admirer would recognize her in the stands and make a huge fuss, turning all
eyes in the arena her way. Damn her luck.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Landon has <span class="aspect">I Owe Old Finn Everything</span> and is returning to
his home village after hearing it was sacked by barbarians, so it makes sense
that, unfortunately, Old Finn was captured and taken far into the mountains
with their war party. Damn his luck.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Zird has <span class="aspect">Rivals in the Collegia Arcana</span> and is attempting
to get an audience with their Inner Council, so it makes sense that,
unfortunately, his rivals force the Collegia to demand he provide a detailed
account of his highly-coveted research to re-establish his relationship with
the organization. Damn his luck.</p>
</blockquote>

<p>As you’ll see with decision-based compels, the real mileage is in the
complication itself. Without that, you don’t really have anything worth
focusing on—the fact that the PCs continually have complicated and dramatic
things happen to them is, well, exactly what makes them PCs in the first
place.</p>

<p>GMs, event-based compels are your opportunity to party. You’re expected to
control the world around the PCs, so having that world react to them in an
unexpected way is pretty much part and parcel of your job description.</p>

<p>Players, event-based compels are great for you. You get rewarded simply by
being there—how much more awesome can you get? You might have a difficult time
justifying an event-based compel yourself, as it requires you to assert
control over an element of the game that you typically aren’t in charge of.
Feel free to propose an event-based compel, but remember that the <abbr title="Game Master">GM</abbr> has the
final say on controlling the game world and may veto you if she’s got
something else in mind.</p>

<h4 id="decisions">Decisions</h4>

<p>A decision is a kind of compel that is internal to the character. It happens
because of a decision he makes, hence the name. It looks like this:</p>

<ul>
  <li>You have <strong>__ aspect in __</strong> situation, so it makes sense that you’d decide to <strong>__. This goes wrong when __</strong> happens.</li>
</ul>

<p>Here are a few:</p>

<blockquote>
  <p>Landon has <span class="aspect">The Manners of a Goat</span> while trying to impress
a dignitary at a royal ball, so it makes sense that he’d decide to share some
boorish, raunchy humor and/or commentary. This goes wrong when he discovers
she’s the princess of this country, and his offense is tantamount to a crime.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Cynere has <span class="aspect">Tempted by Shiny Things</span> while touring an
ancient museum, so it makes sense that she’d decide to, ahem, liberate a
couple of baubles for her personal collection. This goes wrong when she
discovers that the artifacts are cursed, and she’s now beholden to the Keepers
of the Museum if she wants the curse lifted.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>Zird has <span class="aspect">Not the Face!</span> when he gets challenged to a
barfight, so it makes sense that he’d decide to back down from the challenge.
This goes wrong when the rest of the patrons decide he’s a coward and throw
him unceremoniously out into the street.</p>
</blockquote>

<p>So the real dramatic impact from these kinds of compels is not what decision
the character makes, most of the time—it’s how things go wrong. Before
something goes wrong, the first sentence could be a prelude to making a skill
roll or simply a matter of roleplaying. The complication that the decision
creates is really what makes it a compel.</p>

<p>GMs, remember that a player is ultimately responsible for everything that the
character says and does. you can offer decision-based compels, but if the
player doesn’t feel like the decision is one that the character would make,
don’t force the issue by charging a fate point. instead, negotiate the terms
of the compel until you find a decision the player is comfortable making, and
a complication that chains from that decision instead. if you can’t agree on
something, drop it.</p>

<p>The decision part should be very self-evident, and something a player might
have been thinking about doing anyway. The same goes for players trying to
compel NPCs or each other’s PCs—make sure you have a strong mutual
understanding of what that <abbr title="Non-Player Character">NPC</abbr> or other character might do before proposing
the compel.</p>

<p>Players, if you need fate points, this is a really good way of getting them.
If you propose a decision-based compel for your character to the <abbr title="Game Master">GM</abbr>, then what
you’re basically asking is for something you’re about to do to go wrong
somehow. You don’t even have to have a complication in mind—simply signaling
the <abbr title="Game Master">GM</abbr> should be enough to start a conversation. GMs, as long as the compel
isn’t weak (as in, as long as there’s a good, juicy complication), you should
go with this. If the compel is weak, poll the rest of the group for ideas
until something more substantial sticks.</p>

<p>If you offer a decision-based compel, and no one can agree on what the
decision part should be, it shouldn’t cost a fate point to counter—just drop
it. Countering a decision-based compel should only mean that the “what goes
wrong” part doesn’t happen.</p>

<p>GMs, remember that a player is ultimately responsible for everything that the
character says and does. You can offer decision-based compels, but if the
player doesn’t feel like the decision is one that the character would make,
don’t force the issue by charging a fate point. Instead, negotiate the terms
of the compel until you find a decision the player is comfortable making, and
a complication that chains from that decision instead. If you can’t agree on
something, drop it.</p>

<h3 id="retroactive-compels">Retroactive Compels</h3>

<p>Sometimes, you’ll notice during the game that you’ve fulfilled the criteria
for a compel without a fate point getting awarded. You’ve played your aspects
to the hilt and gotten yourself into all kinds of trouble, or you’ve narrated
crazy and dramatic stuff happening to a character related to their aspects
just out of reflex.</p>

<p>Anyone who realizes this in play can mention it, and the fate point can be
awarded retroactively, treating it like a compel after the fact. GMs, you’re
the final arbiter. It should be pretty obvious when something like this
occurs, though—just look at the guidelines for event and decision compels
above, and see if you can summarize what happened in the game according to
those guidelines. If you can, award a fate point.</p>

<h3 id="compelling-with-situation-aspects">Compelling with Situation Aspects</h3>

<p>Just like with every other kind of aspect use, you can use situation aspects
(and by extension, game aspects) for compels. Because situation aspects are
usually external to characters, you’re almost always looking at event-based
compels rather than decision-based ones. The character or characters affected
get a fate point for the compel.</p>

<p>Here are a few examples:</p>

<blockquote>
  <p>Because the warehouse is <span class="aspect">On Fire</span>, and the player
characters are trapped in the middle of it, it makes sense that,
unfortunately, the ruffian they’re chasing can get away in the confusion. Damn
their luck.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>The manor house Cynere is searching through is <span class="aspect">Littered with
Debris</span>, so it makes sense that, unfortunately, the city guard is going
to arrive there before she finds what she’s looking for, which will leave her
with a lot of explaining to do. Damn her luck.</p>
</blockquote>

<blockquote>

</blockquote>

<blockquote>
  <p>The ancient library Zird is currently working in has <span class="aspect">Layers of
Dust</span> everywhere, so it makes sense that, unfortunately, while he might
be able to find the information he’s looking for, the bounty hunter pursuing
him will know that he was here. Damn his luck.</p>
</blockquote>

<ul>
  <li><a href="/fate-srd/fate-core/making-good-aspect">« Making A Good Aspect</a></li>
  <li><a href="/fate-srd/fate-core/using-aspects-roleplaying">Using Aspects For Roleplaying »</a></li>
</ul>

:ET