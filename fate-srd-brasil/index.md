---
title: Fate SRD em português
layout: page
---


- [Fate Sistema Básico](fate-basico/)
- [Fate Acelerado](fate-acelerado/)
- [Fate Ferramentas de Sistema](ferramentas-de-sistema/)
- [Fate Condensado](fate-condensado/)
