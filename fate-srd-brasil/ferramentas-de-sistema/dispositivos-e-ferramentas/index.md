---
title: Dispositivos e Ferramentas
layout: default
---

 
## Dispositivos e Ferramentas

No Fate Sistema Básico, equipamentos podem ser simples como um aspecto, como um **_Lançador de Arpéu Magnético_** , ou uma façanha, como “ _Lançador de Arpéu Magnético:_ +2 para superar usando Atletismo ao escalar ou balançar de um local ao outro quando houver uma âncora de metal por perto”. Porém, para um pouco mais de detalhes, isso pode ser combinado em um único extra como um _dispositivo_.

### Funções e Defeitos

Dispositivos já vêm com dois aspectos grátis – um **aspecto de função** e um **aspecto de defeito**. A função diz qual o propósito do dispositivo e o defeito representa algum problema existente. Você pode pensar na função como o conceito e seu defeito como a dificuldade ou uma consequência que nunca desaparece. Isso não influencia os aspectos pessoais de seu personagem.

---

#### Lançador De Arpéu Magnético

**Função: _Gancho Eletromagnético de Alta Potência_**
**Defeito: _Ainda Resolvendo Falhas_**

---

### Façanhas

Dê uma ou mais façanhas ao dispositivo para refletir as vantagens mecânicas que ele confere a seu usuário. Essas façanhas custam uma recarga cada.

---
#### Lançador De Arpéu Magnético

##### Aspectos

**Função: _Gancho Eletromagnético de Alta Potência_**
**Defeito: _Ainda Resolvendo Falhas_**

##### Façanhas

**CLANG!:** Gaste um ponto de destino para firmar a garra magnética em um objeto metálico de uma forma dramática, como um veículo se movendo em alta velocidade, um rifle sônico que esteja caindo ou a parede do outro lado de um abismo em uma estação espacial de construção questionável.

**Tiro Malandro:** +2 para criar vantagem usando Atirar quando usa a lançador de arpéu para balançar, desarmar um oponente ou criar uma barreira.

**Custo:** 2 De Recarga

--- 
 
Os dispositivos também não precisam ser dispositivos no sentido literal. Você pode facilmente, por exemplo, usar estas regras para criar itens mágicos em um cenário de fantasia.

---

#### O Anel Da Verdade


##### Aspectos
**Função: _Detector Mágico de Mentiras_**
**Defeito: _O Portador é Amaldiçoado a Dizer Sempre a Verdade_**

##### Façanhas

**Perfurar o Véu da Mentira:** Gaste um ponto de destino para, durante a cena, perguntar até três vezes ao Narrador se alguém está mentindo. O Narrador, por sua vez, deve responder verdadeiramente.

**Custo:** 1 De Recarga


---
 
### Defeitos Adicionais

É possível incluir falhas adicionais para reduzir o custo em recarga do dispositivo, ao valor de uma recarga por defeito adicional. O custo mínimo para um dispositivo com qualquer quantidade de façanhas é 1 recarga, independentemente de quantas falhas ele possua.

Narradores, esses defeitos não podem ser superficiais, como ***Alguma Interferência Quando Está no Fundo do Oceano***. Se acabar deixando algo assim passar despercebido, certifique-se de que o jogador saiba que terá que passar mais tempo do que o esperado nas profundezas do oceano; na mais funda das profundezas.
 
---

Um defeito adicional para o Lançador de Arpéu Magnético pode ser ***Pesado e Desengonçado.***

O Anel da Verdade pode ter o defeito adicional ***Procurado Pelos Servos de Ssask, Deus da Mentira.***
 
---
 
------

- [« Cibernética](../cibernetica/)
- [Monstros »](../monstros/)
