---
title: Riquezas
layout: default
---


## Riquezas

A importância do dinheiro em um jogo depende bastante do gênero. O Fate Sistema Básico sugere o uso da perícia [Recursos](../../fate-basico/mais-exemplos-de-extras/#riqueza), mas para alguns gêneros em que adquirir riqueza frequentemente seja a motivação principal, como um jogo cyberpunk ou a clássica fantasia medieval em masmorras, você pode querer algo um pouco mais flexível.

### Estresse de Riqueza

Uma alternativa é adicionar uma nova linha de estresse: Riqueza. Quando tenta realizar algo que pode se tornar mais fácil com a aplicação de dinheiro, você marca uma caixa de estresse de riqueza para receber um bônus em sua rolagem (veja a variante de estresse Esforço Extra, na página 49). 

Por exemplo, se estiver debatendo o preço de um propulsor usando Comunicação — ou Provocar, se estiver barganhando de forma desagradável — e falhar em sua rolagem, você poderia marcar uma caixa de estresse de riqueza para pagar caro pelo produto. Talvez esteja planejando um assalto e falhou em sua rolagem de Roubo? Marque uma caixa de estresse de riqueza para conseguir as ferramentas caras que precisa para o serviço. Não consegue encontrar o assassino que procura usando Contatos? Talvez marcar uma caixa de estresse de riqueza refresque a memória dos seus informantes, e assim por diante. Em outras palavras, representa ser bem-sucedido a um alto custo que neste caso é um custo literal.

### Riqueza Inicial

Isto depende bastante do seu jogo — os PJs são profissionais com emprego fixo ou vivem no aperto? —, mas começar com uma base de duas caixas de estresse de riqueza seria uma média boa. PJs recebem +1 caixa de estresse para cada aspecto que possuam relacionado a isso, até um máximo de três caixas extras de estresse.

### Obtendo Riqueza

Estresse de riqueza não desaparece por conta própria. Em vez disso, essas caixas só são restauradas quando você ganha mais bens – ouro, créditos, trocas ou qualquer coisa que tenha valor em seu jogo. Adquirir uma quantia de riqueza permite que você limpe qualquer número de caixas de estresse cujo o valor total não exceda o valor dessa quantia. Se uma ou mais caixas marcadas em sua ficha forem de um valor maior do que a quantia recebida, não faça nada com as caixas que excederem esse valor — não é riqueza o suficiente para fazer diferença a você. Por exemplo, se você adquire 3 de riqueza, poderá limpar sua primeira, segunda ou terceira caixa de estresse, se qualquer uma delas estiver marcadas, ou a primeira e segunda – mas não uma quarta, se for sortudo o bastante para ter uma.

A única forma de adicionar caixas de estresse de riqueza é através de um marco, ao trocar um aspecto existente por um que esteja ligado a algum tipo de riqueza.


------

- [« Companheiros e Aliados](../companheiros-e-aliados/)
- [Armas e Armaduras Alternativas »](../armas-e-armaduras-alternativas/)
