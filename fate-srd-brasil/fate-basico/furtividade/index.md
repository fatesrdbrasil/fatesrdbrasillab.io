---
title: Furtividade
layout: default
---

### Furtividade

A perícia Furtividade dificulta que outros o percebam, tanto quando se esconde como quando tenta se deslocar sem ser visto. Ela combina muito bem com a perícia Roubo.

- `O`{: .fate_font} **Superar:** Você pode usar Furtividade para superar qualquer situação que dependa de você passar despercebido. Esgueirar-se entre sentinelas e seguranças, esconder-se de um perseguidor, apagar evidências de que você esteve em um lugar e outras situações desse tipo são resolvidas com Furtividade.
- `C`{: .fate_font} **Criar Vantagem:** Você vai usar Furtividade para criar aspectos em você mesmo, se posicionando perfeitamente para um ataque ou emboscada em um conﬂito. Dessa forma, você pode estar ***Bem Escondido*** quando os guardas passarem por você e tirar vantagem disso ou ***Difícil de Encontrar*** quando estiver lutando no escuro.
- `A`{: .fate_font} **Atacar:** Furtividade não é utilizada em ataques.
- `D`{: .fate_font} **Defender:** de Percepção para encontrá-lo Você pode usar esta perícia para impedir tentativas , assim como para tentar apagar seus rastros e atrapalhar a rolagem da perícia Investigar de um perseguidor.

#### Façanhas Para Furtividade

-  **Mais Um Na Multidão:** +2 em Furtividade para se camuﬂar em uma multidão. O significado de “multidão” depende do ambiente – uma estação de metrô exige mais pessoas do que um bar pequeno.
-  **Invisibilidade Ninja:**  Uma vez por sessão, você pode desaparecer de vista ao custo de um ponto de destino, usando uma fonte de fumaça (pastilhas, bombas, etc.) ou outra técnica misteriosa. Isso coloca o impulso ***Desaparecido***. Enquanto estiver assim, ninguém pode atacar ou criar vantagem sobre você até que sejam bem-sucedidos em uma ação de superar com Percepção para saber onde você está (basicamente significa que eles abrirão mão de uma ação na tentativa). Esse aspecto desaparece assim que invocado, ou se alguém for bem-sucedido na rolagem de superar.
-  **Alvo Deslizante:** Contanto que esteja na escuridão ou nas sombras, você pode usar Furtividade para se defender contra ataques de Atirar de adversários que estejam ao menos a uma zona de distância.

- [« Enganar](../enganar/)
- [Investigar »](../investigar/)
