---
title: Capítulo 7 - Ferramentas Personalizadas
layout: default
---

# Capítulo 7 - Ferramentas Personalizadas

## Estresse


As caixas de estresse possuem um propósito bem específico no Fate Sistema Básico, mas essa não é a única forma de lidar com elas. Aqui apresentamos outras opções.

___Caixas de uma tensão:___ Cada caixa de estresse absorve apenas uma tensão de dano, mas marque quantas desejar de uma vez. Essa opção torna os personagens consideravelmente mais frágeis.

___Marque duas:___ Marque até duas caixas de uma vez, some seus valores e reduza o dano na mesma quantidade. Essa opção torna os personagens significativamente mais fortes – um personagem com cinco caixas de estresse pode receber até 9 de dano em uma cena sem precisar recorrer às consequências.

___Recuperação reduzida:___ O estresse se recupera gradualmente, não inteiro de uma vez. Ao invés de limpar uma caixa de estresse automaticamente ao fim de uma cena, apague-a e marque a caixa à esquerda dela (assumindo que haja alguma).

___Apenas bônus:___ Os personagens não possuem caixas de estresse no início, mas recebem caixas extras de estresse de perícias como Vigor e Vontade. Isso torna os personagens mais frágeis e enfatiza as consequências. 

___Esforço extra:___ Marque uma caixa de estresse voluntariamente a qualquer momento para receber um bônus. O bônus é igual ao valor da caixa marcada. Isso pode gerar resultados imprevisíveis em seu jogo. Isto não só pode criar criar mais variações nas rolagens, mas também pode substituir parcialmente a economia de pontos de destino. Este recurso pode se tornar um pouco mais viável quando combinado com qualquer uma das opções acima.

___Sem estresse:___ Os personagens não possuem caixas de estresse e não recebem caixas extras de perícias. Ao invés disso, essas perícias concedem uma consequência suave extra (física ou mental) quando for seu nível Bom (+3) ou duas quando for Ótimo (+4). Esta é a opção mais perigosa e dinâmica – cada acerto resulta em uma consequência e possui impacto narrativo – mas pode criar uma quantidade de aspectos difícil de se manejar.


------

- [« Conflito Social](../conflito-social/)
- [Consequências »](../consequencias/)
