---
title: Desafios
layout: default
---

## Desafios

Uma simples ação de superar é suficiente para lidar com um empecilho ou obstáculo – o herói precisa destrancar a porta, desarmar a bomba, descobrir uma informação vital, etc. Também é bastante útil quando os detalhes de como algo aconteceu não são tão importantes ou não vale a pena gastar tempo com isso, quando o que você precisa saber é que o personagem consegue realizar algo sem reforços ou gastos adicionais.

Às vezes, no entanto, as coisas ficam um pouco complicadas. Não é suficiente apenas abrir a porta, porque você também precisa conter a horda de zumbis que está atacando e realizar a magia que providenciará cobertura. Desarmar a bomba não é suficiente, porque você também precisa evitar que o dirigível caia enquanto evita que o cientista desacordado que você está resgatando se machuque.

**Um desafio é uma série de ações de superar que você usa para resolver uma situação especialmente complicada ou dramática**. Cada uma dessas ações usa uma perícia **diferente** para lidar com as tarefas ou as partes da situação e o conjunto dos resultados determinará o que acontece.

Narradores que estiverem em dúvida se o momento é adequado para um desafio devem se fazer as seguintes perguntas:

-  Cada tarefa individual pode gerar tensão e drama, independente das outras tarefas? Se todas as tarefas forem parte de um único objetivo, como "desconectar o detonador", "parar o relógio" e "se livrar do explosivo" quando desarmar uma bomba, então essa tarefa deverá ser uma única ação de superar, usando esses detalhes para explicar o que aconteceu se a rolagem der errado.
-  A situação exige perícias diferentes para ser resolvida? Conter os zumbis (Lutar) enquanto empurra uma barricada (Vigor) e conserta a carroça (Ofícios) para poder fugir seria uma situação adequada para um desafio.

Para organizar um desafio, simplesmente identifique as tarefas individuais u objetivos que a situação exige e resolva cada uma delas com uma rolagem separada de uma ação de superar (às vezes, apenas uma certa sequência de rolagens fará sentido para você; isso também é válido). Dependendo da situação um personagem pode precisar realizar diversas rolagens ou vários personagens podem participar.

> Bandu está tentando ativar as defesas da estação especial onde estão a fim de garantir a proteção do grupo de sobreviventes que vieram resgatar. Normalmente isso não seria muito interessante, exceto que ele está tentando realizar essa tarefa antes de serem devorados por uma horda de alienígenas selvagens que nasceram dos ovos no compartimento de carga.
>
> Amanda vê vários componentes nessa cena. Primeiro há o sistema de computadores, depois bloquear a entrada e, por último, manter os sobreviventes calmos. Isso indica rolagens de Conhecimentos, Ofícios e algum tipo de perícia social – Michel escolhe Comunicação.
>
> Assim, Michel rolará essas três perícias separadamente, uma rolagem para cada componente que Amanda identificou. Ela define uma oposição passiva Boa (+3) para cada uma delas – ela quer que ele tenha uma chance justa, mas deixando espaço para várias possibilidades de resultado.
>
> Agora estão prontos para começar.

Para conduzir o desafio, faça as jogadas de superar na ordem que parecer mais interessante, mas não decida nada sobre como a situação será resolvida antes de saber o resultado dos dados – é importante ter a liberdade de escolher a sequência dos eventos de cada rolagem na ordem que fizer mais sentido e for mais interessante. Jogadores que conseguirem um impulso em alguma de suas rolagens devem se sentir livres para usá-lo em outra rolagem do desafio, desde que faça sentido.

Narradores devem, depois de terminadas as rolagens, interpretar o conjunto de sucessos, falhas e custos de cada ação para determinar como a cena acontece. Esses resultados podem levar a outros desafios, disputas ou até mesmo conﬂitos.

> Michel respira fundo. "Vamos lá", ele diz, e pega os dados.
>
> Ele decide que primeiro irá garantir a segurança da sala, faz a rolagem de Ofícios, que possui num nível Bom (+3), e consegue 0 nos dados. Isso é um empate, o que lhe permite alcançar o objetivo a um custo menor. Amanda diz “Que tal se eu criar um impulso chamado ***Trabalho Apressado*** para usar contra você se for necessário – afnal você está com pressa”.
>
> Michel suspira e concorda, em seguida vai para a segunda rolagem do desafio: acalmar as pessoas no local com sua Comunicação Boa (+3). Ele faz a rolagem e consegue um resultado horrível de -3 nos dados! Agora ele tem a opção de falhar ou de ser bem-sucedido a um grande custo. Ele opta pelo sucesso, deixando Amanda pensar em algo para seu custo.
>
> Ela pensa um pouco. Como acalmar as pessoas ali a um grande custo? Então ela sorri. “Isso é mais um detalhe de história do que mecânico, mas sabe como é... você está usando Comunicação, então provavelmente você está bastante inspirado agora. Vejo que você poderia acidentalmente convencer alguns desses astronautas que os aliens não são uma grande ameaça, e que é totalmente viável lutar contra eles. Afinal de contas, você está cuidando das defesas da estação, não é mesmo?”
>
> Michel diz, “Mas eles precisam ficar dentro da sala pra se proteger!” Amanda sorri maliciosamente. Michel suspira de novo. “Certo. Algumas pessoas entendem errado a ideia e existe a chance de serem mortas. Já posso ouvi-los dizendo... Bandu, porque você deixou meu marido morrer? Fazer o quê. ” Amanda sorri ainda mais.
>
> Michel segue para a parte final do desafio – a ativação das defesas da sala com sua perícia Conhecimentos nível Ótimo (+4). Amanda invoca o impulso que conseguiu mais cedo e diz “´Sim, você está bastante desconcentrado vendo os alienígenas derrubarem sua barricada. Muito distraído”. Isso eleva a dificuldade para Excepcional (+5). Ele rola e consegue +2 o que o deixa com um resultado Fantástico (+6), o suficiente para ser bem-sucedido sem nenhum custo.
>
> Amanda concorda e juntos eles finalizam a descrição da cena – Bandu finaliza os comandos a tempo e as armas da nave começam a trabalhar. Alguns alienígenas a ponto de entrarem começam a rosnar e se afastar, feridos pelos lasers, e Bandu suspira de alívio... até ouvir os gritos de pânico dos astronautas do lado de fora da sala...
>
> Mas isso é a próxima cena.

Se você possuir algum impulso que não foi usado no desafio, sinta-se livre para mantê-lo pelo resto da cena ou até quando fizer sentido, caso os eventos do desafio se interliguem à próxima cena.

### Vantagens em Desafios

Você pode tentar criar vantagem em um desafio para ajudá-lo ou ajudar alguém. Criar vantagem não ajuda a completar um dos objetivos do desafio, mas falhar nessa rolagem pode criar um custo ou problema que trará um impacto negativo em um dos outros objetivos. Tenha cuidado ao usar essa tática; vantagens podem ajudar a completar tarefas de forma mais eficaz, mas tentar criá-las sempre traz riscos.

### Atacando em Desafios

Como você está indo contra uma oposição passiva em um desafio, nunca será preciso usar atacar. Se estiver em uma situação onde pareça razoável usar uma ação de ataque, você estará iniciando um conflito.

- [« Desafios, Disputas e Conflitos](../desafios-disputas-e-conflitos/)
- [Disputas »](../disputas/)

