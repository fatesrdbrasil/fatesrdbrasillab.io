---
title: Rostos E Lugares
layout: default
---

## Rostos E Lugares

**Decida quais pessoas e lugares são importantes.**

Neste momento as questões de sua campanha devem estar decididas e o grupo pode ter pensado em organizações ou grupos importantes no jogo.

Agora você dará rostos a essas questões e grupos para que os PJs possam interagir com esses elementos. Os PJs fazem parte de algum grupo que os representa ou são autônomos considerados exemplos de excelência no contexto das questões do jogo? Qualquer ideia poderá ser anotada em um pedaço de papel: um nome, a ligação com uma organização ou com uma questão específica e os aspectos importantes para a história.

Faça a mesma coisa para qualquer local que tenha relevância na sua ambientação. Será que há um lugar importante onde coisas acontecem que sejam marcantes para o mundo, para uma questão ou para os protagonistas? Se houver um lugar onde você consegue imaginar várias cenas acontecendo, é interessante falar um pouco sobre ele. Diferente dos PdNs, eles não precisam de aspectos.

O Narrador pode modificar essas pessoas e lugares depois, dependendo do seu papel na história. Talvez uma dessas ideias seja uma inspiração perfeita para um protagonista e pode ter certeza de que ideias novas surgirão ao longo do desenrolar da história.

Se alguma parte do seu cenário for um mistério que deve ser desvendado pelos personagens, defina-o apenas em termos gerais. Os detalhes específicos podem ser elaborados conforme são revelados no jogo.

> Após alguns minutos de debate, o grupo escreve o seguinte:
>
> -  Meegoo, o Falasiano, é um líder do crime local que secretamente serve ao Clã do Dedo. Seu aspecto é **Todos em Ondrin Me Temem**.
> -  O que nos leva a pensar ao planeta, Ondrin. Um planeta comercial coberto de lojas e postos de troca, o que o torna um centro econômico.
> -  Amanda pensa numa personagem simpática, Erta-5, uma androide que atende num bar em Ondrin. Ela está preocupada com a diminuição de clientes e a dificuldade em adquirir mercadorias, por causa do controle de Meegoo sobre os portos, e tema que será desmontada se o bar fechar. Seu aspecto é **Meu Trabalho É Minha Vida**.
> -  O Herdeiro, líder dos Bruxos de Moondor, cuja identidade é um mistério. Por isso, eles não criam aspectos,deixando os detalhes para Amanda criar em segredo.
> Eles poderiam continuar, mas sabem que terão mais ideias depois da criação de personagens e durante o jogo. O que têm agora é suficiente para ter uma noção geral do que está acontecendo no início do jogo.

------

### Erta-5

Bartender em Ondrin

+ ***Meu Trabalho É Minha Vida***

------

------

### Meegoo

Filiado ao Clã do Dedo

+ ***Todos em Ondrin Me Temem***

------

------

### O Herdeiro

Líder dos Bruxos de Moondor

------

- [« Questões De Ambientação](../questoes-de-ambientacao/)
- [Criando Personagens »](../criando-personagens/)
