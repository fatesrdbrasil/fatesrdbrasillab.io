---
title: Capítulo 5 - O Grande Jogo
layout: default
---

# Capítulo 5 - O Grande Jogo

## Eu Quero Emoção!

Às vezes uma pequena mudança é o suficiente, mas às vezes não. Pode haver um momento no qual você precise de algo grande, alterações dramáticas do sistema, algo que abale as estruturas.

------

- [« Custo de Façanhas](../custo-de-facanhas/)
- [Opções Para Criação de Personagens »](../opcoes-criacao-personagens/)
