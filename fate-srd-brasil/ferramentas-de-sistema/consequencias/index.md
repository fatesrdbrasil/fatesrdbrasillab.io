---
title: Consequências
layout: default
---

## Consequências


Assim como o estresse, as consequências podem preencher uma variedade de papéis em seu jogo, além do padrão apresentado no Fate Sistema Básico.

___Esforço extremo:___ Receba voluntariamente uma consequência a qualquer momento em troca de um bônus igual ao valor da consequência – uma consequência suave concede +2, uma moderada +4 e uma severa +6. Isso pode ser perigoso, tanto para o PJ quanto para a economia de pontos de destino, como o Esforço Extra, apresentado anteriormente.

___Consequências de grupo:___ Ao invés de mantê-las individualmente, PdNs principais e de suporte compartilham uma reserva de consequências de grupo. Isso lhe fornece a opção de “sacrificar” um PdN de suporte agora para tornar um PdN principal mais desafiador no futuro. Para cada dois PdNs de suporte que espera incluir no cenário, adicione duas consequências de grupo suaves e uma moderada, até o máximo de três moderadas. Para cada PdN principal, adicione outra consequência de grupo suave, moderada e severa. Se um PdN de suporte ou principal conceder o conflito, baseie a recompensa da concessão no número total de consequências que o PdN recebeu durante a cena.

---

O antagonista no jogo de super-heróis de Isa pertence a uma organização terrorista chamada QUIMERA. Minotauro e Scylla são os principais PdNs, enquanto Cerberus, Talos e Cerastes são PdNs de suporte. Com isso, Isa recebe uma reserva de quatro consequências suaves, três moderadas e duas severas para esse grupo de PdNs. 

Em uma das primeiras cenas, os PJs enfrentam Cerberus, Talos e Minotauro. Durante o conflito, Cerberus e Minotauro recebem uma consequência suave cada — então, Cerberus é derrotado, Talos concede a luta e Minotauro consegue escapar. Isa poderia optar por usar mais algumas consequências para manter Cerberus e Minotauro na luta, mas preferiu guardá-las para as próximas cenas. Além disso, ela recebe um ponto de destino pela concessão — apenas um, já que Talos não recebeu uma consequência.

Quando chegam à batalha mais importante, Isa possui uma consequência de grupo suave, uma moderada e duas severas
para usar com Scylla, Minotauro e Cerastes, os três PdNs importantes na cena, à medida que tentam realizar suas maquinações malignas.

---

**Consequências Colaterais:** Adicionalmente ao uso comum das consequências, os jogadores também podem fazer uso de três consequências compartilhadas, uma em cada grau de severidade. Isto representa o dano ao ambiente ou novas complicações na história, coisas como Inocentes Feridos ou Histeria Anti-Mutante. Os jogadores podem usá-las para evitar serem feridos, direcionando esse ferimento para o mundo ao seu redor. Você se livra de uma consequência colateral como qualquer outra, usando uma ação de superar com qualquer perícia que seja apropriada, com duas exceções. A primeira é que isso deve ser feito durante a cena em que a consequência surge. A segunda é que não há demora — ela desaparece imediatamente com uma rolagem boa o suficiente em uma perícia. Essa opção é mais adequada a alguns gêneros, como super-heróis, onde os PJs podem se preocupar mais com o mundo que os cerca.


------

- [« Ferramentas Personalizadas](../ferramentas-personalizadas/)
- [Zonas »](../zonas/)
