---
title: 1 - O Básico
layout: default
---
# 1 - O Básico

## Bem-Vindo ao Fate!

Se você nunca jogou um jogo de interpretação antes, aqui está a ideia principal: você e um grupo de amigos se reúnem para contar uma história interativamente sobre um grupo de personagens que vocês criaram. Você dirá quais os desafios e obstáculos que esses personagens irão encarar, como eles reagirão, o que eles dizem e fazem e o que está acontecendo com eles.

Mas não é apenas na conversa, às vezes você usará dados e as regras presentes neste livro para criar imprevistos na história e tornar as coisas mais excitantes.

Fate não vem com um cenário padrão, mas funciona bem com qualquer ideia que inclua **personagens proativos e capazes que vivem vidas dramáticas.** No próximo capítulo mostramos diversas maneiras de trazer à tona essa característica para temperar seus jogos.

------

### Novo no Fate

Se você é um novo jogador, tudo o que realmente precisa saber está neste capítulo e na sua ficha de personagem – o Narrador lhe ajudará a desenvolver o resto. Você pode querer verificar a Planilha de Resumo para conﬂitos na página XX apenas para dividir a responsabilidade com o Narrador, mas com apenas isso já será possível começar.

Se você é um novo Narrador este capítulo é a ponta do *iceberg* para você. Você deve ler e se familiarizar com o livro inteiro.

------

------

### Para Veteranos

Você talvez esteja lendo este livro por estar familiarizado com outros jogos que o usam *Fate*, como *Espírito do Século* ou *The Dresden Files Roleplaying Game*. Vários outros jogos do mercado como o *Bulldogs!* da Galileo Games ou *Legends of Aglerre*, da Cubicle Seven’s, também usam *Fate*.

Esta é uma nova versão do Fate que desenvolvemos para atualizar e simplificar o sistema. Você reconhecerá algumas coisas presentes aqui, mas alteramos algumas regras e algumas terminologias. Você encontrará um guia com todas as mudanças que fizemos, próxima ao final do livro

- [« Licença](../legal/)
- [O Que Você Precisa Para Jogar »](../o-que-voce-precisa-para-jogar/)
