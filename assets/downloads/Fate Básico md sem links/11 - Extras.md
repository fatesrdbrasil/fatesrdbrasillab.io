# 11 -  Extras


## O Que São Extras?

Um **Extra** em *Fate* é um termo bastante amplo. Vamos utilizá-lo para descrever tudo que tecnicamente faz parte do personagem ou é controlado por ele, mas que precisa de regras especiais. Se a sua aventura em *Fate* fosse um filme, os extras seriam os efeitos especiais.

Alguns exemplos incluem:

- Poderes mágicos e sobrenaturais;
- Ferramentas e equipamentos especiais, como armas e armaduras encantadas em um jogo de fantasia ou super tecnologia em um jogo de ficção;
- Veículos que os personagens possuem;
- Organizações ou locais sobre as quais os personagens possuam uma grande inﬂuência.

As ferramentas apresentadas aqui permitirão a você criar extras que se enquadrem perfeitamente em seu jogo.

Consideramos os extras como uma extensão da ficha de personagem, portanto quem controla o personagem que possui algum extra, controla também esses benefícios. Na maioria das vezes serão jogadores, mas PdNs também podem possuir extras controlados pelo Narrador.

É preciso pagar um **custo** ou receber **permissão** para adquirir um extra.

## A Regra de Bronze ou Fate Fractal

Antes de continuar, lembre-se do seguinte:

**Em *Fate*, tudo pode ser tratado como se fosse um personagem. Qualquer coisa pode possuir aspectos, perícias, façanhas, barra de estresse e consequências se for preciso.**

Chamamos isso de Regra de Bronze, mas você também pode conhecê-la como *Fate* Fractal caso costume ler sobre o assunto na internet. Já vimos alguns exemplos desta regra neste livro; damos aspectos ao jogo durante sua criação, colocamos aspectos de situação no ambiente assim como em personagens, e o Narrador pode permitir que riscos ambientais ataquem como se tivessem perícias.

Neste capítulo, estenderemos as possibilidades ainda mais.

## Criando Um Extra

A criação de um extra começa com uma conversa. Isso deve acontecer durante a criação do jogo ou criação do personagem. O grupo precisa decidir o seguinte:

- Quais elementos de sua ambientação se enquadram melhor como um extra?
- O que você quer que o extra faça?
- Quais características do personagem você precisa para expressar melhor o extra?
- Quais são os custos ou as permissões para possuir um extra? Uma vez que tenha respondido a tais perguntas, procure por exemplos nesse livro que lhe ajudem a traçar os detalhes e criar algo dentro do que deseja. Pronto!

### Definindo os Detalhes

Vocês possivelmente já têm ideias formadas para os extras depois da elaboração do jogo; praticamente todos os jogos de fantasia possuem algum tipo de poder mágico, assim como um jogo com heróis tem superpoderes. Se as coisas girarem em torno de algum lugar de importância – como uma nave intergaláctica, uma base central ou uma taverna favorita – considere criar esse ambiente como um extra.

Extras naturalmente chamam bastante atenção quando entram em cena – os jogadores têm uma atração por opções legais e interessantes, então é normal que essas coisas recebam muita atenção. Quando estiver conversando sobre as possibilidades de extras, tenha certeza que está preparado para que esses elementos recebam muito do foco do jogo.

> O grupo conversa sobre extras em *Caroneiros de Asteroide*. 
>
> Os poderes de Bandu são uma primeira opção óbvia, assim como a lançadora de cascavéis de Esopo. Léo e Michel não têm interesse em listas compridas de poderes e armas. Além disso, como é um jogo onde existem poderes, eles concordam que haverá itens poderosos também.
>
> Pensando nas questões e locais, eles decidem que não tornarão nenhum deles em extras – eles viajarão com frequência, e os personagens não possuem uma participação ativa em alguma organização para criar algum extra relacionado a isso.

### O Que Os Extras Fazem

Em termos gerais, procure saber o que você quer que o extra faça, comparado ao que as perícias, façanhas e aspectos já podem fazer. Também, pense em como são "diante das câmeras". O que as pessoas veem quando você o usa? Qual é sua aparência e quais são as sensações passadas?

Considere estes pontos:

- O extra tem inﬂuência na historia? Como?
- O extra permite que você faça algo que nenhuma outra perícia permite?
- O extra torna as suas perícias mais úteis e poderosas?
- Como descreveria o uso desse extra?

Este é um passo importante porque pode revelar que o extra na verdade não contribui tanto quanto imaginado, o que lhe ajuda a adicionar mais detalhes ou desconsiderá-lo.

> Para os poderes de Bandu, o grupo decide que querem manter as coisas bem discretas e vagas – é apenas mais uma forma de resolver problemas, como a propensão de brigar de Esopo ou a habilidade de Fräk com chicotes (que Maira prefere manter apenas na técnica) – um psiônico treinado deve ser tão temido quanto um atirador profissional, não mais.
>
> Eles concordam que isso pode inﬂuenciar a história de várias maneiras; imaginam lugares repletos de efeitos desconhecidos e enredos perfeitos para Bandu, assim como o desejo do Império de tomar posse desses conhecimentos.
>
> Eles decidem então que os poderes de Bandu permitirão que alguém interaja com o sobrenatural de uma forma que ninguém mais consegue fazer, e isso pode afetar e prejudicar as pessoas, mas novamente enfatizam que esses poderes não serão mais poderosos que as outras perícias. Efeitos básicos irão usar as quatro ações normais, e poderes mentais usarão desafios, disputas ou conﬂitos conforme apropriado.
>
> O grupo descarta a presença de poderes titânicos, capazes de alterar o universo ou a realidade, incendiando cidades e planetas, assim por diante. Se essas coisas existirem, serão o foco de algum cenário e fruto dos esforços de muitas pessoas sacrificando coisas muito importantes. O grupo não vê esses poderes mentais inﬂuenciando outras perícias diretamente, o que ajuda a manter sua natureza exclusiva. Michel imagina limitações e requisitos imprevisíveis – algumas coisas ele pode conseguir facilmente, outras não, e tudo depende do drama do momento para determinar quando e como. O grupo gosta da ideia e todos concordam.

### Atribuindo Elementos ao Personagem

Depois de ter a ideia geral, descubra que características do personagem você precisa para fazer o extra.

- Se o extra inﬂuencia a história, então deve usar aspectos.
- Se o extra cria um novo contexto para a ação, então ele deve usar perícias.
- Se o extra permite realizar algo extraordinário com uma perícia, então deve usar façanhas.
- Se o extra pode sofrer dano ou ser consumido de alguma forma, então deve poder receber estresse e consequências.

Um extra pode usar um aspecto como uma permissão – exigindo um determinado aspecto de personagem para acessar outras habilidades do aspecto. Seu personagem pode precisar ter nascido com algum traço ou obter certo status para fazer uso do aspecto. Ou o extra pode dar um novo aspecto ao qual o personagem tem acesso, se é o próprio extra que importa
na história.

Extras podem usar perícias de vários modos. O extra pode ser uma nova perícia que não pertença a lista padrão; pode modificar uma perícia, acrescentando funções às suas quatro ações; pode custar um espaço dedicado a uma perícia durante a criação de personagem ou durante sua evolução. É possível que um extra inclua uma ou mais perícias existentes que o jogador pode acessar enquanto o controla.

Escrever um extra como uma façanha funciona da mesma forma que criar uma façanha. Um extra pode ter algumas façanhas ligadas a ele – pode até mesmo incluir as perícias que tais façanhas modificam. Extras que incluem façanhas costumam custar pontos de Recarga, como façanhas.

Um extra que descreve alguma habilidade inerente ao personagem pode conceder-lhe mais uma linha de estresse – além de físico e mental. Um extra que represente uma entidade separada do personagem, como um local ou veículo, pode possuir suas próprias linhas de estresse físico. Você também pode escolher uma perícia que inﬂuencie essa nova linha de estresse – assim como Vigor provê caixas extras de estresse físico e consequências.

Sabendo melhor o que o extra faz, será mais fácil escolher os elementos do personagem que reforçam a ideia e determinar como serão usados.

> Para os poderes de Bandu, o grupo decide que usará aspectos e perícias - há uma inﬂuência direta na história, e seus poderes criam um novo jeito de lidar com problemas. Ele não quer que isso afete outras perícias e sim que seja um fator isolado, então acham melhor que não seja uma façanha. Eles também não imaginam algum tipo de “mana” ou outros recursos associados, então não vão usar estresse e consequências.

### Permissões e Custos

Uma **permissão** é uma justificativa narrativa que permite a você criar um extra. Na maioria das vezes, você estabelece uma permissão ao criar um extra que esteja relacionado aos aspectos de seu personagem, descrevendo o que o torna qualificado a receber o extra. Você também pode simplesmente concordar que faz sentido que aquela pessoa tenha aquele extra e pronto.

Um **custo** é como você paga pelo extra com os recursos disponíveis na sua ficha de personagem, podendo ser um ponto de perícia, um ponto de recarga, um espaço de façanha ou espaço de aspecto.

Felizmente, como os extras usam elementos do personagem que já são familiares a você, as coisas ficam mais simples – você vai pagar o que normalmente pagaria durante a criação de personagem. Se o extra é uma nova perícia, então coloque-o na pirâmide de perícias normalmente. Se for um aspecto, escolha um de seus cinco aspectos para sê-lo. Se for uma façanha, pague um ponto de recarga (ou mais) e coloque-o na sua ficha.

Narradores que não quiserem que os jogadores escolham entre possuir um extra e possuir as características normais de um personagem inicial têm a liberdade de aumentar a quantidade de cada uma das características disponíveis para que possam acomodar melhor os extras – apenas esteja certo de que cada PJ receba a mesma quantidade de espaços adicionais.

> Amanda sugere que Bandu possua um aspecto com algum efeito no jogo como permissão para o extra. Ele sugere ser colocado na mira de um grupo, o Império. Para poupar esforços, ela decide que a perícia a ser usada será Conhecimentos, logo não precisa de uma perícia a mais na pirâmide. Ela sugere que outros personagens que possuam tal perícia e algum aspecto relacionado podem manifestar poderes. Michel gostou, porque é simples e equilibrado, e concorda.

### Escrevendo

Uma vez que tenha todos esses elementos estabelecidos, você pode escrever seu extra. Parabéns!

------

### Extra: Poderes Mentais

**Permissões:** Um aspecto relacionado que seja reﬂetido no jogo.

**Custo:** Um nível de perícia, especificamente para a perícia Conhecimentos (normalmente, você também gastaria pontos de recarga por estar adicionando novas ações a uma perícia, mas Amanda e seu grupo consentem que isso não será necessário). 

Pessoas com poderes mentais são capazes de usar seu conhecimento para criar efeitos sobrenaturais, adicionando as seguintes ações à perícia Conhecimentos:

**Superar:** Use Conhecimentos para superar desafios mentais e disputas psicológicas, ou para questionar alguém.

**Criar Vantagem:** Use Conhecimentos para impor condições mentais ao seu alvo, como Lentidão ou Atordoado. O personagem pode usar Vontade para se defender.

**Ataque:** Use Conhecimentos para ferir diretamente alguém causando dano por agressão mental. O alvo pode se defender usando Vontade, ou Conhecimentos se possuir treinamento.

**Defender:** Use Conhecimentos para se defender de ataques mentais e dano psicológico.

------

## Extra e Evolução


Os extras evoluem assim como qualquer característica do personagem, de acordo com os marcos em *Criando Campanhas.* Para isso, podemos seguir algumas orientações:

- Um extra em forma de aspecto pode ser alterado em um marco menor ou em um marco maior se estiver ligado a seu conceito.
- Um extra em forma de perícia pode ser alterado a qualquer marco significativo ou maior, contanto que isso seja possível de acordo com a pirâmide, e você também pode adicionar novos extras nesses marcos. Também é possível trocar o nível do seu extra com outra perícia em um marco menor, como poderia fazer com duas perícias normais.
- Um extra em forma de façanha pode avançar em um marco maior ao ganhar um ponto de recarga. Isso pode significar a adição de novos efeitos a um extra existente ou a compra de um novo extra em forma de façanha. Você também pode alterar este tipo de extra em um marco menor como qualquer outra façanha.

Claro que muitos extras usam mais de um elemento. Recomendamos que você permita que os jogadores desenvolvam partes diferentes do extra em marcos separados, para minimizar a confusão durante o jogo.

## Mais Exemplos De Extras

Abaixo seguem mais alguns exemplos de extras prontos para uso, em níveis diferentes de detalhe, abrangendo o que é mais comum em mesas de RPG.

### Potência de Armas e Armaduras

Várias sugestões nesta sessão se referem a classificações de armas e armaduras. Você pode usá-las como padrão em jogos mais difíceis ou realistas, sem considerá-las como extras, se for apropriado - ser atingido por uma arma causará mais estragos, e usar armaduras impedirá que isso aconteça.

O valor da Arma é adicionado ao valor total de tensões de um golpe bem sucedido. Ou seja, se você possui uma Arma:2, significa que ela vale 2 tensões a mais do que o normal. Isso também conta para empates, então ao usar uma arma você causa estresse num empate ao invés de receber um impulso. Isso faz com que armas se tornem bem perigosas.

Um valor de Armadura reduz o número de tensões de um golpe recebido. Então, uma Armadura:2 faz o golpe que acertar causar 2 tensões a menos do que o normal. **Se você acertar mas a Armadura do alvo reduzir suas tensões a 0 ou menos, você recebe um impulso, mas não causa nenhum dano**.

Recomendamos ajustar a escala de dano de Armas variando de 1 a 4, tendo em mente que num empate uma Arma:4 será capaz de derrubar quatro PdNs sem importância de nível regular. Equilibre as Armaduras baseando-se no que você acha necessário para se proteger contra as armas de cada nível.

> Amanda conversa com o grupo sobre a possibilidade de adicionar classificações para Armas e Armaduras. Eles concordam, e então ela começa a criar exemplos de armas e suas classificações correspondentes. Como é um mundo de aventura e bravura, ela pensa sobre quais seriam as mais poderosas para se enquadrar em Armas 4 e acha que um canhão de fótons se encaixaria perfeitamente, o que pode acabar facilmente com um grupo de PdNs em apenas um golpe.
>
> Partindo desse ponto, ela termina assim:
>
> Arma:1 equivale a itens como soco-inglês, facas pequenas ou armas improvisadas. Armadura:1 equivale a roupas acolchoadas.
>
> Arma:2 equivale a pistolas, espadas de metal, adagas e cassetetes. Armadura:2 significa cobertura parcial ou proteção superficial.
>
> Arma:3 abrange a maioria dos riﬂes, armas de energia, espadas e qualquer coisa que use apenas uma mão. Armadura:3 inclui armaduras de placas de metal e escudos de energia.
>
> Arma:4 equivale a armas grandes e de duas mãos. Armadura:4 é usado para armaduras completas.

------

### Poder Equivalente

Antes de sair criando armas e armaduras loucamente para sua campanha, pense se isso é realmente *importante *e se fará diferença em um conﬂito.

Dizemos isso pois a primeira coisa que seus jogadores buscarão é eliminar a eficácia de seus adversários se equipando até os dentes. E a não ser que você queira que seus PdNs sejam abatidos facilmente, eventualmente você terá que fazer o mesmo. Se todos estiverem em mesmo nível em termos de armas e armaduras, você terá um jogo tipo “poder equivalente”, o que você poderia facilmente substituir por rolagens normais de perícia.

Uma forma de lidar com isso é criar uma diferença proposital entre os níveis de classificação de armas e armaduras, permitindo que um possa ter níveis mais altos que o outro. A história está do seu lado – a maioria das armaduras não podem proteger completamente contra armas paras as quais não estão preparadas. Uma cota de malha pode evitar o corte de uma espada, mas não será tão eficiente contra o golpe de uma maça com espinhos. Da mesma forma, uma armadura de placas pode salvar de um golpe de maça, mas a ponta de uma espada ou uma lança pode deslizar por entre as placas.

Outra opção é fazer com que armaduras realmente boas sejam extremamente raras, disponível apenas para os mais privilegiados, ricos ou de outro tipo de elite. Então, embora possa ser fácil obter uma espada tipo Arma:3, apenas a guarda real de uma região possui um ferreiro realmente capaz de produzir armaduras em tal nível. Os jogadores podem gastar bastante tempo tentando comprar, roubar ou obter uma, mas ao menos haverá muita ação e interpretação no processo.

Apenas tenha em mente que se você for colocar armas e armaduras de mesmo nível em sua campanha, você corre o risco do esforço desnecessário em algo que pode nem ser tão importante assim.

------

### Superpoderes

A maioria dos jogos com superpoderes tem algo em comum: o propósito de um superpoder é tornar o que você faz (suas perícias) mais incrível, e o fato de que é considerado normal que todos possuam superpoderes no jogo.

Isso facilita criar uma estrutura aplicável a vários tipos de cenário. Permissões não são necessárias, pois todos podem fazer isso (ou talvez o poder precise apenas de um aspecto de “origem”). Pegue qualquer poder que desejar e o transforme numa façanha. Se for preciso ir além dos limites normais de uma façanha para abranger aquele poder, acrescente mais um ponto de recarga para cada duas tensões de efeito causadas (ou para cada ação adicionada, ou exceção à regra). Se quiser vários "níveis" de poder, varie o valor de recarga que pode ser gasto neles.

Em seguida, dê aos PJs um número adicional de pontos de recarga para que comprem poderes.

Listamos alguns poderes abaixo! Este sistema também funciona se você está preparando um cenário com magia onde todos conhecem certas magias básicas, ou implantes cibernéticos simples.

*Todos são de um jogo chamado Chrome City. São superpoderes bem tradicionais com uma pegada cyberpunk em uma sociedade inteligente, cibernética e praticante de artes marciais.*

------

### Extra: Raio de Energia

**Custo ** 2 pontos de recarga

Você pode usar Atirar para lançar raios de energia, sem a necessidade de uma arma ou acessório. Você possui a liberdade de decidir qual a aparência do raio, seja uma força Elemental ou relâmpagos (isso não custa recarga, porque você já usa Atirar para atacar).

Você recebe +2 quando usa seu raio de energia para atacar ou criar vantagem e ele atinge como uma Arma:2. Se o seu cenário inclui armas mundanas, este poder tem um valor de Arma 2 níveis acima da arma mundana mais forte existente.

------

------

### Extra: Superforça

**Custo:** 2-6 pontos de recarga 

Seus ataques de Lutar equivalem a Arma:2 e quase todas as aplicações de “força bruta” de Vigor recebem +2. Cada 2 pontos adicionais de recarga gastos adicionam +2 aos bônus.

------

------

### Extra: Supervelocidade

**Custo:** 3 pontos de Recarga

Você sempre age primeiro na rodada. Se mais alguém no conﬂito possuir supervelocidade, compare as perícias normalmente.

Você recebe +2 em todas as rolagens de defesa que usarem Atletismo ou em disputas que dependam de velocidade.

Você ignora todos os aspectos de situação que impedem movimento, exceto por barreiras sólidas como paredes, e pode se colocar em qualquer zona que desejar no início de cada rodada, pois teve tempo o bastante para chegar lá.

------

------

### Extra: Super-resistência

**Custo:** 1-3 pontos de recarga

Você possui Armadura:2 em suas rolagens de defesa contra qualquer ataque físico. Cada ponto adicional de recarga adiciona +2 a esse total.

------

------

### Extra: Visão de Raio-X

**Custo:** 2 pontos de Recarga

Você não realiza rolagens de Percepção e Investigar se o objeto que procura estiver escondido atrás de superfícies opacas – assuma que você é automaticamente bem-sucedido.

Isso também ajuda a permanecer escondido, pois pode ver as pessoas que o procuram. +2 em Furtividade para evitar ser detectado.

------

------

### Dimensionando Poderes

Como pode ver, “equilibrar” os poderes em *Fate* é mais uma arte que uma ciência. Há algumas equivalências para você trabalhar, como 1 ponto de destino = 1 invocação ou 1 façanha, mas quando você começa a trabalhar nas exceções às regras, como na visão de raio-X, não há uma maneira fácil de calcular a equivalência de poder. Isso está ligado diretamente ao seu gosto pessoal e o *Fate* é um sistema bastante maleável.

Então não se preocupe muito ao criar poderes - escolha o que parece legal, e se algo der errado mais tarde, corrija. Pedimos aos jogadores para não resmungarem quanto a isso caso um de seus poderes seja suavizado. Você encontrará orientações mais precisas sobre isto no *Fate RPG Ferramentas do Sistema*.

------

### Equipamento Especial

Assim como os poderes, equipamentos normalmente melhoram o que seu personagem consegue realizar, então uma façanha parece ser o mais adequado (os veteranos em *Espírito do Século* irão lembrar-se da façanha Engenhoca Pessoal).

No entanto, equipamentos podem possuir muito valor narrativo. Uma espada encantada pode ter sua própria lenda e personalidade ou uma relíquia amaldiçoada pode reﬂetir uma família forçada a mantê-la por séculos. Use aspectos para descrever esse tipo de coisa e lembre-se que aspectos devem criar oportunidades para serem invocados e forçados. É possível dar um toque especial a essas invocações, se quiser, criando um bônus similar a uma façanha, uma única vez.

Um aspecto em um equipamento pode sugerir também a melhor situação para usá-lo, ou indicar porque ele é diferente de outros do mesmo tipo (como um riﬂe ***Perfeito Para Grandes Distâncias*** ou um modelo em particular que ***Nunca, Jamais Trava***).

Recomendamos não aprofundar demais nisso, dando a cada item dos PJs um aspecto ou façanha. Este é um jogo sobre personagens, não sobre seus pertences. Em geral, você deve assumir que se o personagem possui uma determinada perícia, isso incluirá as ferramentas apropriadas para que ela seja funcional. Reserve extras para itens que são únicos ou possuam valor pessoal, algo que não mudará constantemente ao longo do jogo.

------

### Equipamentos Baseados Na História

Se não quiser lidar com extras, há uma forma de criar equipamentos sem muitas regras: pense neles como uma ação bem-sucedida de criar vantagem que você está trazendo para a cena. Narradores já colocam coisas como ***Beco Estreito ***ou ***Terreno Acidentado***, e também podem aplicar isso para descrever as vantagens situacionais criadas pelos equipamentos dos personagens.

Então, se um PJ possui um riﬂe automático e ataca alguém que possui uma pistola, adicione o aspecto ***Maior Poder de Fogo*** ao seu personagem com uma invocação grátis no início da cena, assim como faria se tivesse criado essa vantagem com uma rolagem. Dessa forma você pode ajustar os benefícios de acordo com a narração – se você está lutando em um beco estreito, sua espada pode ser uma ferramenta desajeitada contra o seu oponente que possui uma adaga, então ele pode receber uma invocação grátis de ***Péssima Escolha*** como um aspecto ligado a você.

Nas raras situações onde você possui a ferramenta perfeita para um trabalho, o aspecto recebido pode contar como sendo "com estilo" e vir com duas invocações gratuitas.

------

### Extra: Flagelo do Mal, A Espada Encantada

**Permissão:** Encontrar a espada no jogo

**Custo:** Nenhum

A espada Flagelo do Mal possui o aspecto ***Destruidora de Criaturas Malignas***. O portador da espada pode invocar esse aspecto se estiver combatendo criaturas malignas. Você também pode estar sujeito a alguém forçar force esse aspecto; ela o deixa tentado a procurar e destruir criaturas sem se importar com suas prioridades, o que pode complicar seus objetivos, impedi-lo de fugir de tais criaturas ou outras complicações.

Além disso, invocar tal aspecto tem outros dois efeitos especiais: pode banir qualquer PdN maligno sem importância instantaneamente, sem a necessidade de um conﬂito ou disputa e pode revelar a presença de seres malignos em qualquer circunstância.

------

------

### Extra: Revólver de Duelo de Brace Jovannich

**Permissão:** Possuir o aspecto ***O Legado de Brace***

**Custo:** Um espaço de aspecto (para o aspecto de permissão) e um ponto de recarga

Brace Jovannich é o pistoleiro mais temido e mais respeitado no mundo de Vilania. Sua arma, conhecida mundialmente pelo assassinato de centenas, é sua agora. Apenas você sabe o porquê de não jogá-la fora e salvar-se dos problemas de uma reputação como essa.

Prepare-se para ter esse aspecto forçado quando as pessoas reconhecerem a arma e exigirem provas de que é digno dela, queiram se vingar por seus crimes passados, ou outros tipos de atenção indesejada. Por outro lado, além dos benefícios óbvios de combate, você pode invocar o aspecto quando usar a temível reputação de Brace em sua vantagem.

A pistola lhe concede +2 de bônus em qualquer ataque com Atirar feito contra um oponente em um duelo. Estamos falando de duelos formais e não apenas disparar contra alguém em um tiroteio – você desafia ou é desafiado por alguém, há contagem regressiva, etc. Se você estiver usando os valores de Armas em seu jogo, ela pode ser tratada como qualquer outro revólver normal.

------

### Cibernética e Superperícias

Na maioria das vezes você pode criar acessórios cibernéticos da mesma forma que os superpoderes mostrados acima: façanhas com valor de custo de recarga variável, baseado em quão legais elas são.

Em algumas ambientações, no entanto, há outro papel para a cibernética que beira a magia: ela permite que as pessoas façam coisas no ciberespaço, criando um novo contexto para ações relacionadas à tecnologia em si.

Nesse caso, você precisa de uma perícia personalizada, descrevendo essa nova área repleta de possibilidades e o que acontece nela.

Outro uso interessante de perícias personalizadas é determinar campos de conhecimento específicos de cada personagem em seu jogo, de forma a ter apenas uma pessoa que é chamada para resolver aquela situação. Ao invés de possuir a perícia Lutar, que qualquer um pode ter, por exemplo, você poderia ter uma perícia chamada Guerreiro, e apenas o PJ que for um guerreiro pode possuí-la. Histórias de crime combinam com esse tipo de abordagem, pois os tipos clássicos já são conhecidos (o planejador, o motorista, o golpista). Apenas certifique-se que todos entendam esse tipo de situação, pois atuar fora do seu tipo pode criar sérios problemas.

------

### Extra: Interface

**Permissão:** Possuir um kit de interface (isso acontece quando você escolhe esta perícia)

**Custo:** Níveis de perícia

A perícia Interface permite a você interagir com computadores e aparelhos tecnológicos de uma maneira que as pessoas comuns não podem. Você pode entrar na “cabeça” de uma máquina, conversar com ela como se fosse um amigo e lutar contra ela como faria em uma briga de bar. Claro, isso significa que a máquina também pode fazer esse tipo de coisa com você.

**Superar:** Use Interface para consertar sistemas digitais, invadir um sistema para contornar a segurança eletrônica de fechaduras e outros obstáculos, forçar um aparelho a ativar um gatilho pré-programado ou evitar que um gatilho seja ativado.

**Criar Vantagem:** Use Interface para aprender sobre as propriedades de algum dispositivo (ou seja descobrir seus aspectos), para verificar sistemas, plantar assinaturas falsas e informações forjadas em um sistema e
criar interrupções.

**Atacar:** Use Interface para invadir um sistema diretamente.

**Defender:** Use Interface para se defender contra ataques de sistemas digitais. Falhar em rolagens de defesas resultará em estresse e consequências físicas – uma interface neural significa que seu próprio cérebro está em risco.

------

------

### Extra: Comunicador

**Permissão:** Escolha o arquétipo “O Comunicador” na criação de personagem

**Custo:** Níveis de perícia e recarga para criar façanhas 

Outros podem espalhar boatos e rumores, mas você se foca nos meios de comunicação. Em seu mundo, os eventos diários tornam-se notícias, seja na televisão, rádio ou internet.

**Superar:** Use a perícia Mídia para disseminar informação ao público, com a velocidade de propagação que desejar. Incidentes
mais obscuros ou locais serão mais difíceis de espalhar e será difícil fazer sua versão ganhar atenção se a história já foi divulgada por outros canais. Sucesso significa que o público no geral acredita no que quer que você deseja que acreditem, embora PdNs mais importantes possam ter opiniões mais complexas.

**Criar Vantagem:** Use Mídia para colocar aspectos em eventos ou indivíduos que reﬂitam sua reputação depois da divulgação da sua história.

**Ataque:** Se você possuir poder o suficiente para afetar alguém psicologicamente com difamação e/ou intimidação através da mídia, use esta perícia como ataque.

**Defender:** Use Mídia para se defender de danos à sua reputação ou para ficar tranquilo quando outra pessoa usar esta perícia.

**Façanhas:**

- **Procura-se:** Você pode usar Mídia no lugar de Contatos numa ação de Superar para buscar serviços através de classificados.
- **Justiça Com as Próprias Mãos:** Você pode incitar pessoas à violência física usando Mídia e recebe como aliado dois PdNs sem importância Regulares (+1) na cena, que atacarão quem estiver contra você.

------

### Riqueza

Em alguns jogos, é importante ter na ponta do lápis quanta riqueza seu personagem possui - senhores feudais competindo por poder, presidentes de companhias usando de seus recursos para minar adversários, ou mesmo apostadores entre gângsteres. Em geral, *Fate* é bem liberal com esses valores, e costumamos recomendar não manter atenção exagerada a quantas moedas de ouro seu personagem tem no bolso.

Quando quiser que os recursos de um personagem sejam finitos, uma boa opção é usar uma caixa de estresse personalizada para representar o esgotamento das riquezas. Você cria uma nova possibilidade de conﬂito quando faz isso, permitindo receber estresse da mesma forma que recebe em ataques físicos ou mentais.

Você também pode usar algo como o modelo abaixo simular honra e reputação cenários onde isso for importante, como no Japão feudal.

------

### Extra: Recursos

**Permissões:** Nenhuma, qualquer um pode ter esta perícia

**Custo:** Níveis de perícia

Durante a criação os personagens ganham campos de consequência suave (***Um Empréstimo de Um Amigo***), moderada (***Dia de Pagamento***) e grave (***Querem Me Partir os Ossos!***) que eles podem usar nos conﬂitos relacionado a Recursos. 

Adicione as seguintes ações à perícia Recursos:

**Ataque:** Você pode realizar ataques financeiros para destruir os recursos de alguém ou forçá-los a gastarem muito para lidarem com você; isso inﬂige estresse e consequências financeiras. Se você tirar alguém de conﬂito dessa forma, significa que alguma mudança permanente grave ocorreu em suas finanças.

**Defender:** Use Recursos para manter o seu status contra as tentativas de destruir seu capital.

**Especial:** A perícia Recursos agora também adiciona caixas de estresse à sua ficha: Estresse Financeiro. Você pode ser forçado a receber estresse financeiro sempre que falhar em uma rolagem de Recursos – essencialmente, usar o seu dinheiro é considerado um ataque. Estresse financeiro não se recupera tão rapidamente quando o físico ou mental – a linha de estresse se recupera a cada sessão e não a cada cena.

------

------

Uma opção interessante de avanço é considerar a possibilidade de redução permanente da perícia Recursos como uma troca para melhorar alguns extras, se for algo que possa ser comprado.

------

### Veículos, Locais e Organizações

Agrupamos os três na mesma categoria porque se você deseja que sejam realmente importantes, seu impacto deveria ser significativo o bastante para justificar que tenham sua própria ficha.

Nem sempre precisa ser algo tão complicado, especialmente se você estiver criando algo mais leve – é completamente válido, por exemplo, colocar algumas façanhas legais em um veículo e usar as regras acima para superpoderes ou equipamento. Isso deve ser usado em veículos que possuam papéis simbólicos e marcantes no jogo, tão icônicos quanto a *Enterprise *ou a *Millennium Falcon*.

Se você fornecer ao extra as suas próprias perícias, estará sugerindo que esse extra possui a capacidade de agir independentemente de você e precisará justificar o porquê. Dependendo do extra, pode ser preciso contextualizar o que as perícias significam ou criar uma nova lista de perícias apropriadas para a maneira como o extra age.

*Neste jogo, os personagens recebem um punhado de pontos extras de recarga, níveis de perícia e espaços de aspectos para investir em navios. O grupo decide que todos irão investir em um único e incrível veleiro.*

------

### Extra: O Ventania

**Permissões:** Nenhuma; faz parte do conceito do jogo

**Custo:** Níveis de perícia, recarga e espaços de aspectos, investimento de vários personagens

**Aspectos:** ***Navio Mais Veloz Da Frota, Compartimentos Secretos, Lorde Tamarin Deseja Afundá-lo***

**Perícias:** (representando a tripulação; os PJs podem usar suas próprias perícias se forem mais altas)

Percepção Boa (+3), Atirar e Velejar Razoáveis (+2)

**Façanhas:**

- **Incansavelmente Veloz:** O Ventania recebe +2 em qualquer rolagem de Velejar para vencer disputas de velocidade.
- **Armadilhas Inesperadas:** Por um ponto de destino, qualquer PJ pode ter uma Arma:2 ou adicionar ao valor de sua arma para qualquer ataque usando Lutar que acontecer a bordo, por ativar qualquer uma das armadilhas espalhadas por todo o convés e interior do navio como parte de sua ação.

------

*Este extra é para um jogo onde os PJs interpretam personagens de diferentes nações em um mundo de fantasia e todas as ações estão ligadas intimamente a política. Os PJs precisam criar uma ficha de personagem para suas nações.*

------

------

### Extra: A Teocracia Satveriana

**Permissões: **Nenhuma; faz parte do cenário de jogo

**Custo: **Alguns aspectos especiais, níveis de perícia e façanhas

Este pequeno estado-nação é conhecido por sua vasta rede de espiões e as leis que protegem os ricos e poderosos, normalmente às custas dos camponeses. Você é o governante; parabéns. Quando agir contra outras nações use as perícias presentes aqui no lugar das de seu personagem. Nesse caso, essas perícias representam os esforços de seus espiões, nobres, artesãos e exércitos.

**Aspectos:** ***Estamos Lhe Observando; Os Ricos Engolem Os Pobres; Mentes Afiadas, Lâminas Cegas***

**Perícias:**

- Investigar Ótimo (+4)
- Recursos Bons (+3)
- Ofícios Razoáveis (+2)
- Lutar Regular (+1)

**Façanha: Contraespionagem:** A Teocracia pode usar Investigar para se defender das tentativas de outras nações de descobrir seus aspectos. Sucesso com estilo numa rolagem de defesa permite que a Teocracia envie um aspecto de informação falsa.

------

### Magia

Quando estiver criando um sistema de magia, a discussão preliminar é extremamente importante, pois é quando serão estabelecidas algumas diretrizes sobre o que é possível ou não e quão significativos são os efeitos. Não há dois mundos de fantasia que possuam as mesmas propriedades mágicas e, muitas vezes, definir os elementos arcanos define uma parte vital do funcionamento do mundo. Portanto, estes exemplos são bastante detalhados, usando o máximo de elementos dos personagens.

*Magdalene é uma feiticeira poderosa no mundo de* Vilania, *uma alta fantasia de proporções intensas e perigosas, onde o acesso a poderes sombrios propicia o surgimento constante de seres cruéis e de desejos insaciáveis. Seu grupo é formado por uma miríade de seres mágicos, de almas aprisionadas em armaduras vivas e paladinos sem deuses a monstros que replicam habilidades ao devorar suas vítimas e pistoleiros mortos-vivos.*

*Há três formas de canalizar poderes em* Vilania. *Para os exemplos a seguir, utilizaremos os Pactos, uma transmissão de aptidões de forças mais intensas através da energia tênue que permeia o mundo. Eles são o amálgama da força de vontade remanescente nas zonas de oniração, onde sonhos e pesadelos tomam forma, e a força das palavras de criação. Basicamente, palavras de poder conciliadas ao poder da crença. Além disso, os usuários de magia recebem uma linha de estresse mágico, reﬂetindo o peso do uso desses poderes em suas vidas.*

------

### Extra: Pactos

**Permissões:** Nenhuma, pois virtualmente qualquer um pode assumir pactos para receber poderes

**Custos:** Aspectos, perícias, estresse e consequências

Durante a criação, os personagens ganham 2 espaços para aspectos que usarão para descrever suas relações com as entidades, declarando também sua ligação com os mesmos.

------

Para usufruir dos poderes de um pacto, você deve possuir a perícia Concórdia.

------

### Concórdia

Esta é a perícia utilizada para manipular poderes de outras entidades.

**Superar:** Use Concórdia para revidar e anular efeitos mágicos de espíritos menores ou sem forma (PdNs sem importância), ou para impor sua vontade sobre uma entidade com a qual você não possui um pacto. Falhar em rolagens desse tipo pode causar danos à sua barra de estresse mágico, através de estresse ou consequências.

**Criar Vantagem:** Use Concórdia para conseguir invocações gratuitas dos aspectos da sua entidade ou para modificar energias de um local a seu favor.

**Ataque:** Use Concórdia para nota: não é possível atacar humanos dissipar espíritos e demônios ou outros seres corpóreos diretamente com esta ação).

**Defender:** Use Concórdia para se defender de inﬂuências e ataques mágicos hostis. Falhar em rolagens desse tipo pode causar danos à sua barra de estresse mágico.

**Especial:** Concórdia adiciona caixas extras de estresse e consequência à sua barra de estresse mágico, usando as mesmas regras de Vigor e Vontade. Consequências de um ataque mágico literalmente alteram o universo ao redor do personagem, como ***Má Sorte Constante*** e ***Corrupção Por Onde Passo***.

------

Cada entidade recebe uma ficha simples descrevendo suas características, interesses e benefícios - você pode receber estes gastando invocações que conseguir com a perícia Concórdia, ou ao gastar dois pontos de destino. Um benefício sempre permite que você descreva algo que acontece na história sem precisar rolar os dados.

------

### Escarja, Entidade das Cinzas

**Domínios:** Defesa e Proteção

**Interesses:** Promover equilíbrio segundo seu próprio julgamento

**Benefícios:**

- Você pode impedir qualquer calamidade mundana uma vez por cena – evitar um acidente, deter alguém antes de uma queda ou tirar alguém do alcance da explosão. Não há necessidade de rolagem para isso; apenas acontece. Você não pode usar isso para evitar um acontecimento, apenas para alterar o seu resultado.
- Você pode criar um escudo de energia Ótimo (+4) para proteger a si mesmo e qualquer um que desejar. Esse valor é cumulativo com qualquer outra oposição ativa que você ou o alvo que escolher puder gerar. Se alguém conseguir ultrapassar a oposição, o escudo desaparece e você precisará criá-lo novamente. (Sim, você pode fazer isso com as suas invocações grátis. Supostamente, há outros seres cujos poderes podem atingir estes escudos com mais facilidade).

------

*Abaixo segue a construção de um mundo de fantasia usando as escolas de magia.*

------

### Extra: Escolas de Poder

**Permissões:* *Um aspecto que descreva a ordem a qual pertence

**Custos:** Aspecto (para a permissão), perícias (de certa forma) e recarga

Seu aspecto lhe permite tornar-se membro de uma das diversas ordens arcanas. Essas ordens possuem fichas simples com aspectos, perícias e façanhas. Tornar-se membro de uma ordem permite a você “adotar” parte de suas características.

Você pode pertencer a apenas uma ordem por vez e deixar uma ordem para servir a outra é praticamente inadmissível (uma opção interessante para os PJs lidarem durante uma campanha).

------

------

### O Bálsamo

**Aspectos:** ***Decepção é a Única Verdade, A Morte Nos Espreita, Mate Os Poderosos Antes Que Eles Matem Você***

**Perícias:**

- Aprender Ótimo (+4)
- Criar Bom (+3)
- Destruir Razoável (+2)
- Alterar Regular (+1)

**Façanhas:**

- **Necromancia:** +2 em qualquer perícia da Liga Negra para afetar os mortos.
- **Segredos Ocultos:** Uma vez por cena, você pode refazer uma rolagem de Enganar e escolher o melhor resultado.
- **Dançarino Das Sombras:** Quando usa a perícia Criar, adicione uma invocação grátis a qualquer aspecto de situação que criou que envolva as trevas.

------

As perícias mágicas são Criar, Destruir, Aprender e Alterar. Cada ordem prioriza uma delas de Ótimo (+4) a Regular (+1). Use a perícia *mais baixa* entre a perícia da ordem e a sua perícia Conhecimentos quando realizar ações mágicas.

Você ganha uma façanha grátis além das oferecidas por sua ordem e pode criar outras gastando recarga. Os aspectos da ordem podem ser invocados por você ou forçados contra você como se fossem seus.

Você pode pedir uma rolagem de suas perícias mágicas quando algo torna o uso de suas perícias mundanas impossível. Por exemplo, se você não consegue dar continuidade ao interrogatório de um suspeito porque a tortura o matou, você pode realizar uma rolagem de superar usando a perícia Aprender para descobrir o que for preciso usando sua magia. Se alguém está passando por uma depressão profunda e incurável por meios comuns, crie uma vantagem com Alterar para mudar seu temperamento.
